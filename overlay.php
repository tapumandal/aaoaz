<?php
error_reporting(0);
if ($_GET['job'] == "overlay") {
	if ($_GET[cata] == "food") {
		foodOverlay();
	} else if ($_GET[cata] == "electronics") {
		electronicsOverlay();
	} else if ($_GET[cata] == "life style") {
		lifeStyleOverlay();
	} else if ($_GET[cata] == "tourism") {
		tourismOverlay();
	} else if ($_GET[cata] == "residence") {
		residenceOverlay();
	}
}

function overlayStringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '/', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '.', $string);
	$string = str_replace(')', '', $string);
	$string = str_replace('(', '', $string);
	return $string;
}

function rating($value) {
	return '<div class="comment_review_rate">
						<ul class="star-rating">
						  <li class="current-rating" id="current-rating" style="width:' . $value . '%"><!-- will show current rating --></li>
						  <span id="ratelinks">
						  <li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
						  <li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
						  <li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
						  <li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
						  <li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
						  </span>
						</ul>
					</div>';
}

function fetchMultiImage($item_id, $itemMainImg) {
	include 'data_connection.php';

	$fetch_multi_image = "SELECT * FROM `item_multi_image` WHERE item_id = '$item_id' LIMIT 5;";
	$store = "";

	$store = '<div class="multi_single_img "><img class="img_bt" src="image/item_image/s_' . $itemMainImg . '"> </div>';

	$data = $conn->query($fetch_multi_image);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$store = $store . '<div class="multi_single_img "><img class="img_bt" src="image/item_image/item_multi_image/s_' . $row[image_link] . '"> </div>';

		}
	}
	return $store;
}

function giveRate() {

	$getCata = overlayStringFilter($_GET[cata]);
	$getSub_cata = overlayStringFilter($_GET[sub_cata]);
	$getId = overlayStringFilter($_GET[id]);

	echo '<div id="review">
				<div class="comment_review_input">
					<input class="product_id" type="text" name="content" onkeyup="rate_it(this.value, \'id\')" placeholder="Enter Item ID">
					<input class="product_name" type="text" name="content" onkeyup="rate_it(this.value, \'name\')" placeholder="Enter Item Name">
					<input class="review_txt" type="text" name="content" onkeyup="rate_it(this.value, \'text\')" placeholder="Enter your Openion">

				</div>

				<div class="comment_user_review">
					<div class="comment_review_quality">
						<div class="comment_review_title">
							QUALITY
						</div>
						<div>
								<ul class="star-rating">
								  <li class="current-rating" id="current-rating_1" style="width:0%"><!-- will show current rating --></li>
								  <span id="ratelinks_1" >
								  <li><a href="javascript:void(0)" title="1 star out of 5" onclick="review_in(1,1)" class="one-star">1</a></li>
								  <li><a href="javascript:void(0)" title="2 stars out of 5" onclick="review_in(2,1)" class="two-stars">2</a></li>
								  <li><a href="javascript:void(0)" title="3 stars out of 5" onclick="review_in(3,1)" class="three-stars">3</a></li>
								  <li><a href="javascript:void(0)" title="4 stars out of 5" onclick="review_in(4,1)" class="four-stars">4</a></li>
								  <li><a href="javascript:void(0)" title="5 stars out of 5" onclick="review_in(5,1)" class="five-stars">5</a></li>
								  </span>
								</ul>



						</div>
					</div>

					<div class="comment_review_quality">
						<div class="comment_review_title">
							PRICE
						</div>
						<div>
								<ul class="star-rating">
								  <li class="current-rating" id="current-rating_2" style="width:' . $rate . '%"><!-- will show current rating --></li>
								  <span id="ratelinks_2">
								  <li><a href="javascript:void(0)" title="1 star out of 5" onclick="review_in(1,2)" class="one-star">1</a></li>
								  <li><a href="javascript:void(0)" title="2 stars out of 5" onclick="review_in(2,2)" class="two-stars">2</a></li>
								  <li><a href="javascript:void(0)" title="3 stars out of 5" onclick="review_in(3,2)" class="three-stars">3</a></li>
								  <li><a href="javascript:void(0)" title="4 stars out of 5" onclick="review_in(4,2)" class="four-stars">4</a></li>
								  <li><a href="javascript:void(0)" title="5 stars out of 5" onclick="review_in(5,2)" class="five-stars">5</a></li>
								  </span>
								</ul>


						</div>
					</div>

					<div class="comment_review_quality">
						<div class="comment_review_title">
							SERVICE
						</div>
						<div>
								<ul class="star-rating">
								  <li class="current-rating" id="current-rating_3" style="width:' . $rate . '%"><!-- will show current rating --></li>
								  <span id="ratelinks_3">
								  <li><a href="javascript:void(0)" title="1 star out of 5" onclick="review_in(1,3)" class="one-star">1</a></li>
								  <li><a href="javascript:void(0)" title="2 stars out of 5" onclick="review_in(2,3)" class="two-stars">2</a></li>
								  <li><a href="javascript:void(0)" title="3 stars out of 5" onclick="review_in(3,3)" class="three-stars">3</a></li>
								  <li><a href="javascript:void(0)" title="4 stars out of 5" onclick="review_in(4,3)" class="four-stars">4</a></li>
								  <li><a href="javascript:void(0)" title="5 stars out of 5" onclick="review_in(5,3)" class="five-stars">5</a></li>
								  </span>
								</ul>

						</div>
					</div>

				</div>
				<a class="review_but com_submit" href="#0" onclick="submit_rate(\'rate\', ' . $getCata . ' , ' . $getSub_cata . ' , ' . $getId . ')">Submit</a>
				<img style="height:40px" src="image/default_image/review.png">
			</div>';
}

function foodOverlay() {
	include "data_connection.php";
	include "code_php.php";

	$getItem_id = overlayStringFilter($_GET[item_id]);
	$getCata = overlayStringFilter($_GET[cata]);
	$getSub_cata = overlayStringFilter($_GET[sub_cata]);
	$getId = overlayStringFilter($_GET[id]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$total_rate = ($row[total_rate] / $row[rate_people]) / 3;
			$total_rate = $total_rate * 20;
			$rate_1 = ($row[rate_1] / $row[rate_people]) * 20;
			$rate_2 = ($row[rate_2] / $row[rate_people]) * 20;
			$rate_3 = ($row[rate_3] / $row[rate_people]) * 20;

			$multiImage = fetchMultiImage($getItem_id, $row[image_link]);

			// Blank image remove
			$image = strpos($row['image_link'], ".jpg");
			if (strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false || empty($row['image_link'])) {
				$imageLink = "SELECT image_link FROM general_info WHERE id = $row[cata_id]";
				$imgLink = $conn->query($imageLink);
				if ($imgLink->num_rows > 0) {
					while ($imgRow = $imgLink->fetch_assoc()) {
						$image = "./image/title_image/" . $imgRow[image_link];
					}
				}
			} else {
				$image = "./image/item_image/" . $row['image_link'];
			}
			// Blank image remove

			echo '
							<div class="col-sm-12 popup">
								<div class="col-sm-12 popup_head">
									 <div class="side_nav_close popup_details_close_btn">
							            <a href="javascript:void(0)" class="closebtn sideNavClose" onclick="closeDetailsOverlay()">&times;</a>
							          </div>
									<div class="col-sm-3 img_">
										<div class="single_image" id="single_image">
											<img class="one_img" src="' . $image . '">
										</div>

										<div class="multi_image">
										' . $multiImage . '
										</div>

									</div>
									<div class="col-sm-9">
										<div class="col-sm-3 name_rate">
											<div class="popup_name">
												<span>' . $row[name] . '</span>
											</div>
											<div class="popup_rate">
													<div class="comment_user_review">

																<div class="comment_review_quality">
																	<div class="comment_review_title">
																		<span>Avg Rating</span>
																	</div>
																	' . rating($total_rate) . '
																</div>


																<div class="comment_review_quality">
																	<div class="comment_review_title">
																		<span>TASTE</span>
																	</div>
																	' . rating($rate_1) . '
																</div>

																<div class="comment_review_quality">
																	<div class="comment_review_title">
																		<span>PRICE</span>
																	</div>
																	' . rating($rate_2) . '
																</div>

																<!-- style="margin-left:110px" -->
																<div class="comment_review_quality" >
																	<div class="comment_review_title">
																		<span>SERVICE</span>
																	</div>
																	' . rating($rate_3) . '
																</div>
													</div>
											</div>
										</div>
										<div class="col-sm-8 popup_des ">
											<span>' . $row[price] . '</span>
											<br><br>
											<span class="pop_des">' . $row[description] . '</span>

										</div>
									</div>
								</div>';
			//giveRate();
			echo '</div>
							<!-- popup -->
							';
		} //overlay item data while

		$addView = "UPDATE `item_list` SET `view` = `view`+1 WHERE `id`=$getItem_id";
		$conn->query($addView);
	} //overlay item data if

	$fetch_item_des = "SELECT * FROM `item_des` WHERE item_id = '$getItem_id';";

	$data = $conn->query($fetch_item_des);
	$left_right_pos = 1;

	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			if ($left_right_pos % 2 == 1) {
				echo '
						<div class="col-sm-12">
							<div class="product_details">
								<div class="product_details_1">
									<div class="product_details_left product_img">
										<img class="img-responsive " src="image/item_des/' . $row[image_link] . '">
										<div class="product_img_name_left"><span>' . $row[image_name] . '</span></div>
									</div>
									<div class="product_details_left_des">
										<span>' . $row[image_des] . '</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						';
				$left_right_pos++;
			} else if ($left_right_pos % 2 == 0) {
				echo '
						<div class="col-sm-12">
							<div class="product_details">
								<div class="product_details_1">
									<div class="product_details_right product_img">
										<img class="img-responsive" src="image/item_des/' . $row[image_link] . '">
										<div class="product_img_name_right"><span>' . $row[image_name] . '</span></div>
									</div>
									<div class="product_details_right_des">
										<span>' . $row[image_des] . '</span>
									</div>

								</div>
							</div>
						</div>

						';
				$left_right_pos++;
			}
		}
	}
	echo '<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
					<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->

		   				<div class="com">';

	$general_obj = new general();
	$comment_id = null;
	$general_obj->fetch_comment($getCata, $getSub_cata, $getId, $comment_id, $getItem_id);

	echo '		</div>
		   			</div>   <!-- COMMENT  -->
				</div>   <!-- COMMENT AND QUESTIOIN -->
				';

} //food_overlay

function electronicsOverlay() {

	include "data_connection.php";
	include "code_php.php";

	$getItem_id = overlayStringFilter($_GET[item_id]);
	$getCata = overlayStringFilter($_GET[cata]);

	$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$total_rate = ($row[total_rate] / $row[rate_people]) / 3;
			$total_rate = $total_rate * 20;
			$rate_1 = ($row[rate_1] / $row[rate_people]) * 20;
			$rate_2 = ($row[rate_2] / $row[rate_people]) * 20;
			$rate_3 = ($row[rate_3] / $row[rate_people]) * 20;

			$k = 0;
			$fetch_item_des = "SELECT * FROM `item_multi_image` WHERE item_id = '$getItem_id' AND cata='$getCata';";
			$data2 = $conn->query($fetch_item_des);

			$multiImage = fetchMultiImage($getItem_id, $row[image_link]);

			if ($data2->num_rows > 0) {
				while ($row2 = $data2->fetch_assoc()) {

					echo "===";
					if ($k % 2 == 0) {
						$imgDesData = $imgDesData . '
										<div class="product_details_1">
											<div class="product_details_left">
												<img class="img-responsive" src="image/item_image/item_multi_image/' . $row2[image_link] . '">
											</div>
											<div class="product_details_right">
												<span>' . $row2[short_note] . '</span>
											</div>
										</div>
										<div class="clearfix"></div>'
						;
					} else {
						$imgDesData = $imgDesData . '
										<div class="product_details_2">
											<div class="product_details_left">
												<span>' . $row2[short_note] . '</span>
											</div>
											<div class="product_details_right">
												<img class="img-responsive" src="image/item_image/item_multi_image/' . $row2[image_link] . '">
											</div>
										</div>
										<div class="clearfix"></div>
										';
					}
					$k++;

				}
			}
			echo '
						<div class=" popup">
							<div class="col-sm-12 popup_head">
									<div class="col-sm-3 img_">
										<div class="single_image" id="single_image">
											<img class="one_img" src="image/item_image/' . $row[image_link] . '">
										</div>

										<div class="multi_image">
										' . $multiImage . '
										</div>

									</div>
									<div class="col-sm-2 name_rate">
										<div class="popup_name">
											<span>' . $row[name] . '</span>
										</div>
										<div class="popup_rate">
												<div class="comment_user_review">

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>Avg Rating</span>
																</div>
																' . rating($total_rate) . '
															</div>


															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>QUALITY</span>
																</div>
																' . rating($rate_1) . '
															</div>

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>PRICE</span>
																</div>
																' . rating($rate_2) . '
															</div>

															<!-- style="margin-left:110px" -->
															<div class="comment_review_quality" >
																<div class="comment_review_title">
																	<span>SERVICE</span>
																</div>
																' . rating($rate_3) . '
															</div>
												</div>
										</div>
									</div>
									<!-- popup -->
									<div class="col-sm-7 popup_des ">
										<span><b>Price:</b> ' . $row[price] . '</span><br>
										<span><b>Short Description:</b> ' . $row[description] . '</span>
									</div>
								</div>
							<div class="clearfix"></div>

							<!-- details info table -->
							<div class="col-sm-12 elec_details">
								<div class="col-sm-6 pro_table">
										<table>
										      <thead>
										        <tr>
										          <th class="spe_1">Specification</th>
										          <th class="spe_2">Details</th>
										        </tr>
										      </thead>
										      <tbody>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Sim</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_1] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">ANT</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_2] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">USB</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_3] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Ear Jack</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_4] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">MHL</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_5] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Wi-Fi</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_6] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Bluetooth</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_7] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Operating System</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_8] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Display</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_9] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Resolution and Color</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_10] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">S Pen</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_11] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Processor</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_12] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">RAM</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_13] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Rear Camera</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_14] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Front Camera</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_15] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Camera Flash</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_16] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Sensor</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_17] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Dimention</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_18] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Battery</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_19] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Internet Usage Time</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_20] . '</td>
										        </tr>

										        <tr>
										          <td class="spe_1 spe_line1 item-name">Talk Time</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_21] . '</td>
										        </tr>

										        <tr>
										          <td class="spe_1 spe_line2 item-name">Batery Removale</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_22] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Video Playing Format</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_23] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Audio Playing Formate</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_24] . '</td>
										        </tr>



										      </tbody>
										</table>
								</div>

									<div class="col-sm-6">
										<div class="product_details">



										' . $imgDesData . '
										</div>
									</div>

							</div>
							<!-- details info table -->

						</div>
						<!-- popup -->
					';
		}
	}
}
// electronics_overlay function

function lifeStyleOverlay() {
	include "data_connection.php";
	include "code_php.php";

	$getItem_id = overlayStringFilter($_GET[item_id]);
	$getCata = overlayStringFilter($_GET[cata]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$multiImage = fetchMultiImage($getItem_id, $row[image_link]);
			if ($getcata == "beauty" || $getcata == "fitness") {
				echo '
							<div class=" popup">
								<div class="col-sm-12 popup_head">
										<div class="col-sm-3 img_">
										<div class="single_image" id="single_image">
											<img class="one_img" src="image/item_image/' . $row[image_link] . '">
										</div>

										<div class="multi_image">
										' . $multiImage . '
										</div>

									</div>

									<div class="col-sm-2 name_rate">
										<div class="popup_name">
											<span>' . $row[name] . '</span>
										</div>

										<div class="">
											<span>Price: ' . $row[price] . '</span>
										</div>

										<div class="">
											<span>Color: ' . $row[color] . '</span>
										</div>

										<div class="">
											<span>Size: ' . $row[size] . '</span>
										</div>

										<div class="popup_rate">
											<div class="comment_user_review">

														<div class="comment_review_quality">
															<div class="comment_review_title">
																<span>Avg Rating</span>
															</div>
															' . rating($total_rate) . '
														</div>


														<div class="comment_review_quality">
															<div class="comment_review_title">
																<span>QUALITY</span>
															</div>
															' . rating($rate_1) . '
														</div>

														<div class="comment_review_quality">
															<div class="comment_review_title">
																<span>PRICE</span>
															</div>
															' . rating($rate_2) . '
														</div>

														<!-- style="margin-left:110px" -->
														<div class="comment_review_quality" >
															<div class="comment_review_title">
																<span>SERVICE</span>
															</div>
															' . rating($rate_3) . '
														</div>
											</div>
										</div>
									</div>

									<div class="col-sm-7 popup_des ">
										<span>' . $row[description] . '</span>
									</div>

								</div>
							</div>
						';
			} else {
				echo '
							<div class=" popup">
								<div class="col-sm-12 popup_head">
									<div class="col-sm-3 img_">
										<div class="single_image" id="single_image">
											<img class="one_img" src="image/item_image/' . $row[image_link] . '">
										</div>

										<div class="multi_image">
										' . $multiImage . '
										</div>

									</div>

									<div class="col-sm-2 name_rate">
										<div class="popup_name">
											<span>' . $row[name] . '</span>
										</div>

										<div class="">
											<span>Price: ' . $row[price] . '</span>
										</div>

										<div class="">
											<span>Color: ' . $row[color] . '</span>
										</div>

										<div class="">
											<span>Size: ' . $row[size] . '</span>
										</div>
									</div>

									<div class="col-sm-7 popup_des ">
										<span>' . $row[description] . '</span>
									</div>

								</div>
							</div>
						';

			}
		} //overlay item data while
	} //overlay item data if
} //lifeStyleOverlay

function tourismOverlay() {
	include "data_connection.php";
	include "code_php.php";

	$getItem_id = overlayStringFilter($_GET[item_id]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$multiImage = fetchMultiImage($getItem_id, $row[image_link]);
			echo '
							<div class=" popup">
								<div class="col-sm-12 popup_head">
									<div class="col-sm-3 img_">
										<div class="single_image" id="single_image">
											<img class="one_img" src="image/item_image/' . $row[image_link] . '">
										</div>

										<div class="multi_image">
										' . $multiImage . '
										</div>

									</div>
									<div class="col-sm-2 name_rate">
										<div class="popup_name">
											<span>' . $row[name] . '</span>
										</div>
										<div class="popup_rate">
												<div class="comment_user_review">

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>Avg Rating</span>
																</div>
																' . rating($total_rate) . '
															</div>


															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>QUALITY</span>
																</div>
																' . rating($rate_1) . '
															</div>

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>PRICE</span>
																</div>
																' . rating($rate_2) . '
															</div>

															<!-- style="margin-left:110px" -->
															<div class="comment_review_quality" >
																<div class="comment_review_title">
																	<span>SERVICE</span>
																</div>
																' . rating($rate_3) . '
															</div>
												</div>
										</div>
									</div>
									<!-- popup -->
									<div class="col-sm-7 popup_des ">
										<span>' . $row[description] . '</span>

									</div>
								</div>
							</div>
							<!-- popup -->
							';
		} //overlay item data while
	} //overlay item data if
}

function residenceOverlay() {
	include "data_connection.php";
	include "code_php.php";

	$getItem_id = overlayStringFilter($_GET[item_id]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$multiImage = fetchMultiImage($getItem_id, $row[image_link]);
			echo '
							<div class=" popup">
								<div class="col-sm-12 popup_head">
									<div class="col-sm-3 img_">
										<div class="single_image" id="single_image">
											<img class="one_img" src="image/item_image/' . $row[image_link] . '">
										</div>

										<div class="multi_image">
										' . $multiImage . '
										</div>

									</div>
									<div class="col-sm-3 name_rate">
										<div class="popup_name">
											<span>' . $row[name] . '</span>
										</div>
										<div class="">
		                                    <span><b>Price in BDT : </b> ' . $row[price] . '</span>
		                                </div>
		                                <div class="">
		                                    <span><b>Price in USD : </b> ' . $row[price_2] . '</span>
		                                </div>

										<div class="popup_rate">
												<div class="comment_user_review">

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>Avg Rating</span>
																</div>
																' . rating($total_rate) . '
															</div>


															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>QUALITY</span>
																</div>
																' . rating($rate_1) . '
															</div>

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>PRICE</span>
																</div>
																' . rating($rate_2) . '
															</div>

															<!-- style="margin-left:110px" -->
															<div class="comment_review_quality" >
																<div class="comment_review_title">
																	<span>SERVICE</span>
																</div>
																' . rating($rate_3) . '
															</div>
												</div>
										</div>
									</div>
									<!-- popup -->
									<div class="col-sm-6 popup_des ">
										<span><b>Bed Type : </b> ' . $row[column_1] . '</span><br>
										<span><b>View : </b> ' . $row[column_2] . '</span><br>
										<span><b>Facilities : </b> ' . $row[column_3] . '</span><br>
										<span><b>Overview : </b> ' . $row[description] . '</span>

									</div>
								</div>
							</div>
							<!-- popup -->
							';
		} //overlay item data while
	} //overlay item data if
}

?>