<base href="/">
<meta charset="utf-8">
<link rel="icon" href="image/default_image/short_logo.png">
<?php
// include "data_connection.php";
$getMetaData = "SELECT `meta_title`, `meta_description`, `meta_keyword` FROM `general_info` WHERE id = '$_SESSION[content_id]'";
$meta_title = "";
$meta_description = "";
$meta_keyword = "";

$data = $conn->query($getMetaData);
if ($data->num_rows > 0) {
	while ($row = $data->fetch_assoc()) {
		$meta_title = $row[meta_title];
		$meta_description = $row[meta_description];
		$meta_keyword = $row[meta_keyword];
	}
}
?>
<title><?php
if ($meta_title == "") {
	echo $_SESSION[content_name] . ' | ' . $_SESSION[content_type] . ' | ' . $_SESSION[all_location];
} else {
	echo $meta_title;
}
?>
</title>
<meta name="description" content="<?php
if ($meta_description == "") {

	echo $_SESSION[content_name] . " | ";
	getFbSingleDescription($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
	echo ". Aaoaz contain all information and public review of " . $_SESSION[content_name] . ". Aaoaz also provide images, contact, address, google map, menu, user comment etc.";
} else {
	echo $meta_description;
}
?>
">
<meta name="keywords" content="<?php
if ($meta_keyword == "") {
	echo "review, rating, contact, best, popular, product, item, address, location, map,  information, " . $_SESSION[content_name] . ', ' . $_SESSION[all_location] . ', ' . $_SESSION[content_type] . ', ' . $_SESSION[cata] . ', ' . $_SESSION[sub_cata] . ", ";

	if ($_SESSION[cata] == "food") {
		echo " menu, restaurant, cafe, cart, sweets, cake_pastry, biriyani, pizza, burger, pasta, buffet, chainees, thai, maxican, dhanmondi, gulshan, uttara, mirpur, badda, rampura, farmgate";
	} else if ($_SESSION[cata] == "life style") {
		echo " fashion house, gym, parlor, salon, gym, fitness, accessorice etc from dhanmondi, gulshan, uttara, mirpur, badda, rampura, farmgate the entire dhaka even from sylhet or cox's bazar";
	} else if ($_SESSION[cata] == "education") {
		echo " university, college, coaching, result, exam, admission, dhanmondi, gulshan, uttara, mirpur, badda, rampura, farmgate, dhaka, sylhet, cox's bazar";
	} else if ($_SESSION[cata] == "health") {
		echo " hospital, clinic, doctors, ambulance, dhanmondi, gulshan, uttara, mirpur, badda, rampura, farmgate, dhaka, sylhet, cox's bazar";
	} else if ($_SESSION[cata] == "residence") {
		echo "  best holet, room, resort, dhanmondi, gulshan, uttara, mirpur, badda, rampura, farmgate, dhaka, sylhet, cox's bazar";
	} else if ($_SESSION[cata] == "tourism") {
		echo " natural site, heritage, lake, park, picnic spot, dhanmondi, gulshan, uttara, mirpur, badda, rampura, farmgate, dhaka, sylhet, cox's bazar";
	} else if ($_SESSION[cata] == "ecommerce") {
		echo " health, beauty, fashion, food, grovery, electronics, gadegt, gift, buy, sell, job, ecommerce, dhanmondi, gulshan, uttara, mirpur, badda, rampura, farmgate, dhaka, sylhet, cox's bazar";
	}

} else {
	echo $meta_keyword;
}
?>
">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
$f_sub_cata = explode(" ", $_SESSION[sub_cata]);
$tmpSubCata = str_replace(",", "", $f_sub_cata[0]);

?>

<?php

$fetch_image_data = "SELECT image_link FROM `general_image` WHERE cata_id = '$_SESSION[content_id]'  LIMIT 1";

$data = $conn->query($fetch_image_data);
if ($data->num_rows > 0) {
	while ($row = $data->fetch_assoc()) {

		$image_link = $row[image_link];
		echo '<meta property="og:image" content="http://aaoaz.com/image/main_image/' . $image_link . '"/>';

	}
}

?>


<meta property="og:url"           content="http://www.aaoaz.com/<?php echo $tmpSubCata . "/" . $_GET[content]; ?>" />
<meta property="og:type"          content="Website" />
<meta property="og:title"         content="<?php
if ($meta_title == "") {
	echo $_SESSION[content_name] . ' | ' . $_SESSION[content_type] . ' | ' . $_SESSION[all_location];
} else {
	echo $meta_title;
}
?>" />
<meta property="og:description"   content="<?php
if ($meta_description == "") {

	echo $_SESSION[content_name] . " | ";
	getFbSingleDescription($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
	echo ". Aaoaz contain all information and public review of " . $_SESSION[content_name] . ". Aaoaz also provide images, contact, address, google map, menu, user comment etc.";
} else {
	echo $meta_description;
}
?>" />
<meta property="fb:app_id" 		  content="1819956078267410" />


<link rel="canonical" href="http://www.aaoaz.com<?php echo $_SERVER["REQUEST_URI"]; ?>">



<?php

$sql = "SELECT `sub_cata`, `cata`, `image_link`, `total_avg_review`, `review_people` FROM `general_info` WHERE `id` = '$_SESSION[content_id]'";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {

		if ($row[review_people] == 0) {
			$curRating = 0;
		} else {
			$curRating = $row[total_avg_review] / $row[review_people];
		}

		if ($row[cata] == "food") {
			echo '<script type=\'application/ld+json\'>
							{
								"@context": "http://schema.org/",
								"@type": "Restaurant",
							    "name": "' . $_SESSION[content_name] . '",
							    "image" : "http://aaoaz.com/image/title_image/' . $row[image_link] . '",
								"aggregateRating": {
									"@type": "AggregateRating",
									"ratingCount": "' . $row[review_people] . '",
									"bestRating": "5",
									"ratingValue": "' . $curRating . '",
									"worstRating": "0.5"
								}
							}
							</script>';
		} else if ($row[cata] == "life style") {
			echo '<script type=\'application/ld+json\'>
							{
								"@context": "http://schema.org/",
								"@type": "Localbusiness",
							    "name": "' . $_SESSION[content_name] . '",
							    "image" : "http://aaoaz.com/image/title_image/' . $row[image_link] . '",
								"aggregateRating": {
									"@type": "AggregateRating",
									"ratingCount": "' . $row[review_people] . '",
									"bestRating": "5",
									"ratingValue": "' . $curRating . '",
									"worstRating": "0.5"
								}
							}
							</script>';
		} else if ($row[cata] == "education") {
			echo '<script type=\'application/ld+json\'>
							{
								"@context": "http://schema.org/",
								"@type": "EducationalOrganization",
							    "name": "' . $_SESSION[content_name] . '",
							    "image" : "http://aaoaz.com/image/title_image/' . $row[image_link] . '",
								"aggregateRating": {
									"@type": "AggregateRating",
									"ratingCount": "' . $row[review_people] . '",
									"bestRating": "5",
									"ratingValue": "' . $curRating . '",
									"worstRating": "0.5"
								}
							}
							</script>';
		} else if ($row[cata] == "health") {
			echo '<script type=\'application/ld+json\'>
							{
								"@context": "http://schema.org/",
								"@type": "Hospital",
							    "name": "' . $_SESSION[content_name] . '",
							    "image" : "http://aaoaz.com/image/title_image/' . $row[image_link] . '",
								"aggregateRating": {
									"@type": "AggregateRating",
									"ratingCount": "' . $row[review_people] . '",
									"bestRating": "5",
									"ratingValue": "' . $curRating . '",
									"worstRating": "0.5"
								}
							}
							</script>';
		} else if ($row[cata] == "residence") {
			echo '<script type=\'application/ld+json\'>
							{
								"@context": "http://schema.org/",
								"@type": "Hotel",
							    "name": "' . $_SESSION[content_name] . '",
							    "image" : "http://aaoaz.com/image/title_image/' . $row[image_link] . '",
								"aggregateRating": {
									"@type": "AggregateRating",
									"ratingCount": "' . $row[review_people] . '",
									"bestRating": "5",
									"ratingValue": "' . $curRating . '",
									"worstRating": "0.5"
								}
							}
							</script>';
		} else if ($row[cata] == "tourism") {
			echo '<script type=\'application/ld+json\'>
							{
								"@context": "http://schema.org/",
								"@type": "TouristAttraction",
							    "name": "' . $_SESSION[content_name] . '",
							    "image" : "http://aaoaz.com/image/title_image/' . $row[image_link] . '",
								"aggregateRating": {
									"@type": "AggregateRating",
									"ratingCount": "' . $row[review_people] . '",
									"bestRating": "5",
									"ratingValue": "' . $curRating . '",
									"worstRating": "0.5"
								}
							}
							</script>';
		} else if ($row[cata] == "") {
			echo '<script type=\'application/ld+json\'>
							{
								"@context": "http://schema.org/",
								"@type": "ShoppingCenter",
							    "name": "' . $_SESSION[content_name] . '",
							    "image" : "http://aaoaz.com/image/title_image/' . $row[image_link] . '",
								"aggregateRating": {
									"@type": "AggregateRating",
									"ratingCount": "' . $row[review_people] . '",
									"bestRating": "5",
									"ratingValue": "' . $curRating . '",
									"worstRating": "0.5"
								}
							}
							</script>';
		}
	}
}
?>