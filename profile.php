
<!DOCTYPE html>
<head>
	<?php
session_start();
error_reporting(0);
if (!isset($_SESSION[userid])) {
	header('Location: index.php');
}
?>
 <meta charset="utf-8">



<title>Profile</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
include 'top_link_list.php';
?>



<link href="https://fonts.googleapis.com/css?family=Josefin+Slab|Arsenal|Quicksand|Italianno|Courgette" rel="stylesheet">

</head>

<body onload="userData('points', <?php echo '\'' . $_SESSION[userid] . '\'' ?>, '0')">
<div class="page_loader"></div>


<?php
include 'operation/profile_php_code.php';
$profileData_obj = new profileData();
?>

<div class="image_change">
	<div class="image_change_panel">
		<img class="imgChangeCancle" onclick="cancleImageUPload()" src="./image/default_image/NO.png">
		<div class="profile_image_frame"><iframe src="admin/upload_crop/upload_crop.php?type=profile_image"></iframe></div>
	</div>
</div>


<div class = "container">

		<div class="profile_overlay">
			<div class="points_status">
				<div class="close_btn">
					<img onclick="closePointsStatus()" src="image/default_image/close.png">
				</div>

				<div id="status_content"><span></span></div>
			</div>
		</div>

<div class = "main-top user_profile">
	<div class="main">


			<?php
include 'header.php';
// include 'navigation_bar.php';
?>


			<div class = "content">
			<div class="container-fluid">
					<div class="profile_panel">
						<div class="total_profile col-sm-12">
								<div class="profile_left col-sm-12">
								 		<?php
$profileData_obj->getUserInfo($_SESSION[userid]);
?>
								</div>
								<!-- <div class="profile_right"> -->
								<div class="col-sm-12 profile_right">
								 	<nav class="navbar navbar-default">
									  <div class="container-fluid user_option_nav">
									    <ul class="nav navbar-nav">
									        <li class="active" ><a data-toggle="tab" href="#points" onclick="userData('points', <?php echo '\'' . $_SESSION[userid] . '\'' ?>, '0')">POINTS</a></li>
									 	  	<li class="package_name"><a data-toggle="tab" href="#requested_package" onclick="userData('requested_package', <?php echo '\'' . $_SESSION[userid] . '\'' ?>, '0')"><span>REQUESTED PACKAGE</span></a></li>
										    <li class=""><a data-toggle="tab" href="#review" onclick="userData('review', <?php echo '\'' . $_SESSION[userid] . '\'' ?>, '0')">REVIEW</a></li>
										    <li class="" ><a data-toggle="tab" href="#comment" onclick="userData('comment', <?php echo '\'' . $_SESSION[userid] . '\'' ?>, '0')">COMMENT</a></li>
										    <li class="" ><a data-toggle="tab" href="#bookmark" onclick="userData('bookmark', <?php echo '\'' . $_SESSION[userid] . '\'' ?>, '0')">BOOKMARK</a></li>
									    </ul>
									  </div>
									</nav>
								    <div class="tab-content">
											<div id="points" class="tab-pane fade  in active"></div>

										    <div id="requested_package" class="tab-pane fade"></div>

									   		<div id="review" class="tab-pane fade"></div>


										    <div id="bookmark" class="tab-pane fade "></div>

										    <div id="comment" class="tab-pane fade">
										    	<h3>Comment</h3>
										    </div>
								    </div>
								</div>
						</div>
				    </div>
			</div>
			</div>

	</div>
</div>
</div>
<!-- END OF container -->

<?php
include 'footer.php';
include 'bottom_link_list.php';
?>
</body>
</html>