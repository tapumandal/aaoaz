<?php

error_reporting(0);
session_start();

$token = "";
if (isset($_POST['token'])) {
	$token = $_POST['token'];
} else if (isset($_GET['token'])) {
	$token = $_POST['token'];
}

$tokenTime = time() - $_SESSION['token_time'];

if ($token == $_SESSION['token'] && $tokenTime < 500) {
} else {
	$_SESSION[access_failed] = '
					<div class="access_failed">
						<span>There is something wrong in your request or time out.
						 Please try again.</span>
					</div>
				';
	echo '<script> window.location.replace("./login"); </script>';
	exit();
}

include 'data_connection.php';
include 'operation/cookie.php';

if (!empty($_POST[email]) && !empty($_POST[pass])) {

	$email = stringFilter($_POST[email]);
	$pass = stringFilter($_POST[pass]);
	$encryptPass = encryptPassword($pass);

	if ($stmt = $conn->prepare("SELECT id FROM `user` WHERE email=? AND pass=? ")) {
		$stmt->bind_param("ss", $email, $encryptPass);
		$stmt->execute();
		$result = $stmt->get_result();

		if ($result->num_rows === 0) {
			$_SESSION[access_failed] = '
						<div class="access_failed">
							<span>Your email address or password is incorrect</span>
						</div>
					';
			echo '<script> window.location.replace("./login"); </script>';
		}
		while ($row = $result->fetch_assoc()) {

			if (isset($_SESSION[userid])) {
				//unset($_SESSION[userid]);
			} else {
				$id = $row[id];
				$_SESSION[userid] = $id;

				setUserCookie($_SESSION[userid]);
			}

			$user_acc = "INSERT INTO `user_access_time`(`user_id`, `login date`, `login time`) VALUES ('$_SESSION[userid]', now(),now())";

			if ($conn->query($user_acc)) {
				echo "Logging in...";
				$_SESSION[loginStatTime] = time();
			} else {
				echo "user access time problem";
			}
		}
	} else {
		$_SESSION[access_failed] = '
					<div class="access_failed">
						<span>Your email address or password is incorrect</span>
					</div>
				';
		echo '<script> window.location.replace("./login"); </script>';
	}

	// $login = "SELECT id FROM `user` WHERE email='$email' AND pass='$encryptPass';";
	// $data = $conn->query($login);

	// if ($data->num_rows > 0) {
	// 	while ($row = $data->fetch_assoc()) {

	// 		if (isset($_SESSION[userid])) {
	// 			//unset($_SESSION[userid]);
	// 		} else {
	// 			$id = $row[id];
	// 			$_SESSION[userid] = $id;

	// 			setUserCookie($_SESSION[userid]);
	// 		}

	// 		$user_acc = "INSERT INTO `user_access_time`(`user_id`, `login date`, `login time`) VALUES ('$_SESSION[userid]', now(),now())";

	// 		if ($conn->query($user_acc)) {
	// 			echo "Logging in...";
	// 			$_SESSION[loginStatTime] = time();
	// 		} else {
	// 			echo "user access time problem";
	// 		}
	// 	}
	// } else {
	// 	$_SESSION[access_failed] = '
	// 				<div class="access_failed">
	// 					<span>Your email address or password is incorrect</span>
	// 				</div>
	// 			';
	// 	echo '<script> window.location.replace("./login"); </script>';
	// }

	echo '<script>history.back();</script>';
	exit();

} else {
	$_SESSION[access_failed] = '
					<div class="access_failed">
						<span>Your email address or password is incorrect</span>
					</div>
				';
	echo '<script> window.location.replace("./login"); </script>';
	exit();
}

function encryptPassword($string) {
	$enPass = md5($string);
	$enTmp1 = "";
	$enTmp2 = "";
	$enTmp3 = "";
	$enTmp4 = "";
	$enTmp5 = "";

	$i = 0;
	for ($i = 0; $i < strlen($enPass); $i++) {
		if ($i < 4) {
			$enTmp1 = $enTmp1 . $enPass[$i];
		} else if ($i < 8) {
			$enTmp2 = $enTmp2 . $enPass[$i];
		} else if ($i < 12) {
			$enTmp3 = $enTmp3 . $enPass[$i];
		} else if ($i < 16) {
			$enTmp4 = $enTmp4 . $enPass[$i];
		} else {
			$enTmp5 = $enTmp5 . $enPass[$i];
		}
	}

	$encrypted = $enTmp1 . $enTmp4 . $enTmp2 . $enTmp3 . $enTmp5;

	return $encrypted;
}

function stringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('SELECT', '', $string);
	$string = str_replace('UPDATE', '', $string);
	$string = str_replace('DROP', '', $string);
	$string = str_replace('INSERT', '', $string);
	$string = str_replace('DELETE', '', $string);
	$string = str_replace('FROM', '', $string);
	$string = str_replace('WHERE', '', $string);

	$string = str_replace('select', '', $string);
	$string = str_replace('update', '', $string);
	$string = str_replace('drop', '', $string);
	$string = str_replace('insert', '', $string);
	$string = str_replace('delete', '', $string);
	$string = str_replace('from', '', $string);
	$string = str_replace('where', '', $string);

	return $string;
}

?>