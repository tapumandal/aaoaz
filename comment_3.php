
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function (e) {
	document.getElementById("comment_img").onchange = function() {

 		$('img.comment_camera').css("cssText", "opacity:0;");
 		$('img.comment_img_select').css("cssText", "opacity:1;");
	};

	document.getElementById("comment_img2").onchange = function() {

 		
 		$('img.comment_camera').css("cssText", "opacity:0;");
 		$('img.comment_img_select').css("cssText", "opacity:1;");
    	
	};
});
</script>




	<div class="input-field">
			<?php 
			// Select Rating title
				if($_SESSION[cata] == "food"){
					$rateTitle1 = "TASTE";
					$rateTitle2 = "PRICE";
					$rateTitle3 = "SERVICE";
				}
				else if($_SESSION[cata] == "life style"){
					if($_SESSION[sub_cata] == "fashion" || $_SESSION[sub_cata] == "accessorice"){
						$rateTitle1 = "QUALITY";
						$rateTitle2 = "PRICE";
						$rateTitle3 = "SERVICE";
								
					}
					else if($_SESSION[sub_cata] == "parlor_salon" || $_SESSION[sub_cata] == "gym_fitness"){
						$rateTitle1 = "ENVIRONMENT";
						$rateTitle2 = "EXPENSE";
						$rateTitle3 = "SERVICE";		
					}
				}
				else if($_SESSION[cata] == "education"){
					$rateTitle1 = "FACULTY";
					$rateTitle2 = "EXPENSE";
					$rateTitle3 = "FACILITIE";
				}

				else if($_SESSION[cata] == "health"){
					if($_SESSION[sub_cata]=="hospital_clinic"){
						$rateTitle1 = "DOCTOR";
						$rateTitle2 = "EXPENSE";
						$rateTitle3 = "FACILITIE";
					}
				}
				else if($_SESSION[cata] == "education"){
					if($_SESSION[sub_cata]=="university" || $_SESSION[sub_cata]=="college" || $_SESSION[sub_cata]=="coaching"){
						$rateTitle1 = "FACULTY";
						$rateTitle2 = "EXPENSE";
						$rateTitle3 = "FACILITIES";
					}
				}
				else if($_SESSION[cata] == "electronics"){
						$rateTitle1 = "DESIGN";
						$rateTitle2 = "PERFORMANCE";
						$rateTitle3 = "BATTERY LIFE";
				}
				else if($_SESSION[cata] == "residence"){
					if($_SESSION[sub_cata] != "to-let"){
						$rateTitle1 = "FACILITIE";
						$rateTitle2 = "EXPENSE";
						$rateTitle3 = "SERVICE";
					}
				}
				else if($_SESSION[cata] == "ecommerce"){
					if($_SESSION[sub_cata] != "buy_sell" || $_SESSION[sub_cata] != "others" || $_SESSION[sub_cata] != "job"){
						
						$rateTitle1 = "RESPONSE";
						$rateTitle2 = "PRICE";
						$rateTitle3 = "SERVICE";
						
						
					}
					else{
						$rateTitle1 = "RESPONSE";
						$rateTitle2 = "PRICE";
						$rateTitle3 = "SERVICE";
					}
				}

			?>
			<form id="uploadForm" action="code_php.php" method="post">
			
			<div class="comment_op_tab">
				<ul>
					<li class="active"><a class="review_bt" href="#0" data-toggle="tab" onclick="show3option('review')"   >REVIEW</a></li>
					<li ><a class="comment_bt" href="#0" data-toggle="tab" onclick="show3option('comment')"   >COMMENT</a></li>
					<!-- <li><a href="#0" data-toggle="tab" onclick="show3option('question')" >QUESTION</a></li> -->
				</ul>
			</div>





			<div id="comment" class="col-sm-12" style="display:none">
				<div class="comment_panel">

				

					
						<div class="comment_panel_input">
							<textarea rows="3" class="txt form-control"  type="text" name="content"  <?php if(!isset($_SESSION[userid])){ ?> onclick="comment_input_click()" <?php } ?> onkeyup="com_in(this.value)" placeholder="Enter your Openion"></textarea>
							
							<div class="image_up_btn">
								<img class="comment_img_select" src="image/default_image/comment_img_select.png">
								<img class="comment_camera" src="image/default_image/comment_camera.png">
								<input id="comment_img" class="img_btn" type="file" name="comment_image[]" multiple>
							</div>

							<div class="comment_panel_sub">
								<a class="review_but com_submit" href="#0" onclick="post_com('comment', '<?php echo $_SESSION[cata]; ?>' , '<?php echo $_SESSION[sub_cata]; ?>' , '<?php echo $_SESSION[content_id]; ?>', '<?php echo $_SESSION[userid]; ?>')"><input type="submit" value="Submit" class="btnUpload" /></a> 
							</div>
						</div>
					
						
				</div>
			</div>




			<div id="review" class="col-sm-12" style="display:block">
				<div class="review_inside">

					<div class="comment_review_input">
							<div class="review_left_option">

								<div class="selectandimg">
									<select class="product_id form-control" id="ratingProID"
											<?php if(!isset($_SESSION[userid])){ echo 'onclick="comment_input_click()" ">';  }
											 else { echo 'onchange="rate_it(this.value, \'id\')">';}
											 ?> 
											 <option style="font-size: 12px;" selected disabled>Select Item</option>
										 	<?php
												$general_obj->item_list_btn($_SESSION[content_id]);
											?>
								    </select>
								    <div class="image_up_btn" id="dxImgBut">
										<img class="comment_img_select" src="image/default_image/comment_img_select.png">
										<img class="comment_camera" src="image/default_image/comment_camera.png">
										<input id="comment_img2" class="img_btn" type="file" name="comment_image[]" multiple>
									</div>
								</div>
							
								<div class="rate_box">
									<div class="comment_user_review ">
										<div class="comment_review_quality">
											<div class="comment_review_title">
												<?php echo $rateTitle1; ?>
											</div>
											<div>
													<ul class="star-rating star_move">
													  <li class="current-rating" id="current-rating_1" style="width:0%"><!-- will show current rating --></li>
													  <span id="ratelinks_1" >
													  <li><a href="javascript:void(0)" title="1 star out of 5" onclick="review_in(1,1)" class="one-star">1</a></li>
													  <li><a href="javascript:void(0)" title="2 stars out of 5" onclick="review_in(2,1)" class="two-stars">2</a></li>
													  <li><a href="javascript:void(0)" title="3 stars out of 5" onclick="review_in(3,1)" class="three-stars">3</a></li>
													  <li><a href="javascript:void(0)" title="4 stars out of 5" onclick="review_in(4,1)" class="four-stars">4</a></li>
													  <li><a href="javascript:void(0)" title="5 stars out of 5" onclick="review_in(5,1)" class="five-stars">5</a></li>
													  </span>
													</ul>
											</div>
										</div>

										<div class="comment_review_quality">
											<div class="comment_review_title">
												<?php echo $rateTitle2; ?>
											</div>
											<div>
													<ul class="star-rating star_move">
													  <li class="current-rating" id="current-rating_2" style="width:'.$rate.'%"><!-- will show current rating --></li>
													  <span id="ratelinks_2">
													  <li><a href="javascript:void(0)" title="1 star out of 5" onclick="review_in(1,2)" class="one-star">1</a></li>
													  <li><a href="javascript:void(0)" title="2 stars out of 5" onclick="review_in(2,2)" class="two-stars">2</a></li>
													  <li><a href="javascript:void(0)" title="3 stars out of 5" onclick="review_in(3,2)" class="three-stars">3</a></li>
													  <li><a href="javascript:void(0)" title="4 stars out of 5" onclick="review_in(4,2)" class="four-stars">4</a></li>
													  <li><a href="javascript:void(0)" title="5 stars out of 5" onclick="review_in(5,2)" class="five-stars">5</a></li>
													  </span>
													</ul>


											</div>
										</div>

										<div class="comment_review_quality">
											<div class="comment_review_title">
												<?php echo $rateTitle3; ?>
											</div>
											<div>
													<ul class="star-rating star_move">
													  <li class="current-rating" id="current-rating_3" style="width:'.$rate.'%"><!-- will show current rating --></li>
													  <span id="ratelinks_3">
													  <li><a href="javascript:void(0)" title="1 star out of 5" onclick="review_in(1,3)" class="one-star">1</a></li>
													  <li><a href="javascript:void(0)" title="2 stars out of 5" onclick="review_in(2,3)" class="two-stars">2</a></li>
													  <li><a href="javascript:void(0)" title="3 stars out of 5" onclick="review_in(3,3)" class="three-stars">3</a></li>
													  <li><a href="javascript:void(0)" title="4 stars out of 5" onclick="review_in(4,3)" class="four-stars">4</a></li>
													  <li><a href="javascript:void(0)" title="5 stars out of 5" onclick="review_in(5,3)" class="five-stars">5</a></li>
													  </span>
													</ul>

											</div>
										</div>
										
									</div>
								</div>
							</div>

							<div class="review_right_option">
								<textarea class="review_txt form-control" type="text" name="content" <?php if(!isset($_SESSION[userid])){ ?> onclick="comment_input_click()" <?php } ?> onkeyup="rate_it(this.value, 'text')" onkeypress="return detectEnter(event, this)"  style="overflow:hidden" placeholder="Enter your Openion"></textarea>
								<div class="rate_submit sub_panel">
										<div class="sub_but_middle">
											<a class="review_but com_submit" href="#0" onclick="post_com('rate', '<?php echo $_SESSION[cata]; ?>' , '<?php echo $_SESSION[sub_cata]; ?>' , '<?php echo $_SESSION[content_id]; ?>', '<?php echo $_SESSION[userid]; ?>')"><input type="submit" value="Submit" class="btnUpload" /></a>
										</div>
								</div>
							</div>
													
								
						<!-- </div> -->
					
					</div>

					
					<!-- <input class="review_but com_submit" type="submit" onclick="submit_rate('rate', '<?php //echo $_SESSION[cata]; ?>' , '<?php //echo $_SESSION[sub_cata]; ?>' , '<?php //echo $_SESSION[content_id]; ?>')" value="POST REVIEW"> -->
					
				</div>
			</div>
		</form>
	</div>
	<div class="rate_warrning_container">
		<div class='rate_warrning'></div>
	</div>