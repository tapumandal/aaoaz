<?php
 
session_start();
//error_reporting();
//ini_set('display_errors', TRUE);
//ini_set('display_startup_errors', TRUE);
//ini_set('memory_limit', '-1');
 
 
// Include the autoloader provided in the SDK
require_once __DIR__ . '/facebook-php-sdk/autoload.php';
 
// Include required libraries
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
 
$appId = '1819956078267410'; //Facebook App ID
$appSecret = '32aeee6b1edd5f648de45e58d9b50f93'; //Facebook App Secret
$redirectURL = 'http://www.aaoaz.com/fblogin.php'; //Callback URL
$fbPermissions = array('email');  //Optional permissions
 
$fb = new Facebook(array(
    'app_id' => $appId,
    'app_secret' => $appSecret,
    'default_graph_version' => 'v2.10',
        ));
 
// Get redirect login helper
$helper = $fb->getRedirectLoginHelper();
 
// Try to get access token
try {
        // Already login
        if (isset($_SESSION['facebook_access_token'])) {
            $accessToken = $_SESSION['facebook_access_token'];
    
        } else {
           $accessToken = $helper->getAccessToken();
        }
     
        if (isset($accessToken)) {
       
            if (isset($_SESSION['facebook_access_token'])) {
                
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            } else {
                // Put short-lived access token in session
                $_SESSION['facebook_access_token'] = (string) $accessToken;
     
                // OAuth 2.0 client handler helps to manage access tokens
                $oAuth2Client = $fb->getOAuth2Client();
     
                // Exchanges a short-lived access token for a long-lived one
                $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
                $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
     
                // Set default access token to be used in script
                $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
            }
     
            // Redirect the user back to the same page if url has "code" parameter in query string
                if (isset($_GET['code'])) {
                
                        // Getting user facebook profile info
                        try {
                                $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email, link,gender,locale,picture');
                                $fbUserProfile = $profileRequest->getGraphNode()->asArray();
                                
                                
    
                                if(isset($fbUserProfile[id])){
                                    processFbData($fbUserProfile);
                                }
                                else{
                                   
                                    echo '<script>history.back();</script>';
                                }
                            
                            
                        } catch (FacebookResponseException $e) {
                   
                            echo 'Graph returned an error: ' . $e->getMessage();
                            session_destroy();
                            // Redirect user back to app login page
                            header("Location: ./");
                            exit;
                        } catch (FacebookSDKException $e) {
                            echo 'Facebook SDK returned an error: ' . $e->getMessage();
                            exit;
                        }
                } else {
                                 $profileRequest = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,picture');
                                $fbUserProfile = $profileRequest->getGraphNode()->asArray();
                                if(isset($fbUserProfile[id])){
                                    processFbData($fbUserProfile);
                                }
                                else{
                                    echo '<script>history.back();</script>';
                                }
                     }
            } else {
            // Get login url
     
            $loginURL = $helper->getLoginUrl($redirectURL, $fbPermissions);
            header("Location: " . $loginURL);
            
         }
    } catch (FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
} catch (FacebookSDKException $e) {
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
 
 
 function processFbData($fbUserData){
    
    //echo "<pre/>";
    //print_r($fbUserData);
    echo "logging in...";
    
    if(isset($fbUserData[email]) && strlen($fbUserData[email])>5){
        $fbUserAccessID =  $fbUserData[email];
    }
    else{
        $fbUserAccessID =  $fbUserData[id];
    }


    
    $proFlag = checkProfile($fbUserAccessID);
    if($proFlag == false){
        createProfile($fbUserData, $fbUserAccessID);
    }
 }

 function checkProfile($email){
    include 'data_connection.php';
    
    $sql = "SELECT * FROM `user` WHERE `email` = '$email' ";
    
    $res = $conn->query($sql);
    
    if($res->num_rows>0){
        while($row = $res->fetch_assoc()){
        
                        if(isset($_SESSION[userid])){
                            //unset($_SESSION[userid]);
                        }else{
                            $id = $row[id];
                            $_SESSION[userid] = $id;    
                        }
                        

                        $user_acc = "INSERT INTO `user_access_time`(`user_id`, `login date`, `login time`) VALUES ('$_SESSION[userid]', now(),now())";

                        if($conn->query($user_acc)){
                            $_SESSION[loginStatTime] = time();
                        }else{
                            echo "user access time problem";
                        }
                        echo '<script>history.back();</script>';
        }
    }
    return false;
 }
 
 
 
 function createProfile($fbUserData, $fbUserAccessID){
        
        include 'data_connection.php';
        $setPass = $fbUserData['id'].$fbUserData['gender'];
        $encryptPass = encryptPassword($setPass);
        
        
        $imgId = $fbUserData['id'];
        
        $fbImgLink = "https://graph.facebook.com/".$imgId."/picture?type=large";
        //echo '<img src="https://graph.facebook.com/'.$imgId.'/picture?type=large">';
        
        $imageLink = insertProImage($fbUserData[name], $fbImgLink);
        
        $sign_up = "INSERT INTO `user`(`name`, `email`, `pass`, `gender`, `type`, `image_link`,  `signup_type`, `fbid`, `status`, `date`, `time`) VALUES ('$fbUserData[name]','$fbUserAccessID','$encryptPass','$fbUserData[gender]','user', '$imageLink', 'facebook', $fbUserData[id], 'enable',now(),now());";
        
        if ($conn->query($sign_up) === TRUE) {
            $proFlag = checkProfile($fbUserAccessID);
        } else {
            // echo "Error: " . $sign_up . "<br>" . $conn->error;
            //echo "Your  account is not opened correctly.<br> Please contact aaoaz.com via email or phone.<br> Thank you__aaoaz.com";
        }
 }
 
 
 function insertProImage($name, $fbImgLink){
    
    date_default_timezone_set('Asia/Dhaka'); // CDT
    $info = getdate();
    
    $hour = $info['hours'];
    $min = $info['minutes'];
    $sec = $info['seconds'];
    
    $name = str_replace(" ", "_", $name);
    $imageName = $name."_".$sec.$hour.$min.".jpg";
    
    $proImgLink = './image/user_image/'.$imageName;
    if(copy($fbImgLink, $proImgLink)){
        return $imageName;
    }else{
        return "";
    }
    return ""; 
 }
 
 function encryptPassword($string){
        $enPass = md5($string);
        $enTmp1 ="";
        $enTmp2 ="";
        $enTmp3 ="";
        $enTmp4 ="";
        $enTmp5 ="";

        $i=0;
        for($i=0; $i<strlen($enPass);  $i++){
            if($i<4){
                $enTmp1 = $enTmp1.$enPass[$i];
            }
            else if($i<8){
                $enTmp2 = $enTmp2.$enPass[$i];
            }
            else if($i<12){
                $enTmp3 = $enTmp3.$enPass[$i];
            }
            else if($i<16){
                $enTmp4 = $enTmp4.$enPass[$i];
            }
            else{
                $enTmp5 = $enTmp5.$enPass[$i];
            }
        }

        $encrypted = $enTmp1.$enTmp4.$enTmp2.$enTmp3.$enTmp5;

        return $encrypted;
    }
    
    
?>