<?php
session_start();
include 'operation/url_operation.php';


function specificStringFilter($string){
	$string = trim($string);
	$string = str_replace('\\','/', $string);
	$string = str_replace('\'',"", $string);
	$string = str_replace('"','', $string);
	$string = str_replace('`',"", $string);
	$string = str_replace(';','.', $string);
	return $string;
}


$getCata = specificStringFilter($_GET[cata]);
$getSub_cata = specificStringFilter($_GET[sub_cata]);
$getContent = specificStringFilter($_GET[content]);

if($getContent != "" && $getCata == "" && $getSub_cata == ""){
	header('Location: ./'.$_SESSION[cata].'.php?content='.$getContent);
		// header('Location: http://www.youtube.com');
}
?>
<!DOCTYPE html>
<head>
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106976768-1');
	</script>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="image/default_image/short_logo.png">
	<title> <?php 

	if(isset($_SESSION[sub_cata]) && $_SESSION[sub_cata] !=""){
		echo ucfirst($_SESSION[sub_cata]);
		if(!isset($_SESSION[locationFilter]) || $_SESSION[locationFilter] == 'ANYWHERE'){
			echo " list of Dhaka | Cox's Bazar | Sylhet even entire Bangladesh.";
		}
		else{
			echo " list of ".$_SESSION[locationFilter];	
		}
	}
	else{
		echo ucfirst($_SESSION[cata])." | aaoaz | ";
		if(!isset($_SESSION[locationFilter]) || $_SESSION[locationFilter] == 'ANYWHERE'){
			echo " Dhaka | Cox's Bazar | Sylhet even entire Bangladesh.";
		}
		else{
			echo $_SESSION[locationFilter];	
		}
	}
	?>
</title>

<meta name="description" content="If you'r looking for<?php 

if(isset($_SESSION[sub_cata])){
	echo " ".$_SESSION[sub_cata];
}
else{
	echo " ".$_SESSION[cata];	

	if($_SESSION[cata] == "health") echo " check up ";
}

if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){
	echo " in ".$_SESSION[locationFilter].".";
}
else if($_SESSION[locationFilter]=="ANYWHERE") {
	echo " in Bangladesh."; 
}
echo ". Aaoaz.com is the right place to take decision.";

if($_SESSION[cata] == "food"){
	echo " Here you will get verified and reliable information and review of best and popular restaurant, cafe, cart, sweets, cake_pastry.";
}
else if($_SESSION[cata] == "life style"){
	echo " The information you will get is authentic and verified by it's reliable user. aaoaz contains information and review of fashion house, gym, parlor, salon, gym, fitness, accessorice etc.";
}
else if($_SESSION[cata] == "education"){
	echo " The information you will get is authentic and verified by it's reliable user. aaoaz contains information and review of university, college, coaching, result, exam, admission etc.";
}
else if($_SESSION[cata] == "health"){
	echo " The information you will get is authentic and verified by it's reliable user. aaoaz contains information and review of hospital, clinic, doctors, ambulance.";	
}
else if($_SESSION[cata] == "residence"){
	echo " aaoaz will help you to choose the best holet, room, resort in dhaka, cox's bazar or sylhet where ever you want to stay.";
}
else if($_SESSION[cata] == "tourism"){
	echo " Before you chose aaoaz.com will help you to decide where you want to spend your vacation.";
}
else if($_SESSION[cata] == "ecommerce"){
	echo " Most of the ecommerce site are not still relible. But In aaoaz people share their personal expericnce about their online shopping and aaoaz team always busy to find which is better for you. So before you order something online check aaoaz to feel safe.";
}

?>
"/>

<meta name="keywords" content="<?php 
if($_SESSION[cata] == "food")
{
	echo "popular, best, restaurant, cafe, cart, sweets, pizza, biriyani, fast-food, burger, deshi, bangla, coffee, kabab, chainees, offer, dhaka, cox's bazar, dhanmondi, gulshan, uttara, mirpur, basundhara, banani, khilgaon, Mohammadpur, panthapath, shylet, motijhil, air port, baily road, bangladesh ";  
}
else if($_SESSION[cata] == "life style")
{
	echo "popular, exclusive, life style, fashion house, seasson, offer, discount, shopping, watch,  shoe, gym, parlor, beauty, dhaka, cox's bazar, dhanmondi, gulshan, uttara, mirpur, basundhara, banani, khilgaon, Mohammadpur, panthapath, shylet, motijhil, air port, baily road, bangladesh";  

}
else if($_SESSION[cata] == "education")
{
	echo "popular, best, education, university, college, coaching, result, addmission, exam, dhaka, cox's bazar, dhanmondi, gulshan, uttara, mirpur, basundhara, banani, khilgaon, Mohammadpur, panthapath, shylet, motijhil, air port, baily road, bangladesh"; 

}
else if($_SESSION[cata] == "health")
{
	echo "popular, care, best, facilated, reliable, health, hospital, ambulance, doctor ,dhaka, cox's bazar, dhanmondi, gulshan, uttara, mirpur, basundhara, banani, khilgaon, Mohammadpur, panthapath, shylet, motijhil, air port, baily road, bangladesh";  

}
else if($_SESSION[cata] == "residence")
{
	echo "cheap, popular, best, sea side,  facilated, reliable, low budget, cheap, hotel, resort, to-let, place, residence, dhaka, cox's bazar, kolatoli, beach, sugondha, dhanmondi, gulshan, uttara, mirpur, basundhara, banani, khilgaon, Mohammadpur, panthapath, shylet, motijhil, air port, baily road, bangladesh"; 

}
else if($_SESSION[cata] == "tourism")
{
	echo "popular, secure, best, entertaining, near by, beautyful, tourism, tourist site, natural site, lake, park, heritage, monument, dhaka, cox's bazar, dhanmondi, gulshan, uttara, mirpur, basundhara, banani, khilgaon, Mohammadpur, panthapath, shylet, motijhil, air port, baily road, bangladesh"; 
}
else if($_SESSION[cata] == "tourism"){
	echo "popular, reliable, fastest, delevery, ecommerce, online, shopping, gift, beauty, health, other, grocery, electronics, gadegt, craft, buy, sell, job, fashion, shoe, dhaka, cox's bazar, dhanmondi, gulshan, uttara, mirpur, basundhara, banani, khilgaon, Mohammadpur, panthapath, shylet, motijhil, air port, baily road, bangladesh";
}
else {
	echo "best popular food, restaurant, cafe, cart, sweets, bekary, pizza, biriyani, kabab, chainees food, mexican food ";  if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){echo " of ".$_SESSION[locationFilter];} 
	else if($_SESSION[locationFilter]=="ANYWHERE") echo " in Bangladesh"; 			echo "popular, exclusive, life style, fashion house, shopping, accessorice, watch, purse, shoe, gym, parlor, beauty ";  if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){echo " of ".$_SESSION[locationFilter];} 
	else if($_SESSION[locationFilter]=="ANYWHERE") echo " in Bangladesh"; 			echo "popular, best, education, university, college, coaching, result, addmission, exam ";  if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){echo " of ".$_SESSION[locationFilter];} 
	else if($_SESSION[locationFilter]=="ANYWHERE") echo " in Bangladesh"; 			echo "popular, facilated, reliable, health, hospital, ambulance, doctor ";   if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){echo " of ".$_SESSION[locationFilter];} 
	else if($_SESSION[locationFilter]=="ANYWHERE") echo " in Bangladesh"; 			echo "new, upcoming, best, electronics, mobile, comnputer, laptop, gadegt, mother board, processor, ram, sound system, DSLR camera ";   if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){echo " of ".$_SESSION[locationFilter];} 
	else if($_SESSION[locationFilter]=="ANYWHERE") echo " in Bangladesh"; 			echo "popular, facilated, reliable, low budget, cheap, hotel, resort, to-let, place, residence ";   if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){echo " of ".$_SESSION[locationFilter];} 
	else if($_SESSION[locationFilter]=="ANYWHERE") echo " in Bangladesh"; 			echo "popular, secure, best, entertaining, near by, beautyful, tourism, tourist site, natural site, lake, park, heritage, monument ";   if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){echo " of ".$_SESSION[locationFilter];} 
	else if($_SESSION[locationFilter]=="ANYWHERE") echo " in Bangladesh"; 
}
?>
"/>
<meta name="author" content="aaoaz.com <?php echo $getCata ?>"/>



<meta property="og:url"           content="http://www.aaoaz.com<?php if($_SESSION[sub_cata]){echo "/".$_SESSION[sub_cata];} else{echo "/".$_SESSION[cata];}   ?>" />
<meta property="og:type"          content="Website" />
<meta property="og:title"         content="<?php 
if(isset($_SESSION[sub_cata]) && $_SESSION[sub_cata] !=""){
	echo ucfirst($_SESSION[sub_cata]);
	if(!isset($_SESSION[locationFilter]) || $_SESSION[locationFilter] == 'ANYWHERE'){
		echo " list of Dhaka | Cox's Bazar | Sylhet even entire Bangladesh.";
	}
	else{
		echo " list of ".$_SESSION[locationFilter];	
	}
}
else{
	echo ucfirst($_SESSION[cata])." | aaoaz | ";
	if(!isset($_SESSION[locationFilter]) || $_SESSION[locationFilter] == 'ANYWHERE'){
		echo " Dhaka | Cox's Bazar | Sylhet even entire Bangladesh.";
	}
	else{
		echo $_SESSION[locationFilter];	
	}
}
?>" />
<meta property="og:description"   content="If you'r looking for<?php 

if(isset($_SESSION[sub_cata])){
	echo " ".$_SESSION[sub_cata];
}
else{
	echo " ".$_SESSION[cata];	
}

if(isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != 'ANYWHERE'){
	echo " in ".$_SESSION[locationFilter].".";
}
else if($_SESSION[locationFilter]=="ANYWHERE") {
	echo " in Bangladesh."; 
}
echo ". Aaoaz.com is the right place.";

if($_SESSION[cata] == "food"){
	echo " Here you will get verified and reliable information and review of best and popular restaurant, cafe, cart, sweets, cake_pastry. To find the best biriyani, pizza, burger, pasta, buffet, chainees, thai, maxican etc take a look at the authentic review of aaoaz.com.";
}

?> "/>

<meta property="og:image"         content="http://www.aaoaz.com/image/default_image/nav/<?php echo $_SESSION[cata]; ?>.jpg" />
<meta property="fb:app_id" 		  content="1819956078267410" />

<link rel="canonical" href="http://www.aaoaz.com/<?php if(!isset($_SESSION[sub_cata]) || $_SESSION[sub_cata] =="")echo $_SESSION[cata]; else echo $_SESSION[sub_cata]; ?>">

<?php include 'top_link_list.php'; ?>
</head> 

<body>
	<?php
		// include 'sideNabBar.php';
		// include 'sideNavBarNew.php';
	?>
	<div class="page_loader"></div>

	<div class = "container">

		<div class = "main-top">
			<div class="main">
				

				<?php 
				include 'header.php'; 
				?>


				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-sm-12" id="">
								<div class="col-sm-12">
									<?php
									if($_SESSION[locationFilter] == "ANYWHERE" || !isset($_SESSION[locationFilter])){

										include 'explore/explore.php';

										if($_SESSION[scrWidth]<767){

										}
										else{
		  								// include 'dynamic_specific.php';
											include 'dxSpecification.php';
										}
									}

									?>
								</div>

								<div class="load_more_panel">
									<div class="random_content_title">
										<h3>Find More</h3>
									</div>
									<div class="col-sm-12 area-demo"  >

										<?php 
										include 'data_connection.php';

										if($_SESSION[locationFilter] == "ANYWHERE" || $_SESSION[locationFilter]==""){
			    			// echo "<h1> ANYWHERE </h1>";
											if($getSub_cata == "")
											{
					    			//echo $getCata;
												$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `type`, `image_link`, `main_location` from general_info where cata= '$getCata' AND `status` = 'approved' ORDER BY RAND() LIMIT 10;";
											}
											else if($getCata)
											{
												$sub_cata_tmp = '%'.$getSub_cata.'%';
												$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `type`, `image_link`, `main_location` from general_info where cata= '$getCata' AND sub_cata LIKE '$sub_cata_tmp' AND `status` = 'approved' ORDER BY RAND() LIMIT 10;";	
											}
											else{
												$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `type`, `image_link`, `main_location` from general_info where cata= '$getCata' AND sub_cata LIKE '$getSub_cata' AND `status` = 'approved' ORDER BY RAND() LIMIT 10;";		
											}
										}

										else {

											if($getSub_cata == "")
											{
			    			//echo $getCata;
												$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata,  general_info.name, general_info.type, general_info.image_link,   map.cata_id, map.location from general_info LEFT JOIN map ON general_info.id = map.cata_id where general_info.cata= '$getCata' AND general_info.status = 'approved' AND  (map.location = '$_SESSION[locationFilter]' OR map.address LIKE '%$_SESSION[locationFilter]%')  GROUP BY general_info.id ORDER BY RAND() LIMIT 10 ;";
											}
											else if($getCata)
											{
												$sub_cata_tmp = '%'.$getSub_cata.'%';
												$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata,  general_info.name, general_info.type, general_info.image_link, general_info.main_location,  map.cata_id, map.location from general_info LEFT JOIN map ON general_info.id = map.cata_id where general_info.cata= '$getCata' AND general_info.status = 'approved' AND  (map.location = '$_SESSION[locationFilter]' OR map.address LIKE '%$_SESSION[locationFilter]%') AND general_info.sub_cata LIKE '$sub_cata_tmp' GROUP BY general_info.id ORDER BY RAND() LIMIT 10 ;";	
											}
											else{
												$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata,  general_info.name, general_info.type, general_info.image_link, general_info.main_location,  map.cata_id, map.location from general_info LEFT JOIN map ON general_info.id = map.cata_id where general_info.cata= '$getCata' AND general_info.status = 'approved'  AND (map.location = '$_SESSION[locationFilter]' OR map.address LIKE '%$_SESSION[locationFilter]%') AND general_info.sub_cata LIKE '$getSub_cata' GROUP BY general_info.id ORDER BY RAND() LIMIT 10 ;";		
											}
										}




										$data = $conn->query($sql);

										unset($_SESSION[loadedContentId]);
										$_SESSION[loadedContentId] = array('');

										
										
										if($data->num_rows>0)
										{
											$i=1;

											while($row = $data-> fetch_assoc())
											{
												$id = $row['id'];
												$name = $row['name'];
												$link_name = $row[name];
												$link_name = str_replace(" ", "-", $link_name);
												$rating = $row['rating'];
												$location =  $row['location'];
												$catagory =  $row['cata'];
												
												// $image = strpos($row['image_link'], ".jpg");

												// if(strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false)
												// {
												// 	$image   = "1_1no_title_image.jpg";
												// }
												// else if($row['image_link'] == "")
												// {
												// 	$image   = "1_1no_title_image.jpg";
												// }

												// else {
												// 	$image   = $row['image_link'];
												// }

												// Blank image remove
												$image = strpos($row['image_link'], ".jpg");
												if(strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false)
												{
														$image   = "1_1no_title_image.jpg";
												}
												else if(empty($row['image_link']))
												{
														$image   = "1_1no_title_image.jpg";
												}
												else {
													$image   = $row['image_link'];
												}
												// Blank image remove



												$flagType=0;

												if(strlen($row[type])>25)$type = $type."...";


												$subcata = strtolower($row['sub_cata']);


												array_push($_SESSION[loadedContentId], $row[id]);


												$link = strtolower($catagory);

												$subcata_url = explode(" ", $subcata);
												$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

												if (!file_exists("image/title_image/".$image."")) {   
													$image = "1_1no_title_image.jpg";
												}



												if($_SESSION[scrWidth]>500){
													$nameLen = strlen ( $row[name] );
													$setpadding="";
													if($nameLen>27){
														$setpadding = 'style="padding-top:6px;"';
													}
													$general_rate_obj = new general();
													

													// GET LOCATION
														$fetch_map = "SELECT * FROM `map`  WHERE cata_id = '$id' AND cata= '$catagory';";
														$data2= $conn->query($fetch_map);
														$location= "";
														if($data2->num_rows>0){
															
															while ($row3=$data2->fetch_assoc()) {
																
																if(!empty($row3[location]))
																	{	
																		$location = $location.$row3[location]." | ";
																	}

															}
														}
														$location = $location." ";
														$location = str_replace(" |  ", "", $location);
													// GET LOCATION



													if(strlen($location) >25){
														$location = substr($location, 0, 35);		
														$location .= "...";
													}

													$type = substr($row[type], 0, 25);
													
													echo '
														<div class="col-sm-3 area" >
															<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
																<div class="dx_box_img">
											                  		<img class="img-responsive" src="image/title_image/'.$image.'" alt="image">
											                  	</div>
											                </a>
										                  	<div class="dx_box_info">
											                	<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
											                		<h3 '.$setpadding.'> '.$row[name].'</h3>
											                	</a>
											                  	'.$general_rate_obj->ratingSingle($total_rate).'
											                	<span>'.$row[type].'</span>
											                	<br>
											                	<span>'.$location.'</span>
											              	</div>
											             
										                </div>
										                ';
												}else{

													$type = substr($row[type], 0, 60);

													$fetch_map2 = "SELECT * FROM `map`  WHERE `cata_id` = '$row[id]';";
													$data2= $conn->query($fetch_map2);
													$location2= "";
													if($data2->num_rows>0){

														while ($rowMap=$data2->fetch_assoc()) {

															if(!empty($rowMap[location]))
															{	
																$location2 = $location2.$rowMap[location]." | ";
															}

														}
													}
													$location2 = $location2." ";
													$location2 = str_replace(" |  ", "", $location2);

													$location = substr($location2, 0, 40);

													if(strlen($location2) > 40){
														$location = $location."...";
														$location = str_replace(" |.", ".", $location);
														$location = str_replace(" | .", "	.", $location);
													}

													if(strlen($name)>22){
														$name = substr($name, 0, 20);
														$name = $name."..";
													}


													if (!file_exists("image/title_image/s_".$image."")) {   
														$image = "1_1no_title_image.jpg";
													}





													echo '
													<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
													<div class="content_list">
													<div class="content_list_image">
													<img class="img-responsive" src="image/title_image/s_'.$image.'" class="img-responsive img-circle"	 alt="IMAGE"></img>
													</div>

													<div class="content_list_info">
													<div class="content_list_name"> <h2>'.$name.'</h2> </div>
													<div class="content_list_type"> <span>'.$type.'</span> </div>
													<div class="content_list_location"> <span>'.$location.'</span> </div>
													</div>
													</div>
													</a>
													';
												}

												if($window_width>1549){
													if($i%5 == 0) echo '<div class="clearfix"> </div>';

												}
												else if($window_width<1549 && $window_width>1049){
													if($i%4 == 0) echo '<div class="clearfix"> </div>';

												}
												else if($window_width<1050 && $window_width>759){
													if($i%3 == 0) echo '<div class="clearfix"> </div>';
												}
												 			//else if($window_width<760 && $window_width>349){
												else if($window_width<760){
													if($i%2 == 0) echo '<div class="clearfix"> </div>';
												}
												else if($window_width<350){
												 				//if($i%1 == 0) echo '<div class="clearfix"> </div>';
												}
												$i = $i+1;

											}

										}

												// $_SESSION[loadedContentId] = $loadedContentId;


										?>

										<!-- <div class="clearfix"> </div> -->
										<div class="lode_more_area">
										</div>
										<div class="load_button col-sm-12">
											<div class="load_button_inside">
												<button onclick="loadMore('specific', <?php echo "'".$getCata."','".$getSub_cata."'"; ?>)">Load more</button>
											</div>
										</div>	

									</div>

									<!-- <div class="col-sm-3 side_bar">
										<div class="side_bar_top">
											<div class="top_side_bar_head">
												<h2>You May Need</h2>	
											</div>
											<div class="top_side_bar_panel">

												<?php 
												// include 'operation/suggestion.php';
												// include 'data_connection.php';
												// $obj = new suggestion();
												// $obj->outOfCourse($getCata, $getSub_cata);
												?>

											</div>
										</div>
									</div> -->
								</div>
							</div>


						</div>
					</div>
				</div>
			</div> <!-- search_result -->			
		</div>
	</div>

</div>
<!-- END OF container -->
<?php
include 'footer.php';
include 'bottom_link_list.php';
?>

</body>
</html>











