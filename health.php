<?php
session_start();
include 'operation/url_operation.php';
?>
<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:fb="http://ogp.me/ns/fb#">

<head>	
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106976768-1');
	</script>

	<?php
	include 'header_tags.php';
	include 'top_link_list.php'; 
	?>

	<script>
		$(document).ready(function(){


			$(".aurthopedix_but").click(function(){
				$('.edu-info-box').not('.aurthopedix').hide(800);
				$('.aurthopedix').show(800);    
			});

			$(".nurologist_but").click(function(){
				$('.edu-info-box').not('.nurologist').hide(800);
				$('.nurologist').show(800);    
			});

			$(".general_surgery_but").click(function(){
				$('.edu-info-box').not('.general_surgery').hide(800);
				$('.general_surgery').show(800);    
			});

			var height = $('#info_height').height();        //Get the height of the browser window
			height = height;
			if (height>400) {
		    	$('.edu_item_name_list').height(height-15);  //Resize the pageContent div, with a size of 60 - page height.
		    }
		    else{
		    	$('.edu_item_name_list').height(400-15);  //Resize the pageContent div, with a size of 60 - page height.
		    }
		    
		    $("#comment_option").val($("#comment_option option:first").val()); //auto select the first option in select tag in comment section
		});

	</script>
</head>

<body>
	<?php
		// include 'sideNabBar.php';
	?>
	<div class="page_loader"></div>
<div class = "container details_page">

		<div class = "main-top">
			<div class="main">
				<?php

				include 'header.php';

				?>
			</div>
		</div>

	<div class="main_content_center">
		<div class="col-sm-12 field ">
			<div class=" wide_pattern">  <!-- Whole information without add -->



				<div class="col-sm-12 first">    <!-- FOR NAME -->
					<div class="info_box">
					<!-- <div class="title">
						<span>Name</span>
					</div> -->
					<div class="title_info">
						<span>
							<?php
							$general_obj = new general();
							$general_obj->general_name($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
							?>
						</span>
					</div>
				</div>
			</div><!-- FOR NAME -->


			
			<!-- Item List LIST -->
			<!-- <div class="col-sm-12 doc-list" >
					<iframe  src="item-list/yellow_custoom_list.php"></iframe>
				</div> -->
				<!-- Item LIST -->

				


				<div class="col-sm-8 first" ID="info_height">  <!-- NORMAL INFORMATION  -->

					<!-- PHOTO GALLARY -->
				<div class="col-sm-12 second" >   
					<div>
						<?php 
						include 'slide-show.php';
						?>
					</div>
				</div>			
				<!-- PHOTO GALLARY -->

					<?php
					$general_obj = new general();
					$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
					$general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
					$general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
					?>

				</div>   <!-- NORMAL INFORMATION  -->

				<!-- DOCTORS LIST -->
				<?php
				$general_obj = new general();

				if($_SESSION[sub_cata] == "hospital_clinic" && $_SESSION[cata] == "health")
				{
					$faculty_menu = '<div class="dept_list">
					<ul>
					<li>
					<div class="title_doc_tea">
					<span>DOCTORS</span>
					</div>

					<div class="edu_list_button">
					<ul class="nav navbar-nav edu_button_option_list">
					';
					$faculty_menu2=$general_obj->getFilterList($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
												                          // <li><a href="#0" class="education_filter cse_but">CSE</a></li>
												                          // <li><a href="#0" class="education_filter civil_but">CIVIL</a></li>
												                          // <li><a href="#0" class="education_filter archi_but">ARCHI</a></li>
												                          // <li><a href="#0" class="education_filter bba_but">BBA</a></li>

					$faculty_menu3  =                '</ul>
					</div>
					</li>
					</ul>
					</div>';

					$faculty_menu = $faculty_menu." ".$faculty_menu2." ".$faculty_menu3;
				}

				$list_top = '<div class="edu_item_name_list " >
				<div class="list">';
				$list_bottom = ' </div>
				</div>';


				$general_obj->doctor_faculty_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], $list_top, $list_bottom, $faculty_menu);
				?>




	   	

										<!-- <div class="clearfix"></div> -->

										<div class="clearfix"></div>

										<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->

											<?php
											include 'comment_content.php';
											?>


											<div class="com">
												<?php
												$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
												?>
											</div>

										</div>   <!-- COMMENT  -->

										<div style="height:200px">

										</div>


									</div> <!-- Whole information without add -->

									<!-- <div class="col-sm-2 third"> 
										<div class="inside_top_right_panel">
											<div class="inside_top_head">
												<h3>Recomended</h3>
											</div>
											<div class="insider_box_panel">

												<?php
												// include 'operation/suggestion.php';
												// $obj = new suggestion();
												// $obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
												?>

											</div>
										</div>
									</div> -->
								</div>

	</div>								
</div>
							<!-- END OF container -->

							<?php
							include 'footer.php';
							include 'bottom_link_list.php';
							?>

</body>
</html>