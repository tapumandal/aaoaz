<?php
// error_reporting(0);
session_start();
date_default_timezone_set('Asia/Dhaka');

function codePhpStringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '/', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '.', $string);
	$string = strip_tags($string);
	return $string;
}

function codePhpStringFilter2($string) {
	$string = htmlspecialchars($string);
	$string = strip_tags($string);

	$string = str_replace("'", "\'", $string);
	$string = str_replace("\\\\\'", "\'", $string);
	$string = str_replace("\\\\\\\'", "\'", $string);
	$string = str_replace("\\\\\\\\\'", "\'", $string);
	$string = str_replace("\\\\\\\\\\\'", "\'", $string);
	return $string;
}

$getSrcWidth = codePhpStringFilter($_GET[scrWidth]);

if ($getSrcWidth != "" && $getSrcWidth > 550) {
	echo "<div id='search_result'>";
	include 'nav_page.php';
	echo "</div>";
} else if ($getSrcWidth != "" && $getSrcWidth < 551) {
	// include 'navigation_bar.php';
	include 'mobile_home.php';
} else {
	echo "";
}

if ($getSrcWidth != "") {
	unset($_SESSION[scrWidth]);
	$_SESSION[scrWidth] = $getSrcWidth;
}

$getActionType = codePhpStringFilter($_GET[actionType]);
$getLocation = codePhpStringFilter($_GET[location]);
$getCata = codePhpStringFilter($_GET[cata]);
$getSub_cata = codePhpStringFilter($_GET[sub_cata]);
$getId = codePhpStringFilter($_GET[id]);
$getProValue = codePhpStringFilter($_GET[proValue]);
$getLowRange = codePhpStringFilter($_GET[lowRange]);
$getHighRange = codePhpStringFilter($_GET[highRange]);

if ($getActionType == "setLocationSession") {
	$_SESSION[locationFilter] = $getLocation;
	echo $_SESSION[locationFilter];
} else if ($getActionType == "elecProFilter") {
	$general_obj = new general();
	$general_obj->general_item_list($getCata, $getSub_cata, $getId, "", "", "", $getProValue);

} else if ($getActionType == "elecPriceProFilter") {

	$general_obj = new general();
	$general_obj->general_item_list($getCata, $getSub_cata, $getId, "", "", "", "", $getLowRange, $getHighRange);

}

$postCom_type = codePhpStringFilter($_POST[com_type]);
$postCom_id = codePhpStringFilter($_POST[com_id]);
$postCata = codePhpStringFilter($_POST[cata]);
$postSub_cata = codePhpStringFilter($_POST[sub_cata]);
$postCom_value = codePhpStringFilter2($_POST[com_value]);
$postCata_id = codePhpStringFilter($_POST[cata_id]);

if ($postCom_type == "getRecentComment") {
	$last_id = $postCom_id;
	$cata = $postCata;
	$sub_cata = $postSub_cata;
	$cata_id = $postCata_id;

	$general_obj = new general();
	$general_obj->fetch_comment($cata, $sub_cata, $cata_id, $last_id);

}

if ($postCom_type == "comment" && !empty($postCom_value) && isset($_SESSION[userid])) {

	echo $postCom_value;
	include "data_connection.php";

	$comment_txt = $postCom_value;
	// $comment_txt = str_replace('#', ' ', $comment_txt);
	// $comment_txt = str_replace(array("'", "\"", "&quot;"), " ", htmlspecialchars($comment_txt));
	// $comment_txt = filter_var($comment_txt, FILTER_SANITIZE_STRING);
	// $comment_txt = filter_var($comment_txt, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
	// $comment_txt = str_replace(str_split('\\/:*`~#$%^_-<>|'), ' ', $comment_txt);

	if ($comment_txt == 'undefined' || $comment_txt == null) {
		$comment_txt = "";
	}

	$com_ins = "INSERT INTO `comment`(`cata_id`, `cata`, `sub_cata`, `user_id`, `user_type`, `text`, `type`, `time`, `date`, `status`)
	VALUES ('$postCata_id', '$postCata', '$postSub_cata', '$_SESSION[userid]', 'user', '$comment_txt', '$postCom_type', now(), now(), 'show')";

	if ($conn->query($com_ins) === TRUE) {

		$last_id = $conn->insert_id;
		insertCommentImg($last_id);

		echo $last_id;

	} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}

}

$postRate_text = codePhpStringFilter2($_POST[rate_text]);
$postRate_1 = codePhpStringFilter($_POST[rate_1]);
$postRate_2 = codePhpStringFilter($_POST[rate_2]);
$postRate_3 = codePhpStringFilter($_POST[rate_3]);
$postTmp_item_id = codePhpStringFilter2($_POST[tmp_item_id]);

if ($postCom_type == "rate" && isset($_SESSION[userid])) {
	include "data_connection.php";

	$rate_txt = $postRate_text;
	// $rate_txt = str_replace('#', ' ', $rate_txt);
	// $rate_txt = str_replace(array("'", "\"", "&quot;"), " ", htmlspecialchars($rate_txt));
	// $rate_txt = filter_var($rate_txt, FILTER_SANITIZE_STRING);
	// $rate_txt = filter_var($rate_txt, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
	// $rate_txt = str_replace(str_split('\\/:*`~#$%^_-<>|'), ' ', $rate_txt);

	if ($rate_txt == 'undefined' || $rate_txt == null) {
		$rate_txt = "";
	}

	$getTime = "SELECT `date`, `time` FROM `comment` WHERE `type`= 'rate' AND `user_id` = '$_SESSION[userid]' AND `cata_id` = '$postCata_id' AND `status`='show' ORDER BY id DESC LIMIT 1";
	$res = $conn->query($getTime);
	if ($res->num_rows > 0) {
		while ($row = $res->fetch_assoc()) {
			// echo "(".$row[date]."----".$row[time].")";
			$date1 = date_create($row[date]);
			$date2 = date_create(date("Y-m-d"));

			$diff = date_diff($date1, $date2);
			$day = $diff->format("%a");
			if ($day < 7) {
				echo "<span>To keep the review effective. <br> Aaoaz don't allow more than one review within 7 days.<span>";
				exit();
			}
		}
	}

	$rate_avg = $postRate_1 + $postRate_2 + $postRate_3;
	$rate_avg = $rate_avg / 3;

	$item_id = $postTmp_item_id;

	$itemAvailable = " SELECT id FROM `item_list` WHERE id = '$item_id'";
	// echo "----".$itemAvailable."-----";
	$itemRateAccess = "";
	$res = $conn->query($itemAvailable);
	if ($res->num_rows > 0) {
		while ($row = $res->fetch_assoc()) {
			$itemRateAccess = "access";
		}
	}

	if ($postCata == "food" && $itemRateAccess == "access" || $postCata == "electronics" && $itemRateAccess == "access") {

		$com_ins = "INSERT INTO `comment`(`cata_id`, `cata`, `sub_cata`, `item_id`, `user_id`,	`user_type`, `text`, `type`,
		`rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`, `status`)
		VALUES ('$postCata_id', '$postCata', '$postSub_cata', '$item_id', '$_SESSION[userid]', 'user', '$rate_txt', '$postCom_type',
		'$postRate_1', '$postRate_2', '$postRate_3', '$rate_avg',  now(), now(), 'show')";

		if ($conn->query($com_ins) === TRUE) {
			$last_comment_id = $conn->insert_id;
		} else {
			echo " SQL Error: " . $sql . "<br>" . $conn->error;
		}

		$total_rate = $postRate_1 + $postRate_2 + $postRate_3;
		if ($postCata == "food") {
			$item_rate = "UPDATE `item_list` SET `total_rate`=total_rate+'$total_rate', `rate_1`=rate_1+'$postRate_1', `rate_2`=rate_2+'$postRate_2',
			`rate_3`= rate_3+'$postRate_3',`rate_people`=rate_people+1 WHERE id = '$item_id';";
		}
		if ($postCata == "electronics") {
			$item_rate = "UPDATE `elec_item_details` SET `total_rate`=total_rate+'$total_rate', `rate_1`=rate_1+'$postRate_1', `rate_2`=rate_2+'$postRate_2',
			`rate_3`= rate_3+'$postRate_3',`rate_people`=rate_people+1 WHERE id = '$item_id';";
		}

		if ($conn->query($item_rate) === TRUE) {

			$total_rate = $postRate_1 + $postRate_2 + $postRate_3;
			$total_avg_rate = $total_rate / 3;
			$item_rate = "UPDATE `general_info` SET `total_avg_review`=total_avg_review+'$total_avg_rate', `rate_1`=rate_1+'$postRate_1', `rate_2`=rate_2+'$postRate_2',
			`rate_3`= rate_3+'$postRate_3',`review_people`=review_people+1 WHERE id = '$postCata_id';";

			if ($conn->query($item_rate) === TRUE) {

				insertCommentImg($last_comment_id);
				echo $last_comment_id;

			} else {
				echo " SQL Error: " . $sql . "<br>" . $conn->error;
			}

			// $general_obj = new general();
			// $general_obj->fetch_comment($postCata, $postSub_cata, $postCata_id, $last_id);

		} else {
			echo " SQL Error: " . $sql . "<br>" . $conn->error;
		}
	} else {
		$com_ins = "INSERT INTO `comment`(`cata_id`, `cata`, `sub_cata`,  `user_id`,	`user_type`, `text`, `type`,
		`rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`, `status`)
		VALUES ('$postCata_id', '$postCata', '$postSub_cata', '$_SESSION[userid]', 'user', '$rate_txt', '$postCom_type',
		'$postRate_1', '$postRate_2', '$postRate_3', '$rate_avg',  now(), now(), 'show')";

		if ($conn->query($com_ins) === TRUE) {

			$last_comment_id = $conn->insert_id;
		} else {
			echo " SQL Error: " . $sql . "<br>" . $conn->error;
		}

		$total_rate = $postRate_1 + $postRate_2 + $postRate_3;
		$total_avg_rate = $total_rate / 3;
		$item_rate = "UPDATE `general_info` SET `total_avg_review`=total_avg_review+'$total_avg_rate', `rate_1`=rate_1+'$postRate_1', `rate_2`=rate_2+'$postRate_2',
		`rate_3`= rate_3+'$postRate_3',`review_people`=review_people+1 WHERE id = '$postCata_id';";

		if ($conn->query($item_rate) === TRUE) {

			insertCommentImg($last_comment_id);
			echo $last_comment_id;
			// $general_obj = new general();
			// $general_obj->fetch_comment($postCata, $postSub_cata, $postCata_id, $last_id);

		} else {
			echo " SQL Error: " . $sql . "<br>" . $conn->error;
		}
	}

}

function insertCommentImg($last_id) {
	include 'data_connection.php';
	$i = 0;
	while ($_SESSION[tmp_img_name][$i] != null) {
		$sourcePath = "image/comment_img/comment_tmp/" . $_SESSION[tmp_img_name][$i];
		$targetPath = "image/comment_img/" . $last_id . "_" . $_SESSION[tmp_img_name][$i];
		// if(move_uploaded_file($sourcePath, $targetPath)) {
		if (copy($sourcePath, $targetPath)) {
			$img_link = $last_id . "_" . $_SESSION[tmp_img_name][$i];
			$imgInsert = "INSERT INTO `comment_image`(`user_id`, `comment_id`, `image_link`) VALUES ('$_SESSION[userid]', '$last_id', '$img_link')";
			if ($conn->query($imgInsert)) {
			} else {
				echo "comment img NOT insert into db";
			}

		}

		$i++;
	}

	unset($_SESSION[tmp_img_name]);

}

if (is_array($_FILES) && count($_FILES['comment_image']['name'])) {

	$_SESSION[tmp_img_name] = array();

	$j = 0;

	foreach ($_FILES['comment_image']['name'] as $name => $value) {
		if ($_FILES['comment_image']['tmp_name'][$name]) {

			$sourcePath = $_FILES['comment_image']['tmp_name'][$name];

			$type = exif_imagetype($_FILES['comment_image']['tmp_name'][$name]);

			if ($type == 1) {
				$tmp_img_name = "us" . $name . "_ui" . $_SESSION[userid] . ".gif";
			} else if ($type == 2) {
				$tmp_img_name = "us" . $name . "_ui" . $_SESSION[userid] . ".jpg";
			} else if ($type == 3) {
				$tmp_img_name = "us" . $name . "_ui" . $_SESSION[userid] . ".png";
			}

			// $tmp_img_name = $name."_".$_SESSION[userid]."_tmp_img".$_FILES['comment_image']['name'][$name];

			$targetPath = "image/comment_img/comment_tmp/" . $tmp_img_name;

			if (move_uploaded_file($sourcePath, $targetPath)) {

				$_SESSION[tmp_img_name][$j] = $tmp_img_name;
				echo "1";
				$j++;
			}
		}
	}

}

function getLifeStyleFilter($cata_id) {
	include 'data_connection.php';

	$sql = "SELECT `filter_1` FROM `item_list` WHERE `cata_id` = '$cata_id' GROUP BY `filter_1`";

	$res = $conn->query($sql);

	$subFilter = "";

	$FilterTop = '<div class="fashion_cata_list"><ul>';
	$menu_list = 1;
	if ($res->num_rows > 0) {
		while ($row = $res->fetch_assoc()) {

			$subFilter = $subFilter . getLifeStyleSubFilter($cata_id, $row[filter_1], $menu_list);
			$menu_list++;
		}
	}

	$FilterBottom = "</ul></div>";

	$LSFilter = $FilterTop . $subFilter . $FilterBottom;

	return $LSFilter;
}

function getLifeStyleSubFilter($cata_id, $filter1, $menu_list) {
	include 'data_connection.php';
	$sql2 = "SELECT `filter_2` FROM `item_list` WHERE `cata_id` = '$cata_id' AND `filter_1` = '$filter1' GROUP BY `filter_2`";

	$res2 = $conn->query($sql2);

	$filter1Class = str_replace(" ", "_", $filter1);
	// $filter1Class = strtolower($filter1Class);

	$SubFilterTop = '<li class="fashion_cata"><a class="lifetStyleFilter ' . $filter1Class . '_but" id="menu_list' . $menu_list . '" href="#">' . $filter1 . '</a>
	<div id="sub_menu_list' . $menu_list . '">
		<ul class="fashion_sub_cata">';

	// $sub_menu_list=1;
	if ($res2->num_rows > 0) {
		while ($row2 = $res2->fetch_assoc()) {
			// $SubFilter = $SubFilter." ".$row2[filter_2];
			$subFilterClass = str_replace(" ", "_", $row2[filter_2]);
			$SubFilter = $SubFilter . '<a class="lifetStyleFilter ' . $subFilterClass . '_but" href="#"><li>' . $row2[filter_2] . '</li></a>';
		}
	}

	$SubFilterBottom = '</ul>
		</div>
	</li>';

	$SubFilter = $SubFilterTop . $SubFilter . $SubFilterBottom;

	return $SubFilter;
}

class general {

	function getNearbyInfo($cata_id, $res_hotel) {
		include 'data_connection.php';
		if ($res_hotel == "hotel") {

			$getNearby = "SELECT tourism_nearby.cata_id, tourism_nearby.hotel_id, general_info.cata, general_info.name, general_info.image_link, general_info.type
			FROM `tourism_nearby` LEFT JOIN `general_info` ON tourism_nearby.hotel_id=general_info.id WHERE tourism_nearby.cata_id = '$cata_id' LIMIT 5";
		} else if ($res_hotel == 'restaurant') {
			$getNearby = "SELECT tourism_nearby.cata_id, tourism_nearby.restaurant_id, general_info.cata, general_info.name, general_info.image_link, general_info.type
			FROM `tourism_nearby` LEFT JOIN `general_info` ON tourism_nearby.restaurant_id=general_info.id WHERE tourism_nearby.cata_id = '$cata_id' LIMIT 5";
		}

		$result = $conn->query($getNearby);

		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				if ($res_hotel == "restaurant" && $row[restaurant_id] != "") {
					echo '
				<div class="nearby_box">
					<div class="nearby_img">
						<img class="img-responsive" src="./image/title_image/' . $row[image_link] . '">
					</div>
					<div class="nearby_data">
						<a target="blank" href="' . $row[cata] . '.php?content=' . $row[name] . '"><span>' . $row[name] . '</span></a><br>
						<span>' . $row[type] . '</span><br>
						<span>Full Address</span>
					</div>
				</div>
				';
				}

				if ($res_hotel == "hotel" && $row[hotel_id] != "") {
					echo '
				<div class="nearby_box">
					<div class="nearby_img">
						<img class="img-responsive" src="./image/title_image/' . $row[image_link] . '">
					</div>
					<div class="nearby_data">
						<a target="blank" href="' . $row[cata] . '.php?content=' . $row[name] . '"><span>' . $row[name] . '</span></a><br>
						<span>' . $row[type] . '</span><br>
						<span>Full Address</span>
					</div>
				</div>
				';
				}

			}

		}
	}

	function getUserImage($userid) {
		include 'data_connection.php';
		$getId = "SELECT gender, image_link FROM user where id='$userid';";
		$getdata = $conn->query($getId);
		if ($getdata->num_rows > 0) {
			while ($row = $getdata->fetch_assoc()) {

				// echo '<a href="#0" class="pro_link"  ><img src="./image/user_image/'.$row[image_link].'"></a>';
				$image_link = "image/user_image/" . $row[image_link];
				$imgExist = file_exists($image_link);
				if ($row[image_link] == "" || $imgExist == false) {
					if ($row[gender] == 'female') {
						$image_link = "image/user_image/profile2.jpg";
					} else {
						$image_link = "image/user_image/profile.jpg";
					}
				}

				echo '<a href="./profile"  "><img src="' . $image_link . '"></a>';

			}
		}

	}

	function general_name($cata, $sub_cata, $id) {

		include 'data_connection.php';
		$fetch_name = "SELECT name FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

		$data = $conn->query($fetch_name);

		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {
				echo $row[name];
			}
		}
	}

	function general_image($cata, $sub_cata, $id) {
		include "data_connection.php";

		//$fetch_image_data = "SELECT * FROM `general_image` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";
		$fetch_image_data = "SELECT * FROM `general_image` WHERE cata_id = '$id' AND cata= '$cata';";

		$data = $conn->query($fetch_image_data);
		$ac = 0;
		if ($data->num_rows < 1) {
			echo "
					<style type='text/css'>
					     .attrction_panel{
					     	display:none;
					     }
					</style>
				";
		} else if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {

				$image_link = $row[image_link];

				// Blank image remove
				$image = strpos($row['image_link'], ".jpg");
				if (strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false || empty($row['image_link'])) {
					$image_link = "./image/main_image/1_no_image_main.jpg";
				} else {
					$image_link = "./image/main_image/" . $row['image_link'];
				}
				// Blank image remove

				$imgAlt = $image_link;
				$imgAlt = str_replace(".jpg", "", $imgAlt);
				$imgAlt = str_replace(".png", "", $imgAlt);
				$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);

				if ($imgAlt != "") {
					if ($ac == 0) {
						echo '
						<div class="item active">
							<img src="' . $image_link . '" alt="' . $imgAlt . '">
							<div class="carousel-caption">
							</div>
						</div>
						';
						$ac = $ac + 1;
					} else {
						echo '
						<div class="item">
							<img src="' . $image_link . '" alt="' . $imgAlt . '">
							<div class="carousel-caption">

							</div>
						</div>
						';
					}
				}
			} //image while
		} //image if
	} //general_image_function
	function getFilterList($cata, $sub_cata, $id) {
		include "data_connection.php";

		$fetch_list_data = "SELECT depertment FROM `doctor_faculty` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' GROUP BY depertment;";

		$data = $conn->query($fetch_list_data);
		$filter_list = "";
		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {
				$deptUP = strtoupper($row[depertment]);
				$deptlow = strtolower($row[depertment]);
				$filter_list = $filter_list . '<li><a href="#0" class="education_filter ' . $deptlow . '_but">' . $deptUP . '</a></li>';
			}

		}
		return $filter_list;
	}

	function doctor_faculty_list($cata, $sub_cata, $id, $top, $bottom, $menu) {
		include "data_connection.php";
		$fetch_list_data = "SELECT * FROM `doctor_faculty` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

		$data = $conn->query($fetch_list_data);

		if ($data->num_rows > 0) {
			echo '<div class="col-sm-4" >';
			echo $menu;
			echo $top;
			while ($row = $data->fetch_assoc()) {
				echo '  <div class="col-sm-12 edu-info-box ' . strtolower($row[depertment]) . '">';

				if (!empty($row[image_link])) {
					echo '<div class="fac_doc_img img-responsive"><img src="./image/item_image/' . $row[image_link] . '"></div>';
				}

				echo "<div class='fac_doc_data'>";
				if (!empty($row[name])) {
					echo '<h3>' . $row[name] . '</h3>';
				}

				if (!empty($row[designation])) {
					echo '<h4>' . $row[designation] . '</h4>';
				}

				if (!empty($row[depertment])) {
					echo '<span> Dept: ' . $row[depertment] . '<br> </span>';
				}

				if (!empty($row[schedule])) {
					echo '<span>' . $row[schedule] . '<br></span>';
				}

				if (!empty($row[education])) {
					echo '<span>' . $row[education] . '<br></span>';
				}

				if (!empty($row[email])) {
					echo '<span> Email: ' . $row[email] . '<br> </span>';
				}

				if (!empty($row[mobile])) {
					echo '<span> Phone: ' . $row[mobile] . '<br> </span>';
				}

				echo '</div>';
				echo '</div>';
			}
			echo $bottom;
			echo '</div>';
		}

	}

	function item_list_btn($contantId) {

		include 'data_connection.php';

		$getItemList = "SELECT id, name FROM item_list WHERE cata_id = $contantId ORDER BY name ASC";

		$res = $conn->query($getItemList);

		$i = 1;
		if ($res->num_rows > 0) {
			while ($row = $res->fetch_assoc()) {

				echo '<option value="' . $row[id] . '">' . $row[name] . '</option>';
				$i++;
			}
		}

	}

	function general_item_list($cata, $sub_cata, $id, $table_pattern_top, $table_pattern_bottom, $life_style_menu, $elecValue, $elecPriceLow, $elecPriceHigh) {
		unset($_SESSION[real_item_id]);
		include "data_connection.php";
		if ($cata == "electronics") {
			if (isset($elecValue) && $elecValue != "") {
				$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata' AND what_item = '$elecValue';";
			} else if (isset($elecPriceLow) && isset($elecPriceHigh)) {
				$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata'  AND price >= '$elecPriceLow' AND price <= '$elecPriceHigh';";

			} else {
				$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata';";
			}
		} else if ($cata == "food") {
			$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata'  ORDER BY filter_1;";
		} else if ($cata == "residence") {
			// $table_pattern_top-----room/servivce
			$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata' AND filter_1 = '$table_pattern_top';";
		} else {
			$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata';";
		}
		$data = $conn->query($fetch_item_data);
		$item_number = 1;

		if ($data->num_rows < 1) {
			echo "
					<style type='text/css'>
					     .attrction_panel{
					     	display:none;
					     }
					</style>
				";
		} else if ($data->num_rows > 0 && $cata == "food") {

			$filter_access = "true";
			while ($row = $data->fetch_assoc()) {

				$_SESSION[food_item_detect][$item_number] = $row[id];

				$rate = ($row[total_rate] / $row[rate_people]) / 3;
				$rate = $rate * 20;

				if ($item_number == 1) {
					echo $table_pattern_top;
				}
				//$id = 5;

				if ($filter_access == "false" && $filter_access_tmp != $row[filter_1]) {
					$filter_access = "true";
				}

				$f_class = strtolower($row[filter_1]);
				$f_class = str_replace(" ", "_", $f_class);
				$f_class = str_replace("&", "", $f_class);

				$imgAlt = $row[image_link];
				$imgAlt = str_replace(".jpg", "", $imgAlt);
				$imgAlt = str_replace(".png", "", $imgAlt);
				$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
				$imgAlt = $imgAlt . " " . $row[filter_1];

				if ($filter_access == "true") {

					$filter_access_tmp = $row[filter_1];
					$filter_access = "false";
					echo '
						<tr>
							<td class="food_item_Type" colspan="6">
								<div class="item_type item_filter ' . $f_class . '" ID="' . $f_class . ' ">
									<span>' . strtoupper($row[filter_1]) . '</span>
								</div>
							</td>
						</tr>
						';

				}

				$tmpImgLink = 'image/item_image/s_' . $row[image_link];
				if (file_exists($tmpImgLink)) {
					$tmpImgLink = 'image/item_image/s_' . $row[image_link];
				} else {
					$tmpImgLink = 'image/default_image/fooditem.png';
				}
				echo '

					<tr  class="item_filter ' . $f_class . '">

						<td class="item-num">
							<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '&content=' . $_SESSION[content_name] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">
								<span>' . $item_number . '</span>
							</a>
						</td>
						<td class="table_img">
							<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '&content=' . $_SESSION[content_name] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">
							 	<img src="' . $tmpImgLink . '" alt="' . $imgAlt . '">
							 </a>
							</td>
						<td class="item-name">
							<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '&content=' . $_SESSION[content_name] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">
								' . $row[name] . '
							</a>
						</td>
						<td class="item-price">
							<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '&content=' . $_SESSION[content_name] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">
								' . $row[price] . '
							</a>
						</td>
						<td class="item-rate">
							<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '&content=' . $_SESSION[content_name] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">

							<ul class="star-rating">
								<li class="current-rating" id="current-rating" style="width:' . $rate . '%"><!-- will show current rating --></li>
								<span id="ratelinks">
									<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
									<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
									<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
									<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
									<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
								</span>
							</ul>
							</a>

						</td>

						<td class="item-see">
							<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '&content=' . $_SESSION[content_name] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">See Details</a>
						</td>

					</tr>

					';
				$real_item_id[$item_number] = $row[id];

				$item_number = $item_number + 1;
			} //item_list while
			echo $table_pattern_bottom;

			$_SESSION[real_item_id] = $real_item_id;
		} //item_list if
		else if ($data->num_rows > 0 && $cata == "life style") {
			if ($sub_cata == "gym_fitness" || $sub_cata == "parlor_salon") {
				while ($row = $data->fetch_assoc()) {

					$imgAlt = $row[image_link];
					$imgAlt = str_replace(".jpg", "", $imgAlt);
					$imgAlt = str_replace(".png", "", $imgAlt);
					$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
					$imgAlt = $imgAlt . " " . $row[filter_1];

					// Blank image remove
					$image = strpos($row['image_link'], ".jpg");
					if (strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false || empty($row['image_link'])) {
						$imageLink = "SELECT image_link FROM general_info WHERE id = $id";
						$imgLink = $conn->query($imageLink);
						if ($imgLink->num_rows > 0) {
							while ($imgRow = $imgLink->fetch_assoc()) {
								$image = "./image/title_image/" . $imgRow[image_link];
							}
						}
					} else {
						$image = "./image/item_image/" . $row['image_link'];
					}
					// Blank image remove

					$des = substr($row[description], 0, 50) . '...';
					echo '<div class="slide attraction_box col-sm-2">
						<img class="img-responsive"  src="' . $image . '" alt="' . $imgAlt . '">
						<div class="attraction_info">
							<div class="details">
								<a class="show-popup" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">Details</a>
							</div>
							<span><u>' . $row[name] . '</u></span>
							<p>' . $des . '</p>
						</div>
					</div>';
				}

			} else if ($sub_cata == "accessorice") {
				echo '<div class="col-sm-12 life-list" ><!-- Item List LIST -->';
				echo $table_pattern_top;
				echo $life_style_menu;
				echo "<div class='all_items'>";
				while ($row = $data->fetch_assoc()) {
					$filter1 = str_replace(" ", "_", $row[filter_1]);
					$filter2 = str_replace(" ", "_", $row[filter_2]);
					$filter3 = str_replace(" ", "_", $row[filter_3]);

					$imgAlt = $row[image_link];
					$imgAlt = str_replace(".jpg", "", $imgAlt);
					$imgAlt = str_replace(".png", "", $imgAlt);
					$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
					$imgAlt = $imgAlt . " " . $row[filter_1];
					// $filter1=strtolower($filter1);
					echo '<div class="info-box  item_filter ' . $filter1 . ' ' . $filter2 . ' ' . $filter3 . '" >
							<div class="info_box_img">
								<img  src="./image/item_image/s_' . $row[image_link] . '" alt="' . $imgAlt . '">
							</div>
							<div class="product_info">
								<div class="table-responsive">
									<table class="table">

										<tbody>
											<tr>
												<td>' . $row[name] . '</td>
											</tr>
											<tr>
												<td>Price:' . $row[price] . '</td>
											</tr>

											<tr class="item-see">
												<a class="show-popup" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">See Details</a>
											</tr>

										</tbody>
									</table>
								</div>
							</div>
						</div>
						';
				}

				echo "</div>";
				echo $table_pattern_bottom;
				echo '	</div><!-- Item LIST -->';
			} else {
				echo '<div class="col-sm-12 life-list" ><!-- Item List LIST -->';
				echo $table_pattern_top;
				echo $life_style_menu;
				echo "<div class='all_items'>";
				while ($row = $data->fetch_assoc()) {
					$filter1 = str_replace(" ", "_", $row[filter_1]);
					$filter2 = str_replace(" ", "_", $row[filter_2]);
					$filter3 = str_replace(" ", "_", $row[filter_3]);
					// $filter1=strtolower($filter1);
					$imgAlt = $row[image_link];
					$imgAlt = str_replace(".jpg", "", $imgAlt);
					$imgAlt = str_replace(".png", "", $imgAlt);
					$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
					$imgAlt = $imgAlt . " " . $row[filter_1];

					echo '<div class="info-box  item_filter ' . $filter1 . ' ' . $filter2 . ' ' . $filter3 . '" >
						<div class="info_box_img">
							<img  src="./image/item_image/s_' . $row[image_link] . '" alt="' . $imgAlt . '">
						</div>
						<div class="product_info">
							<div class="table-responsive">
								<table class="table">

									<tbody>
										<tr>
											<td>' . $row[name] . '</td>
										</tr>
										<tr>
											<td>Price:' . $row[price] . '</td>
										</tr>

										<tr class="item-see">
											<a class="show-popup" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">See Details</a>
										</tr>

									</tbody>
								</table>
							</div>
						</div>
						</div>
						';
				}
				echo "</div>";
				echo $table_pattern_bottom;
				echo '	</div><!-- Item LIST -->';
			}
		} //life style
		else if ($data->num_rows > 0 && $cata == "electronics") {
			$countItem = 1;
			while ($row = $data->fetch_assoc()) {

				$imgAlt = $row[image_link];
				$imgAlt = str_replace(".jpg", "", $imgAlt);
				$imgAlt = str_replace(".png", "", $imgAlt);
				$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
				$imgAlt = $imgAlt . " " . $row[filter_1];

				echo '
					<div class="col-sm-2 product_area" >
						<a href="#">
							<div class="product-box">
								<div class="product-box-hover">

									<div class="remember">
										<a href="www.youtube.com">REMEMBER</a>
									</div>
									<div class="compare">
										<a  href="www.facebook.com">COMPARE</a>
									</div>
									<div class="details">
										<a class="show-popup" target="_blank" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">Details</a>
									</div>
								</div>

								<!-- <div class="image-space"></div>  -->
								<img src="./image/item_image/' . $row[image_link] . '" class="img-responsive"  alt="' . $imgAlt . '"></img>
								<div class="table-responsive">
									<table class="pro_table">

										<tbody>
											<tr>
												<td><b style="color:green;">' . $countItem . '. </b>' . $row[name] . '</td>
											</tr>
											<!--<tr>
											<td>' . $row[description] . '</td>
										</tr> -->
										<tr>
											<td>Price : ' . $row[price] . '</td>
										</tr>
										<tr>
											<td>Review : </td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</a>
				</div>
				';
				$real_item_id[$countItem] = $row[id];

				$countItem++;
			}

			$_SESSION[real_item_id] = $real_item_id;
		} else if ($data->num_rows > 0 && $cata == "residence") {
			while ($row = $data->fetch_assoc()) {

				$imgAlt = $row[image_link];
				$imgAlt = str_replace(".jpg", "", $imgAlt);
				$imgAlt = str_replace(".png", "", $imgAlt);
				$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
				$imgAlt = $imgAlt . " " . $row[filter_1];

				if ($table_pattern_top == "room") {
					echo '<div class="slide attraction_box col-sm-2">
					<img class="img-responsive"  src="./image/item_image/' . $row[image_link] . '" alt="' . $imgAlt . '">
					<div class="attraction_info">
						<div class="details">
							<a class="show-popup" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">Details</a>
						</div>
						<span><u>' . $row[name] . '</u></span>
						<p>' . $row[description] . '</p>
					</div>
					</div>';
				}

				if ($table_pattern_top == "service") {
					echo '<div class="slide attraction_box2 col-sm-2">
					<img class="img-responsive"  src="./image/item_image/' . $row[image_link] . '" alt="' . $imgAlt . '">
					<div class="attraction_info">
						<div class="details">
							<a class="show-popup" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">Details</a>
						</div>
						<span><u>' . $row[name] . '</u></span>
						<p>' . $row[description] . '</p>
					</div>
					</div>';
				}

			}
		} else if ($data->num_rows > 0 && $cata == "tourism") {
			while ($row = $data->fetch_assoc()) {

				$imgAlt = $row[image_link];
				$imgAlt = str_replace(".jpg", "", $imgAlt);
				$imgAlt = str_replace(".png", "", $imgAlt);
				$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
				$imgAlt = $imgAlt . " " . $row[filter_1];

				echo '<div class="slide attraction_box col-sm-2">
				<img class="img-responsive"  src="./image/item_image/' . $row[image_link] . '" alt="' . $imgAlt . '">
				<div class="attraction_info">
					<div class="details">
						<a class="show-popup" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">Details</a>
					</div>
					<span><u>' . $row[name] . '</u></span>
					<p>' . $row[description] . '</p>
				</div>
				</div>';
			}

		} else if ($data->num_rows > 0 && $cata == "ecommerce") {

			echo '<div class="col-sm-12 life-list" ><!-- Item List LIST -->';
			echo $table_pattern_top;
			echo $life_style_menu;
			echo "<div class='all_items'>";
			while ($row = $data->fetch_assoc()) {
				$filter1 = str_replace(" ", "_", $row[filter_1]);
				$filter2 = str_replace(" ", "_", $row[filter_2]);
				$filter3 = str_replace(" ", "_", $row[filter_3]);
				// $filter1=strtolower($filter1);
				$imgAlt = $row[image_link];
				$imgAlt = str_replace(".jpg", "", $imgAlt);
				$imgAlt = str_replace(".png", "", $imgAlt);
				$imgAlt = str_replace(str_split('1234567890_-'), ' ', $imgAlt);
				$imgAlt = $imgAlt . " " . $row[filter_1];

				echo '<div class="info-box  item_filter ' . $filter1 . ' ' . $filter2 . ' ' . $filter3 . '" >
						<div class="info_box_img">
							<img  src="./image/item_image/s_' . $row[image_link] . '" alt="' . $imgAlt . '">
						</div>
						<div class="product_info">
							<div class="table-responsive">
								<table class="table">

									<tbody>
										<tr>
											<td>' . $row[name] . '</td>
										</tr>
										<tr>
											<td>Price:' . $row[price] . '</td>
										</tr>

										<tr class="item-see">
											<a class="show-popup" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">See Details</a>
										</tr>

									</tbody>
								</table>
							</div>
						</div>
						</div>
						';
			}
			echo "</div>";
			echo $table_pattern_bottom;
			echo '	</div><!-- Item LIST -->';
		} else if ($data->num_rows < 1 && $cata == "tourism") {
			echo '
				<style type="text/css">
					.attrction_panel{
						display:none;
					}
				</style>
			';
		} else if ($data->num_rows < 1 && $cata == "residence" && $table_pattern_top == "room") {
			echo '
				<style type="text/css">
					.attrction_room{
						display:none;
					}
				</style>
			';
		} else if ($data->num_rows < 1 && $cata == "residence" && $table_pattern_top == "service") {
			echo '
				<style type="text/css">
					.attrction_service{
						display:none;
					}
				</style>
			';
		}

	} //item_list_function

	function fetch_comment($cata, $sub_cata, $id, $last_comment_id, $fixed_item_value) {

		$comment_id = $last_comment_id;
		$last_comment_id = preg_replace('/[^0-9.]+/', '', $last_comment_id);

		// Select Rating title
		if ($_SESSION[cata] == "food") {
			$rateTitle1 = "TASTE";
			$rateTitle2 = "PRICE";
			$rateTitle3 = "SERVICE";
		}

		// echo "LAST ITEM CHECK 55555555555555".$item_id;
		include "data_connection.php";

		if ($sub_cata == "dxHomeFlag") {
			$activeClass = "active";
			$fetch_comment_data = "SELECT comment.id as commentid, comment.cata_id, comment.sub_cata, comment.user_id, comment.item_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, comment.points, user.name,user.id, user.name, user.gender, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id
				WHERE comment.cata='$cata' AND comment.status = 'show' ORDER BY comment.points DESC;";
		} else if (!empty($fixed_item_value)) {

			$fetch_comment_data = "SELECT comment.id as commentid, comment.cata_id, comment.sub_cata, comment.user_id, comment.item_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, user.name,user.id, user.name, user.gender, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id
				WHERE comment.item_id='$fixed_item_value' AND comment.status = 'show' ORDER BY comment.date DESC, comment.time DESC ;";
		} else if (empty($last_comment_id)) {

			$fetch_comment_data = "SELECT comment.id as commentid, comment.cata_id, comment.sub_cata, comment.user_id, comment.item_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, user.name,user.id, user.name, user.gender, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id
				WHERE comment.cata_id = '$id' AND comment.cata= '$cata' AND comment.sub_cata='$sub_cata' AND comment.status = 'show' ORDER BY comment.date DESC, comment.time DESC ;";

		} else if (!empty($last_comment_id)) {

			$fetch_comment_data = "SELECT comment.id as commentid, comment.cata_id, comment.sub_cata, comment.user_id, comment.item_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, user.name,user.id, user.name, user.gender, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id
				WHERE comment.id='$last_comment_id' AND comment.status = 'show' ORDER BY comment.date DESC, comment.time DESC ;";
		}

		$image_link = "";
		$data = $conn->query($fetch_comment_data);
		$item_number = 1;
		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {
				if (!empty($row[image_link])) {
					$image_link = "image/user_image/" . $row[image_link];
				} else {
					if ($row[gender] == 'female') {
						$image_link = "image/user_image/profile2.jpg";
					} else {
						$image_link = "image/user_image/profile.jpg";
					}
				}

				$general_obj_com = new general();
				$getCommentImg = $general_obj_com->getCommentImg($row[commentid]);

				// $date1=date_create($row[date]);
				// $date2=date_create(date("Y-m-d"));
				// $format = 'M-d-y';
				// $date = DateTime::createFromFormat($format, $row[date]);
				// echo "Format: $format; " . $date->format('Y-m-d H:i:s') . "\n";
				// $date =  $date->format('Y-m-d');

				$date = date_create_from_format("Y-m-d", $row[date]);
				$date = date_format($date, "j M Y");

				$time = date_create_from_format("H:i:s", $row[time]);
				$time = date_format($time, "g:i:a");

				// $format = 'Y-m-d';
				// $date = DateTime::createFromFormat($format, $row[date]);
				// $date = $date->format();

				if ($cata == "food" && $row[type] == "rate") {

					$getCommentItem = "SELECT * FROM `item_list` WHERE `id` = '$row[item_id]'";
					$resItemId = $conn->query($getCommentItem);

					if ($resItemId->num_rows > 0) {
						while ($res2 = $resItemId->fetch_assoc()) {
							$tmp = $res2[id];
							$id = $res2[cata_id];
							$itemIndex = array_search($tmp, $_SESSION[food_item_detect]);
							$sub_cata = $res2[sub_cata];
							$comment_item = '
											<div class="comment_item">
												<div class="comment_item_num">
													<span>' . $itemIndex . '</span>
												</div>

												<div class="comment_item_img">
													<img class="img-responsive" src="./image/item_image/s_' . $res2[image_link] . '">
												</div>

												<div class="comment_item_name">
													<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $res2[id] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $res2[id] . ')">' . $res2[name] . '</a>
												</div>
											</div>
										';
						}
					} else {
						$comment_item = "";
					}

				}

				$contentName = "";

				$getCommentContentName = "SELECT name FROM `general_info` WHERE `id` = '$row[cata_id]'";
				$ContentName = $conn->query($getCommentContentName);
				if ($ContentName->num_rows > 0) {
					while ($res3 = $ContentName->fetch_assoc()) {
						$contentName = $res3[name];
					}
				}

				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

				$link_name = strtolower($contentName);
				$link_name = str_replace(" ", "-", $link_name);

				if ($row[type] == "comment" || $row[type] == "question") {
					$com_type_img = "image/default_image/comment.png";
					if ($row[type] == "question") {
						$com_type_img = "image/default_image/question.png";
					} else {
						$com_type_img = "image/default_image/comment.png";
					}

					echo '
											<div class="item ' . $activeClass . '">
													<div class="only_com ">
														<div class="only_com_user_info">
															<div class="com_user_data">
																<img class="img-responsive user_img" src="' . $image_link . '"></img>
																<div class="com_user_all">
																	<div class="com_user_name">
																		<span class="name">' . $row[name] . '</span><br>
																		<span class="comment_date">' . $time . '<br>' . $date . '</span><br>
																		<span class="comment_date"><b>Points: ' . $row[points] . '</b></span>
																	</div>

																	<div class="com_user_content_info">
																		<a href="' . $subcata_url[0] . '/' . strtolower($link_name) . '">' . $contentName . '</a>
																	</div>
																</div>

															</div>
														</div>

														<div class="comment_section">
															<div class="opinion">
																<span class="op"> ' . $row[text] . '</span>
															</div>


															<div class="comment_img">
																' . $getCommentImg . '
															</div>
														</div>
													</div>
											</div>
											';

				} else if ($row[type] == "rate") {

					$rate_1 = $row[rate_1] * 20;
					$rate_2 = $row[rate_2] * 20;
					$rate_3 = $row[rate_3] * 20;

					echo '
								<div class="item ' . $activeClass . '">
								<div class="only_com">
									<div class="only_com_user_info">
										<div class="com_user_data">
											<img class="img-responsive user_img" src="' . $image_link . '"></img>
											<div class="com_user_all">
												<div class="com_user_name">
													<span class="name">' . $row[name] . '</span><br>
													<span class="comment_date">' . $time . '<br>' . $date . '</span><br>
													<span class="comment_date"><b>Points: ' . $row[points] . '</b></span>
												</div>

												<div class="com_user_content_info">
													<a href="' . $subcata_url[0] . '/' . strtolower($link_name) . '">' . $contentName . '</a>
												</div>
											</div>

										</div>
									</div>

									<div class="review_comment">
										<div class="com_user_review">
											<div class="comment_user_review">
												<div class="comment_review_quality">
													<div class="comment_review_title">
														<span>' . $rateTitle1 . '</span>
													</div>
													<div class="comment_review_rate">
														<ul class="star-rating star_not_move">
															<li class="current-rating" id="current-rating" style="width:' . $rate_1 . '%"><!-- will show current rating --></li>
															<span id="ratelinks">
																<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
																<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
																<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
																<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
																<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
															</span>
														</ul>


													</div>
												</div>

												<div class="comment_review_quality">
													<div class="comment_review_title">
														<span>' . $rateTitle2 . '</span>
													</div>
													<div class="comment_review_rate">

														<ul class="star-rating">
															<li class="current-rating" id="current-rating" style="width:' . $rate_2 . '%"><!-- will show current rating --></li>
															<span id="ratelinks">
																<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
																<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
																<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
																<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
																<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
															</span>
														</ul>

													</div>
												</div>

												<!-- style="margin-left:110px" -->
												<div class="comment_review_quality" >
													<div class="comment_review_title">
														<span>' . $rateTitle3 . '</span>
													</div>
													<div class="comment_review_rate">

														<ul class="star-rating">
															<li class="current-rating" id="current-rating" style="width:' . $rate_3 . '%"><!-- will show current rating --></li>
															<span id="ratelinks">
																<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
																<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
																<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
																<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
																<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
															</span>
														</ul>

													</div>
												</div>
											</div>
											' . $comment_item . '
										</div>
										<div class="review_text">

											<div class="opinion rate_opinion">
												<span class="op"> ' . $row[text] . '</span>
											</div>
										</div>

										<div class="comment_img">
											' . $getCommentImg . '
										</div>
									</div>


								</div>
								</div>
								';

				} // else if of rate

				$activeClass = "";
			}
		}
	}

	function getCommentImg($commentId) {
		include 'data_connection.php';

		$imgStore = "";
		$getImg = "SELECT * FROM `comment_image` WHERE `comment_id` = '$commentId' LIMIT 3";

		$data = $conn->query($getImg);

		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {
				$imgStore = $imgStore . '<div class="comment_img_box"> <img class="img-responsive" src="./image/comment_img/' . $row[image_link] . '"></div>';
			}
		}
		return $imgStore;
	}

	function formateOpeningHour($openingHour) {
		$formatHour = "<div class='openingHourFormate'>";

		$time_note = explode(';', $openingHour);
		$hourArray = explode(',', $time_note[0]);
		$hourCount = count($hourArray);

		if ($hourCount <= 1) {
			$formatHour = $formatHour . "<span class='hourFdate'>Sunday</span>  <span>:&nbsp; " . $hourArray[0] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Monday</span> <span>:&nbsp; " . $hourArray[0] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Tueday</span>  <span>:&nbsp; " . $hourArray[0] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Wednesday</span> <span>:&nbsp; " . $hourArray[0] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Thursday</span>  <span>:&nbsp; " . $hourArray[0] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Friday</span>  <span>:&nbsp; " . $hourArray[0] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Saturday</span>  <span>:&nbsp; " . $hourArray[0] . "</span>";
		} else {
			$formatHour = $formatHour . "<span class='hourFdate'>Sunday</span>  <span>:&nbsp; " . $hourArray[0] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Monday</span> <span>:&nbsp; " . $hourArray[1] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Tueday</span>  <span>:&nbsp; " . $hourArray[2] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Wednesday</span> <span>:&nbsp; " . $hourArray[3] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Thursday</span>  <span>:&nbsp; " . $hourArray[4] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Friday</span>  <span>:&nbsp; " . $hourArray[5] . "</span>";
			$formatHour = $formatHour . "<br><span class='hourFdate'>Saturday</span>  <span>:&nbsp; " . $hourArray[6] . "</span>";
		}

		$formatHour = $formatHour . "<br><span>" . $time_note[1] . "</span></div>";
		return $formatHour;
	}

	function general_info($cata, $sub_cata, $id) {
		include "data_connection.php";
		//$np means narrow pattern
		$nptop = '<div class="info-area">
			<div class="info-area-title">
				<span>';
		$npmiddle = '</span>
				</div>
				<div class="info-ans">
					<span class="info_span">';
		$npbottom = '</span>
					</div>
				</div>';

		$wptop = '<div class="info-area">
				<div class="info-area-title">
					<span>';
		$wpmiddle = '</span>
					</div>
					<div class="info-ans">
						<span class="info_span">';
		$wpbottom = '</span>
						</div>
					</div>';

		$fetch_g_data = "SELECT * FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

		$data = $conn->query($fetch_g_data);

		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {

				$_SESSION[content_name] = $row[name];

				$total_rate = ($row[total_avg_review] / $row[review_people]);

				$total_rate = $total_rate * 20;
				$rate_1 = ($row[rate_1] / $row[review_people]) * 20;
				$rate_2 = ($row[rate_2] / $row[review_people]) * 20;
				$rate_3 = ($row[rate_3] / $row[review_people]) * 20;

				$general_hour_obj = new general();

				if ($cata == "food") {

					if (!empty($row[name])) {
						echo '<div class="narrow_pattern">

													<div class="shop_title"><h1>' . $row[name] . '</h1></div>
											  </div>
										';
						echo '<div class="share_option" id="expand_test">
												<div id="inside_expand">
													<div class="bookmark_bt">
														<a  href="#0" onclick="return bookmark(\'' . $_SESSION[content_id] . '\',\'' . $_SESSION[userid] . '\')">
														<i class="fas fa-heart"></i>
														<i class="far fa-heart"></i>
														</a>
													</div>';
						include 'share_site.php';

						echo '  </div>
											   </div>';
					}

					if (!empty($row[type])) {
						echo $nptop . "Type" . $npmiddle . "" . $row[type] . "" . $npbottom;
					}

					$general_rate_obj = new general();
					echo $nptop . "Rating" . $npmiddle . "" . $general_rate_obj->ratingAll($total_rate, $rate_1, $rate_2, $rate_3) . "" . $npbottom;

					if (!empty($row[description])) {
						echo $nptop . "About" . $npmiddle . "" . $row[description] . "" . $npbottom;
					}

					if (!empty($row[column_5])) {
						$foodFacilities = $row[column_5];
						$facilities_other = "";

						$foodFacilitiesArray = explode(";", $foodFacilities);

						$foodFacilitiesTop = explode(",", $foodFacilitiesArray[0]);

						$listOfFacilities = '<ul>';

						$listOfFacilities = $listOfFacilities . "<div class='col-sm-6 feature_list'>";

						if ($foodFacilitiesTop[0] != '' && $foodFacilitiesTop[0] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[0] . '.png"> AC</li>';
						}
						if ($foodFacilitiesTop[1] != '' && $foodFacilitiesTop[1] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[1] . '.png"> WiFi</li>';
						}
						if ($foodFacilitiesTop[2] != '' && $foodFacilitiesTop[2] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[2] . '.png"> Card Payment</li>';
						}

						if ($foodFacilitiesTop[3] != '' && $foodFacilitiesTop[3] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[3] . '.png"> Reservation</li>';
						}

						if ($foodFacilitiesTop[4] != '' && $foodFacilitiesTop[4] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[4] . '.png"> Parking Lot</li>';
						}

						$listOfFacilities = $listOfFacilities . "</div>";

						$listOfFacilities = $listOfFacilities . "<div class='col-sm-6 feature_list'>";

						if ($foodFacilitiesTop[5] != '' && $foodFacilitiesTop[5] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[5] . '.png"> Smoking Zone</li>';
						}
						if ($foodFacilitiesTop[6] != '' && $foodFacilitiesTop[6] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[6] . '.png"> Kids Zone</li>';
						}
						if ($foodFacilitiesTop[7] != '' && $foodFacilitiesTop[7] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/' . $foodFacilitiesTop[7] . '.png"> Self Service</li>';
						}

						if ($foodFacilitiesTop[3] != '' && $foodFacilitiesTop[3] != null) {
							$listOfFacilities = $listOfFacilities . '<li><img src="./image/default_image/YES.png"> Capacity : ' . $foodFacilitiesTop[8] . '</li>';
						}

						$listOfFacilities = $listOfFacilities . "</div>";

						if (strlen($foodFacilitiesArray[1]) > 4) {

							$facilities_other_array = explode(",", $foodFacilitiesArray[1]);

							$i = 0;
							$facilities_other_ex = "";
							while ($facilities_other_array[$i]) {
								// $facilities_other_ex = $facilities_other_ex."<br> &#8226; ".$facilities_other_array[$i];
								$listOfFacilities = $listOfFacilities . '<li> &#8226; ' . $facilities_other_array[$i] . '</li>';
								$i++;
							}
							// $facilities_other = "<div class='facilities_other'><span>Others</span>   <span>".$facilities_other_ex."</span>  </div>";

						} else {
							$foodOtherFacilities = "";
						}

						$listOfFacilities = $listOfFacilities . "</ul>";

						echo $nptop . "Feature" . $npmiddle . "" . $listOfFacilities . "" . $npbottom;
					}

					if (!empty($row[column_2])) {
						echo $nptop . "Speciality" . $npmiddle . "" . $row[column_2] . "" . $npbottom;
					}

					if (!empty($row[offer])) {
						echo $nptop . "Offer" . $npmiddle . "" . $row[offer] . "" . $npbottom;
					}

					if (!empty($row[column_4])) {
						$hourFormate = $general_hour_obj->formateOpeningHour($row[column_4]);
						echo $nptop . "Opening Hour" . $npmiddle . "" . $hourFormate . "" . $npbottom;
					}

					if (!empty($row[web_site])) {
						$web_site_name_tmp = parse_url($row[web_site]);
						$web_site_name = $web_site_name_tmp['host'];
						echo $nptop . "Web Site" . $npmiddle . "<a target='_blank' href='" . $row[web_site] . "'>" . $web_site_name . "</a>" . $npbottom;
					}

					if (!empty($row[location])) {
						echo $nptop . "Location" . $npmiddle . "" . $row[location] . "" . $npbottom;
					}

					if (!empty($row[social_links])) {

						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode("<br>", $social_links);
						} else {
							$linkArray = array($social_links);
						}
						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 30) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}

					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $nptop . "Contact" . $npmiddle . "" . $contact . "" . $npbottom;
					}
					/*
									if(!empty($row[map]))
									{
										echo '<div class="info-area">
								   				'.$row[map].'
								   			</div>
										     ';
									}
									*/
				} else if ($cata == "life style") {
					echo '<div class="share_option" id="expand_test">
									<div id="inside_expand">
										<div class="bookmark_bt for_padding">
											<a  href="#0" onclick="return bookmark(\'' . $_SESSION[content_id] . '\',\'' . $_SESSION[userid] . '\')">
											<i class="fas fa-heart"></i>
											<i class="far fa-heart"></i>
											</a>
										</div>';
					include 'share_site.php';

					echo '  </div>
									</div>';
					if (!empty($row[type])) {
						echo $nptop . "Type" . $npmiddle . "" . $row[type] . "" . $npbottom;
					}

					$general_rate_obj = new general();
					echo $nptop . "Rating" . $npmiddle . "" . $general_rate_obj->ratingAll($total_rate, $rate_1, $rate_2, $rate_3) . "" . $npbottom;

					if (!empty($row[description])) {
						echo $nptop . "About" . $npmiddle . "" . $row[description] . "" . $npbottom;
					}

					if (!empty($row[offer])) {
						echo $nptop . "Offer" . $npmiddle . "" . $row[offer] . "" . $npbottom;
					}

					if (!empty($row[column_3])) {
						echo $nptop . "Speciality" . $npmiddle . "" . $row[column_3] . "" . $npbottom;
					}

					if (!empty($row[column_4])) {
						echo $nptop . "Speciality" . $npmiddle . "" . $row[column_4] . "" . $npbottom;
					}

					if (!empty($row[column_5])) {
						echo $nptop . "Programm Schedual" . $npmiddle . "" . $row[column_5] . "" . $npbottom;
					}

					if (!empty($row[column_6])) {
						echo $nptop . "Opening Hour" . $npmiddle . "" . $row[column_6] . "" . $npbottom;
					}

					if (!empty($row[column_7])) {
						echo $nptop . "Sponsor" . $npmiddle . "" . $row[column_7] . "" . $npbottom;
					}

					if (!empty($row[web_site])) {
						$web_site_name_tmp = parse_url($row[web_site]);
						$web_site_name = $web_site_name_tmp['host'];
						echo $nptop . "Web Site" . $npmiddle . "<a target='_blank' href='" . $row[web_site] . "'>" . $web_site_name . "</a>" . $npbottom;
					}
					if (!empty($row[social_links])) {
						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode("<br>", $social_links);
						} else if (strpos($social_links, "  ")) {
							$linkArray = explode("  ", $social_links);
						} else if (strpos($social_links, "   ")) {
							$linkArray = explode("   ", $social_links);
						} else {
							$linkArray = array($social_links);
						}

						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 10) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}
					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $nptop . "Contact" . $npmiddle . "" . $contact . "" . $npbottom;
					}
				} else if ($cata == "education") {
					echo '<div class="share_option" id="expand_test">
									<div id="inside_expand">
										<div class="bookmark_bt">
											<a  href="#0" onclick="return bookmark(\'' . $_SESSION[content_id] . '\',\'' . $_SESSION[userid] . '\')">
											<i class="fas fa-heart"></i>
											<i class="far fa-heart"></i>
											</a>
										</div>';
					include 'share_site.php';

					echo '  </div>
									</div>';

					if ($_SESSION[sub_cata] == "university" || $_SESSION[sub_cata] == "college" || $_SESSION[sub_cata] == "coaching") {

						$general_rate_obj = new general();
						echo $nptop . "Rating" . $npmiddle . "" . $general_rate_obj->ratingAll($total_rate, $rate_1, $rate_2, $rate_3) . "" . $npbottom;
					}

					if (!empty($row[description])) {
						echo "<div class='text_height_1'>" . $wptop . "About" . $wpmiddle . "" . $row[description] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_1])) {
						echo "<div class='text_height_2'>" . $wptop . "Department List" . $wpmiddle . "" . $row[column_1] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_2])) {
						echo "<div class='text_height_3'>" . $wptop . "Information" . $wpmiddle . "" . $row[column_2] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_3])) {
						echo "<div class='text_height_4'>" . $wptop . "Highlight" . $wpmiddle . "" . $row[column_3] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_4])) {
						echo "<div class='text_height_5'>" . $wptop . "Admission" . $wpmiddle . "" . $row[column_4] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_5])) {
						echo "<div class='text_height_6'>" . $wptop . "Admission Requirment" . $wpmiddle . "" . $row[column_5] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_6])) {
						echo "<div class='text_height_7'>" . $wptop . "Notice" . $wpmiddle . "" . $row[column_6];
						if (!empty($row[column_7])) {
							echo "<br><a href=" . $row[column_7] . " >See PDF</a>";
						}
						echo $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_8])) {
						echo "<div class='text_height_8'>" . $wptop . "Course Details" . $wpmiddle . "" . $row[column_8];
						if (!empty($row[column_9])) {
							echo "<br><a href=" . $row[column_9] . " >See PDF</a>";
						}
						echo $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_10])) {
						echo "<div class='text_height_9'>" . $wptop . "CGPA System" . $wpmiddle . "" . $row[column_10] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_11])) {
						echo "<div class='text_height_10'>" . $wptop . "Class Plan" . $wpmiddle . "" . $row[column_11];
						if (!empty($row[column_12])) {
							echo "<br><a href=" . $row[column_12] . " >See PDF</a>";
						}
						echo $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_13])) {
						echo "<div class='text_height_11'>" . $wptop . "Exam Schedual" . $wpmiddle . "" . $row[column_13];
						if (!empty($row[column_14])) {
							echo "<br><a href=" . $row[column_14] . " >See PDF</a>";
						}
						echo $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_15])) {
						echo "<div class='text_height_12'>" . $wptop . "Exam Hall" . $wpmiddle . "" . $row[column_15] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_16])) {
						echo "<div class='text_height_13'>" . $wptop . "Expanse" . $wpmiddle . "" . $row[column_16] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[web_site])) {
						echo "<div class='text_height_14'>" . $wptop . "Web Site" . $wpmiddle . "" . $row[web_site] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[social_links])) {
						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode("<br>", $social_links);
						} else if (strpos($social_links, "  ")) {
							$linkArray = explode("  ", $social_links);
						} else if (strpos($social_links, "   ")) {
							$linkArray = explode("   ", $social_links);
						} else {
							$linkArray = array($social_links);
						}

						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 10) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}

					// if(!empty($row[location]))
					// {
					// 	echo "<div class='text_height_16'>".$wptop."Address".$wpmiddle."".$row[location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					// }
					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $wptop . "Contact" . $wpmiddle . "" . $contact . "" . $wpbottom;
					}

				} else if ($cata == "health") {
					echo '<div class="share_option" id="expand_test">
									<div id="inside_expand">
										<div class="bookmark_bt">
											<a  href="#0" onclick="return bookmark(\'' . $_SESSION[content_id] . '\',\'' . $_SESSION[userid] . '\')">
											<i class="fas fa-heart"></i>
											<i class="far fa-heart"></i>
											</a>
										</div>';
					include 'share_site.php';

					echo '  </div>
									</div>';

					if ($_SESSION[sub_cata] == "hospital_clinic") {
						$general_rate_obj = new general();
						echo $nptop . "Rating" . $npmiddle . "" . $general_rate_obj->ratingAll($total_rate, $rate_1, $rate_2, $rate_3) . "" . $npbottom;
					}

					if (!empty($row[description])) {
						echo "<div class='text_height_18'>" . $wptop . "About" . $wpmiddle . "" . $row[description] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_1])) {
						echo "<div class='text_height_19'>" . $wptop . "Department List" . $wpmiddle . "" . $row[column_1] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_2])) {
						echo "<div class='text_height_20'>" . $wptop . "Facilities" . $wpmiddle . "" . $row[column_2] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_3])) {
						echo "<div class='text_height_21'>" . $wptop . "Ambulance" . $wpmiddle . "" . $row[column_3] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_4])) {
						echo "<div class='text_height_22'>" . $wptop . "Opening Hour" . $wpmiddle . "" . $row[column_4] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[web_site])) {
						echo "<div class='text_height_23'>" . $wptop . "Web Site" . $wpmiddle . "" . $row[web_site] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[social_links])) {
						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode("<br>", $social_links);
						} else if (strpos($social_links, "  ")) {
							$linkArray = explode("  ", $social_links);
						} else if (strpos($social_links, "   ")) {
							$linkArray = explode("   ", $social_links);
						} else {
							$linkArray = array($social_links);
						}

						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 10) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}
					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $wptop . "Contact" . $wpmiddle . "" . $contact . "" . $wpbottom;
					}
					// if(!empty($row[location]))
					// {
					// 	echo "<div class='text_height_25'>".$wptop."Address".$wpmiddle."".$row[location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					// }
					// if(!empty($row[map]))
					// {
					// 	echo "<div class='text_height_26'>".$wptop."Google Map".$wpmiddle."".$row[map].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					// }
				} else if ($cata == "electronics") {
					echo '<div class="share_option" id="expand_test">
									<div id="inside_expand">
										<div class="bookmark_bt">
											<a  href="#0" onclick="return bookmark(\'' . $_SESSION[content_id] . '\',\'' . $_SESSION[userid] . '\')">
											<i class="fas fa-heart"></i>
											<i class="far fa-heart"></i>
											</a>
										</div>';
					include 'share_site.php';

					echo '  </div>
									</div>';

					if (!empty($row[description])) {
						echo "<div class='text_height_43'>" . $wptop . "About" . $wpmiddle . "" . $row[description] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
					if (!empty($row[type])) {
						echo "<div class='text_height_44'>" . $wptop . "Type" . $wpmiddle . "" . $row[type] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
					if (!empty($row[column_2])) {
						echo "<div class='text_height_45'>" . $wptop . "Service" . $wpmiddle . "" . $row[column_2] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
					if (!empty($row[column_3])) {
						echo "<div class='text_height_46'>" . $wptop . "Warrenty" . $wpmiddle . "" . $row[column_3] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
					if (!empty($row[column_4])) {
						echo "<div class='text_height_47'>" . $wptop . "Servicing Center" . $wpmiddle . "" . $row[column_4] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
					if (!empty($row[column_5])) {
						echo "<div class='text_height_48'>" . $wptop . "Help Line" . $wpmiddle . "" . $row[column_5] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
					if (!empty($row[web_site])) {
						echo "<div class='text_height_49'>" . $wptop . "Web Site" . $wpmiddle . "" . $row[web_site] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[social_links])) {
						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode("<br>", $social_links);
						} else if (strpos($social_links, "  ")) {
							$linkArray = explode("  ", $social_links);
						} else if (strpos($social_links, "   ")) {
							$linkArray = explode("   ", $social_links);
						} else {
							$linkArray = array($social_links);
						}

						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 10) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}
					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $wptop . "Contact" . $wpmiddle . "" . $contact . "" . $wpbottom;
					}
				} else if ($cata == "residence") {

					if (!empty($row[name])) {
						echo '<div class="narrow_pattern">
										<div class="shop_title"><h1>' . $row[name] . '</h1></div>
									</div>
									';
					}

					if ($_SESSION[sub_cata] == "hotel" || $_SESSION[sub_cata] == "resort") {
						$general_rate_obj = new general();
						echo $nptop . "Rating" . $npmiddle . "" . $general_rate_obj->ratingAll($total_rate, $rate_1, $rate_2, $rate_3) . "" . $npbottom;
					}

					if (!empty($row[type])) {
						echo $nptop . "Type" . $npmiddle . "" . $row[type] . "" . $npbottom;
					}

					if (!empty($row[offer])) {
						echo $nptop . "Offer" . $npmiddle . "" . $row[offer] . "" . $npbottom;
					}
					// if(!empty($row[main_location]))
					// {
					// 	echo $nptop."Location".$npmiddle."".$row[main_location]."".$npbottom;
					// }
					if (!empty($row[column_2])) {
						echo $nptop . "Size" . $npmiddle . "" . $row[column_2] . "" . $npbottom;
					}

					if (!empty($row[column_3])) {
						echo $nptop . "Rent" . $npmiddle . "" . $row[column_3] . "" . $npbottom;
					}

					if (!empty($row[description])) {
						echo $nptop . "About" . $npmiddle . "" . $row[description] . "" . $npbottom;
					}

					// if(!empty($row[column_4]))
					// {
					// 	echo $nptop."Contact".$npmiddle."".$row[column_4]."".$npbottom;
					// }

					if (!empty($row[web_site])) {
						$web_site_name_tmp = parse_url($row[web_site]);
						$web_site_name = $web_site_name_tmp['host'];
						echo $nptop . "Web Site" . $npmiddle . "<a target='_blank' href='" . $row[web_site] . "'>" . $web_site_name . "</a>" . $npbottom;
					}

					if (!empty($row[social_links])) {
						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, "  ")) {
							$linkArray = explode("  ", $social_links);
						} else if (strpos($social_links, "   ")) {
							$linkArray = explode("   ", $social_links);
						} else {
							$linkArray = array($social_links);
						}

						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 10) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}
					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $nptop . "Contact" . $npmiddle . "" . $contact . "" . $npbottom;
					}
					// if(!empty($row[location]))
					// {
					// 	echo $nptop."Address".$npmiddle."".$row[location]."".$npbottom;
					// }

					// if(!empty($row[map]))
					// {
					// 	echo $nptop."Google Map".$npmiddle."".$row[map]."".$npbottom;
					// }
					// if(!empty($row[map]))
					// {
					// 	echo '<div ID="branch_map">';
					// 	echo '<div class="info-area">
					// 			<span>'.$row[main_location].' </span>
					//   				'.$row[map].'
					//   			</div>
					// 	     ';
					// 	echo '</div>';
					// }

				} else if ($cata == "tourism") {

					if (!empty($row[description])) {
						// echo "<div class='text_height_27'>".$wptop."About".$wpmiddle."".$row[description]."".$wpbottom." <div class='see_more'><a href='#0' onclick=\"seeMore('text_height_28')\">See More</a></div>".$wpbottom."</div>";
						echo "<div class='text_height_27'>" . $wptop . "About" . $wpmiddle . "" . $row[description] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_1])) {
						echo "<div class='text_height_28'>" . $wptop . "Area" . $wpmiddle . "" . $row[column_1] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_2])) {
						echo "<div class='text_height_29'>" . $wptop . "Opening" . $wpmiddle . "" . $row[column_2] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_3])) {
						echo "<div class='text_height_30'>" . $wptop . "Close Day" . $wpmiddle . "" . $row[column_3] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_4])) {
						echo "<div class='text_height_31'>" . $wptop . "Entry Fee" . $wpmiddle . "" . $row[column_4] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_5])) {
						echo "<div class='text_height_32'>" . $wptop . "Pakage" . $wpmiddle . "" . $row[column_5] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[offer])) {
						echo "<div class='text_height_33'>" . $wptop . "Offer" . $wpmiddle . "" . $row[offer] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_7])) {
						echo "<div class='text_height_34'>" . $wptop . "Travel Alert" . $wpmiddle . "" . $row[column_7] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_8])) {
						echo "<div class='text_height_34'>" . $wptop . "How to Go" . $wpmiddle . "" . $row[column_8] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_9])) {
						echo "<div class='text_height_35'>" . $wptop . "Distance Chart" . $wpmiddle . "" . $row[column_9] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[column_10])) {
						echo "<div class='text_height_36'>" . $wptop . "History" . $wpmiddle . "" . $row[column_10] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $wptop . "Contact" . $wpmiddle . "" . $contact . "" . $wpbottom;
					}

					if (!empty($row[web_site])) {
						echo "<div class='text_height_38'>" . $wptop . "Web Site" . $wpmiddle . "" . $row[web_site] . $wpbottom . " <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[social_links])) {
						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode("<br>", $social_links);
						} else if (strpos($social_links, "  ")) {
							$linkArray = explode("  ", $social_links);
						} else if (strpos($social_links, "   ")) {
							$linkArray = explode("   ", $social_links);
						} else {
							$linkArray = array($social_links);
						}

						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 10) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}

					if (!empty($row[main_location])) {
						//echo "<div class='text_height_40'>".$wptop."Main Location".$wpmiddle."".$row[main_location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}

					if (!empty($row[location])) {
						//echo "<div class='text_height_41'>".$wptop."Address".$wpmiddle."".$row[location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
					if (!empty($row[map])) {
						//echo "<div class='text_height_42'>".$wptop."Google Map".$wpmiddle."".$row[map].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
					}
				} //tourism

				else if ($cata == "ecommerce") {
					echo '<div class="share_option" id="expand_test">
									<div id="inside_expand">
										<div class="bookmark_bt for_padding">
											<a  href="#0" onclick="return bookmark(\'' . $_SESSION[content_id] . '\',\'' . $_SESSION[userid] . '\')">
											<i class="fas fa-heart"></i>
											<i class="far fa-heart"></i>
											</a>
										</div>';
					include 'share_site.php';
					echo '  </div>
									</div>';

					if (!empty($row[social_links])) {
						$social_links = $row[social_links];
						$social_links = str_replace("http", " http", $social_links);
						$social_links = str_replace("  ", " ", $social_links);
						$social_links = str_replace("   ", " ", $social_links);
						$social_links = str_replace("    ", " ", $social_links);
						if (strpos($social_links, ", ") !== false) {
							$linkArray = explode(", ", $social_links);
						} else if (strpos($social_links, ",")) {
							$linkArray = explode(",", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode(" ", $social_links);
						} else if (strpos($social_links, " ")) {
							$linkArray = explode("<br>", $social_links);
						} else if (strpos($social_links, "  ")) {
							$linkArray = explode("  ", $social_links);
						} else if (strpos($social_links, "   ")) {
							$linkArray = explode("   ", $social_links);
						} else {
							$linkArray = array($social_links);
						}

						$k = 0;
						while ($linkArray[$k]) {
							if (strpos($linkArray[$k], "facebook") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>facebook</a>";
							} else if (strpos($linkArray[$k], "twitter") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>twitter</a>";
							} else if (strpos($linkArray[$k], "google") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>google</a>";
							} else if (strpos($linkArray[$k], "instagram") !== false) {
								$linkArray[$k] = "<a class='social_a_tag' href='" . $linkArray[$k] . "' target='_blank'>instagram</a>";
							}

							if ($k == 10) {
								break;
							}

							$k++;

						}
						echo $nptop . "Social Links" . $npmiddle . "" . $linkArray[0] . "  " . $linkArray[1] . "  " . $linkArray[2] . " " . $linkArray[3] . " " . $linkArray[4] . " " . $linkArray[5] . " " . $linkArray[6] . " " . $linkArray[7] . " " . $linkArray[8] . " " . $linkArray[9] . " " . $npbottom;
					}

					if (!empty($row[type])) {
						echo $nptop . "Type" . $npmiddle . "" . $row[type] . "" . $npbottom;
					}

					$general_rate_obj = new general();
					echo $nptop . "Rating" . $npmiddle . "" . $general_rate_obj->ratingAll($total_rate, $rate_1, $rate_2, $rate_3) . "" . $npbottom;

					if (!empty($row[offer])) {
						echo $nptop . "Offer" . $npmiddle . "" . $row[offer] . "" . $npbottom;
					}

					if (!empty($row[web_site])) {
						$web_site_name_tmp = parse_url($row[web_site]);
						$web_site_name = $web_site_name_tmp['host'];
						echo $nptop . "Web Site" . $npmiddle . "<a target='_blank' href='" . $row[web_site] . "'>" . $web_site_name . "</a>" . $npbottom;
					}

					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo $nptop . "Contact" . $npmiddle . "" . $contact . "" . $npbottom;
					}
				} //ecommerce
			}
			//general_info while
		} //general_info if
	} // general_info function

	function general_description_ecommerce($cata, $sub_cata, $id) {

		include "data_connection.php";
		$nptop = '<div class="info-area">
				<div class="info-area-title">
					<span>';
		$npmiddle = '</span>
					</div>
					<div class="info-ans">
						<span class="info_span">';
		$npbottom = '</span>
						</div>
					</div>';

		$wptop = '<div class="info-area">
					<div class="info-area-title">
						<span>';
		$wpmiddle = '</span>
						</div>
						<div class="info-ans">
							<span class="info_span">';
		$wpbottom = '</span>
							</div>
						</div>';

		$fetch_g_data = "SELECT * FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

		$data = $conn->query($fetch_g_data);

		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {

				if ($cata == "ecommerce") {

					if (!empty($row[description])) {

						echo $nptop . "About" . $npmiddle . "" . $row[description] . "" . $npbottom;
					}
				}
			}
		}
	}
	function ratingSingle($value) {

		return '<div class="comment_review_rate">
			<ul class="star-rating">
				<li class="current-rating" id="current-rating" style="width:' . $value . '%"><!-- will show current rating --></li>
				<span id="ratelinks">
					<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
					<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
					<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
					<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
					<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
				</span>
			</ul>
		</div>';
	}
	// function ratingAll(){
	// 	return "RATINGGGGGGGGGGGGG";
	// }
	function ratingAll($total_rate, $rate_1, $rate_2, $rate_3) {
		if ($_SESSION[cata] == "food") {
			$rateTitle1 = "TASTE";
			$rateTitle2 = "PRICE";
			$rateTitle3 = "SERVICE";
		} else if ($_SESSION[cata] == "life style") {
			if ($_SESSION[sub_cata] == "fashion" || $_SESSION[sub_cata] == "accessorice") {
				$rateTitle1 = "QUALITY";
				$rateTitle2 = "SERVICE";
				$rateTitle3 = "PRICE";
			} else if ($_SESSION[sub_cata] == "parlor_salon" || $_SESSION[sub_cata] == "gym_fitness") {
				$rateTitle1 = "ENVIRONMENT";
				$rateTitle2 = "SERVICE";
				$rateTitle3 = "EXPENSE";
			}
		} else if ($_SESSION[cata] == "education") {
			$rateTitle1 = "FACULTY";
			$rateTitle2 = "FACILITIE";
			$rateTitle3 = "EXPENSE";
		} else if ($_SESSION[cata] == "health") {
			if ($_SESSION[sub_cata] == "hospital_clinic") {
				$rateTitle1 = "DOCTOR";
				$rateTitle2 = "FACILITIE";
				$rateTitle3 = "EXPENSE";
			}
		} else if ($_SESSION[cata] == "education") {
			if ($_SESSION[sub_cata] == "university" || $_SESSION[sub_cata] == "college" || $_SESSION[sub_cata] == "coaching") {
				$rateTitle1 = "FACULTY";
				$rateTitle2 = "FACILITIES";
				$rateTitle3 = "EXPENSE";
			}
		} else if ($_SESSION[cata] == "electronic") {
			$rateTitle1 = "DESIGN";
			$rateTitle2 = "PERFORMANCE";
			$rateTitle3 = "BATTERY LIFE";
		} else if ($_SESSION[cata] == "residence") {
			if ($_SESSION[sub_cata] != "to-let") {
				$rateTitle1 = "SERVICE";
				$rateTitle2 = "FACILITIE";
				$rateTitle3 = "EXPENSE";
			}
		} else if ($_SESSION[cata] == "ecommerce") {
			if ($_SESSION[sub_cata] != "buy_sell" || $_SESSION[sub_cata] != "others" || $_SESSION[sub_cata] != "job") {

				$rateTitle1 = "PRICE";
				$rateTitle2 = "SERVICE";
				$rateTitle3 = "RESPONSE";
			} else {
				$rateTitle1 = "SERVICE";
				$rateTitle2 = "PRICE";
				$rateTitle3 = "RESPONSE";
			}
		}

		$general_rate_obj = new general();

		return '
		<div class="content_review">
			<div class="comment_review_quality review_avg">

				<div class="comment_review_title ">
					<span>Overall</span>
				</div>
				' . $general_rate_obj->ratingSingle($total_rate) . '

			</div>


			<div class="comment_review_quality">
				<div class="comment_review_title">
					<span>' . $rateTitle1 . '</span>
				</div>
				' . $general_rate_obj->ratingSingle($rate_1) . '
			</div>

			<div class="comment_review_quality">
				<div class="comment_review_title">
					<span>' . $rateTitle2 . '</span>
				</div>
				' . $general_rate_obj->ratingSingle($rate_2) . '
			</div>

			<!-- style="margin-left:110px" -->
			<div class="comment_review_quality" >
				<div class="comment_review_title">
					<span>' . $rateTitle3 . '</span>
				</div>
				' . $general_rate_obj->ratingSingle($rate_3) . '
			</div>
		</div>
		';
	}

	function general_time_item_list($cata, $sub_cata, $id, $time_status) {
		include "data_connection.php";

		$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' AND item_time_status='$time_status';";

		$data = $conn->query($fetch_item_data);
		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {
				echo '
				<tr>
					<td> <img style="height:50px;" src="./image/item_image/' . $row[image_link] . '"> </td>
					<td class="item-name">' . $row[name] . '</td>
					<td class="item-name">' . $row[price] . ' </td>

					<td class="item-name">
						<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $row[id] . '"  data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $row[id] . ')">See Details</a>
					</td>
				</tr>
				';

			}
		} else {
			if ($time_status == "new") {
				echo '
				<center><h3 style="text_alignment:center;   font-family:Palatino Linotype;">No new item found</h3></center>
				';
			} else if ($time_status == "coming") {
				echo '
				<h3>No Item Found</h3>
				';
			}
		}

	}

	function general_map_location($cata, $sub_cata, $id) {

		$nptop = '<div class="info-area">
		<div class="info-area-title">
			<span>';
		$npmiddle = '</span>
			</div>
			<div class="info-ans">
				<span>';
		$npbottom = '</span>
				</div>
			</div>';

		include "data_connection.php";
		$fetch_map = "SELECT id,  location FROM `map` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

		$data = $conn->query($fetch_map);
		if ($data->num_rows > 0) {

			if ($cata == "residence" || $sub_cata == "natural_site" || $sub_cata == "heritage" || $sub_cata == "lake_park") {
				echo $nptop . "Location " . $npmiddle;
			} else {
				echo $nptop . "Branch " . $npmiddle;
			}
			$i = 0;
			while ($row = $data->fetch_assoc()) {

				if ($i != 0) {
					echo "-";
				}
				echo '
					<a href="#" onclick="return getMap(\'' . $row[id] . '\' )">' . $row[location] . ' </a>
					';
				$i++;
			}
			echo $npbottom;
		}
	}

	function general_map($cata, $sub_cata, $id) {
		include "data_connection.php";
		$fetch_map = "SELECT * FROM `map`  WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' LIMIT 1;";

		$data = $conn->query($fetch_map);

		if ($data->num_rows > 0) {

			while ($row = $data->fetch_assoc()) {

				if (!empty($row[map]) || !empty($row[address])) {

					echo '<div ID="branch_map">';
					if (!empty($row[address])) {
						echo '<div class="branch_info"><span class="branch_address"><strong>Full Address <i> ' . $row[location] . '</i>: </strong>' . $row[address] . '</span></div><br>';
					}
					if (!empty($row[contact])) {
						$contact = $row[contact];

						$contact = str_replace("<br>", "", $contact);
						$contact = str_replace("Email", "<b>Email</b>", $contact);
						$contact = str_replace("email", "<b>Email</b>", $contact);

						$contact = str_replace("Phone", "<b>Phone</b>", $contact);
						$contact = str_replace("phone", "<b>Phone</b>", $contact);

						$contact = str_replace("Mobile", "<b>Mobile</b>", $contact);
						$contact = str_replace("mobile", "<b>Mobile</b>", $contact);

						$contact = str_replace("Tele", "<b>Tele</b>", $contact);
						$contact = str_replace("tele", "<b>Tele</b>", $contact);
						$contact = str_replace("Telephone", "<b>Telephone</b>", $contact);

						$contact = str_replace("Fax", "<b><Fax/b>", $contact);

						$contact = str_replace("<b>", "<br><b>", $contact);

						echo '<div class="branch_info"><span class="branch_contact"><strong>Contact: </strong>' . $contact . '</span></div><br>';
					}
					if (!empty($row[map])) {
						echo '<div class="info-area branch_map">
							<span class="location_span">' . $row[location] . ' </span>
							' . $row[map] . '
						</div>
						';
					}
					echo '</div>';
				}

			}
		}
	}

	function filter_list($cata, $sub_cata, $id) {
		include 'data_connection.php';
		// echo $cata."<>".$sub_cata."<>".$id;
		// $tmp='<li><a data-toggle="tab" href="#" class="combo_but" >'.$cata.'</a></li>';
		//     $tmp = $tmp.'<li><a data-toggle="tab" href="#" class="combo_but" >'.$sub_cata.'</a></li>';

		//     return $tmp;

		$sql = "SELECT filter_1 from `item_list` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' GROUP BY filter_1;";
		$result = $conn->query($sql);
		$tmp = "";
		$tmp = $tmp . '<li class=""><a data-toggle="tab" href="#" class="food_filter_list_but food_filter_list_all" >ALL</a></li>';
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				$f_class = strtolower($row[filter_1]);
				$f_class = str_replace(" ", "_", $f_class);
				$f_class = str_replace("&", "", $f_class);

				if ($row[filter_1] == " " || $row[filter_1] == "") {

				} else if ($row[filter_1] == "---") {
					$tmp = $tmp . '<li class=""><a data-toggle="tab" href="#" class="food_filter_list_but ' . $f_class . '_but" >OTHERS</a></li>';
				} else {
					$tmp = $tmp . '<li class=""><a data-toggle="tab" href="#" class="food_filter_list_but ' . $f_class . '_but" >' . strtoupper($row[filter_1]) . '</a></li>';
				}
			}
		}

		return $tmp;
	}

	function elecFilterList($cata, $sub_cata, $id) {
		include 'data_connection.php';
		$sql = "SELECT what_item from `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' GROUP BY what_item;";
		$result = $conn->query($sql);
		$tmp = "";
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				$f_class = strtolower($row[filter_1]);
				$f_class = str_replace(" ", "_", $f_class);
				$tmp = $tmp . '<a href="#0" onclick="elecProFilter(\'' . $row[what_item] . '\', \'' . $cata . '\', \'' . $sub_cata . '\', \'' . $id . '\')">' . $row[what_item] . '</a>';
			}
		}

		echo $tmp;
	}

	function tourismMapAndVideo($cata, $sub_cata, $id) {
		include 'data_connection.php';
		$fetch_g_data = "SELECT `column_12`, `column_13`, `column_14` FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

		$data = $conn->query($fetch_g_data);

		if ($data->num_rows > 0) {
			while ($row = $data->fetch_assoc()) {
				if (!empty($row[column_12])) {
					echo '
					<div class=" spot-in-map">
						<img src="./image/main_image/' . $row[column_12] . '" class="img-responsive" alt="MAP OF BANGLADESH">

					</div>';
				}

				if (!empty($row[column_13])) {
					echo '
					<div class=" spot-in-map">
						<img src="./image/main_image/' . $row[column_13] . '" class="img-responsive" alt="MAP OF BANGLADESH">

					</div>';
				}

				if (!empty($row[column_14])) {
					echo '
					<div class="spot-in-map">
						' . $row[column_14] . '
					</div>
					';
				}
			}
		}

	}

	function general_item_list2($cata, $sub_cata, $id, $table_pattern_top, $table_pattern_bottom, $life_style_menu, $elecValue) {
		include "data_connection.php";
		if ($cata == "electronics") {
			if (isset($elecValue)) {
				$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata' AND what_item = '$elecValue';";
			} else {
				$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata' ;";
			}
		} else if ($cata == "food") {
			$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata'  ORDER BY filter_1;";
		} else {
			$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata';";
		}
		$data = $conn->query($fetch_item_data);
		$item_number = 1;
		$ac = 0;
		if ($data->num_rows > 0 && $cata == "tourism") {
			while ($row = $data->fetch_assoc()) {
				// echo '<div class="attraction_box col-sm-2">
				// 				<img class="img-responsive"  src="./image/item_image/'.$row[image_link].'">
				// 				<div class="attraction_info">
				// 					<span><u>'.$row[name].'</u></span>
				// 					<p>'.$row[description].'</p>
				// 				</div>
				// 			</div>';

				if ($ac == 0) {
					echo '
					<div class="item active">
						<div class="attraction_box col-sm-2">
							<img class="img-responsive"  src="./image/item_image/' . $row[image_link] . '">
							<div class="attraction_info">
								<span><u>' . $row[name] . '</u></span>
								<p>' . $row[description] . '</p>
							</div>
						</div>
					</div>
					';
					$ac = $ac + 1;
				} else {
					echo '
					<div class="item">
						<div class="attraction_box col-sm-2">
							<img class="img-responsive"  src="./image/item_image/' . $row[image_link] . '">
							<div class="attraction_info">
								<span><u>' . $row[name] . '</u></span>
								<p>' . $row[description] . '</p>
							</div>
						</div>
					</div>
					';
				}
			}
		}
	} //item_list_function

} //class general

function getLocationList() {
	include "data_connection.php";
	$getLocation = "SELECT location, COUNT(*) AS numOfLoc FROM `map` GROUP BY location HAVING numOfLoc > 5 ORDER BY location ASC";

	$data = $conn->query($getLocation);

	// $value = '<option></option><option style="color:red; font-weight: bold;" >Popular Places</option>';
	if ($data->num_rows > 0) {

		while ($row = $data->fetch_assoc()) {

			if ($row[location] != '') {
				$location = $row[location];
				$value = '<option value="' . $location . '">' . $location . '</option>';
			}
		}

		echo '<optgroup label="Popular Place">';
		echo $value;
		echo '<optgroup>';
	}

	$getLocation = "SELECT location, COUNT(*) AS numOfLoc FROM `map` GROUP BY location HAVING numOfLoc < 6 ORDER BY location ASC";

	$data = $conn->query($getLocation);

	// $value = $value.'<option></option><option style="color:red; font-weight: bold;" >Other Places</option>';
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {

			if ($row[location] != '') {
				$location = $row[location];
				$value = $value . '<option value="' . $location . '">' . $location . '</option>';
			}
		}
	}

	echo '<optgroup label="Other Place">';
	echo $value;
	echo '<optgroup>';
}

?>