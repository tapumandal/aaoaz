-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2016 at 05:30 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `review-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, 'Tapu Mandal', '123', '123', '01739995117', '', '', '', '', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(255) NOT NULL,
  `question_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `answer` varchar(600) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE `bookmark` (
  `id` int(200) NOT NULL,
  `user_id` int(200) NOT NULL,
  `business_pro_id` int(200) NOT NULL,
  `item_id` int(200) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmark`
--

INSERT INTO `bookmark` (`id`, `user_id`, `business_pro_id`, `item_id`, `date`) VALUES
(1, 0, 0, NULL, '2016-07-31'),
(2, 1, 1, NULL, '2016-07-31'),
(3, 2, 1, NULL, '2016-07-31');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `text` varchar(600) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `avg` double NOT NULL,
  `time` time NOT NULL,
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `cata_id`, `cata`, `sub_cata`, `item_id`, `user_id`, `user_type`, `text`, `type`, `rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`, `status`) VALUES
(1, 1, 'food', 'restaurant', 5, '', 'user', 'average', 'rate', 1, 3, 5, 3, '04:39:18', '2016-07-31', ''),
(2, 1, 'food', 'restaurant', 0, '', 'user', 'dggssfdgsdfsdf', 'comment', 0, 0, 0, 0, '06:03:52', '2016-07-31', ''),
(3, 1, 'food', 'restaurant', 6, '1', 'user', 'good', 'rate', 4, 3, 4, 3.6666666666667, '07:36:47', '2016-07-31', ''),
(4, 1, 'food', 'restaurant', 5, '2', 'user', 'very bad', 'rate', 1, 2, 1, 1.3333333333333, '14:39:08', '2016-07-31', ''),
(5, 1, 'food', 'restaurant', 4, '3', 'user', 'very good', 'rate', 5, 4, 4, 4.3333333333333, '00:52:11', '2016-08-05', ''),
(6, 5, 'electronics', 'computer, mobile, camera', 6, '4', 'user', 'check electronic rate', 'rate', 3, 4, 5, 4, '08:29:33', '2016-08-09', ''),
(7, 1, 'food', 'restaurant', 5, '4', 'user', 'error check', 'rate', 5, 4, 5, 4.6666666666667, '08:30:54', '2016-08-09', ''),
(8, 2, 'life style', 'fashion', 0, '4', 'user', 'all rate check', 'rate', 3, 5, 5, 4.3333333333333, '09:40:51', '2016-08-09', ''),
(9, 2, 'life style', 'fashion', 0, '4', 'user', 'fdgdgdfgdfg', 'rate', 5, 5, 5, 5, '09:41:50', '2016-08-09', ''),
(10, 2, 'life style', 'fashion', 0, '4', 'user', 'ryfghfghf', 'rate', 5, 1, 1, 2.3333333333333, '09:43:12', '2016-08-09', ''),
(11, 2, 'life style', 'fashion', 0, '4', 'user', 'ccccccccc', 'rate', 1, 3, 5, 3, '10:07:14', '2016-08-09', ''),
(12, 11, 'life style', 'gym_fitness', 0, '1', 'user', 'average', 'rate', 3, 4, 2, 3, '04:40:50', '2016-08-10', ''),
(13, 3, 'education', 'university', 0, '1', 'user', 'very good', 'rate', 4, 5, 5, 4.6666666666667, '06:23:47', '2016-08-10', ''),
(14, 1, 'food', 'restaurant', 0, '1', 'user', 'good', 'rate', 5, 4, 5, 4.6666666666667, '07:39:09', '2016-08-10', ''),
(15, 1, 'food', 'restaurant', 0, '1', 'user', 'undefined', 'rate', 5, 5, 5, 5, '07:39:54', '2016-08-10', ''),
(16, 1, 'food', 'restaurant', 0, '1', 'user', 'jjj', 'rate', 1, 1, 1, 1, '07:40:24', '2016-08-10', ''),
(17, 1, 'food', 'restaurant', 0, '1', 'user', 'trtrdtdtd', 'rate', 2, 2, 2, 2, '07:40:50', '2016-08-10', '');

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE `crop` (
  `left` int(50) NOT NULL,
  `top` int(50) NOT NULL,
  `width` int(50) NOT NULL,
  `height` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doctor_faculty`
--

CREATE TABLE `doctor_faculty` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `schedule` text NOT NULL,
  `day` varchar(150) NOT NULL,
  `room` varchar(100) NOT NULL,
  `visite` varchar(50) NOT NULL,
  `depertment` varchar(50) NOT NULL,
  `extra_activity` varchar(200) NOT NULL,
  `education` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `link` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_faculty`
--

INSERT INTO `doctor_faculty` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `designation`, `schedule`, `day`, `room`, `visite`, `depertment`, `extra_activity`, `education`, `email`, `mobile`, `link`, `description`, `image_link`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'education', 3, 'university', 'First', 'DH', '', '', '', '', 'cse', '', 'Edu', 'Email', 'Phone', '', '', '1_3_education_university.jpg', '2016-07-31', '17:04:51', 1, ''),
(2, 'education', 3, 'university', 'Second', 'Ass prof.', '', '', '', '', 'civil', '', 'Edu2', 'Email2', '017', '', '', '2_3_education_university.jpg', '2016-07-31', '17:04:51', 1, ''),
(3, 'health', 4, 'hospital', 'Doc name', 'Dept Head', '9-12 3rd Floor', '', '', '', 'discharge_lounge', '', 'E.S.C', 'email', '01478', '', '', '3_4_health_hospital.jpg', '2016-07-31', '17:15:54', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `elec_item_details`
--

CREATE TABLE `elec_item_details` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `what_item` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `model` varchar(100) NOT NULL,
  `item_time_status` varchar(50) NOT NULL,
  `column_1` varchar(500) NOT NULL,
  `column_2` varchar(500) NOT NULL,
  `column_3` varchar(500) NOT NULL,
  `column_4` varchar(500) NOT NULL,
  `column_5` varchar(500) NOT NULL,
  `column_6` varchar(500) NOT NULL,
  `column_7` varchar(500) NOT NULL,
  `column_8` varchar(500) NOT NULL,
  `column_9` varchar(500) NOT NULL,
  `column_10` varchar(500) NOT NULL,
  `column_11` varchar(500) NOT NULL,
  `column_12` varchar(500) NOT NULL,
  `column_13` varchar(500) NOT NULL,
  `column_14` varchar(500) NOT NULL,
  `column_15` varchar(500) NOT NULL,
  `column_16` varchar(500) NOT NULL,
  `column_17` varchar(500) NOT NULL,
  `column_18` varchar(500) NOT NULL,
  `column_19` varchar(500) NOT NULL,
  `column_20` varchar(500) NOT NULL,
  `column_21` varchar(500) NOT NULL,
  `column_22` varchar(500) NOT NULL,
  `column_23` varchar(500) NOT NULL,
  `column_24` varchar(500) NOT NULL,
  `column_25` varchar(500) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `elec_item_details`
--

INSERT INTO `elec_item_details` (`id`, `cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`, `image_link`, `price`, `model`, `item_time_status`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `column_16`, `column_17`, `column_18`, `column_19`, `column_20`, `column_21`, `column_22`, `column_23`, `column_24`, `column_25`, `total_rate`, `rate_1`, `rate_2`, `rate_3`, `rate_people`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'electronics', 5, 'computer, mobile, camera', 'mobile', 'P16', 'Description', '1_5_electronics_computer, mobile, camera.jpg', 3000, '', 'running', 'Nano', 'yes', 'Usb', 'Ear jack', 'MHL', 'Yes', '3.2', 'OS', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-07-31', '20:38:07', 1, ''),
(2, 'electronics', 5, 'computer, mobile, camera', 'laptop', 'G-p1578', 'Description Description Description Description Description ', '2_5_electronics_computer, mobile, camera.jpg', 6000, '', 'coming', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-07-31', '20:38:07', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_image`
--

CREATE TABLE `general_image` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `image_link` varchar(50) NOT NULL,
  `image_des` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_image`
--

INSERT INTO `general_image` (`id`, `cata_id`, `cata`, `sub_cata`, `image_link`, `image_des`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 1, 'food', 'restaurant', '1_food_1_restaurant.jpg', '', '2016-07-30', '15:53:42', 1, ''),
(3, 2, 'life style', 'fashion', '3_life style_2_fashion.jpg', '', '2016-07-31', '14:48:44', 1, ''),
(4, 3, 'education', 'university', '4_education_3_university.jpg', '', '2016-07-31', '17:04:51', 1, ''),
(5, 3, 'education', 'university', '5_education_3_university.jpg', '', '2016-07-31', '17:04:51', 1, ''),
(6, 4, 'health', 'hospital_clinic', '6_health_4_hospital.jpg', '', '2016-07-31', '17:24:06', 1, ''),
(7, 5, 'electronics', 'computer, mobile, camera', '7_electronics_5_computer, mobile, camera.jpg', '', '2016-07-31', '20:38:07', 1, ''),
(8, 7, 'residence', 'hotel', '8_residence_7_hotel.jpg', '', '2016-07-31', '23:30:06', 1, ''),
(9, 8, 'tourism', 'natural_site', '9_tourism_8_tourism place.jpg', '', '2016-08-01', '05:20:59', 1, ''),
(10, 11, 'life style', 'gym_fitness', '10_life style_11_fitness.jpg', '', '2016-08-05', '17:48:05', 1, ''),
(11, 11, 'life style', 'gym_fitness', '11_life style_11_fitness.jpg', '', '2016-08-05', '17:48:05', 1, ''),
(12, 11, 'life style', 'gym_fitness', '12_life style_11_fitness.jpg', '', '2016-08-05', '17:48:05', 1, ''),
(13, 12, 'life style', 'gym_fitness', '13_life style_12_fitness.jpg', '', '2016-08-05', '19:38:24', 1, ''),
(14, 13, 'tourism', 'natural_site', '14_tourism_13_tourism place.jpg', '', '2016-08-07', '06:22:02', 1, ''),
(15, 14, 'education', 'admission', '15_education_14_admission.jpg', '', '2016-08-10', '07:16:49', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_info`
--

CREATE TABLE `general_info` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `type` varchar(250) NOT NULL,
  `column_1` varchar(1500) NOT NULL,
  `column_2` varchar(1500) NOT NULL,
  `column_3` varchar(1500) NOT NULL,
  `column_4` varchar(1500) NOT NULL,
  `column_5` varchar(1500) NOT NULL,
  `column_6` varchar(1500) NOT NULL,
  `column_7` varchar(1500) NOT NULL,
  `column_8` varchar(1500) NOT NULL,
  `column_9` varchar(1500) NOT NULL,
  `column_10` varchar(1500) NOT NULL,
  `column_11` varchar(1500) NOT NULL,
  `column_12` varchar(1500) NOT NULL,
  `column_13` varchar(1500) NOT NULL,
  `column_14` varchar(1500) NOT NULL,
  `column_15` varchar(1500) NOT NULL,
  `column_16` varchar(1500) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `social_links` varchar(500) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(1500) NOT NULL,
  `total_avg_review` float NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `total_view` int(255) NOT NULL,
  `daily_view` int(250) NOT NULL,
  `weekly_view` int(250) NOT NULL,
  `monthly_view` int(250) NOT NULL,
  `yearly_view` int(250) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_info`
--

INSERT INTO `general_info` (`id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `type`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `column_16`, `branches`, `web_site`, `social_links`, `main_location`, `location`, `map`, `total_avg_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `total_view`, `daily_view`, `weekly_view`, `monthly_view`, `yearly_view`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'restaurant', 'food', 'Food restaurant', '1_food_restaurant.jpg', 'Description', 'chainees', 'Chainees | Indian', 'Speciality', 'Offer', 'Opening ', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.com', '', '', '', '', 12.6667, 13, 12, 13, 4, 186, 186, 186, 186, 186, '2016-07-30', '15:50:42', 1, ''),
(2, 'fashion', 'life style', 'Fashion House', '2_life style_fashion.jpg', 'Description', 'Male and Female Clothing', '', 'Speciality', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 14.3333, 14, 14, 16, 4, 148, 148, 148, 148, 148, '2016-07-31', '14:47:05', 1, ''),
(3, 'university', 'education', 'university', '3_education_university.jpg', 'Description', '', 'Department list', 'Information', 'Heighlight', 'Admission', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'google', 4.66667, 4, 5, 5, 1, 36, 36, 36, 36, 36, '2016-07-31', '17:04:51', 1, ''),
(4, 'hospital_clinic', 'health', 'Health', '4_health_hospital.jpg', 'Description', '', '', 'Facilities ', 'Ambulance', 'Opening Hour', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Array', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, 22, 22, 22, 22, 22, '2016-07-31', '17:15:54', 1, ''),
(5, 'computer, mobile, camera', 'electronics', 'Electronics', '5_electronics_computer, mobile, camera.jpg', 'Description', 'Computer | Laptop | Mobile | Cameara', '', 'Service', 'Warrenty Policy', 'Servcing Center', '+88017989525858', '', '', '', '', '', '', '', '', '', '', '', '', 'www.website.com', '', '', '', '', 0, 0, 0, 0, 0, 173, 173, 173, 173, 173, '2016-07-31', '20:38:07', 1, ''),
(7, 'hotel', 'residence', 'Hotel', '7_residence_hotel.jpg', '', 'Type', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Array', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, 62, 62, 62, 62, 62, '2016-07-31', '23:24:10', 1, ''),
(8, 'natural_site', 'tourism', 'Tourism Place', '8_tourism_tourism place.jpg', 'Description', '', '121 Kilomeater', '', '', '', '', '', '', '', '', '', '', '8_tourism_1.jpg', '8_tourism_2.jpg', '<iframe width="560" height="315" src="https://www.youtube.com/embed/BKiUKSbXaiQ" frameborder="0" allowfullscreen></iframe>', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 53, 53, 53, 53, 53, '2016-08-01', '05:20:59', 1, ''),
(11, 'gym_fitness', 'life style', 'Fitness Check', '11_life style_fitness.jpg', 'Description', 'Health', '', 'Speciality', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 3, 3, 4, 2, 1, 63, 63, 63, 63, 63, '2016-08-05', '17:48:05', 1, ''),
(12, 'gym_fitness', 'life style', 'Fitness Again', '12_life style_fitness.jpg', '', 'Tyepo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 24, 24, 24, 24, 24, '2016-08-05', '19:38:24', 1, ''),
(13, 'natural_site', 'tourism', 'Second', '13_tourism_tourism place.jpg', 'Description 2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 16, 16, 16, 16, 16, '2016-08-07', '06:22:01', 1, ''),
(14, 'admission', 'education', 'Dhaka University Admission 2017', '14_education_admission.jpg', 'Short description with details', '', '', 'Few Information', 'If something to heighlight', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 14, 14, 14, 14, 14, '2016-08-10', '07:16:48', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `item_des`
--

CREATE TABLE `item_des` (
  `id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(80) NOT NULL,
  `image_des` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_list`
--

CREATE TABLE `item_list` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` varchar(300) NOT NULL,
  `color` varchar(150) NOT NULL,
  `size` varchar(30) NOT NULL,
  `model` varchar(100) NOT NULL,
  `price_2` varchar(50) NOT NULL,
  `column_1` varchar(200) NOT NULL,
  `column_2` varchar(200) NOT NULL,
  `column_3` varchar(200) NOT NULL,
  `column_4` varchar(255) NOT NULL,
  `filter_1` varchar(40) NOT NULL,
  `filter_2` varchar(40) NOT NULL,
  `filter_3` varchar(100) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_list`
--

INSERT INTO `item_list` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `description`, `image_link`, `price`, `color`, `size`, `model`, `price_2`, `column_1`, `column_2`, `column_3`, `column_4`, `filter_1`, `filter_2`, `filter_3`, `total_rate`, `rate_1`, `rate_2`, `rate_3`, `rate_people`, `date`, `time`, `admin_id`, `status`) VALUES
(4, 'food', 1, 'restaurant', 'first', '', '4_1_food_restaurant.jpg', '900', '', '', '', '', '', '', '', '', 'Soup', '', '', 13, 5, 4, 4, 1, '2016-07-31', '00:24:40', 1, ''),
(5, 'food', 1, 'restaurant', 'secobnd', '', '5_1_food_restaurant.jpg', '1234', '', '', '', '', '', '', '', '', 'Unknown', '', '', 27, 7, 9, 11, 3, '2016-07-31', '01:17:15', 1, ''),
(6, 'food', 1, 'restaurant', 'third', '', '6_1_food_restaurant.jpg', '987654', '', '', '', '', '', '', '', '', 'Desert', '', '', 23, 7, 7, 9, 2, '2016-07-31', '04:36:37', 1, ''),
(7, 'life style', 2, 'fashion', 'First', 'Des', '7_2_life style_fashion.jpg', '852', 'BLACk', 'Large', '', '', '', '', '', '', 'female', 'Gown', '', 0, 0, 0, 0, 0, '2016-07-31', '14:47:06', 1, ''),
(8, 'residence', 7, 'hotel', 'Name', 'description', '8_7_residence_hotel.jpg', '', '', '', '', '', 'Type', 'sea view', '', '', 'room', '', '', 0, 0, 0, 0, 0, '2016-08-01', '00:17:37', 1, ''),
(9, 'residence', 7, 'hotel', 'old', '', '9_7_residence_hotel.jpg', '', '', '', '', '', '', '', 'full faci', '', 'room', '', '', 0, 0, 0, 0, 0, '2016-08-01', '00:29:37', 1, ''),
(10, 'residence', 7, 'hotel', 'name', 'desc', '10_7_residence_hotel.jpg', 'rate bdt', '', '', '', 'usd', 'bed type', 'view', 'facilities', '', 'room', '', '', 0, 0, 0, 0, 0, '2016-08-01', '04:04:12', 1, ''),
(11, 'residence', 7, 'hotel', 'new name', 'Des', '11_7_residence_hotel', '', '', '', '', '', '', '', '', '', 'service', '', '', 0, 0, 0, 0, 0, '2016-08-01', '04:04:12', 1, ''),
(12, 'residence', 7, 'hotel', 'ddd', 'uyjgkjyfkifkjhfgiuyfkjhbvklugiuytou', '12_7_residence_hotel.jpg', '', '', '', '', '', '', '', '', '', 'service', '', '', 0, 0, 0, 0, 0, '2016-08-01', '04:25:04', 1, ''),
(13, 'tourism', 8, 'natural_site', 'First Spot', 'Desssssssssss', '13_8_tourism_tourism place.jpg', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-08-01', '05:21:00', 1, ''),
(14, 'life style', 11, 'gym_fitness', 'Trainer (Female)', 'Name, experience, etcccccc', '14_11_life style_fitness.jpg', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-08-05', '17:48:05', 1, ''),
(15, 'life style', 11, 'gym_fitness', 'Facilities 1', 'Description Description Description Description Description Description Description ', '15_11_life style_fitness.jpg', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-08-05', '17:48:06', 1, ''),
(16, 'life style', 12, 'gym_fitness', 'adasdasd', 'asdasdad', '16_12_life style_fitness.jpg', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-08-05', '19:38:24', 1, ''),
(17, 'life style', 2, 'gym_fashion', 'Second', 'Des', '17_2_life style_fashion.jpg', '8975', 'Red', 'Medium', '', '', '', '', '', '', 'kids', 'New Born', '', 0, 0, 0, 0, 0, '2016-08-08', '06:31:04', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `item_multi_image`
--

CREATE TABLE `item_multi_image` (
  `id` int(50) NOT NULL,
  `cata` varchar(100) NOT NULL,
  `item_id` int(50) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `short_note` varchar(1200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_multi_image`
--

INSERT INTO `item_multi_image` (`id`, `cata`, `item_id`, `image_link`, `short_note`) VALUES
(12, '', 4, '0_4_1_food_restaurant.jpg', ''),
(15, '', 4, '13_4_1_food_restaurant.jpg', ''),
(16, '', 5, '0_5_1_food_restaurant.jpg', ''),
(17, '', 5, '1_5_1_food_restaurant.jpg', ''),
(18, '', 5, '18_5_1_food_restaurant.jpg', ''),
(19, '', 5, '19_5_1_food_restaurant.jpg', ''),
(20, '', 6, '20_6_1_food_restaurant.jpg', ''),
(21, '', 6, '21_6_1_food_restaurant.jpg', ''),
(22, '', 7, '22_7_2_life style_fashion.jpg', ''),
(23, '', 9, '23_9_7_residence_hotel.jpg', ''),
(24, '', 1, '24_1_5_electronics_computer, mobile, camera.jpg', ''),
(25, '', 8, '25_8_7_residence_hotel.jpg', ''),
(26, '', 9, '26_9_7_residence_hotel.jpg', ''),
(27, '', 13, '27_13_8_tourism_tourism place.jpg', ''),
(28, '', 14, '28_14_11_life style_fitness.jpg', ''),
(29, '', 16, '29_16_12_life style_fitness.jpg', ''),
(30, '', 17, '30_17_2_life style_fashion.jpg', ''),
(31, '', 17, '31_17_2_life style_fashion.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE `map` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `map` varchar(600) NOT NULL,
  `date` date NOT NULL,
  `admin_id` int(255) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `cata_id`, `cata`, `sub_cata`, `location`, `address`, `map`, `date`, `admin_id`, `status`) VALUES
(1, 1, 'food', 'restaurant', 'Dhanmondi', 'Dhanmondi, Jigatola.', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14608.267806000724!2d90.3701279!3d23.7449918!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1469927347689" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-07-31', 1, ''),
(2, 1, 'food', 'restaurant', 'Uttara', 'Sector:8, Road: 13', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1824.2458419797267!2d90.39129769217095!3d23.872177520020216!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1469927399864" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-07-31', 1, ''),
(3, 3, 'education', 'university', 'Mirpur', 'Mirpur, 1no', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1824.2458419797267!2d90.39129769217095!3d23.872177520020216!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1469927399864" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-07-31', 1, ''),
(4, 4, 'health', 'hospital_clinic', 'Banani', 'Square', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1824.2458419797267!2d90.39129769217095!3d23.872177520020216!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1469927399864" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-07-31', 1, ''),
(5, 7, 'residence', 'hotel', 'Singapor', 'Singapor City', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d255279.05060788017!2d103.81221661445313!3d1.3351071417562883!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da11238a8b9375%3A0x887869cf52abf5c4!2sSingapore!5e0!3m2!1sen!2sbd!4v1469985821717" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-07-31', 1, ''),
(6, 7, 'residence', 'hotel', 'Location 2', 'Location 22222 address', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14608.267806000724!2d90.3701279!3d23.7449918!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1470416041408" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-08-05', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(255) NOT NULL,
  `main_info_id` int(50) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `user_id` varchar(60) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `question` varchar(300) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(255) NOT NULL,
  `comment_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `reply` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search_suggestion`
--

CREATE TABLE `search_suggestion` (
  `id` int(255) NOT NULL,
  `text_s` varchar(500) NOT NULL,
  `s_text_type` varchar(50) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `cata_item_id` int(120) NOT NULL,
  `count` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search_suggestion`
--

INSERT INTO `search_suggestion` (`id`, `text_s`, `s_text_type`, `cata`, `sub_cata`, `cata_item_id`, `count`) VALUES
(1, 'Food restaurant', 'business_profile', 'food', 'restaurant', 1, 0),
(2, 'first item', 'item', 'food', 'restaurant', 1, 0),
(3, 'second', 'item', 'food', 'restaurant', 2, 0),
(4, 'new item', 'item', 'food', 'restaurant', 3, 0),
(5, 'first', 'item', 'food', 'restaurant', 4, 0),
(6, 'secobnd', 'item', 'food', 'restaurant', 5, 0),
(7, 'third', 'item', 'food', 'restaurant', 6, 0),
(8, 'First', 'item', 'life style', 'fashion', 7, 0),
(9, 'Fashion House', 'business_profile', 'life style', 'fashion', 2, 0),
(10, 'First', 'item_doc_fac', 'education', 'university', 1, 0),
(11, 'Second', 'item_doc_fac', 'education', 'university', 2, 0),
(12, 'university', 'business_profile', 'education', 'university', 3, 0),
(13, 'Doc name', 'item_doc_fac', 'health', 'hospital', 3, 0),
(14, 'Health', 'business_profile', 'health', 'hospital', 4, 0),
(15, 'Electronics', 'business_profile', 'electronics', 'computer, mobile, camera', 5, 0),
(16, '', 'business_profile', 'electronics', '', 6, 0),
(17, 'Hotel', 'business_profile', 'residence', 'hotel', 7, 0),
(18, 'Name', 'item', 'residence', 'hotel', 8, 0),
(19, 'NEW', 'item', 'residence', 'hotel', 9, 0),
(20, 'name', 'item', 'residence', 'hotel', 10, 0),
(21, 'facilities name', 'item', 'residence', 'hotel', 11, 0),
(22, 'ddd', 'item', 'residence', 'hotel', 12, 0),
(23, 'First Spot', 'item', 'tourism', 'tourism place', 13, 0),
(24, 'Tourism Place', 'business_profile', 'tourism', 'tourism place', 8, 0),
(25, '', 'business_profile', 'life style', 'fitness', 9, 0),
(26, 'Trainer (Female)', 'item', 'life style', 'fitness', 14, 0),
(27, 'Facilities 1', 'item', 'life style', 'fitness', 15, 0),
(28, 'Fitness Check', 'business_profile', 'life style', 'fitness', 11, 0),
(29, 'adasdasd', 'item', 'life style', 'fitness', 16, 0),
(30, 'Fitness Again', 'business_profile', 'life style', 'fitness', 12, 0),
(31, 'Second', 'business_profile', 'tourism', 'tourism place', 13, 0),
(32, 'Second', 'item', 'life style', 'fashion', 17, 0),
(33, 'Dhaka University Admission 2017', 'business_profile', 'education', 'admission', 14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `set_view`
--

CREATE TABLE `set_view` (
  `setDailyView` varchar(200) NOT NULL,
  `setWeeklyView` varchar(200) NOT NULL,
  `setMonthlyView` varchar(200) NOT NULL,
  `setYearlyView` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE `social_link` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `link_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tourism_nearby`
--

CREATE TABLE `tourism_nearby` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `hotel_id` varchar(100) NOT NULL,
  `restaurant_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tourism_nearby`
--

INSERT INTO `tourism_nearby` (`id`, `cata_id`, `hotel_id`, `restaurant_id`) VALUES
(126, 13, '3', '1'),
(127, 13, '1', ''),
(128, 13, '', ''),
(129, 13, '', ''),
(130, 13, '', ''),
(131, 8, '', '1'),
(132, 8, '', ''),
(133, 8, '', ''),
(134, 8, '', ''),
(135, 8, '', ''),
(136, 14, '', ''),
(137, 14, '', ''),
(138, 14, '', ''),
(139, 14, '', ''),
(140, 14, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `date`, `time`, `status`) VALUES
(1, 'Tapu Mandal', '456', '456', '', '', 'user', '1_.jpg', '2016-07-31', '07:36:21', 'enable'),
(2, 'Mandal', '789', '789', '', '', 'user', '2_.jpg', '2016-07-31', '14:38:29', 'enable'),
(3, 'alrazib', '555', '555', '', '', 'user', '3_.jpg', '2016-08-05', '00:51:15', 'enable'),
(4, 'New User', '999', '999', '', '', 'user', '', '2016-08-09', '08:29:10', 'enable');

-- --------------------------------------------------------

--
-- Table structure for table `user_upload_image`
--

CREATE TABLE `user_upload_image` (
  `id` int(255) NOT NULL,
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elec_item_details`
--
ALTER TABLE `elec_item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_image`
--
ALTER TABLE `general_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_info`
--
ALTER TABLE `general_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `item_des`
--
ALTER TABLE `item_des`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_list`
--
ALTER TABLE `item_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_multi_image`
--
ALTER TABLE `item_multi_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map`
--
ALTER TABLE `map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search_suggestion`
--
ALTER TABLE `search_suggestion`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tourism_nearby`
--
ALTER TABLE `tourism_nearby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `elec_item_details`
--
ALTER TABLE `elec_item_details`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `general_image`
--
ALTER TABLE `general_image`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `general_info`
--
ALTER TABLE `general_info`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `item_des`
--
ALTER TABLE `item_des`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_list`
--
ALTER TABLE `item_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `item_multi_image`
--
ALTER TABLE `item_multi_image`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `search_suggestion`
--
ALTER TABLE `search_suggestion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tourism_nearby`
--
ALTER TABLE `tourism_nearby`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
