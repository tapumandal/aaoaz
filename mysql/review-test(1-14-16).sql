-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2016 at 09:08 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `review-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `pin` varchar(10) NOT NULL,
  `user_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `pin`, `user_name`) VALUES
(1, 'Supti', 'supti@gmail.com', '123456', 'xxx', 'as1010');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, 'tapu', 'tapu', '123', '', '', 'user', '', 'enable', '2015-12-14', '03:08:15.000000');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(255) NOT NULL,
  `question_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `answer` varchar(600) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `text` varchar(600) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `avg` double NOT NULL,
  `time` time(6) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `cata_id`, `cata`, `sub_cata`, `item_id`, `user_id`, `user_type`, `text`, `type`, `rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`) VALUES
(1, 1, 'food', 'restaurant', 0, '4', 'user', 'first comment after ', 'comment', 0, 0, 0, 0, '03:50:37.000000', '2015-12-08'),
(6, 1, 'food', 'restaurant', 2, '4', 'user', 'good', 'rate', 3, 3, 3, 3, '03:57:24.000000', '2015-12-08'),
(7, 1, 'food', 'restaurant', 2, '4', 'user', 'bad', 'rate', 1, 1, 1, 1, '03:57:41.000000', '2015-12-08'),
(8, 1, 'food', 'restaurant', 2, '4', 'user', 'very bad', 'rate', 1, 1, 1, 1, '03:58:05.000000', '2015-12-08'),
(9, 1, 'food', 'restaurant', 2, '4', 'user', '', 'rate', 1, 1, 1, 1, '03:58:23.000000', '2015-12-08'),
(10, 1, 'food', 'restaurant', 2, '4', 'user', 'undefined', 'rate', 5, 5, 5, 5, '03:58:36.000000', '2015-12-08'),
(11, 1, 'food', 'restaurant', 1, '4', 'user', 'very good', 'rate', 5, 5, 5, 5, '03:58:54.000000', '2015-12-08'),
(12, 3, 'life style', 'fashion', 0, '4', 'user', 'comment', 'comment', 0, 0, 0, 0, '03:47:17.000000', '2015-12-10'),
(13, 1, 'food', 'restaurant', 3, '4', 'user', 'good', 'rate', 5, 5, 5, 5, '18:32:18.000000', '2015-12-10'),
(14, 1, 'food', 'restaurant', 0, '5', 'user', 'razib', 'comment', 0, 0, 0, 0, '18:36:34.000000', '2015-12-10'),
(15, 6, 'education', 'university', 0, '5', 'user', 'united international university ', 'comment', 0, 0, 0, 0, '02:59:59.000000', '2015-12-12'),
(16, 6, 'education', 'university', 0, '', 'user', 'I want to know the expence', 'comment', 0, 0, 0, 0, '17:08:18.000000', '2015-12-12'),
(17, 3, 'life style', 'fashion', 0, '', 'user', 'ccccc', 'comment', 0, 0, 0, 0, '17:09:51.000000', '2015-12-12'),
(18, 3, 'life style', 'fashion', 0, '4', 'user', 'adadsd', 'question', 0, 0, 0, 0, '17:10:09.000000', '2015-12-12'),
(19, 6, 'education', 'university', 0, '4', 'user', 'adasdasd', 'comment', 0, 0, 0, 0, '17:10:47.000000', '2015-12-12'),
(20, 3, 'life style', 'fashion', 0, '7', 'user', 'asdasdad', 'comment', 0, 0, 0, 0, '17:11:24.000000', '2015-12-12'),
(21, 6, 'education', 'university', 0, '8', 'user', 'dfgsad', 'comment', 0, 0, 0, 0, '17:14:19.000000', '2015-12-12'),
(22, 6, 'education', 'university', 0, '3', 'user', 'I want to know the expence', 'comment', 0, 0, 0, 0, '17:15:00.000000', '2015-12-12'),
(23, 7, 'health', 'hospital', 0, '3', 'user', 'good hospital', 'comment', 0, 0, 0, 0, '21:58:36.000000', '2015-12-12'),
(24, 149, 'food', 'restaurant', 1, '', 'user', 'good', 'rate', 5, 5, 5, 5, '03:06:04.000000', '2015-12-22'),
(25, 149, 'food', 'restaurant', 0, '', 'user', 'KFC is good', 'comment', 0, 0, 0, 0, '12:54:12.000000', '2015-12-22'),
(26, 149, 'food', 'restaurant', 0, '', 'user', 'kfc ', 'rate', 3, 3, 3, 3, '09:25:09.000000', '2015-12-24'),
(27, 1, 'food', 'restaurant', 0, '', 'user', '3', 'rate', 1, 1, 1, 1, '09:26:27.000000', '2015-12-24'),
(28, 1, 'food', 'restaurant', 3, '', 'user', '333', 'rate', 1, 1, 1, 1, '09:28:25.000000', '2015-12-24'),
(29, 149, 'food', 'restaurant', 0, '', 'user', 'kfc ori', 'rate', 3, 3, 3, 3, '09:35:29.000000', '2015-12-24'),
(30, 149, 'food', 'restaurant', 0, '', 'user', '123', 'rate', 2, 2, 2, 2, '09:36:52.000000', '2015-12-24'),
(31, 149, 'food', 'restaurant', 0, '', 'user', '123456789', 'rate', 5, 5, 5, 5, '09:39:50.000000', '2015-12-24'),
(32, 149, 'food', 'restaurant', 136, '', 'user', '123456789', 'rate', 1, 1, 1, 1, '09:42:38.000000', '2015-12-24'),
(33, 149, 'food', 'restaurant', 135, '', 'user', 'kfc chicken review', 'rate', 5, 5, 5, 5, '09:43:57.000000', '2015-12-24'),
(34, 149, 'food', 'restaurant', 136, '', 'user', 'Krushers correct rating', 'rate', 5, 5, 5, 5, '09:47:38.000000', '2015-12-24'),
(35, 151, 'food', 'restaurant', 137, '', 'user', 'first', 'rate', 5, 1, 3, 3, '20:55:24.000000', '2015-12-24'),
(36, 4, 'education', 'university', 0, '', 'user', '1', 'rate', 1, 1, 1, 1, '07:48:20.000000', '2015-12-25'),
(37, 149, 'food', 'restaurant', 135, '', 'user', 'chicken is good', 'rate', 5, 3, 4, 4, '19:53:23.000000', '2015-12-28'),
(38, 152, 'education', '', 0, '', 'user', 'comment from tapu', 'comment', 0, 0, 0, 0, '04:41:10.000000', '2015-12-29'),
(39, 153, 'education', '', 0, '9', 'user', 'sdfsdfsf', 'question', 0, 0, 0, 0, '04:53:57.000000', '2015-12-29'),
(40, 149, 'food', 'restaurant', 135, '9', 'user', 'very bad', 'rate', 1, 1, 1, 1, '04:55:44.000000', '2015-12-29'),
(41, 2, 'food', 'restaurant', 152, '', 'user', 'good', 'rate', 4, 3, 4, 3.6666666666667, '00:47:53.000000', '2015-12-31'),
(42, 2, 'food', 'restaurant', 0, '', 'user', 'comment check', 'comment', 0, 0, 0, 0, '00:59:52.000000', '2015-12-31'),
(43, 2, 'food', 'restaurant', 0, '', 'user', '123', 'comment', 0, 0, 0, 0, '01:00:35.000000', '2015-12-31'),
(44, 2, 'food', 'restaurant', 0, '7', 'user', '123456', 'comment', 0, 0, 0, 0, '01:01:09.000000', '2015-12-31'),
(45, 2, 'food', 'restaurant', 0, '3', 'user', 'gtfsgsdf', 'comment', 0, 0, 0, 0, '01:02:02.000000', '2015-12-31'),
(46, 10, 'food', 'restaurant', 0, '3', 'user', 'commetn', 'comment', 0, 0, 0, 0, '01:09:51.000000', '2016-01-07'),
(47, 10, 'food', 'restaurant', 158, '3', 'user', 'good', 'rate', 4, 3, 3, 3.3333333333333, '01:10:04.000000', '2016-01-07'),
(48, 10, 'food', 'restaurant', 159, '3', 'user', 'very good', 'rate', 4, 4, 4, 4, '01:10:20.000000', '2016-01-07'),
(49, 10, 'food', 'restaurant', 161, '3', 'user', 'what about it', 'rate', 2, 2, 3, 2.3333333333333, '01:11:15.000000', '2016-01-07');

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE `crop` (
  `left` int(50) NOT NULL,
  `top` int(50) NOT NULL,
  `width` int(50) NOT NULL,
  `height` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crop`
--

INSERT INTO `crop` (`left`, `top`, `width`, `height`) VALUES
(0, 0, 0, 0),
(147, 173, 0, 0),
(0, 0, 0, 0),
(0, 0, 0, 0),
(239, 184, 0, 0),
(239, 184, 0, 0),
(160, 155, 0, 0),
(160, 155, 0, 0),
(89, 149, 100, 100),
(73, 152, 121, 44),
(42, 18, 254, 281),
(42, 18, 254, 281),
(17, 6, 275, 302),
(286, 407, 89, 114),
(286, 407, 89, 114),
(208, 246, 100, 100),
(208, 246, 167, 298),
(65, 22, 250, 366),
(0, 0, 308, 435),
(8, 42, 330, 339),
(24, 96, 100, 100),
(22, 63, 100, 100),
(49, 115, 100, 100),
(33, 109, 100, 100),
(113, -45, 100, 100),
(83, 131, 100, 100),
(0, 81, 100, 100),
(9, 101, 100, 100),
(76, 41, 100, 100),
(24, 67, 100, 100),
(183, 192, 100, 100),
(214, -48, 100, 100),
(0, 153, 100, 100),
(137, 208, 100, 100),
(137, 208, 238, 244),
(91, 64, 238, 244),
(80, 0, 295, 421),
(219, -45, 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_faculty`
--

CREATE TABLE `doctor_faculty` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `day` varchar(150) NOT NULL,
  `room` varchar(100) NOT NULL,
  `visite` varchar(50) NOT NULL,
  `depertment` varchar(50) NOT NULL,
  `extra_activity` varchar(200) NOT NULL,
  `education` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `link` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_faculty`
--

INSERT INTO `doctor_faculty` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `designation`, `day`, `room`, `visite`, `depertment`, `extra_activity`, `education`, `email`, `mobile`, `link`, `description`, `image_link`, `date`, `time`) VALUES
(1, 'education', 6, 'university', 'Alok kumar Shaha', 'Depertment Head', '', '', '', '', '', 'Msc in cse from DU', 'alok@uap-bd.edu', '', '', '', '', '0000-00-00', '00:00:00.000000'),
(2, 'education', 6, 'university', 'Anowarul Abedin ', 'lecturar', '', '', '', 'Computer Science and engineering', '', 'Bsc in CSE from IUT', 'mithu@gmail.com', '', '', '', '', '0000-00-00', '00:00:00.000000'),
(3, 'health', 7, 'hospital', 'AAAAAA', 'Nurologist', '', '', '', 'Nuron', '', '', '', '', '', '', '', '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `general_image`
--

CREATE TABLE `general_image` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `image_link` varchar(50) NOT NULL,
  `image_des` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_image`
--

INSERT INTO `general_image` (`id`, `cata_id`, `cata`, `sub_cata`, `image_link`, `image_des`, `date`, `time`) VALUES
(1, 1, 'food', 'restaurant', '1_food_1_restaurant.jpg', 'food image 1', '0000-00-00', '00:00:00.000000'),
(2, 1, 'food', 'restaurant', '2_food_1_restaurant.jpg', 'food image 2', '0000-00-00', '00:00:00.000000'),
(3, 1, 'food', 'restaurant', '3_food_1_restaurant.jpg', 'food image 3', '0000-00-00', '00:00:00.000000'),
(4, 1, 'food', 'restaurant', '4_food_1_restaurant.jpg', 'food image 4', '0000-00-00', '00:00:00.000000'),
(5, 1, 'food', 'restaurant', '5_food_1_restaurant.jpg', 'food image 5', '0000-00-00', '00:00:00.000000'),
(6, 1, 'food', 'restaurant', '6_food_1_restaurant.jpg', 'food image 6', '0000-00-00', '00:00:00.000000'),
(383, 148, 'food', 'grocery', '383_food_148_grocery.jpg', '', '2015-12-21', '06:34:28.000000'),
(384, 149, 'food', 'restaurant', '384_food_149_restaurant.jpg', '', '2015-12-21', '06:40:57.000000'),
(385, 149, 'food', 'restaurant', '385_food_149_restaurant.jpg', '', '2015-12-21', '06:40:57.000000'),
(386, 149, 'food', 'restaurant', '386_food_149_restaurant.jpg', '', '2015-12-21', '06:40:57.000000'),
(387, 151, 'food', 'restaurant', '387_food_151_restaurant.jpg', '', '2015-12-22', '12:56:46.000000'),
(388, 151, 'food', 'restaurant', '388_food_151_restaurant.png', '', '2015-12-22', '12:56:46.000000'),
(389, 152, 'education', '', '389_education_152_.jpg', '', '2015-12-25', '08:50:41.000000'),
(390, 152, 'education', '', '390_education_152_.jpg', '', '2015-12-25', '08:50:42.000000'),
(391, 152, 'education', '', '391_education_152_.jpg', '', '2015-12-25', '08:50:42.000000'),
(392, 153, 'education', '', '392_education_153_.jpg', '', '2015-12-25', '09:31:37.000000'),
(393, 153, 'education', '', '393_education_153_.jpg', '', '2015-12-25', '09:31:37.000000'),
(394, 154, 'health', '', '394_health_154_.png', '', '2015-12-25', '10:09:29.000000'),
(395, 154, 'health', '', '395_health_154_.jpg', '', '2015-12-25', '10:09:29.000000'),
(396, 154, 'health', '', '396_health_154_.png', '', '2015-12-25', '10:09:29.000000'),
(397, 156, 'residence', '', '397_residence_156_.jpg', '', '2015-12-26', '08:47:31.000000'),
(398, 156, 'residence', '', '398_residence_156_.jpg', '', '2015-12-26', '08:47:31.000000'),
(399, 157, 'tourism', 'tourism place', '399_tourism_157_.jpg', '', '2015-12-26', '09:01:35.000000'),
(400, 157, 'tourism', 'tourism place', '400_tourism_157_.png', '', '2015-12-26', '09:01:36.000000'),
(401, 2, 'food', 'restaurant', '401_food_2_restaurant.jpg', '', '2015-12-31', '00:47:16.000000'),
(402, 2, 'food', 'restaurant', '402_food_2_restaurant.jpg', '', '2015-12-31', '00:47:16.000000'),
(403, 3, 'life style', 'fashion', '403_life style_3_fashion.jpg', '', '2015-12-31', '01:15:26.000000'),
(404, 3, 'life style', 'fashion', '404_life style_3_fashion.jpg', '', '2015-12-31', '01:15:26.000000'),
(405, 4, '', '', '405__4_.jpg', '', '2016-01-07', '00:40:49.000000'),
(406, 4, '', '', '406__4_.jpg', '', '2016-01-07', '00:40:49.000000'),
(407, 4, '', '', '407__4_.jpg', '', '2016-01-07', '00:40:49.000000'),
(408, 4, '', '', '408__4_.jpg', '', '2016-01-07', '00:40:49.000000'),
(409, 10, 'food', 'restaurant', '409_food_10_restaurant.jpg', '', '2016-01-07', '01:04:10.000000'),
(410, 10, 'food', 'restaurant', '410_food_10_restaurant.jpg', '', '2016-01-07', '01:04:11.000000'),
(411, 10, 'food', 'restaurant', '411_food_10_restaurant.jpg', '', '2016-01-07', '01:04:11.000000'),
(412, 10, 'food', 'restaurant', '412_food_10_restaurant.jpg', '', '2016-01-07', '01:04:11.000000'),
(413, 26, 'food', 'restaurant', '413_food_26_restaurant.jpg', '', '2016-01-09', '04:01:33.000000'),
(414, 26, 'food', 'restaurant', '414_food_26_restaurant.jpg', '', '2016-01-09', '04:01:33.000000'),
(415, 36, 'life style', 'fashion', '415_life style_36_fashion.jpg', '', '2016-01-11', '03:47:58.000000'),
(416, 38, 'food', 'restaurant', '416_food_38_restaurant.jpg', '', '2016-01-11', '03:59:44.000000'),
(417, 38, 'food', 'restaurant', '417_food_38_restaurant.jpg', '', '2016-01-11', '03:59:44.000000'),
(418, 40, 'food', 'restaurant', '418_food_40_restaurant.jpg', '', '2016-01-11', '04:06:29.000000'),
(419, 40, 'food', 'restaurant', '419_food_40_restaurant.jpg', '', '2016-01-11', '04:06:29.000000'),
(420, 42, 'food', 'restaurant', '420_food_42_restaurant.jpg', '', '2016-01-11', '04:15:13.000000'),
(421, 43, 'food', 'restaurant', '421_food_43_restaurant', '', '2016-01-11', '04:21:08.000000'),
(422, 44, 'food', 'restaurant', '422_food_44_restaurant.jpg', '', '2016-01-11', '04:24:50.000000'),
(423, 44, 'food', 'restaurant', '423_food_44_restaurant.jpg', '', '2016-01-11', '04:24:50.000000'),
(424, 46, 'food', 'cafe', '424_food_46_cafe.jpg', '', '2016-01-11', '04:39:41.000000'),
(425, 51, 'life style', 'accessorice', '425_life style_51_accessorice', '', '2016-01-13', '04:32:15.000000'),
(426, 55, 'life style', 'accessorice', '426_life style_55_accessorice', '', '2016-01-13', '04:45:07.000000'),
(427, 55, 'life style', 'accessorice', '427_life style_55_accessorice.jpg', '', '2016-01-13', '04:45:08.000000');

-- --------------------------------------------------------

--
-- Table structure for table `general_info`
--

CREATE TABLE `general_info` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `column_1` varchar(300) NOT NULL,
  `column_2` varchar(300) NOT NULL,
  `column_3` varchar(300) NOT NULL,
  `column_4` varchar(300) NOT NULL,
  `column_5` varchar(300) NOT NULL,
  `column_6` varchar(300) NOT NULL,
  `column_7` varchar(300) NOT NULL,
  `column_8` varchar(300) NOT NULL,
  `column_9` varchar(300) NOT NULL,
  `column_10` varchar(300) NOT NULL,
  `column_11` varchar(300) NOT NULL,
  `column_12` varchar(300) NOT NULL,
  `column_13` varchar(300) NOT NULL,
  `column_14` varchar(300) NOT NULL,
  `column_15` varchar(300) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(800) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_info`
--

INSERT INTO `general_info` (`id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `branches`, `web_site`, `main_location`, `location`, `map`, `average_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `date`, `time`) VALUES
(1, '', '', '', '1__', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2015-12-31', '00:42:37.000000'),
(2, 'restaurant', 'food', 'Star Kabab', '2_food_restaurant.jpg', 'Description of star kabab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.starkabab.com', 'Dhanmondi', 'Science lab Dhanmondi', '<iframe height=250px width=100% src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1451501084924" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '2015-12-31', '00:47:16.000000'),
(3, 'fashion', 'life style', 'Star Kabab', '3_life style_fashion.jpg', 'Description of star kabab', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.starkabab.com', 'Dhanmondi', 'Science lab Dhanmondi', '<iframe height=250px width=100% src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1451501084924" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '2015-12-31', '01:15:26.000000'),
(10, 'restaurant', 'food', 'Al- Fresco', '10_food_restaurant.jpg', 'Description', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No Website', 'Dhanmondi', 'Dhanmondi, Jigatola.', '<iframe height=250px width=100% src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1451501084924" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '2016-01-07', '01:04:10.000000'),
(11, 'coaching', 'education', '', '11_education_coaching', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-08', '04:43:00.000000'),
(12, 'college', 'education', '', '12_education_college', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-08', '04:43:06.000000'),
(13, 'restaurant', 'food', '', '13_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:29:26.000000'),
(14, 'restaurant', 'food', '', '14_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:29:29.000000'),
(15, 'restaurant', 'food', '', '15_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:29:52.000000'),
(16, 'restaurant', 'food', '', '16_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:34:53.000000'),
(17, 'restaurant', 'food', '', '17_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:37:06.000000'),
(18, 'restaurant', 'food', '', '18_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:38:05.000000'),
(19, 'restaurant', 'food', '', '19_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:39:19.000000'),
(20, 'restaurant', 'food', '', '20_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:39:29.000000'),
(21, 'restaurant', 'food', '', '21_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:40:25.000000'),
(22, 'restaurant', 'food', '', '22_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '03:33:20.000000'),
(23, 'restaurant', 'food', '', '23_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '03:33:33.000000'),
(24, 'restaurant', 'food', '', '24_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '03:33:43.000000'),
(25, 'restaurant', 'food', '', '25_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '03:34:07.000000'),
(26, 'restaurant', 'food', 'Sharma House', '26_food_restaurant.jpg', 'Number one Sharma House in Dhaka', 'Fast Food', 'Sharma is best here', 'No Offer is going on', '10am-11pm', '', '', '', '', '', '', '', '', '', '', '', '', 'www.sharmahouse.com', 'Dhanmondi', 'Dhanmondi, 6/a, Satmoshjid road', 'No Map Available', 0, 0, 0, 0, 0, '2016-01-09', '04:01:33.000000'),
(27, 'restaurant', 'food', '', '27_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '01:55:50.000000'),
(28, 'restaurant', 'food', 'tapu', '28_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '01:56:31.000000'),
(29, 'restaurant', 'food', 'mandal', '29_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '02:00:12.000000'),
(30, 'restaurant', 'food', 'Asmita', '30_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '02:01:13.000000'),
(31, 'fashion', 'life style', '', '31_life style_fashion', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '03:07:48.000000'),
(32, 'fashion', 'life style', '', '32_life style_fashion', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '03:08:06.000000'),
(33, 'fashion', 'life style', '', '33_life style_fashion', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '03:08:45.000000'),
(34, 'fashion', 'life style', '', '34_life style_fashion', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '03:09:06.000000'),
(35, 'fashion', 'life style', '', '35_life style_fashion', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '03:32:48.000000'),
(36, 'fashion', 'life style', 'dfasasd', '36_life style_fashion.jpg', 'asdasd', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'dsadsadada', 'asdasdsad', 'sadsa', '', 0, 0, 0, 0, 0, '2016-01-11', '03:47:58.000000'),
(37, 'restaurant', 'food', 'adfad', '37_food_restaurant.jpg', 'sadasdd', 'asdsad', 'asd', 'dadsa', 'dasdasd', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sadasd', 'asd', '', 0, 0, 0, 0, 0, '2016-01-11', '03:56:33.000000'),
(38, 'restaurant', 'food', 'adfad', '38_food_restaurant.jpg', 'sadasdd', 'asdsad', 'asd', 'dadsa', 'dasdasd', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sadasd', 'asd', '', 0, 0, 0, 0, 0, '2016-01-11', '03:59:43.000000'),
(39, 'restaurant', 'food', 'adfad', '39_food_restaurant.jpg', 'sadasdd', 'asdsad', 'asd', 'dadsa', 'dasdasd', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sadasd', 'asd', '', 0, 0, 0, 0, 0, '2016-01-11', '04:04:37.000000'),
(40, 'restaurant', 'food', 'adfad', '40_food_restaurant.jpg', 'sadasdd', 'asdsad', 'asd', 'dadsa', 'dasdasd', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sadasd', 'asd', '', 0, 0, 0, 0, 0, '2016-01-11', '04:06:29.000000'),
(41, 'restaurant', 'food', 'adfad', '41_food_restaurant.jpg', 'sadasdd', 'asdsad', 'asd', 'dadsa', 'dasdasd', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sadasd', 'asd', '', 0, 0, 0, 0, 0, '2016-01-11', '04:12:48.000000'),
(42, 'restaurant', 'food', 'adfad', '42_food_restaurant.jpg', 'sadasdd', 'asdsad', 'asd', 'dadsa', 'dasdasd', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sadasd', 'asd', '', 0, 0, 0, 0, 0, '2016-01-11', '04:15:13.000000'),
(43, 'restaurant', 'food', '', '43_food_restaurant.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:21:07.000000'),
(44, 'restaurant', 'food', '', '44_food_restaurant.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:24:50.000000'),
(45, 'restaurant', 'food', '', '45_food_restaurant.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:33:56.000000'),
(46, 'cafe', 'food', '', '46_food_cafe.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:39:41.000000'),
(47, 'sweet', 'food', '', '47_food_sweet.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:40:21.000000'),
(48, 'sweet', 'food', '', '48_food_sweet.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:40:56.000000'),
(49, 'sweet', 'food', '', '49_food_sweet.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:43:04.000000'),
(50, 'sweet', 'food', '', '50_food_sweet.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-11', '04:43:54.000000'),
(51, 'accessorice', 'life style', '', '51_life style_accessorice', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-13', '04:32:15.000000'),
(52, 'accessorice', 'life style', '', '52_life style_accessorice', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-13', '04:32:33.000000'),
(53, 'accessorice', 'life style', '', '53_life style_accessorice', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-13', '04:37:12.000000'),
(54, 'accessorice', 'life style', '', '54_life style_accessorice', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-01-13', '04:41:09.000000'),
(55, 'accessorice', 'life style', 'Ryan', '55_life style_accessorice.jpg', 'My nephew. ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'not yet', 'Earth', 'Azimpur', '', 0, 0, 0, 0, 0, '2016-01-13', '04:45:07.000000'),
(56, 'accessorice', 'life style', 'Ryan', '56_life style_accessorice', 'My nephew. ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'not yet', 'Earth', 'Azimpur', '', 0, 0, 0, 0, 0, '2016-01-13', '04:48:09.000000');

-- --------------------------------------------------------

--
-- Table structure for table `general_infos`
--

CREATE TABLE `general_infos` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `property_type_acco` varchar(100) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `service_elec` int(11) NOT NULL,
  `warranty_elec` varchar(500) NOT NULL,
  `servicing_elec` varchar(200) NOT NULL,
  `help_line_elec` varchar(200) NOT NULL,
  `type` varchar(90) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `speciality` varchar(600) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `offer` varchar(300) NOT NULL,
  `open_hour` varchar(100) NOT NULL,
  `close_day` varchar(30) NOT NULL,
  `enrty_fee` varchar(30) NOT NULL,
  `package` varchar(200) NOT NULL,
  `travel_alert` varchar(250) NOT NULL,
  `transport` varchar(250) NOT NULL,
  `distance_chart` varchar(250) NOT NULL,
  `travel_agent` varchar(250) NOT NULL,
  `schedual` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `booking_online` varchar(250) NOT NULL,
  `area2` varchar(250) NOT NULL,
  `size` varchar(250) NOT NULL,
  `admission` varchar(250) NOT NULL,
  `requirements` varchar(250) NOT NULL,
  `facility` varchar(250) NOT NULL,
  `information` varchar(250) NOT NULL,
  `highlight` varchar(250) NOT NULL,
  `extra_facilities` varchar(250) NOT NULL,
  `ambulance` varchar(250) NOT NULL,
  `program_schedual` varchar(250) NOT NULL,
  `sponsor` varchar(250) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(400) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_infos`
--

INSERT INTO `general_infos` (`id`, `sub_cata`, `cata`, `property_type_acco`, `name`, `image_link`, `service_elec`, `warranty_elec`, `servicing_elec`, `help_line_elec`, `type`, `description`, `speciality`, `branches`, `area`, `offer`, `open_hour`, `close_day`, `enrty_fee`, `package`, `travel_alert`, `transport`, `distance_chart`, `travel_agent`, `schedual`, `price`, `booking_online`, `area2`, `size`, `admission`, `requirements`, `facility`, `information`, `highlight`, `extra_facilities`, `ambulance`, `program_schedual`, `sponsor`, `web_site`, `main_location`, `location`, `map`, `average_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `date`, `time`) VALUES
(1, 'restaurant', 'food', '', 'Star Kabab', 'star.jpg', 0, '', '', '', 'Bengali food', 'Family environment.', 'Kabab is Spetial here.', '', '', 'No offer going on', '6am-11.30pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Science Lab', 'Science Lab', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.222418785588!2d90.3798702126277!3d23.739446684346348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8b6264409eb%3A0x48558c56551a5947!2sStar+Kabab+%26+Restaurant!5e0!3m2!1sen!2sbd!4v1448220890572" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(3, 'fashion', 'life style', '', 'Artisti', 'artisti.jpg', 0, '', '', '', 'Cloths', 'Only for Male. Every types of shirt, T-shart and Pants.', 'Panjabi is spetial here', '', '', 'No offer is going on', '11am to 8pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Dhanmondi-15, Satmasjid road, Anam Rangs 6/a', '', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(4, 'cafe', 'food', '', 'COFFEE HOUSE ', 'cofee.jpg', 0, '', '', '', 'Cafe|Sancks|Pastry', 'All kind of cafe is available here. Items of snacks are also rich.', 'Coffee is directly imported from Brazil.', '', '', 'For Under 12 age snacks price is half', '7am to 11.30pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No Website', 'Dhanmondi', 'Dhanmondi, 8/a, 15.', '', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `image_link` varchar(70) NOT NULL,
  `image_name` varchar(60) NOT NULL,
  `image_text` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_des`
--

CREATE TABLE `item_des` (
  `id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(80) NOT NULL,
  `image_des` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_des`
--

INSERT INTO `item_des` (`id`, `item_id`, `cata`, `cata_id`, `sub_cata`, `image_des`, `image_link`, `image_name`, `date`, `time`) VALUES
(1, 1, 'food', 1, 'restaurant', 'image1 descriptionimage descriptionimage descriptionimage descriptionimage description', '1_1_food_1.jpg', 'Image 1', '0000-00-00', '00:00:00.000000'),
(2, 1, 'food', 1, 'restaurant', 'image2 descriptionimage descriptionimage descriptionimage descriptionimage description', '2_1_food_1.jpg', 'Image 2', '0000-00-00', '00:00:00.000000'),
(3, 1, 'food', 1, 'restaurant', 'image3 descriptionimage descriptionimage descriptionimage descriptionimage description', '3_1_food_1.jpg', 'Image 3', '0000-00-00', '00:00:00.000000'),
(4, 2, 'food', 1, 'restaurant', 'image4 descriptionimage descriptionimage descriptionimage descriptionimage description', '4_2_food_1.jpg', 'Image 4', '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `item_list`
--

CREATE TABLE `item_list` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `size` varchar(30) NOT NULL,
  `model` varchar(100) NOT NULL,
  `filter_1` varchar(40) NOT NULL,
  `filter_2` varchar(40) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_list`
--

INSERT INTO `item_list` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `description`, `image_link`, `price`, `size`, `model`, `filter_1`, `filter_2`, `total_rate`, `rate_1`, `rate_2`, `rate_3`, `rate_people`, `date`, `time`) VALUES
(1, 'food', 1, 'restaurant', 'kabab', 'It''s test great', '1_food_1_restaurant.jpg', 150, '', '', '', '', 84, 28, 28, 28, 7, '0000-00-00', '00:00:00.000000'),
(2, 'food', 1, 'restaurant', 'pizza', 'awesome', '2_food_1_restaurant.jpg', 920, '', '', '', '', 36, 12, 12, 12, 6, '0000-00-00', '00:00:00.000000'),
(3, 'food', 1, 'restaurant', 'Unknown', 'haventual', '2_food_1_restaurant.jpg', 999999999, '', '', '', '', 21, 7, 7, 7, 3, '0000-00-00', '00:00:00.000000'),
(4, 'life style', 3, 'fashion', 'Shirt', 'Green and White check ', '4_life style_3_fashion.jpg', 1200, 'XL', '', 'male', 'shirt', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(5, 'life style', 3, 'fashion', 'Pant', '', '4_life style_3_fashion.jpg', 0, '', '', 'male', 'denim', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(6, 'life style', 3, 'fashion', 'T-Shirt', '', '4_life style_3_fashion.jpg', 0, '', '', 'male', 'tshirt', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(7, 'life style', 32, 'beauty', 'Option 1', 'Description of Option 1', '', 1000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:26:00.000000'),
(8, 'life style', 32, 'beauty', 'Option 2', 'Description of Option 2', '', 2000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:26:00.000000'),
(9, 'life style', 32, 'beauty', 'Option 3', 'Description of Option 3', '', 3000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:26:00.000000'),
(10, 'life style', 32, 'beauty', 'Option 4', 'Description of Option 4', '', 4000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:26:00.000000'),
(11, 'food', 33, 'beauty', 'Option 1', 'Description of Option 1', '', 1000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:29:12.000000'),
(12, 'food', 33, 'beauty', 'Option 2', 'Description of Option 2', '', 2000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:29:12.000000'),
(13, 'food', 33, 'beauty', 'Option 3', 'Description of Option 3', '', 3000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:29:12.000000'),
(14, 'food', 33, 'beauty', 'Option 4', 'Description of Option 4', '', 4000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-18', '06:29:12.000000'),
(125, 'food', 127, 'accessorice', '1', '100', '', 10, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-20', '04:40:54.000000'),
(126, 'food', 127, 'accessorice', '2', '200', '', 20, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-20', '04:40:54.000000'),
(127, 'food', 128, 'accessorice', 'aa', 'aa', '', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-20', '04:43:48.000000'),
(128, 'food', 128, 'accessorice', 'yyy', 'y', '', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-20', '04:43:48.000000'),
(129, 'food', 129, 'accessorice', 'adasd', 'dsad', '', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-20', '04:47:19.000000'),
(130, 'food', 130, 'accessorice', '111111111111', '123456789', '__', 10000000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-20', '04:50:02.000000'),
(131, 'food', 130, 'accessorice', '22222222222222222222', '23232323232333', '__', 2e18, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-20', '04:50:03.000000'),
(132, 'food', 132, 'fashion', 'Special Porota', '', '132_food_fashion.jpg', 35, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-21', '02:16:45.000000'),
(133, 'food', 132, 'fashion', 'Grill Chicken Full', '', '133_food_fashion.jpg', 350, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-21', '02:16:45.000000'),
(134, 'food', 137, 'restaurant', '7777777', '987', '134_food_restaurant.jpg', 700000000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-21', '04:21:50.000000'),
(135, 'food', 149, 'restaurant', 'Chicken', 'Spicy', '135_food_restaurant.jpg', 105, '', '', '', '', 15, 6, 4, 5, 2, '2015-12-21', '06:40:57.000000'),
(136, 'food', 149, 'restaurant', 'Krushers', 'Available in 3 flavors.', '136_food_restaurant.jpg', 135, '', '', '', '', 15, 5, 5, 5, 1, '2015-12-21', '06:40:57.000000'),
(137, 'food', 151, 'restaurant', 'First', 'than u', '137_food_restaurant.jpg', 100, '', '', '', '', 9, 5, 1, 3, 1, '2015-12-22', '12:56:46.000000'),
(138, 'health', 154, '', 'Printer', 'Laser Printer', '138_health_.png', 5200, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-25', '10:09:29.000000'),
(139, 'health', 154, '', 'Laptop', 'Description', '139_health_.jpg', 42, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-25', '10:09:29.000000'),
(140, 'health', 154, '', 'Gaming Laptop', 'For all kinds of 4th generation gaming ', '140_health_', 130, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-25', '10:09:29.000000'),
(141, 'health', 154, '', 'Stylish laptop', 'For classic people', '141_health_.jpg', 60, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-25', '10:09:29.000000'),
(142, 'health', 154, '', 'HP TAB', 'For portable use only', '142_health_.png', 23, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-25', '10:09:29.000000'),
(143, 'tourism', 0, '', 'First', 'First and test text. First and test text. First and test text. First and test text. ', '143_tourism_.png', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-26', '09:54:17.000000'),
(144, 'tourism', 0, '', 'Second', 'First and test text. First and test text. First and test text. First and test text. First and test text. First and test text. First and test text. First and test text. First and test text. First and t', '144_tourism_.png', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-26', '09:54:17.000000'),
(145, 'tourism', 158, 'tourism place', 'First', 'Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Descript', '145_tourism_.jpg', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-26', '09:56:25.000000'),
(146, 'tourism', 158, 'tourism place', 'Second', 'Second Description Second Description Second Description Second Description Second Description Second Description Second Description Second Description Second Description Second Description Second Des', '146_tourism_.jpg', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-26', '09:56:25.000000'),
(147, 'life style', 162, 'Select Sub Catagory', 'First', '123456789', '147_life style_Select Sub Catagory.jpg', 5200, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-28', '21:58:58.000000'),
(148, 'life style', 163, 'Select Sub Catagory', 'afasdfad', 'asdadsdad', '148_life style_Select Sub Catagory.jpg', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-28', '21:59:52.000000'),
(149, 'life style', 165, 'Select Sub Catagory', 'sdfsdf', 'adsdad', '149_life style_Select Sub Catagory.png', 0, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-28', '22:00:59.000000'),
(150, 'life style', 193, 'accessorice', '1', '100000000', '150_life style_.jpg', 100, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-30', '12:28:42.000000'),
(151, 'life style', 193, 'accessorice', '2', '20000000000000000', '151_life style_.jpg', 200, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-30', '12:28:42.000000'),
(152, 'food', 2, 'restaurant', 'item 1', 'item i is good', '152_food_restaurant.jpg', 100, '', '', '', '', 11, 4, 3, 4, 1, '2015-12-31', '00:47:16.000000'),
(153, 'food', 2, 'restaurant', 'item2', 'item2 is good', '153_food_restaurant.jpg', 200, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-31', '00:47:16.000000'),
(154, 'life style', 3, 'fashion', 'Option 1', '12345789', '154_life style_fashion.jpg', 1000, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-31', '01:15:26.000000'),
(155, 'life style', 3, 'fashion', 'Printer', '46546846565', '155_life style_fashion.jpg', 130, '', '', '', '', 0, 0, 0, 0, 0, '2015-12-31', '01:15:26.000000'),
(156, '', 4, '', 'Item 1', 'Price is good', '156__.jpg', 100, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-07', '00:40:49.000000'),
(157, '', 4, '', 'Item 2', 'Descrtption 2', '157__.jpg', 200, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-07', '00:40:49.000000'),
(158, 'food', 10, 'restaurant', 'Item 1', 'item is good', '158_food_restaurant.jpg', 100, '', '', '', '', 10, 4, 3, 3, 1, '2016-01-07', '01:04:11.000000'),
(159, 'food', 10, 'restaurant', 'Item 2', 'very good', '159_food_restaurant.jpg', 200, '', '', '', '', 27, 9, 9, 9, 2, '2016-01-07', '01:04:11.000000'),
(160, 'food', 10, 'restaurant', 'Item 4', 'not bad', '160_food_restaurant.jpg', 4000, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-07', '01:04:11.000000'),
(161, 'food', 10, 'restaurant', 'Item 5', 'ok', '161_food_restaurant.jpg', 5000, '', '', '', '', 7, 2, 2, 3, 1, '2016-01-07', '01:04:11.000000'),
(162, 'food', 16, 'restaurant', '', '', '162_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:34:53.000000'),
(163, 'food', 17, 'restaurant', 'fa', 'dasd', '163_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:37:06.000000'),
(164, 'food', 18, 'restaurant', '', '', '164_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:38:05.000000'),
(165, 'food', 20, 'restaurant', '', '', '165_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:39:29.000000'),
(166, 'food', 21, 'restaurant', '', '', '166_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '02:40:25.000000'),
(167, 'food', 22, 'restaurant', '', '', '167_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '03:33:20.000000'),
(168, 'food', 24, 'restaurant', '', '', '168_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '03:33:43.000000'),
(169, 'food', 25, 'restaurant', '', '', '169_food_restaurant', 0, '', '', '', '', 0, 0, 0, 0, 0, '2016-01-09', '03:34:08.000000'),
(170, 'food', 30, 'restaurant', '123456', '56', '170_food_restaurant', 7897987987987, '', '', 'starter', '', 0, 0, 0, 0, 0, '2016-01-11', '02:01:13.000000'),
(171, 'life style', 36, 'fashion', 'asdasd', 'asdasdad', '171_life style_fashion.jpg', 0, '', '', 'Select Filter 3', '', 0, 0, 0, 0, 0, '2016-01-11', '03:47:58.000000'),
(172, 'food', 38, 'restaurant', 'Item 1', 'dfassa', '172_food_restaurant.jpg', 100, '', '', 'fast food', '', 0, 0, 0, 0, 0, '2016-01-11', '03:59:44.000000'),
(173, 'food', 40, 'restaurant', 'Item 2', 'asdadsadad', '173_food_restaurant.jpg', 200, '', '', 'dessert', '', 0, 0, 0, 0, 0, '2016-01-11', '04:06:29.000000'),
(174, 'life style', 55, 'accessorice', '123', '123', '174_life style_accessorice.jpg', 123, '', '', 'Select filter_1', '', 0, 0, 0, 0, 0, '2016-01-13', '04:45:08.000000'),
(175, 'life style', 55, 'accessorice', 'Item 2', '7987987', '175_life style_accessorice.jpg', 879, '', '', 'Select filter_1', '', 0, 0, 0, 0, 0, '2016-01-13', '04:45:08.000000'),
(176, 'life style', 56, 'accessorice', '123', '123', '176_life style_accessorice', 123, '', '', 'Select filter_1', '', 0, 0, 0, 0, 0, '2016-01-13', '04:48:09.000000'),
(177, 'life style', 56, 'accessorice', 'Item 2', '7987987', '177_life style_accessorice', 879, '', '', 'Select filter_1', '', 0, 0, 0, 0, 0, '2016-01-13', '04:48:09.000000'),
(178, 'life style', 56, 'accessorice', 'Item 4', 'asdasdsa', '178_life style_accessorice.jpg', 0, '', '', 'Select filter_1', '', 0, 0, 0, 0, 0, '2016-01-13', '04:48:09.000000');

-- --------------------------------------------------------

--
-- Table structure for table `overview`
--

CREATE TABLE `overview` (
  `id` int(255) NOT NULL,
  `name` varchar(26) NOT NULL,
  `rating` int(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `catagory` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overview`
--

INSERT INTO `overview` (`id`, `name`, `rating`, `location`, `catagory`, `sub_cata`, `image`) VALUES
(1, 'Star Kabab', 4, 'Science-Lab', 'FOOD', 'Restaurant', 'star.jpg'),
(2, 'Appolo', 4, 'Panthapath', 'Health', 'Hospital', 'gta.png'),
(3, 'YELLOW', 5, 'Science Lab', 'Life Style', 'Fashion', 'girl1.jpg'),
(4, 'University Of Asia PacifiC', 3, 'Dhanmondi', 'Education', 'University', 'iron1.png'),
(5, 'HSC Result', 0, 'Bangladesh', 'Education', 'Results', 'car1.png'),
(6, 'Trust College & BritishC', 2, 'Uttara', 'Education', 'College', 'image1.png'),
(7, 'Rajoir K.J.S', 3, 'Madaripur', 'Education', 'School', 'girl1.jpg'),
(8, 'BCS Examination', 0, 'Bangladesh', 'Education', 'Exam', 'car1.png'),
(9, 'BCS Examination', 3, 'Bangladesh', 'Education', 'Exam', 'car1.png'),
(10, 'aceraceraceraceracer', 4, '', 'Electronics', 'Computer', 'computer1.png'),
(11, 'PROCESSOR', 0, 'No Location', 'electronics', 'computer component', 'pro1.jpg'),
(12, 'Sonarga', 4, 'KarwanBazar', 'residence', 'hotel', 'car1.png'),
(13, 'DOM-INNO', 0, 'Dhanmondi', 'residence', 'Apartment', 'image1.png'),
(14, 'COX''S BAZER', 0, 'Cox''s Bazer', 'tourism', 'tourism place', 'cox.jpg'),
(15, 'WATER HOUSE', 5, 'Gazipur', 'tourism', 'resort', 'water.jpg'),
(16, 'BRAC BANK', 5, '', 'SERVICE', 'BANK', 'brack1.png');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(255) NOT NULL,
  `main_info_id` int(50) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `user_id` varchar(60) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `question` varchar(300) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(255) NOT NULL,
  `comment_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `reply` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rivew`
--

CREATE TABLE `rivew` (
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `score` int(20) NOT NULL,
  `date` date NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `user_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE `social_link` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `link_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, 'Tapu Mandal', 'online', '123', '017', 'tmandal', 'user', '', 'enable', '0000-00-00', '00:00:00.000000'),
(2, 'Tapu Mandal', 'online', '123', '', '', 'user', '', 'enable', '2015-12-07', '23:36:12.000000'),
(3, 'Asmita', 'sarker', '555', '', '', 'user', '', 'enable', '2015-12-08', '02:38:12.000000'),
(4, 'Hrithik Roshan', 'roshan', 'roshan', '', '', 'user', '', 'enable', '2015-12-08', '03:24:03.000000'),
(5, 'Razib', '123', '123', '', '1231', 'user', '', 'enable', '2015-12-10', '18:36:16.000000'),
(6, 'Asmita Sarker', 'asmita', '123', '', '', 'user', '', 'enable', '2015-12-12', '17:07:50.000000'),
(7, '123', '123', '123', '', '123', 'user', '', 'enable', '2015-12-12', '17:11:13.000000'),
(8, '456', '456', '456', '', '', 'user', '', 'enable', '2015-12-12', '17:14:04.000000'),
(9, 'Tapu', 'mandal', '123', '', '', 'user', '', 'enable', '2015-12-29', '04:40:39.000000');

-- --------------------------------------------------------

--
-- Table structure for table `user_upload_image`
--

CREATE TABLE `user_upload_image` (
  `id` int(255) NOT NULL,
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_image`
--
ALTER TABLE `general_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_info`
--
ALTER TABLE `general_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_infos`
--
ALTER TABLE `general_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_des`
--
ALTER TABLE `item_des`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_list`
--
ALTER TABLE `item_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overview`
--
ALTER TABLE `overview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `general_image`
--
ALTER TABLE `general_image`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=428;
--
-- AUTO_INCREMENT for table `general_info`
--
ALTER TABLE `general_info`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `general_infos`
--
ALTER TABLE `general_infos`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_des`
--
ALTER TABLE `item_des`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_list`
--
ALTER TABLE `item_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT for table `overview`
--
ALTER TABLE `overview`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
