-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2017 at 06:08 AM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `krazekit_aaoaz`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, 'Asmita Sarker', '123', '123', '01928039678', '', '', '', '', '0000-00-00', '00:00:00'),
(2, 'Tapu Mandal', '456', '456', '01739995117', '', '', '', '', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(255) NOT NULL,
  `question_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `answer` varchar(600) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE IF NOT EXISTS `bookmark` (
  `id` int(200) NOT NULL,
  `user_id` int(200) NOT NULL,
  `business_pro_id` int(200) NOT NULL,
  `item_id` int(200) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `text` varchar(600) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `avg` double NOT NULL,
  `time` time NOT NULL,
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE IF NOT EXISTS `crop` (
  `left` int(50) NOT NULL,
  `top` int(50) NOT NULL,
  `width` int(50) NOT NULL,
  `height` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doctor_faculty`
--

CREATE TABLE IF NOT EXISTS `doctor_faculty` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `schedule` text NOT NULL,
  `day` varchar(150) NOT NULL,
  `room` varchar(100) NOT NULL,
  `visite` varchar(50) NOT NULL,
  `depertment` varchar(50) NOT NULL,
  `extra_activity` varchar(200) NOT NULL,
  `education` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `link` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `elec_item_details`
--

CREATE TABLE IF NOT EXISTS `elec_item_details` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `what_item` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `model` varchar(100) NOT NULL,
  `item_time_status` varchar(50) NOT NULL,
  `column_1` varchar(500) NOT NULL,
  `column_2` varchar(500) NOT NULL,
  `column_3` varchar(500) NOT NULL,
  `column_4` varchar(500) NOT NULL,
  `column_5` varchar(500) NOT NULL,
  `column_6` varchar(500) NOT NULL,
  `column_7` varchar(500) NOT NULL,
  `column_8` varchar(500) NOT NULL,
  `column_9` varchar(500) NOT NULL,
  `column_10` varchar(500) NOT NULL,
  `column_11` varchar(500) NOT NULL,
  `column_12` varchar(500) NOT NULL,
  `column_13` varchar(500) NOT NULL,
  `column_14` varchar(500) NOT NULL,
  `column_15` varchar(500) NOT NULL,
  `column_16` varchar(500) NOT NULL,
  `column_17` varchar(500) NOT NULL,
  `column_18` varchar(500) NOT NULL,
  `column_19` varchar(500) NOT NULL,
  `column_20` varchar(500) NOT NULL,
  `column_21` varchar(500) NOT NULL,
  `column_22` varchar(500) NOT NULL,
  `column_23` varchar(500) NOT NULL,
  `column_24` varchar(500) NOT NULL,
  `column_25` varchar(500) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general_image`
--

CREATE TABLE IF NOT EXISTS `general_image` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `image_link` varchar(200) NOT NULL,
  `image_des` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_image`
--

INSERT INTO `general_image` (`id`, `cata_id`, `cata`, `sub_cata`, `image_link`, `image_des`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 1, 'tourism', 'natural_site', 'Bisnakandi_1_tourism_1_natural_site.jpg', '', '2017-03-19', '11:04:44', 1, ''),
(2, 1, 'tourism', 'natural_site', 'Bisnakandi_2_tourism_1_natural_site.jpg', '', '2017-03-19', '11:04:44', 1, ''),
(3, 2, 'tourism', 'heritage', 'Lalbagh Fort (Lalbagh Kella)_3_tourism_2_heritage.jpg', '', '2017-03-19', '11:25:08', 1, ''),
(4, 2, 'tourism', 'heritage', 'Lalbagh Fort (Lalbagh Kella)_4_tourism_2_heritage.jpg', '', '2017-03-19', '11:27:18', 1, ''),
(5, 3, 'tourism', 'picnic_spot', 'Mohammadi  Garden_5_tourism_3_picnic_spot.jpg', '', '2017-03-19', '11:50:34', 1, ''),
(6, 3, 'tourism', 'picnic_spot', 'Mohammadi  Garden_6_tourism_3_picnic_spot.jpg', '', '2017-03-19', '11:50:34', 1, ''),
(7, 4, 'tourism', 'lake_park', 'Dhanmondi Lake_7_tourism_4_lake_park.jpg', '', '2017-03-19', '12:01:11', 1, ''),
(8, 5, 'residence', 'resort', 'Nokkhottrobari Resort & Conference Center_8_residence_5_resort.jpg', '', '2017-03-19', '12:19:25', 1, ''),
(9, 5, 'residence', 'resort', 'Nokkhottrobari Resort & Conference Center_9_residence_5_resort.jpg', '', '2017-03-19', '12:19:25', 1, ''),
(10, 6, 'food', 'restaurant', 'Shad Tehari Ghar_10_food_6_restaurant.jpg', '', '2017-03-19', '14:52:45', 2, ''),
(11, 6, 'food', 'restaurant', 'Shad Tehari Ghar_11_food_6_restaurant.jpg', '', '2017-03-19', '14:52:45', 2, ''),
(12, 7, 'residence', 'hotel', 'Padma Resort_12_residence_7_hotel.jpg', '', '2017-03-19', '15:06:35', 1, ''),
(13, 7, 'residence', 'hotel', 'Padma Resort_13_residence_7_hotel.jpg', '', '2017-03-19', '15:06:35', 1, ''),
(14, 8, 'residence', 'hotel', 'The Westin Dhaka_14_residence_8_hotel.jpg', '', '2017-03-19', '15:41:26', 1, ''),
(15, 8, 'residence', 'hotel', 'The Westin Dhaka_15_residence_8_hotel.jpg', '', '2017-03-19', '15:41:26', 1, ''),
(17, 9, 'food', 'restaurant', 'Hakka Dhaka	_17_food_9_restaurant.jpg', '', '2017-03-19', '15:57:24', 2, ''),
(18, 10, 'food', 'restaurant', 'adasd_18_food_10_restaurant.jpg', '', '2017-03-19', '16:19:11', 2, ''),
(19, 11, 'food', 'restaurant', 'The_Mughal_Kitchen_19_food_11_restaurant.jpg', '', '2017-03-19', '21:52:02', 2, ''),
(20, 11, 'food', 'restaurant', 'The_Mughal_Kitchen_20_food_11_restaurant.jpg', '', '2017-03-19', '21:52:02', 2, ''),
(21, 12, 'life style', 'fashion', 'Yellow_21_life_style_12_fashion.jpg', '', '2017-03-22', '04:39:13', 1, ''),
(22, 12, 'life style', 'fashion', 'Yellow_22_life_style_12_fashion.png', '', '2017-03-22', '04:39:13', 1, ''),
(23, 12, 'life style', 'fashion', 'Yellow_23_life_style_12_fashion.jpg', '', '2017-03-22', '04:39:13', 1, ''),
(24, 13, 'life style', 'accessorice', 'Diamond_World_Ltd._24_life_style_13_accessorice.jpg', '', '2017-03-22', '05:16:22', 1, ''),
(25, 13, 'life style', 'accessorice', 'Diamond_World_Ltd._25_life_style_13_accessorice.jpg', '', '2017-03-22', '05:16:22', 1, ''),
(26, 13, 'life style', 'accessorice', 'Diamond_World_Ltd._26_life_style_13_accessorice.jpg', '', '2017-03-22', '05:16:22', 1, ''),
(27, 14, 'life style', 'parlor_salon', 'Farzana_Shakils_Makeover_Salon_Ltd._27_life_sty', '', '2017-03-22', '05:26:11', 1, ''),
(28, 14, 'life style', 'parlor_salon', 'Farzana_Shakils_Makeover_Salon_Ltd._28_life_sty', '', '2017-03-22', '05:26:11', 1, ''),
(29, 14, 'life style', 'parlor_salon', 'Farzana_Shakils_Makeover_Salon_Ltd._29_life_sty', '', '2017-03-22', '05:26:11', 1, ''),
(30, 15, 'life style', 'gym_fitness', 'Adonize_Fitness_Center_LTD_30_life_style_15_gym_fi', '', '2017-03-22', '05:33:59', 1, ''),
(31, 15, 'life style', 'gym_fitness', 'Adonize_Fitness_Center_LTD_31_life_style_15_gym_fi', '', '2017-03-22', '05:33:59', 1, ''),
(32, 16, 'life style', 'accessorice', 'Bay_Emporium_32_life_style_16_accessorice.jpg', '', '2017-03-22', '07:04:17', 1, ''),
(33, 16, 'life style', 'accessorice', 'Bay_Emporium_33_life_style_16_accessorice.jpg', '', '2017-03-22', '07:04:17', 1, ''),
(34, 17, 'food', 'cafe', 'Gloria_Jeans_34_food_17_cafe.jpg', '', '2017-03-22', '07:27:35', 1, ''),
(35, 17, 'food', 'cafe', 'Gloria_Jeans_35_food_17_cafe.jpg', '', '2017-03-22', '07:27:35', 1, ''),
(36, 18, 'food', 'cafe', 'North_End_Coffee_Roasters_36_food_18_cafe.jpg', '', '2017-03-22', '07:42:43', 1, ''),
(37, 18, 'food', 'cafe', 'North_End_Coffee_Roasters_37_food_18_cafe.jpg', '', '2017-03-22', '07:42:43', 1, ''),
(38, 19, 'food', 'restaurant cart', 'Fajitas.bd_38_food_19_restaurant_cart.jpg', '', '2017-03-22', '07:58:40', 1, ''),
(39, 19, 'food', 'restaurant cart', 'Fajitas.bd_39_food_19_restaurant_cart.jpg', '', '2017-03-22', '07:58:40', 1, ''),
(40, 20, 'food', 'sweets', 'Khazana_Mithai_40_food_20_sweets.png', '', '2017-03-22', '08:18:23', 1, ''),
(41, 20, 'food', 'sweets', 'Khazana_Mithai_41_food_20_sweets.jpg', '', '2017-03-22', '08:18:23', 1, ''),
(42, 21, 'food', 'cake_pastry', 'My_Donuts_42_food_21_cake_pastry.jpg', '', '2017-03-22', '08:29:39', 1, ''),
(43, 21, 'food', 'cake_pastry', 'My_Donuts_43_food_21_cake_pastry.jpg', '', '2017-03-22', '08:29:39', 1, ''),
(44, 21, 'food', 'cake_pastry', 'My_Donuts_44_food_21_cake_pastry.jpg', '', '2017-03-22', '08:29:39', 1, ''),
(46, 22, 'tourism', 'heritage', 'Tests_heritage_46_tourism_22_heritage.png', '', '2017-03-23', '16:37:01', 2, ''),
(47, 22, 'tourism', 'heritage', 'Tests_heritage_47_tourism_22_heritage.png', '', '2017-03-23', '16:37:01', 2, ''),
(48, 23, 'food', 'cart', 'Khanaâ€™s_48_food_23_cart.jpg', '', '2017-04-04', '04:53:50', 1, ''),
(49, 23, 'food', 'cart', 'Khanaâ€™s_49_food_23_cart.jpg', '', '2017-04-04', '04:53:50', 1, ''),
(50, 23, 'food', 'cart', 'Khanaâ€™s_50_food_23_cart.jpg', '', '2017-04-04', '04:53:50', 1, ''),
(51, 24, 'food', 'cart', 'TAKEOUT_51_food_24_cart.jpg', '', '2017-04-04', '05:08:35', 1, ''),
(52, 24, 'food', 'cart', 'TAKEOUT_52_food_24_cart.jpg', '', '2017-04-04', '05:08:35', 1, ''),
(53, 24, 'food', 'cart', 'TAKEOUT_53_food_24_cart.jpg', '', '2017-04-04', '05:08:35', 1, ''),
(54, 24, 'food', 'cart, restaurant', 'TAKEOUT_54_food_24_cart,_restaurant.jpg', '', '2017-04-04', '05:13:04', 2, ''),
(55, 25, 'food', 'cart', 'Panaâ€™s_Panini_55_food_25_cart.jpg', '', '2017-04-06', '16:20:55', 1, ''),
(56, 25, 'food', 'cart', 'Panaâ€™s_Panini_56_food_25_cart.jpg', '', '2017-04-06', '16:20:55', 1, ''),
(57, 26, 'food', 'cart', 'Wifi_Street_Food_57_food_26_cart.jpg', '', '2017-04-06', '16:35:57', 1, ''),
(58, 26, 'food', 'cart', 'Wifi_Street_Food_58_food_26_cart.jpg', '', '2017-04-06', '16:35:57', 1, ''),
(59, 27, 'food', 'sweets', 'Premium_Sweets_59_food_27_sweets.jpg', '', '2017-04-07', '02:48:59', 1, ''),
(60, 27, 'food', 'sweets', 'Premium_Sweets_60_food_27_sweets.jpg', '', '2017-04-07', '02:48:59', 1, ''),
(61, 27, 'food', 'sweets', 'Premium_Sweets_61_food_27_sweets.jpg', '', '2017-04-07', '02:48:59', 1, ''),
(62, 29, 'food', 'cake_pastry', 'Bread_&_Beyond_62_food_29_cake_pastry.jpg', '', '2017-04-07', '04:17:45', 1, ''),
(63, 29, 'food', 'cake_pastry', 'Bread_&_Beyond_63_food_29_cake_pastry.jpg', '', '2017-04-07', '04:17:45', 1, ''),
(64, 29, 'food', 'cake_pastry', 'Bread_&_Beyond_64_food_29_cake_pastry.jpg', '', '2017-04-07', '04:17:45', 1, ''),
(65, 30, 'food', 'cafe', 'Apon_Coffee_House_65_food_30_cafe.jpg', '', '2017-04-07', '09:12:52', 1, ''),
(66, 30, 'food', 'cafe', 'Apon_Coffee_House_66_food_30_cafe.jpg', '', '2017-04-07', '09:12:52', 1, ''),
(67, 30, 'food', 'cafe', 'Apon_Coffee_House_67_food_30_cafe.jpg', '', '2017-04-07', '09:12:52', 1, ''),
(68, 31, 'food', 'cafe', 'Barista_Lavazza_Bangladesh_68_food_31_cafe.jpg', '', '2017-04-07', '11:27:42', 1, ''),
(69, 31, 'food', 'cafe', 'Barista_Lavazza_Bangladesh_69_food_31_cafe.jpg', '', '2017-04-07', '11:27:42', 1, ''),
(70, 31, 'food', 'cafe', 'Barista_Lavazza_Bangladesh_70_food_31_cafe.jpg', '', '2017-04-07', '11:27:42', 1, ''),
(71, 31, 'food', 'cafe', 'Barista_Lavazza_Bangladesh_71_food_31_cafe.jpg', '', '2017-04-07', '11:27:42', 1, ''),
(72, 32, 'food', 'cafe', 'TABAQ_COFFEE_72_food_32_cafe.jpg', '', '2017-04-07', '14:49:04', 1, ''),
(73, 32, 'food', 'cafe', 'TABAQ_COFFEE_73_food_32_cafe.jpg', '', '2017-04-07', '14:49:04', 1, ''),
(74, 32, 'food', 'cafe', 'TABAQ_COFFEE_74_food_32_cafe.jpg', '', '2017-04-07', '14:49:04', 1, ''),
(75, 33, 'food', 'cafe', 'Coffee_World_75_food_33_cafe.jpg', '', '2017-04-07', '15:39:31', 1, ''),
(76, 33, 'food', 'cafe', 'Coffee_World_76_food_33_cafe.jpg', '', '2017-04-07', '15:39:31', 1, ''),
(77, 33, 'food', 'cafe', 'Coffee_World_77_food_33_cafe.jpg', '', '2017-04-07', '15:39:31', 1, ''),
(78, 35, 'food', 'cafe', 'Beans_&_Aroma_Coffees_78_food_35_cafe.jpg', '', '2017-04-08', '05:20:37', 1, ''),
(79, 35, 'food', 'cafe', 'Beans_&_Aroma_Coffees_79_food_35_cafe.jpg', '', '2017-04-08', '05:20:37', 1, ''),
(80, 35, 'food', 'cafe', 'Beans_&_Aroma_Coffees_80_food_35_cafe.jpg', '', '2017-04-08', '05:20:37', 1, ''),
(81, 36, 'food', 'cafe', 'Crimson_Cup_BD_81_food_36_cafe.jpg', '', '2017-04-08', '05:32:37', 1, ''),
(82, 36, 'food', 'cafe', 'Crimson_Cup_BD_82_food_36_cafe.jpg', '', '2017-04-08', '05:32:37', 1, ''),
(83, 36, 'food', 'cafe', 'Crimson_Cup_BD_83_food_36_cafe.jpg', '', '2017-04-08', '05:32:37', 1, ''),
(84, 37, 'food', 'cafe', 'Georges_Cafe_84_food_37_cafe.jpg', '', '2017-04-08', '06:00:04', 1, ''),
(85, 37, 'food', 'cafe', 'Georges_Cafe_85_food_37_cafe.jpg', '', '2017-04-08', '06:00:04', 1, ''),
(86, 37, 'food', 'cafe', 'Georges_Cafe_86_food_37_cafe.jpg', '', '2017-04-08', '06:00:04', 1, ''),
(87, 38, 'food', 'restaurant', 'Thai_Emerald_87_food_38_restaurant.jpg', '', '2017-04-08', '16:23:53', 1, ''),
(88, 38, 'food', 'restaurant', 'Thai_Emerald_88_food_38_restaurant.jpg', '', '2017-04-08', '16:23:53', 1, ''),
(89, 38, 'food', 'restaurant', 'Thai_Emerald_89_food_38_restaurant.jpg', '', '2017-04-08', '16:23:53', 1, ''),
(90, 39, 'food', 'restaurant', 'Kabab_Factory_90_food_39_restaurant.jpg', '', '2017-04-08', '16:48:58', 1, ''),
(91, 39, 'food', 'restaurant', 'Kabab_Factory_91_food_39_restaurant.png', '', '2017-04-08', '16:48:58', 1, ''),
(92, 39, 'food', 'restaurant', 'Kabab_Factory_92_food_39_restaurant.png', '', '2017-04-08', '16:48:58', 1, ''),
(93, 22, 'tourism', 'heritage', 'Tests_heritage_93_tourism_22_heritage.jpg', '', '2017-04-08', '16:59:39', 2, ''),
(94, 40, 'food', 'restaurant', 'Grand_Nawab_94_food_40_restaurant.jpg', '', '2017-04-08', '17:29:07', 1, ''),
(95, 40, 'food', 'restaurant', 'Grand_Nawab_95_food_40_restaurant.jpg', '', '2017-04-08', '17:29:07', 1, ''),
(96, 40, 'food', 'restaurant', 'Grand_Nawab_96_food_40_restaurant.jpg', '', '2017-04-08', '17:29:07', 1, ''),
(97, 41, 'food', 'restaurant', 'THE_DARK,_Music_Cafe_&_Restaurant_97_food_41_restaurant.jpg', '', '2017-04-09', '08:16:19', 1, ''),
(98, 41, 'food', 'restaurant', 'THE_DARK,_Music_Cafe_&_Restaurant_98_food_41_restaurant.jpg', '', '2017-04-09', '08:16:19', 1, ''),
(99, 41, 'food', 'restaurant', 'THE_DARK,_Music_Cafe_&_Restaurant_99_food_41_restaurant.png', '', '2017-04-09', '08:16:19', 1, ''),
(100, 42, 'food', 'restaurant', 'Bhooter_Bari_100_food_42_restaurant.jpg', '', '2017-04-09', '08:41:34', 1, ''),
(101, 42, 'food', 'restaurant', 'Bhooter_Bari_101_food_42_restaurant.jpg', '', '2017-04-09', '08:41:34', 1, ''),
(102, 42, 'food', 'restaurant', 'Bhooter_Bari_102_food_42_restaurant.jpg', '', '2017-04-09', '08:41:34', 1, ''),
(103, 43, 'food', 'restaurant', 'SBARRO__103_food_43_restaurant.jpg', '', '2017-04-11', '12:21:27', 1, ''),
(104, 43, 'food', 'restaurant', 'SBARRO__104_food_43_restaurant.jpg', '', '2017-04-11', '12:21:28', 1, ''),
(105, 44, 'food', 'restaurant', 'Veni_Vidi_Vici_105_food_44_restaurant.jpg', '', '2017-04-12', '00:11:50', 0, ''),
(106, 44, 'food', 'restaurant', 'Veni_Vidi_Vici_106_food_44_restaurant.jpg', '', '2017-04-12', '00:11:50', 0, ''),
(107, 44, 'food', 'restaurant', 'Veni_Vidi_Vici_107_food_44_restaurant.jpg', '', '2017-04-12', '00:11:50', 0, ''),
(108, 45, 'food', 'restaurant', 'Nandoâ€™s_108_food_45_restaurant.jpg', '', '2017-04-12', '01:12:17', 1, ''),
(109, 45, 'food', 'restaurant', 'Nandoâ€™s_109_food_45_restaurant.jpg', '', '2017-04-12', '01:12:17', 1, ''),
(110, 45, 'food', 'restaurant', 'Nandoâ€™s_110_food_45_restaurant.jpg', '', '2017-04-12', '01:12:17', 1, ''),
(111, 46, 'food', 'restaurant', 'Fish_&_Co._111_food_46_restaurant.jpg', '', '2017-04-12', '02:29:39', 1, ''),
(112, 47, 'food', 'restaurant', 'Rice_&_Noodle_112_food_47_restaurant.jpg', '', '2017-04-12', '02:53:40', 1, ''),
(113, 48, 'food', 'restaurant', 'Street_BBQ_113_food_48_restaurant.jpg', '', '2017-04-12', '03:54:29', 1, ''),
(114, 49, 'food', 'restaurant', 'American_Blend-_Burger_&_Beyond_114_food_49_restaurant.png', '', '2017-04-12', '04:18:26', 1, ''),
(115, 49, 'food', 'restaurant', 'American_Blend-_Burger_&_Beyond_115_food_49_restaurant.jpg', '', '2017-04-12', '04:18:26', 1, ''),
(116, 49, 'food', 'restaurant', 'American_Blend-_Burger_&_Beyond_116_food_49_restaurant.png', '', '2017-04-12', '04:18:26', 1, ''),
(117, 50, 'food', 'restaurant', 'Oregano_117_food_50_restaurant.jpg', '', '2017-04-12', '04:43:39', 1, ''),
(118, 50, 'food', 'restaurant', 'Oregano_118_food_50_restaurant.png', '', '2017-04-12', '04:43:39', 1, ''),
(119, 50, 'food', 'restaurant', 'Oregano_119_food_50_restaurant.jpg', '', '2017-04-12', '04:43:39', 1, ''),
(120, 51, 'food', 'restaurant', 'Comic_Cafe_120_food_51_restaurant.jpg', '', '2017-04-12', '04:58:11', 1, ''),
(121, 51, 'food', 'restaurant', 'Comic_Cafe_121_food_51_restaurant.jpg', '', '2017-04-12', '04:58:11', 1, ''),
(122, 52, 'food', 'restaurant', 'Mad_chef_122_food_52_restaurant.jpg', '', '2017-04-12', '05:12:57', 1, ''),
(123, 52, 'food', 'restaurant', 'Mad_chef_123_food_52_restaurant.jpg', '', '2017-04-12', '05:12:57', 1, ''),
(124, 53, 'food', 'restaurant', 'Food_Republic_124_food_53_restaurant.jpg', '', '2017-04-12', '08:54:23', 1, ''),
(125, 53, 'food', 'restaurant', 'Food_Republic_125_food_53_restaurant.jpg', '', '2017-04-12', '08:54:23', 1, ''),
(126, 53, 'food', 'restaurant', 'Food_Republic_126_food_53_restaurant.jpg', '', '2017-04-12', '08:54:23', 1, ''),
(127, 54, 'food', 'restaurant', 'Peda_Ting_Ting_127_food_54_restaurant.jpg', '', '2017-04-12', '12:57:25', 0, ''),
(128, 54, 'food', 'restaurant', 'Peda_Ting_Ting_128_food_54_restaurant.jpg', '', '2017-04-12', '12:57:25', 0, ''),
(129, 54, 'food', 'restaurant', 'Peda_Ting_Ting_129_food_54_restaurant.jpg', '', '2017-04-12', '12:57:25', 0, ''),
(130, 56, 'food', 'cafe', 'Coffee_Time_130_food_56_cafe.jpg', '', '2017-04-12', '17:35:14', 1, ''),
(131, 56, 'food', 'cafe', 'Coffee_Time_131_food_56_cafe.jpg', '', '2017-04-12', '17:35:14', 1, ''),
(132, 56, 'food', 'cafe', 'Coffee_Time_132_food_56_cafe.jpg', '', '2017-04-12', '17:35:14', 1, ''),
(133, 56, 'food', 'cafe', 'Coffee_Time_133_food_56_cafe.jpg', '', '2017-04-12', '17:35:14', 1, ''),
(134, 57, 'food', 'cafe', 'Popeyes_Coffee_&_Fast_Food_134_food_57_cafe.jpg', '', '2017-04-13', '07:53:42', 1, ''),
(135, 57, 'food', 'cafe', 'Popeyes_Coffee_&_Fast_Food_135_food_57_cafe.jpg', '', '2017-04-13', '07:53:42', 1, ''),
(136, 57, 'food', 'cafe', 'Popeyes_Coffee_&_Fast_Food_136_food_57_cafe.jpg', '', '2017-04-13', '07:53:42', 1, ''),
(137, 58, 'food', 'cafe cake_pastry', 'Bittersweet_Cafe_137_food_58_cafe_cake_pastry.jpg', '', '2017-04-13', '13:01:54', 1, ''),
(138, 58, 'food', 'cafe cake_pastry', 'Bittersweet_Cafe_138_food_58_cafe_cake_pastry.png', '', '2017-04-13', '13:01:54', 1, ''),
(139, 58, 'food', 'cafe cake_pastry', 'Bittersweet_Cafe_139_food_58_cafe_cake_pastry.jpg', '', '2017-04-13', '13:01:54', 1, ''),
(140, 58, 'food', 'cafe cake_pastry', 'Bittersweet_Cafe_140_food_58_cafe_cake_pastry.jpg', '', '2017-04-13', '13:01:54', 1, ''),
(141, 59, 'food', 'cafe cake_pastry', 'The_Coffee_Bean_&_Tea_Leaf,_Bangladesh_141_food_59_cafe_cake_pastry.png', '', '2017-04-15', '06:13:36', 1, ''),
(142, 59, 'food', 'cafe cake_pastry', 'The_Coffee_Bean_&_Tea_Leaf,_Bangladesh_142_food_59_cafe_cake_pastry.jpg', '', '2017-04-15', '06:13:36', 1, ''),
(143, 59, 'food', 'cafe cake_pastry', 'The_Coffee_Bean_&_Tea_Leaf,_Bangladesh_143_food_59_cafe_cake_pastry.jpg', '', '2017-04-15', '06:13:36', 1, ''),
(144, 59, 'food', 'cafe cake_pastry', 'The_Coffee_Bean_&_Tea_Leaf,_Bangladesh_144_food_59_cafe_cake_pastry.png', '', '2017-04-15', '06:13:36', 1, ''),
(145, 60, 'food', 'restaurant cafe', 'Brews_&_Bites_145_food_60_restaurant_cafe.jpg', '', '2017-04-18', '06:06:42', 1, ''),
(146, 60, 'food', 'restaurant cafe', 'Brews_&_Bites_146_food_60_restaurant_cafe.jpg', '', '2017-04-18', '06:06:42', 1, ''),
(147, 60, 'food', 'restaurant cafe', 'Brews_&_Bites_147_food_60_restaurant_cafe.jpg', '', '2017-04-18', '06:06:42', 1, ''),
(148, 61, 'food', 'cafe', 'PappaRoti_148_food_61_cafe.jpg', '', '2017-04-18', '08:27:31', 1, ''),
(149, 61, 'food', 'cafe', 'PappaRoti_149_food_61_cafe.jpg', '', '2017-04-18', '08:27:31', 1, ''),
(150, 61, 'food', 'cafe', 'PappaRoti_150_food_61_cafe.jpg', '', '2017-04-18', '08:27:31', 1, ''),
(151, 61, 'food', 'cafe', 'PappaRoti_151_food_61_cafe.jpg', '', '2017-04-18', '08:27:31', 1, ''),
(152, 62, 'food', 'restaurant cake_pastry', 'California_Fried_Chicken_and_Pastry_Shop_152_food_62_restaurant_cake_pastry.jpg', '', '2017-04-18', '15:36:38', 1, ''),
(153, 62, 'food', 'restaurant cake_pastry', 'California_Fried_Chicken_and_Pastry_Shop_153_food_62_restaurant_cake_pastry.jpg', '', '2017-04-18', '15:36:38', 1, ''),
(154, 62, 'food', 'restaurant cake_pastry', 'California_Fried_Chicken_and_Pastry_Shop_154_food_62_restaurant_cake_pastry.jpg', '', '2017-04-18', '15:36:38', 1, ''),
(155, 62, 'food', 'restaurant cake_pastry', 'California_Fried_Chicken_and_Pastry_Shop_155_food_62_restaurant_cake_pastry.jpg', '', '2017-04-18', '15:36:38', 1, ''),
(156, 63, 'food', 'cake_pastry', 'Shumiâ€™s_Hot_Cake_156_food_63_cake_pastry.jpg', '', '2017-04-20', '01:07:34', 1, ''),
(157, 63, 'food', 'cake_pastry', 'Shumiâ€™s_Hot_Cake_157_food_63_cake_pastry.jpg', '', '2017-04-20', '01:07:34', 1, ''),
(158, 63, 'food', 'cake_pastry', 'Shumiâ€™s_Hot_Cake_158_food_63_cake_pastry.jpg', '', '2017-04-20', '01:07:34', 1, ''),
(159, 63, 'food', 'cake_pastry', 'Shumiâ€™s_Hot_Cake_159_food_63_cake_pastry.jpg', '', '2017-04-20', '01:07:34', 1, ''),
(160, 64, 'food', 'cake_pastry', 'Holey_Artisan_Bakery_160_food_64_cake_pastry.jpg', '', '2017-04-20', '07:16:22', 1, ''),
(161, 64, 'food', 'cake_pastry', 'Holey_Artisan_Bakery_161_food_64_cake_pastry.jpg', '', '2017-04-20', '07:16:22', 1, ''),
(162, 64, 'food', 'cake_pastry', 'Holey_Artisan_Bakery_162_food_64_cake_pastry.jpg', '', '2017-04-20', '07:16:22', 1, ''),
(163, 65, 'food', 'cake_pastry', 'Cake_World_163_food_65_cake_pastry.jpg', '', '2017-04-20', '09:35:00', 1, ''),
(164, 65, 'food', 'cake_pastry', 'Cake_World_164_food_65_cake_pastry.jpg', '', '2017-04-20', '09:35:01', 1, ''),
(165, 65, 'food', 'cake_pastry', 'Cake_World_165_food_65_cake_pastry.jpg', '', '2017-04-20', '09:35:01', 1, ''),
(166, 65, 'food', 'cake_pastry', 'Cake_World_166_food_65_cake_pastry.jpg', '', '2017-04-20', '09:35:01', 1, ''),
(167, 66, 'food', 'cake_pastry', 'Krispy_Kreme_Bangladesh_167_food_66_cake_pastry.jpg', '', '2017-04-20', '16:11:38', 1, ''),
(168, 66, 'food', 'cake_pastry', 'Krispy_Kreme_Bangladesh_168_food_66_cake_pastry.jpg', '', '2017-04-20', '16:11:38', 1, ''),
(169, 66, 'food', 'cake_pastry', 'Krispy_Kreme_Bangladesh_169_food_66_cake_pastry.jpg', '', '2017-04-20', '16:11:38', 1, ''),
(170, 66, 'food', 'cake_pastry', 'Krispy_Kreme_Bangladesh_170_food_66_cake_pastry.png', '', '2017-04-20', '16:11:38', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_info`
--

CREATE TABLE IF NOT EXISTS `general_info` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `type` varchar(250) NOT NULL,
  `column_1` varchar(1500) NOT NULL,
  `column_2` varchar(1500) NOT NULL,
  `column_3` varchar(1500) NOT NULL,
  `column_4` varchar(1500) NOT NULL,
  `column_5` varchar(1500) NOT NULL,
  `column_6` varchar(1500) NOT NULL,
  `column_7` varchar(1500) NOT NULL,
  `column_8` varchar(1500) NOT NULL,
  `column_9` varchar(1500) NOT NULL,
  `column_10` varchar(1500) NOT NULL,
  `column_11` varchar(1500) NOT NULL,
  `column_12` varchar(1500) NOT NULL,
  `column_13` varchar(1500) NOT NULL,
  `column_14` varchar(1500) NOT NULL,
  `column_15` varchar(1500) NOT NULL,
  `column_16` varchar(1500) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `social_links` varchar(500) NOT NULL,
  `contact` varchar(300) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(1500) NOT NULL,
  `total_avg_review` float NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `total_view` int(255) NOT NULL,
  `daily_view` int(250) NOT NULL,
  `weekly_view` int(250) NOT NULL,
  `monthly_view` int(250) NOT NULL,
  `yearly_view` int(250) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_info`
--

INSERT INTO `general_info` (`id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `type`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `column_16`, `branches`, `web_site`, `social_links`, `contact`, `main_location`, `location`, `map`, `total_avg_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `total_view`, `daily_view`, `weekly_view`, `monthly_view`, `yearly_view`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'natural_site', 'tourism', 'Bisnakandi', 'Bisnakandi_1_tourism_natural_site.jpg', 'Bisnakandi is a different flavor of natural beauty in the country, a border village in north Gowainghat Upazila in Sylhet. Gowainghat was, nestled between the Meghalaya hills (INDIA) and the plains of Sylhet. This is where many layers of the Khasi mountain meet at a single point from both sides.\r\nYou will find various kinds of stones over which water flows. Rainy season is the best time to visit the site. Water flowing over the stones gives a unique look. In the rainy season, the site looks spectacular. The dark clouds hug the mountains in the rainy season. Winter is not the suitable time to visit the site. In winter, there will be a lot of stone-laden boats, Lorries, and mechanized mining. So, everyone should make the plan to visit the site in the rainy season.\r\n', '', '', 'None', 'None', 'None', '', '', 'Very sleepy area. So, need to be alarted', 'The distance from Dhaka is 315 kilometers away. It is 25 kilometers away from Sylhet. You can go to the site via Sylhet-Gowainghat road. After reaching Hadarpar you have to take a boat to reach Bisnakandi. You can go to Hadarpar by auto-rickshaws. You will get the autos at the Amberkhana point in Sylhet. One can reach the site going to Pangthumai first.', '', '', '', '', '', '', '', '', '', '', '', '', 'Array', 'Bisnakandi, Gowainghat, Sylhet, Bangladesh', 'width=100% height=250px src', 0, 0, 0, 0, 0, 310, 310, 310, 310, 310, '2017-03-19', '11:04:44', 1, 'approved'),
(2, 'heritage', 'tourism', 'Lalbagh Fort (Lalbagh Kella)', 'Lalbagh Fort (Lalbagh Kella)_2_tourism_heritage.jpg', 'Lalbagh Fort is an incomplete Mughal palace fortress beside the river Buriganga in the southwestern part of the old Dhaka city. Prince Muhammed Azam, third son of Moghul Emperor Aurangzeb laid the construction in 1678-9. Continued by the next viceroy, Shaista Khan, its building stopped after the death of his daughter, Iran Dukht, popularly known as Pari Bibi, whose tomb is one of the monuments on the site. For long the fort was considered to be a combination of three buildings (the mosque, the tomb of Pari Bibi and the Diwan-i-Aam), two gateways and a portion of the partly damaged fortification wall. But recently Dept. of Archaeology found existence of other structures. In the total area of Lalbagh fort there are royal gardens and a drainage system. This has become a key tourist attraction in Dhaka. Almost 3million people visit this place every year. ', '', '', 'Summer Season (April â€“ September) ïƒ˜	Tuesday â€“ Saturday: 10:00AM â€“ 01:00PM, 01:30PM â€“ 06:00PM ïƒ˜	Friday: 10:00AM â€“ 12:30PM, 02:30PM â€“ 06:00PM ïƒ˜	Monday: 02:30PM â€“ 06:00PM Winter Season (October â€“ March) ïƒ˜	Tuesday â€“ Saturday: 09:00AM â€“ 01:00PM, 01:30PM â€“ 05:00PM ïƒ˜	Friday: 09:00AM â€“ 12:30PM, 02:00PM â€“ 05:00PM ïƒ˜	Monday: 01:30PM â€“ 05:00PM â€¢	The fort remains closed during govt. Holidays.', 'Sunday', 'Local Tourist: 20tk SAARC Tourist: 100tk Other Foreigner: 200tk', '', '', 'Outside food is not allowed inside the fort', '', '', '', '', '', '', '', '', '', '', '', '', 'Tele: 9673018', 'Array', 'Lalbagh, Puran Dhaka, Banglaadesh ', 'width=100% height=250px src', 0, 0, 0, 0, 0, 55, 55, 55, 55, 55, '2017-03-19', '11:25:08', 1, 'approved'),
(3, 'picnic_spot', 'tourism', 'Mohammadi  Garden', 'Mohammadi  Garden_3_tourism_picnic_spot.jpg', 'Mohammadi Garden is a beautiful picnic spot in dhaka. It is situated five Kilometre North of Kalampur Bus stand, Dhamrai. There is a big swimming pool, nagor dola, 2 field for outdoor sports, fishing and boating in the pond, Kids sports zone and catering facility. For the visitors they have 4 rest room and also 1 VVIP rest room.', '', 'Mohammadi Garden picnic spotâ€™s total Land Capacity is arround 40 Bigga', 'Anyday on booking', '', '', '', '', '', 'You can go there from Dhaka by CNG, bus or by private transport. It''ll take around 2 hours.', '', '', '', '', '', '', '', '', '', '', '', 'Phone: +8801715-043416, 01717374904, 01190257062', 'Array', 'Kalampur,Mohisati,Dhamrai, Dhaka', 'width=100% height=250px src', 0, 0, 0, 0, 0, 59, 59, 59, 59, 59, '2017-03-19', '11:50:34', 1, 'approved'),
(4, 'lake_park', 'tourism', 'Dhanmondi Lake', 'Dhanmondi Lake_4_tourism_lake_park.jpg', 'Located in the middle of Dhaka city, within the prosperous Dhanmondi residential area, Dhanmondi Lake is a popular tourist attraction, with lush green vegetation, a lakeside walkway and benches for visitors to sit and relax on.  The lake was originally a dead channel of the Karwan Bazar River, and was connected to the Turag River. The lake is partially connected with the Begunbari Canal. In 1956, Dhanmondi was developed as a residential area. In the development plan, about 16% of the total area of Dhanmondi was designated for the lake.\r\nThe lake has become a well visited tourist spot, with cultural hubs such as the Rabindra-Sarobar located along its side. There are many food courts and small restaurants near nicely planned side walks of the lake. Dingi, Panshi, Shampaan are one of the popular restaurants beside the lake. Tea, soft drinks, pickles, pithas and ice-creams are available in the walkways too. Starting from Jigatola the lake extends up to road # 27. It is 3 km in length, 35-100m in width, with a maximum depth of 4.77m and the total area of the water body is 37.37 ha.\r\n', '', '', 'From morning till 8:00PM', 'None', 'None', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/DhanmondiLakeBD/', '', 'Array', 'Dhanmondi Lake Rd, Dhanmondi, Dhaka, Bangladesh', 'width=100% height=250px src', 0, 0, 0, 0, 0, 98, 98, 98, 98, 98, '2017-03-19', '12:01:11', 1, 'approved'),
(5, 'resort', 'residence', 'Nokkhottrobari Resort & Conference Center', 'Nokkhottrobari Resort & Conference Center_5_residence_resort.jpg', 'Nokkhottrobari is an introvert concept designed by Tauquir Ahmed - Architect, Actor and Filmmaker, and his wife Bipasha Hayat - Actor, Playwright and Painter. This project is their vision to serve people with a place of relaxation and a fun place for all the members of the family. \r\nNokkhottrobari Resort & Conference Center is a beautiful resort hidden in the green village of Chinashukhania, just 30 mn drive from Rajendrapur in Gazipur. Nokkhottrobari Resort which consists of a main brick and mortar complex and a number of luxury water bungalows built of wood and featuring thatched roof, has a swimming pool and a jacuzzi. \r\nEach air-conditioned room is fitted with a flat-screen TV with cable service, a refrigerator, a desk, and a fan. Nokkhottrobari Resort has an on-site restaurant managed by experienced chefs. Guests can order Italian, Japanese, Thai/Chinese, Indian/Bangladeshi and continental dishes or can request for barbecues. There is also an on-site bar that serves fruit juices, tea, coffee, and cocktails. The resort has a jacuzzi, an outdoor pool, and organizes boat rides for adventurous guests. There is a pool table, a table tennis board, and a kid zone. \r\nThe hotel has 2 conference halls for hosting seminars, presentations, and professional training programs. \r\n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.nokkhottrobari.com/ ', 'https://www.facebook.com/Nokkhottrobari/ ', 'Office:  House: 157/B (2nd Floor), Road: 22  Mohakhali New DOHS, Dhaka 1206, Bangladesh. Phone: +88 01712 882179, +8802-9835173, 01771-799410, 01190980214, 01195356165 Email: nokkhottrobari@gmail.com , nokkhottrobari@live.com  ', 'Array', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, 83, 83, 83, 83, 83, '2017-03-19', '12:19:25', 1, 'approved'),
(6, 'restaurant', 'food', 'Shad Tehari Ghar', 'Shad Tehari Ghar_6_food_restaurant.jpg', 'Tasty and delicious tehari from Dhaka Lalmatiaâ€™s famous for â€œShad Tehari Gharâ€. Prepared by fine rice, beef, and various spices. Itâ€™s really awesome.  Takes order for parties', 'Tehari and other items', '', 'Chicken tehari', '<b>Pohela Boishak Offer</b><br>\r\nà¦–à¦¿à¦šà§à§œà¦¿+à¦¡à¦¿à¦® à¦­à¦¾à¦œà¦¾+à¦Ÿà¦¾à¦•à¦¿ à¦­à¦°à§à¦¤à¦¾+à¦¬à§‡à¦—à§à¦¨ à¦­à¦°à§à¦¤à¦¾+à¦•à¦¾à¦²à¦œà¦¿à¦°à¦¾ à¦­à¦°à§à¦¤à¦¾à¦° à¦¸à§à¦ªà§‡à¦¶à¦¾à¦² à¦ªà§à¦¯à¦¾à¦•à§‡à¦œ à¦®à¦¾à¦¤à§à¦° à§¨à§¨à§« à¦Ÿà¦¾à¦•à¦¾à§Ÿ!', 'Mon-Sun 08:00 AM â€“ 11:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/shadteharighar/ ', '', '', '', '', 0, 0, 0, 0, 0, 47, 47, 47, 47, 47, '2017-03-19', '14:52:44', 2, 'approved'),
(7, 'hotel', 'residence', 'Padma Resort', 'Padma Resort_7_residence_hotel.jpg', 'Ranked 2nd As Nature Based Resort Of Bangladesh ', 'Natural Resort', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.padmaresort.net', '', '<br><b>Booking Office, Dhaka</b> <br>House # 380, Road # 28(Ground Floor), <br>Mohakhali DOHS, Dhaka-1206, Bangladesh. <br>Phone # 01712-170330, 01752-987688 (10am - 6pm) <br><b>Email:</b>  info@padmaresort.net    <br><b>In case of Emergency:</b> <br>General Manager  <br>S.M. Nazrul Islam  <br>Phone', 'Array', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, 87, 87, 87, 87, 87, '2017-03-19', '15:06:35', 1, 'approved'),
(8, 'hotel', 'residence', 'The Westin Dhaka', 'The Westin Dhaka_8_residence_hotel.jpg', '', 'Five Star Hotel', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.starwoodhotels.com/westin/property/over', 'https://www.facebook.com/westin, https://twitter.com/westin, https://www.instagram.com/westin/', 'Phone: (880)(2) 9891988', 'Array', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, 99, 99, 99, 99, 99, '2017-03-19', '15:41:26', 1, 'approved'),
(9, 'restaurant', 'food', 'Hakka Dhaka', 'Hakka Dhaka	_9_food_restaurant.jpg', 'Hakka Cuisine is the cooking style of the Hakka people, who originated mainly from the South Eastern Chinese province of Guangdong ', 'Chinese Restaurant', '', '', '', '12:30PM â€“ 10:30PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Banani: https://www.facebook.com/hakkadhaka/  Uttara: https://www.facebook.com/pages/HAKKA-DHAKA-Uttara/653764884733478', 'Banani: 01616666543<br> Uttara: 01616666544', '', '', '', 0, 0, 0, 0, 0, 1119, 1119, 1119, 1119, 1119, '2017-03-19', '15:54:55', 2, 'approved'),
(10, 'restaurant', 'food', 'adasd', 'adasd_10_food_restaurant.jpg', 'asdasdasdad', 'asdasd', '', 'dasdsada', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, '2017-03-19', '16:16:25', 2, 'not approved'),
(11, 'restaurant', 'food', 'The Mughal Kitchen', 'The_Mughal_Kitchen_11_food_restaurant.jpg', 'Mughal food is legendary and a priceless heritage. The Mughals ruled in India for many centuries and thus Indian food is incorporated with the richness of Mughlai khana.\r\n\r\nLocated at Gulshan, The Mughal kitchen is open to customers from 12:30 pm to 10:30 pm on weekdays with the exception of Thursday and Friday till 1am. The ambiance is bright however, proper lighting and decoration is needed to provide customers the feeling of dining at a posh restaurant that complements the Name.\r\n', 'Indian Food ', '', '', '', '12:30 PM â€“ 10:45 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/TheMughalKitchen/', 'Phone: +88029854110, +8801841684425', '', '', '', 0, 0, 0, 0, 0, 137, 137, 137, 137, 137, '2017-03-19', '21:52:02', 2, 'approved'),
(12, 'fashion', 'life style', 'Yellow', 'Yellow_12_life_style_fashion.png', 'YELLOW, the trendiest fashion brand from Bangladesh, is mostly distinguished for its true international quality designs and fabrics. We are inspired by our customers- souls full of unconventional fashion senses. As a retailer of our parent brand BEXIMCO, we started our journey in 2004 and now we have 15 outlets across Bangladesh and Pakistan including a 24/7 online store. Since origin we have been offering world class designs at amazing value price. Our product line includes a wide range of fashion clothing, fragrance, and accessories for men, women and children; textiles for home decoration; avant-garde ceramic items; paintings; books; and many more.\r\n\r\nExplore YELLOW and look through our windows for contemporary global fashion trends.\r\n', 'Fashion Store', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.yellowclothing.net/ ', 'https://www.facebook.com/YellowClothing/', '', '', '', '', 0, 0, 0, 0, 0, 46, 46, 46, 46, 46, '2017-03-22', '04:39:13', 1, 'approved'),
(13, 'accessorice', 'life style', 'Diamond World Ltd.', 'Diamond_World_Ltd._13_life_style_accessorice.jpg', 'We have 13 diamond jewellery showroom in Bangladesh.  One & only authorized platinum retailer in Bangladesh.  100% Certified Jewelry. Life Time Exchange. Free Karatage checking of your Gold and Diamond Jewellery', 'Jewellery Shop', '', '', 'Only Online E-commerce website in Bangladesh', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://diamondworldltd.com/ ', 'https://www.facebook.com/DiamondWorldLtd/ ', '', '', '', '', 0, 0, 0, 0, 0, 33, 33, 33, 33, 33, '2017-03-22', '05:16:22', 1, 'approved'),
(14, 'parlor_salon', 'life style', 'Farzana Shakilâ€™s Makeover Salon Ltd.', 'Farzana_Shakils_Makeover_Salon_Ltd._14_life_style_parlor_salon.png', 'Beauty, Lifestyle & Beyond', 'Beauty Parlor', '', '', '', '', '', '9:00 AM â€“ 8:30 PM	', '', '', '', '', '', '', '', '', '', '', '', 'http://www.farzanashakils.com/ ', 'https://www.facebook.com/FSMSL/ ', '', '', '', '', 0, 0, 0, 0, 0, 24, 24, 24, 24, 24, '2017-03-22', '05:26:10', 1, 'approved'),
(15, 'gym_fitness', 'life style', 'Adonize Fitness Center LTD', 'Adonize_Fitness_Center_LTD_15_life_style_gym_fitness.jpg', 'Founded on December 1, 2012, helps you to get your desire shape and fitness.  It has highly trained instructors , physicians and hundreds of exercise equipments are available here with friendly atmosphere. For both men and women', 'Gym and Fitness center', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/AdonizeFitnessCenterLtd/ ', 'Phone : 8158864-5,Mobile:01731922942 (office),  02-8158864  <br>Email:  ronyislam99@gmail.com', '', '', '', 0, 0, 0, 0, 0, 19, 19, 19, 19, 19, '2017-03-22', '05:33:59', 1, 'approved'),
(16, 'accessorice', 'life style', 'Bay Emporium', 'Bay_Emporium_16_life_style_accessorice.jpg', 'Bay Emporium Ltd was established in 2006. It is a footwear marketing company with wide range of products for men, women & children. Its target customer is medium to medium high income groups. Today it is one of the major shoe retailing companies. The brand is getting popular very fast. New shops are opened in strategic locations. Famous for leather shoes & leather sandals.', 'Footwear Company', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.bayemporium-bd.com/ ', 'https://www.facebook.com/pages/Bay-Emporium/176441382396505 ', '  BAY EMPORIUM LIMITED <br>GROUND FLOOR & FIRST FLOOR, AFSANA TRANSPERENT TOWER, <br>18 ALAUL AVENUE, SECTOR 6, UTTARA 1230, BANGLADESH <br>PHONE: +88 02 58951677', '', '', '', 0, 0, 0, 0, 0, 19, 19, 19, 19, 19, '2017-03-22', '07:04:17', 1, 'approved'),
(17, 'cafe', 'food', 'Gloria Jeanâ€™s', 'Gloria_Jeans_17_food_cafe.jpg', 'At Gloria Jeanâ€™s Coffees, we are committed to making the ultimate coffee experience for you. Thatâ€™s why we put so much passion into selecting the worldâ€™s highest quality Arabica beans. ', 'International Coffe Shop', '', 'World Class Coffee', '', 'Dhanmondi: 8 AM â€“ 12 AM <br>Gulshan-1: 8 AM â€“ 1 AM <br>Gulshan-2: 7 AM â€“ 1:30 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.gloriajeanscoffees.com/bd/Home.aspx', 'https://www.facebook.com/gloriajeansjcoffeesbangladesh', 'Phone: +8801929333888, <br>Tele: +801929333999', '', '', '', 0, 0, 0, 0, 0, 144, 144, 144, 144, 144, '2017-03-22', '07:27:35', 1, 'approved'),
(18, 'cafe', 'food', 'North End Coffee Roasters', 'North_End_Coffee_Roasters_18_food_cafe.jpg', 'North End Coffee Roasters is the dream of Rick and Chris Hubbard, an American couple currently living in Dhaka, Bangladesh. Rick is a coffee roaster who previously opened and managed a Starbucks Coffee store stateside. Opening hours slightly varries branch to branch. ', 'Coffee Shop', '', 'They also sell coffee beans', '', '8:30 AM â€“ 9:3o PM ', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.northendcoffee.com/', 'https://www.facebook.com/NORTHENDcoffee/', 'Phone: 01741055597, 01941788506', '', '', '', 0, 0, 0, 0, 0, 45, 45, 45, 45, 45, '2017-03-22', '07:42:43', 1, 'approved'),
(19, 'restaurant cart', 'food', 'Fajitas.bd', 'Fajitas.bd_19_food_restaurant_cart.jpg', 'Provides maxican tastes in Bangladesh. Price and product availability might varry from branch to branch', 'Maxican Food ', '', '', '', '11:00am â€“ 10:pm <br>(arround)', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/fajitas.bd/ ', '', '', '', '', 0, 0, 0, 0, 0, 138, 138, 138, 138, 138, '2017-03-22', '07:58:40', 1, 'approved'),
(20, 'sweets', 'food', 'Khazana Mithai', 'Khazana_Mithai_20_food_sweets.jpg', 'Welcome to our Khazana Mithai...\r\n\r\n<br>True to its name Khazana offers a treasure trove of sweets from undivided Bengal. From regular favorites like Gulab Jamun and Rosogolla to such exotic delicacies as the succulent Chanar Payesh to tempting Motipak, Khazanaâ€™s range of assorted sweets promises that same quality and excellence that that brand Khazana is known for.\r\n\r\n<br>Each item that comes out of Khazanaâ€™s state of art kitchen is created with utmost care and diligence making it a gourmet loverâ€™s delight.\r\n', 'Snacks, Indian and Bengali sweets', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://khazanamithai.com/ ', 'https://www.facebook.com/Khazana-Mithai-1523953937844355/ ', 'Phone: +88-02-9898378, 8814849,01787676572, 01787676573 <br>Email: khazanamithai@yahoo.com', '', '', '', 0, 0, 0, 0, 0, 48, 48, 48, 48, 48, '2017-03-22', '08:18:23', 1, 'approved'),
(21, 'cake_pastry', 'food', 'My Donuts', 'My_Donuts_21_food_cake_pastry.png', 'Introducing My Donuts... a Bangladeshi donuts chain shop. My Donuts is a bakery shop specially for donuts. It is very new to the market, the core of this business is to avail quality bakery products from Australia. Deliver the most possible miost and fresh cakes & donuts in time. Awasome taste and good quality ingredients puss customers to order us again & again.', 'Donuts & cake shop', '', '', 'Buy 5 donuts get 1 FREE !!!', 'Tuesday to Monday <br>9am till 11pm', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://mmpp209.wix.com/mydonuts', 'https://www.facebook.com/mydonutsshop/', 'Phone: 01760822356 <br>Email: burgernboost@gmail.com', '', '', '', 0, 0, 0, 0, 0, 60, 60, 60, 60, 60, '2017-03-22', '08:29:39', 1, 'approved'),
(22, 'heritage', 'tourism', 'Test''s (heritage)', 'Tests_heritage_22_tourism_heritage.jpg', '/hgkghjkghk/gjghj', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-03-23', '16:37:01', 2, 'not approved'),
(23, 'cart', 'food', 'Khanaâ€™s', 'Khanaâ€™s_23_food_cart.jpg', 'Inspire by street food business by the name Ande Khana Since 2010. We had an incredible success. Now we will be serving with Khanaâ€™s', 'Food Cart	', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/khanaseat/ ', 'Phone: 01715-818714 ', '', '', '', 0, 0, 0, 0, 0, 57, 57, 57, 57, 57, '2017-04-04', '04:53:50', 1, 'approved'),
(24, 'cart, restaurant', 'food', 'TAKEOUT', 'TAKEOUT_24_food_cart.jpg', 'A popular burger stand in Dhaka', 'Burger Restaurant', '', '', '', 'Dhanmondi:  <br>Mon:	 11:30 am - 9:45 pm <br>Tue:	 2:00 pm - 9:45 pm <br>Wed-Sun:	 11:30 am - 9:45 pm  <br>Banani: 11:30 am â€“ 10:45 pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Banani : https://www.facebook.com/Takeout.banani/ <br>Dhanmondi:https://www.facebook.com/takeout.dhanmondi/', '', '', '', '', 0, 0, 0, 0, 0, 162, 162, 162, 162, 162, '2017-04-04', '05:08:35', 1, 'approved'),
(25, 'cart', 'food', 'Panaâ€™s Panini', 'Panaâ€™s_Panini_25_food_cart.jpg', 'A popular food stand', 'Fast Food', '', '', '', '10:30AM â€“ 10:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/panaspanini/ ', 'Phone: 01913213949 <br>Email: mr.manik0001@gmail.com', '', '', '', 0, 0, 0, 0, 0, 10, 10, 10, 10, 10, '2017-04-06', '16:20:55', 1, 'approved'),
(26, 'cart', 'food', 'Wifi Street Food', 'Wifi_Street_Food_26_food_cart.jpg', 'Are u ready to eat food or drink sold in a street and enjoy free Wifi!!?!!... come "eat n adda" em all', 'Food Cart	', '', '', '', '4:00 PM â€“ 10:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/WifiStreetFood/ ', 'Phone: 01716-937871', '', '', '', 0, 0, 0, 0, 0, 15, 15, 15, 15, 15, '2017-04-06', '16:35:57', 1, 'approved'),
(27, 'sweets', 'food', 'Premium Sweets', 'Premium_Sweets_27_food_sweets.jpg', 'We sincerely believe in long-term impact of complete satisfaction. \r\n<br>Premium Sweets is not just a mere coincidence. Since 1999, under Canadian management, it has been a strict tradition of creating outstanding desserts, chic presentation, discriminating packaging, submitting service by pleasant professional staffs in an ambience that complement the lifestyle of real influencers. Eventually slow cooked signature savoury was introduced in clay pot, which combines extraordinary richness, premium quality, and iconic style. <br>Today, upscale patrons in 14 countries perceive Premium Sweets desserts, savoury and snacks at a functional and emotional level. \r\n<br>Headquartered in Mississauga Ontario, the company manages multiple production units, retail outlets, inflight catering of dairy based sweets and slow cooked ready to eat savoury meals in Canada and Bangladesh and exports to USA, Europe, Australia and other part of Asia.\r\n', 'Sweet Shop', '', '', '', 'Sat â€“ Fri  <br>09:00 AM â€“ 10:00 PM ', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.premiumsweets.ca/ ', 'https://www.facebook.com/premium.sweets/ ', 'Phone: 01759115124', '', '', '', 0, 0, 0, 0, 0, 26, 26, 26, 26, 26, '2017-04-07', '02:48:59', 1, 'approved'),
(28, 'cake_pastry', 'food', 'Glazed', 'Glazed_28_food_cake_pastry.png', 'In the spring of 2012, three donut aficionados came together to create the worldâ€™s best donut. This was no easy task, due to the limited resources in Bangladesh. So, they travelled across multiple continents to sample and source the worldâ€™s finest ingredients. After an extensive period of research and development, they finally developed the winning recipe for their scrumptious original glazed, as well as fourteen other amazing flavors. With confidence in their product, they prepared to create and establish Glazed. A multi store donut and coffee shop that sells fresh donuts that meet world standards of quality, taste, and hygiene.\r\n\r\n<br>This led the Glazed team to the next inevitable question: whatâ€™s a great donut without great coffee? After all, thereâ€™s nothing more satisfying than washing down a great donut with a great coffee. For this we have picked premium coffee beans especially blended for Glazed Coffee to complement our gourmet donuts.\r\n\r\n<br>A sister concern of Zen Treats Ltd.\r\n', 'Donut & Coffee Shop', '', 'Fresh made donuts', '', 'Mon-Sun: 10:00AM â€“ 10:00PM', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.glazedbd.com', 'https://www.facebook.com/glazedbd/ ', 'Phone: 01611 452933, 01971 452933', '', '', '', 0, 0, 0, 0, 0, 19, 19, 19, 19, 19, '2017-04-07', '03:57:34', 1, 'approved'),
(29, 'cake_pastry', 'food', 'Bread & Beyond', 'Bread_&_Beyond_29_food_cake_pastry.png', '', 'Cake,Bakery and Coffee Shop', '', '', '', 'Mon-Sun:	 <br>7:00 am - 11:00 pm', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.foodedgeltd.com/ ', 'https://www.facebook.com/breadnbeyond.dhaka/ ', '01841320944-45 (Dhanmondi), <br>01841320946-47 (Banani), <br>01841320948-49 (Gulshan)', '', '', '', 0, 0, 0, 0, 0, 831, 831, 831, 831, 831, '2017-04-07', '04:17:45', 1, 'approved'),
(30, 'cafe', 'food', 'Apon Coffee House', 'Apon_Coffee_House_30_food_cafe.jpg', 'A place to have adda with coffees in a cheap rate', 'Coffee Shop', '', 'Cold Coffee', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/apon.coffee.house/', 'Phone: 01717611339 01792234263 01817112426', '', '', '', 0, 0, 0, 0, 0, 14, 14, 14, 14, 14, '2017-04-07', '09:12:52', 1, 'approved'),
(31, 'cafe', 'food', 'Barista Lavazza Bangladesh', 'Barista_Lavazza_Bangladesh_31_food_cafe.jpg', 'You are welcome to our â€™Cafetopiaâ€™ where you can chill and relax with a warm cup of coffee in your hand. So come and enjoy Barista Lavazza', 'Coffee Shop', '', '', '', '7:00 AM â€“ 12:00 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/BaristaBD/', '', '', '', '', 0, 0, 0, 0, 0, 28, 28, 28, 28, 28, '2017-04-07', '11:27:42', 1, 'approved'),
(32, 'cafe', 'food', 'TABAQ COFFEE', 'TABAQ_COFFEE_32_food_cafe.jpg', 'A cosy place for Gourmet coffee and desserts. Over the years, TABAQÂ© has been serving a loyal customer base from a small cafÃ© in DOHS Baridhara, Dhaka. TABAQ COFFEE hence started in 2013 to serve with a brand new tabaq and coffee which is now one of the best coffee shops in town.\r\n<br>Won the Winner''s trophy for the Best Coffee Category at the The Daily Star Foodiez Choice Award 2015 presented by Dr. Mahfuz Anam, Chief Editor of Daily Star, Mr. Shadab Ahmed, CEO of Coca-Cola Bangladesh and Mr. Ashiqur Rahman Rean, Founder of Dhaka Foodies\r\n', 'Coffee Shop', '', '', '', '12:00 PM â€“ 10:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/tabaqcoffee/', 'Phone:01726588566 <br>HR : hr.tabaq@gmail.com', '', '', '', 0, 0, 0, 0, 0, 16, 16, 16, 16, 16, '2017-04-07', '14:49:04', 1, 'approved'),
(33, 'cafe', 'food', 'Coffee World', 'Coffee_World_33_food_cafe.jpg', 'Our goal is to serve you the finest beverages, our specialty waffles, fresh made-to-order sandwiches and salads, in an ambiance that is comfortable and relaxing.', 'Coffee Shop', '', '', '', '9:00 AM â€“ 11:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/CoffeeWorldDhaka/', 'Tele: 02-9127056', '', '', '', 0, 0, 0, 0, 0, 9, 9, 9, 9, 9, '2017-04-07', '15:39:30', 1, 'approved'),
(34, 'cafe', 'food', 'Butlers Chocolate CafÃ© - Bangladesh', 'Butlers_Chocolate_CafÃ©_-_Bangladesh_34_food_cafe.jpg', 'Butlers Chocolates was founded in Ireland in 1932 by Marion Butler, with an aim "to bring a little happiness to the world, one chocolate at a time." In 1998, the company opened its first cafÃ© in Dublin, Ireland offering premium chocolates, coffee, tea, and chocolate beverages. <br>Now customers in Bangladesh can also experience two great pleasures together â€“ chocolate and coffee ', 'Chocolate & Coffee beverages', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.butlerschocolates.com/', 'https://www.facebook.com/butlersbd/', 'Email: info@bd.butlerschocolates.com Phone: +88029850860', '', '', '', 0, 0, 0, 0, 0, 13, 13, 13, 13, 13, '2017-04-07', '16:59:45', 1, 'approved'),
(35, 'cafe', 'food', 'Beans & Aroma Coffees', 'Beans_&_Aroma_Coffees_35_food_cafe.jpg', '"A global coffee shop especially adapted for local way of life" says all about us.', 'Coffee Shop', '', '', '', '11:00 AM - 12:15 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/beansnaroma/', 'Phone: +880 2 8931906 <br>Hot Line : 0171808050', '', '', '', 0, 0, 0, 0, 0, 125, 125, 125, 125, 125, '2017-04-08', '05:20:37', 1, 'approved'),
(36, 'cafe', 'food', 'Crimson Cup BD', 'Crimson_Cup_BD_36_food_cafe.jpg', 'Columbus Coffee serves Crimson Cup, which is a globally renowned caffeine brand. Columbus Coffee Shop offers a wide range of handcrafted hot, iced and frozen espresso drinks as well as hot and iced teas. Sandwiches and fresh-baked goods round out the menu. Customers can also buy bags of Crimson Cup beans to brew at home.', 'Coffee Shop ', '', 'CrimsonCup coffee and tea from Columbus, Ohio', '', '11:00 AM -1:30 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/columbuscoffeebd/', 'Phone: 01717-105042', '', '', '', 0, 0, 0, 0, 0, 9, 9, 9, 9, 9, '2017-04-08', '05:32:37', 1, 'approved'),
(37, 'cafe', 'food', 'George''s Cafe', 'Georges_Cafe_37_food_cafe.jpg', 'A slice of New York in Dhaka!', 'Coffee Shop', '', '', '', '11:00 AM â€“ 11:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/gcafebd', 'Phone: 01784-436743', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2017-04-08', '06:00:04', 1, 'approved'),
(38, 'restaurant', 'food', 'Thai Emerald', 'Thai_Emerald_38_food_restaurant.jpg', 'Authentic Thai Cuisine Restaurant', 'Thai Restaurant', '', '', '', '12:00 PM â€“ 3:00 PM, <br>6:00 PM â€“ 10:30 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Gulshan: https://www.facebook.com/thaiemeraldgulshan/   <br>Uttara: https://www.facebook.com/thai.emerald.bd/', 'Gulshan: 01766293935 <br>Uttara: 01776234517', '', '', '', 0, 0, 0, 0, 0, 10, 10, 10, 10, 10, '2017-04-08', '16:18:52', 0, 'approved'),
(39, 'restaurant', 'food', 'Kabab Factory', 'Kabab_Factory_39_food_restaurant.png', 'From the moment you walk into our restaurant, be prepared to be mesmerized with the aromatic smells of our kitchen grill. Be ready to be engrossed with the finest variations in our diverse menu of mouthwatering delicacies, from the juicy Kababs to the luscious Thai Cuisines. Moreover, we provide special hand-made Naans baked fresh in our own kitchens for all your meals, which we guarantee will be the tastiest and softest Naan youâ€™ve ever had! Besides our signature Kababs we have mouth-watering seafood delicacies and desserts like kulfi to soothe your palate after a mildly spicy meal.\r\n<br> Offers Breakfast only in weekends\r\n', 'Kababs & Thai Foods', '', 'Kabab Items. Offers home delivery', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.kababfactory.com.bd/ ', 'https://www.facebook.com/kababfactory/', 'Gulshan: 01941111333, 01941111555 <br>Uttara: 01754244455', '', '', '', 0, 0, 0, 0, 0, 24, 24, 24, 24, 24, '2017-04-08', '16:48:58', 1, 'approved'),
(40, 'restaurant', 'food', 'Grand Nawab', 'Grand_Nawab_40_food_restaurant.jpg', 'Grand Nawab is an ethnic cruisines of royal bengal situated at Old Dhaka. It will serve authentic nawabi and mughal food.', 'Mughal type Biriyani Place', '', 'Kacchi Biriyani', '', '8:00 AM â€“ 10:00PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/grandnawab/', 'Phone: 01718-360657, 01941095484, 01556332867 <br>Email: info@grandnawab.com', '', '', '', 0, 0, 0, 0, 0, 14, 14, 14, 14, 14, '2017-04-08', '17:29:07', 1, 'approved'),
(41, 'restaurant', 'food', 'THE DARK, Music Cafe & Restaurant', 'THE_DARK,_Music_Cafe_&_Restaurant_41_food_restaurant.png', 'Our significant motive is to please our guests in a very gratitude & humbly attractive manner', 'Cafe & Restaurant', '', '', 'Take their membership card to enjoy 15% discount', '11:00AM â€“ 11:00PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/The-Dark-Music-Cafe-Restaurant-1468311886829439/', 'Phone: 01721-904504', '', '', '', 0, 0, 0, 0, 0, 16, 16, 16, 16, 16, '2017-04-09', '08:16:19', 1, 'approved'),
(42, 'restaurant', 'food', 'Bhooter Bari', 'Bhooter_Bari_42_food_restaurant.jpg', 'Bhooter Bari is one of a best places to feel like a heaven for the food lovers because of itâ€™s dazzling environment,mouthwatering dishes,amazing menues, heart touching behaviour of the hosts, and above all of these, the reasonable prices', 'Pizza & Fastfood Place', '', '', '', '11:30 AM - 11:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Lalbagh: https://www.facebook.com/BhooterBariLalbagh/  <br>Dhanmondi: https://www.facebook.com/BhooterBariDhanmondi/', 'Phone: +8801779722400,  +8801979722400', '', '', '', 0, 0, 0, 0, 0, 32, 32, 32, 32, 32, '2017-04-09', '08:41:33', 1, 'approved'),
(43, 'restaurant', 'food', 'SBARRO ', 'SBARRO__43_food_restaurant.jpg', 'Americaâ€™s favourite pizza now in Bangladesh. Just one of our giant super slice is enough to satisfy your appetite!', 'Pizza Place', '', '14â€ cheesy pizza slices are sold separatly, per piece basis ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.sbarrobd.com', 'https://www.facebook.com/sbarrobd/', 'Phone: 01777-700664 <br>E-mail: gulshan@sbarrobd.com', '', '', '', 0, 0, 0, 0, 0, 9, 9, 9, 9, 9, '2017-04-11', '12:21:26', 1, 'approved'),
(44, 'restaurant', 'food', 'Veni Vidi Vici', 'Veni_Vidi_Vici_44_food_restaurant.jpg', 'Our Italian food is simple, vibrant, subtle, delicious and diverse. It can be enjoyed by everyone, not just Italians', 'Italian Restaurant', '', 'Pizza items, offer buffet', '', 'Mon- Sun, 12:00 PM â€“ 11:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/venividivicidhaka/', 'Phone: 01705-737888', '', '', '', 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, '2017-04-12', '00:11:50', 0, 'approved'),
(45, 'restaurant', 'food', 'Nandoâ€™s', 'Nandoâ€™s_45_food_restaurant.jpg', 'Nandoâ€™s, one of the top branded South African dining restaurant group originated from the Mozambican-Potuguese community. Founded in 1987, the restaurant has a Portuguese/ Mozambiquan theme attracting many customers all over the world. Presently Nandoâ€™s operates in thirty countries on five continents ', 'South African Dining', '', 'Peri-Peri Chicken', '', '12:00 PM â€“ 12:00 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.nandos.com ', '', 'Phone: 01833362175', '', '', '', 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, '2017-04-12', '01:12:17', 1, 'approved'),
(46, 'restaurant', 'food', 'Fish & Co.', 'Fish_&_Co._46_food_restaurant.jpg', 'We are a casual, full service, family chain restaurant serving fresh seafood in a pan.  Our unique dining experience drew inspiration from the simple practices of the fishermen in the Mediterranean Sea.  There, fishermen cooked freshly caught seafood on board their boats and ate the seafood off the pan to ease their hunger pangs out in the sea. \r\nWe use only the freshest seafood, as well as natural and wholesome ingredients like trans-fat free oil, herbs, olive oil and various spices from around the world to ensure that YOU always get to enjoy great tasting and healthy meals in generous portions.  \r\n', 'Seafood Restaurant ', '', 'Seafood platters', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.fish-co.com/', 'https://www.facebook.com/fishandco.bd/ ', 'Phone: 01777794181, +88-02-9895711', '', '', '', 0, 0, 0, 0, 0, 21, 21, 21, 21, 21, '2017-04-12', '02:29:39', 1, 'approved'),
(47, 'restaurant', 'food', 'Rice & Noodle', 'Rice_&_Noodle_47_food_restaurant.jpg', 'It is all about Rice & Noodle, starting from appetizers to desserts. Itâ€™ll tingle your taste buds with some radical rice, noodle, gravy & stir fry dishes', 'Asian Restaurant', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/riceandnoodlebd/?rf=1456344814613202', 'Phone: 01789531819, 01710811918', '', '', '', 0, 0, 0, 0, 0, 32, 32, 32, 32, 32, '2017-04-12', '02:53:40', 1, 'approved'),
(48, 'restaurant', 'food', 'Street BBQ', 'Street_BBQ_48_food_restaurant.jpg', 'A reasonable rooftop BBQ Place where you can enjoy BBQ with an cozy environment', 'BBQ Place', '', 'Roof top place where BBQ with freshly BRAC Chicken will be made in front of you. Sides with BBQ can also be chosen ', '10% student discount', 'Mon â€“ Sun, <br>12:30PM- 09:30PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/StreetBBQ21', 'Phone: 01726-456088', '', '', '', 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, '2017-04-12', '03:54:29', 1, 'approved'),
(49, 'restaurant', 'food', 'American Blend- Burger & Beyond', 'American_Blend-_Burger_&_Beyond_49_food_restaurant.jpg', 'New blend in town! American Blend is a house of fresh, juicy and cheesy burgers and pizzas. Well-decorated and hygienic environment for food lovers', 'Burger & other fast food place', '', 'Juicy burger in reasonable price', '', '10:00 AM- 11:30 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/AmericanBlendBurgerBeyond/', 'Phone: 01782-506121', '', '', '', 0, 0, 0, 0, 0, 10, 10, 10, 10, 10, '2017-04-12', '04:18:26', 1, 'approved'),
(50, 'restaurant', 'food', 'Oregano', 'Oregano_50_food_restaurant.jpg', 'Replenish your zest with the most astonishing Sea foods of the town.\r\nGet ready to slobber with the flavorsome, zealous platters. Rush now & enjoy the ecstasy.\r\n', 'British, French, Seafood, Spanish, Italian, Steaks, American', '', 'Sea foods ', '', '12:00PM- 10:30PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/oreganobd/timeline?ref=page_internal ', 'Phone: 880 1614-204913', '', '', '', 0, 0, 0, 0, 0, 18, 18, 18, 18, 18, '2017-04-12', '04:43:39', 1, 'approved'),
(51, 'restaurant', 'food', 'Comic Cafe', 'Comic_Cafe_51_food_restaurant.jpg', 'It''s all about Bengali Comics & Food.\r\n', '', '', '', '', 'Mon â€“ Sun <br>12:00PM â€“ 10:30PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ' facebook.com/comic.cafe.bd', 'Phone: +8801670727370, +8801670727370 ', '', '', '', 0, 0, 0, 0, 0, 16, 16, 16, 16, 16, '2017-04-12', '04:58:11', 1, 'approved'),
(52, 'restaurant', 'food', 'Mad chef', 'Mad_chef_52_food_restaurant.png', 'Home of Gourmet burgers, steaks and grilled cheese sandwiches', 'Burger Restaurant', '', '', 'EAT FREE ON YOUR BIRTHDAY!! <br>Madchef likes to make your special days extra special. The Birthday Boy/Girl will get a complimentary Burger and Soft Drink on his/her birthday.', 'Sat- Thu: 11:30AM â€“ 11:00PM <br>Fri 2:00PM - 11:00PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Banani: https://www.facebook.com/madchef2/timeline?ref=page_internal <br>Dhanmondi: https://www.facebook.com/madchefdhk/timeline?ref=page_internal', 'Phone: 01715-780379, 01735549567', '', '', '', 0, 0, 0, 0, 0, 40, 40, 40, 40, 40, '2017-04-12', '05:12:57', 1, 'approved'),
(53, 'restaurant', 'food', 'Food Republic', 'Food_Republic_53_food_restaurant.jpg', 'Food Republic is the place if you want to have awesome food. We have an awesome menu with Thai, Indian, Seafood, North End coffee and a lot more', 'Mainly seafood Restaurant', '', '', 'Offers lunnch buffet. Has smoking zone', 'Sat-Thu 12:30pm-11:00pm,  <br>Friday 2:00pm-11:00pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/foodrepublicbd/ ', 'Phone: 01935-535353, 01703-655163', '', '', '', 0, 0, 0, 0, 0, 13, 13, 13, 13, 13, '2017-04-12', '08:54:23', 1, 'approved'),
(54, 'restaurant', 'food', 'Peda Ting Ting', 'Peda_Ting_Ting_54_food_restaurant.jpg', 'Peda Ting Ting restaurant originally located at Kaptai, Rangamati, BD. Recently we have opened a new outlet in the capital at Gulshan 1. Peda Ting Ting is famous for its signature dishes that are cooked inside bamboo and banana leaf in traditional â€™Chakmaâ€™ style. It has around 11 thousand square feet of floor space comprising of green outdoor and AC indoor sitting arrangement. It also includes an art gallery, handcrafted souvenir shop and a boutiqe corner. Considering different taste bud of city dweller, we are also offering Continental, Indian and Middle Eastern cuisines. Its the only place in Dhaka where one can enjoy art, food and culture of indigenous Chakma community', 'Greek, Seafood, BBQ, Mediterranean, Chakma', '', 'Chakma food, outdoor seating, Photo Gallery, Some Chakma Clothing, takes reservation', '', 'Mon - Sun: 11:00 am - 11:00 pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/PedaTingTing ', 'Phone: +8801741102403', '', '', '', 0, 0, 0, 0, 0, 7, 7, 7, 7, 7, '2017-04-12', '12:57:25', 0, 'approved'),
(55, 'restaurant cafe', 'food', 'KIVA HAN', 'KIVA_HAN_55_food_restaurant_cafe.jpg', 'Inspired by the world''s first coffee shop Kiva Han opened around 1475, in Constantinople, Turkey. An enthused gathering of coffee drinkers and fusion food lovers. A place where intellects, professionals, artists and youth throng together.', 'Coffee Shop & fusion food place', '', 'Special coffee beans. They also take order for special red velvet cake for any occation.', '', '10:00 AM â€“ 11:30 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.kivahancafe.com/', 'https://www.facebook.com/kivahancafe/', 'Phone: 01755-555844', '', '', '', 0, 0, 0, 0, 0, 5, 5, 5, 5, 5, '2017-04-12', '15:44:51', 1, 'approved'),
(56, 'cafe', 'food', 'Coffee Time', 'Coffee_Time_56_food_cafe.jpg', 'Coffee Time only provides different types of coffees ....Like Hot Regular Coffee , Hot Chocolate Coffee, Ice Regular Coffee, Ice Chocolate Coffee & etc....', 'Coffee Shop', '', '', '', '11:00 AM â€“ 10:30 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/coffeetimeuttara/', 'Phone:  +8801679318593/4', '', '', '', 0, 0, 0, 0, 0, 19, 19, 19, 19, 19, '2017-04-12', '17:35:13', 1, 'approved'),
(57, 'cafe', 'food', 'Popeyes Coffee & Fast Food', 'Popeyes_Coffee_&_Fast_Food_57_food_cafe.jpg', 'A popular cold coffee place in Dhaka', 'Coffee Shop & Fast Food', '', 'Chocolate Cold Coffee', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/Popeyes-Cold-Coffee-507956402682645/', 'Phone: 01799646400, 01676914307', '', '', '', 0, 0, 0, 0, 0, 13, 13, 13, 13, 13, '2017-04-13', '07:53:42', 1, 'approved'),
(58, 'cafe cake_pastry', 'food', 'Bittersweet Cafe', 'Bittersweet_Cafe_58_food_cafe_cake_pastry.jpg', 'BitterSweet launched with a bang in 2007, and became an instant hit with the expats and the crÃ¨me de la crÃ¨me. The interior is stylish and intentionally â€˜comfortingâ€™, and in one word â€˜uniqueâ€™ in Dhaka! Comprising mostly of Continental food and a dash of Asian cuisine, we take pride in delighting the palate of our customers. A latte and a cupcake, on a comfy sofa with some hummable tunes in the air make us the coziest spot in dhaka city', 'Cafe & Pastry shop', '', 'Coffee & Cookies ', '', '12:00 PM â€“ 12:30 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/bittersweetcafebd/', 'Phone: 02-9861389', '', '', '', 0, 0, 0, 0, 0, 17, 17, 17, 17, 17, '2017-04-13', '13:01:54', 1, 'approved'),
(59, 'cafe cake_pastry', 'food', 'The Coffee Bean & Tea Leaf, Bangladesh', 'The_Coffee_Bean_&_Tea_Leaf,_Bangladesh_59_food_cafe_cake_pastry.png', 'In 1963, Herbert B. Hyman started The Coffee Bean & Tea Leaf in California. Now, nearly 50 years old, The Coffee Bean & Tea LeafÂ® has grown into one of the largest privately-owned, family-run coffee and tea companies in the world. Since 1963, we have continued to search the earth for only the finest and rarest loose-leaf teas and premium coffees. Today, we proudly offer over 22 varieties of coffee and 20 kinds of tea. Over the years, we have worked hard to build relationships with individual farms and estates so that we can be assured of the best coffee and tea harvests available.', 'Coffee, Pastry, Snacks shop', '', 'Imported special coffee beans & tea leafs makes it special', '', '10:00 AM â€“ 12:00 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.coffeebean.com.bd/', 'https://www.facebook.com/The-Coffee-Bean-Tea-Leaf-Bangladesh-174946036191672/', 'Phone: +88 09683 995 995 <br>Email: mostafa.safa@coffeebeanbd.com', '', '', '', 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, '2017-04-15', '06:13:35', 1, 'approved'),
(60, 'restaurant cafe', 'food', 'Brews & Bites', 'Brews_&_Bites_60_food_restaurant_cafe.png', 'A cafe made purely out of love for coffee and art, with great sandwiches and desserts as a side dish. Let the effects of Italian flavour make your day!', 'Coffee, Pastry, Italian Items', '', '', '', '10:30 AM â€“ 12:30 AM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/brewsnbites/', 'Phone: 01919774559 ', '', '', '', 0, 0, 0, 0, 0, 9, 9, 9, 9, 9, '2017-04-18', '06:06:42', 1, 'approved'),
(61, 'cafe', 'food', 'PappaRoti', 'PappaRoti_61_food_cafe.jpg', 'The PappaRoti bun is described as a â€˜Crispy, fluffy, fragrant coffee-cream coated bun with a buttery fillingâ€™ . Also adding the coffee, chocolate, honey topping on the dough to baked fresh in front of you served should be hot. It is really good combination to pappaRoti with fresh brewed coffee & aroma.', 'Bun shop and cafe', '', '', '', 'Mon:	 08:00 AM - 10:00 PM <br>Tues:	 4:00 PM - 10:00 PM <br>Wed-Sun:	 08:00 AM - 10:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/PappaRoti.Bangladesh/', 'Phone: 01713014828 <br>Email:  prabeer@officextracts.com ', '', '', '', 0, 0, 0, 0, 0, 15, 15, 15, 15, 15, '2017-04-18', '08:27:31', 1, 'approved'),
(62, 'restaurant cake_pastry', 'food', 'California Fried Chicken and Pastry Shop', 'California_Fried_Chicken_and_Pastry_Shop_62_food_restaurant_cake_pastry.jpg', 'The name of the chain shop started with "California" because it was dreamed in California to open this kind of chain shop in Bangladesh in 1982. Finally 1st shop was open at Dhanmondi Road #27 in 15th June 2003.  Availability of all products might vary branch to branch', 'Fast food, bakery and Pastry shop', '', '', '', 'Mon-Sun:	 <br>9:00 am - 10:30 pm', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://cfc-bd.com/', 'https://www.facebook.com/California.Fried.Chicken/', '', '', '', '', 0, 0, 0, 0, 0, 9, 9, 9, 9, 9, '2017-04-18', '15:36:37', 1, 'approved'),
(63, 'cake_pastry', 'food', 'Shumiâ€™s Hot Cake', 'Shumiâ€™s_Hot_Cake_63_food_cake_pastry.jpg', 'Famous Bakery and Fast Food Chain....25 outlets across Dhaka and 3 in Chittagong....', 'Cake and bakery shop', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/Shumis-Hot-Cake-Ltd-134774773237222/ ', '', '', '', '', 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, '2017-04-20', '01:07:34', 1, 'approved'),
(64, 'cake_pastry', 'food', 'Holey Artisan Bakery', 'Holey_Artisan_Bakery_64_food_cake_pastry.png', 'Holey Artisan Bakery was born from passion.\r\n\r\nIt all began with Lillianâ€™s longing for real bread and so her husband Porag took action. He joined forces with Sadat, a close friend and the owner of the best restaurant in Dhaka, Izumi and they brought Mr. Maurice, Baker Maestro, the magician behind all creations at Holey, to train us.\r\n\r\n<br>It is hard to articulate how thankful we are to him and how immeasurable has been his contribution.\r\n\r\n<br>The venture into Holey was not about making profit. Everyone involved could easily be making more money with much less effort in other sectors.\r\nIt has to be sustainable, of course, but this project was born out of love and everyone involved is passionate about improving life. The family at Holey has grown and everyone involved is a key ingredient.\r\n', 'Bakery', '', 'made fresh everyday ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://www.holeybread.com/ ', 'https://www.facebook.com/HoleyBread/ ', 'Phone: 01969200200  <br>Email: info@holeybread.com', '', '', '', 0, 0, 0, 0, 0, 6, 6, 6, 6, 6, '2017-04-20', '07:16:21', 1, 'approved'),
(65, 'cake_pastry', 'food', 'Cake World', 'Cake_World_65_food_cake_pastry.jpg', 'Jannat''s Cake World is an online cake shop.\r\nOrder for Pick Up or Local Delivery.\r\n\r\n<br>Please mention the following with your order\r\nDesired date, time and pick up point to get your order delivered.\r\n\r\nYou have to Place your order at least 2 days ahead to pickup and 3/4 days ahead in case of customized orders.\r\n\r\n<br>Payment Procedure\r\n\r\nYou have to BKASH 60% of the whole amount in advance to secure your order. Rest amount has to be paid in cash on delivery.\r\n', 'Online Cake Shop', '', 'They offer cake baking classes.', '', ' 08:00 AM â€“ 10:00 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'https://www.facebook.com/Cake-World-1467573770139519/', 'Phone: 01680172178', '', '', '', 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, '2017-04-20', '09:35:00', 1, 'approved');
INSERT INTO `general_info` (`id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `type`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `column_16`, `branches`, `web_site`, `social_links`, `contact`, `main_location`, `location`, `map`, `total_avg_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `total_view`, `daily_view`, `weekly_view`, `monthly_view`, `yearly_view`, `date`, `time`, `admin_id`, `status`) VALUES
(66, 'cake_pastry', 'food', 'Krispy Kreme Bangladesh', 'Krispy_Kreme_Bangladesh_66_food_cake_pastry.png', 'Vernon Rudolph bought a secret yeast-raised doughnut recipe from a New Orleans French chef, rented a building in what is now historic Old Salem in Winston-Salem, NC, and began selling Krispy Kreme doughnuts to local grocery stores.The delicious scent of cooking doughnuts drifted into the streets, and passers-by stopped to ask if they could buy hot doughnuts. So, he cut a hole in an outside wall and started selling hot Original Glazed doughnuts directly to customers on the sidewalk.\r\n\r\n<br>By 1940s, there was a small chain of stores, mostly family-owned. They all used the Krispy Kreme recipe, but each store made its doughnuts from scratch. For Rudolph and Krispy Kreme, the results were always good but not consistent enough. So Krispy Kreme built a mix plant and developed a distribution system that delivered the perfect dry doughnut mix to each store.\r\n\r\n<br>Rudolph and his equipment engineers, then invented and built Krispy Kremeâ€™s own doughnut-making equipment. From the 1950s on, they focused on improving and automating the doughnut-making process.\r\nDuring the 1960s, Krispy Kreme enjoyed steady growth throughout the Southeast and began expanding outside its traditional roots.\r\n\r\n<br>Keep an eye on our official Facebook page for Bangladesh to know more about our amazing doughnuts and the grand opening of Krispy Kreme in Bangladesh!\r\nAlso sell some collectibles like ti-shirt, cap, mug etc.\r\n', 'International Donut Shop', '', 'Fresh donuts', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'http://krispykremebd.com/ ', 'https://www.facebook.com/kkdbd/ ', 'Phone: 02-8836818  <br>Email:  kkdbd.info@gmail.com ', '', '', '', 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, '2017-04-20', '16:11:37', 1, 'approved');

-- --------------------------------------------------------

--
-- Table structure for table `item_des`
--

CREATE TABLE IF NOT EXISTS `item_des` (
  `id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(80) NOT NULL,
  `image_des` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_list`
--

CREATE TABLE IF NOT EXISTS `item_list` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(150) NOT NULL,
  `price` varchar(300) NOT NULL,
  `color` varchar(150) NOT NULL,
  `size` varchar(30) NOT NULL,
  `model` varchar(100) NOT NULL,
  `price_2` varchar(50) NOT NULL,
  `column_1` varchar(200) NOT NULL,
  `column_2` varchar(200) NOT NULL,
  `column_3` varchar(200) NOT NULL,
  `column_4` varchar(255) NOT NULL,
  `filter_1` varchar(40) NOT NULL,
  `filter_2` varchar(40) NOT NULL,
  `filter_3` varchar(100) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_list`
--

INSERT INTO `item_list` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `description`, `image_link`, `price`, `color`, `size`, `model`, `price_2`, `column_1`, `column_2`, `column_3`, `column_4`, `filter_1`, `filter_2`, `filter_3`, `total_rate`, `rate_1`, `rate_2`, `rate_3`, `rate_people`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'food', 6, 'restaurant', 'Parata', '', 'Parata_1_6_food_restaurant.jpg', '25 +', '', '', '', '', '', '', '', '', 'Morning Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '14:52:45', 2, ''),
(2, 'food', 6, 'restaurant', 'Normal Nan', '', 'Normal Nan_2_6_food_restaurant', ' 30 +', '', '', '', '', '', '', '', '', 'Morning Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '14:52:45', 2, ''),
(3, 'residence', 8, 'hotel', 'DELUXE ROOMS', 'Deluxe Rooms welcome you to spectacular panoramic lake and city views, and floor-to-ceiling windows allow an abundance of natural light to create an unconstrained atmosphere above the bustling city. W', 'DELUXE ROOMS_3_8_residence_hotel.jpg', '', '', '', '', 'USD 151.05 / night <br>1 Room(s), 1 Adult(s)', '', 'City and Lake Views', '<br>398-431 Square Feet / 37-40 Square Meters <br>Accessible Rooms Available <br>City and Lake Views <br>Connecting Rooms Available', '', 'room', '', '', 0, 0, 0, 0, 0, '2017-03-19', '15:41:26', 1, ''),
(4, 'residence', 8, 'hotel', 'Business Center', 'Those traveling to Dhaka on business can utilize our Business Center for stress-free productivity during your stay. Increase efficiency from our desktop workstations with High Speed Internet Access an', 'Business Center_4_8_residence_hotel.jpg', '', '', '', '', '', '', '', '', '', 'service', '', '', 0, 0, 0, 0, 0, '2017-03-19', '15:41:26', 1, ''),
(5, 'food', 11, 'restaurant', 'Nasi Goreng Set', 'Indonesian Chicken Fried Rice Served with Poached Egg + Fried Chicken + Satay Vegetable + Mineral Water + Complimentery - Dessert', 'Nasi Goreng Set_5_11_food_restaurant.jpg', '300 ', '', '', '', '', '', '', '', '', 'Set Lunch Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '21:52:02', 2, ''),
(6, 'food', 11, 'restaurant', 'Phat Khai Mao Set', 'Thai Chicken Noodles Served with Poached Egg + Fried Chicken + Satay Vegetable + Mineral Water + Complimentary - Dessert', 'Phat Khai Mao Set_6_11_food_restaurant.jpg', '300 ', '', '', '', '', '', '', '', '', 'Set Lunch Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '22:02:19', 2, ''),
(7, 'food', 11, 'restaurant', 'Khichuri Set ', 'Chicken/ Beef Khichuri served with Fried Egg Plant + Potato Chop + Egg Omlete + Achar + Salad & Minx', 'Khichuri Set _7_11_food_restaurant.jpg', '300 ', '', '', '', '', '', '', '', '', 'Set Lunch Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '22:02:19', 2, ''),
(8, 'food', 11, 'restaurant', 'Oriental Set', 'Fried Rice Served with Sweet & Sour Prawn + Fried Chicken + Satay Vegetable + Mineral Water + Complimentary - Dessert', 'Oriental Set_8_11_food_restaurant', '350 ', '', '', '', '', '', '', '', '', 'Set Lunch Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '22:02:19', 2, ''),
(9, 'food', 11, 'restaurant', 'Hyderabadi Set', 'Hyderabadi Mutton Biriyani Served with Boiled Egg + Tzatziki + Salad + Mineral Water + Complimentary - Dessert', 'Hyderabadi Set_9_11_food_restaurant', '350 ', '', '', '', '', '', '', '', '', 'Set Lunch Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '22:02:19', 2, ''),
(10, 'food', 11, 'restaurant', 'Tandoori Set', 'Tandoori Chicken Kebab with Garlic Naan + Afgan Rice + Sabzi Milijhili/ Daal Makhani + Tzatziki/ Hummus + Salad + Mineral Water + Complimentary â€“ Dessert)', 'Tandoori Set_10_11_food_restaurant.jpg', '350 ', '', '', '', '', '', '', '', '', 'Set Lunch Menu', '', '', 0, 0, 0, 0, 0, '2017-03-19', '22:02:19', 2, ''),
(11, 'food', 11, 'restaurant', 'Mughal Mixed Kebab', 'Hariyali + Reshmi + Afgan & Beef Sheekh Kebab Served with Sabzi Milijhili/ Daal Makhani + Garlic Naan + Afgan Rice + Hummus/ Tzatziki + Salad + Complimentary - Dessert', 'Mughal Mixed Kebab_11_11_food_restaurant', '450 ', '', '', '', '', '', '', '', '', 'Combo', '', '', 0, 0, 0, 0, 0, '2017-03-19', '22:02:19', 2, ''),
(12, 'food', 17, 'cafe', 'Cappuccino/ Cafe Latte', 'CLASSICS regular made with a double shot', 'Cappuccino/ Cafe Latte_12_17_food_cafe.jpg', 'Tk 184 +   SML <br>Tk 235 +   REG', '', '', '', '', '', '', '', '', 'Espresso', '', '', 0, 0, 0, 0, 0, '2017-03-22', '07:27:35', 1, ''),
(13, 'food', 17, 'cafe', 'Creme Brulee', 'ESPRESSO CHILLERS made with fresh espresso', 'Creme Brulee_13_17_food_cafe.jpg', 'Tk 315 +    SML <br>Tk 325 +   REG', '', '', '', '', '', '', '', '', 'Chillers', '', '', 0, 0, 0, 0, 0, '2017-03-22', '07:27:35', 1, ''),
(14, 'food', 19, 'restaurant cart', 'Taco', 'Tk 145   Grill Steak\r\n<br>Tk 145   Gordita Chicken\r\n<br>Tk 140   Spicy Chicken\r\n<br>Tk 140   Ranchi Chicken\r\n', 'Taco_14_19_food_restaurant cart.jpg', '140', '', '', '', '', '', '', '', '', 'Mexican Items', '', '', 0, 0, 0, 0, 0, '2017-03-22', '07:58:40', 1, ''),
(15, 'food', 19, 'restaurant cart', 'Chicken Subway Sandwich', '', 'Chicken Subway Sandwich_15_19_food_restaurant cart.jpg', '150', '', '', '', '', '', '', '', '', 'Sandwichs', '', '', 0, 0, 0, 0, 0, '2017-03-22', '07:58:40', 1, ''),
(16, 'food', 20, 'sweets', 'Gulab Jamun', 'Khazana Mithai Special Desert Item.Our Top and Famous product Gulab Jamun as like as Indian HOLDIRAMâ€™\\S Product.It\\â€™\\s look like a round  and different test,You can use ,mawa,chana,sugar,flour,oil', 'Gulab Jamun_16_20_food_sweets.jpg', '', '', '', '', '', '', '', '', '', 'Fresh Sweets', '', '', 0, 0, 0, 0, 0, '2017-03-22', '08:18:23', 1, ''),
(17, 'food', 20, 'sweets', 'Doi', 'Doi Specially uncommon looking and different muddy pot use.Any time you can test it but after lunch and dinner time much better.You can make it eassly just use the Milk,sugar and Alach Powder timely.', 'Doi_17_20_food_sweets.jpg', '', '', '', '', '', '', '', '', '', 'Fresh Sweets', '', '', 0, 0, 0, 0, 0, '2017-03-22', '08:18:23', 1, ''),
(18, 'food', 21, 'cake_pastry', 'Original natural glazed', 'The softness of the donuts and the topping taste will delight you all the time. Buy 5 get 1 FREE !!!', 'Original natural glazed_18_21_food_cake_pastry', '75', '', '', '', '', '', '', '', '', 'Yeast Raised Donuts', '', '', 0, 0, 0, 0, 0, '2017-03-22', '08:29:39', 1, ''),
(19, 'food', 21, 'cake_pastry', 'Plain Lava Cake', 'This is one of the most popular and most delightful product of My Donuts. The melted rich chocolate will surely make you go for the next one. Buy 5 get 1 FREE !!!', 'Plain Lava Cake_19_21_food_cake_pastry.jpg', '80', '', '', '', '', '', '', '', '', 'Australian Choco Lava Cake', '', '', 0, 0, 0, 0, 0, '2017-03-22', '08:29:39', 1, ''),
(20, 'food', 23, 'cart', 'Chicken Cheese Burger', 'With 100gm steak', 'Chicken Cheese Burger_20_23_food_cart.jpg', 'Tk 150', '', '', '', '', '', '', '', '', 'Burger', '', '', 0, 0, 0, 0, 0, '2017-04-04', '04:53:50', 1, ''),
(21, 'food', 23, 'cart', 'Beef Cheese Burger', '100gm patty', 'Beef Cheese Burger_21_23_food_cart.jpg', 'Tk 180', '', '', '', '', '', '', '', '', 'Burger', '', '', 0, 0, 0, 0, 0, '2017-04-04', '04:53:50', 1, ''),
(22, 'food', 24, 'cart', '', '', '_22_24_food_cart', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-04', '05:08:35', 1, ''),
(23, 'food', 25, 'cart', 'B.B.Q. Wings', '6 pcs', 'B.B.Q. Wings_23_25_food_cart.jpg', 'Tk 160', '', '', '', '', '', '', '', '', 'appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-06', '16:20:55', 1, ''),
(24, 'food', 25, 'cart', 'Chefâ€™s Special Chicken Delicious Burger', 'Extra Cheese 20tk', 'Chefâ€™s Special Chicken Delicious Burger_24_25_food_cart.jpg', 'Tk 220', '', '', '', '', '', '', '', '', 'Burger', '', '', 0, 0, 0, 0, 0, '2017-04-06', '16:20:55', 1, ''),
(25, 'food', 26, 'cart', 'Wifi special burger (beef/chicken)', '', 'Wifi special burger beefchicken_25_26_food_cart.jpg', 'Tk 150', '', '', '', '', '', '', '', '', 'Burger', '', '', 0, 0, 0, 0, 0, '2017-04-06', '16:35:57', 1, ''),
(26, 'food', 27, 'sweets', 'Rosh Gulla', 'Fresh, bright, milky, and mild Queso fresco (a.k.a. "fresh cheeseâ€, or â€œHomemade cottage cheeseâ€ â€œChenna) balls gracefully cooked in boiling frothed lightly sweetened sugar syrup to become ful', 'Rosh Gulla_26_27_food_sweets.jpg', '', '', '', '', '', '', '', '', '', 'Traditional Sweets', '', '', 0, 0, 0, 0, 0, '2017-04-07', '02:48:59', 1, ''),
(27, 'food', 27, 'sweets', 'Kala Jamun', 'Fresh, bright, milky, and mild Queso fresco (a.k.a. "fresh cheeseâ€, or â€œHomemade cottage cheeseâ€ â€œChenna), pure saffron and smidge of semolina gracefully cooked in slightly sweetened sugar syr', 'Kala Jamun_27_27_food_sweets.jpg', '', '', '', '', '', '', '', '', '', 'Traditional Sweets', '', '', 0, 0, 0, 0, 0, '2017-04-07', '02:48:59', 1, ''),
(28, 'food', 28, 'cake_pastry', 'Glazed', 'The original sugar glazed donut', 'Glazed_28_28_food_cake_pastry.jpg', 'Tk 100', '', '', '', '', '', '', '', '', 'Donuts', '', '', 0, 0, 0, 0, 0, '2017-04-07', '03:57:34', 1, ''),
(29, 'food', 28, 'cake_pastry', 'Winter Glazed', 'The ring donut, dusted with icing sugar', 'Winter Glazed_29_28_food_cake_pastry.jpg', '', '', '', '', '', '', '', '', '', 'Donuts', '', '', 0, 0, 0, 0, 0, '2017-04-07', '03:57:34', 1, ''),
(30, 'food', 29, 'cake_pastry', '', '', '_30_29_food_cake_pastry', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-07', '04:17:45', 1, ''),
(31, 'food', 30, 'cafe', '', '', '_31_30_food_cafe', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-07', '09:12:52', 1, ''),
(32, 'food', 32, 'cafe', 'Espresso', '', 'Espresso_32_32_food_cafe.jpg', 'Tk 130 ', '', '', '', '', '', '', '', '', 'Warmers', '', '', 0, 0, 0, 0, 0, '2017-04-07', '14:49:04', 1, ''),
(33, 'food', 32, 'cafe', 'Cappuccino', '', 'Cappuccino_33_32_food_cafe.jpg', 'Tk 180 ', '', '', '', '', '', '', '', '', 'Warmers', '', '', 0, 0, 0, 0, 0, '2017-04-07', '14:49:04', 1, ''),
(34, 'food', 33, 'cafe', 'Mocha Chip Frappe', '', 'Mocha Chip Frappe_34_33_food_cafe.jpg', 'Tk 350 +   Pro <br>Tk 375 +   Alto', '', '', '', '', '', '', '', '', 'Frappes', '', '', 0, 0, 0, 0, 0, '2017-04-07', '15:39:31', 1, ''),
(35, 'food', 33, 'cafe', 'Mango Smoothie (Seasonal)', '', 'Mango Smoothie Seasonal_35_33_food_cafe.jpg', 'Tk 275 +   Pro', '', '', '', '', '', '', '', '', 'Smoothie', '', '', 0, 0, 0, 0, 0, '2017-04-07', '15:39:31', 1, ''),
(36, 'food', 34, 'cafe', '', '', '_36_34_food_cafe', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-07', '16:59:45', 1, ''),
(37, 'food', 37, 'cafe', 'Fruit Smoothie', '', 'Fruit Smoothie_37_37_food_cafe.jpg', 'Tk 220', '', '', '', '', '', '', '', '', 'Smoothie Bar', '', '', 0, 0, 0, 0, 0, '2017-04-08', '06:00:04', 1, ''),
(38, 'food', 38, 'restaurant', 'Dumpling', '', 'Dumpling_38_38_food_restaurant', 'Tk 380 +', '', '', '', '', '', '', '', '', 'appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-08', '16:18:52', 0, ''),
(39, 'food', 38, 'restaurant', 'Prawn Cake', '', 'Prawn Cake_39_38_food_restaurant', 'Tk 475 +', '', '', '', '', '', '', '', '', 'Appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-08', '16:18:52', 0, ''),
(40, 'food', 38, 'restaurant', 'Fresh Vietnamies', '', 'Fresh Vietnamies_40_38_food_restaurant', 'Tk 495 +', '', '', '', '', '', '', '', '', 'Appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-08', '16:18:52', 0, ''),
(41, 'food', 39, 'restaurant', 'Beef Afgani Curry', '', 'Beef Afgani Curry_41_39_food_restaurant.png', 'Tk 550 +', '', '', '', '', '', '', '', '', 'Afgan and Middle Eastern Speciality', '', '', 0, 0, 0, 0, 0, '2017-04-08', '16:48:58', 1, ''),
(42, 'food', 39, 'restaurant', 'Chicken Boti', '', 'Chicken Boti_42_39_food_restaurant.png', 'Tk 450 +', '', '', '', '', '', '', '', '', 'Kabab Factory Dishes', '', '', 0, 0, 0, 0, 0, '2017-04-08', '16:48:58', 1, ''),
(43, 'tourism', 22, 'heritage', 'check image', 'desc', 'check image_43_22_tourism_heritage.png', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-08', '16:59:39', 2, ''),
(44, 'food', 40, 'restaurant', 'Nawab â€“ E â€“ Kacchi Biriyani', '', 'Nawab â€“ E â€“ Kacchi Biriyani_44_40_food_restaurant.jpg', 'Tk 160', '', '', '', '', '', '', '', '', 'Main Dish', '', '', 0, 0, 0, 0, 0, '2017-04-08', '17:29:07', 1, ''),
(45, 'food', 40, 'restaurant', '', '', '_45_40_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-08', '17:29:07', 1, ''),
(46, 'food', 41, 'restaurant', 'Set Menu', 'Fried Rice + 1 pcs Fried Chicken + Chicken Curry + Thai Vegetable +  Drinks/ Water', 'Set Menu_46_41_food_restaurant.jpg', 'Tk 355 +', '', '', '', '', '', '', '', '', 'Set menu', '', '', 0, 0, 0, 0, 0, '2017-04-09', '08:16:19', 1, ''),
(47, 'food', 41, 'restaurant', 'Thai Soup Thick', '', 'Thai Soup Thick_47_41_food_restaurant.jpg', 'Tk 185 +', '', '', '', '', '', '', '', '', 'Soup', '', '', 0, 0, 0, 0, 0, '2017-04-09', '08:16:19', 1, ''),
(48, 'food', 42, 'restaurant', 'Pizza Bhoot Special ', '3 types of cheese', 'Pizza Bhoot Special _48_42_food_restaurant.jpg', 'Tk 530   6â€ <br>Tk 640   8â€ <br>Tk 750   10â€ <br>Tk 850   12â€', '', '', '', '', '', '', '', '', 'Pizza', '', '', 0, 0, 0, 0, 0, '2017-04-09', '08:41:34', 1, ''),
(49, 'food', 42, 'restaurant', 'Chicken Shawarma', '', 'Chicken Shawarma_49_42_food_restaurant.jpg', 'Tk 100', '', '', '', '', '', '', '', '', 'Shawarma', '', '', 0, 0, 0, 0, 0, '2017-04-09', '08:41:34', 1, ''),
(50, 'food', 43, 'restaurant', 'Cheese Pizza ', 'New Yorker Special', 'Cheese Pizza _50_43_food_restaurant.jpg', 'Tk 165 +  slice <br>Tk 1300 +  whole', '', '', '', '', '', '', '', '', 'Pizza', '', '', 0, 0, 0, 0, 0, '2017-04-11', '12:21:28', 1, ''),
(51, 'food', 43, 'restaurant', 'Meat Delight Pizza ', 'New Yorker Special', 'Meat Delight Pizza _51_43_food_restaurant', 'Tk 240 +  slice <br>Tk 1900 +  whole', '', '', '', '', '', '', '', '', 'Pizza', '', '', 0, 0, 0, 0, 0, '2017-04-11', '12:21:28', 1, ''),
(52, 'food', 43, 'restaurant', 'Vegge Supreme Pizza ', 'New Yorker Special', 'Vegge Supreme Pizza _52_43_food_restaurant.jpg', 'Tk 175 +  slice <br>Tk 1380 + whole', '', '', '', '', '', '', '', '', 'Pizza', '', '', 0, 0, 0, 0, 0, '2017-04-11', '12:21:28', 1, ''),
(53, 'food', 44, 'restaurant', 'Warm Chicken Salad', 'chicken, vegetable, crisp bread, caeser dressing', 'Warm Chicken Salad_53_44_food_restaurant', 'Tk 450 +', '', '', '', '', '', '', '', '', 'appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-12', '00:11:50', 0, ''),
(54, 'food', 44, 'restaurant', 'Napolitana', 'Tomato sauce, mozzarella cheese and anchovies', 'Napolitana_54_44_food_restaurant.jpg', 'Tk 425 +  6â€ <br>Tk 845 +  12â€', '', '', '', '', '', '', '', '', 'Pizza', '', '', 0, 0, 0, 0, 0, '2017-04-12', '00:11:50', 0, ''),
(55, 'food', 44, 'restaurant', 'Tiramisu', 'Mascarpone cheese, double cream, savoiardi biscuit, coffee, coco powder, egg', 'Tiramisu_55_44_food_restaurant.jpg', 'Tk 400 +', '', '', '', '', '', '', '', '', 'dessert', '', '', 0, 0, 0, 0, 0, '2017-04-12', '00:11:50', 0, ''),
(56, 'food', 45, 'restaurant', 'Corn-On-the-Cob', 'Corn-on-the cob, flame grilled and basted in your choice of peri-peri for extra flavour', 'Corn-On-the-Cob_56_45_food_restaurant.jpg', 'Tk 165 +', '', '', '', '', '', '', '', '', 'Appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-12', '01:12:17', 1, ''),
(57, 'food', 45, 'restaurant', 'Fries', '', 'Fries_57_45_food_restaurant.jpg', 'Tk 99 + (regular) <br>Tk 155 + (Large) <br>Tk 265 + (jumbo)', '', '', '', '', '', '', '', '', 'Side', '', '', 0, 0, 0, 0, 0, '2017-04-12', '01:12:17', 1, ''),
(58, 'food', 45, 'restaurant', 'Quarter Chicken Meal', '', 'Quarter Chicken Meal_58_45_food_restaurant.jpg', 'Tk 399 +', '', '', '', '', '', '', '', '', 'Main Dish', '', '', 0, 0, 0, 0, 0, '2017-04-12', '01:12:17', 1, ''),
(59, 'food', 46, 'restaurant', 'Blue Mussels ', 'With garlic lemon butter sauce, served with bread', 'Blue Mussels _59_46_food_restaurant', 'Tk 685 +', '', '', '', '', '', '', '', '', 'Main Dish', '', '', 0, 0, 0, 0, 0, '2017-04-12', '02:29:39', 1, ''),
(60, 'food', 46, 'restaurant', 'Salmon & White Fish', 'A perfect combo of sashimi-grade salmon and grilled white fish', 'Salmon & White Fish_60_46_food_restaurant', 'Tk 975 +', '', '', '', '', '', '', '', '', 'Main Dish', '', '', 0, 0, 0, 0, 0, '2017-04-12', '02:29:39', 1, ''),
(61, 'food', 46, 'restaurant', 'Grilled Peri-Peri Prawns', 'Succulent prawns butterfried and grilled, topped with peri-peri spicy sauce', 'Grilled Peri-Peri Prawns_61_46_food_restaurant.png', 'Tk 800 +', '', '', '', '', '', '', '', '', 'Main Dish', '', '', 0, 0, 0, 0, 0, '2017-04-12', '02:29:39', 1, ''),
(62, 'food', 47, 'restaurant', 'Set Menu', 'Fire Cracker Prawn/Korean Wings, Thai Fried Rice, Chowmein, Garlic & Pepper Chicken, Shredded Beef, Fish in Black Pepper, Brownie, Mousse', 'Set Menu_62_47_food_restaurant.jpg', 'Tk 1300 + (for 2) <br>Tk 2500 + (for 4)', '', '', '', '', '', '', '', '', 'Set menu', '', '', 0, 0, 0, 0, 0, '2017-04-12', '02:53:40', 1, ''),
(63, 'food', 47, 'restaurant', 'Set Menu', 'Fire Cracker Prawn/Korean Wings, Egg Fried Rice, Chowmein, Chicken Chili Onion,  Shredded Beef, Fish in Black Pepper, Mixed Vegetable ', 'Set Menu_63_47_food_restaurant', 'Tk 1100 + (for 2) <br>Tk 2100 + (for 4)', '', '', '', '', '', '', '', '', 'Set menu', '', '', 0, 0, 0, 0, 0, '2017-04-12', '02:53:40', 1, ''),
(64, 'food', 47, 'restaurant', 'Set Menu', '2 Fire Cracker Prawn, Egg Fried Rice, Beef Chilli Dry, Chicken Naga Curry, Mustered Spiced Prawn', 'Set Menu_64_47_food_restaurant.jpg', 'Tk 360 +  <br>(single set)', '', '', '', '', '', '', '', '', 'Set menu', '', '', 0, 0, 0, 0, 0, '2017-04-12', '02:53:40', 1, ''),
(65, 'food', 48, 'restaurant', 'Cashew nut Salad', '', 'Cashew nut Salad_65_48_food_restaurant.jpg', 'Tk 150 +', '', '', '', '', '', '', '', '', 'Appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-12', '03:54:29', 1, ''),
(66, 'food', 48, 'restaurant', 'Â¼ BBQ Chicken + 2 sides', '', 'Â¼ BBQ Chicken  2 sides_66_48_food_restaurant.jpg', 'Tk 210 +', '', '', '', '', '', '', '', '', 'appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-12', '03:54:30', 1, ''),
(67, 'food', 48, 'restaurant', 'Pomfret Fish Fry + 1 side dish', '', 'Pomfret Fish Fry  1 side dish_67_48_food_restaurant.jpg', 'Tk 390 +', '', '', '', '', '', '', '', '', 'Main Dish', '', '', 0, 0, 0, 0, 0, '2017-04-12', '03:54:30', 1, ''),
(68, 'food', 49, 'restaurant', 'Blend Burger', '', 'Blend Burger_68_49_food_restaurant.jpg', 'Tk 180', '', '', '', '', '', '', '', '', 'Burger', '', '', 0, 0, 0, 0, 0, '2017-04-12', '04:18:26', 1, ''),
(69, 'food', 49, 'restaurant', 'Big Blend Burger ', 'half pound', 'Big Blend Burger _69_49_food_restaurant.jpg', 'Tk 395', '', '', '', '', '', '', '', '', 'Burger', '', '', 0, 0, 0, 0, 0, '2017-04-12', '04:18:26', 1, ''),
(70, 'food', 49, 'restaurant', 'American Philly Cheese Steak', '', 'American Philly Cheese Steak_70_49_food_restaurant', 'Tk 250', '', '', '', '', '', '', '', '', 'Steak', '', '', 0, 0, 0, 0, 0, '2017-04-12', '04:18:26', 1, ''),
(71, 'food', 49, 'restaurant', '', '', '_71_49_food_restaurant.jpg', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-12', '04:18:26', 1, ''),
(72, 'food', 50, 'restaurant', 'Buffalo Wings (8pcs)', 'Itâ€™s sweet, tangy and savory. The perfect dish to warm up your taste buds', 'Buffalo Wings 8pcs_72_50_food_restaurant', 'Tk 250+', '', '', '', '', '', '', '', '', 'Appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-12', '04:43:39', 1, ''),
(73, 'food', 50, 'restaurant', 'New England Mussel-Clam Chowder ', 'A soup filled with richness of clams, mussels and other seafood', 'New England Mussel-Clam Chowder _73_50_food_restaurant', 'Tk 220+', '', '', '', '', '', '', '', '', 'appetizer', '', '', 0, 0, 0, 0, 0, '2017-04-12', '04:43:39', 1, ''),
(74, 'food', 54, 'restaurant', '', '', '_74_54_food_restaurant', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-12', '12:57:25', 0, ''),
(75, 'food', 56, 'cafe', 'Regular Hot Coffee', '', 'Regular Hot Coffee_75_56_food_cafe', 'Tk 35', '', '', '', '', '', '', '', '', 'Coffee', '', '', 0, 0, 0, 0, 0, '2017-04-12', '17:35:14', 1, ''),
(76, 'food', 56, 'cafe', 'Hot Chocolate Coffee', '', 'Hot Chocolate Coffee_76_56_food_cafe.jpg', 'Tk 45', '', '', '', '', '', '', '', '', 'Coffee', '', '', 0, 0, 0, 0, 0, '2017-04-12', '17:35:14', 1, ''),
(77, 'food', 56, 'cafe', 'Mango Crushers', '', 'Mango Crushers_77_56_food_cafe.jpg', 'Tk 50', '', '', '', '', '', '', '', '', 'Cold Beverage', '', '', 0, 0, 0, 0, 0, '2017-04-12', '17:35:14', 1, ''),
(78, 'food', 56, 'cafe', 'Strawberry Crushers', '', 'Strawberry Crushers_78_56_food_cafe', 'Tk 50', '', '', '', '', '', '', '', '', 'Cold Beverage', '', '', 0, 0, 0, 0, 0, '2017-04-12', '17:35:14', 1, ''),
(79, 'food', 56, 'cafe', 'Chocolate Blast', '', 'Chocolate Blast_79_56_food_cafe.jpg', 'Tk 70', '', '', '', '', '', '', '', '', 'Cold Beverage', '', '', 0, 0, 0, 0, 0, '2017-04-12', '17:35:14', 1, ''),
(80, 'food', 56, 'cafe', 'Chocolate Crushers', '', 'Chocolate Crushers_80_56_food_cafe.jpg', 'Tk 50', '', '', '', '', '', '', '', '', 'Cold Beverage', '', '', 0, 0, 0, 0, 0, '2017-04-12', '17:35:14', 1, ''),
(81, 'food', 59, 'cafe cake_pastry', '', '', '_81_59_food_cafe cake_pastry', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2017-04-15', '06:13:36', 1, ''),
(82, 'food', 60, 'restaurant cafe', 'Espresso illy ', 'single', 'Espresso illy _82_60_food_restaurant cafe.png', 'Tk 220 +', '', '', '', '', '', '', '', '', 'Specials', '', '', 0, 0, 0, 0, 0, '2017-04-18', '06:06:42', 1, ''),
(83, 'food', 60, 'restaurant cafe', 'Affogato', '', 'Affogato_83_60_food_restaurant cafe.jpg', 'Tk 250 +', '', '', '', '', '', '', '', '', 'Cold Specials', '', '', 0, 0, 0, 0, 0, '2017-04-18', '06:06:42', 1, ''),
(84, 'food', 60, 'restaurant cafe', 'Grilled Chicken Sandwich', '', 'Grilled Chicken Sandwich_84_60_food_restaurant cafe.jpg', 'Tk 380 +', '', '', '', '', '', '', '', '', 'Sandwich with fries', '', '', 0, 0, 0, 0, 0, '2017-04-18', '06:06:42', 1, ''),
(85, 'food', 61, 'cafe', 'PappaRoti Bun', '', 'PappaRoti Bun_85_61_food_cafe.jpg', 'Tk 145', '', '', '', '', '', '', '', '', 'Bun', '', '', 0, 0, 0, 0, 0, '2017-04-18', '08:27:31', 1, ''),
(86, 'food', 61, 'cafe', 'Flavoured Shake', '', 'Flavoured Shake_86_61_food_cafe', 'Tk 280', '', '', '', '', '', '', '', '', 'Non-coffee Drinks', '', '', 0, 0, 0, 0, 0, '2017-04-18', '08:27:31', 1, ''),
(87, 'food', 61, 'cafe', 'Cafe Latte', '', 'Cafe Latte_87_61_food_cafe.jpg', 'Tk 210', '', '', '', '', '', '', '', '', 'Espresso Base Drinks', '', '', 0, 0, 0, 0, 0, '2017-04-18', '08:27:31', 1, ''),
(88, 'food', 61, 'cafe', 'Cappuccino', '', 'Cappuccino_88_61_food_cafe.jpg', 'Tk 210', '', '', '', '', '', '', '', '', 'Espresso Base Drinks', '', '', 0, 0, 0, 0, 0, '2017-04-18', '08:27:31', 1, ''),
(89, 'food', 62, 'restaurant cake_pastry', 'Dizzy Dog', '', 'Dizzy Dog_89_62_food_restaurant cake_pastry.jpg', 'Tk 60', '', '', '', '', '', '', '', '', 'Bakery Item', '', '', 0, 0, 0, 0, 0, '2017-04-18', '15:36:38', 1, ''),
(90, 'food', 62, 'restaurant cake_pastry', 'Beef Puff', '', 'Beef Puff_90_62_food_restaurant cake_pastry', 'Tk 65', '', '', '', '', '', '', '', '', 'Bakery Item', '', '', 0, 0, 0, 0, 0, '2017-04-18', '15:36:38', 1, ''),
(91, 'food', 62, 'restaurant cake_pastry', 'Angry Burger', 'One Angry Burger Regular/Spicy', 'Angry Burger_91_62_food_restaurant cake_pastry.jpg', 'Tk 230/ 255', '', '', '', '', '', '', '', '', 'Burger', '', '', 0, 0, 0, 0, 0, '2017-04-18', '15:36:38', 1, ''),
(92, 'food', 62, 'restaurant cake_pastry', 'American Choco Cake (L)', '', 'American Choco Cake L_92_62_food_restaurant cake_pastry.png', 'Tk 1500', '', '', '', '', '', '', '', '', 'Cake', '', '', 0, 0, 0, 0, 0, '2017-04-18', '15:36:38', 1, ''),
(93, 'food', 63, 'cake_pastry', 'Mini Cakes', '', 'Mini Cakes_93_63_food_cake_pastry', 'Tk 300', '', '', '', '', '', '', '', '', 'Regular Cakes', '', '', 0, 0, 0, 0, 0, '2017-04-20', '01:07:34', 1, ''),
(94, 'food', 63, 'cake_pastry', 'Fondant Cake 1kg', 'Minimum order 2kg', 'Fondant Cake 1kg_94_63_food_cake_pastry.jpg', 'Tk 1800', '', '', '', '', '', '', '', '', 'Pre Order Cake', '', '', 0, 0, 0, 0, 0, '2017-04-20', '01:07:34', 1, ''),
(95, 'food', 64, 'cake_pastry', 'House White', 'our signature house loaf. It is semi-sour in flavour, square in shape, & ideal for sandwiches', 'House White_95_64_food_cake_pastry.jpg', 'Tk 210 +', '', '', '', '', '', '', '', '', 'Artisan Breads', '', '', 0, 0, 0, 0, 0, '2017-04-20', '07:16:22', 1, ''),
(96, 'food', 65, 'cake_pastry', 'Vanilla frosting with whipped cream + Fondant decoration', '', 'Vanilla frosting with whipped cream  Fondant decoration_96_65_food_cake_pastry', 'Tk 950', '', '', '', '', '', '', '', '', 'Vanilla Flavor', '', '', 0, 0, 0, 0, 0, '2017-04-20', '09:35:01', 1, ''),
(97, 'food', 65, 'cake_pastry', 'Vanilla frosting with butter cream + Fondant decoration', '', 'Vanilla frosting with butter cream  Fondant decoration_97_65_food_cake_pastry', 'Tk 1400', '', '', '', '', '', '', '', '', 'Vanilla Flavor', '', '', 0, 0, 0, 0, 0, '2017-04-20', '09:35:01', 1, ''),
(98, 'food', 66, 'cake_pastry', 'Krispy Red Velvet', '', 'Krispy Red Velvet_98_66_food_cake_pastry.png', '', '', '', '', '', '', '', '', '', 'Doughnuts', '', '', 0, 0, 0, 0, 0, '2017-04-20', '16:11:38', 1, ''),
(99, 'food', 66, 'cake_pastry', 'Powdered Blueberry Filled', 'A A yeast-raised doughnut shell jam-packed with blueberry filing and coated with a light dusting of powdered sugar', 'Powdered Blueberry Filled_99_66_food_cake_pastry.png', '', '', '', '', '', '', '', '', '', 'Doughnuts', '', '', 0, 0, 0, 0, 0, '2017-04-20', '16:11:38', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `item_multi_image`
--

CREATE TABLE IF NOT EXISTS `item_multi_image` (
  `id` int(50) NOT NULL,
  `cata` varchar(100) NOT NULL,
  `item_id` int(50) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `short_note` varchar(1200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_multi_image`
--

INSERT INTO `item_multi_image` (`id`, `cata`, `item_id`, `image_link`, `short_note`) VALUES
(1, '', 73, 'New England Mussel-Clam Chowder __73_50_food_restaurant.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE IF NOT EXISTS `map` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `contact` varchar(300) NOT NULL,
  `address` varchar(200) NOT NULL,
  `map` varchar(600) NOT NULL,
  `date` date NOT NULL,
  `admin_id` int(255) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `cata_id`, `cata`, `sub_cata`, `location`, `contact`, `address`, `map`, `date`, `admin_id`, `status`) VALUES
(1, 1, 'tourism', 'natural_site', 'Sylhet', '', 'Bisnakandi, Gowainghat, Sylhet, Bangladesh', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14443.93250573035!2d91.8772029308056!3d25.170047592539362!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x37505936b3e510e7%3A0x840163039a06ef35!2sBisnakandi%2C+Bangladesh!5e0!3m2!1sen!2s!4v1489666952268" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2017-03-19', 1, ''),
(2, 2, 'tourism', 'heritage', 'Puran Dhaka', 'Tele: 9673018', 'Lalbagh, Puran Dhaka, Banglaadesh ', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.818469390087!2d90.38441871498075!3d23.718175784605354!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8d900000001%3A0x4e16316ffc66e862!2sLalbagh+Fort!5e0!3m2!1sen!2s!4v1483617405176" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> ', '2017-03-19', 1, ''),
(3, 3, 'tourism', 'picnic_spot', 'Dhamrai', 'Phone: +8801715-043416, 01717374904, 01190257062', 'Kalampur,Mohisati,Dhamrai, Dhaka', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3646.3496504799296!2d90.11171801498513!3d23.948072984492285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755f07e7a232ed3%3A0x1405889ce6104574!2sMohammadi+Garden!5e0!3m2!1sen!2s!4v1484298233772" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> ', '2017-03-19', 1, ''),
(4, 3, 'tourism', 'picnic_spot', '', '', '', '', '2017-03-19', 1, ''),
(5, 4, 'tourism', 'lake_park', 'Dhanmondi', '', 'Dhanmondi Lake Rd, Dhanmondi, Dhaka, Bangladesh', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14608.123604037246!2d90.36684758036087!3d23.74627745346081!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8b346e82523%3A0x7a53eb89f2c8fddd!2sDhanmondi+Lake!5e0!3m2!1sen!2s!4v1484301208211" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2017-03-19', 1, ''),
(6, 5, 'residence', 'resort', 'Gazipur', 'Office:  House: 157/B (2nd Floor), Road: 22  Mohakhali New DOHS, Dhaka 1206, Bangladesh. Phone: +88 01712 882179, +8802-9835173, 01771-799410, 01190980214, 01195356165 Email: nokkhottrobari@gmail.com , nokkhottrobari@live.com  ', 'Chinashukhania, Rajabari Bazaar, Rajendrapur, Bangladesh.', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3642.2222123932543!2d90.5038080149879!3d24.093679784421266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755d6f53f7a100f%3A0x323477a2ef963c24!2sNokkhottrobari+Resort+%26+Conference+Center!5e0!3m2!1sen!2s!4v1484557943540" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2017-03-19', 1, ''),
(7, 6, 'food', 'restaurant', 'Lalmatia', '9118695, 01195057002, 01745206473', '2/4 Block-C, Lalmatia, Dhaka-1207 Phone: 9118695, 01195057002', '', '2017-03-19', 2, ''),
(8, 7, 'residence', 'hotel', 'Munshiganj ', '	<br><b>Booking Office, Dhaka</b> <br>House # 380, Road # 28(Ground Floor), <br>Mohakhali DOHS, Dhaka-1206, Bangladesh. <br>Phone # 01712-170330, 01752-987688 (10am - 6pm) <br><b>Email:</b>  info@padmaresort.net   	<br><b>In case of Emergency:</b> <br>General Manager  <br>S.M. Nazrul Islam  <br>Phon', 'Louhajang , Munshiganj', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d117117.90802638102!2d90.2618742!3d23.4628225!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755a19b7a0bea37%3A0x7a584cc40730c80!2sPadma+Resort!5e0!3m2!1sen!2sbd!4v1489952884963" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2017-03-19', 1, ''),
(9, 7, 'residence', 'hotel', '', '', '', '', '2017-03-19', 1, ''),
(10, 8, 'residence', 'hotel', 'Dhaka', 'Phone: (880)(2) 9891988', 'Gulshan Avenue, Plot-01, Road 45, <br>Gulshan-2 Dhaka, 1212 Bangladesh', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.715311800482!2d90.41244381454408!3d23.79314998456839!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7a72f17bb83%3A0x57188ff62fd95026!2sThe+Westin+Dhaka!5e0!3m2!1sen!2sbd!4v1475172374720" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2017-03-19', 1, ''),
(11, 9, 'food', 'restaurant', 'Banani', 'Phone: 01616-666543', 'House 34 (2nd Floor), Road 10, Block D, Banani, Banani Model Town, 1213 Dhaka', '', '2017-03-19', 2, ''),
(12, 9, 'food', 'restaurant', 'Uttara', 'Phone: 01616-666544', 'Auckland Center (3rd Floor), House 11, Road 6, Sector 4, 1230 Uttara 1230 Dhaka', '', '2017-03-19', 2, ''),
(13, 11, 'food', 'restaurant', 'Gulshan', 'Phone: +88029854110, +8801841684425', 'House 4, Road 9, Gulshan 1 (next to DCC market), 1212 Dhaka, Bangladesh ', '', '2017-03-19', 2, ''),
(14, 12, 'life style', 'fashion', 'Dhanmondi', 'Phone: 02-8618220 <br>ext- 10017', 'House# 17, Road# 02, Dhanmondi, Dhaka', '', '2017-03-22', 1, ''),
(15, 12, 'life style', 'fashion', 'Gulshan', 'Phone: 02-8881376', 'Shop No# 14-19 and 24-29 <br>House# 15(CEN), Road# 103 <br>Gulshan Avenue, Gulshan 2.', '', '2017-03-22', 1, ''),
(16, 12, 'life style', 'fashion', 'Panthapath', 'Phone: 02-91114401 <br>ext- 101057', 'BASHUNDHARA CITY STORE <br>Shop No# 56, 57, 58, 57, 68, 69 <br>Level# 01, Block# A, Panthapath', '', '2017-03-22', 1, ''),
(17, 12, 'life style', 'fashion', 'Bashundhara', 'Phone: 02-9823224', 'JAMUNA STORE <br>Shop No# 18,19,20 <br>Block# D, Level# 01, Bashundhara Residential Area.', '', '2017-03-22', 1, ''),
(18, 12, 'life style', 'fashion', 'Banani', 'Phone: 88 01676343938', 'House# 82, Road# 11, Block# D <br>Lintoo Centre, Banani.', '', '2017-03-22', 1, ''),
(19, 12, 'life style', 'fashion', 'Uttara', 'Phone: 02-8956137', 'House# 15, Sector# 07 <br>Sonargaon Janapath, Uttara.', '', '2017-03-22', 1, ''),
(20, 12, 'life style', 'fashion', 'Uttara', 'Phone: 02-8956137', 'House# 17, Sector# 13 <br>Sonargaon Janapath, Uttara.', '', '2017-03-22', 1, ''),
(21, 12, 'life style', 'fashion', 'Maghbazar', 'Phone: 02-9357812', '70, Outer Circular Road <br>Shahid Sangbadik Selina Parvin Road, Maghbazar.', '', '2017-03-22', 1, ''),
(22, 12, 'life style', 'fashion', 'Mohammadpur', 'Phone: 88 01798558129', '02, Ring Road, Mohammadpur <br>Opposite to Japan Garden City.', '', '2017-03-22', 1, ''),
(23, 12, 'life style', 'fashion', 'Ramna', 'Phone: 02-9330772', 'Navana Bailey Star <br>House# 09, Shop No# 24 <br>New Bailey Road, Ramna.', '', '2017-03-22', 1, ''),
(24, 12, 'life style', 'fashion', 'Wari', 'Phone: 88 01725222211', '17/A, Rankin Street, Wari', '', '2017-03-22', 1, ''),
(25, 13, 'life style', 'accessorice', 'Gulshan', 'Phone: +8801713199270', '68/1 Gulshan Avenue, Gulshan-1, <br>Dhaka-1212', '', '2017-03-22', 1, ''),
(26, 13, 'life style', 'accessorice', 'Baily Road', 'Phone: +8801713199270', 'Gold Place (2nd floor), 3 New Baily Road, <br>(10 Natok Sarani), Dhaka â€“ 1000.', '', '2017-03-22', 1, ''),
(27, 13, 'life style', 'accessorice', 'Mirpur', 'Phone: +8801713199270', 'Ejab Islam Tower, (1st Floor),  <br>House No- 08, Block-A, Section- 11, (Main Road ) <br>Pallabi, Mirpur, Dhaka.', '', '2017-03-22', 1, ''),
(28, 13, 'life style', 'accessorice', 'Panthapath', 'Phone: +8801713199270', 'Bashundhara City - Level-5,  Block -A , <br>Bashundhara City , Panthapath , Dhaka -1215 .', '', '2017-03-22', 1, ''),
(29, 13, 'life style', 'accessorice', 'Uttara', 'Phone: +8801713199270', 'Noyab Mansion(1st floor) <br>Plot:22,Sector-11 <br>Sonargoan  Janopath Road Uttara,Dhaka', '', '2017-03-22', 1, ''),
(30, 13, 'life style', 'accessorice', 'Baitul Mukarram', '', 'Najrana, Shop-27 Baitul Mukaram (1st Floor)  <br>Dhaka - 1000', '', '2017-03-22', 1, ''),
(31, 13, 'life style', 'accessorice', 'Dhanmondi', '', 'Rangs Nasim Square 275/D, <br>Dhanmondi-27, Dhaka (Beside Meena Bazar)', '', '2017-03-22', 1, ''),
(32, 13, 'life style', 'accessorice', 'Mohammadpur', 'Phone: +8801713199270', 'Level: 04, Shop No: 436, Tokyo Square Shopping Center, <br>Japan Garden City, Tajmahal Road, Mohammadpur, <br>Dhaka â€“ 1207.', '', '2017-03-22', 1, ''),
(33, 13, 'life style', 'accessorice', '', '', '', '', '2017-03-22', 1, ''),
(34, 14, 'life style', 'parlor_salon', 'Gulshan', 'Phone: 01926-153010, 02-5-8812215, 02-5-8812172', 'SAM Tower, Level-2 <br>House #4, Road #22 <br>Gulshan-1, Dhaka-1212', '', '2017-03-22', 1, ''),
(35, 14, 'life style', 'parlor_salon', '', '', '', '', '2017-03-22', 1, ''),
(36, 14, 'life style', 'parlor_salon', 'Dhanmondi', 'Phone: 01713-451776, 02-9116057', 'Gemcon Building, Level-3 (above Meena Bazar) <br>House #44, Road #27 (old)/ Road #16 (new) <br>Dhanmondi, Dhaka-1205.', '', '2017-03-22', 1, ''),
(37, 14, 'life style', 'parlor_salon', 'Uttara', 'Phone: 01776-194215, 02-8953436', 'House #08, Rabindra Sarani, Sector-07 <br>Uttara Model Town, Dhaka-1230	', '', '2017-03-22', 1, ''),
(38, 15, 'life style', 'gym_fitness', 'Panthapath', '', 'Adonize Fitness Center LTD,Bashundhara City, Level 9 & 10,Panthapath, Dhaka, Bangladesh', 'Phone : 8158864-5,Mobile:01731922942 (office),  02-8158864 \r\n<br>Email:  ronyislam99@gmail.com\r\nwidth=100% height=250px src', '2017-03-22', 1, ''),
(39, 16, 'life style', 'accessorice', 'Elephant Road', '', '43, ELEPHANT ROAD, DHAKA', '', '2017-03-22', 1, ''),
(40, 16, 'life style', 'accessorice', 'Dhanmondi', '', 'SHIMANTO SQUARE, 3RD FLOOR DHAKA', '', '2017-03-22', 1, ''),
(41, 16, 'life style', 'accessorice', 'Newmarket', '', 'BALAKA BUILDING, NEWMARKET, DHAKA ', '', '2017-03-22', 1, ''),
(42, 16, 'life style', 'accessorice', 'Panthapath', '', 'BASHUNDHORA CITY SHOPPING MALL, LEVEL -6, DHAKA', '', '2017-03-22', 1, ''),
(43, 16, 'life style', 'accessorice', 'Mirpur', '', 'RING ROAD, SHAMOLI, DHAKA', '', '2017-03-22', 1, ''),
(44, 16, 'life style', 'accessorice', 'Banani', '', '36, KAMAL ATATURK AVENUE, BANANI, DHAKA', '', '2017-03-22', 1, ''),
(45, 16, 'life style', 'accessorice', 'Uttara', '', 'SONARGAON JANAPATH, SECTOR -9, UTTARA, DHAKA', '', '2017-03-22', 1, ''),
(46, 16, 'life style', 'accessorice', 'Dhanmondi', '', 'RAPA PLAZA, 2ND FLOOR DHANMONDI, DHAKA', '', '2017-03-22', 1, ''),
(47, 17, 'food', 'cafe', 'Dhanmondi', 'Phone: (88) 0192-933-3888, 0192-933-3999', '67 (New), 767 (Old), GH Heights ( 1stFloor) <br>Satmasjid Road, Dhanmondi <br>Dhaka, Bangladesh.', '', '2017-03-22', 1, ''),
(48, 17, 'food', 'cafe', 'Gulshan', 'Tele: (88-02) 883-1117 <br>Phone:0197-000-8989', 'House 35, Gulshan Avenue,  Gulshan <br>Dhaka, Bangladesh.', '', '2017-03-22', 1, ''),
(49, 17, 'food', 'cafe', 'Gulshan', '', 'House 2B, Block NE(B)  <br>Road 71, Gulshan North Avenue, Gulshan-2 <br>Dhaka, Bangladesh.', '', '2017-03-22', 1, ''),
(50, 18, 'food', 'cafe', 'Badda', 'Phone: 01741055597, 01941788506', 'Kha- 47/1 Pragati Sharani, Shahjadpur, <br>Gulshan, Dhaka 1212', '', '2017-03-22', 1, ''),
(51, 18, 'food', 'cafe', 'Banani', '', 'Lakeshore Hotel, Banani', '', '2017-03-22', 1, ''),
(52, 18, 'food', 'cafe', 'Baridhara', '', 'AISD (Open except when school campus is closed. <br>Customers must have authorized access to campus)  <br>12 United Nations Rd, Dhaka 1212, Bangladesh ', '', '2017-03-22', 1, ''),
(53, 18, 'food', 'cafe', '', '', '', '', '2017-03-22', 1, ''),
(54, 18, 'food', 'cafe', 'Bashundhara', '', 'Apollo Hospital, Bashundhara', '', '2017-03-22', 1, ''),
(55, 18, 'food', 'cafe', 'Gulshan', '', '53 Gulshan Avenue', '', '2017-03-22', 1, ''),
(56, 18, 'food', 'cafe', '', '', '', '', '2017-03-22', 1, ''),
(57, 18, 'food', 'cafe', '', '', '', '', '2017-03-22', 1, ''),
(58, 18, 'food', 'cafe', '', '', '', '', '2017-03-22', 1, ''),
(59, 19, 'food', 'restaurant cart', 'Uttara', 'Phone: 01797162916, 01796205716', 'Shwapno Super Shop (3rd Floor) <br>Beside Rajlaxmi,Sector#03,Uttara.', '', '2017-03-22', 1, ''),
(60, 19, 'food', 'restaurant cart', 'Dhanmondi', 'Phone: 01704899901, 01796205716', 'Shimanto Square (Ground Floor) <br>Road#02,Dhanmondi,Dhaka-1205.', '', '2017-03-22', 1, ''),
(61, 19, 'food', 'restaurant cart', 'Uttara', 'Phone: 01704899902, 01796205716', '63,Gausul Azam Avenue, <br>Sector#14,Uttara.', '', '2017-03-22', 1, ''),
(62, 19, 'food', 'restaurant cart', 'Malibagh', 'Phone: 01704899903, 01796205716', 'Shwapno Super Shop (Ground Floor) <br>Malibagh Mor,Dhaka.', '', '2017-03-22', 1, ''),
(63, 19, 'food', 'restaurant cart', 'Rampura', 'Phone: 01911733167', 'House #01,Road #05,<br>Block#F (Beside DBBL ATM Booth) <br>Banasree, Rampura, Dhaka.', '', '2017-03-22', 1, ''),
(64, 20, 'food', 'sweets', 'Gulshan', 'Phone: 01617000080', 'House No.# 9, Road# 55/56, <br>Gulshan 2, Dhaka- 1212', '', '2017-03-22', 1, ''),
(65, 20, 'food', 'sweets', 'Gulshan', 'Phone: 01611000080', 'Shop No.# 122, Plot# 10, <br>Gulshan Circle 2, Dhaka-1212', '', '2017-03-22', 1, ''),
(66, 20, 'food', 'sweets', 'Banani', 'Phone: 01617000040', 'House No.# 26, Road# 11, Block# D, <br>Banani, Dhaka-1212', '', '2017-03-22', 1, ''),
(67, 20, 'food', 'sweets', 'Uttara', 'Phone: 01611000080', 'Road No.# 1, Sector# 1, Zashin Plaza (ground floor), <br>Uttara, Dhaka-1230', '', '2017-03-22', 1, ''),
(68, 20, 'food', 'sweets', 'Uttara', 'Phone: 01617000020', 'House No.# 08 (ground floor), <br>Sonargaon Janapath Road,Sector# 11, Uttara, Dhaka-1230', '', '2017-03-22', 1, ''),
(69, 20, 'food', 'sweets', 'Dhanmondi', 'Phone: 01616000050', 'House No.# 35, Road# 16 (old-27), <br>Dhanmondi R/A, Dhaka-1209', '', '2017-03-22', 1, ''),
(70, 20, 'food', 'sweets', 'Khilgaon', 'Phone: 01631000080', '593/C, Malibagh Chowdhury Para Shahid Baqi Road <br>(opposite of peoples hospital), Khilgaon, Dhaka', '', '2017-03-22', 1, ''),
(71, 20, 'food', 'sweets', 'Panthapath', 'Phone: 01631000070', '89/2, West Pathapath,Testuri Bazar Chalklane (East Raja Bazar), <br>Tejgaon,Dhaka-1215', '', '2017-03-22', 1, ''),
(72, 20, 'food', 'sweets', 'Baily Road', 'Phone: 01616000070', 'Green Cozy Cot, Shop#FFB (1st Floor), <br>Baily Road (Opposite of Baily Star)', '', '2017-03-22', 1, ''),
(73, 21, 'food', 'cake_pastry', 'Dhanmondi', '', '74, Bikolpo Bhobon, Saat Mashjid Road, <br>Dhanmondi 5a, Dhaka', '', '2017-03-22', 1, ''),
(74, 21, 'food', 'cake_pastry', 'Banani', 'Phone: 01760822356', '93, Road-6, Block-c, <br>Banani, Dhaka 1215', '', '2017-03-22', 1, ''),
(75, 23, 'food', 'cart', 'Kuril Bishwa Road', 'Phone: 01715-818714 ', 'Kazi food island, plot- 16720, 300 feet road, near bashundhara g block entrance, Dhaka', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3649.7494376888135!2d90.4294647149828!3d23.8275073845514!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c64757bb6ab1%3A0x1aa4cc12ef1b6765!2sKazi+Food+Island!5e0!3m2!1sen!2s!4v1491297915587" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2017-04-04', 1, ''),
(76, 24, 'food', 'cart', 'Banani', '', 'House # 93, Road # 6, Block # C, Banani, Dhaka', '', '2017-04-04', 1, ''),
(77, 24, 'food', 'cart', 'Dhanmondi', '', 'Located inside Shimanto Square Shopping, Dhaka, Bangladesh, 1209 	', '', '2017-04-04', 1, ''),
(78, 25, 'food', 'cart', 'Dhanmondi', 'Phone: 01913213949', 'Shimanto Square Market, Dhanmondi-2, Dhaka-1209', '', '2017-04-06', 1, ''),
(79, 26, 'food', 'cart', 'Dhanmondi', 'Phone: 01716-937871	', '5/A, Shat Mashjid Road, Dhanmondi (opposite of Jahajbari, medinova)', '', '2017-04-06', 1, ''),
(80, 26, 'food', 'cart', 'Banani', 'Phone: 01716-937871	', 'Banani 11 (beside Luamâ€™s)', '', '2017-04-06', 1, ''),
(81, 27, 'food', 'sweets', 'Gulshan', 'Phone: 01759115124', 'Plot No.22, Gulshan Circle 2, <br>Dhaka 1212, Bangladesh', '', '2017-04-07', 1, ''),
(82, 27, 'food', 'sweets', 'Dhanmondi', 'Phone: 01752683068', 'House No. 7, Road No. 4, Momataz Plaza, Dhanmondi, <br>Dhaka 1209, Bangladesh', '', '2017-04-07', 1, ''),
(83, 27, 'food', 'sweets', 'Gulshan', 'Phone: 01755997678, 01759997678', 'Green Square, House No. 1/B, Road No. 8, Apartment No. B/1, Gulshan-1, <br>Dhaka 1212, Bangladesh', '', '2017-04-07', 1, ''),
(84, 27, 'food', 'sweets', 'Sat Mosjid Road', '', 'Main road, Sat Mosjid Road', '', '2017-04-07', 1, ''),
(85, 27, 'food', 'sweets', '', '', '', '', '2017-04-07', 1, ''),
(86, 27, 'food', 'sweets', '', '', '', '', '2017-04-07', 1, ''),
(87, 27, 'food', 'sweets', '', '', '', '', '2017-04-07', 1, ''),
(88, 28, 'food', 'cake_pastry', 'Gulshan', '', 'Dhaka City Corporation Market, Gulshan 2', '', '2017-04-07', 1, ''),
(89, 28, 'food', 'cake_pastry', 'Dhanmondi', '', '1st Flr KB Square, 736 saatmasjid rd, Dhaka, Bangladesh ', '', '2017-04-07', 1, ''),
(90, 29, 'food', 'cake_pastry', 'Banani', 'Tel: (88) 0184-1320-946', 'Shop 10 (Ground Floor) <br>Bangladesh UAE Maitry Shopping Complex <br>Kemal Ataturk Avenue, Banani <br>Dhaka, Bangladesh.', '', '2017-04-07', 1, ''),
(91, 29, 'food', 'cake_pastry', 'Dhanmondi', 'Tel: (88) 0184-132-0944, 0184-132-0945', 'House 8/A/C, Road 13 <br>Behind Sobhanbag Mosque <br>Sobhanbag Square, Dhanmondi <br>Dhaka, Bangladesh.', '', '2017-04-07', 1, ''),
(92, 29, 'food', 'cake_pastry', 'Gulshan ', 'Tel: (88) 0184-132-0946, 0184-132-0947', 'SAM Tower, Road 22, Gulshan 2, <br>Dhaka, Bangladesh.', '', '2017-04-07', 1, ''),
(93, 29, 'food', 'cake_pastry', 'Uttara', 'Tel: (88) 0184-132-0946, 0184-132-0947', 'Jasimuddin Road,  Uttara <br>Dhaka, Bangladesh.', '', '2017-04-07', 1, ''),
(94, 29, 'food', 'cake_pastry', 'Uttara', 'Phone: +8801841321016', 'KC Tower <br>Plot# 13, Road# 13 <br>Shonargaon Janapath Road, Uttara', '', '2017-04-07', 1, ''),
(95, 30, 'food', 'cafe', 'Khilgaon', 'Phone: 01717611339 01792234263 01817112426', '381/B, Khilgaon, Chowdhuri Para, Dhaka', '', '2017-04-07', 1, ''),
(96, 31, 'food', 'cafe', 'Gulshan', 'Tel: (88-02) 881-2121', 'Plot SE(F)1 <br>Gulshan South Avenue <br>Biruttam Mir Showkat Ali Road Gulshan 1 <br>Dhaka, Bangladesh.', '', '2017-04-07', 1, ''),
(97, 31, 'food', 'cafe', 'Kawran Bazar', 'Tel: (88-02) 812-7596', 'Plot 10 <br>Jahangir Tower(Ground Floor) <br>Karwan Bazar (near Pan Pacific Sonargaon Hotel), <br>Dhaka, Bangladesh', '', '2017-04-07', 1, ''),
(98, 31, 'food', 'cafe', 'Dhanmondi', 'Tel: (88) 0167-316-0206', 'Concord Arcadia Shopping Center (inside the mall, ground floor) , <br>Road 4, Dhanmondi <br>Dhaka, Bangladesh.', '', '2017-04-07', 1, ''),
(99, 31, 'food', 'cafe', 'Airport', '', 'Hazrat Shah Jalal International Airport <br>Airport Barista Lounge <br>Dhaka, Bangladesh.', '', '2017-04-07', 1, ''),
(100, 31, 'food', 'cafe', '', '', '', '', '2017-04-07', 1, ''),
(101, 32, 'food', 'cafe', 'Bashundhara', 'Phone: 01726588566', 'Shop 5C 23, Level 5, Zone C, Jamuna Future Park, <br>Pragati Sarani, Dhaka, Bangladesh', '', '2017-04-07', 1, ''),
(102, 33, 'food', 'cafe', 'Uttara', 'Tel: (88-02) 896-1665', 'RAK Tower, Plot 1/A, Jashimuddin Avenue, <br>Sector 3, Uttara', '', '2017-04-07', 1, ''),
(103, 33, 'food', 'cafe', 'Dhanmondi', 'Tel: (88-02) 912-7056', 'Gemcon Building (1st floor), <br>Road 27, Dhanmondi', '', '2017-04-07', 1, ''),
(104, 33, 'food', 'cafe', 'Banani', '', 'House 65, Road 27, Block K (Next to Banani playground)', '', '2017-04-07', 1, ''),
(105, 33, 'food', 'cafe', 'Banani ', '', 'Banani 11', '', '2017-04-07', 1, ''),
(106, 33, 'food', 'cafe', '', '', '', '', '2017-04-07', 1, ''),
(107, 34, 'food', 'cafe', 'Gulshan', 'Tel: +880 29 850860', '175 Gulshan Avenue, Gulshan 2, Dhaka. <br>Opposite the Australian Embassy (beside Nandos, under Khaana Khazanna).', '', '2017-04-07', 1, ''),
(108, 35, 'food', 'cafe', 'Uttara', 'Phone: +880 2 8931906 <br>Hot Line : 0171808050', 'House# 9, Road# 18, Sector# 3, <br>Uttara, Dhaka - 1230.', '', '2017-04-08', 1, ''),
(109, 36, 'food', 'cafe', 'Banani', 'Phone:  01717-105042', '2nd Floor, Point 11, House # 25, <br>Block # H, Road # 11, Banani, Dhaka', '', '2017-04-08', 1, ''),
(110, 36, 'food', 'cafe', 'Dhanmondi', '', 'House 275/D, Rangs Nasim Square, <br>Ground Floor, Road 27 (Old), Dhanmondi', '', '2017-04-08', 1, ''),
(111, 37, 'food', 'cafe', 'Uttara', 'Phone: 01784-436743', 'House-2, Road-10, Sector-1, Uttara, Dhaka', '', '2017-04-08', 1, ''),
(112, 37, 'food', 'cafe', 'Dhanmondi', '', '2nd floor, Shop no. 265-266, Shimanto Square', '', '2017-04-08', 1, ''),
(113, 38, 'food', 'restaurant', 'Gulshan', 'Phone: 01766293935', 'House 24, Road 2, Gulshan 1, Dhaka', '', '2017-04-08', 0, ''),
(114, 38, 'food', 'restaurant', 'Uttara', 'Phone: 01776234517', 'House 54, Road 2, Sector 3, Uttara, Dhaka', '', '2017-04-08', 0, ''),
(115, 39, 'food', 'restaurant', 'Gulshan', 'Phone: 01941111333, 01941111555', 'House 9/A, Road 53, Gulshan 2, <br>Dhaka, Bangladesh', '', '2017-04-08', 1, ''),
(116, 39, 'food', 'restaurant', 'Uttara', 'Phone: 01754244455', 'House 02, Lake Drive Road, Sector 7, Uttara, <br>Dhaka, Bangladesh', '', '2017-04-08', 1, ''),
(117, 40, 'food', 'restaurant', 'Puran Dhaka', 'Phone: 01718-360657, 01941095484, 01556332867', '13/1 Abul Hasnat Road, <br>Satrowja, Dhaka 1100', '', '2017-04-08', 1, ''),
(118, 41, 'food', 'restaurant', 'Dhanmondi', 'Phone: 01721-904504', 'ZR Plaza, Level 10, Satmasjid Road, Dhanmondi 19 (9/A old), <br>Dhaka-1209, Bangladesh', '', '2017-04-09', 1, ''),
(119, 42, 'food', 'restaurant', 'Lalbagh ', '', '32/ 34 Ka Lalbagh Road (Near Lalbagh Fort), Dhaka-1211', '', '2017-04-09', 1, ''),
(120, 42, 'food', 'restaurant', 'Dhanmondi', '', '1/5 Block (D), Lalmatia, Dhanmondi, Dhaka-1207', '', '2017-04-09', 1, ''),
(121, 43, 'food', 'restaurant', 'Gulshan', 'Phone: 01777-700664', '155/A North Gulshan Avenue, Gulshan Circle-2 <br>Dhaka-1212,Bangladesh. ', '', '2017-04-11', 1, ''),
(122, 44, 'food', 'restaurant', 'Uttara', 'Phone: 01705-737888', 'House- 48, Road- 4, Sector- 03, Uttara, Dhaka', '', '2017-04-12', 0, ''),
(123, 45, 'food', 'restaurant', 'Gulshan ', 'Phone: 8812121, 8812122, 01841996644', 'Plot# SE (F) â€“ Gulshan South Avenue, Bir Uttam Mir Showkat Road, <br>Gulshan 1, Dhaka- 1212, Bangladesh', '', '2017-04-12', 1, ''),
(124, 45, 'food', 'restaurant', 'Dhanmondi', 'Phone: 8128125, 8121575', 'House- 43, Road- 16 (New) 27 (old), <br>Dhanmondi, Dhaka- 1206, Dhaka, Bangladesh', '', '2017-04-12', 1, ''),
(125, 45, 'food', 'restaurant', 'Gulshan', 'Phone: 984784, 9854785', 'House # 175/A, Road # 61, Gulshan Avenue, <br>Gulshan # 2, Dhaka', '', '2017-04-12', 1, ''),
(126, 45, 'food', 'restaurant', 'Banani', 'Phone: 9854697, 984698', 'House # 25, Road # 11, Block # H, Banani, <br>Dhaka, Bangladesh', '', '2017-04-12', 1, ''),
(127, 46, 'food', 'restaurant', 'Gulshan', 'Phone: 01777794181, +88-02-9895711', 'Crystal Palace, 22 Gulshan South Avenue', '', '2017-04-12', 1, ''),
(128, 47, 'food', 'restaurant', 'Banani', 'Phone: 01789531819 ', 'Road 12, House 37, Banani, Dhaka', '', '2017-04-12', 1, ''),
(129, 47, 'food', 'restaurant', 'Uttara', 'Phone: 01710811918', 'Sector 11, Gareeb-E Newaz Avenue, House 16, <br>Solar Trade Center, Uttara, Dhaka', '', '2017-04-12', 1, ''),
(130, 48, 'food', 'restaurant', 'Bashundhara ', 'Phone: 01726-456088', '300feet road, Bashundhara R/A, <br>Dhaka, Bangladesh', '', '2017-04-12', 1, ''),
(131, 49, 'food', 'restaurant', 'Bashundhara', 'Phone: 01782-506121', 'Bashundhara R/A old gate road, Heavily Nosor Tower, <br>Opposite of Mr. Baker', '', '2017-04-12', 1, ''),
(132, 50, 'food', 'restaurant', 'Dhanmondi', 'Phone: 880 1614-204913', 'Road 5A, Level 6, House 74, Bikalpa Tower, <br>Dhanmondi, Dhaka.', '', '2017-04-12', 1, ''),
(133, 51, 'food', 'restaurant', 'Khilgaon', 'Phone: +8801670727370', 'House # 566/A, Block-C, Khilgaon Taltola (5th Floor), <br>Dhaka - 1219, Bangladesh.', '', '2017-04-12', 1, ''),
(134, 51, 'food', 'restaurant', 'Dhanmondi', 'Phone: +8801670727370 ', 'House 67, Level 5, Sat Masjid Road, Dhanmondi R/A, <br>Opposite to Bangladesh Medical College, Sankar, Dhaka 1205.', '', '2017-04-12', 1, ''),
(135, 52, 'food', 'restaurant', 'Banani', 'Phone: 01715-780379', 'Rd No.17A, Dhaka 1213, Bangladesh', '', '2017-04-12', 1, ''),
(136, 52, 'food', 'restaurant', 'Dhanmondi', 'Phone: 01735549567,01843481477 ', 'Dhanmondi 3/A, Dhaka< Bangladesh', '', '2017-04-12', 1, ''),
(137, 53, 'food', 'restaurant', 'Banani', '', 'Abedin Tower 35 Kamal Ataturk Avenue, <br>Banani, Dhaka, Bangladesh', '', '2017-04-12', 1, ''),
(138, 53, 'food', 'restaurant', 'Dhanmondi', '', 'GH Heights, 67 Satmasjid Road, <br>Dhanmondi Dhaka, Bangladesh', '', '2017-04-12', 1, ''),
(139, 55, 'food', 'restaurant cafe', '', 'Phone: 01755-555844', 'HB tower, House # 1/A, Road # 23, <br>Gulshan 1, Dhaka, Bangladesh', '', '2017-04-12', 1, ''),
(140, 56, 'food', 'cafe', 'Uttara', 'Phone:  +8801679318593/4', 'Sah Mokhdum Road No # 20, House # 2, <br>Sector # 11, Uttara, Dhaka', '', '2017-04-12', 1, ''),
(141, 57, 'food', 'cafe', 'Khilgaon', 'Phone: 01799646400, 01676914307', '379/B, Khilgaon (Taltola), Dhaka', '', '2017-04-13', 1, ''),
(142, 58, 'food', 'cafe cake_pastry', 'Gulshan ', 'Phone: 02-9861389', '1st Floor, Bashati Avenue, House# 10, <br>Road# 53, Gulshan Circle- 2, Dhaka', '', '2017-04-13', 1, ''),
(143, 58, 'food', 'cafe cake_pastry', 'Dhanmondi', '', 'House# 32/1, Road# 3, Dhanmondi, Dhaka', '', '2017-04-13', 1, ''),
(144, 59, 'food', 'cafe cake_pastry', 'Gulshan', 'Phone: +88 09683 995 995', 'Plot# 11/A, Road# 117, Gulshan 2, Dhaka 1212', '', '2017-04-15', 1, ''),
(145, 60, 'food', 'restaurant cafe', 'Gulshan', 'Phone: 01919774559 ', 'First Floor, 112 Gulshan Avenue, <br>Dhaka 1212, Bangladesh ', '', '2017-04-18', 1, ''),
(146, 61, 'food', 'cafe', 'Banani', 'Phone: 01713014828 <br>Email:  prabeer@officextracts.com ', 'Shop # 04, Ground floor, F. R. Tower, <br>32 Kemal Ataturk Ave, Dhaka 1213', '', '2017-04-18', 1, ''),
(147, 62, 'food', 'restaurant cake_pastry', 'Dhanmondi', 'Telephone: +88-02-8125889', 'Main branch, House-4, Road-27 (old), <br>16 (new) Dhanmondi, Dhaka-1209', '', '2017-04-18', 1, ''),
(148, 62, 'food', 'restaurant cake_pastry', 'Gulshan', 'Telephone: +88-02-9862098', '"Meher Nibesh"(ground floor), House # 12B, <br>Road # 55 Gulshan Avenue-North, Dhaka-1212.', '', '2017-04-18', 1, ''),
(149, 62, 'food', 'restaurant cake_pastry', 'Wari', 'Telephone: +88-02-9512034', '16/10 Rankin Street, Post Office Lane. <br>Wari, Dhaka-1203', '', '2017-04-18', 1, ''),
(150, 62, 'food', 'restaurant cake_pastry', 'Shahbagh', 'Telephone: 9660500', '10, Mawlana Vashani Road (flower market), <br>Shabagh, Dhaka- 1000', '', '2017-04-18', 1, ''),
(151, 62, 'food', 'restaurant cake_pastry', 'Dhanmondi', 'Telephone: +88-02-9135719', 'Shop # 05,House # 01,Road # 10,<br>Alta plaza. Kalabagan, Dhaka-1205.', '', '2017-04-18', 1, ''),
(152, 62, 'food', 'restaurant cake_pastry', 'Baily Road', 'Telephone: +88-02-9355195', 'Joinal Abedin Super Market 14,<br>New Baily Road,Dhaka-1217.', '', '2017-04-18', 1, ''),
(153, 62, 'food', 'restaurant cake_pastry', 'Dhanmondi', 'Telephone: 9139108', 'House # 87, Road # 12/A, (Abahoni field) <br>Dhanmondi R/A, Dhaka- 1209', '', '2017-04-18', 1, ''),
(154, 62, 'food', 'restaurant cake_pastry', 'Mirpur', 'Telephone: +88-02-9027001', '2/6, Alba Tower, Pallabi Bus Stand,  <br>Mirpur 11, Dhaka-1216', '', '2017-04-18', 1, ''),
(155, 62, 'food', 'restaurant cake_pastry', 'Uttara', 'Telephone: +88-02-58950296', 'Sector-4, Road no-4, House no-4. <br>Uttara, Dhaka-1230.', '', '2017-04-18', 1, ''),
(156, 62, 'food', 'restaurant cake_pastry', 'Uttara', 'Telephone: +88-02-8950646', 'House # 4,Garib-E-Newaaz Avenew. <br>Sector # 13, Uttara,Dhaka-1230.', '', '2017-04-18', 1, ''),
(157, 62, 'food', 'restaurant cake_pastry', '', '', '', '', '2017-04-18', 1, ''),
(158, 63, 'food', 'cake_pastry', 'Sobhanbagh', 'Tele: 044-78685841, 02-8121138', '8 J & J Mansion, Sobhanbagh, Dhaka', '', '2017-04-20', 1, ''),
(159, 63, 'food', 'cake_pastry', 'Panthapath', 'Tele: 044-78685840, 02-8142150', '(Bakerâ€™s Basket) Panthapath, Dhaka', '', '2017-04-20', 1, ''),
(160, 63, 'food', 'cake_pastry', 'Banani', 'Tele: 044-78685832, 02-55042123', 'House- 50, Rd- 11, Block- F, Banani, Dhaka', '', '2017-04-20', 1, ''),
(161, 63, 'food', 'cake_pastry', 'Uttara', 'Tele: 044-78685831, 02-58950919', '1, Shaista Khan Avenue, Sector-4, Uttara, Dhaka', '', '2017-04-20', 1, ''),
(162, 63, 'food', 'cake_pastry', 'Baily Road', 'Tele: 044-78685837, 02-58315876', '1, Baily Road (Natok Sharani), Dhaka', '', '2017-04-20', 1, ''),
(163, 63, 'food', 'cake_pastry', 'Mirpur', 'Tele: 044-78685847, 02-9026710', 'Plot-30, Rd-6, Rupnagar R/A, Shealbari, Mirpur-2, Pallabi, Dhaka', '', '2017-04-20', 1, ''),
(164, 63, 'food', 'cake_pastry', 'Mohakhali', 'Tele: 044-78685833, 02-9882435', 'Rupayan Center, 72, Mohakhali C/A, Dhaka', '', '2017-04-20', 1, ''),
(165, 63, 'food', 'cake_pastry', 'Hatirpul', 'Tele: 044-78685839, 02-9614590', '16, Bir Uttam C.R. Dutta Road, Hatirpul Bazar, Dhaka', '', '2017-04-20', 1, ''),
(166, 63, 'food', 'cake_pastry', 'Wari', 'Tele: 044-78685838, 02-9512033', '1/2, Chandi Charan Bose Street, Wari, Dhaka', '', '2017-04-20', 1, ''),
(167, 63, 'food', 'cake_pastry', 'Khilgaon', 'Tele: 044-78685836, 02-7219520', '401/A, Khilgaon, Dhaka', '', '2017-04-20', 1, ''),
(168, 63, 'food', 'cake_pastry', 'Rampura', 'Tele: 044-78685835, 02-8322310', '4, Malibagh Chowdhury Para (Hajipara), Rampura, Dhaka', '', '2017-04-20', 1, ''),
(169, 63, 'food', 'cake_pastry', 'Badda', 'Tele: 044-78685834, 02-8411479', 'Bashundhara Rd, Badda, Dhaka', '', '2017-04-20', 1, ''),
(170, 63, 'food', 'cake_pastry', 'Mirpur', 'Tele: 044-78685850, 02-8050635', 'Spring Rahmat-e-Tuba Complex, Plot-132, Rd-2, Sec-12, Mirpur, Dhaka (Beside Pizza Hut)', '', '2017-04-20', 1, ''),
(171, 63, 'food', 'cake_pastry', 'Uttara', 'Tele: 044-78685830', 'House-6, Ishakha Avenue, Sector-6, Uttara', '', '2017-04-20', 1, ''),
(172, 63, 'food', 'cake_pastry', 'Mohammadpur', 'Tele: 044-78685844', 'House-19, Road-02, Mohammadi Housing Society, Mohammadpur', '', '2017-04-20', 1, ''),
(173, 63, 'food', 'cake_pastry', 'Dhanmondi', 'Tele: 044-78685842', 'Shop-104, Holding-21/1, Hoque Mansion, Jigatola, Dhaka', '', '2017-04-20', 1, ''),
(174, 63, 'food', 'cake_pastry', 'Mohammadpur', 'Tele: 044-78685843', 'Plot-2/1, Road-3, Block-A, Lalmatia, Mohammadpur, Dhaka', '', '2017-04-20', 1, ''),
(175, 63, 'food', 'cake_pastry', 'Rampura', 'Tele: 044-78685846', 'House-2, Road-6, Block-D, Banasree, Rampura (beside global ideal school & college), Dhaka-1219', '', '2017-04-20', 1, ''),
(176, 63, 'food', 'cake_pastry', 'Gulshan', 'Tele: 044-78685845', 'House-121/D, Road-44, Gulshan-2 (beside Gulshan thana), Dhaka-1212', '', '2017-04-20', 1, ''),
(177, 63, 'food', 'cake_pastry', 'Khilkhet', 'Tele: 044-78685859', 'House-1/B, Rd-10, Farook Sharani, Nikunja-2, Khilkhet, Dhaka', '', '2017-04-20', 1, ''),
(178, 63, 'food', 'cake_pastry', 'Dhaka Cantonment', 'Tele: 044-78685860', 'Shop-101, Kunja-02, 527 Manikdi, Dhaka Cantonment', '', '2017-04-20', 1, ''),
(179, 63, 'food', 'cake_pastry', 'Tejgaon', 'Tele: 044-78685852', 'Shop-78, Tejkunipara, Tejgaon, Dhaka', '', '2017-04-20', 1, ''),
(180, 63, 'food', 'cake_pastry', 'Kafrul', 'Tele: 044-78685857', '284 Khan Mansion, Shop-02, Pullpar, Ibrahimpur, Kafrul, Dhaka-1206', '', '2017-04-20', 1, ''),
(181, 63, 'food', 'cake_pastry', 'Utara', 'Tele: 01911283209', 'House-21, Sector-7, Sonargaon Janapath Road, Uttara, Dhaka', '', '2017-04-20', 1, ''),
(182, 63, 'food', 'cake_pastry', '', '', '', '', '2017-04-20', 1, ''),
(183, 63, 'food', 'cake_pastry', 'Uttara', 'Tele: 01778472422', 'House-03, Road-14, Sector-01, Uttara (opposite R.A.K. Tower)', '', '2017-04-20', 1, ''),
(184, 64, 'food', 'cake_pastry', 'Gulshan', 'Phone: 01969200200  <br>Email: info@holeybread.com', 'Road# 79, House# 5, Gulshan 2, Next to Lake View Clinic, Dhaka', '', '2017-04-20', 1, ''),
(185, 65, 'food', 'cake_pastry', 'Mohammadpur', 'Phone: 01680172178', 'House# 52, Road# 3, Mohammadi Housing Society, Mohammadpur, 1207 Dhaka', '', '2017-04-20', 1, ''),
(186, 66, 'food', 'cake_pastry', 'Banani', 'Phone: 02-8836818  <br>Email:  kkdbd.info@gmail.com ', 'Block: E, Road: 11, House No: 119, 1213 Banani Town, Dhaka, Bangladesh', '', '2017-04-20', 1, ''),
(187, 66, 'food', 'cake_pastry', '', '', '', '', '2017-04-20', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `id` int(255) NOT NULL,
  `main_info_id` int(50) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `user_id` varchar(60) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `question` varchar(300) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE IF NOT EXISTS `reply` (
  `id` int(255) NOT NULL,
  `comment_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `reply` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search_suggestion`
--

CREATE TABLE IF NOT EXISTS `search_suggestion` (
  `id` int(255) NOT NULL,
  `text_s` varchar(500) NOT NULL,
  `s_text_type` varchar(50) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `cata_item_id` int(120) NOT NULL,
  `count` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search_suggestion`
--

INSERT INTO `search_suggestion` (`id`, `text_s`, `s_text_type`, `cata`, `sub_cata`, `cata_item_id`, `count`) VALUES
(1, 'Bisnakandi', 'business_profile', 'tourism', 'natural_site', 1, 0),
(2, 'Lalbagh Fort (Lalbagh Kella)', 'business_profile', 'tourism', 'heritage', 2, 0),
(3, 'Mohammadi  Garden', 'business_profile', 'tourism', 'picnic_spot', 3, 0),
(4, 'Dhanmondi Lake', 'business_profile', 'tourism', 'lake_park', 4, 0),
(5, 'Nokkhottrobari Resort & Conference Center', 'business_profile', 'residence', 'resort', 5, 0),
(6, 'Parata', 'item', 'food', 'restaurant', 1, 0),
(7, 'Normal Nan', 'item', 'food', 'restaurant', 2, 0),
(8, 'Shad Tehari Ghar', 'business_profile', 'food', 'restaurant', 6, 0),
(9, 'Padma Resort', 'business_profile', 'residence', 'hotel', 7, 0),
(10, 'DELUXE ROOMS', 'item', 'residence', 'hotel', 3, 0),
(11, 'Business Center', 'item', 'residence', 'hotel', 4, 0),
(12, 'The Westin Dhaka', 'business_profile', 'residence', 'hotel', 8, 0),
(13, 'Hakka Dhaka	', 'business_profile', 'food', 'restaurant', 9, 0),
(14, 'adasd', 'business_profile', 'food', 'restaurant', 10, 0),
(15, 'Nasi Goreng Set', 'item', 'food', 'restaurant', 5, 0),
(16, 'The Mughal Kitchen', 'business_profile', 'food', 'restaurant', 11, 0),
(17, 'Phat Khai Mao Set', 'item', 'food', 'restaurant', 6, 0),
(18, 'Khichuri Set ', 'item', 'food', 'restaurant', 7, 0),
(19, 'Oriental Set', 'item', 'food', 'restaurant', 8, 0),
(20, 'Hyderabadi Set', 'item', 'food', 'restaurant', 9, 0),
(21, 'Tandoori Set', 'item', 'food', 'restaurant', 10, 0),
(22, 'Mughal Mixed Kebab', 'item', 'food', 'restaurant', 11, 0),
(23, 'Yellow', 'business_profile', 'life style', 'fashion', 12, 0),
(24, 'Diamond World Ltd.', 'business_profile', 'life style', 'accessorice', 13, 0),
(25, 'Farzana Shakilâ€™s Makeover Salon Ltd.', 'business_profile', 'life style', 'parlor_salon', 14, 0),
(26, 'Adonize Fitness Center LTD', 'business_profile', 'life style', 'gym_fitness', 15, 0),
(27, 'Bay Emporium', 'business_profile', 'life style', 'accessorice', 16, 0),
(28, 'Cappuccino/ Cafe Latte', 'item', 'food', 'cafe', 12, 0),
(29, 'Creme Brulee', 'item', 'food', 'cafe', 13, 0),
(30, 'Gloria Jeanâ€™s', 'business_profile', 'food', 'cafe', 17, 0),
(31, 'North End Coffee Roasters', 'business_profile', 'food', 'cafe', 18, 0),
(32, 'Taco', 'item', 'food', 'restaurant cart', 14, 0),
(33, 'Chicken Subway Sandwich', 'item', 'food', 'restaurant cart', 15, 0),
(34, 'Fajitas.bd', 'business_profile', 'food', 'restaurant cart', 19, 0),
(35, 'Gulab Jamun', 'item', 'food', 'sweets', 16, 0),
(36, 'Doi', 'item', 'food', 'sweets', 17, 0),
(37, 'Khazana Mithai', 'business_profile', 'food', 'sweets', 20, 0),
(38, 'Original natural glazed', 'item', 'food', 'cake_pastry', 18, 0),
(39, 'Plain Lava Cake', 'item', 'food', 'cake_pastry', 19, 0),
(40, 'My Donuts', 'business_profile', 'food', 'cake_pastry', 21, 0),
(41, 'Test''s (heritage)', 'business_profile', 'tourism', 'heritage', 22, 0),
(42, 'Chicken Cheese Burger', 'item', 'food', 'cart', 20, 0),
(43, 'Beef Cheese Burger', 'item', 'food', 'cart', 21, 0),
(44, 'Khanaâ€™s', 'business_profile', 'food', 'cart', 23, 0),
(45, '', 'item', 'food', 'cart', 22, 0),
(46, 'TAKEOUT', 'business_profile', 'food', 'cart', 24, 0),
(47, 'B.B.Q. Wings', 'item', 'food', 'cart', 23, 0),
(48, 'Chefâ€™s Special Chicken Delicious Burger', 'item', 'food', 'cart', 24, 0),
(49, 'Panaâ€™s Panini', 'business_profile', 'food', 'cart', 25, 0),
(50, 'Wifi special burger (beef/chicken)', 'item', 'food', 'cart', 25, 0),
(51, 'Wifi Street Food', 'business_profile', 'food', 'cart', 26, 0),
(52, 'Rosh Gulla', 'item', 'food', 'sweets', 26, 0),
(53, 'Kala Jamun', 'item', 'food', 'sweets', 27, 0),
(54, 'Premium Sweets', 'business_profile', 'food', 'sweets', 27, 0),
(55, 'Glazed', 'item', 'food', 'cake_pastry', 28, 0),
(56, 'Winter Glazed', 'item', 'food', 'cake_pastry', 29, 0),
(57, 'Glazed', 'business_profile', 'food', 'cake_pastry', 28, 0),
(58, '', 'item', 'food', 'cake_pastry', 30, 0),
(59, 'Bread & Beyond', 'business_profile', 'food', 'cake_pastry', 29, 0),
(60, '', 'item', 'food', 'cafe', 31, 0),
(61, 'Apon Coffee House', 'business_profile', 'food', 'cafe', 30, 0),
(62, 'Barista Lavazza Bangladesh', 'business_profile', 'food', 'cafe', 31, 0),
(63, 'Espresso', 'item', 'food', 'cafe', 32, 0),
(64, 'Cappuccino', 'item', 'food', 'cafe', 33, 0),
(65, 'TABAQ COFFEE', 'business_profile', 'food', 'cafe', 32, 0),
(66, 'Mocha Chip Frappe', 'item', 'food', 'cafe', 34, 0),
(67, 'Mango Smoothie (Seasonal)', 'item', 'food', 'cafe', 35, 0),
(68, 'Coffee World', 'business_profile', 'food', 'cafe', 33, 0),
(69, '', 'item', 'food', 'cafe', 36, 0),
(70, 'Butlers Chocolate CafÃ© - Bangladesh', 'business_profile', 'food', 'cafe', 34, 0),
(71, 'Beans & Aroma Coffees', 'business_profile', 'food', 'cafe', 35, 0),
(72, 'Crimson Cup BD', 'business_profile', 'food', 'cafe', 36, 0),
(73, 'Fruit Smoothie', 'item', 'food', 'cafe', 37, 0),
(74, 'George''s Cafe', 'business_profile', 'food', 'cafe', 37, 0),
(75, 'Dumpling', 'item', 'food', 'restaurant', 38, 0),
(76, 'Prawn Cake', 'item', 'food', 'restaurant', 39, 0),
(77, 'Fresh Vietnamies', 'item', 'food', 'restaurant', 40, 0),
(78, 'Thai Emerald', 'business_profile', 'food', 'restaurant', 38, 0),
(79, 'Beef Afgani Curry', 'item', 'food', 'restaurant', 41, 0),
(80, 'Chicken Boti', 'item', 'food', 'restaurant', 42, 0),
(81, 'Kabab Factory', 'business_profile', 'food', 'restaurant', 39, 0),
(82, 'check image', 'item', 'tourism', 'heritage', 43, 0),
(83, 'Nawab â€“ E â€“ Kacchi Biriyani', 'item', 'food', 'restaurant', 44, 0),
(84, '', 'item', 'food', 'restaurant', 45, 0),
(85, 'Grand Nawab', 'business_profile', 'food', 'restaurant', 40, 0),
(86, 'Set Menu', 'item', 'food', 'restaurant', 46, 0),
(87, 'Thai Soup Thick', 'item', 'food', 'restaurant', 47, 0),
(88, 'THE DARK, Music Cafe & Restaurant', 'business_profile', 'food', 'restaurant', 41, 0),
(89, 'Pizza Bhoot Special ', 'item', 'food', 'restaurant', 48, 0),
(90, 'Chicken Shawarma', 'item', 'food', 'restaurant', 49, 0),
(91, 'Bhooter Bari', 'business_profile', 'food', 'restaurant', 42, 0),
(92, 'Cheese Pizza ', 'item', 'food', 'restaurant', 50, 0),
(93, 'Meat Delight Pizza ', 'item', 'food', 'restaurant', 51, 0),
(94, 'Vegge Supreme Pizza ', 'item', 'food', 'restaurant', 52, 0),
(95, 'SBARRO ', 'business_profile', 'food', 'restaurant', 43, 0),
(96, 'Warm Chicken Salad', 'item', 'food', 'restaurant', 53, 0),
(97, 'Napolitana', 'item', 'food', 'restaurant', 54, 0),
(98, 'Tiramisu', 'item', 'food', 'restaurant', 55, 0),
(99, 'Veni Vidi Vici', 'business_profile', 'food', 'restaurant', 44, 0),
(100, 'Corn-On-the-Cob', 'item', 'food', 'restaurant', 56, 0),
(101, 'Fries', 'item', 'food', 'restaurant', 57, 0),
(102, 'Quarter Chicken Meal', 'item', 'food', 'restaurant', 58, 0),
(103, 'Nandoâ€™s', 'business_profile', 'food', 'restaurant', 45, 0),
(104, 'Blue Mussels ', 'item', 'food', 'restaurant', 59, 0),
(105, 'Salmon & White Fish', 'item', 'food', 'restaurant', 60, 0),
(106, 'Grilled Peri-Peri Prawns', 'item', 'food', 'restaurant', 61, 0),
(107, 'Fish & Co.', 'business_profile', 'food', 'restaurant', 46, 0),
(108, 'Set Menu', 'item', 'food', 'restaurant', 62, 0),
(109, 'Set Menu', 'item', 'food', 'restaurant', 63, 0),
(110, 'Set Menu', 'item', 'food', 'restaurant', 64, 0),
(111, 'Rice & Noodle', 'business_profile', 'food', 'restaurant', 47, 0),
(112, 'Cashew nut Salad', 'item', 'food', 'restaurant', 65, 0),
(113, 'Â¼ BBQ Chicken + 2 sides', 'item', 'food', 'restaurant', 66, 0),
(114, 'Pomfret Fish Fry + 1 side dish', 'item', 'food', 'restaurant', 67, 0),
(115, 'Street BBQ', 'business_profile', 'food', 'restaurant', 48, 0),
(116, 'Blend Burger', 'item', 'food', 'restaurant', 68, 0),
(117, 'Big Blend Burger ', 'item', 'food', 'restaurant', 69, 0),
(118, 'American Philly Cheese Steak', 'item', 'food', 'restaurant', 70, 0),
(119, '', 'item', 'food', 'restaurant', 71, 0),
(120, 'American Blend- Burger & Beyond', 'business_profile', 'food', 'restaurant', 49, 0),
(121, 'Buffalo Wings (8pcs)', 'item', 'food', 'restaurant', 72, 0),
(122, 'New England Mussel-Clam Chowder ', 'item', 'food', 'restaurant', 73, 0),
(123, 'Oregano', 'business_profile', 'food', 'restaurant', 50, 0),
(124, 'Comic Cafe', 'business_profile', 'food', 'restaurant', 51, 0),
(125, 'Mad chef', 'business_profile', 'food', 'restaurant', 52, 0),
(126, 'Food Republic', 'business_profile', 'food', 'restaurant', 53, 0),
(127, '', 'item', 'food', 'restaurant', 74, 0),
(128, 'Peda Ting Ting', 'business_profile', 'food', 'restaurant', 54, 0),
(129, 'KIVA HAN', 'business_profile', 'food', 'restaurant cafe', 55, 0),
(130, 'Regular Hot Coffee', 'item', 'food', 'cafe', 75, 0),
(131, 'Hot Chocolate Coffee', 'item', 'food', 'cafe', 76, 0),
(132, 'Mango Crushers', 'item', 'food', 'cafe', 77, 0),
(133, 'Strawberry Crushers', 'item', 'food', 'cafe', 78, 0),
(134, 'Chocolate Blast', 'item', 'food', 'cafe', 79, 0),
(135, 'Chocolate Crushers', 'item', 'food', 'cafe', 80, 0),
(136, 'Coffee Time', 'business_profile', 'food', 'cafe', 56, 0),
(137, 'Popeyes Coffee & Fast Food', 'business_profile', 'food', 'cafe', 57, 0),
(138, 'Bittersweet Cafe', 'business_profile', 'food', 'cafe cake_pastry', 58, 0),
(139, '', 'item', 'food', 'cafe cake_pastry', 81, 0),
(140, 'The Coffee Bean & Tea Leaf, Bangladesh', 'business_profile', 'food', 'cafe cake_pastry', 59, 0),
(141, 'Espresso illy ', 'item', 'food', 'restaurant cafe', 82, 0),
(142, 'Affogato', 'item', 'food', 'restaurant cafe', 83, 0),
(143, 'Grilled Chicken Sandwich', 'item', 'food', 'restaurant cafe', 84, 0),
(144, 'Brews & Bites', 'business_profile', 'food', 'restaurant cafe', 60, 0),
(145, 'PappaRoti Bun', 'item', 'food', 'cafe', 85, 0),
(146, 'Flavoured Shake', 'item', 'food', 'cafe', 86, 0),
(147, 'Cafe Latte', 'item', 'food', 'cafe', 87, 0),
(148, 'Cappuccino', 'item', 'food', 'cafe', 88, 0),
(149, 'PappaRoti', 'business_profile', 'food', 'cafe', 61, 0),
(150, 'Dizzy Dog', 'item', 'food', 'restaurant cake_pastry', 89, 0),
(151, 'Beef Puff', 'item', 'food', 'restaurant cake_pastry', 90, 0),
(152, 'Angry Burger', 'item', 'food', 'restaurant cake_pastry', 91, 0),
(153, 'American Choco Cake (L)', 'item', 'food', 'restaurant cake_pastry', 92, 0),
(154, 'California Fried Chicken and Pastry Shop', 'business_profile', 'food', 'restaurant cake_pastry', 62, 0),
(155, 'Mini Cakes', 'item', 'food', 'cake_pastry', 93, 0),
(156, 'Fondant Cake 1kg', 'item', 'food', 'cake_pastry', 94, 0),
(157, 'Shumiâ€™s Hot Cake', 'business_profile', 'food', 'cake_pastry', 63, 0),
(158, 'House White', 'item', 'food', 'cake_pastry', 95, 0),
(159, 'Holey Artisan Bakery', 'business_profile', 'food', 'cake_pastry', 64, 0),
(160, 'Vanilla frosting with whipped cream + Fondant decoration', 'item', 'food', 'cake_pastry', 96, 0),
(161, 'Vanilla frosting with butter cream + Fondant decoration', 'item', 'food', 'cake_pastry', 97, 0),
(162, 'Cake World', 'business_profile', 'food', 'cake_pastry', 65, 0),
(163, 'Krispy Red Velvet', 'item', 'food', 'cake_pastry', 98, 0),
(164, 'Powdered Blueberry Filled', 'item', 'food', 'cake_pastry', 99, 0),
(165, 'Krispy Kreme Bangladesh', 'business_profile', 'food', 'cake_pastry', 66, 0);

-- --------------------------------------------------------

--
-- Table structure for table `set_view`
--

CREATE TABLE IF NOT EXISTS `set_view` (
  `setDailyView` varchar(200) NOT NULL,
  `setWeeklyView` varchar(200) NOT NULL,
  `setMonthlyView` varchar(200) NOT NULL,
  `setYearlyView` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE IF NOT EXISTS `social_link` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `link_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tourism_nearby`
--

CREATE TABLE IF NOT EXISTS `tourism_nearby` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `hotel_id` varchar(100) NOT NULL,
  `restaurant_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tourism_nearby`
--

INSERT INTO `tourism_nearby` (`id`, `cata_id`, `hotel_id`, `restaurant_id`) VALUES
(1, 1, '', ''),
(2, 1, '', ''),
(3, 1, '', ''),
(4, 1, '', ''),
(5, 1, '', ''),
(6, 2, '', ''),
(7, 2, '', ''),
(8, 2, '', ''),
(9, 2, '', ''),
(10, 2, '', ''),
(11, 2, '', ''),
(12, 2, '', ''),
(13, 2, '', ''),
(14, 2, '', ''),
(15, 2, '', ''),
(16, 3, '', ''),
(17, 3, '', ''),
(18, 3, '', ''),
(19, 3, '', ''),
(20, 3, '', ''),
(21, 4, '', ''),
(22, 4, '', ''),
(23, 4, '', ''),
(24, 4, '', ''),
(25, 4, '', ''),
(26, 5, '', ''),
(27, 5, '', ''),
(28, 5, '', ''),
(29, 5, '', ''),
(30, 5, '', ''),
(31, 6, '', ''),
(32, 6, '', ''),
(33, 6, '', ''),
(34, 6, '', ''),
(35, 6, '', ''),
(36, 7, '', ''),
(37, 7, '', ''),
(38, 7, '', ''),
(39, 7, '', ''),
(40, 7, '', ''),
(41, 8, '', ''),
(42, 8, '', ''),
(43, 8, '', ''),
(44, 8, '', ''),
(45, 8, '', ''),
(46, 9, '', ''),
(47, 9, '', ''),
(48, 9, '', ''),
(49, 9, '', ''),
(50, 9, '', ''),
(51, 10, '', ''),
(52, 10, '', ''),
(53, 10, '', ''),
(54, 10, '', ''),
(55, 10, '', ''),
(56, 11, '', ''),
(57, 11, '', ''),
(58, 11, '', ''),
(59, 11, '', ''),
(60, 11, '', ''),
(61, 12, '', ''),
(62, 12, '', ''),
(63, 12, '', ''),
(64, 12, '', ''),
(65, 12, '', ''),
(66, 13, '', ''),
(67, 13, '', ''),
(68, 13, '', ''),
(69, 13, '', ''),
(70, 13, '', ''),
(71, 14, '', ''),
(72, 14, '', ''),
(73, 14, '', ''),
(74, 14, '', ''),
(75, 14, '', ''),
(76, 15, '', ''),
(77, 15, '', ''),
(78, 15, '', ''),
(79, 15, '', ''),
(80, 15, '', ''),
(81, 16, '', ''),
(82, 16, '', ''),
(83, 16, '', ''),
(84, 16, '', ''),
(85, 16, '', ''),
(86, 17, '', ''),
(87, 17, '', ''),
(88, 17, '', ''),
(89, 17, '', ''),
(90, 17, '', ''),
(91, 18, '', ''),
(92, 18, '', ''),
(93, 18, '', ''),
(94, 18, '', ''),
(95, 18, '', ''),
(96, 19, '', ''),
(97, 19, '', ''),
(98, 19, '', ''),
(99, 19, '', ''),
(100, 19, '', ''),
(101, 20, '', ''),
(102, 20, '', ''),
(103, 20, '', ''),
(104, 20, '', ''),
(105, 20, '', ''),
(106, 21, '', ''),
(107, 21, '', ''),
(108, 21, '', ''),
(109, 21, '', ''),
(110, 21, '', ''),
(111, 22, '', ''),
(112, 22, '', ''),
(113, 22, '', ''),
(114, 22, '', ''),
(115, 22, '', ''),
(116, 23, '', ''),
(117, 23, '', ''),
(118, 23, '', ''),
(119, 23, '', ''),
(120, 23, '', ''),
(121, 24, '', ''),
(122, 24, '', ''),
(123, 24, '', ''),
(124, 24, '', ''),
(125, 24, '', ''),
(126, 25, '', ''),
(127, 25, '', ''),
(128, 25, '', ''),
(129, 25, '', ''),
(130, 25, '', ''),
(131, 26, '', ''),
(132, 26, '', ''),
(133, 26, '', ''),
(134, 26, '', ''),
(135, 26, '', ''),
(136, 27, '', ''),
(137, 27, '', ''),
(138, 27, '', ''),
(139, 27, '', ''),
(140, 27, '', ''),
(141, 28, '', ''),
(142, 28, '', ''),
(143, 28, '', ''),
(144, 28, '', ''),
(145, 28, '', ''),
(146, 29, '', ''),
(147, 29, '', ''),
(148, 29, '', ''),
(149, 29, '', ''),
(150, 29, '', ''),
(151, 30, '', ''),
(152, 30, '', ''),
(153, 30, '', ''),
(154, 30, '', ''),
(155, 30, '', ''),
(156, 31, '', ''),
(157, 31, '', ''),
(158, 31, '', ''),
(159, 31, '', ''),
(160, 31, '', ''),
(161, 32, '', ''),
(162, 32, '', ''),
(163, 32, '', ''),
(164, 32, '', ''),
(165, 32, '', ''),
(166, 33, '', ''),
(167, 33, '', ''),
(168, 33, '', ''),
(169, 33, '', ''),
(170, 33, '', ''),
(171, 34, '', ''),
(172, 34, '', ''),
(173, 34, '', ''),
(174, 34, '', ''),
(175, 34, '', ''),
(176, 35, '', ''),
(177, 35, '', ''),
(178, 35, '', ''),
(179, 35, '', ''),
(180, 35, '', ''),
(181, 36, '', ''),
(182, 36, '', ''),
(183, 36, '', ''),
(184, 36, '', ''),
(185, 36, '', ''),
(186, 37, '', ''),
(187, 37, '', ''),
(188, 37, '', ''),
(189, 37, '', ''),
(190, 37, '', ''),
(191, 38, '', ''),
(192, 38, '', ''),
(193, 38, '', ''),
(194, 38, '', ''),
(195, 38, '', ''),
(196, 39, '', ''),
(197, 39, '', ''),
(198, 39, '', ''),
(199, 39, '', ''),
(200, 39, '', ''),
(201, 22, '', ''),
(202, 22, '', ''),
(203, 22, '', ''),
(204, 22, '', ''),
(205, 22, '', ''),
(206, 40, '', ''),
(207, 40, '', ''),
(208, 40, '', ''),
(209, 40, '', ''),
(210, 40, '', ''),
(211, 41, '', ''),
(212, 41, '', ''),
(213, 41, '', ''),
(214, 41, '', ''),
(215, 41, '', ''),
(216, 42, '', ''),
(217, 42, '', ''),
(218, 42, '', ''),
(219, 42, '', ''),
(220, 42, '', ''),
(221, 43, '', ''),
(222, 43, '', ''),
(223, 43, '', ''),
(224, 43, '', ''),
(225, 43, '', ''),
(226, 44, '', ''),
(227, 44, '', ''),
(228, 44, '', ''),
(229, 44, '', ''),
(230, 44, '', ''),
(231, 45, '', ''),
(232, 45, '', ''),
(233, 45, '', ''),
(234, 45, '', ''),
(235, 45, '', ''),
(236, 46, '', ''),
(237, 46, '', ''),
(238, 46, '', ''),
(239, 46, '', ''),
(240, 46, '', ''),
(241, 47, '', ''),
(242, 47, '', ''),
(243, 47, '', ''),
(244, 47, '', ''),
(245, 47, '', ''),
(246, 48, '', ''),
(247, 48, '', ''),
(248, 48, '', ''),
(249, 48, '', ''),
(250, 48, '', ''),
(251, 49, '', ''),
(252, 49, '', ''),
(253, 49, '', ''),
(254, 49, '', ''),
(255, 49, '', ''),
(256, 50, '', ''),
(257, 50, '', ''),
(258, 50, '', ''),
(259, 50, '', ''),
(260, 50, '', ''),
(261, 51, '', ''),
(262, 51, '', ''),
(263, 51, '', ''),
(264, 51, '', ''),
(265, 51, '', ''),
(266, 52, '', ''),
(267, 52, '', ''),
(268, 52, '', ''),
(269, 52, '', ''),
(270, 52, '', ''),
(271, 53, '', ''),
(272, 53, '', ''),
(273, 53, '', ''),
(274, 53, '', ''),
(275, 53, '', ''),
(276, 54, '', ''),
(277, 54, '', ''),
(278, 54, '', ''),
(279, 54, '', ''),
(280, 54, '', ''),
(281, 55, '', ''),
(282, 55, '', ''),
(283, 55, '', ''),
(284, 55, '', ''),
(285, 55, '', ''),
(286, 56, '', ''),
(287, 56, '', ''),
(288, 56, '', ''),
(289, 56, '', ''),
(290, 56, '', ''),
(291, 57, '', ''),
(292, 57, '', ''),
(293, 57, '', ''),
(294, 57, '', ''),
(295, 57, '', ''),
(296, 58, '', ''),
(297, 58, '', ''),
(298, 58, '', ''),
(299, 58, '', ''),
(300, 58, '', ''),
(301, 59, '', ''),
(302, 59, '', ''),
(303, 59, '', ''),
(304, 59, '', ''),
(305, 59, '', ''),
(306, 60, '', ''),
(307, 60, '', ''),
(308, 60, '', ''),
(309, 60, '', ''),
(310, 60, '', ''),
(311, 61, '', ''),
(312, 61, '', ''),
(313, 61, '', ''),
(314, 61, '', ''),
(315, 61, '', ''),
(316, 62, '', ''),
(317, 62, '', ''),
(318, 62, '', ''),
(319, 62, '', ''),
(320, 62, '', ''),
(321, 63, '', ''),
(322, 63, '', ''),
(323, 63, '', ''),
(324, 63, '', ''),
(325, 63, '', ''),
(326, 64, '', ''),
(327, 64, '', ''),
(328, 64, '', ''),
(329, 64, '', ''),
(330, 64, '', ''),
(331, 65, '', ''),
(332, 65, '', ''),
(333, 65, '', ''),
(334, 65, '', ''),
(335, 65, '', ''),
(336, 66, '', ''),
(337, 66, '', ''),
(338, 66, '', ''),
(339, 66, '', ''),
(340, 66, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_upload_image`
--

CREATE TABLE IF NOT EXISTS `user_upload_image` (
  `id` int(255) NOT NULL,
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elec_item_details`
--
ALTER TABLE `elec_item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_image`
--
ALTER TABLE `general_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_info`
--
ALTER TABLE `general_info`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `item_des`
--
ALTER TABLE `item_des`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_list`
--
ALTER TABLE `item_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_multi_image`
--
ALTER TABLE `item_multi_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map`
--
ALTER TABLE `map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search_suggestion`
--
ALTER TABLE `search_suggestion`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tourism_nearby`
--
ALTER TABLE `tourism_nearby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elec_item_details`
--
ALTER TABLE `elec_item_details`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_image`
--
ALTER TABLE `general_image`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT for table `general_info`
--
ALTER TABLE `general_info`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `item_des`
--
ALTER TABLE `item_des`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_list`
--
ALTER TABLE `item_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `item_multi_image`
--
ALTER TABLE `item_multi_image`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `search_suggestion`
--
ALTER TABLE `search_suggestion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT for table `tourism_nearby`
--
ALTER TABLE `tourism_nearby`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=341;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
