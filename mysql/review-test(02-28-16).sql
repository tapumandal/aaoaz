-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2016 at 05:54 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `review-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, 'Tapu Mandal', 'tapu', '123', '', '', '', '', '', '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(255) NOT NULL,
  `question_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `answer` varchar(600) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `text` varchar(600) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `avg` double NOT NULL,
  `time` time(6) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `cata_id`, `cata`, `sub_cata`, `item_id`, `user_id`, `user_type`, `text`, `type`, `rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`) VALUES
(1, 8, 'health', 'hospital', 0, '', 'user', 'Comment', 'comment', 0, 0, 0, 0, '04:24:54.000000', '2016-02-28');

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE `crop` (
  `left` int(50) NOT NULL,
  `top` int(50) NOT NULL,
  `width` int(50) NOT NULL,
  `height` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doctor_faculty`
--

CREATE TABLE `doctor_faculty` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `day` varchar(150) NOT NULL,
  `room` varchar(100) NOT NULL,
  `visite` varchar(50) NOT NULL,
  `depertment` varchar(50) NOT NULL,
  `extra_activity` varchar(200) NOT NULL,
  `education` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `link` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_faculty`
--

INSERT INTO `doctor_faculty` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `designation`, `day`, `room`, `visite`, `depertment`, `extra_activity`, `education`, `email`, `mobile`, `link`, `description`, `image_link`, `date`, `time`, `admin_id`) VALUES
(1, 'education', 1, 'university', 'Teacher Name', 'Designation', '', '', '', 'eee', '', 'Education', 'Email', 'Phone', '', '', '1_1_education_university.jpg', '2016-02-27', '09:12:24.000000', 1),
(2, 'education', 1, 'university', 'Teacher Name2', 'Designation2', '', '', '', 'eee', '', 'ed2', 'Email2', '017', '', '', '2_1_education_university.jpg', '2016-02-27', '09:12:24.000000', 1),
(3, 'education', 2, 'university', 'Teacher Name', 'Designation', '', '', '', 'e', '', 'Education', 'Email', 'Phone', '', '', '3_2_education_university.jpg', '2016-02-27', '09:18:17.000000', 1),
(4, 'education', 2, 'university', 'Teacher Name2', 'Designation2', '', '', '', 'e', '', 'ed2', 'Email2', '017', '', '', '4_2_education_university.jpg', '2016-02-27', '09:18:17.000000', 1),
(5, 'education', 3, 'university', 'Teacher Name', 'Designation', '', '', '', 'e', '', 'Education', 'Email', 'Phone', '', '', '5_3_education_university.jpg', '2016-02-27', '09:19:43.000000', 1),
(6, 'education', 3, 'university', 'Teacher Name2', 'Designation2', '', '', '', 'e', '', 'ed2', 'Email2', '017', '', '', '6_3_education_university.jpg', '2016-02-27', '09:19:44.000000', 1),
(7, 'education', 4, 'university', 'yhdfgf', 'gasgs', '', '', '', 'cse', '', 'dfsf', 'sdfsd', 'fsdfs', '', '', '7_4_education_university', '2016-02-27', '09:20:39.000000', 1),
(8, 'education', 4, 'university', 'sadf', 'sdf', '', '', '', 'eee', '', 'dsfsfds', 'fdsfs', 'fsf', '', '', '8_4_education_university', '2016-02-27', '09:20:39.000000', 1),
(9, 'education', 4, 'university', 'dsfsdff', 'dsfsdf', '', '', '', 'civil', '', 'sdfsfds', 'fsdf', 'sdfsfsfs', '', '', '9_4_education_university', '2016-02-27', '09:20:39.000000', 1),
(10, 'education', 5, 'university', 'yhdfgf', 'gasgs', '', '', '', 'cse', '', 'dfsf', 'sdfsd', 'fsdfs', '', '', '10_5_education_university', '2016-02-27', '12:20:55.000000', 1),
(11, 'education', 5, 'university', 'sadf', 'sdf', '', '', '', 'eee', '', 'dsfsfds', 'fdsfs', 'fsf', '', '', '11_5_education_university', '2016-02-27', '12:20:55.000000', 1),
(12, 'education', 5, 'university', 'dsfsdff', 'dsfsdf', '', '', '', 'civil', '', 'sdfsfds', 'fsdf', 'sdfsfsfs', '', '', '12_5_education_university', '2016-02-27', '12:20:55.000000', 1),
(13, 'health', 8, 'hospital', 'DOC NAME', 'DOC DESIG', '', '', '', 'anaesthetics', '', 'DOC EDU', 'DOC EMAIL', 'DOC PHONE', '', '', '13_8_health_hospital.jpg', '2016-02-28', '04:19:49.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `general_image`
--

CREATE TABLE `general_image` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `image_link` varchar(50) NOT NULL,
  `image_des` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_image`
--

INSERT INTO `general_image` (`id`, `cata_id`, `cata`, `sub_cata`, `image_link`, `image_des`, `date`, `time`, `admin_id`) VALUES
(1, 1, 'education', 'university', '1_education_1_university.jpg', '', '2016-02-27', '09:12:24.000000', 1),
(2, 1, 'education', 'university', '2_education_1_university.jpg', '', '2016-02-27', '09:12:24.000000', 1),
(3, 2, 'education', 'university', '3_education_2_university.jpg', '', '2016-02-27', '09:18:16.000000', 1),
(4, 2, 'education', 'university', '4_education_2_university.jpg', '', '2016-02-27', '09:18:16.000000', 1),
(5, 3, 'education', 'university', '5_education_3_university.jpg', '', '2016-02-27', '09:19:43.000000', 1),
(6, 3, 'education', 'university', '6_education_3_university.jpg', '', '2016-02-27', '09:19:43.000000', 1),
(7, 4, 'education', 'university', '7_education_4_university.jpg', '', '2016-02-27', '09:20:39.000000', 1),
(8, 4, 'education', 'university', '8_education_4_university.jpg', '', '2016-02-27', '09:20:39.000000', 1),
(9, 5, 'education', 'university', '9_education_5_university.jpg', '', '2016-02-27', '12:20:55.000000', 1),
(10, 5, 'education', 'university', '10_education_5_university.jpg', '', '2016-02-27', '12:20:55.000000', 1),
(11, 6, 'food', 'restaurant', '11_food_6_restaurant.jpg', '', '2016-02-27', '12:21:44.000000', 1),
(12, 6, 'food', 'restaurant', '12_food_6_restaurant.jpg', '', '2016-02-27', '12:21:44.000000', 1),
(13, 7, 'health', 'hospital', '13_health_7_hospital.jpg', '', '2016-02-28', '03:55:04.000000', 1),
(14, 7, 'health', 'hospital', '14_health_7_hospital.jpg', '', '2016-02-28', '03:55:04.000000', 1),
(15, 8, 'health', 'hospital', '15_health_8_hospital', '', '2016-02-28', '04:19:49.000000', 1),
(16, 8, 'health', 'hospital', '16_health_8_hospital.jpg', '', '2016-02-28', '04:19:49.000000', 1),
(17, 9, 'electronics', '', '17_electronics_9_.jpg', '', '2016-02-28', '06:44:35.000000', 1),
(18, 9, 'electronics', '', '18_electronics_9_.jpg', '', '2016-02-28', '06:44:35.000000', 1),
(19, 10, 'electronics', '', '19_electronics_10_.jpg', '', '2016-02-28', '09:41:49.000000', 1),
(20, 10, 'electronics', '', '20_electronics_10_.jpg', '', '2016-02-28', '09:41:49.000000', 1),
(21, 11, 'electronics', 'processor, graphics card', '21_electronics_11_processor, graphics card.jpg', '', '2016-02-28', '09:45:51.000000', 1),
(22, 11, 'electronics', 'processor, graphics card', '22_electronics_11_processor, graphics card.jpg', '', '2016-02-28', '09:45:51.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `general_info`
--

CREATE TABLE `general_info` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `column_1` varchar(300) NOT NULL,
  `column_2` varchar(300) NOT NULL,
  `column_3` varchar(300) NOT NULL,
  `column_4` varchar(300) NOT NULL,
  `column_5` varchar(300) NOT NULL,
  `column_6` varchar(300) NOT NULL,
  `column_7` varchar(300) NOT NULL,
  `column_8` varchar(300) NOT NULL,
  `column_9` varchar(300) NOT NULL,
  `column_10` varchar(300) NOT NULL,
  `column_11` varchar(300) NOT NULL,
  `column_12` varchar(300) NOT NULL,
  `column_13` varchar(300) NOT NULL,
  `column_14` varchar(300) NOT NULL,
  `column_15` varchar(300) NOT NULL,
  `column_16` varchar(500) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `social_links` varchar(500) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(800) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_info`
--

INSERT INTO `general_info` (`id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `column_16`, `branches`, `web_site`, `social_links`, `main_location`, `location`, `map`, `average_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `date`, `time`, `admin_id`) VALUES
(1, 'university', 'education', 'Name', '1_education_university.jpg', 'Des', 'Dept', 'Inform', 'Highlight', 'Admission', 'Add Req', '', 'image/education_pdf/1_notice.pdf', 'Course', 'image/education_pdf/1_course_details.pdf', 'CGPA', 'Class plane', 'image/education_pdf/1_class_plan.pdf', 'Exam', 'image/education_pdf/1_exam_schedule.pdf', 'Exam Hall', 'Expace', '', 'Web site', 'Social links', 'Main location', 'Full address', 'Map', 0, 0, 0, 0, 0, '2016-02-27', '09:12:23.000000', 1),
(2, 'university', 'education', 'Name', '2_education_university.jpg', 'Des', 'Dept', 'Inform', 'Highlight', 'Admission', 'Add Req', '', 'image/education_pdf/2_notice.pdf', 'Course', 'image/education_pdf/2_course_details.pdf', 'CGPA', 'Class plane', 'image/education_pdf/2_class_plan.pdf', 'Exam', 'image/education_pdf/2_exam_schedule.pdf', 'Exam Hall', 'Expace', '', 'Web site', 'Social links', 'Main location', 'Full address', 'Map', 0, 0, 0, 0, 0, '2016-02-27', '09:18:16.000000', 1),
(3, 'university', 'education', 'Name', '3_education_university.jpg', 'Des', 'Dept', 'Inform', 'Highlight', 'Admission', 'Add Req', '', 'image/education_pdf/3_notice.pdf', 'Course', 'image/education_pdf/3_course_details.pdf', 'CGPA', 'Class plane', 'image/education_pdf/3_class_plan.pdf', 'Exam', 'image/education_pdf/3_exam_schedule.pdf', 'Exam Hall', 'Expace', '', 'Web site', 'Social links', 'Main location', 'Full address', 'Map', 0, 0, 0, 0, 0, '2016-02-27', '09:19:42.000000', 1),
(4, 'university', 'education', '', '4_education_university.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-02-27', '09:20:39.000000', 1),
(5, 'university', 'education', '', '5_education_university.jpg', '\r\n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Dhanmondi', 'Dhanmondi Jigatola', 'fsdkjfdlkjfgdskjlfgsfgsdkfjgsdf', 0, 0, 0, 0, 0, '2016-02-27', '12:20:55.000000', 1),
(6, 'restaurant', 'food', 'ertgert', '6_food_restaurant.jpg', 'rwetfreg', '', 'erger', 'ger', 'gerge', '', '', '', '', '', '', '', '', '', '', '', '', '', 'rgerg', 'ergeg', '', '', '', 0, 0, 0, 0, 0, '2016-02-27', '12:21:43.000000', 1),
(7, 'hospital', 'health', 'Name', '7_health_hospital.jpg', 'description', 'Dept', 'Facilities', 'Ambulance', 'Opening', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Web', 'Soci', 'Main Location', 'Full address', 'Array', 0, 0, 0, 0, 0, '2016-02-28', '03:55:04.000000', 1),
(8, 'hospital', 'health', 'dfgsdfgsdf', '8_health_hospital', 'dsfgdfsgd', 'fg', 'gfdsg', 'dsfg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Array', 0, 0, 0, 0, 0, '2016-02-28', '04:19:49.000000', 1),
(9, '', 'electronics', 'Array', '9_electronics_.jpg', 'Array', 'Array', 'Array', 'Array', 'Array', 'Array', '', '', '', '', '', '', '', '', '', '', '', '', 'Array', 'Array', '', '', '', 0, 0, 0, 0, 0, '2016-02-28', '06:44:35.000000', 1),
(10, '', 'electronics', 'Array', '10_electronics_.jpg', 'Array', 'Array', 'Array', 'Array', 'Array', 'Array', '', '', '', '', '', '', '', '', '', '', '', '', 'Array', 'Array', '', '', '', 0, 0, 0, 0, 0, '2016-02-28', '09:41:49.000000', 1),
(11, 'processor, graphics card', 'electronics', 'Intel', '11_electronics_processor, graphics card.jpg', 'Des', 'Type', 'Ser', 'Warr', 'Ser', 'Help', '', '', '', '', '', '', '', '', '', '', '', '', 'Web', 'Social', '', '', '', 0, 0, 0, 0, 0, '2016-02-28', '09:45:51.000000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `general_infos`
--

CREATE TABLE `general_infos` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `property_type_acco` varchar(100) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `service_elec` int(11) NOT NULL,
  `warranty_elec` varchar(500) NOT NULL,
  `servicing_elec` varchar(200) NOT NULL,
  `help_line_elec` varchar(200) NOT NULL,
  `type` varchar(90) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `speciality` varchar(600) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `offer` varchar(300) NOT NULL,
  `open_hour` varchar(100) NOT NULL,
  `close_day` varchar(30) NOT NULL,
  `enrty_fee` varchar(30) NOT NULL,
  `package` varchar(200) NOT NULL,
  `travel_alert` varchar(250) NOT NULL,
  `transport` varchar(250) NOT NULL,
  `distance_chart` varchar(250) NOT NULL,
  `travel_agent` varchar(250) NOT NULL,
  `schedual` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `booking_online` varchar(250) NOT NULL,
  `area2` varchar(250) NOT NULL,
  `size` varchar(250) NOT NULL,
  `admission` varchar(250) NOT NULL,
  `requirements` varchar(250) NOT NULL,
  `facility` varchar(250) NOT NULL,
  `information` varchar(250) NOT NULL,
  `highlight` varchar(250) NOT NULL,
  `extra_facilities` varchar(250) NOT NULL,
  `ambulance` varchar(250) NOT NULL,
  `program_schedual` varchar(250) NOT NULL,
  `sponsor` varchar(250) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(400) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `image_link` varchar(70) NOT NULL,
  `image_name` varchar(60) NOT NULL,
  `image_text` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_des`
--

CREATE TABLE `item_des` (
  `id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(80) NOT NULL,
  `image_des` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_list`
--

CREATE TABLE `item_list` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `color` varchar(150) NOT NULL,
  `size` varchar(30) NOT NULL,
  `model` varchar(100) NOT NULL,
  `filter_1` varchar(40) NOT NULL,
  `filter_2` varchar(40) NOT NULL,
  `filter_3` varchar(100) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE `map` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `map` varchar(600) NOT NULL,
  `date` date NOT NULL,
  `admin_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `cata_id`, `cata`, `sub_cata`, `location`, `address`, `map`, `date`, `admin_id`) VALUES
(1, 6, 'food', 'restaurant', '11111', '2222', '333333width=100% height=250px src', '2016-02-27', 1),
(2, 6, 'food', 'restaurant', '4444', '555555', '666666width=100% height=250px src', '2016-02-27', 1),
(3, 6, 'food', 'restaurant', '777777', '888888', '9999999width=100% height=250px src', '2016-02-27', 1),
(4, 9, 'electronics', '', 'main elec', 'Full ', 'Mapwidth=100% height=250px src', '2016-02-28', 1),
(5, 9, 'electronics', '', 'Main 2 elec', 'Full 2', 'Map 2width=100% height=250px src', '2016-02-28', 1),
(6, 10, 'electronics', '', 'main elec', 'Full ', 'Mapwidth=100% height=250px src', '2016-02-28', 1),
(7, 10, 'electronics', '', 'Main 2 elec', 'Full 2', 'Map 2width=100% height=250px src', '2016-02-28', 1),
(8, 11, 'electronics', 'processor, graphics card', 'Main Elec', 'Full', 'Mapwidth=100% height=250px src', '2016-02-28', 1),
(9, 11, 'electronics', 'processor, graphics card', 'Main', 'Full', 'Mapwidth=100% height=250px src', '2016-02-28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `overview`
--

CREATE TABLE `overview` (
  `id` int(255) NOT NULL,
  `name` varchar(26) NOT NULL,
  `rating` int(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `catagory` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(255) NOT NULL,
  `main_info_id` int(50) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `user_id` varchar(60) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `question` varchar(300) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(255) NOT NULL,
  `comment_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `reply` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rivew`
--

CREATE TABLE `rivew` (
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `score` int(20) NOT NULL,
  `date` date NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `user_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE `social_link` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `link_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_upload_image`
--

CREATE TABLE `user_upload_image` (
  `id` int(255) NOT NULL,
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_image`
--
ALTER TABLE `general_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_info`
--
ALTER TABLE `general_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_infos`
--
ALTER TABLE `general_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_des`
--
ALTER TABLE `item_des`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_list`
--
ALTER TABLE `item_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map`
--
ALTER TABLE `map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overview`
--
ALTER TABLE `overview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `general_image`
--
ALTER TABLE `general_image`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `general_info`
--
ALTER TABLE `general_info`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `general_infos`
--
ALTER TABLE `general_infos`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_des`
--
ALTER TABLE `item_des`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_list`
--
ALTER TABLE `item_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `overview`
--
ALTER TABLE `overview`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
