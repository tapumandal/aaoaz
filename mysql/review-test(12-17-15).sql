-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2015 at 10:33 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `review-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `pin` varchar(10) NOT NULL,
  `user_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `pin`, `user_name`) VALUES
(1, 'Supti', 'supti@gmail.com', '123456', 'xxx', 'as1010');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, 'tapu', 'tapu', '123', '', '', 'user', '', 'enable', '2015-12-14', '03:08:15.000000');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(255) NOT NULL,
  `question_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `answer` varchar(600) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `text` varchar(600) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `avg` double NOT NULL,
  `time` time(6) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `cata_id`, `cata`, `sub_cata`, `item_id`, `user_id`, `user_type`, `text`, `type`, `rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`) VALUES
(1, 1, 'food', 'restaurant', 0, '4', 'user', 'first comment after ', 'comment', 0, 0, 0, 0, '03:50:37.000000', '2015-12-08'),
(6, 1, 'food', 'restaurant', 2, '4', 'user', 'good', 'rate', 3, 3, 3, 3, '03:57:24.000000', '2015-12-08'),
(7, 1, 'food', 'restaurant', 2, '4', 'user', 'bad', 'rate', 1, 1, 1, 1, '03:57:41.000000', '2015-12-08'),
(8, 1, 'food', 'restaurant', 2, '4', 'user', 'very bad', 'rate', 1, 1, 1, 1, '03:58:05.000000', '2015-12-08'),
(9, 1, 'food', 'restaurant', 2, '4', 'user', '', 'rate', 1, 1, 1, 1, '03:58:23.000000', '2015-12-08'),
(10, 1, 'food', 'restaurant', 2, '4', 'user', 'undefined', 'rate', 5, 5, 5, 5, '03:58:36.000000', '2015-12-08'),
(11, 1, 'food', 'restaurant', 1, '4', 'user', 'very good', 'rate', 5, 5, 5, 5, '03:58:54.000000', '2015-12-08'),
(12, 3, 'life style', 'fashion', 0, '4', 'user', 'comment', 'comment', 0, 0, 0, 0, '03:47:17.000000', '2015-12-10'),
(13, 1, 'food', 'restaurant', 3, '4', 'user', 'good', 'rate', 5, 5, 5, 5, '18:32:18.000000', '2015-12-10'),
(14, 1, 'food', 'restaurant', 0, '5', 'user', 'razib', 'comment', 0, 0, 0, 0, '18:36:34.000000', '2015-12-10'),
(15, 6, 'education', 'university', 0, '5', 'user', 'united international university ', 'comment', 0, 0, 0, 0, '02:59:59.000000', '2015-12-12'),
(16, 6, 'education', 'university', 0, '', 'user', 'I want to know the expence', 'comment', 0, 0, 0, 0, '17:08:18.000000', '2015-12-12'),
(17, 3, 'life style', 'fashion', 0, '', 'user', 'ccccc', 'comment', 0, 0, 0, 0, '17:09:51.000000', '2015-12-12'),
(18, 3, 'life style', 'fashion', 0, '4', 'user', 'adadsd', 'question', 0, 0, 0, 0, '17:10:09.000000', '2015-12-12'),
(19, 6, 'education', 'university', 0, '4', 'user', 'adasdasd', 'comment', 0, 0, 0, 0, '17:10:47.000000', '2015-12-12'),
(20, 3, 'life style', 'fashion', 0, '7', 'user', 'asdasdad', 'comment', 0, 0, 0, 0, '17:11:24.000000', '2015-12-12'),
(21, 6, 'education', 'university', 0, '8', 'user', 'dfgsad', 'comment', 0, 0, 0, 0, '17:14:19.000000', '2015-12-12'),
(22, 6, 'education', 'university', 0, '3', 'user', 'I want to know the expence', 'comment', 0, 0, 0, 0, '17:15:00.000000', '2015-12-12'),
(23, 7, 'health', 'hospital', 0, '3', 'user', 'good hospital', 'comment', 0, 0, 0, 0, '21:58:36.000000', '2015-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE `crop` (
  `left` int(50) NOT NULL,
  `top` int(50) NOT NULL,
  `width` int(50) NOT NULL,
  `height` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crop`
--

INSERT INTO `crop` (`left`, `top`, `width`, `height`) VALUES
(0, 0, 0, 0),
(147, 173, 0, 0),
(0, 0, 0, 0),
(0, 0, 0, 0),
(239, 184, 0, 0),
(239, 184, 0, 0),
(160, 155, 0, 0),
(160, 155, 0, 0),
(89, 149, 100, 100),
(73, 152, 121, 44),
(42, 18, 254, 281),
(42, 18, 254, 281),
(17, 6, 275, 302),
(286, 407, 89, 114),
(286, 407, 89, 114),
(208, 246, 100, 100),
(208, 246, 167, 298),
(65, 22, 250, 366),
(0, 0, 308, 435),
(8, 42, 330, 339),
(24, 96, 100, 100),
(22, 63, 100, 100),
(49, 115, 100, 100),
(33, 109, 100, 100),
(113, -45, 100, 100),
(83, 131, 100, 100),
(0, 81, 100, 100),
(9, 101, 100, 100),
(76, 41, 100, 100),
(24, 67, 100, 100),
(183, 192, 100, 100),
(214, -48, 100, 100),
(0, 153, 100, 100),
(137, 208, 100, 100),
(137, 208, 238, 244),
(91, 64, 238, 244),
(80, 0, 295, 421),
(219, -45, 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_faculty`
--

CREATE TABLE `doctor_faculty` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `day` varchar(150) NOT NULL,
  `room` varchar(100) NOT NULL,
  `visite` varchar(50) NOT NULL,
  `depertment` varchar(50) NOT NULL,
  `extra_activity` varchar(200) NOT NULL,
  `education` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `link` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_faculty`
--

INSERT INTO `doctor_faculty` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `designation`, `day`, `room`, `visite`, `depertment`, `extra_activity`, `education`, `email`, `mobile`, `link`, `description`, `image_link`, `date`, `time`) VALUES
(1, 'education', 6, 'university', 'Alok kumar Shaha', 'Depertment Head', '', '', '', '', '', 'Msc in cse from DU', 'alok@uap-bd.edu', '', '', '', '', '0000-00-00', '00:00:00.000000'),
(2, 'education', 6, 'university', 'Anowarul Abedin ', 'lecturar', '', '', '', 'Computer Science and engineering', '', 'Bsc in CSE from IUT', 'mithu@gmail.com', '', '', '', '', '0000-00-00', '00:00:00.000000'),
(3, 'health', 7, 'hospital', 'AAAAAA', 'Nurologist', '', '', '', 'Nuron', '', '', '', '', '', '', '', '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `general_image`
--

CREATE TABLE `general_image` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `image_des` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_image`
--

INSERT INTO `general_image` (`id`, `cata_id`, `cata`, `sub_cata`, `image_name`, `image_des`, `date`, `time`) VALUES
(1, 1, 'food', 'restaurant', '1_food_1_restaurant.jpg', 'food image 1', '0000-00-00', '00:00:00.000000'),
(2, 1, 'food', 'restaurant', '2_food_1_restaurant.jpg', 'food image 2', '0000-00-00', '00:00:00.000000'),
(3, 1, 'food', 'restaurant', '3_food_1_restaurant.jpg', 'food image 3', '0000-00-00', '00:00:00.000000'),
(4, 1, 'food', 'restaurant', '4_food_1_restaurant.jpg', 'food image 4', '0000-00-00', '00:00:00.000000'),
(5, 1, 'food', 'restaurant', '5_food_1_restaurant.jpg', 'food image 5', '0000-00-00', '00:00:00.000000'),
(6, 1, 'food', 'restaurant', '6_food_1_restaurant.jpg', 'food image 6', '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `general_info`
--

CREATE TABLE `general_info` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `column_1` varchar(300) NOT NULL,
  `column_2` varchar(300) NOT NULL,
  `column_3` varchar(300) NOT NULL,
  `column_4` varchar(300) NOT NULL,
  `column_5` varchar(300) NOT NULL,
  `column_6` varchar(300) NOT NULL,
  `column_7` varchar(300) NOT NULL,
  `column_8` varchar(300) NOT NULL,
  `column_9` varchar(300) NOT NULL,
  `column_10` varchar(300) NOT NULL,
  `column_11` varchar(300) NOT NULL,
  `column_12` varchar(300) NOT NULL,
  `column_13` varchar(300) NOT NULL,
  `column_14` varchar(300) NOT NULL,
  `column_15` varchar(300) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(800) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_info`
--

INSERT INTO `general_info` (`id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `branches`, `web_site`, `main_location`, `location`, `map`, `average_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `date`, `time`) VALUES
(1, 'restaurant', 'food', 'Star Kabab', 'star.jpg', 'Family environment.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Science Lab', 'Science Lab', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.222418785588!2d90.3798702126277!3d23.739446684346348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8b6264409eb%3A0x48558c56551a5947!2sStar+Kabab+%26+Restaurant!5e0!3m2!1sen!2sbd!4v1448220890572" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(3, 'fashion', 'life style', 'Artisti', 'artisti.jpg', 'Only for Male. Every types of shirt, T-shart and Pants.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Dhanmondi-15, Satmasjid road, Anam Rangs 6/a', '', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(4, 'cafe', 'food', 'COFFEE HOUSE ', 'cofee.jpg', 'All kind of cafe is available here. Items of snacks are also rich.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No Website', 'Dhanmondi', 'Dhanmondi, 8/a, 15.', '', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(5, 'fashion', 'life style', 'Raymonds', 'raymonds.jpg', 'Best Blazer maker of the country', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.raymonds.com', 'Dhanmondi', 'Dhanmondi, 6/a, Anam rangs;', '', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(6, 'university', 'education', 'United International University', 'uiu.jpg', 'University od dhanmondi. Very fast developing  university.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.uiu.ac.bd', 'Dhanmondi', 'Dhanmondi, 8/a, 15', '<iframe width=100% height=250px  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.0800265187963!2d90.37066061553004!3d23.744525494916385!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755bf4b7bf2bc75%3A0x91d851eada954844!2sUnited+International+University!5e0!3m2!1sen!2sbd!4v1449867240886" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(7, 'hospital', 'health', 'Popular', '', 'Hospital and diagnostic center ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.popularhealth.com.bd', 'Dhanmondi', 'Dhanmondi, science-lab', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.2319779149643!2d90.38007441551485!3d23.73910569512328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8c9f90c9edb%3A0x47894d1f1eb9de95!2z4Kaq4Kaq4KeB4Kay4Ka-4KawIOCmruCnh-CmoeCmv-CmleCnh-CmsiDgppXgprLgp4fgppwg4Ka54Ka-4Ka44Kaq4Ka-4Kak4Ka-4Kay!5e0!3m2!1sbn!2sbd!4v1449935758026" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(8, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-15', '23:59:44.000000'),
(9, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:32:00.000000'),
(10, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:33:18.000000'),
(11, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:38:03.000000'),
(12, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:38:55.000000'),
(13, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:39:19.000000'),
(14, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:39:42.000000'),
(15, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:40:10.000000'),
(16, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:42:51.000000'),
(17, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:44:19.000000'),
(18, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '07:46:35.000000'),
(19, 'entertainment', 'life style', 'Tapu', '', 'Programmer', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'www.upcoming.com', 'Dhanmondi', 'staff quarter', 'no map available', 0, 0, 0, 0, 0, '2015-12-16', '08:43:50.000000'),
(20, 'fashion', 'food', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2015-12-16', '09:59:08.000000'),
(21, 'fashion', 'food', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2015-12-16', '10:10:42.000000');

-- --------------------------------------------------------

--
-- Table structure for table `general_infos`
--

CREATE TABLE `general_infos` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `property_type_acco` varchar(100) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `service_elec` int(11) NOT NULL,
  `warranty_elec` varchar(500) NOT NULL,
  `servicing_elec` varchar(200) NOT NULL,
  `help_line_elec` varchar(200) NOT NULL,
  `type` varchar(90) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `speciality` varchar(600) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `offer` varchar(300) NOT NULL,
  `open_hour` varchar(100) NOT NULL,
  `close_day` varchar(30) NOT NULL,
  `enrty_fee` varchar(30) NOT NULL,
  `package` varchar(200) NOT NULL,
  `travel_alert` varchar(250) NOT NULL,
  `transport` varchar(250) NOT NULL,
  `distance_chart` varchar(250) NOT NULL,
  `travel_agent` varchar(250) NOT NULL,
  `schedual` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `booking_online` varchar(250) NOT NULL,
  `area2` varchar(250) NOT NULL,
  `size` varchar(250) NOT NULL,
  `admission` varchar(250) NOT NULL,
  `requirements` varchar(250) NOT NULL,
  `facility` varchar(250) NOT NULL,
  `information` varchar(250) NOT NULL,
  `highlight` varchar(250) NOT NULL,
  `extra_facilities` varchar(250) NOT NULL,
  `ambulance` varchar(250) NOT NULL,
  `program_schedual` varchar(250) NOT NULL,
  `sponsor` varchar(250) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(400) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_infos`
--

INSERT INTO `general_infos` (`id`, `sub_cata`, `cata`, `property_type_acco`, `name`, `image_link`, `service_elec`, `warranty_elec`, `servicing_elec`, `help_line_elec`, `type`, `description`, `speciality`, `branches`, `area`, `offer`, `open_hour`, `close_day`, `enrty_fee`, `package`, `travel_alert`, `transport`, `distance_chart`, `travel_agent`, `schedual`, `price`, `booking_online`, `area2`, `size`, `admission`, `requirements`, `facility`, `information`, `highlight`, `extra_facilities`, `ambulance`, `program_schedual`, `sponsor`, `web_site`, `main_location`, `location`, `map`, `average_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `date`, `time`) VALUES
(1, 'restaurant', 'food', '', 'Star Kabab', 'star.jpg', 0, '', '', '', 'Bengali food', 'Family environment.', 'Kabab is Spetial here.', '', '', 'No offer going on', '6am-11.30pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Science Lab', 'Science Lab', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3652.222418785588!2d90.3798702126277!3d23.739446684346348!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8b6264409eb%3A0x48558c56551a5947!2sStar+Kabab+%26+Restaurant!5e0!3m2!1sen!2sbd!4v1448220890572" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(3, 'fashion', 'life style', '', 'Artisti', 'artisti.jpg', 0, '', '', '', 'Cloths', 'Only for Male. Every types of shirt, T-shart and Pants.', 'Panjabi is spetial here', '', '', 'No offer is going on', '11am to 8pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Dhanmondi-15, Satmasjid road, Anam Rangs 6/a', '', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(4, 'cafe', 'food', '', 'COFFEE HOUSE ', 'cofee.jpg', 0, '', '', '', 'Cafe|Sancks|Pastry', 'All kind of cafe is available here. Items of snacks are also rich.', 'Coffee is directly imported from Brazil.', '', '', 'For Under 12 age snacks price is half', '7am to 11.30pm', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No Website', 'Dhanmondi', 'Dhanmondi, 8/a, 15.', '', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `image_link` varchar(70) NOT NULL,
  `image_name` varchar(60) NOT NULL,
  `image_text` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_des`
--

CREATE TABLE `item_des` (
  `id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(80) NOT NULL,
  `image_des` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_des`
--

INSERT INTO `item_des` (`id`, `item_id`, `cata`, `cata_id`, `sub_cata`, `image_des`, `image_link`, `image_name`, `date`, `time`) VALUES
(1, 1, 'food', 1, 'restaurant', 'image1 descriptionimage descriptionimage descriptionimage descriptionimage description', '1_1_food_1.jpg', 'Image 1', '0000-00-00', '00:00:00.000000'),
(2, 1, 'food', 1, 'restaurant', 'image2 descriptionimage descriptionimage descriptionimage descriptionimage description', '2_1_food_1.jpg', 'Image 2', '0000-00-00', '00:00:00.000000'),
(3, 1, 'food', 1, 'restaurant', 'image3 descriptionimage descriptionimage descriptionimage descriptionimage description', '3_1_food_1.jpg', 'Image 3', '0000-00-00', '00:00:00.000000'),
(4, 2, 'food', 1, 'restaurant', 'image4 descriptionimage descriptionimage descriptionimage descriptionimage description', '4_2_food_1.jpg', 'Image 4', '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `item_list`
--

CREATE TABLE `item_list` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `size` varchar(30) NOT NULL,
  `model` varchar(100) NOT NULL,
  `filter_1` varchar(40) NOT NULL,
  `filter_2` varchar(40) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_list`
--

INSERT INTO `item_list` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `description`, `image_link`, `price`, `size`, `model`, `filter_1`, `filter_2`, `total_rate`, `rate_1`, `rate_2`, `rate_3`, `rate_people`, `date`, `time`) VALUES
(1, 'food', 1, 'restaurant', 'kabab', 'It''s test great', '1_food_1_restaurant.jpg', 150, '', '', '', '', 15, 5, 5, 5, 1, '0000-00-00', '00:00:00.000000'),
(2, 'food', 1, 'restaurant', 'pizza', 'awesome', '2_food_1_restaurant.jpg', 920, '', '', '', '', 33, 11, 11, 11, 5, '0000-00-00', '00:00:00.000000'),
(3, 'food', 1, 'restaurant', 'Unknown', 'haventual', '2_food_1_restaurant.jpg', 999999999, '', '', '', '', 15, 5, 5, 5, 1, '0000-00-00', '00:00:00.000000'),
(4, 'life style', 3, 'fashion', 'Shirt', 'Green and White check ', '4_life style_3_fashion.jpg', 1200, 'XL', '', 'male', 'shirt', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(5, 'life style', 3, 'fashion', 'Pant', '', '4_life style_3_fashion.jpg', 0, '', '', 'male', 'denim', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000'),
(6, 'life style', 3, 'fashion', 'T-Shirt', '', '4_life style_3_fashion.jpg', 0, '', '', 'male', 'tshirt', 0, 0, 0, 0, 0, '0000-00-00', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `overview`
--

CREATE TABLE `overview` (
  `id` int(255) NOT NULL,
  `name` varchar(26) NOT NULL,
  `rating` int(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `catagory` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `overview`
--

INSERT INTO `overview` (`id`, `name`, `rating`, `location`, `catagory`, `sub_cata`, `image`) VALUES
(1, 'Star Kabab', 4, 'Science-Lab', 'FOOD', 'Restaurant', 'star.jpg'),
(2, 'Appolo', 4, 'Panthapath', 'Health', 'Hospital', 'gta.png'),
(3, 'YELLOW', 5, 'Science Lab', 'Life Style', 'Fashion', 'girl1.jpg'),
(4, 'University Of Asia PacifiC', 3, 'Dhanmondi', 'Education', 'University', 'iron1.png'),
(5, 'HSC Result', 0, 'Bangladesh', 'Education', 'Results', 'car1.png'),
(6, 'Trust College & BritishC', 2, 'Uttara', 'Education', 'College', 'image1.png'),
(7, 'Rajoir K.J.S', 3, 'Madaripur', 'Education', 'School', 'girl1.jpg'),
(8, 'BCS Examination', 0, 'Bangladesh', 'Education', 'Exam', 'car1.png'),
(9, 'BCS Examination', 3, 'Bangladesh', 'Education', 'Exam', 'car1.png'),
(10, 'aceraceraceraceracer', 4, '', 'Electronics', 'Computer', 'computer1.png'),
(11, 'PROCESSOR', 0, 'No Location', 'electronics', 'computer component', 'pro1.jpg'),
(12, 'Sonarga', 4, 'KarwanBazar', 'residence', 'hotel', 'car1.png'),
(13, 'DOM-INNO', 0, 'Dhanmondi', 'residence', 'Apartment', 'image1.png'),
(14, 'COX''S BAZER', 0, 'Cox''s Bazer', 'tourism', 'tourism place', 'cox.jpg'),
(15, 'WATER HOUSE', 5, 'Gazipur', 'tourism', 'resort', 'water.jpg'),
(16, 'BRAC BANK', 5, '', 'SERVICE', 'BANK', 'brack1.png');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(255) NOT NULL,
  `main_info_id` int(50) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `user_id` varchar(60) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `question` varchar(300) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(255) NOT NULL,
  `comment_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `reply` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rivew`
--

CREATE TABLE `rivew` (
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `score` int(20) NOT NULL,
  `date` date NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `user_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE `social_link` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `link_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, 'Tapu Mandal', 'online', '123', '017', 'tmandal', 'user', '', 'enable', '0000-00-00', '00:00:00.000000'),
(2, 'Tapu Mandal', 'online', '123', '', '', 'user', '', 'enable', '2015-12-07', '23:36:12.000000'),
(3, 'Asmita', 'sarker', '555', '', '', 'user', '', 'enable', '2015-12-08', '02:38:12.000000'),
(4, 'Hrithik Roshan', 'roshan', 'roshan', '', '', 'user', '', 'enable', '2015-12-08', '03:24:03.000000'),
(5, 'Razib', '123', '123', '', '', 'user', '', 'enable', '2015-12-10', '18:36:16.000000'),
(6, 'Asmita Sarker', 'asmita', '123', '', '', 'user', '', 'enable', '2015-12-12', '17:07:50.000000'),
(7, '123', '123', '123', '', '', 'user', '', 'enable', '2015-12-12', '17:11:13.000000'),
(8, '456', '456', '456', '', '', 'user', '', 'enable', '2015-12-12', '17:14:04.000000');

-- --------------------------------------------------------

--
-- Table structure for table `user_upload_image`
--

CREATE TABLE `user_upload_image` (
  `id` int(255) NOT NULL,
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_image`
--
ALTER TABLE `general_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_info`
--
ALTER TABLE `general_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_infos`
--
ALTER TABLE `general_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_des`
--
ALTER TABLE `item_des`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_list`
--
ALTER TABLE `item_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overview`
--
ALTER TABLE `overview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `general_image`
--
ALTER TABLE `general_image`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `general_info`
--
ALTER TABLE `general_info`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `general_infos`
--
ALTER TABLE `general_infos`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_des`
--
ALTER TABLE `item_des`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_list`
--
ALTER TABLE `item_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `overview`
--
ALTER TABLE `overview`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
