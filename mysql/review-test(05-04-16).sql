-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2016 at 02:30 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `review-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `status`, `date`, `time`) VALUES
(1, '123', '123', '123', '', '', '', '', '', '0000-00-00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `id` int(255) NOT NULL,
  `question_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `answer` varchar(600) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE `bookmark` (
  `id` int(200) NOT NULL,
  `user_id` int(200) NOT NULL,
  `business_pro_id` int(200) NOT NULL,
  `item_id` int(200) DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmark`
--

INSERT INTO `bookmark` (`id`, `user_id`, `business_pro_id`, `item_id`, `date`) VALUES
(1, 0, 1, NULL, '2016-04-12'),
(2, 2, 1, NULL, '2016-04-12'),
(3, 1, 1, NULL, '2016-04-14'),
(4, 1, 7, NULL, '2016-04-14'),
(5, 1, 18, NULL, '2016-04-14'),
(6, 1, 21, NULL, '2016-04-14');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `item_id` int(100) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `text` varchar(600) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `avg` double NOT NULL,
  `time` time NOT NULL,
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `cata_id`, `cata`, `sub_cata`, `item_id`, `user_id`, `user_type`, `text`, `type`, `rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`, `status`) VALUES
(1, 1, 'food', 'restaurant', 1, '1', 'user', 'not good', 'rate', 2, 2, 1, 1.6666666666667, '14:08:56', '2016-04-11', ''),
(2, 15, 'food', 'cafe', 0, '2', 'user', 'check comment', 'comment', 0, 0, 0, 0, '22:12:05', '2016-04-12', ''),
(3, 5, 'life style', 'fashion', 0, '2', 'user', 'what is it??', 'question', 0, 0, 0, 0, '22:12:43', '2016-04-12', ''),
(4, 1, 'food', 'restaurant', 0, '5', 'user', 'aa', 'comment', 0, 0, 0, 0, '05:06:20', '2016-04-18', ''),
(5, 1, 'food', 'restaurant', 0, '5', 'user', 'dfgdhg', 'comment', 0, 0, 0, 0, '05:07:22', '2016-04-18', ''),
(6, 1, 'food', 'restaurant', 0, '5', 'user', 'tapu mandal', 'comment', 0, 0, 0, 0, '05:07:36', '2016-04-18', ''),
(7, 1, 'food', 'restaurant', 0, '', 'user', 'admin user comment check', 'comment', 0, 0, 0, 0, '05:59:25', '2016-04-24', ''),
(8, 1, 'food', 'restaurant', 1, '', 'user', 'very good', 'rate', 5, 5, 5, 5, '06:00:03', '2016-04-24', ''),
(9, 15, 'food', 'cafe', 6, '', 'user', 'ggggggggggggg', 'rate', 5, 5, 5, 5, '07:07:58', '2016-04-24', ''),
(10, 11, 'food', 'restaurant', 0, '7', 'user', 'tapu', 'comment', 0, 0, 0, 0, '22:34:00', '2016-05-03', ''),
(11, 1, 'food', 'restaurant', 0, '7', 'user', 'image check', 'question', 0, 0, 0, 0, '22:41:14', '2016-05-03', '');

-- --------------------------------------------------------

--
-- Table structure for table `crop`
--

CREATE TABLE `crop` (
  `left` int(50) NOT NULL,
  `top` int(50) NOT NULL,
  `width` int(50) NOT NULL,
  `height` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `doctor_faculty`
--

CREATE TABLE `doctor_faculty` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `schedule` text NOT NULL,
  `day` varchar(150) NOT NULL,
  `room` varchar(100) NOT NULL,
  `visite` varchar(50) NOT NULL,
  `depertment` varchar(50) NOT NULL,
  `extra_activity` varchar(200) NOT NULL,
  `education` varchar(200) NOT NULL,
  `email` varchar(150) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `link` varchar(150) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_faculty`
--

INSERT INTO `doctor_faculty` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `designation`, `schedule`, `day`, `room`, `visite`, `depertment`, `extra_activity`, `education`, `email`, `mobile`, `link`, `description`, `image_link`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'education', 22, 'university', 'j', 'j', '', '', '', '', 'cse', '', 'j', 'j', 'j', '', '', '1_22_education_university.jpg', '2016-04-13', '19:27:05', 1, ''),
(2, 'education', 24, 'university', 't 1', 'd1', '', '', '', '', 'eee', '', 'e 1', 'e 1', 'p 1', '', '', '2_24_education_university', '2016-04-24', '02:50:04', 1, ''),
(3, 'education', 24, 'university', 't 2', 'd 2', '', '', '', '', 'cse', '', '', '', '', '', '', '3_24_education_university', '2016-04-24', '02:50:20', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `elec_item_details`
--

CREATE TABLE `elec_item_details` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `what_item` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `model` varchar(100) NOT NULL,
  `item_time_status` varchar(50) NOT NULL,
  `column_1` varchar(500) NOT NULL,
  `column_2` varchar(500) NOT NULL,
  `column_3` varchar(500) NOT NULL,
  `column_4` varchar(500) NOT NULL,
  `column_5` varchar(500) NOT NULL,
  `column_6` varchar(500) NOT NULL,
  `column_7` varchar(500) NOT NULL,
  `column_8` varchar(500) NOT NULL,
  `column_9` varchar(500) NOT NULL,
  `column_10` varchar(500) NOT NULL,
  `column_11` varchar(500) NOT NULL,
  `column_12` varchar(500) NOT NULL,
  `column_13` varchar(500) NOT NULL,
  `column_14` varchar(500) NOT NULL,
  `column_15` varchar(500) NOT NULL,
  `column_16` varchar(500) NOT NULL,
  `column_17` varchar(500) NOT NULL,
  `column_18` varchar(500) NOT NULL,
  `column_19` varchar(500) NOT NULL,
  `column_20` varchar(500) NOT NULL,
  `column_21` varchar(500) NOT NULL,
  `column_22` varchar(500) NOT NULL,
  `column_23` varchar(500) NOT NULL,
  `column_24` varchar(500) NOT NULL,
  `column_25` varchar(500) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `elec_item_details`
--

INSERT INTO `elec_item_details` (`id`, `cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`, `image_link`, `price`, `model`, `item_time_status`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `column_16`, `column_17`, `column_18`, `column_19`, `column_20`, `column_21`, `column_22`, `column_23`, `column_24`, `column_25`, `total_rate`, `rate_1`, `rate_2`, `rate_3`, `rate_people`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'electronics', 26, '', 'mobile', 'asdasd', 'dasd', '1_26_electronics_', 0, '', 'running', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:53:29', 1, ''),
(2, 'electronics', 26, '', 'processor', 'asdasd', 'asdad', '2_26_electronics_', 0, '', 'coming', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:53:29', 1, ''),
(3, 'electronics', 26, '', 'storage', 'asdasd', 'sda', '3_26_electronics_', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:53:29', 1, ''),
(4, 'electronics', 26, '', 'mobile', 'asdasd', 'dasd', '4_26_electronics_', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '03:06:50', 1, ''),
(5, 'electronics', 26, '', 'processor', 'asdasd', 'asdad', '5_26_electronics_', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '03:06:50', 1, ''),
(6, 'electronics', 26, '', 'storage', 'asdasd', 'sda', '6_26_electronics_', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '03:06:50', 1, ''),
(7, 'electronics', 27, '', 'mobile', 'asdasdasdasd', '22222222222', '7_27_electronics_.jpg', 222, '', 'running', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '03:08:33', 1, ''),
(8, 'electronics', 27, '', 'mobile', 'yyyyyyyyyyyyyyyyyy', 'sdfsdfsf', '8_27_electronics_.jpg', 77777777777, '', 'new', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '03:10:19', 1, ''),
(9, 'electronics', 27, '', 'mother_board', 'qqqqqqqqqqqqqq', '231231231233', '9_27_electronics_.jpg', 1111111, '', 'coming', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '03:15:57', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_image`
--

CREATE TABLE `general_image` (
  `id` int(100) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `image_link` varchar(50) NOT NULL,
  `image_des` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_image`
--

INSERT INTO `general_image` (`id`, `cata_id`, `cata`, `sub_cata`, `image_link`, `image_des`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 1, 'food', 'restaurant', '1_food_1_restaurant.jpg', '', '2016-04-09', '21:32:54', 1, ''),
(2, 1, 'food', 'restaurant', '2_food_1_restaurant.jpg', '', '2016-04-09', '21:32:54', 1, ''),
(3, 2, 'life style', 'fashion', '3_life style_2_fashion.jpg', '', '2016-04-09', '21:36:19', 1, ''),
(4, 2, 'life style', 'fashion', '4_life style_2_fashion.jpg', '', '2016-04-09', '21:36:19', 1, ''),
(5, 3, 'life style', 'fashion', '5_life style_3_fashion.jpg', '', '2016-04-10', '16:50:11', 1, ''),
(6, 3, 'life style', 'fashion', '6_life style_3_fashion.jpg', '', '2016-04-10', '16:50:11', 1, ''),
(7, 4, 'food', 'restaurant', '7_food_4_restaurant.jpg', '', '2016-04-10', '16:50:12', 1, ''),
(8, 4, 'food', 'restaurant', '8_food_4_restaurant.jpg', '', '2016-04-10', '16:50:12', 1, ''),
(9, 5, 'life style', 'fashion', '9_life style_5_fashion.jpg', '', '2016-04-12', '03:35:32', 1, ''),
(10, 5, 'life style', 'fashion', '10_life style_5_fashion.jpg', '', '2016-04-12', '03:35:33', 1, ''),
(11, 6, 'life style', 'fashion', '11_life style_6_fashion.jpg', '', '2016-04-12', '03:59:04', 1, ''),
(12, 6, 'life style', 'fashion', '12_life style_6_fashion.jpg', '', '2016-04-12', '03:59:04', 1, ''),
(13, 7, 'food', 'restaurant', '13_food_7_restaurant.jpg', '', '2016-04-12', '11:26:33', 1, ''),
(14, 7, 'food', 'restaurant', '14_food_7_restaurant.jpg', '', '2016-04-12', '11:26:33', 1, ''),
(15, 8, 'food', 'restaurant', '15_food_8_restaurant.jpg', '', '2016-04-12', '11:29:37', 1, ''),
(16, 8, 'food', 'restaurant', '16_food_8_restaurant.jpg', '', '2016-04-12', '11:29:38', 1, ''),
(17, 9, 'food', 'restaurant', '17_food_9_restaurant.jpg', '', '2016-04-12', '11:30:36', 1, ''),
(18, 9, 'food', 'restaurant', '18_food_9_restaurant.jpg', '', '2016-04-12', '11:30:36', 1, ''),
(19, 11, 'food', 'restaurant', '19_food_11_restaurant.jpg', '', '2016-04-12', '11:32:32', 1, ''),
(20, 11, 'food', 'restaurant', '20_food_11_restaurant.jpg', '', '2016-04-12', '11:32:32', 1, ''),
(21, 12, 'food', 'restaurant', '21_food_12_restaurant.jpg', '', '2016-04-12', '11:32:55', 1, ''),
(22, 12, 'food', 'restaurant', '22_food_12_restaurant.jpg', '', '2016-04-12', '11:32:55', 1, ''),
(23, 13, 'food', 'restaurant', '23_food_13_restaurant.jpg', '', '2016-04-12', '11:33:07', 1, ''),
(24, 13, 'food', 'restaurant', '24_food_13_restaurant.jpg', '', '2016-04-12', '11:33:07', 1, ''),
(25, 14, 'food', 'restaurant', '25_food_14_restaurant.jpg', '', '2016-04-12', '11:33:20', 1, ''),
(26, 14, 'food', 'restaurant', '26_food_14_restaurant.jpg', '', '2016-04-12', '11:33:20', 1, ''),
(27, 15, 'food', 'cafe', '27_food_15_cafe.jpg', '', '2016-04-12', '12:00:58', 1, ''),
(28, 15, 'food', 'cafe', '28_food_15_cafe.jpg', '', '2016-04-12', '12:00:58', 1, ''),
(29, 16, 'food', 'restaurant', '29_food_16_restaurant.jpg', '', '2016-04-12', '14:15:09', 1, ''),
(30, 16, 'food', 'restaurant', '30_food_16_restaurant.jpg', '', '2016-04-12', '14:15:10', 1, ''),
(31, 23, 'food', 'restaurant', '31_food_23_restaurant.jpg', '', '2016-04-24', '02:46:23', 1, ''),
(32, 23, 'food', 'restaurant', '32_food_23_restaurant.jpg', '', '2016-04-24', '02:46:24', 1, ''),
(33, 24, 'education', 'university', '33_education_24_university.jpg', '', '2016-04-24', '02:50:03', 1, ''),
(34, 24, 'education', 'university', '34_education_24_university.jpg', '', '2016-04-24', '02:50:04', 1, ''),
(35, 25, 'food', 'restaurant', '35_food_25_restaurant.jpg', '', '2016-04-24', '02:52:08', 1, ''),
(36, 25, 'food', 'restaurant', '36_food_25_restaurant.jpg', '', '2016-04-24', '02:52:08', 1, ''),
(37, 26, 'electronics', 'computert mobile', '37_electronics_26_computert mobile.jpg', '', '2016-04-24', '02:53:04', 1, ''),
(38, 26, 'electronics', 'computert mobile', '38_electronics_26_computert mobile.jpg', '', '2016-04-24', '02:53:04', 1, ''),
(39, 27, 'electronics', '', '39_electronics_27_.jpg', '', '2016-04-24', '03:08:33', 1, ''),
(40, 27, 'electronics', '', '40_electronics_27_.jpg', '', '2016-04-24', '03:08:33', 1, ''),
(41, 1, 'food', 'restaurant', '41_food_1_restaurant.jpg', '', '2016-04-24', '07:56:06', 1, ''),
(42, 1, 'food', 'restaurant', '42_food_1_restaurant.jpg', '', '2016-04-24', '07:57:01', 1, ''),
(43, 1, 'food', 'restaurant', '43_food_1_restaurant.jpg', '', '2016-04-24', '07:57:01', 1, ''),
(44, 1, 'food', 'restaurant', '44_food_1_restaurant.jpg', '', '2016-04-24', '07:57:01', 1, ''),
(45, 1, 'food', 'restaurant', '45_food_1_restaurant.jpg', '', '2016-04-24', '07:57:01', 1, ''),
(46, 4, 'food', 'restaurant', '46_food_4_restaurant.jpg', '', '2016-04-24', '07:59:40', 1, ''),
(47, 4, 'food', 'restaurant', '47_food_4_restaurant.jpg', '', '2016-04-24', '07:59:40', 1, ''),
(48, 4, 'food', 'restaurant', '48_food_4_restaurant.jpg', '', '2016-04-24', '07:59:40', 1, ''),
(49, 4, 'food', 'restaurant', '49_food_4_restaurant.jpg', '', '2016-04-24', '07:59:40', 1, ''),
(50, 12, 'food', 'restaurant', '50_food_12_restaurant.jpg', '', '2016-04-24', '08:02:49', 1, ''),
(51, 12, 'food', 'restaurant', '51_food_12_restaurant.jpg', '', '2016-04-24', '08:02:49', 1, ''),
(52, 12, 'food', 'restaurant', '52_food_12_restaurant.jpg', '', '2016-04-24', '08:02:49', 1, ''),
(53, 12, 'food', 'restaurant', '53_food_12_restaurant.jpg', '', '2016-04-24', '08:02:49', 1, ''),
(54, 13, 'food', 'restaurant', '54_food_13_restaurant.jpg', '', '2016-04-24', '08:05:44', 1, ''),
(55, 21, 'food', 'restaurant', '55_food_21_restaurant.jpg', '', '2016-04-24', '08:08:15', 1, ''),
(56, 21, 'food', 'restaurant', '56_food_21_restaurant.jpg', '', '2016-04-24', '08:08:15', 1, ''),
(57, 21, 'food', 'restaurant', '57_food_21_restaurant.jpg', '', '2016-04-24', '08:08:15', 1, ''),
(58, 21, 'food', 'restaurant', '58_food_21_restaurant.jpg', '', '2016-04-24', '08:08:15', 1, ''),
(59, 20, 'food', 'restaurant', '59_food_20_restaurant.jpg', '', '2016-04-24', '08:11:19', 1, ''),
(60, 20, 'food', 'restaurant', '60_food_20_restaurant.jpg', '', '2016-04-24', '08:11:19', 1, ''),
(61, 20, 'food', 'restaurant', '61_food_20_restaurant.jpg', '', '2016-04-24', '08:11:20', 1, ''),
(62, 25, 'food', 'restaurant', '62_food_25_restaurant.png', '', '2016-04-24', '08:12:17', 1, ''),
(63, 28, 'tourism', 'heritage', '63_tourism_28_heritage.jpg', '', '2016-05-03', '15:47:11', 1, ''),
(64, 28, 'tourism', 'heritage', '64_tourism_28_heritage.jpg', '', '2016-05-03', '15:47:11', 1, ''),
(65, 31, 'tourism', 'picnic spot', '65_tourism_31_picnic spot.jpg', '', '2016-05-03', '15:52:38', 1, ''),
(66, 31, 'tourism', 'picnic spot', '66_tourism_31_picnic spot.jpg', '', '2016-05-03', '15:52:38', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_info`
--

CREATE TABLE `general_info` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `description` varchar(1500) NOT NULL,
  `column_1` varchar(1500) NOT NULL,
  `column_2` varchar(1500) NOT NULL,
  `column_3` varchar(1500) NOT NULL,
  `column_4` varchar(1500) NOT NULL,
  `column_5` varchar(1500) NOT NULL,
  `column_6` varchar(1500) NOT NULL,
  `column_7` varchar(1500) NOT NULL,
  `column_8` varchar(1500) NOT NULL,
  `column_9` varchar(1500) NOT NULL,
  `column_10` varchar(1500) NOT NULL,
  `column_11` varchar(1500) NOT NULL,
  `column_12` varchar(1500) NOT NULL,
  `column_13` varchar(1500) NOT NULL,
  `column_14` varchar(1500) NOT NULL,
  `column_15` varchar(1500) NOT NULL,
  `column_16` varchar(1500) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `social_links` varchar(500) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(1500) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_info`
--

INSERT INTO `general_info` (`id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, `column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`, `column_12`, `column_13`, `column_14`, `column_15`, `column_16`, `branches`, `web_site`, `social_links`, `main_location`, `location`, `map`, `average_review`, `rate_1`, `rate_2`, `rate_3`, `review_people`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'restaurant', 'food', 'Star Kabab', '1_food_restaurant.jpg', 'Most Popular medium range Bengali food restaurant.', 'AAOAZ', 'Kabab is spetial here.', '10% at Eid Day', '6am-11.30pm', 'what', 'black', '', '', '', '', '', '', '', '', '', '', '', 'No Website', 'youtube.com', '', 'Dhanomondi', '', 0, 0, 0, 0, 0, '2016-04-09', '21:32:54', 1, ''),
(2, 'fashion', 'life style', 'Yellow', '2_life style_fashion.jpg', 'yellow is the fashion house under Beximco ltm.', 'Clothing(Gents,Ladies,kids)', '', '', '', '11am-11pm', '', '', '', '', '', '', '', '', '', '', '', '', 'yellowclothing.com', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-09', '21:36:19', 1, ''),
(3, 'fashion', 'life style', '', '3_life style_fashion', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-10', '16:50:11', 1, ''),
(4, 'restaurant', 'food', '', '4_food_restaurant.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-10', '16:50:11', 1, ''),
(5, 'fashion', 'life style', 'MAP CHECK', '5_life style_fashion.jpg', 'gg', 'hgahg ahsgh', 'hg', 'hghgjhg', 'hjgjkhgjhg', 'jkhgjhgjhgj', '', '', '', '', '', '', '', '', '', '', '', '', 'hgljkhglj', 'hg', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '03:35:32', 1, ''),
(6, 'fashion', 'life style', 'tasaasd', '6_life style_fashion.jpg', 'sadasdsa', 'dasdas', 'dasd', 'asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas asdas ', 'dasd', 'adad', '', '', '', '', '', '', '', '', '', '', '', '', 'a', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '03:59:04', 1, ''),
(7, 'restaurant', 'food', 'Panel check', '7_food_restaurant.jpg', 'hg', 'sdfdhghg', 'hgkjg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:26:33', 1, ''),
(8, 'restaurant', 'food', 'tga', '8_food_restaurant', 'sdfsd', 'gsdf', 'fsd', '', 'fsfsf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:29:37', 1, ''),
(9, 'restaurant', 'food', 'fsfs', '9_food_restaurant', 'df', 'sfd', '', '', 'sf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:30:36', 1, ''),
(10, 'restaurant', 'food', 'dsfsf', '', '', 'sdfsdfsf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:31:00', 1, ''),
(11, 'restaurant', 'food', 'sdfs', '11_food_restaurant', 'fsdfsdfsdf', '', 'sdfs', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:32:32', 1, ''),
(12, 'restaurant', 'food', 'sdfsdfsdf', '12_food_restaurant', 'sdfsdf', 'sdfsf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:32:55', 1, ''),
(13, 'restaurant', 'food', 'dfgdf', '13_food_restaurant.jpg', 'g', 'gdgdf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:33:07', 1, ''),
(14, 'restaurant', 'food', 'gdg', '14_food_restaurant', 'fgdfg', 'fdgd', 'dgdg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'sdgvsb fsdvdsavs', 'sdfsbvb svsv', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '11:33:20', 1, ''),
(15, 'cafe', 'food', 'cafe ite test', '15_food_cafe.jpg', '', 'coffe', 'xcvxzvxzcvxzcv', 'vxzvsvsdvs', 'svsfdbndesbvsavs', 'sgsdfsdfsdv', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '12:00:57', 1, ''),
(16, 'restaurant', 'food', 'sdaf', '16_food_restaurant', 'asdas', 'sdfsadfsafad', 'dadasd', 'asdadas', 'da', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '14:15:09', 1, ''),
(17, 'restaurant', 'food', 'fdfgfdgd', '17_food_restaurant', '', 'gd', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-13', '02:10:21', 1, ''),
(18, 'restaurant', 'food', 'afad', '18_food_restaurant.jpg', 'asda', 'fasdasd', 'sdasdsad', 'asdasda', 'asd', '', '', '', '', '', '', '', '', '', '', '', '', '', 'dad', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:16:00', 1, ''),
(19, 'restaurant', 'food', 'aaaaaaaaaa', '19_food_restaurant', 'aaaaaaa', 'aaaaaaaaa', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:23:19', 1, ''),
(20, 'restaurant', 'food', 'ffffff', '20_food_restaurant.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:24:51', 1, ''),
(21, 'restaurant', 'food', 'yyyyyy', '21_food_restaurant.jpg', 'yyyyyyyyyy', 'yyyyy', 'yyyyyy', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:26:12', 1, ''),
(22, 'university', 'education', 'jjjjj', '22_education_university', 'jjjjjj', 'jjj', 'j', 'jjj', 'jj', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, '2016-04-13', '19:27:05', 1, ''),
(23, 'restaurant', 'food', 'NEW AD PANEL CHECK', '23_food_restaurant.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:46:23', 1, ''),
(24, 'university', 'education', 'education check', '24_education_university.jpg', 'check check', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, '2016-04-24', '02:50:03', 1, ''),
(25, 'restaurant', 'food', 'sdfsdfsdfs', '25_food_restaurant.jpg', 'sdfsdfsd', 'fdsdf', 'fdsfsd', 'fsfsdfs', 'dfsdfsf', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:52:07', 1, ''),
(26, 'computert mobile', 'electronics', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '26_electronics_computert mobile', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:53:04', 1, ''),
(27, '', 'electronics', 'tttttttttttttttttttttttttt', '27_electronics_.jpg', 'asdasdasdas', '', 'dasd', 'asdas', 'dasdasd', 'a', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-24', '03:08:33', 1, ''),
(28, 'heritage', 'tourism', 'Heritage ', '28_tourism_heritage.jpg', 'Description ', 'Area', 'visiting hour', 'close day', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'width=100% height=250px src', 0, 0, 0, 0, 0, '2016-05-03', '15:47:11', 1, ''),
(29, 'hotel', 'residence', 'home', '29_residence_hotel.png', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No change', 0, 0, 0, 0, 0, '2016-05-03', '15:50:10', 1, ''),
(30, 'diagnostic center', 'health', 'Health', '30_health_diagnostic center.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'No change', 0, 0, 0, 0, 0, '2016-05-03', '15:50:52', 1, ''),
(31, 'picnic spot', 'tourism', 'Picnic Spot', '31_tourism_picnic spot.png', 'Description', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-05-03', '15:52:38', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `general_infos`
--

CREATE TABLE `general_infos` (
  `id` int(50) NOT NULL,
  `sub_cata` varchar(60) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `property_type_acco` varchar(100) NOT NULL,
  `name` varchar(80) NOT NULL,
  `image_link` varchar(151) NOT NULL,
  `service_elec` int(11) NOT NULL,
  `warranty_elec` varchar(500) NOT NULL,
  `servicing_elec` varchar(200) NOT NULL,
  `help_line_elec` varchar(200) NOT NULL,
  `type` varchar(90) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `speciality` varchar(600) NOT NULL,
  `branches` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `offer` varchar(300) NOT NULL,
  `open_hour` varchar(100) NOT NULL,
  `close_day` varchar(30) NOT NULL,
  `enrty_fee` varchar(30) NOT NULL,
  `package` varchar(200) NOT NULL,
  `travel_alert` varchar(250) NOT NULL,
  `transport` varchar(250) NOT NULL,
  `distance_chart` varchar(250) NOT NULL,
  `travel_agent` varchar(250) NOT NULL,
  `schedual` varchar(250) NOT NULL,
  `price` varchar(250) NOT NULL,
  `booking_online` varchar(250) NOT NULL,
  `area2` varchar(250) NOT NULL,
  `size` varchar(250) NOT NULL,
  `admission` varchar(250) NOT NULL,
  `requirements` varchar(250) NOT NULL,
  `facility` varchar(250) NOT NULL,
  `information` varchar(250) NOT NULL,
  `highlight` varchar(250) NOT NULL,
  `extra_facilities` varchar(250) NOT NULL,
  `ambulance` varchar(250) NOT NULL,
  `program_schedual` varchar(250) NOT NULL,
  `sponsor` varchar(250) NOT NULL,
  `web_site` varchar(50) NOT NULL,
  `main_location` varchar(70) NOT NULL,
  `location` varchar(300) NOT NULL,
  `map` varchar(400) NOT NULL,
  `average_review` int(255) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `review_people` int(150) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `image_link` varchar(70) NOT NULL,
  `image_name` varchar(60) NOT NULL,
  `image_text` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_des`
--

CREATE TABLE `item_des` (
  `id` int(100) NOT NULL,
  `item_id` int(100) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(80) NOT NULL,
  `image_des` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_list`
--

CREATE TABLE `item_list` (
  `id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `cata_id` int(100) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `color` varchar(150) NOT NULL,
  `size` varchar(30) NOT NULL,
  `model` varchar(100) NOT NULL,
  `filter_1` varchar(40) NOT NULL,
  `filter_2` varchar(40) NOT NULL,
  `filter_3` varchar(100) NOT NULL,
  `total_rate` int(50) NOT NULL,
  `rate_1` int(50) NOT NULL,
  `rate_2` int(50) NOT NULL,
  `rate_3` int(50) NOT NULL,
  `rate_people` int(100) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `admin_id` int(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_list`
--

INSERT INTO `item_list` (`id`, `cata`, `cata_id`, `sub_cata`, `name`, `description`, `image_link`, `price`, `color`, `size`, `model`, `filter_1`, `filter_2`, `filter_3`, `total_rate`, `rate_1`, `rate_2`, `rate_3`, `rate_people`, `date`, `time`, `admin_id`, `status`) VALUES
(1, 'food', 1, 'restaurant', 'Rumali Ruti', 'azim', '1_1_food_restaurant.jpg', 30, 'Black', '', '', 'starter', '', '', 20, 7, 7, 6, 2, '2016-04-09', '21:32:54', 1, ''),
(2, 'life style', 2, 'fashion', 'Sttriped Shirt', '', '2_2_life style_fashion.jpg', 1980, 'Black and Blue', 'XL, L', '', 'male', 'shirt', 'formal shirt', 0, 0, 0, 0, 0, '2016-04-09', '21:36:19', 1, ''),
(3, 'life style', 5, 'fashion', 'daa', '', '3_5_life style_fashion.jpg', 0, 'asdasdasdaas', 'dasdad', '', 'female', 'shirt', 'Casual shirt', 0, 0, 0, 0, 0, '2016-04-12', '03:35:33', 1, ''),
(4, 'food', 15, 'cafe', 'first', 'des', '4_15_food_cafe.png', 500, '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '12:02:28', 1, ''),
(5, 'food', 15, 'cafe', 'first', 'des', '5_15_food_cafe.png', 500, '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-04-12', '12:02:59', 1, ''),
(6, 'food', 15, 'cafe', 'third', 'desdesdes', '6_15_food_cafe.jpg', 987, '', '', '', '', '', '', 15, 5, 5, 5, 1, '2016-04-12', '12:03:26', 1, ''),
(7, 'food', 21, 'restaurant', 'sv', 'aascac', '7_16_food_restaurant.jpg', 0, '', '', '', 'mid', '', '', 0, 0, 0, 0, 0, '2016-04-12', '14:15:21', 1, ''),
(8, 'food', 18, 'restaurant', '', '', '8_18_food_restaurant', 0, '', '', '', 'combo', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:16:00', 1, ''),
(9, 'food', 21, 'restaurant', '', 'bbbbbbb', '9_19_food_restaurant', 5000000000, '', '', '', 'starter', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:23:30', 1, ''),
(10, 'food', 21, 'restaurant', 'new item ', 'aaa bbbb ccc', '10_20_food_restaurant.jpg', 5.555555555555555e15, '', '', '', 'starter', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:25:06', 1, ''),
(11, 'food', 21, 'restaurant', 'gggg sgsgsg', 'gg', '11_20_food_restaurant.jpg', 0, '', '', '', 'fast food', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:25:27', 1, ''),
(12, 'food', 21, 'restaurant', 'yy', 'yyy', '12_21_food_restaurant.jpg', 0, '', '', '', 'combo', '', '', 0, 0, 0, 0, 0, '2016-04-13', '19:26:13', 1, ''),
(13, 'food', 23, 'restaurant', 'eee', 'rrrrrrrrrrrr', '13_23_food_restaurant.jpg', 1111111111, '', '', '', 'beverage', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:46:24', 1, ''),
(14, 'food', 25, 'restaurant', 'dfsdfsfs', 'fdsfdsfsdf', '14_25_food_restaurant', 0, '', '', '', 'salad', '', '', 0, 0, 0, 0, 0, '2016-04-24', '02:52:15', 1, ''),
(15, 'tourism', 28, 'heritage', 'HJ Name', 'adasdasd', '15_28_tourism_heritage.jpg', 0, '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-05-03', '15:47:11', 1, ''),
(16, 'tourism', 31, 'picnic spot', 'item 1', 'ssdffsdfsf', '16_31_tourism_picnic spot.jpg', 0, '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-05-03', '15:52:38', 1, ''),
(17, 'tourism', 31, 'picnic spot', 'dsfsdfsdf', 'dsfsdfsdfsfsfsfsf', '17_31_tourism_picnic spot.jpg', 0, '', '', '', '', '', '', 0, 0, 0, 0, 0, '2016-05-03', '15:52:38', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE `map` (
  `id` int(255) NOT NULL,
  `cata_id` int(255) NOT NULL,
  `cata` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `map` varchar(600) NOT NULL,
  `date` date NOT NULL,
  `admin_id` int(255) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`id`, `cata_id`, `cata`, `sub_cata`, `location`, `address`, `map`, `date`, `admin_id`, `status`) VALUES
(1, 1, 'food', 'restaurant', 'Science Lab', 'Science Lab, besides city college', '<iframe width=100% height=250px  src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14608.267806000724!2d90.3701279!3d23.7449918!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1460410506486" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-04-09', 1, ''),
(2, 1, 'food', 'restaurant', 'Dhanmondi', 'Dhanmondi 9/a. Satmoshjid road', 'width=100% height=250px src', '2016-04-09', 1, ''),
(3, 2, 'life style', 'fashion', 'Dhanmondi', 'Dhanmondi, jigatola road.', 'width=100% height=250px src', '2016-04-09', 1, ''),
(4, 5, 'life style', 'fashion', 'Dhanmondi', 'jigatola', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14608.267806000724!2d90.3701279!3d23.7449918!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1460410506486" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-04-12', 1, ''),
(5, 15, 'food', 'cafe', 'Dhanmondi', 'jigatola', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14608.267806000724!2d90.3701279!3d23.7449918!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1460440799961" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-04-12', 1, ''),
(6, 15, 'food', 'cafe', 'Uttara', '16 no sector', '<iframe width=100% height=250px src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14608.267806000724!2d90.3701279!3d23.7449918!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1460440799961" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '2016-04-12', 1, ''),
(7, 23, 'food', 'restaurant', '1', '1111', '1234width=100% height=250px src', '2016-04-24', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `overview`
--

CREATE TABLE `overview` (
  `id` int(255) NOT NULL,
  `name` varchar(26) NOT NULL,
  `rating` int(100) NOT NULL,
  `location` varchar(200) NOT NULL,
  `catagory` varchar(100) NOT NULL,
  `sub_cata` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(255) NOT NULL,
  `main_info_id` int(50) NOT NULL,
  `cata` varchar(30) NOT NULL,
  `user_id` varchar(60) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `question` varchar(300) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(255) NOT NULL,
  `comment_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `reply` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rivew`
--

CREATE TABLE `rivew` (
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `score` int(20) NOT NULL,
  `date` date NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `user_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search_suggestion`
--

CREATE TABLE `search_suggestion` (
  `id` int(255) NOT NULL,
  `text_s` varchar(500) NOT NULL,
  `s_text_type` varchar(50) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `sub_cata` varchar(50) NOT NULL,
  `cata_item_id` int(120) NOT NULL,
  `count` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `search_suggestion`
--

INSERT INTO `search_suggestion` (`id`, `text_s`, `s_text_type`, `cata`, `sub_cata`, `cata_item_id`, `count`) VALUES
(1, 'Rumali Ruti', 'item', 'food', 'restaurant', 1, 0),
(2, 'Star Kabab', 'business_profile', 'food', 'restaurant', 1, 0),
(3, 'Sttriped Shirt', 'item', 'life style', 'fashion', 2, 0),
(4, 'Yellow', 'business_profile', 'life style', 'fashion', 2, 0),
(5, '', 'business_profile', 'life style', 'fashion', 3, 0),
(6, '', 'business_profile', 'food', 'restaurant', 4, 0),
(7, 'daa', 'item', 'life style', 'fashion', 3, 0),
(8, 'MAP CHECK', 'business_profile', 'life style', 'fashion', 5, 0),
(9, 'tasaasd', 'business_profile', 'life style', 'fashion', 6, 0),
(10, 'Panel check', 'business_profile', 'food', 'restaurant', 7, 0),
(11, 'tga', 'business_profile', 'food', 'restaurant', 8, 0),
(12, 'fsfs', 'business_profile', 'food', 'restaurant', 9, 0),
(13, 'sdfs', 'business_profile', 'food', 'restaurant', 11, 0),
(14, 'sdfsdfsdf', 'business_profile', 'food', 'restaurant', 12, 0),
(15, 'dfgdf', 'business_profile', 'food', 'restaurant', 13, 0),
(16, 'gdg', 'business_profile', 'food', 'restaurant', 14, 0),
(17, 'cafe ite test', 'business_profile', 'food', 'cafe', 15, 0),
(18, 'first', 'item', 'food', 'cafe', 4, 0),
(19, 'first', 'item', 'food', 'cafe', 5, 0),
(20, 'third', 'item', 'food', 'cafe', 6, 0),
(21, 'sdaf', 'business_profile', 'food', 'restaurant', 16, 0),
(22, 'sv', 'item', 'food', 'restaurant', 7, 0),
(23, 'fdfgfdgd', 'business_profile', 'food', 'restaurant', 17, 0),
(24, '', 'item', 'food', 'restaurant', 8, 0),
(25, 'afad', 'business_profile', 'food', 'restaurant', 18, 0),
(26, 'aaaaaaaaaa', 'business_profile', 'food', 'restaurant', 19, 0),
(27, 'bbbbbbbbb', 'item', 'food', 'restaurant', 9, 0),
(28, 'ffffff', 'business_profile', 'food', 'restaurant', 20, 0),
(29, 'ff', 'item', 'food', 'restaurant', 10, 0),
(30, 'gggg', 'item', 'food', 'restaurant', 11, 0),
(31, 'yy', 'item', 'food', 'restaurant', 12, 0),
(32, 'yyyyyy', 'business_profile', 'food', 'restaurant', 21, 0),
(33, 'j', 'item_doc_fac', 'education', 'university', 1, 0),
(34, 'jjjjj', 'business_profile', 'education', 'university', 22, 0),
(35, 'eee', 'item', 'food', 'restaurant', 13, 0),
(36, 'NEW AD PANEL CHECK', 'business_profile', 'food', 'restaurant', 23, 0),
(37, 't 1', 'item_doc_fac', 'education', 'university', 2, 0),
(38, 'education check', 'business_profile', 'education', 'university', 24, 0),
(39, 't 2', 'item_doc_fac', 'education', 'university', 3, 0),
(40, 'sdfsdfsdfs', 'business_profile', 'food', 'restaurant', 25, 0),
(41, 'dfsdfsfs', 'item', 'food', 'restaurant', 14, 0),
(42, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'business_profile', 'electronics', 'computert mobile', 26, 0),
(43, 'tttttttttttttttttttttttttt', 'business_profile', 'electronics', '', 27, 0),
(44, 'HJ Name', 'item', 'tourism', 'heritage', 15, 0),
(45, 'Heritage ', 'business_profile', 'tourism', 'heritage', 28, 0),
(46, 'home', 'business_profile', 'residence', 'hotel', 29, 0),
(47, 'Health', 'business_profile', 'health', 'diagnostic center', 30, 0),
(48, 'item 1', 'item', 'tourism', 'picnic spot', 16, 0),
(49, 'dsfsdfsdf', 'item', 'tourism', 'picnic spot', 17, 0),
(50, 'Picnic Spot', 'business_profile', 'tourism', 'picnic spot', 31, 0);

-- --------------------------------------------------------

--
-- Table structure for table `social_link`
--

CREATE TABLE `social_link` (
  `id` int(50) NOT NULL,
  `main_info_id` varchar(100) NOT NULL,
  `cata` varchar(60) NOT NULL,
  `link` varchar(100) NOT NULL,
  `link_name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(70) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `phone` varchar(18) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `image_link` varchar(120) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `pass`, `phone`, `user_name`, `type`, `image_link`, `date`, `time`, `status`) VALUES
(1, 'mobile user', 'email', 'email', '', '', 'user', '1_ada.jpg', '2016-04-05', '09:47:57', ''),
(2, 'Tapu', 'mandal', 'mandal', '', '', 'user', '2_Tapu.jpg', '2016-04-12', '22:10:30', ''),
(3, 'sdfdsasaf', 'sdfsfsdf', 'sfsafsf', '', '', 'user', '3_sdfdsasaf.jpg', '2016-04-12', '22:33:24', ''),
(4, 'sfkjsfgg', 'ggkg', 'kgkjgk', '', '', 'user', '4_sfkjsfgg', '2016-04-12', '22:34:17', ''),
(5, '123', '123', '123', '', '', 'user', '5_123.jpg', '2016-04-12', '22:34:52', ''),
(6, 'ada', '222', '222', '', '', 'user', '', '2016-04-14', '00:49:08', ''),
(7, 'Tapu Mandal', 'tapu', 'tapu', '', '', 'user', '7_.jpg', '2016-05-03', '16:21:40', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_upload_image`
--

CREATE TABLE `user_upload_image` (
  `id` int(255) NOT NULL,
  `main_info_id` int(255) NOT NULL,
  `cata` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `image_link` varchar(100) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elec_item_details`
--
ALTER TABLE `elec_item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_image`
--
ALTER TABLE `general_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_info`
--
ALTER TABLE `general_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_infos`
--
ALTER TABLE `general_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_des`
--
ALTER TABLE `item_des`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_list`
--
ALTER TABLE `item_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map`
--
ALTER TABLE `map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `overview`
--
ALTER TABLE `overview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `search_suggestion`
--
ALTER TABLE `search_suggestion`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `doctor_faculty`
--
ALTER TABLE `doctor_faculty`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `elec_item_details`
--
ALTER TABLE `elec_item_details`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `general_image`
--
ALTER TABLE `general_image`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `general_info`
--
ALTER TABLE `general_info`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `general_infos`
--
ALTER TABLE `general_infos`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_des`
--
ALTER TABLE `item_des`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_list`
--
ALTER TABLE `item_list`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `map`
--
ALTER TABLE `map`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `overview`
--
ALTER TABLE `overview`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `search_suggestion`
--
ALTER TABLE `search_suggestion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
