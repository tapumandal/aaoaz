<?php
session_start();
include 'operation/url_operation.php';
?>

<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106976768-1');
	</script>


	<?php
include 'header_tags.php';
include 'top_link_list.php';
?>
</head>



<body>
	<?php
// include 'sideNabBar.php';
?>
	<div class="page_loader"></div>

	<div class = "container details_page">

		<!-- FOR BLUR -->

		<div class="overlay-bg" id="food_overlay_bg"> </div> <!-- FOR BLUR -->
		<div class="overlay-content popup54441" >
			<!-- <button class="close-btn">X</button> -->
			<div ID="overlay_content">

			</div>

		</div>

		<!-- OVERLAY -->


		<div class = "main-top">
			<div class="main">
				<?php

include 'header.php';

?>
			</div>
		</div>


		<div class="main_content_center">
			<div class="col-sm-12 field food">
				<div class="wide_pattern col-sm-12" id="search_result">  <!-- Whole information without add -->

					<div class="col-sm-6 first food_left_side" ID="info_height">  <!-- NORMAL INFORMATION  -->
						<?php
$general_obj = new general();
$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
$general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
$general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);

?>
					</div>   <!-- NORMAL INFORMATION  -->

					<div class="col-sm-6 second"  id="slide_height">   <!-- PHOTO GALLARY -->

						<?php
include 'slide-show.php';
?>

						<!-- PHOTO GALLARY -->


						<?php
$general_obj = new general();
$table_pattern_top_1 = '
						<!-- ITEM LIST -->
						<div class="menu food_item_table">


						<ul class="nav nav-tabs">
						';
$table_pattern_top_2 = $general_obj->filter_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);

$table_pattern_top_3 = '</ul>

						<div class="food_item_search">
						<input class="form-control" id="myInput" type="text" placeholder="Search Item...">
						</div>

						<div class="food_item_list" ID="food_list_height" style="height:300px">
						<div class="table-responsive">
						<table class="table table-bordered table-striped">
						<thead>
						<tr class="food_list_table_hader">
						<th width="2%"></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						</tr>
						</thead>
						<tbody id="myTable">
						';
$table_pattern_top = $table_pattern_top_1 . $table_pattern_top_2 . $table_pattern_top_3;
$table_pattern_bottom = '</tbody>
						</table>
						</div>
						</div>
						</div>  <!-- ITEM LIST -->
						';

$general_obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], $table_pattern_top, $table_pattern_bottom);
?>
					</div>


					<!-- <div class="clearfix"></div> -->

					<!-- <div class="col-sm-12">   COMMENT AND QUESTIOIN -->
						<div class="col-sm-12 comment comment_3" id="jump" >   <!-- COMMENT -->


							<?php
include 'comment_3.php';
?>

							<div class="com">

								<?php
$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
?>
							</div>

						</div>   <!-- COMMENT  -->
						<!-- </div>   COMMENT AND QUESTIOIN -->



					</div> <!-- Whole information without add -->

					<!-- add -->
					<!-- <div class="col-sm-2 third">
						<div class="inside_top_right_panel">
							<div class="inside_top_head">
								<h3>Recomended</h3>
							</div>
							<div class="insider_box_panel">

								<?php
include 'operation/suggestion.php';
$obj = new suggestion();
$obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
?>

							</div>
						</div>
					</div> -->
					<!-- add -->

				</div>


			</div>
			<!-- END OF container -->
		</div>
	</div>
		<?php
include 'footer.php';
include 'bottom_link_list.php';
?>


	</body>
	</html>