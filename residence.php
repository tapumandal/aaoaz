<?php
session_start();
include 'operation/url_operation.php';
?>
<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:fb="http://ogp.me/ns/fb#">

<head>	
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106976768-1');
	</script>

	<?php
	include 'header_tags.php';
	include 'top_link_list.php'; 
	?>
</head>

<body>
	<?php
		// include 'sideNabBar.php';
	?>
	<div class="page_loader"></div>
<div class = "container details_page">

		<div class="overlay-bg"> </div> <!-- FOR BLUR -->
		<div class="overlay-content popup54441" >
			<!-- <button class="close-btn">X</button> -->
			<div ID="overlay_content">
				
			</div>
			
		</div>


		<div class = "main-top">
			<div class="main">
				<?php

				include 'header.php';

				?>
			</div>
		</div>
	<div class="main_content_center">
		<div class="col-sm-12 field">
			<div class="">  <!-- Whole information without add -->





				<div class="clearfix"></div>



				<div class="col-sm-6 first" ID="info_height">  <!-- NORMAL INFORMATION  -->
					<?php
					$general_obj = new general();
					$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
					?>	

				</div>   <!-- NORMAL INFORMATION  -->


				<div class="col-sm-6 second" >   <!-- PHOTO GALLARY -->
					<div >
						<?php 
						include 'slide-show.php';
						$general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
						$general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					

						?>

					</div>
				</div>			<!-- PHOTO GALLARY -->
				<div class="clearfix"> </div>
				<div class="attrction_panel attrction_room">
					<div class="attraction_title">
						<span>ROOMS AND SUITES</span>
					</div>
					<div class="attraction_iframe">
						<!-- <iframe src="tourist_attraction.php?cata=<?php echo $_SESSION[cata]; ?>&sub_cata=<?php echo $_SESSION[sub_cata]; ?>&id=<?php echo $_SESSION[content_id]; ?>"></iframe>  -->

						<button class="slide_right_bt" onclick="tourismSlide('left')"><img src="image/default_image/left.png"></button>
						<button class="slide_left_bt" onclick="tourismSlide('right')"><img src="image/default_image/right.png"></button>
						<?php
		   					// include 'code_php.php';
						echo '<div class="attraction col-sm-12">';
						$obj = new general();
						$obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], "room", $table_pattern_bottom);
						echo '</div>';
						?>
					</div>
				</div>


				<div class="clearfix"> </div>

				<div class="attrction_panel attrction_service">
					<div class="attraction_title">
						<span>Service And Facilities</span>
					</div>
					<div class="attraction_iframe">
						<!-- <iframe src="tourist_attraction.php?cata=<?php echo $_SESSION[cata]; ?>&sub_cata=<?php echo $_SESSION[sub_cata]; ?>&id=<?php echo $_SESSION[content_id]; ?>"></iframe>  -->

						<button class="slide_right_bt2" onclick="tourismSlide('left')"><img src="image/default_image/left.png"></button>
						<button class="slide_left_bt2" onclick="tourismSlide('right')"><img src="image/default_image/right.png"></button>
						<?php
		   					// include 'code_php.php';
						echo '<div class="attraction2 col-sm-12">';
						$obj = new general();
						$obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], "service", $table_pattern_bottom);
						echo '</div>';
						?>
					</div>
				</div>


				<div class="clearfix"></div>

				<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
					<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->
						<?php
						include 'comment_content.php';
						?>


						<div class="com">
							<?php
							$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
							?>
						</div>

					</div>   <!-- COMMENT  -->

				</div>   <!-- COMMENT AND QUESTIOIN -->


			</div> <!-- Whole information without add -->

			<!-- <div class="col-sm-2 third">
				<div class="inside_top_right_panel">
					<div class="inside_top_head">
						<h3>Recomended</h3>
					</div>
					<div class="insider_box_panel">
						<?php
						// include 'operation/suggestion.php';
						// $obj = new suggestion();
						// $obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
						?>
						
					</div>
				</div>
			</div> -->
		</div>

	</div>
</div>
	<!-- END OF container -->

	<?php
	include 'footer.php';
	include 'bottom_link_list.php';
	?>

</body>
</html>