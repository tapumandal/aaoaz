<?php
session_start();
include 'operation/url_operation.php';
if (isset($_GET[content]) && !isset($_GET[cata]) && !isset($_GET[sub_cata])) {
	header('Location: ./' . $_SESSION[cata] . '.php?content=' . $_SESSION[content_name]);
}
?>
<!DOCTYPE html>
<head>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106976768-1');
</script>

 <meta charset="utf-8">
<link rel="icon" href="image/default_image/short_logo.png">
<title>Aaoaz for Restaurant | Life Style | Tourism | Hotel and More</title>
<meta name="description" content="Welcome to aaoaz.com | We serve you the most reliable information and public review of restaurant, cafe, fashion, accessorice, parlor, university, hospital, doctor, hotel, resort, lake, park etc. We are updating everyday to make sure our data is not outdated and you don't get bored.">
<meta name="keywords" content="aaoaz, information, best, popular, where, restaurant, cafe, fashion,  parlor, university, doctor,  ambulance, hotel, resort, tourism, heritage, lake, rating, review, dhaka, dhanmondi, gulshan, uttara">
<meta name="author" content="aaoaz.com">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="theme-color" content="#317EFB"/>



<meta property="og:url"           content="http://www.aaoaz.com" />
<meta property="og:type"          content="Website" />
<meta property="og:title"         content="Aaoaz for Restaurant | Life Style | Tourism | Hotel and More" />
<meta property="og:description"   content="Join aaoaz.com | We serve you the most reliable information and public review of restaurant, cafe, fashion, accessorice, parlor, university, hospital, doctor, hotel, resort, lake, park etc. We are updating everyday to make sure our data is not outdated and you don't get bored." />
<meta property="og:image"         content="http://www.aaoaz.com/image/default_image/aaoaz_logo.jpg" />
<meta property="fb:app_id" 		  content="1819956078267410" />


<link rel="canonical" href="http://www.aaoaz.com/">


<?php include 'top_link_list.php';?>

<style type="text/css">
	.mobile_view .side_menu_btn{
		display: none;
	}
	@media screen and (max-width: 560px){
		.search_box input.search_input {
		    padding-left: 10px !important;
		}
	}
</style>

</head>

<body onload="getContent()" >
<div class="page_loader"></div>
<div class = "container">

	<div class = "main-top">
		<div class="main">
				<?php
include 'header.php';
?>

				<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script> -->


				<div id="shownav"></div>

				<?php
// include 'content.php';
?>
		</div>
	</div>

<div id="test2">

</div>







</div>
<!-- END OF container -->
<?php
include 'footer.php';
include 'bottom_link_list.php';
?>
</body>
</html>
