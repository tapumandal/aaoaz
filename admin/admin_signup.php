<?php
error_reporting(0);
session_start();

$token = "";
if (isset($_POST['_token'])) {
	$token = $_POST['_token'];
} else if (isset($_GET['_token'])) {
	$token = $_POST['_token'];
}

$tokenTime = time() - $_SESSION['_token_time'];

if ($token == $_SESSION['_token'] && $tokenTime < 300) {

} else {

	echo '<script> window.location.replace("./"); </script>';
	exit();
}

include 'data_connection.php';

function adminSignStringFilter($string) {
	include 'data_connection.php';
	$string = mysqli_real_escape_string($conn, $string);

	$string = trim($string);
	$string = str_replace('\\', '', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('SELECT', '', $string);
	$string = str_replace('UPDATE', '', $string);
	$string = str_replace('DROP', '', $string);
	$string = str_replace('INSERT', '', $string);
	$string = str_replace('DELETE', '', $string);
	$string = str_replace('FROM', '', $string);
	$string = str_replace('WHERE', '', $string);
	$string = str_replace('JOIN', '', $string);

	$string = str_replace('select', '', $string);
	$string = str_replace('update', '', $string);
	$string = str_replace('drop', '', $string);
	$string = str_replace('insert', '', $string);
	$string = str_replace('delete', '', $string);
	$string = str_replace('from', '', $string);
	$string = str_replace('where', '', $string);
	$string = str_replace('join', '', $string);
	return $string;
}

$postName = adminSignStringFilter($_POST[name]);
$postEmail = adminSignStringFilter($_POST[email]);
$postPass = adminSignStringFilter($_POST[pass]);
$postPhone = adminSignStringFilter($_POST[phone]);

$encryptPostPass = encryptPassword($postPass);

if ($postName != "" && $postEmail != "" && $postPass != "") {
	// $sign_up = "INSERT INTO `admin`(`name`, `email`, `pass`, `phone`, `type`, `status`, `date`, `time`) VALUES ('$postName','$postEmail','$encryptPostPass','$postPhone', 'admin','disable', now(), now());";

	$stmt = $conn->prepare("INSERT INTO `admin`(`name`, `email`, `pass`, `phone`, `type`, `status`, `date`, `time`)
							VALUES (?, ?, ?, ?, 'admin','disable', now(), now())");

	$stmt->bind_param("ssss", $postName, $postEmail, $encryptPostPass, $postPhone);

	// $stmt->execute();
	// echo $stmt->execute();

	if (!$stmt->execute()) {
		echo "There is a problem. You are recomended to contat aaoaz authority";
	} else {
		$stmt->close();
		header('Location: ' . $_POST['page']);

	}
	$stmt->close();
}

function encryptPassword($string) {
	$enPass = md5($string);
	$enTmp1 = "";
	$enTmp2 = "";
	$enTmp3 = "";
	$enTmp4 = "";
	$enTmp5 = "";

	$i = 0;
	for ($i = 0; $i < strlen($enPass); $i++) {
		if ($i < 4) {
			$enTmp1 = $enTmp1 . $enPass[$i];
		} else if ($i < 8) {
			$enTmp2 = $enTmp2 . $enPass[$i];
		} else if ($i < 12) {
			$enTmp3 = $enTmp3 . $enPass[$i];
		} else if ($i < 16) {
			$enTmp4 = $enTmp4 . $enPass[$i];
		} else {
			$enTmp5 = $enTmp5 . $enPass[$i];
		}
	}

	$encrypted = $enTmp1 . $enTmp4 . $enTmp3 . $enTmp2 . $enTmp5;

	return $encrypted;
}
?>