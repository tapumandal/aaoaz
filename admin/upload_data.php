<?php
session_start();
ob_start();
error_reporting(0);
include "data_connection.php";

if (!isset($_SESSION[adminId])) {
	header("Location: ../admin_login.php");
}

$cata = strtolower($_GET[cata]);
$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
$cata = str_replace($arrayName, "", $cata);

$sub_cata = strtolower($_GET[sub_cata]);
$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
$sub_cata = str_replace($arrayName, "", $sub_cata);

if ($cata == "electronics") {
	$sub_cata = strtolower($_POST[elec_sub_cata]);
	$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
	$sub_cata = str_replace($arrayName, "", $sub_cata);
} else {
	$sub_cata = strtolower($_GET[sub_cata]);
	$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
	$sub_cata = str_replace($arrayName, "", $sub_cata);
}

$content_id = $_GET[content_id];
$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
$content_id = str_replace($arrayName, "", $content_id);

if (empty($content_id)) {
	echo '<div class="content_name"><h3>Content Name : </h3><h2>' . $_POST[name] . '.</h2> <br> <h3>Catagoty : </h3><h2>' . $_GET[cata] . '.</h2> <br> <h3>Sub Catagory : </h3><h2>' . $_GET[sub_cata] . '</h2></div>';
	echo "<div class='function_calling'><h5>InsertGeneralInfo($cata, $sub_cata); calling to insert General Info </h5></div>";
	InsertGeneralInfo($cata, $sub_cata);
} else if (!empty($content_id)) {
	echo "<div class='advance_update'><h2> Advance Update</h2></div>";
	$getContentName = "SELECT name FROM general_info WHERE id = '$content_id';";

	$res = $conn->query($getContentName);
	if ($res->num_rows > 0) {
		while ($row = $res->fetch_assoc()) {
			$content_name = $row[name];
		}
	}

	$last_content_id = $content_id;

	$general_info_update = "";

	if (isset($_SESSION[title_image_name]) && isset($_SESSION[title_image_type]) && $_SESSION[title_image_type] != "") {
		echo "<div class='function_calling'><h5>insert_title_image($last_content_id, $cata, $sub_cata, $name); calling to insert Title Image </h5></div>";
		insert_title_image($last_content_id, $cata, $sub_cata, $content_name);

		$general_info_update = $general_info_update . " | T-Image";
	}

	$main_location = arrayCharReplace($_POST[main_location]);

	if (isset($main_location[0]) && $main_location[0] != "") {
		echo "<div class='function_calling'><h5>insertMap($last_content_id, $cata, $sub_cata); calling to insert Map </h5></div>";
		insertMap($last_content_id, $cata, $sub_cata);

		$general_info_update = $general_info_update . " | Location-" . count($main_location);
	}

	if (isset($_SESSION[main_image_name][0]) && isset($_SESSION[main_image_type][0]) && $_SESSION[main_image_type][0] != "") {
		echo "<div class='function_calling'><h5>insert_main_image($last_content_id, $cata, $sub_cata, $name); calling to insert Main Image </h5></div>";
		insert_main_image($last_content_id, $cata, $sub_cata, $content_name);
		$main_image_name = $_SESSION[main_image_name];
		$general_info_update = $general_info_update . " | M-Image";
	}

	//ITEM INSERTION
	if ($cata == "electronics") {
		echo "<div class='function_calling'><h5>elecInsertItem($last_content_id, $cata, $sub_cata); calling to insert electronic product </h5></div>";
		elecInsertItem($last_content_id, $cata, $sub_cata);
	} else {
		echo "<div class='function_calling'><h5>insertItem($last_content_id, $cata, $sub_cata); calling to insert Item List</h5></div>";
		insertItem($last_content_id, $cata, $sub_cata);

		$itemList = null;
		if (count($_POST[item_name]) > 0) {
			$itemList = $_POST[item_name];
		} else if (count($_POST[teacher_name]) > 0) {
			$itemList = $_POST[teacher_name];
		} else if (count($_POST[doctor_name]) > 0) {
			$itemList = $_POST[doctor_name];
		}

		if (count($itemList) > 0) {
			echo $itemList[0] . "<<";
			$item_info_update = date("Y.m.d") . " | " . $_SESSION[adminId] . " | " . count($itemList) . "-Inserted";

			$getContentName = "SELECT item_info_update FROM general_info WHERE id = '$content_id';";
			$res = $conn->query($getContentName);
			if ($res->num_rows > 0) {
				while ($row = $res->fetch_assoc()) {
					$item_info_update = $item_info_update . "  #  " . $row[item_info_update];

					$insert = "UPDATE `general_info` SET `item_info_update` = '$item_info_update' WHERE id = '$content_id'";
					if ($conn->query($insert) === TRUE) {
						echo "<br> General  Item Info Updated Status Inserted";
					}
				}
			} else {
				$general_info_update = $general_info_update . "  #  ";

				$insert = "UPDATE `general_info` SET `g_info_update` = '$general_info_update' WHERE id = '$content_id'";
				if ($conn->query($insert) === TRUE) {
					echo "<br> General Info Updated Status Inserted";
				}
			}
		}
	}

	if ($cata == "tourism" && isset($_POST[name])) {
		echo "<div class='function_calling'><h5>insertNearby($last_content_id); calling to insert Nearby Information</h5></div>";
		insertNearby($last_content_id);
	}

	// GeneralInfo update Status
	if ($general_info_update != "") {
		$general_info_update = $_SESSION[adminId] . " | " . date("Y.m.d") . $general_info_update;

		$getContentName = "SELECT g_info_update FROM general_info WHERE id = '$content_id';";

		$res = $conn->query($getContentName);
		if ($res->num_rows > 0) {
			while ($row = $res->fetch_assoc()) {
				$general_info_update = $general_info_update . "  #  " . $row[g_info_update];

				$insert = "UPDATE `general_info` SET `g_info_update` = '$general_info_update' WHERE id = '$content_id'";
				if ($conn->query($insert) === TRUE) {
					echo "<br> General Info Updated Status Inserted";
				}
			}
		} else {
			$general_info_update = $general_info_update . "  #  ";

			$insert = "UPDATE `general_info` SET `g_info_update` = '$general_info_update' WHERE id = '$content_id'";
			if ($conn->query($insert) === TRUE) {
				echo "<br> General Info Updated Status Inserted";
			}
		}

	}

}

function arrayCharReplace($arrayStr) {
	$i = 0;
	while ($arrayStr[$i]) {
		$arrayStr[$i] = trim($arrayStr[$i]);
		$arrayStr[$i] = str_replace('\'', "\'", $arrayStr[$i]);
		$arrayStr[$i] = str_replace('"', '\"', $arrayStr[$i]);
		$arrayStr[$i] = str_replace('\\', '\\', $arrayStr[$i]);
		$arrayStr[$i] = str_replace('`', "\'", $arrayStr[$i]);
		$arrayStr[$i] = str_replace(';', '. ', $arrayStr[$i]);

		$arrayRes[$i] = $arrayStr[$i];
		$i++;
	}

	return $arrayRes;
}

function stringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '\\\ ', $string);
	$string = str_replace('\'', "\'", $string);
	$string = str_replace('"', '\"', $string);
	$string = str_replace('`', "\'", $string);
	$string = str_replace(';', '. ', $string);

	return $string;
}

function InsertGeneralInfo($cata, $sub_cata) {
	echo "<br><br> function InsertGeneralInfo() <br>";
	include "data_connection.php";

	$name = stringFilter($_POST[name]);
	$description = stringFilter($_POST[description]);
	$type = stringFilter($_POST[type]);
	$speciality = stringFilter($_POST[speciality]);
	$offer = stringFilter($_POST[offer]);
	$opening_hour = stringFilter($_POST[opening_hour]);
	$main_contact = stringFilter($_POST[main_contact]);
	$program_schedual = stringFilter($_POST[program_schedual]);
	$sponsor = stringFilter($_POST[sponsor]);
	$dept_list = stringFilter($_POST[dept_list]);
	$information = stringFilter($_POST[information]);
	$highlight = stringFilter($_POST[highlight]);
	$admission = stringFilter($_POST[admission]);
	$admission_requirments = stringFilter($_POST[admission_requirments]);
	$notice = stringFilter($_POST[notice]);
	$course_details = stringFilter($_POST[course_details]);
	$cgpa_system = stringFilter($_POST[cgpa_system]);
	$class_plan = stringFilter($_POST[class_plan]);
	$exam_schedule = stringFilter($_POST[exam_schedule]);
	$exam_hall = stringFilter($_POST[exam_hall]);
	$expense = stringFilter($_POST[expense]);
	$main_location = stringFilter($_POST[main_location]);
	$location = stringFilter($_POST[location]);
	$department = stringFilter($_POST[department]);
	$facilities = stringFilter($_POST[facilities]);
	$ambulance = stringFilter($_POST[ambulance]);
	$product_type = stringFilter($_POST[product_type]);
	$services = stringFilter($_POST[services]);
	$warranty = stringFilter($_POST[warranty]);
	$servicing_center = stringFilter($_POST[servicing_center]);
	$property_type = stringFilter($_POST[property_type]);
	$help_line = stringFilter($_POST[help_line]);
	$size = stringFilter($_POST[size]);
	$rent = stringFilter($_POST[rent]);
	$are = stringFilter($_POST[area]);
	$visiting_hour = stringFilter($_POST[visiting_hour]);
	$close_day = stringFilter($_POST[close_day]);
	$entry_fee = stringFilter($_POST[entry_fee]);
	$pakage = stringFilter($_POST[pakage]);
	$travel_alert = stringFilter($_POST[travel_alert]);
	$how_to_go = stringFilter($_POST[how_to_go]);
	$distance_chart = stringFilter($_POST[distance_chart]);
	$history = stringFilter($_POST[history]);

	$web_site = $_POST[web_site];
	$social_links = $_POST[social_links];

	$facilities_ac = stringFilter($_POST[facilities_ac]);
	$facilities_wifi = stringFilter($_POST[facilities_wifi]);
	$facilities_card = stringFilter($_POST[facilities_card]);

	$facilities_service = stringFilter($_POST[facilities_service]);
	$facilities_reservation = stringFilter($_POST[facilities_reservation]);
	$facilities_parking = stringFilter($_POST[facilities_parking]);

	$facilities_capacity = stringFilter($_POST[facilities_capacity]);
	$facilities_other = stringFilter($_POST[facilities_other]);

	if (!isset($name) || $name == "" || $name == null) {
		echo "<h2 style='background:red;'>Name is Empty</h2>";
		exit();
	}

	$processedMap = funcSplit($_POST[map]);
	if ($cata == "food") {

		$foodFacilities = $facilities_ac . "," . $facilities_wifi . "," . $facilities_card . "," . $facilities_reservation . "," . $facilities_parking . "," . $facilities_service . "," . $facilities_capacity . ";" . $facilities_other;

		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`type`, `offer`, `column_2`, `column_4`, `column_5`,
				  `web_site`, `social_links`, `contact`, `date`, `time`, `admin_id`, `status`)
			VALUES ('$sub_cata', '$cata', '$name', '$description',
				'$type', '$offer', '$speciality', '$opening_hour', '$foodFacilities',
				'$web_site', '$social_links', '$main_contact', now(), now(), '$_SESSION[adminId]', 'not approved')";

	} else if ($cata == "life style") {
		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`type`, `offer`, `column_3`, `column_4`,
				`column_5`,`column_6`, `web_site`, `social_links`, `contact`,
				`date`, `time`, `admin_id`, `status`)
				VALUES ('$sub_cata', '$cata', '$name', '$description',
					'$type', '$offer', '$speciality', '$program_schadual', '$sponsor',
					 '$opening_hour', '$web_site', '$social_links', '$main_contact',
					  now(), now(), '$_SESSION[adminId]', 'not approved')";

	} else if ($cata == "education") {
		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`column_1`, `column_2`, `column_3`, `column_4`,
				`column_5`, `column_6`, `column_8`, `column_10`,
				`column_11`, `column_13`, `column_15`, `column_16`,
				`web_site`, `social_links`, `contact`, `main_location`, `location`, `map`,
				`date`, `time`, `admin_id`, `status`)
			VALUES ('$sub_cata', '$cata', '$name', '$description',
				'$dept_list', '$information', '$highlight', '$admission',
				'$admission_requirments', '$notice', '$course_details', '$cgpa_system',
				'$class_plan', '$exam_schadual', '$exam_hall', '$expense',
				'$web_site', '$social_links', '$main_contact', '$main_location', '$location', '$processedMap',
				 now(), now(), '$_SESSION[adminId]', 'not approved')";
	} else if ($cata == "health") {

		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`column_1`, `column_2`, `column_3`, `column_4`,
				`web_site`, `social_links`, `contact`, `main_location`, `location`, `map`,
				`date`, `time`, `admin_id`, `status`)
			VALUES ('$sub_cata', '$cata', '$name', '$description',
				 '$department', '$facilities', '$ambulance', '$opening_hour',
				'$web_site', '$social_links', '$main_contact', '$main_location', '$location', '$processedMap',
				 now(), now(), '$_SESSION[adminId]', 'not approved')";
	} else if ($cata == "electronics") {

		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`type`, `column_2`, `column_3`, `column_4`,
				`column_5`, `web_site`, `social_links`, `contact`,
				`date`, `time`, `admin_id`, `status`)
			VALUES ('$sub_cata', '$cata', '$name', '$description',
				'$product_type', '$services', '$warranty', '$servicing_center',
				'$help_line', '$web_site', '$social_links', '$main_contact',
				 now(), now(), '$_SESSION[adminId]', 'not approved')";

	} else if ($cata == "residence") {

		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`type`, `offer`, `column_2`, `column_3`,
				`web_site`, `social_links`, `contact`, `main_location`, `location`, `map`,
				`date`, `time`, `admin_id`, `status`)
			VALUES ('$sub_cata', '$cata', '$name', '$description',
				'$property_type', '$offer', '$size', '$rent',
				'$web_site', '$social_links', '$main_contact', '$main_location', '$location', '$processedMap',
				 now(), now(), '$_SESSION[adminId]', 'not approved')";
	} else if ($cata == "tourism") {

		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`offer`,
				`column_1`, `column_2`, `column_3`, `column_4`,
				`column_5`,  `column_7`, `column_8`,
				`column_9`, `column_10`,
				`web_site`, `social_links`, `contact`, `main_location`, `location`, `map`,
				`date`, `time`, `admin_id`, `status`)
			VALUES ('$sub_cata', '$cata', '$name', '$description',
				'$offer',
				'$area', '$visiting_hour', '$close_day', '$entry_fee',
				'$pakage',  '$travel_alert', '$how_to_go',
				'$distance_chart', '$history',
				'$web_site', '$social_links', '$main_contact', '$main_location', '$location', '$processedMap',
				 now(), now(), '$_SESSION[adminId]', 'not approved')";

	} else if ($cata == "ecommerce") {
		$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`type`, `offer`, `column_3`,  `column_4`,
				`column_5`,`column_6`, `web_site`, `social_links`, `contact`,
				`date`, `time`, `admin_id`, `status`)
				VALUES ('$sub_cata', '$cata', '$name', '$description',
					'$type', '$offer', '$speciality', '$program_schadual', '$sponsor',
					 '$opening_hour', '$web_site', '$social_links', '$main_contact',
					  now(), now(), '$_SESSION[adminId]', 'not approved')";

	} else if ($cata == "transport") {
		echo "<h3> IS NOT ACTIVE YET CHECK upload_data.php</h3>";
		/*
			$sql = "INSERT INTO `general_info`(
				`sub_cata`, `cata`, `name`, `description`,
				`column_1`, `column_2`, `column_3`, `column_4`,
				`column_5`, `column_6`, `column_7`, `column_8`,
				`column_9`, `column_10`, `column_11`, `column_12`,
				`column_13`, `column_14`, `column_15`, `column_16`,
				`web_site`, `main_location`, `location`, `map`,
				`date`, `time`)
			VALUES ('$sub_cata', '$cata', '$name', '$description',
				'$_POST[]', '$_POST[]', '$_POST[]', '$_POST[]',
				'$_POST[]', '$_POST[]', '$_POST[]', '$_POST[]',
				'$_POST[]', '$_POST[]', '$_POST[]', '$_POST[]',
				'$_POST[]', '$_POST[]', '$_POST[]', '$_POST[]',
				 now(), now())";
			*/
	} else if ($cata == "service") {
		echo "<h3> IS NOT ACTIVE YET CHECK upload_data.php</h3>";
	}
	echo "<br><div class='general_info_query'><h3>QUERY:  <span>" . $sql . "</span> </h3></div>";
	if ($conn->query($sql) === TRUE) {

		$last_content_id = $conn->insert_id;

		if (strlen($offer) > 6) {

			$offerDate = "UPDATE `general_info` SET `offer_s`= now() WHERE id='$last_content_id';";
			if ($conn->query($offerDate) === TRUE) {
				echo "<h4>Offer Date updated</h4>";
			}
		}

		echo '<br><div class="query_status"><h4>General information inserted successfully. </h4><h5> Insert  ID: ' . $last_content_id . '</h5></div><br>';

		//MULTIPLE MAP INSERTION
		echo "<div class='function_calling'><h5>insertMap($last_content_id, $cata, $sub_cata); calling to insert Map </h5></div>";
		insertMap($last_content_id, $cata, $sub_cata);

		//IMAGE INSERTION
		echo "<div class='function_calling'><h5>insert_title_image($last_content_id, $cata, $sub_cata, $name); calling to insert Title Image </h5></div>";
		insert_title_image($last_content_id, $cata, $sub_cata, $name);

		echo "<div class='function_calling'><h3>insert_main_image($last_content_id, $cata, $sub_cata, $name); calling to insert Main Image </h3></div>";
		insert_main_image($last_content_id, $cata, $sub_cata, $name);

		//ITEM INSERTION
		if ($cata == "electronics") {
			echo "<div class='function_calling'><h5>elecInsertItem($last_content_id, $cata, $sub_cata); calling to insert electronic product </h5></div>";
			elecInsertItem($last_content_id, $cata, $sub_cata);
		} else {
			echo "<div class='function_calling'><h5>insertItem($last_content_id, $cata, $sub_cata); calling to insert Item List</h5></div>";
			insertItem($last_content_id, $cata, $sub_cata);
		}

		//EDUCATOIN PDF FILE INSERTION
		if ($cata == "education") {
			echo "<div class='function_calling'><h5>uploadEducationPdf($last_content_id, $cata, $sub_cata); calling to insert PDF</h5></div>";
			uploadEducationPdf($last_content_id, $cata, $sub_cata);
		}

		if ($cata == "tourism") {
			//insert Nearby Hotel and Restaurant
			echo "<div class='function_calling'><h5>insertNearby($last_content_id); calling to insert Nearby Information</h5></div>";
			insertNearby($last_content_id);
		}
		echo "<div class='function_calling'><h5>insert_search_suggestion($name, 'business_profile', $cata, $sub_cata, $last_content_id); calling to insert search suggestion</h5></div>";
		insert_search_suggestion($name, "business_profile", $cata, $sub_cata, $last_content_id);

	} else {
		echo '<div class="general_info_error_status"><h3>General information is not inserted</h3>
		    		<h3> Query :</3> <span>' . $sql . '
		    		<br>
		    		Error: ' . $conn->error . ' </span>';

	}
	//header('Location: new/new_'.$_GET[cata].'.php?cata='.$_GET[cata].'&sub_cata='.$_GET[sub_cata].'&content_id='.$last_content_id.'&update_option=update');

	exit();

}
// general function

function insertNearby($cata_id) {

	$nearbyHotel = $_POST[hotel_nearby];
	$nearbyRestaurant = $_POST[restaurant_nearby];

	// echo "<h1>".$nearbyHotel[0]."-".$nearbyHotel[1]."-".$nearbyHotel[2]."-".$nearbyHotel[3]."-".$nearbyHotel[4]."</h1>";
	include 'data_connection.php';
	for ($i = 0; $i < 5; $i++) {

		$sql = "INSERT INTO `tourism_nearby`(`cata_id`) VALUES ($cata_id)";

		if ($conn->query($sql)) {
			echo '<h4>nearBy information inserted successfully  ID: ' . $conn->insert_id . '</h4>';
			echo "<br>";
		} else {
			echo '<h2 style="background:red;">nearby information is not inserted</h2> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
			echo "<br>";
		}

	}

	for ($i = 0; $i < 5; $i++) {

		if ($nearbyHotel[$i] > -1 && $nearbyHotel[$i] != "") {
			$sql = "UPDATE `tourism_nearby` SET `hotel_id`='$nearbyHotel[$i]' WHERE cata_id='$cata_id' LIMIT 1;";

			if ($conn->query($sql)) {
				echo '<h4>nearBy information  inserted successfully ID: ' . $conn->insert_id . '</h4>';
				echo "<br>";
			} else {
				echo '<h2 style="background:red;">nearBy information is not inserted</h2> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
				echo "<br>";
			}
		}
	}

	for ($i = 0; $i < 5; $i++) {

		if ($nearbyRestaurant[$i] > -1 && $nearbyRestaurant[$i] != "") {
			$sql = "UPDATE `tourism_nearby` SET `restaurant_id`='$nearbyRestaurant[$i]' WHERE cata_id='$cata_id' LIMIT 1;";
			if ($conn->query($sql)) {
				echo '<h4>nearBy information inserted successfully ID: ' . $conn->insert_id . '</h4>';
				echo "<br>";
			} else {
				echo '<h3 style="background:red;">nearBy information is not inserted</h3> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
				echo "<br>";
			}
		}
	}

}

function insertMap($last_content_id, $cata, $sub_cata) {
	include "data_connection.php";

	$mainLocation = arrayCharReplace($_POST[main_location]);
	$contact = arrayCharReplace($_POST[contact]);
	$fullAddress = arrayCharReplace($_POST[full_address]);
	$map = arrayCharReplace($_POST[map]);
	$i = 0;
	while (isset($mainLocation[$i])) {
		$processedMap = funcSplit($map[$i]);
		$insertMap = "INSERT INTO `map`(
				`cata_id`, `cata`, `sub_cata`, `location`, `contact`,
				`address`, `map`, `date`, `admin_id`)
			VALUES ('$last_content_id', '$cata', '$sub_cata', '$mainLocation[$i]', '$contact[$i]',
					'$fullAddress[$i]', '$processedMap', now(), '$_SESSION[adminId]')";

		if ($conn->query($insertMap) === TRUE) {
			echo '<div class="insert_func_res"><span> Map  inserted successfully <span></div>';
		} else {
			echo '<div class="error_status"><h3>Map insertion failed</h3>
		    		<h2> Query :</h2> <span>' . $sql . '
		    		<br>
		    		Error: ' . $conn->error . ' </span>';

		}

		$i = $i + 1;
	}
}

function funcSplit($tmpMap) {
	if ($tmpMap != null) {
		$store = explode("src", $tmpMap);

		$resultMap = $store[0] . "width=100% height=250px src" . $store[1];
		return $resultMap;
	} else {
		return "";
	}

}
// Insert Map

//if(isset($last_content_id))
function insert_title_image($last_content_id, $cata, $sub_cata, $content_name) {

	echo "<div class='title_img_insert'>";

	include "data_connection.php";
	// Your file
	// $file = 'C:\wamp\www\reelstubs\app\webroot\img\movies\0bf522bb76777ba43393b9be5552b263.jpg';
	$file = 'upload_crop/upload_pic/' . $_SESSION[title_image_name] . $_SESSION[title_image_type];
	echo "<span><b>Source File : </b>" . $file . "</span>";

	$content_name = str_replace("  ", " ", $content_name);
	$name = $content_name . "_" . $last_content_id . "_" . $cata . "_" . $sub_cata . $_SESSION[title_image_type];
	echo "<span><b>New File Name : </b>" . $name . "</span>";

	$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
	$name = str_replace($arrayName, "", $name);

	$name = str_replace("  ", " ", $name);
	$name = str_replace(" ", "_", $name);
	echo "<span><b>New File Name(filtered) : </b>" . $name . "</span>";
	$new = '../image/title_image/' . $name;
	$mImageNew = '../image/title_image/s_' . $name;
	echo "<span><b>File Destination : </b>" . $new . "</span>";

	unlink($new); //DELETE IMAGE

	// Write the contents back to a new file

	// $imageCopyStatus = resizeImage($file, $new, 7.5, 90);
	if (copy($file, $new)) {

		$imageCopyStatus = resizeImage($file, $mImageNew, 4, 90);

		echo "<div class='copy_img'> Title Image Copy successfully.</div>";

		$sql = "UPDATE `general_info` SET `image_link`='$name' WHERE  id='$last_content_id'";

		if ($conn->query($sql) === TRUE) {

			echo '<div class="insert_func_res"><span> Title Image SQL data inserted successfully. <h4> ID: ' . $conn->insert_id . '</h4> </span></div>';

		} else {
			echo '<div class="error_status"><h3>Title Image insertion failed</h3>
			    		<h2> Query :</h2> <span>' . $sql . '
			    		<br>
			    		Error: ' . $conn->error . ' </span>';
		}
	} else {
		echo "<div class='copy_img'> Image Copy failed. <br><b>source : </b><i>" . $file . "</i> <br> <b>Destination : </b><i>" . $new . "</i></div>";
	}

	unset($_SESSION[title_image_name]);
	unset($_SESSION[title_image_type]);
	echo "</div>";
}

function insert_main_image($last_content_id, $cata, $sub_cata, $content_name) {
	echo "<div class='title_img_insert'>";
	include 'data_connection.php';

	$j = 0;
	while (isset($_SESSION[main_image_name][$j]) && $_SESSION[main_image_type][$j] != null) {

		$insert_main_image_data = "INSERT INTO `general_image`(`cata_id`, `cata`, `sub_cata`, `date`, `time`, `admin_id`)
								VALUES ('$last_content_id', '$cata', '$sub_cata',  now(), now(), '$_SESSION[adminId]')";

		if ($conn->query($insert_main_image_data) === TRUE) {
			$main_image_id = $conn->insert_id;

			echo '<div class="insert_func_res"><span> Main Image ' . $j . ' data inserted successfully.  ID: ' . $conn->insert_id . '</span></div>';
		} else {
			echo '<div class="error_status"><h3> Main Image ' . $j . ' data insertion failed</h3>
		    		<h2> Query :</h2> <span>' . $sql . '
		    		<br>
		    		Error: ' . $conn->error . ' </span>';

		}

		$file = 'upload_crop/upload_pic/' . $_SESSION[main_image_name][$j] . $_SESSION[main_image_type][$j];
		echo "<span><b>Source File : </b>" . $file . "</span>";

		$name = $content_name . "_" . $main_image_id . "_" . $cata . "_" . $last_content_id . "_" . $sub_cata . $_SESSION[main_image_type][$j];
		echo "<span><b>New File Name : </b>" . $name . "</span>";

		$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
		$name = str_replace($arrayName, "", $name);

		$name = str_replace("  ", " ", $name);
		$name = str_replace(" ", "_", $name);
		echo "<span><b>New File Name(filtered) : </b>" . $name . "</span>";

		$new = '../image/main_image/' . $name;
		$mImageNew = '../image/main_image/s_' . $name;
		echo "<span><b>File Destination : </b>" . $new . "</span>";

		// $imageCopyStatus = resizeImage($file, $new, 8, 90);
		if (copy($file, $new)) {

			$imageCopyStatus = resizeImage($file, $mImageNew, 4.5, 90);
			echo "<div class='copy_img'> Main Image Copy successfully.</div>";

			$sql = "UPDATE `general_image` SET `image_link`='$name' WHERE  id='$main_image_id'";
			if ($conn->query($sql) === TRUE) {
				echo '<div class="insert_func_res"><span> Main Image  No' . $j . ' SQL data inserted successfully.  ID: ' . $conn->insert_id . '</span></div>';
			} else {
				echo '<div class="error_status"><h3> Main Image ' . $j . ' name insertion failed</h3>
			    		<h2> Query :</h2> <span>' . $sql . '
			    		<br>
			    		Error: ' . $conn->error . ' </span>';

			}
		} else {
			echo "<div class='copy_img'> Image Copy failed. <br><b>source : </b><i>" . $file . "</i> <br> <b>Destination : </b><i>" . $new . "</i></div>";
		}

		$j++;

	}
	unset($_SESSION[main_image_name]);
	unset($_SESSION[main_image_type]);
	echo "</div>";
}

function insertItem($last_content_id, $cata, $sub_cata) {
	include 'data_connection.php';
	echo "<div class='totla_item_insert'>";
	echo "<div class='item_insert'><br><h3>  Insert Item informaion...... </h3></div>";
	$i = 0;
	$item_name = arrayCharReplace($_POST[item_name]);
	$item_price = arrayCharReplace($_POST[item_price]);
	$item_des = arrayCharReplace($_POST[item_description]);

	$item_color = arrayCharReplace($_POST[item_color]);
	$item_size = arrayCharReplace($_POST[item_size]);

	if ($cata == "education") {
		$item_name = arrayCharReplace($_POST[teacher_name]);
	} else if ($cata == "health") {
		$item_name = arrayCharReplace($_POST[doctor_name]);
	}

	$food_filter = arrayCharReplace($_POST[food_filter]);
	$food_filter_b = arrayCharReplace($_POST[food_filter_b]);
	// food_filter_b is used, when need to insert a filter that is not exist in filter list

	// life Style filter
	$lifeStyleFilter = arrayCharReplace($_POST[lifeStyleFilter]);
	$FCOUNT = count($lifeStyleFilter);

	$life_style_filter1_b = arrayCharReplace($_POST[life_style_filter1_b]);
	$life_style_sub_filter_b = arrayCharReplace($_POST[life_style_sub_filter_b]);

	//Education Filter
	$teacher_filter = arrayCharReplace($_POST[teacher_filter]);
	$teacher_filter_b = arrayCharReplace($_POST[teacher_filter_b]);

	//Doctor Filter
	$doctor_filter = arrayCharReplace($_POST[doctor_filter]);
	$doctor_filter_b = arrayCharReplace($_POST[doctor_filter_b]);

	//residence
	$attraction_type = arrayCharReplace($_POST[attraction_type]);
	$bed_type = arrayCharReplace($_POST[bed_type]);
	$view = arrayCharReplace($_POST[view]);
	$rate1 = arrayCharReplace($_POST[price1]);
	$rate2 = arrayCharReplace($_POST[price2]);
	$facilities = arrayCharReplace($_POST[facilities]);

	while (isset($item_name[$i])) {
		echo "<div class='item_inserting'> <span>Item <b>" . $item_name[$i] . " </b> Inserting </span></div>";
		if ($cata == "food") {
			if (empty($food_filter[$i])) {
				if (empty($food_filter_b[$i])) {
					$food_filter[$i] = "";
				} else {
					$food_filter[$i] = $food_filter_b[$i];
				}
			}

			$item_input = "INSERT INTO `item_list`(
					`cata`, `cata_id`, `sub_cata`, `name`,
					`description`,  `price`, `filter_1`,
					`date`, `time`,`admin_id`)
				VALUES ('$cata', '$last_content_id', '$sub_cata', '$item_name[$i]',
					'$item_des[$i]' , '$item_price[$i]', '$food_filter[$i]',
					 now(), now(), '$_SESSION[adminId]')";

		} else if ($cata == "life style") {

			$tmp = "lifeStyle" . $lifeStyleFilter[$i] . "Filter";
			$lifeStyleFilterLevel2 = $_POST[$tmp];

			$lifeStyleFilterTmp = $lifeStyleFilter[$i];
			$lifeStyleSubFilterTmp = $lifeStyleFilterLevel2[$i];

			if (!isset($lifeStyleFilterTmp) || $lifeStyleFilterTmp == "" || $lifeStyleFilterTmp == null) {
				$lifeStyleFilterTmp = $life_style_filter1_b[$i];
			}

			if (!isset($lifeStyleSubFilterTmp) || $lifeStyleSubFilterTmp == "" || $lifeStyleSubFilterTmp == null) {
				$lifeStyleSubFilterTmp = $life_style_sub_filter_b[$i];
			}

			echo "<span>life style Insert With New Filter >>" . $FCOUNT . "</span>";
			$item_input = "INSERT INTO `item_list`(
					`cata`, `cata_id`, `sub_cata`, `name`,
					`description`,  `price`, `color`, `size`, `filter_1`,  `filter_2`,
					`date`, `time`, `admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$item_name[$i]',
					'$item_des[$i]' , '$item_price[$i]', '$item_color[$i]', '$item_size[$i]', '$lifeStyleFilterTmp', '$lifeStyleSubFilterTmp',
					 now(), now(), '$_SESSION[adminId]')";
		} else if ($cata == "education") {
			$designation = $_POST[designation];

			$education = $_POST[education];
			$email = $_POST[email];
			$phone = $_POST[phone];

			if (empty($teacher_filter[$i])) {
				$depertment = $teacher_filter_b[$i];
			} else {
				$depertment = $teacher_filter[$i];
			}

			$item_input = "INSERT INTO `doctor_faculty`(
				`cata`, `cata_id`, `sub_cata`, `name`,
				`designation`,  `depertment`, `education`, `email`,`mobile`,
				`date`, `time`, `admin_id`)
				VALUES ('$cata', '$last_content_id', '$sub_cata', '$item_name[$i]',
				'$designation[$i]',  '$depertment', '$education[$i]', '$email[$i]', '$phone[$i]',
				 now(), now(), '$_SESSION[adminId]')";
		} else if ($cata == "health") {
			$designation = $_POST[designation];
			$schedule = $_POST[item_schedule];
			$education = $_POST[education];
			$email = $_POST[email];
			$phone = $_POST[phone];

			if (empty($doctor_filter[$i])) {
				$depertment = $doctor_filter_b[$i];
			} else {
				$depertment = $doctor_filter[$i];
			}

			$item_input = "INSERT INTO `doctor_faculty`(
				`cata`, `cata_id`, `sub_cata`, `name`,
				`designation`, `schedule`, `depertment`, `education`, `email`,`mobile`,
				`date`, `time`, `admin_id`)
				VALUES ('$cata', '$last_content_id', '$sub_cata', '$item_name[$i]',
				'$designation[$i]', '$schedule[$i]', '$depertment', '$education[$i]', '$email[$i]', '$phone[$i]',
				 now(), now(), '$_SESSION[adminId]')";

		} else if ($cata == "electronics") {
			echo "<h3> Electronics Item will be inserted in another function, elecInsertItem</h3>";
		} else if ($cata == "residence") {

			$item_input = "INSERT INTO `item_list`(
					`cata`, `cata_id`, `sub_cata`, `name`,
					`description`, `price`, `price_2`, `column_1`, `column_2`, `column_3`,
					 `filter_1`, `date`, `time`, `admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$item_name[$i]',
					'$item_des[$i]', '$rate1[$i]', '$rate2[$i]', '$bed_type[$i]', '$view[$i]', '$facilities[$i]',
					'$attraction_type[$i]', now(), now(), '$_SESSION[adminId]')";
		} else if ($cata == "tourism") {
			$item_input = "INSERT INTO `item_list`(
					`cata`, `cata_id`, `sub_cata`, `name`,
					`description`, `date`, `time`, `admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$item_name[$i]',
					'$item_des[$i]', now(), now(), '$_SESSION[adminId]')";
		} else if ($cata == "ecommerce") {

			$tmp = "lifeStyle" . $lifeStyleFilter[$i] . "Filter";
			$lifeStyleFilterLevel2 = $_POST[$tmp];

			$lifeStyleFilterTmp = $lifeStyleFilter[$i];
			$lifeStyleSubFilterTmp = $lifeStyleFilterLevel2[$i];

			if (!isset($lifeStyleFilterTmp) || $lifeStyleFilterTmp == "" || $lifeStyleFilterTmp == null) {
				$lifeStyleFilterTmp = $life_style_filter1_b[$i];
			}

			if (!isset($lifeStyleSubFilterTmp) || $lifeStyleSubFilterTmp == "" || $lifeStyleSubFilterTmp == null) {
				$lifeStyleSubFilterTmp = $life_style_sub_filter_b[$i];
			}

			echo "<span>E-Commerce Insert With New Filter >>" . $FCOUNT . "</span>";

			$item_input = "INSERT INTO `item_list`(
					`cata`, `cata_id`, `sub_cata`, `name`,
					`description`,  `price`, `color`, `size`, `filter_1`,  `filter_2`,
					`date`, `time`, `admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$item_name[$i]',
					'$item_des[$i]' , '$item_price[$i]', '$item_color[$i]', '$item_size[$i]', '$lifeStyleFilterTmp', '$lifeStyleSubFilterTmp',
					 now(), now(), '$_SESSION[adminId]')";
		} else if ($cata == "transport") {
			echo "<h3> IS NOT ACTIVE YET CHECK upload_data.php</h3>";
		} else if ($cata == "service") {
			echo "<h3> IS NOT ACTIVE YET CHECK upload_data.php</h3>";
		}

		if ($conn->query($item_input) === TRUE) {

			$last_item_id = $conn->insert_id;
			echo '<div class="insert_func_res"><span> Item No ' . $last_item_id . ' inserted successfully <span></div>';

			insertItemImage($last_item_id, $i, $last_content_id, $cata, $sub_cata, $item_name);
			if ($cata == "education" || $cata == "health") {
				insert_search_suggestion($item_name[$i], "item_doc_fac", $cata, $sub_cata, $last_item_id);
			} else {
				insert_search_suggestion($item_name[$i], "item", $cata, $sub_cata, $last_item_id);
			}

		} else {
			echo '<div class="error_status"><h3>Item insertion failed</h3>
					<h2> Query :</h2> <span>' . $sql . '
					<br>
					Error: ' . $conn->error . ' </span>';

		}

		$i = $i + 1;
	}
	echo "</div>";
	////header('Location: new/'.$_GET[cata].'.php?cata='.$_GET[cata].'&sub_cata='.$_GET[sub_cata].'&content_id='.$last_content_id.'&update_option=update');
}

function resizeImage($sourceImage, $targetImage, $ratio, $quality) {
	$status = false;
	// Get dimensions of source image.
	list($origWidth, $origHeight) = getimagesize($sourceImage);

	if ($maxWidth == 0) {
		$maxWidth = $origWidth / 10;
		$maxWidth = $maxWidth * $ratio;
	}

	if ($maxHeight == 0) {
		$maxHeight = $origHeight / 10;
		$maxHeight = $maxHeight * $ratio;
	}

	// Calculate ratio of desired maximum sizes and original sizes.
	$widthRatio = $maxWidth / $origWidth;
	$heightRatio = $maxHeight / $origHeight;

	// Ratio used for calculating new image dimensions.
	$ratio = min($widthRatio, $heightRatio);

	// Calculate new image dimensions.
	$newWidth = (int) $origWidth * $ratio;
	$newHeight = (int) $origHeight * $ratio;

	// Obtain image from given source file.
	if (strpos($sourceImage, '.jpg') !== false) {
		if (!$image = @imagecreatefromjpeg($sourceImage)) {
			return false;
		}
		// Create final image with new dimensions.
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagejpeg($newImage, $targetImage, $quality);
		$status = true;
	} else if (strpos($sourceImage, '.png') !== false) {
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		$source = imagecreatefrompng($sourceImage);
		imagecopyresized($newImage, $source, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagepng($newImage, $targetImage);
		$status = true;
	} else if (strpos($sourceImage, '.jpeg') !== false) {
		if (!$image = @imagecreatefromjpeg($sourceImage)) {
			return false;
		}
		// Create final image with new dimensions.
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagejpeg($newImage, $targetImage, $quality);
		$status = true;
	}

	// Free up the memory.
	imagedestroy($image);
	imagedestroy($newImage);

	imagedestroy($source);

	return $status;
}

function insertItemImage($last_item_id, $i, $last_content_id, $cata, $sub_cata, $item_name_list) {
	echo "<div class='title_img_insert'>";
	include "data_connection.php";
	// Your file
	// $file = 'C:\wamp\www\reelstubs\app\webroot\img\movies\0bf522bb76777ba43393b9be5552b263.jpg';

	$item_name = $item_name_list[$i];

	if (!isset($item_name) || $item_name == "" || $item_name == null) {
		if ($cata == "education" || $cata == "health") {
			$getItemName = "SELECT name FROM doctor_faculty WHERE id = '$last_item_id';";
			$res = $conn->query($getItemName);
			if ($res->num_rows > 0) {
				while ($row = $res->fetch_assoc()) {
					$item_name = $row[name];
				}
			}
		} else if ($cata == "electronics") {
			$getItemName = "SELECT name FROM elec_item_details WHERE id = '$last_item_id';";
			$res = $conn->query($getItemName);
			if ($res->num_rows > 0) {
				while ($row = $res->fetch_assoc()) {
					$item_name = $row[name];
				}
			}
		} else {
			$getItemName = "SELECT name FROM item_list WHERE id = '$last_item_id';";
			$res = $conn->query($getItemName);
			if ($res->num_rows > 0) {
				while ($row = $res->fetch_assoc()) {
					$item_name = $row[name];
				}
			}
		}
	}

	$file = 'upload_crop/upload_pic/' . $_SESSION[item_image_name][$i] . $_SESSION[item_image_type][$i];
	echo "<span><b>Source File : </b>" . $file . "</span>";

	$name = $item_name . "_" . $last_item_id . "_" . $last_content_id . "_" . $cata . "_" . $sub_cata . $_SESSION[item_image_type][$i];
	echo "<span><b>New File Name : </b>" . $name . "</span>";

	$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
	$name = str_replace($arrayName, "", $name);

	$name = str_replace("  ", " ", $name);
	$name = str_replace(" ", "_", $name);
	echo "<span><b>New File Name(filtered) : </b>" . $name . "</span>";

	$new = '../image/item_image/' . $name;
	echo "<span><b>File Destination : </b>" . $new . "</span>";

	// Write the contents back to a new file

	if (copy($file, $new)) {
		echo "<div class='copy_img'> Item Image Copy successfully.</div>";

		if ($cata == "education" || $cata == "health") {
			$sql = "UPDATE `doctor_faculty` SET `image_link`='$name' WHERE  id='$last_item_id'";
		} else if ($cata == "electronics") {
			$sql = "UPDATE `elec_item_details` SET `image_link`='$name' WHERE  id='$last_item_id'";
		} else {
			$sql = "UPDATE `item_list` SET `image_link`='$name' WHERE  id='$last_item_id'";
		}

		if ($conn->query($sql) === TRUE) {

			$mImageNew = '../image/item_image/s_' . $name;
			$imageCopyStatus = resizeImage($file, $mImageNew, 2, 90);

			echo '<div class="insert_func_res"><span> Item Image No ' . $j . ' SQL data inserted successfully. <b> ID: ' . $conn->insert_id . '</b> </span></div>';
		} else {
			echo '<div class="error_status"><h3> Item Image ' . $j . ' name insertion failed</h3>
			    		<h2> Query :</h2> <span>' . $sql . '
			    		Error: ' . $conn->error . ' </span>';

		}

	} else {
		echo "<div class='copy_img'> Image Copy failed. <br><b>source : </b><i>" . $file . "</i> <br> <b>Destination : </b><i>" . $new . "</i></div>";
	}

	$j = 0;
	echo '<div class="multi_img">';
	while (isset($_SESSION[item_multi_image_name][$i][$j]) && $_SESSION[item_multi_image_type][$i][$j] != null) {
		$file = 'upload_crop/upload_pic/' . $_SESSION[item_multi_image_name][$i][$j] . $_SESSION[item_multi_image_type][$i][$j];
		echo "<span><b>Multi Img Source File : </b>" . $file . "</span>";

		$lastMultiId;
		$getLastItemId = "SELECT * FROM item_multi_image ORDER BY id DESC LIMIT 1;";
		$getLastItemId = $conn->query($getLastItemId);
		if ($getLastItemId->num_rows > 0) {
			while ($row = $getLastItemId->fetch_assoc()) {
				$lastMultiId = $row[id] + 1;
			}
		}

		$name = $item_name . "_" . $lastMultiId . "_" . $last_item_id . "_" . $last_content_id . "_" . $cata . "_" . $sub_cata . $_SESSION[item_multi_image_type][$i][$j];
		echo "<span><b>New File Name : </b>" . $name . "</span>";

		$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
		$name = str_replace($arrayName, "", $name);

		$name = str_replace("  ", " ", $name);
		$name = str_replace(" ", "_", $name);
		echo "<span><b>New File Name(filtered) : </b>" . $name . "</span>";

		$new = '../image/item_image/item_multi_image/' . $name;
		echo "<span><b>File Destination : </b>" . $new . "</span>";

		// Write the contents back to a new file
		// $imageCopyStatus = resizeImage($file, $new, 9, 90);
		if (copy($file, $new)) {
			echo "<div class='copy_img'> Item Image Copy successfully.</div>";

			$sql = "INSERT INTO `item_multi_image`(`item_id`, `image_link`) VALUES ('$last_item_id','$name')";
			if ($conn->query($sql)) {

				$mImageNew = '../image/item_image/item_multi_image/s_' . $name;
				$imageCopyStatus = resizeImage($file, $mImageNew, 2, 90);

				echo '<div class="insert_func_res"><span> Main Image No ' . $j . ' SQL data inserted successfully. <b> ID: ' . $conn->insert_id . '</b> </span></div>';

			} else {
				echo '<div class="error_status"><h3> Main Image ' . $j . ' name insertion failed</h3>
			    		<h2> Query :</h2> <span>' . $sql . '
			    		<br>
			    		Error: ' . $conn->error . ' </span>';

			}

		} else {
			echo "<div class='copy_img'> Image Copy failed. <br><b>source : </b><i>" . $file . "</i> <br> <b>Destination : </b><i>" . $new . "</i></div>";
		}

		unset($_SESSION[item_multi_image_name][$i][$j]);
		unset($_SESSION[item_multi_image_type][$i][$j]);

		$j++;
	}
	echo '</div>';
	unset($_SESSION[item_image_name][$i]);
	unset($_SESSION[item_image_type][$i]);
	echo '</div>';
}

function uploadEducationPdf($last_content_id, $cata, $sub_cata) {
	echo "<h1>";
	include "data_connection.php";

	$target_dir = "../image/education_pdf/";
	if (!empty($_FILES['notice_file'])) {
		$file = $_FILES['notice_file'];
		$pdf_file_name = $file['name'];
		$file_ext = explode('.', $pdf_file_name);
		$file_ext = strtolower(end($file_ext));
		$target_file = $target_dir . $last_content_id . "_notice." . $file_ext;

		if (move_uploaded_file($_FILES["notice_file"]["tmp_name"], $target_file)) {
			//$sql = "INSERT INTO `general_info`(`column_7`)VALUES ('$target_file') WHERE id = '$last_content_id'";
			$file_name_url = str_replace("../", "", $target_file);
			$sql = "UPDATE `general_info` set `column_7`='$file_name_url' WHERE id = '$last_content_id'";

			if ($conn->query($sql) === TRUE) {
				echo '<h2>Notice PDF inserted successfully</h2><h4> ID: ' . $conn->insert_id . '</h4>';
				echo "<br>";
			} else {
				echo '<h2 style="background:red;">Notice PDF is not inserted</h2> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
				echo "<br>";
			}
		} else {echo "Sorry, there was an error uploading your Notice pdf. ";}
	}

	if (!empty($_FILES['course_details_file'])) {
		$file = $_FILES['course_details_file'];
		$pdf_file_name = $file['name'];
		$file_ext = explode('.', $pdf_file_name);
		$file_ext = strtolower(end($file_ext));
		$target_file = $target_dir . $last_content_id . "_course_details." . $file_ext;

		if (move_uploaded_file($_FILES["course_details_file"]["tmp_name"], $target_file)) {

			$file_name_url = str_replace("../", "", $target_file);
			$sql = "UPDATE `general_info` set `column_9`='$file_name_url' WHERE id = '$last_content_id'";

			if ($conn->query($sql) === TRUE) {
				echo '<h2>Course Details PDF inserted successfully</h3><h4> ID: ' . $conn->insert_id . '</h3>';
				echo "<br>";
			} else {
				echo '<h2 style="background:red;">Course Details PDF is not inserted</h2> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
				echo "<br>";
			}
		} else {echo "Sorry, there was an error uploading your course_details_file. ";}
	}

	if (!empty($_FILES['class_plan_file'])) {
		$file = $_FILES['class_plan_file'];
		$pdf_file_name = $file['name'];
		$file_ext = explode('.', $pdf_file_name);
		$file_ext = strtolower(end($file_ext));
		$target_file = $target_dir . $last_content_id . "_class_plan." . $file_ext;

		if (move_uploaded_file($_FILES["class_plan_file"]["tmp_name"], $target_file)) {

			$file_name_url = str_replace("../", "", $target_file);
			$sql = "UPDATE `general_info` set `column_12`='$file_name_url' WHERE id = '$last_content_id'";

			if ($conn->query($sql) === TRUE) {
				echo '<h2>Class plan Details PDF inserted successfully</h2><h4> ID: ' . $conn->insert_id . '';
				echo "<br><br><br>";
			} else {
				echo '<h2 style="background:red;">Class plan PDF is not inserted</h2> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
				echo "<br><br><br>";
			}
		} else {echo "Sorry, there was an error uploading your class_plan_file pdf. ";}
	}

	if (!empty($_FILES['exam_schedule_file'])) {
		$file = $_FILES['exam_schedule_file'];
		$pdf_file_name = $file['name'];
		$file_ext = explode('.', $pdf_file_name);
		$file_ext = strtolower(end($file_ext));
		$target_file = $target_dir . $last_content_id . "_exam_schedule." . $file_ext;

		if (move_uploaded_file($_FILES["exam_schedule_file"]["tmp_name"], $target_file)) {

			$file_name_url = str_replace("../", "", $target_file);
			$sql = "UPDATE `general_info` set `column_14`='$file_name_url' WHERE id = '$last_content_id'";

			if ($conn->query($sql) === TRUE) {
				echo '<h2>Exam Schedule Details PDF inserted successfully</h2><h4> ID: ' . $conn->insert_id . '';
				echo "<br><br><br>";
			} else {
				echo '<h2 style="background:red;">Exam Schedule plan PDF is not inserted</h2> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
				echo "<br><br><br>";
			}
		} else {echo "Sorry, there was an error uploading your exam_schedule_file pdf. ";}
	}

	echo "<h1>";

} // function uploadEducationPdf

function elecInsertItem($last_content_id, $cata, $sub_cata) {
	include "data_connection.php";
	echo "<h1> ELECTRONICS ITEM INSERT FUNCTION    ";
	echo $_POST[what_item];
	$i = 0;
	$what_item = $_POST[what_item];
	$item_name = $_POST[item_name];
	while (isset($item_name[$i])) {
		if ($what_item[$i] == "mobile") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$sim = $_POST[sim];
			$ant = $_POST[ant];
			$usb = $_POST[usb];
			$earjack = $_POST[earjack];
			$mhl = $_POST[mhl];

			$wifi = $_POST[wifi];
			$bluetooth = $_POST[bluetooth];
			$os = $_POST[os];
			$tech_size = $_POST[tech_size];
			$res_color = $_POST[res_color];

			$pen = $_POST[pen];
			$cpu = $_POST[cpu];
			$ram = $_POST[ram];
			$rear_cam = $_POST[rear_cam];
			$front_cam = $_POST[front_cam];

			$flash = $_POST[flash];
			$sensor = $_POST[sensor];
			$dimension_weight = $_POST[dimension_weight];
			$bat_capacity = $_POST[bat_capacity];
			$internet_usage_time = $_POST[internet_usage_time];

			$talk_time = $_POST[talk_time];
			$removable = $_POST[removable];
			$video_format_resolution = $_POST[video_format_resolution];
			$audio_format = $_POST[audio_format];
			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`, `column_13`, `column_14`, `column_15`,
						 `column_16`, `column_17`, `column_18`, `column_19`, `column_20`,
						 `column_21`, `column_22`, `column_23`, `column_24`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$sim[$i]', '$ant[$i]', '$usb[$i]', '$earjack[$i]', '$mhl[$i]',
						 '$wifi[$i]', '$bluetooth[$i]', '$os[$i]', '$tech_size[$i]', '$res_color[$i]',
						  '$pen[$i]', '$cpu[$i]', '$ram[$i]', '$rear_cam[$i]', '$front_cam[$i]',
						   '$flash[$i]', '$sensor[$i]', '$dimension_weight[$i]', '$bat_capacity[$i]', '$internet_usage_time[$i]',
						    '$talk_time[$i]', '$removable[$i]', '$video_format_resolution[$i]', '$audio_format[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "laptop") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$processor = $_POST[processor];
			$clock_speed = $_POST[clock_speed];
			$cache = $_POST[cache];
			$display = $_POST[display];

			$ram = $_POST[ram];
			$storage = $_POST[storage];
			$graphics = $_POST[graphics];
			$optical_device = $_POST[optical_device];
			$display_port = $_POST[display_port];

			$audio_port = $_POST[audio_port];
			$usb = $_POST[usb];
			$battery = $_POST[battery];
			$backup = $_POST[backup];
			$os = $_POST[os];

			$weight = $_POST[weight];
			$network = $_POST[network];
			$web_cam = $_POST[web_cam];
			$finger_print = $_POST[finger_print];
			$color = $_POST[color];

			$other = $_POST[other];

			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`, `column_13`, `column_14`, `column_15`,
						 `column_16`, `column_17`, `column_18`, `column_19`, `column_20`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						 '$processor[$i]', '$clock_speed[$i]', '$cache[$i]', '$display[$i]',
						'$ram[$i]', '$storage[$i]', '$graphics[$i]', '$optical_device[$i]', '$display_port[$i]',
						'$audio_port[$i]', '$usb[$i]', '$battery[$i]', '$backup[$i]', '$os[$i]',
						'$weight[$i]', '$network[$i]', '$web_cam[$i]', '$finger_print[$i]', '$color[$i]',
						'$other[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "computer") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$processor = $_POST[processor];
			$clock_speed = $_POST[clock_speed];
			$cache = $_POST[cache];
			$mother_board = $_POST[mother_board];

			$display = $_POST[display];
			$ram = $_POST[ram];
			$storage = $_POST[storage];
			$graphics = $_POST[graphics];
			$optical_device = $_POST[optical_device];

			$display_port = $_POST[display_port];
			$Speaker = $_POST[Speaker];
			$audio_port = $_POST[audio_port];
			$keyboard = $_POST[keyboard];
			$mouse = $_POST[mouse];

			$usb = $_POST[usb];
			$os = $_POST[os];
			$network = $_POST[network];
			$turbo_boost = $_POST[turbo_boost];
			$other = $_POST[other];
			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`, `column_13`, `column_14`, `column_15`,
						 `column_16`, `column_17`, `column_18`, `column_19`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$processor[$i]','$clock_speed[$i]','$cache[$i]','$mother_board[$i]',
						'$display[$i]','$ram[$i]','$storage[$i]','$graphics[$i]','$optical_device[$i]',
						'$display_port[$i]','$Speaker[$i]','$audio_port[$i]','$keyboard[$i]','$mouse[$i]',
						'$usb[$i]','$os[$i]','$network[$i]','$turbo_boost[$i]','$other[$i]',
						 now(), now(), '$_SESSION[adminId]')";
		} else if ($what_item[$i] == "processor") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$model = $_POST[model];
			$generation = $_POST[generation];
			$cores = $_POST[cores];
			$threads = $_POST[threads];
			$clock_speed = $_POST[clock_speed];
			$max_turbo_frequency = $_POST[max_turbo_frequency];
			$cache = $_POST[cache];
			$instruction_set = $_POST[instruction_set];
			$lithography = $_POST[lithography];
			$max_memory_size = $_POST[max_memory_size];
			$turbo_boost_techonology = $_POST[turbo_boost_techonology];
			$hyper_threading_techonology = $_POST[hyper_threading_techonology];
			$trusted_execution_techonology = $_POST[trusted_execution_techonology];
			$integreted_graphics = $_POST[integreted_graphics];
			$socket_support = $_POST[socket_support];

			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`, `column_13`, `column_14`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$generation[$i]', '$cores[$i]', '$threads[$i]', '$clock_speed[$i]',
						'$max_turbo_frequency[$i]', '$cache[$i]', '$instruction_set[$i]', '$lithography[$i]', '$max_memory_size[$i]',
						'$turbo_boost_techonology[$i]', '$hyper_threading_techonology[$i]', '$trusted_execution_techonology[$i]', '$integreted_graphics[$i]', '$socket_support[$i]',
						 now(), now(), '$_SESSION[adminId]')";
		} else if ($what_item[$i] == "mother_board") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$form_factor = $_POST[form_factor];
			$socket = $_POST[socket];
			$chipset = $_POST[chipset];
			$supported_cpu = $_POST[supported_cpu];
			$ram_type_size = $_POST[ram_type_size];
			$ram_bus = $_POST[ram_bus];
			$ram_slot = $_POST[ram_slot];
			$pci_slot = $_POST[pci_slot];
			$sata_port = $_POST[sata_port];
			$lan_speed = $_POST[lan_speed];
			$usb_port = $_POST[usb_port];
			$bios = $_POST[bios];
			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`, `column_13`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$form_factor[$i]', '$socket[$i]', '$chipset[$i]',
						'$supported_cpu[$i]', '$ram_type_size[$i]', '$ram_bus[$i]', '$ram_slot[$i]', '$pci_slot[$i]',
						'$sata_port[$i]', '$lan_speed[$i]', '$usb_port[$i]', '$bios[$i]',
						now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "graphics_card") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$chipset = $_POST[chipset];
			$interface = $_POST[interfaces];
			$gpu_clock = $_POST[gpu_clock];
			$memory_and_type = $_POST[memory_and_type];
			$memory_bus = $_POST[memory_bus];
			$memory_clock = $_POST[memory_clock];
			$resolution = $_POST[resolution];
			$directx = $_POST[directx];
			$port = $_POST[port];
			$power = $_POST[power];
			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						  `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$chipset[$i]', '$interface[$i]', '$gpu_clock[$i]', '$memory_and_type[$i]',
						'$memory_bus[$i]', '$memory_clock[$i]', '$resolution[$i]', '$directx[$i]', '$port[$i]',
						'$power[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "ram") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$for = $_POST[for_lap_desk];
			$memory_type = $_POST[memory_type];
			$bus_speed = $_POST[bus_speed];
			$pin_number = $_POST[pin_number];
			$heat_sink = $_POST[heat_sink];

			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$for[$i]', '$memory_type[$i]', '$bus_speed[$i]',
						'$pin_number[$i]', '$heat_sink[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "storage") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$position_type = $_POST[position_type];
			$storage_type = $_POST[storage_type];
			$for = $_POST[for_lap_desk];
			$storage = $_POST[storage];
			$rpm = $_POST[rpm];
			$seek_time = $_POST[seek_time];
			$buffer = $_POST[buffer];
			$from_factor = $_POST[from_factor];
			$transfer_rate = $_POST[transfer_rate];

			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						  `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$position_type[$i]', '$storage_type[$i]', '$for[$i]','$storage[$i]',
						'$rpm[$i]', '$seek_time[$i]', '$buffer[$i]', '$from_factor[$i]', '$transfer_rate[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "monitor") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$type = $_POST[type];
			$type = $_POST[type];
			$size = $_POST[size];
			$screen_type = $_POST[screen_type];
			$resolution = $_POST[resolution];
			$contrast_ratio = $_POST[contrast_ratio];
			$viewing_angel = $_POST[viewing_angel];
			$brightness = $_POST[brightness];
			$port = $_POST[port];
			$power_adapter = $_POST[power_adapter];
			$voltage = $_POST[voltage];
			$response_time = $_POST[response_time];
			$refresh_rate = $_POST[refresh_rate];
			$dimension = $_POST[dimension];
			$weight = $_POST[weight];
			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`, `column_13`, `column_14`, `column_15`,
						 `column_16`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$type[$i]', '$type[$i]', '$size[$i]',
						'$screen_type[$i]', '$resolution[$i]', '$contrast_ratio[$i]', '$viewing_angel[$i]', '$brightness[$i]',
						'$port[$i]', '$power_adapter[$i]', '$voltage[$i]', '$response_time[$i]', '$refresh_rate[$i]',
						'$dimension[$i]', '$weight[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "camera") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$type = $_POST[type];
			$image_sensor = $_POST[image_sensor];
			$processor = $_POST[processor];
			$auto_focus = $_POST[auto_focus];
			$iso = $_POST[iso];
			$shutter = $_POST[shutter];
			$flash = $_POST[flash];
			$resolution = $_POST[resolution];
			$lens = $_POST[lens];
			$weight = $_POST[weight];
			$sensor_type = $_POST[sensor_type];
			$display = $_POST[display];
			$video = $_POST[video];
			$interface = $_POST[interfaces];
			$memory = $_POST[memory];
			$battery = $_POST[battery];
			$dimension = $_POST[dimension];
			$light_sensor = $_POST[light_sensor];
			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`, `column_13`, `column_14`, `column_15`,
						 `column_16`, `column_17`, `column_18`, `column_19`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$type[$i]', '$image_sensor[$i]', '$processor[$i]',
						'$auto_focus[$i]', '$iso[$i]', '$shutter[$i]', '$flash[$i]', '$resolution[$i]',
						'$lens[$i]', '$weight[$i]', '$sensor_type[$i]', '$display[$i]', '$video[$i]',
						'$interface[$i]', '$memory[$i]', '$battery[$i]', '$dimension[$i]', '$light_sensor[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "optical_drive") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$type = $_POST[type];
			$intreface = $_POST[intreface];
			$buffer = $_POST[buffer];
			$read_speed = $_POST[read_speed];
			$form_factor = $_POST[form_factor];
			$access_time = $_POST[access_time];
			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$type[$i]', '$intreface[$i]', '$buffer[$i]', '$read_speed[$i]',
						'$form_factor[$i]', '$access_time[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "power_supply") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$maximum_power = $_POST[maximum_power];
			$input_voltage = $_POST[input_voltage];
			$input_current = $_POST[input_current];
			$oprating_temperature = $_POST[oprating_temperature];
			$short_description = $_POST[short_description];

			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$maximum_power[$i]', '$input_voltage[$i]', '$input_current[$i]', '$oprating_temperature[$i]',
	 					'$short_description[$i]',
						 now(), now(), '$_SESSION[adminId]')";

		} else if ($what_item[$i] == "mouse") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$interface = $_POST[interfaces];
			$num_of_buttons = $_POST[num_of_buttons];
			$wheel = $_POST[wheel];
			$dpi = $_POST[dpi];
			$dimension = $_POST[dimension];
			$weight = $_POST[weight];
			$short_description = $_POST[short_description];

			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$interface[$i]', '$num_of_buttons[$i]', '$wheel[$i]', '$dpi[$i]',
						'$dimension[$i]', '$weight[$i]', '$short_description[$i]',
						 now(), now(), '$_SESSION[adminId]')";
		} else if ($what_item[$i] == "keyboard") {
			$item_name = $_POST[item_name];
			$item_price = $_POST[item_price];
			$item_des = $_POST[item_description];

			$brand = $_POST[brand];
			$type = $_POST[type];
			$interface = $_POST[interfaces];
			$multimedia_key = $_POST[multimedia_key];
			$weight = $_POST[weight];
			$dimension = $_POST[dimension];
			$color = $_POST[color];
			$graphical_ui = $_POST[graphical_ui];
			$on_board_memory = $_POST[on_board_memory];
			$back_light = $_POST[back_light];
			$cable_length = $_POST[cable_length];
			$short_description = $_POST[short_description];

			$item_time_status = $_POST[item_time_status];

			$elec_item_input = "INSERT INTO `elec_item_details`(
						`cata`, `cata_id`, `sub_cata`, `what_item`, `name`, `description`,  `price`, `item_time_status`,
						 `column_1`, `column_2`, `column_3`, `column_4`, `column_5`,
						 `column_6`, `column_7`, `column_8`, `column_9`, `column_10`,
						 `column_11`, `column_12`,
						`date`, `time`,`admin_id`)
					VALUES ('$cata', '$last_content_id', '$sub_cata', '$what_item[$i]', '$item_name[$i]','$item_des[$i]' , '$item_price[$i]', '$item_time_status[$i]',
						'$brand[$i]', '$type[$i]', '$interface[$i]', '$multimedia_key[$i]', '$weight[$i]',
						'$dimension[$i]', '$color[$i]', '$graphical_ui[$i]', '$on_board_memory[$i]', '$back_light[$i]',
						'$cable_length[$i]', '$short_description[$i]',
						 now(), now(), '$_SESSION[adminId]')";
		}

		if ($conn->query($elec_item_input) === TRUE) {
			$last_item_id = $conn->insert_id;
			echo '<h2>Electronics Item  inserted successfully</h2><h4> ID: ' . $conn->insert_id . '</h4>';
			echo "<br><br><br>";
			insertItemImage($last_item_id, $i, $last_content_id, $cata, $sub_cata, $item_name);
		} else {
			echo '<h2 style="background:red;">Electronics Item is not inserted</h2> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
			echo "<br><br><br>";
		}
		$i++;
	}

	echo "</h1>";
	//header('Location: new/new_'.$_GET[cata].'.php?cata='.$_GET[cata].'&sub_cata='.$_GET[sub_cata].'&content_id='.$last_content_id.'&update_option=update');
}
// elecInsertItem

function insert_search_suggestion($search_txt, $text_type, $cata, $sub_cata, $id) {

	$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
	$search_txt = str_replace($arrayName, "", $search_txt);

	include 'data_connection.php';

	$sql = "INSERT INTO `search_suggestion`
				(`text_s`, `s_text_type`, `cata`, `sub_cata`, `cata_item_id`)
		VALUES ( '$search_txt', '$text_type', '$cata', '$sub_cata', '$id');";

	if ($conn->query($sql) === TRUE) {
		echo '<h3 class="suggestion_insert">Suggestion inserted successfully ID: ' . $conn->insert_id . '</h3>';
		echo "<br><br>";

	} else {
		echo '<h3 class="suggestion_not_insert">Suggestion is not inserted</h3> <br> <h4> Query: ' . $sql . '....Error: ' . $conn->error . ' </h4>';
		echo "<br><br>";
	}
}

//session_destroy();
?>
