<?php
	error_reporting(0);
    session_start();
    if(!isset($_SESSION[adminId])){
        header("Location: ../admin_login.php");
    }
	$filename = "../../image/main_image/63_tourism_28_heritage.jpg";
	$filename = $_GET[path];
	$_SESSION[image_width] = 250;
	$_SESSION[rotate] = "yes";
	//header('Content-type: image/jpeg');
	//$degrees = 270;
	//$source = imagecreatefromjpeg($filename) or notfound();
	//$rotate = imagerotate($source,$degrees,0);
	//$data = file_get_contents(imagejpeg($rotate));
	//file_put_contents("check/".imagejpeg($rotate), $data);
	RotateJpg($filename,270,$filename);




	//imagejpeg($rotate);


	function RotateJpg($filename = '',$angle = 0,$savename = false)
    {
        // Your original file
        $original   =   imagecreatefromjpeg($filename);
        // Rotate
        $rotated    =   imagerotate($original, $angle, 0);
        // If you have no destination, save to browser
        if($savename == false) {
                header('Content-Type: image/jpeg');
                imagejpeg($rotated);
            }
        else
            // Save to a directory with a new filename
            imagejpeg($rotated,$savename);

        // Standard destroy command
        imagedestroy($rotated);
    }
?>