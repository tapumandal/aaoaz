

<?php
error_reporting(0);
session_start();
// Token Managment

$tokenCode = md5(uniqid(rand(), TRUE));
$_SESSION['_token'] = $tokenCode;
$_SESSION['_token_time'] = time();

// END Token management

?>
<!DOCTYPE html>
<html>
<head>
	   <link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
	<title>Aaoaz Admin Access</title>



</head>

<style type="text/css">
	.login_main input.box{
		width: 260px;
	}

	.login_main input.but{
		width: 273px;
	}

	h2{
		padding:  15px 0px 0px 0px !important;
		/*border: 1px dotted red;*/
		border-bottom: 1px solid red;

		width: 272px !important;
		font-size: 22px;
		color: red !important;
		background: #E6E6E6 !important;

	}

	input[type=submit]{
		cursor: pointer;
	}
</style>
<body>




<?php
$previous_page = $_SERVER['HTTP_REFERER'];
$current_page = $_SERVER['REQUEST_URI'];
?>
<div class="login_main">
	<div class="login_main_inside">

		<div class="log_in">
			<h2>Identify Yourself</h2>
			<div class="log_input">
				<form method="POST" action="admin_log_return.php">
					<input class="box" placeholder="Enter Email" type="email" name="email" tabindex="2" required>
				    <input class="box" placeholder="Password"  type="password" name="pass"  required>
				    <input type="hidden" name="page" value="<?php echo htmlspecialchars($_SERVER['HTTP_REFERER']); ?>">
				    <input type="hidden" name="_token" value="<?php echo $_SESSION['_token']; ?>">
				    <input class="but" type="submit" value="Access">

				</form>
			</div>

		</div>


		<div class="sign_up">
			<h2>Request Admin Privilege</h2>
			<div class="sign_input">
				<form method="POST" action="admin_signup.php">

					<input class="box" type="text" name="name" placeholder="Full Name" required>
					<input class="box" type="email" name="email" placeholder="Email" required>
					<input class="box" type="password" name="pass" placeholder="Password" required>
					<input class="box" type="text" name="phone" placeholder="Phone" min="11" max="11" maxlength="11" required>
					<br>
					<input type="hidden" name="page" value="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']); ?>">
					<input type="hidden" name="_token" value="<?php echo $_SESSION['_token']; ?>">
					<input class="but" type="submit" value="Request">
				</form>
			</div>
		</div>
	</div>

</div>
</body>
</html>
