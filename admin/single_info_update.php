<?php

session_start();

include 'data_connection.php';

if (!isset($_SESSION[adminId])) {
	header("Location: ../admin_login.php");
}

function GeneralInfoCharReplace($string) {

	$string = str_replace('\'', "\'", $string);
	$string = str_replace('"', '\"', $string);
	$string = str_replace('\\', '\\\ ', $string);
	$string = str_replace('`', "\'", $string);
	$string = str_replace(';', '. ', $string);

	return $string;
}

$update_type = GeneralInfoCharReplace($_GET[updateType]);

if ($update_type == "general_info") {
	updateGeneralInfo();
	updateDataStatus();

	// header('Location: ' . $_SERVER['HTTP_REFERER']);
} else if ($update_type == "item_image") {
	ItemImage();
} else if ($update_type == "itemInfo") {
	updateItemInfo();
} else if ($update_type == "deleteMainImage") {
	mainImageDelete();
} else if ($update_type == "itemDesInfo") {
	itemDesInfo();
} else if ($update_type == "tourism_map1") {
	echo "<h1>First</h1>";
	insertTourismMap1();
} else if ($update_type == "tourism_map_2") {
	echo "<h1>SECOND</h1>";
	insertTourismMap2();
} else if ($update_type == "map_info") {
	updateMapInfo();
} else if ($update_type == "tourismNearby") {
	updateTourismNearby();
} else if ($update_type == "itemDelete") {
	deleteItem();
}

function updateDataStatus() {
	include 'data_connection.php';

	$getContentName = "SELECT g_info_update FROM general_info WHERE id = '$_GET[id]';";

	$res = $conn->query($getContentName);
	if ($res->num_rows > 0) {
		while ($row = $res->fetch_assoc()) {
			$general_info_update = date("Y.m.d") . " | " . $_SESSION[adminId] . " | " . "G-Info";
			echo $general_info_update . "<< <br>";

			$tmpInfo = explode("  #", $row[g_info_update]);
			echo $tmpInfo[0] . "<< <br>";

			if ($tmpInfo[0] != $general_info_update) {
				$general_info_update = $general_info_update . "  #  " . $row[g_info_update];

				$insert = "UPDATE `general_info` SET `g_info_update` = '$general_info_update' WHERE id = '$_GET[id]'";
				if ($conn->query($insert) === TRUE) {
					echo "<br> General Info Updated Status Inserted";
				}
			} else {
				echo "Matches";
			}

		}
	}
}

function deleteItem() {
	include 'data_connection.php';

	$item_id = GeneralInfoCharReplace($_GET[item_id]);
	$cata = GeneralInfoCharReplace($_GET[cata]);

	$sql = "DELETE FROM `item_list` WHERE id = '$item_id' AND cata = '$cata'";

	if ($conn->query($sql) === TRUE) {

		echo "Item Deleted";
	} else {
		echo "Item Not Deleted";
	}
}

function updateTourismNearby() {

	include 'data_connection.php';

	$sql = "UPDATE `tourism_nearby` SET `$_GET[column_name]`='$_POST[text]' WHERE `$_GET[column_name]`='$_GET[nearby_id]' AND cata_id = '$_GET[cata_id]' LIMIT 1;";

	if ($conn->query($sql) === TRUE) {

		echo "<h3>INFO UPDATED</h3>";
	} else {
		echo "<h3>Error: " . $sql . "<br>" . $conn->error . "</h3>";
	}

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}

function updateMapInfo() {
	include 'data_connection.php';

	$insertText = $_POST[text];
	if ($_GET[column_name] == 'map') {
		$insertText = funcSplit($_POST[text]);
	}

	$sql = "UPDATE `map` SET `$_GET[column_name]`='$insertText' WHERE id='$_GET[id]';";

	if ($conn->query($sql) === TRUE) {

		echo "<h3>INFO UPDATED</h3>";
	} else {
		echo "<h3>Error: " . $sql . "<br>" . $conn->error . "</h3>";
	}

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}

function funcSplit($tmpMap) {
	if ($tmpMap != null) {
		$store = explode("src", $tmpMap);

		$resultMap = $store[0] . "width=100% height=250px src" . $store[1];
		echo "<h3>" . $resultMap . "</h3>";
		return $resultMap;
	} else {
		return "";
	}

}

function itemDesInfo() {
	include 'data_connection.php';

	$sql = "UPDATE `item_multi_image` SET `$_GET[column_name]`='$_POST[text]' WHERE id='$_GET[item_id]';";

	if ($conn->query($sql) === TRUE) {

		echo "<h3>INFO UPDATED</h3>";
	} else {
		echo "<h3>Error: " . $sql . "<br>" . $conn->error . "</h3>";
	}

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}

function updateGeneralInfo() {
	include 'data_connection.php';

	$facilities_ac = $_POST[facilities_ac];
	$facilities_wifi = $_POST[facilities_wifi];
	$facilities_card = $_POST[facilities_card];
	$facilities_reservation = $_POST[facilities_reservation];
	$facilities_parking = $_POST[facilities_parking];
	$facilities_smoking_zone = $_POST[facilities_smoking_zone];
	$facilities_kids_zone = $_POST[facilities_kids_zone];
	$facilities_service = $_POST[facilities_service];

	$facilities_capacity = $_POST[facilities_capacity];
	$facilities_other = $_POST[facilities_other];

	$foodFacilities = $facilities_ac . "," . $facilities_wifi . "," . $facilities_card . "," . $facilities_reservation . "," . $facilities_parking . "," . $facilities_smoking_zone . "," . $facilities_kids_zone . "," . $facilities_service . "," . $facilities_capacity . ";" . $facilities_other;

	if (strlen($foodFacilities) > 10) {
		$updateText = $foodFacilities;
	} else {
		$updateText = $_POST[text];
	}

	$sql = "UPDATE `general_info` SET `$_GET[column_name]`='$updateText' WHERE id='$_GET[id]';";

	if ($conn->query($sql) === TRUE) {

		echo "<h3>INFO UPDATED</h3>";

		if ($_GET[column_name] == "offer") {

			if (strlen($updateText) > 6) {
				$sql = "UPDATE `general_info` SET `offer_s`= now() WHERE id='$_GET[id]';";
			} else {
				$sql = "UPDATE `general_info` SET `offer_s`= null WHERE id='$_GET[id]';";
			}
			if ($conn->query($sql) === TRUE) {
				echo "<h4>Offer Date updated</h4>";
			}
		}

	} else {
		echo "<h3>Error: " . $sql . "<br>" . $conn->error . "</h3>";
	}

}

function updateItemInfo() {
	include 'data_connection.php';

	if ($_GET[cata] == "electronics") {
		$sql = "UPDATE `elec_item_details` SET `$_GET[column_name]`='$_POST[text]' WHERE id='$_GET[item_id]';";
	} else {
		$sql = "UPDATE `item_list` SET `$_GET[column_name]`='$_POST[text]' WHERE id='$_GET[item_id]';";
	}

	if ($conn->query($sql) === TRUE) {

		echo "<h3>INFO UPDATED</h3>";
	} else {
		echo "<h3>Error: " . $sql . "<br>" . $conn->error . "</h3>";
	}

	header('Location: ' . $_SERVER['HTTP_REFERER']);
}

function resizeImage($sourceImage, $targetImage, $ratio, $quality) {
	$status = false;
	// Get dimensions of source image.
	list($origWidth, $origHeight) = getimagesize($sourceImage);

	if ($maxWidth == 0) {
		$maxWidth = $origWidth / 10;
		$maxWidth = $maxWidth * $ratio;
	}

	if ($maxHeight == 0) {
		$maxHeight = $origHeight / 10;
		$maxHeight = $maxHeight * $ratio;
	}

	// Calculate ratio of desired maximum sizes and original sizes.
	$widthRatio = $maxWidth / $origWidth;
	$heightRatio = $maxHeight / $origHeight;

	// Ratio used for calculating new image dimensions.
	$ratio = min($widthRatio, $heightRatio);

	// Calculate new image dimensions.
	$newWidth = (int) $origWidth * $ratio;
	$newHeight = (int) $origHeight * $ratio;

	// Obtain image from given source file.
	if (strpos($sourceImage, '.jpg') !== false) {
		if (!$image = @imagecreatefromjpeg($sourceImage)) {
			return false;
		}
		// Create final image with new dimensions.
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagejpeg($newImage, $targetImage, $quality);
		$status = true;
	} else if (strpos($sourceImage, '.png') !== false) {
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		$source = imagecreatefrompng($sourceImage);
		imagecopyresized($newImage, $source, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagepng($newImage, $targetImage);
		$status = true;
	} else if (strpos($sourceImage, '.jpeg') !== false) {
		if (!$image = @imagecreatefromjpeg($sourceImage)) {
			return false;
		}
		// Create final image with new dimensions.
		$newImage = imagecreatetruecolor($newWidth, $newHeight);
		imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
		imagejpeg($newImage, $targetImage, $quality);
		$status = true;
	}

	// Free up the memory.
	imagedestroy($image);
	imagedestroy($newImage);

	imagedestroy($source);

	return $status;
}

function ItemImage() {
	include "data_connection.php";
	$cata = $_GET[cata];
	$short_note = $_POST[short_note];
	$k = 0;
	// Your file
	// $file = 'C:\wamp\www\reelstubs\app\webroot\img\movies\0bf522bb76777ba43393b9be5552b263.jpg';
	if (isset($_SESSION[item_image_name][0][0]) && $_SESSION[item_image_type][0][0] != null) {

		$file = 'upload_crop/upload_pic/' . $_SESSION[item_image_name][0] . $_SESSION[item_image_type][0];
		echo "<h4>";
		echo $file;
		echo "</h4>";
		// Open the file to get existing content
		//$data = file_get_contents($file);

		// New file
		//$new = 'C:\wamp\www\reelstubs\app\webroot\img\movies\newimage.jpg';

		$arrayName = array('[', ']', '^', '\\', '+', '/', '"', '\'', ')', '(');
		$name = str_replace($arrayName, "", $_GET[imageLink]);

		$name = $name;
		if (strpos($name, '.jpg') == false && strpos($name, '.png') == false) {
			$tmpName = str_replace($arrayName, "", $_GET[item_name]);
			$name = $tmpName . "_" . $_GET[item_id] . "_" . $_GET[cata] . $_SESSION[item_image_type][0];
		}
		$new = '../image/item_image/' . $name;

		echo "<br><h2>";
		echo $new;
		echo "</h2>";

		// Write the contents back to a new file
		//file_put_contents($new, $data);
		// $imageCopyStatus = resizeImage($file, $new, 9, 90);
		copy($file, $new);
		if ($cata == "education" || $cata == "health") {
			$sql = "UPDATE `doctor_faculty` SET `image_link`='$name' WHERE  id='$_GET[item_id]'";
		} else if ($cata == "electronics") {
			$sql = "UPDATE `elec_item_details` SET `image_link`='$name' WHERE  id='$_GET[item_id]'";
		} else {
			$sql = "UPDATE `item_list` SET `image_link`='$name' WHERE  id='$_GET[item_id]'";
		}

		if ($conn->query($sql) === TRUE) {

			$last_item_id = $conn->insert_id;
			echo "<h3>Item image insert successfully. Last inserted ID is: " . $last_item_id . " </h3>";

			$mImageNew = '../image/item_image/s_' . $name;
			$imageCopyStatus = resizeImage($file, $mImageNew, 2, 90);

		} else {
			echo "<h3>Item image insert Error: " . $sql . "<br>" . $conn->error . "</h3>";
		}
		$k++;
	}

	$lastMultiId;
	$getLastItemId = "SELECT * FROM item_multi_image ORDER BY id DESC LIMIT 1;";
	$getLastItemId = $conn->query($getLastItemId);
	if ($getLastItemId->num_rows > 0) {
		while ($row = $getLastItemId->fetch_assoc()) {
			$lastMultiId = $row[id];
		}
	}
	unset($_SESSION[item_image_name][0]);
	unset($_SESSION[item_image_type][0]);

	$j = 0;
	while ($_SESSION[item_multi_image_type][$k][$j] != null) {

		$file = 'upload_crop/upload_pic/' . $_SESSION[item_multi_image_name][$k][$j] . $_SESSION[item_multi_image_type][$k][$j];

		$lastMultiId++;
		$name = $lastMultiId . "_" . $_GET[imageLink];
		$new = '../image/item_image/item_multi_image/' . $name;

		// Write the contents back to a new file
		// $imageCopyStatus = resizeImage($file, $new, 9, 90);
		copy($file, $new);

		$sql = "INSERT INTO `item_multi_image`(`item_id`, `image_link`) VALUES ('$_GET[item_id]','$name')";
		if ($conn->query($sql)) {

			$mImageNew = '../image/item_image/item_multi_image/s_' . $name;
			$imageCopyStatus = resizeImage($file, $mImageNew, 2, 90);

			echo "multi imahe inserted successfully";
		} else {
			echo "<h3>Item multi image insert Error: " . $sql . "<br>" . $conn->error . "</h3>";
		}

		unset($_SESSION[item_multi_image_name][$k][$j]);
		unset($_SESSION[item_multi_image_type][$k][$j]);

		$j++;
	}
	//header('Location: ' . $_SERVER['HTTP_REFERER']);
}
function resizeImagePrevious($sourceImage, $targetImage, $maxWidth, $maxHeight, $quality) {
	// Obtain image from given source file.
	if (!$image = @imagecreatefromjpeg($sourceImage)) {
		return false;
	}

	// Get dimensions of source image.
	list($origWidth, $origHeight) = getimagesize($sourceImage);

	if ($maxWidth == 0) {
		$maxWidth = $origWidth;
	}

	if ($maxHeight == 0) {
		$maxHeight = $origHeight;
	}

	// Calculate ratio of desired maximum sizes and original sizes.
	$widthRatio = $maxWidth / $origWidth;
	$heightRatio = $maxHeight / $origHeight;

	// Ratio used for calculating new image dimensions.
	$ratio = min($widthRatio, $heightRatio);

	// Calculate new image dimensions.
	$newWidth = (int) $origWidth * $ratio;
	$newHeight = (int) $origHeight * $ratio;

	// Create final image with new dimensions.
	$newImage = imagecreatetruecolor($newWidth, $newHeight);
	imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
	imagejpeg($newImage, $targetImage, $quality);

	// Free up the memory.
	imagedestroy($image);
	imagedestroy($newImage);

	return true;
}
function mainImageDelete() {
	echo "\nImage Deleted >>" . $_GET[imageId] . "<<<";
	include 'data_connection.php';

	$sql = "SELECT * FROM `general_image` WHERE ID='$_GET[imageId]';";
	$data = $conn->query($sql);

	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$path = "../image/main_image/";
			$image = $row[image_link];
			$file = $path . $image;
			echo "\n" . $file;
			if (!unlink($file)) {
				echo ("\nError deleting $file");
			} else if (unlink($file)) {
				$delete = "DELETE FROM `general_image` WHERE id='$_GET[imageId]';";
				if ($conn->query($delete) === TRUE) {
					echo "\nimage & sql data both deleted";
				} else {
					echo "\nsql data not deleted";
				}
			} else {
				$delete = "DELETE FROM `general_image` WHERE id='$_GET[imageId]';";
				if ($conn->query($delete) === TRUE) {
					echo "\nNot image but sql data deleted";
				} else {
					echo "\nimage & sql data are not deleted";
				}
			}
		}
	}
}

function insertTourismMap1() {
	include "data_connection.php";

	// Your file
	// $file = 'C:\wamp\www\reelstubs\app\webroot\img\movies\0bf522bb76777ba43393b9be5552b263.jpg';
	if (isset($_SESSION[item_image_name][0][0]) && $_SESSION[item_image_type][0][0] != null) {

		$file = 'upload_crop/upload_pic/' . $_SESSION[item_image_name][0] . $_SESSION[item_image_type][0];
		echo "<h1>";
		echo $file;
		echo "</h1>";
		// Open the file to get existing content
		//$data = file_get_contents($file);

		// New file
		//$new = 'C:\wamp\www\reelstubs\app\webroot\img\movies\newimage.jpg';
		$name = $_GET[cata_id] . "_" . $_GET[cata] . "_1.jpg";
		$new = '../image/main_image/' . $name;

		// Write the contents back to a new file
		//file_put_contents($new, $data);
		copy($file, $new);

		$sql = "UPDATE `general_info` SET `column_12`='$name' WHERE  id='$_GET[cata_id]'";

		if ($conn->query($sql) === TRUE) {

			echo "<h3>Map image insert successfully. Last inserted ID is: " . $last_item_id . " </h3>";
		} else {
			echo "<h3>Map image insert Error: " . $sql . "<br>" . $conn->error . "</h3>";
		}
		$k++;
	}
}

function insertTourismMap2() {
	include "data_connection.php";

	// Your file
	// $file = 'C:\wamp\www\reelstubs\app\webroot\img\movies\0bf522bb76777ba43393b9be5552b263.jpg';
	if (isset($_SESSION[item_image_name][0][0]) && $_SESSION[item_image_type][0][0] != null) {

		$file = 'upload_crop/upload_pic/' . $_SESSION[item_image_name][0] . $_SESSION[item_image_type][0];
		echo "<h1>";
		echo $file;
		echo "</h1>";
		// Open the file to get existing content
		//$data = file_get_contents($file);

		// New file
		//$new = 'C:\wamp\www\reelstubs\app\webroot\img\movies\newimage.jpg';
		$name = $_GET[cata_id] . "_" . $_GET[cata] . "_2.jpg";
		$new = '../image/main_image/' . $name;

		// Write the contents back to a new file
		//file_put_contents($new, $data);
		copy($file, $new);

		$sql = "UPDATE `general_info` SET `column_13`='$name' WHERE  id='$_GET[cata_id]'";

		if ($conn->query($sql) === TRUE) {

			echo "<h3>Map image insert successfully. Last inserted ID is: " . $last_item_id . " </h3>";
		} else {
			echo "<h3>Map image insert Error: " . $sql . "<br>" . $conn->error . "</h3>";
		}
		$k++;
	}
}
?>