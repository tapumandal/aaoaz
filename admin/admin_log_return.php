<?php
//error_reporting(0);
session_start();

$token = "";
if (isset($_POST['_token'])) {
	$token = $_POST['_token'];
} else if (isset($_GET['_token'])) {
	$token = $_POST['_token'];
}

$tokenTime = time() - $_SESSION['_token_time'];

if ($token == $_SESSION['_token'] && $tokenTime < 100) {
} else {
	echo '<script> window.location.replace("./"); </script>';
	exit();
}

include 'data_connection.php';

function stringFilter($string) {
	include 'data_connection.php';
	$string = mysqli_real_escape_string($conn, $string);

	$string = trim($string);
	$string = str_replace('\\', '', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('SELECT', '', $string);
	$string = str_replace('UPDATE', '', $string);
	$string = str_replace('DROP', '', $string);
	$string = str_replace('INSERT', '', $string);
	$string = str_replace('DELETE', '', $string);
	$string = str_replace('FROM', '', $string);
	$string = str_replace('WHERE', '', $string);
	$string = str_replace('JOIN', '', $string);

	$string = str_replace('select', '', $string);
	$string = str_replace('update', '', $string);
	$string = str_replace('drop', '', $string);
	$string = str_replace('insert', '', $string);
	$string = str_replace('delete', '', $string);
	$string = str_replace('from', '', $string);
	$string = str_replace('where', '', $string);
	$string = str_replace('join', '', $string);

	return $string;
}

$postEmail = stringFilter($_POST[email]);
$postPass = stringFilter($_POST[pass]);

$encryptPostPass = encryptPassword($postPass);

if ($postEmail != "" && $postPass != "") {

	if ($stmt = $conn->prepare("SELECT * FROM `admin` WHERE email= ? AND pass= ? AND status='enable'")) {
		$stmt->bind_param("ss", $postEmail, $encryptPostPass);
		$stmt->execute();
		$result = $stmt->get_result();

		if ($result->num_rows === 0) {
			echo "You are not IDENTIFIED";
		}
		while ($row = $result->fetch_assoc()) {

			$id = $row[id];
			$_SESSION["adminId"] = $id;
			$_SESSION["adminType"] = $row[type];
			$_SESSION["adminName"] = $row[name];

			$admin_acc = "INSERT INTO `admin_access_time`(`admin_id`, `login date`, `login time`) VALUES ('$_SESSION[adminId]', now(),now())";

			if ($conn->query($admin_acc)) {
				echo "admin access time sucessful";
			} else {
				echo "admin access time problem";
			}

		}
	} else {
	}
	//header('Location: index.php');
	echo '<script>window.location.replace("./");</script>';

}

function encryptPassword($string) {
	$enPass = md5($string);
	$enTmp1 = "";
	$enTmp2 = "";
	$enTmp3 = "";
	$enTmp4 = "";
	$enTmp5 = "";

	$i = 0;
	for ($i = 0; $i < strlen($enPass); $i++) {
		if ($i < 4) {
			$enTmp1 = $enTmp1 . $enPass[$i];
		} else if ($i < 8) {
			$enTmp2 = $enTmp2 . $enPass[$i];
		} else if ($i < 12) {
			$enTmp3 = $enTmp3 . $enPass[$i];
		} else if ($i < 16) {
			$enTmp4 = $enTmp4 . $enPass[$i];
		} else {
			$enTmp5 = $enTmp5 . $enPass[$i];
		}
	}

	$encrypted = $enTmp1 . $enTmp4 . $enTmp3 . $enTmp2 . $enTmp5;

	return $encrypted;
}
?>