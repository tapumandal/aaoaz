<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>

<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="../../js/jquery.min.js"></script>
  <link href="../../css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/css_for_upload.css" rel="stylesheet" type="text/css" media="all" />
   <script src="../../js/self.js"></script>

   <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> -->
  <!-- WINDOW OVERLAY -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/win_overlay.js"></script>
    <link type="text/css" rel="stylesheet" href="../../css/win_overlay.css" />
  <!-- WINDOW OVERLAY -->
<title></title>
</head>


<body>
<script>
	function mapInput(){
		var num_branch= document.getElementById("num_branch").options;		
		var n= document.getElementById("num_branch").selectedIndex;		
		
		//alert(num_branch[n].text);
		for (var i = 1; i <= num_branch[n].text; i++) {
			// alert(i);
				var check = 0;
			 var map_pos       = $(".info");
			$(map_pos).append('<div ID="map_box" class="input_box last"><div class="input_box"><input class="input_field" type="text" name="main_location[]" placeholder="Main Location"></div><div class="input_box"><input class="input_field" type="text" name="contact[]" placeholder="Contact"></div>'+
			'<div class="input_box"><input class="input_field" type="text" name="full_address[]" placeholder="Full Address"></div><textarea class="input_field" type="text" name="map[]" placeholder="Map"></textarea></div>'); //add input box
		};
	}

	
	 $(document).ready(function() {

	 	var cata_name= document.getElementById("cata_name").firstChild.data;
	    var sub_cata_name= document.getElementById("sub_cata_name").firstChild.data;
	    	
	 //    if(sub_cata_name == "restaurant"){
	 //        var foodItemFilter = '<div id="item_list_filter">
		// 		<span>Select Catagory : </span>
		// 		<select name="catagory" class="check_select" id="cata" onchange="change_sub_cata(this)">
		// 		  <option >Select Catagory</option>
		// 		  <option value="starter">STARTER</option>
		// 		  <option value="fast food">FAST FOOD</option>
		// 		  <option value="combo">COMBO</option>
		// 		  <option value="salad">SALAD</option>
		// 		  <option value="dessert">DESSERT</option>
		// 		  <option value="beverage">BEVERAGE</option>
		// 		</select>
		// 	</div>';
		// }

		 if(sub_cata_name == "university" || sub_cata_name == "college" || sub_cata_name == "school"){
	        var teacherListFilter = '<div id="item_list_filter"><span>Select Catagory : </span>'+
	        '<select name="teacher_filter[]" class="check_select" id="cata" onchange="change_sub_cata(this)">'+
	        '<option value="">Select Catagory</option>'+
	        '<option value="cse">CSE</option>'+
	        '<option value="eee">EEE</option>'+
	        '<option value="civil">CIVIL</option>'+
	        '</select>'+
	        '<input type="text" name="teacher_filter_b[]">'+
	        '</div>';
		}


		// $("#sub_cata").val($("#sub_cata option:first").val()); //auto select the first option in select tag in comment section
	    var title_img       = $(".title_img");
	    var title_img_but = $(".title_img_but");

	    var y = 0; //initlal text box count
	    $(title_img_but).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(y < 1){ //max input box allowed
	            y++; //text box increment
	            $(title_img).append('<div class="title_image"><iframe src="../upload_crop/upload_crop.php?type=title_image"></iframe></div>'); //add input box
	        }
	    });



		
		var main_image = $(".main_image");
		var main_img_but = $(".main_img_but");
		var z=-1;
		$(main_img_but).click(function(e){ //on add input button click
	        
	        e.preventDefault();
	        if(z < 10){ //max input box allowed
	            z++; //text box increment
	            $(main_image).append('<div class="col-sm-6"><div class="title_image"><iframe src="../upload_crop/upload_crop.php?cata_name='+cata_name+'&type=main_image&main_image_num='+z+'"></iframe></div></div>'); //add input box
	        }
	    });





		var imageShape="square";
	    $('select').on('change', function() {
		  imageShape=this.value ;
		});

	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_doc_fac"); //Add button ID
	    
	    var x = -1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-6 one_item_list"><div class="item_list_info"><div class="input_box"><input class="input_field item_name" type="text" name="teacher_name[]" placeholder="Teacher NAME"></div>'+
	            	'<div class="input_box"><input class="input_field item_price" type="text" name="designation[]" placeholder="Designation"></div>'+
	            '<div class="input_box"><textarea class="input_field item_des" type="text" name="education[]" placeholder="Education"></textarea></div>'+
	            '<div class="input_box"><input class="input_field item_price" type="text" name="email[]" placeholder="Email"></div>'+
	            '<div class="input_box"><input class="input_field" type="text" name="phone[]" placeholder="Phone">'+teacherListFilter+'</div></div><div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe></div></div>'); //add input box
	        }
	    });
	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })



	     function xlFileUpload(input) {

	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	        	    
	            reader.onload = function (e) {
	                  var url = e.target.result;
	                  var oReq = new XMLHttpRequest();
	                  oReq.open("GET", url, true);
	                  oReq.responseType = "arraybuffer";

	                  oReq.onload = function(e) {
	                    var arraybuffer = oReq.response;

	                    var data = new Uint8Array(arraybuffer);
	                    var arr = new Array();
	                    for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
	                    var bstr = arr.join("");

	                    var workbook = XLSX.read(bstr, {type:"binary"});
	                    
	                    var first_sheet_name = workbook.SheetNames[0];

	                    var worksheet = workbook.Sheets[first_sheet_name];
	                    

	                    var objArray = XLSX.utils.sheet_to_json(worksheet,{raw:true});

	                    var limit=0;

	                    var wrapper2         = $(".item_list");
	                    
	                    var tName = "";
						var tDesignation = "";
						var tEducation = "";
						var tEmail = "";
						var tPhone = "";
						var tDepartment = "";

		                    while(objArray[limit]){
			                      tName = objArray[limit].NAME;
			                      tDesignation = objArray[limit].Designation;
			                      tEducation = objArray[limit].Education;
			                      tEmail = objArray[limit].Email;
			                      tPhone = objArray[limit].Phone;
			                      tDepartment = objArray[limit].Department;


			                      if(tName == 'undefined' || tName == null || tName == ''){
			                      	tName = "";
			                      }
			                      if(tDesignation == 'undefined' || tDesignation == null || tDesignation == ''){
			                      	tDesignation = "---";
			                      }
			                      
			                      if(tEducation == 'undefined' || tEducation == null || tEducation == ''){
			                      	tEducation = "---";
			                      }
			                      if(tEmail == 'undefined' || tEmail == null || tEmail == ''){
			                      	tEmail = "---";
			                      }

			                      if(tPhone == 'undefined' || tPhone == null || tPhone == ''){
			                      	tPhone = "---";
			                      }
			                      if(tDepartment == 'undefined' || tDepartment == null || tDepartment == ''){
			                      	tDepartment = "---";
			                      }

			                      if(tName != "" || tName != null || tName != ''){
			                      		$(wrapper2).append('<div class="xmlFilePanel">'+
			                      					'<span>Name: </span><input class="input_field item_name" type="text" name="teacher_name[]" value="'+tName+'" placeholder="ITEM NAME">'+
			                      					'<span>Name: </span><input class="input_field item_name" type="text" name="designation[]" value="'+tDesignation+'" placeholder="ITEM NAME">'+
			                      					'<span>Name: </span><input class="input_field item_name" type="text" name="education[]" value="'+tEducation+'" placeholder="ITEM NAME">'+
			                      					'<span>Name: </span><input class="input_field item_name" type="text" name="email[]" value="'+tEmail+'" placeholder="ITEM NAME">'+
			                      					'<span>Name: </span><input class="input_field item_name" type="text" name="phone[]" value="'+tPhone+'" placeholder="ITEM NAME">'+
			                      					'<span>Name: </span><input class="input_field item_name" type="text" name="teacher_filter_b[]" value="'+tDepartment+'" placeholder="ITEM NAME">');
			                      }

			                    tName = "";
								tDesignation = "";
								tEducation = "";
								tEmail = "";
								tPhone = "";
								tDepartment = "";

			                      limit++;
		                    }

	                }

	                  oReq.send();



	            }
	            
	            reader.readAsDataURL(input.files[0]);
	        }
	    }
	    
	    $("#xlFile").change(function(){

	        xlFileUpload(this);
	    });



	});

</script>
<div class = "container food">


	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>
	<div class="col-sm-12 field">

	<div class="cata_sub_cata">
		<h1><div class="cata_t">Catagory: </div> <div ID="cata_name"><?php echo $_GET[cata] ?></div></h1>
		<h1><div class="sub_cata_t">Sub Catagory: </div><div ID="sub_cata_name"><?php echo $_GET[sub_cata] ?></div></h1>
	</div>
	<?php
			$update_option=$_GET[update_option];
			if(!isset($update_option)){
			
				
				echo "actual Link".$actual_link;
				echo "<form method='get' action='".$actual_link."'>
						<input type='hidden' name='cata' value='".$_GET[cata]."'>
						<input type='hidden' name='sub_cata' value='".$_GET[sub_cata]."'>
						<input type='hidden' name='update_option' value='new'>
						<input type='submit' value='NEW'>
					</form>";
				echo "<form method='get' action='".$actual_link."'>
						<input type='hidden' name='cata' value='".$_GET[cata]."'>
						<input type='hidden' name='sub_cata' value='".$_GET[sub_cata]."'>
						<input type='text' name='content_id' placeholder='Content ID you want to UPDATE'>
						<input type='hidden' name='update_option' value='update'>
						<input type='submit' value='UPDATE'>
					</form>";
			}

		if($update_option == "new" && isset($update_option)){

	?>
	<form  method="POST" target='_blank' action="../upload_data.php?cata=<?php echo $_GET[cata] ?>&sub_cata=<?php echo $_GET[sub_cata] ?>" enctype="multipart/form-data">
		
			<div class="col-sm-12 info_title_img">
				<h2>Main Information and Title Image</h2>
				<div class="col-sm-12 info">
					<div class="input_box"><input class="input_field" type="text" name="name" placeholder="Name"></div>
					<div class="input_box"><textarea class="input_field" type="text" name="description" placeholder="Description"></textarea></div>
					<div class="input_box"><input class="input_field" type="text" name="dept_list" placeholder="Department List"></div>
					
					<div class="input_box"><input class="input_field" type="text" name="information" placeholder="Information"></div>
					<div class="input_box"><input class="input_field" type="text" name="highlight" placeholder="Highlight"></div>
					<div class="input_box"><input class="input_field" type="text" name="admission" placeholder="Admission"></div>
					<div class="input_box"><input class="input_field" type="text" name="admission_requirments" placeholder="Admission Requirments"></div>

					<div class="input_box"><input class="input_field" type="text" name="notice" placeholder="Notice"></div>
	   <div class="input_box_file"><span>Notice PDF File</span><input type="file" name="notice_file"  /></div>

					<div class="input_box"><input class="input_field" type="text" name="course_details" placeholder="Course Details"></div>
<div class="input_box_file"><span>Course Details PDF File</span><input type="file" name="course_details_file" size="100" /></div>

					<div class="input_box"><input class="input_field" type="text" name="cgpa_system" placeholder="CGPA System"></div>
					<div class="input_box"><input class="input_field" type="text" name="class_plan" placeholder="Class Plan"></div>
	<div class="input_box_file"><span>Class Plan PDF File</span><input type="file" name="class_plan_file" size="100" /></div>

					<div class="input_box"><input class="input_field" type="text" name="exam_schedule" placeholder="Exam Schedule"></div>
	<div class="input_box_file"><span>Exam Hall PDF File</span><input type="file" name="exam_schedule_file" size="100" /></div>

					<div class="input_box"><input class="input_field" type="text" name="exam_hall" placeholder="Exam Hall"></div>

					<div class="input_box"><input class="input_field" type="text" name="expense" placeholder="Expense"></div>

					

					<!-- <div class="input_box"><input class="input_field" type="text" name="main_location" placeholder="Main Location"></div>
					<div class="input_box"><input class="input_field" type="text" name="location" placeholder="Full Address"></div> -->
					<div class="input_box"><input class="input_field" type="text" name="web_site" placeholder="Web Site"></div>
					<div class="input_box"><input class="input_field" type="text" name="social_links" placeholder="Social Links"></div>
					<div class="input_box"><input class="input_field" type="text" name="contact" placeholder="Contact"></div>
					<!-- <div class="input_box"><textarea class="input_field" type="text" name="map" placeholder="Map"></textarea></div> -->

					<select id="num_branch" name="num_branch" onchange="mapInput()">
							<option >Select Number</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="10">20</option>
						</select>
						<span>Select Number of Branches</span>
				</div>

				<div class="col-sm-8 title_img ">
					<button class="title_img_but">UP TITLE IMAGE</button>
				</div>
			</div>

			<div class="col-sm-12 main_image">
				<h2>Main Image (Slide Show)</h2>
				<button class="main_img_but">UP MAIN IMAGE</button>

			</div>

		
		<div class="col-sm-12 item_list">
			
		</div>

		<button class="add_doc_fac">Add Item</button>

		<br> <br><div class="xmlfilebtn">Select Excel File. Formation: NAME, Designation, Education, Email, Phone, Department <br><input type='file' id="xlFile"/> </div> <br> 
		
		<div class="submit_button">
			<input type="submit" value="SUBMIT">
		</div>
	</form>

	<?php
			}
			$content_id = $_GET[content_id];
			if($update_option == "update" && isset($update_option) && !empty($content_id)){
			echo "<h3>UPDATE CONTENT: ".$_GET[content_id]."</h3>";
	?>
			
			<form method="POST" target="_blank" action="../upload_data.php?cata=<?php echo $_GET[cata] ?>&sub_cata=<?php echo $_GET[sub_cata] ?>&content_id=<?php echo $content_id ?>" >
			<div class="col-sm-12 info_title_img">
					

					<div class="col-sm-8 title_img ">
						<button class="title_img_but">UP TITLE IMAGE</button>
					</div>
			</div>

			<div class="info">
			</div>

			<select id="num_branch" name="num_branch" onchange="mapInput()">
						<option >Select Number</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="10">20</option>
			</select>
			<span>Select Number of Branches</span>

				<div class="col-sm-12 main_image">
					<h2>Main Image (Slide Show)</h2>
					<button class="main_img_but">UP MAIN IMAGE</button>

				</div>
			<select id="image_s">
				<option value="square">square</option>
				<option value="landscape">landscape</option>
				<option value="portrait">portrait</option>
			</select>
			<div class="all_item_data">
				<div class="col-sm-12 item_list"></div>
				<button class="add_doc_fac">Add Item</button>

				<br> <br><div class="xmlfilebtn">Select Excel File. Formation: NAME, Designation, Education, Email, Phone, Department <br><input type='file' id="xlFile"/> </div> <br> 
			</div>


			<div class="submit_button">
				<input type="submit" value="SUBMIT">
			</div>
		</form>
	<?php
		}

	?>
		<div class="admin_footer">
			
		</div>
	</div>


	<!-- <div class="col-sm-6 one_item_list">
		<div class="item_list_info">
			<div class="input_box">
				<input class="input_field item_name" type="text" name="teacher_name[]" placeholder="Teacher NAME">
			</div>
			<div class="input_box">
				<input class="input_field item_price" type="text" name="designation[]" placeholder="Designation">
			</div>
			<div class="input_box">
				<textarea class="input_field item_des" type="text" name="education[]" placeholder="Education"></textarea>
			</div>
			<div class="input_box">
				<input class="input_field item_price" type="text" name="contact[]" placeholder="Contact">
			</div>
		</div>
		
		<div class="item_image">
			<iframe src="../upload_crop/upload_crop.php?type=item_image&item_num='+x+'"></iframe>
		</div>
	</div> -->

	
</div>
<!-- END OF container -->
</body>
</html>