<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>

<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="../../image/default_image/short_logo.png">
<title><?php echo $_GET[cata] ?> Data Status</title>

  <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="../../js/jquery.min.js"></script>
  <link href="../../css/style.css" rel="stylesheet" type="text/css" media="all" />	
<title></title>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
th{
	text-align: center !important;
}
td{
	overflow: auto;
	max-width: 300px;
	
}
td div{
	max-height: 100px !important;
	overflow: auto;
	/*background: red;*/
	font-size: 11px; 
}
a img{
	width: 20px;
	height: 20px;
	/*background: gray;*/
	padding: 3px;
	border-radius: 50px;
}
</style>

</head>

<body>
	<div class = "container">

	<div class = "main-top">
		<div class="main">
				
					
				<?php 	include 'header.php'; ?>

		</div>
	</div>
		<?php
			include '../../data_connection.php';

			if($_GET[cata] == "education" || $_GET[cata] == "health"){
				$getSql = "SELECT id, name, cata, sub_cata, (SELECT COUNT(doctor_faculty.cata_id) FROM doctor_faculty WHERE  doctor_faculty.cata_id = general_info.id) AS item_count, `date`, `admin_id`, `g_info_update`, `item_info_update`, `status` FROM general_info WHERE cata = '$_GET[cata]' ORDER BY $_GET[orderBy] $_GET[action];";
			}
			else{
				$getSql = "SELECT id, name, cata, sub_cata, (SELECT COUNT(item_list.cata_id) FROM item_list WHERE  item_list.cata_id = general_info.id) AS item_count, `date`, `admin_id`, `g_info_update`, `item_info_update`, `status` FROM general_info WHERE cata = '$_GET[cata]' ORDER BY $_GET[orderBy] $_GET[action];";
			}			
			$res = $conn->query($getSql);

			if($res->num_rows>0)
			{	
				$i=1;
				echo '<table>
							  <tr>
							    <th>No</th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=id&action=ASC"> <img src="../../../image/default_image/down.png"> </a>ID <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=id&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=name&action=ASC"> <img src="../../../image/default_image/down.png"> </a>Name <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=name&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=sub_cata&action=ASC"> <img src="../../../image/default_image/down.png"> </a>SubCatagory <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=sub_cata&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=item_count&action=ASC"> <img src="../../../image/default_image/down.png"> </a>Item Num <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=item_count&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=date&action=ASC"> <img src="../../../image/default_image/down.png"> </a>Upload Date <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=date&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=admin_id&action=ASC"> <img src="../../../image/default_image/down.png"> </a>A-ID <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=admin_id&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=g_info_update&action=ASC"> <img src="../../../image/default_image/down.png"> </a>General Info Update <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=g_info_update&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=item_info_update&action=ASC"> <img src="../../../image/default_image/down.png"> </a>Item List Update <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=item_info_update&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							    <th><a href="./data_status.php?cata='.$_GET[cata].'&orderBy=status&action=ASC"> <img src="../../../image/default_image/down.png"> </a>Status <a href="./data_status.php?cata='.$_GET[cata].'&orderBy=status&action=DESC"> <img src="../../../image/default_image/up.png"> </a></th>
							  </tr>';
				while($row = $res-> fetch_assoc())
				{
					$g_info_update = str_replace(" # ", "<br>", $row[g_info_update]);
					$item_info_update = str_replace(" # ", "<br>", $row[item_info_update]);
					echo '  
							  <tr>
							    <td>'.$i.'</td>
							    <td>'.$row[id].'</td>
							    <td><a href="./'.$row[cata].'.php?sub_cata='.$row[sub_cata].'&cata='.$row[cata].'&id='.$row[id].'" target="_blank">'.$row[name].'</a></td>
							    <td>'.$row[sub_cata].'</td>
							    <td>'.$row[item_count].'</td>
							    <td>'.$row[date].'</td>
							    <td>'.$row[admin_id].'</td>
							    <td><div><span>'.$g_info_update.'</span></div></td>
							    <td><div><span>'.$item_info_update.'</span></div></td>
							    <td>'.$row[status].'</td>
							  </tr>';
					$i++;
				}
				echo '</table>';
			}

		?>
	</div>
</body>
</html>