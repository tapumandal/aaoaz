<?php
error_reporting(0);
if(!isset($_SESSION[adminId])){
	    header("Location: ../admin_login.php");
	}

	include 'data_connection.php';

	if(isset($_GET['s']) && $_GET['s'] != ''){
		$s = $_GET['s'];
		$sql = "SELECT * FROM `search_suggestion` WHERE text_s LIKE '%$s%'";
		$result = $conn->query($sql);
		if($result->num_rows>0){
			while($row = $result->fetch_assoc()){
				
				$suggestion = $row['text_s'];
				$s_text_type = $row['s_text_type'];
				$cata = $row[cata];
				$cata_item_id = $row[cata_item_id];

				echo "<div style='' id='searchtitle'>" . 
				"<a style='font-family: verdana; text-decoration: none;'  
				onclick='search_result(\"".$suggestion."\",\"".$s_text_type."\",\"".$cata."\",\"".$cata_item_id."\")'> " . $suggestion . "</a>" . "
				</div>";
			}
		}
	
	}
	if(isset($_GET[suggestion]) && $_GET[searchNum]=="first"){
		first_suggestion();
	}
	else if(isset($_GET[suggestion]) && $_GET[searchNum]=="next"){
		next_suggestion();
	}

	function first_suggestion(){
		include 'data_connection.php';
		if($_GET[sTextType]=="business_profile"){
			$sql = "SELECT sub_cata, cata, name, image_link, description FROM `general_info` WHERE cata = '$_GET[cata]' AND id = '$_GET[cataItemId]';";
			$result = $conn->query($sql);
			if($result->num_rows>0){
				while ($row = $result->fetch_assoc()) {

					$des = $row[description];
					 $des = substr($des, 0, 350);
					echo '<div class="search_result_box">
					    	<div class="image_side">
					    		<img src="image/title_image/'.$row[image_link].'" style="max-width: 120px;">		
					    	</div>
					    	<div class="data_side">
					    		<div class="bus_pro_name">
					    			<a href="'.$row[cata].'.php?sub_cata='.$row[sub_cata].'&cata='.$row[cata].'&id='.$_GET[cataItemId].'"><span >'.$row[name].'</span></a>
					    		</div>
					    		<div  class="bus_pro_cata_sub">
					    			 <span><a href="specific.php?cata='.$row[cata].'">'.$row[cata].'</a> > <a href="specific.php?cata='.$row[cata].'&sub_cata='.$row[sub_cata].'">'.$row[sub_cata].'</a></span>
					    		</div>
					    		<div class="bus_pro_des">
					    			<span>'.$des.'</span>
					    		</div>
					    		<!--<div class="search_item">
					    			<a class="search_item_name show-popup" href="#show" data-showpopup="54441" onclick="show_overlay(\'food\',1)">Item Name</a>
					    		</div> -->
					    	</div>
					    </div>

					';
				}
			}
		}
		else if($_GET[sTextType]=="item"){
			$sql = "SELECT general_info.sub_cata, general_info.cata, general_info.name, general_info.image_link, general_info.description, item_list.name AS item_name
					FROM `item_list` LEFT JOIN `general_info` ON item_list.cata_id = general_info.id WHERE item_list.id = '$_GET[cataItemId]';";


			$result = $conn->query($sql);
			if($result->num_rows>0){
				while ($row = $result->fetch_assoc()) {
					$des = $row[description];
					 $des = substr($des, 0, 350);
					 if($_GET[cata] == "food" || $_GET[cata] == "electronics"){
					 	$item_show = '<div class="search_item">
					    				<a class="search_item_name show-popup" href="#show" data-showpopup="54441" onclick="show_overlay(\''.$_GET[cata].'\', '.$_GET[cataItemId].')">'.$row[item_name].'</a>
					    			</div>';
					 }
					echo '<div class="search_result_box">
					    	<div class="image_side">
					    		<img src="image/title_image/'.$row[image_link].'" style="max-width: 120px;">		
					    	</div>
					    	<div class="data_side">
					    		<div class="bus_pro_name">
					    			<a href="'.$row[cata].'.php?sub_cata='.$row[sub_cata].'&cata='.$row[cata].'&id='.$_GET[cataItemId].'"><span >'.$row[name].'</span></a>
					    		</div>
					    		<div  class="bus_pro_cata_sub">
					    			 <span><a href="specific.php?cata='.$row[cata].'">'.$row[cata].'</a> > <a href="specific.php?cata='.$row[cata].'&sub_cata='.$row[sub_cata].'">'.$row[sub_cata].'</a></span>
					    		</div>
					    		<div class="bus_pro_des">
					    			<span>'.$des.'</span>
					    		</div>
					    		'.$item_show.'
					    	</div>
					    </div>

					';
				}
			}
		
		}
		else if($_GET[sTextType]=="item_doc_fac"){
			$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata, general_info.name, general_info.image_link, general_info.description, doctor_faculty.name AS item_name
					FROM `doctor_faculty` LEFT JOIN `general_info` ON doctor_faculty.cata_id = general_info.id WHERE doctor_faculty.id = '$_GET[cataItemId]';";


			$result = $conn->query($sql);
			if($result->num_rows>0){
				while ($row = $result->fetch_assoc()) {
					$des = $row[description];
					 $des = substr($des, 0, 350);
					
					echo '<div class="search_result_box">
					    	<div class="image_side">
					    		<img src="image/title_image/'.$row[image_link].'" style="max-width: 120px;">		
					    	</div>
					    	<div class="data_side">
					    		<div class="bus_pro_name">
					    			<a href="'.$row[cata].'.php?sub_cata='.$row[sub_cata].'&cata='.$row[cata].'&id='.$row[id].'"><span >'.$row[name].'</span></a>
					    		</div>
					    		<div  class="bus_pro_cata_sub">
					    			 <span><a href="specific.php?cata='.$row[cata].'">'.$row[cata].'</a> > <a href="specific.php?cata='.$row[cata].'&sub_cata='.$row[sub_cata].'">'.$row[sub_cata].'</a></span>
					    		</div>
					    		<div class="bus_pro_des">
					    			<span>'.$des.'</span>
					    		</div>
					    		<div class="search_item">
					    			<span>'.$row[item_name].'</span>
					    		</div>
					    	</div>
					    </div>

					';
				}
			}

		}
	}


	function next_suggestion(){
		include 'data_connection.php';
		
		$sql = "SELECT `id`, `cata`, `cata_id`, `sub_cata`, `name`, `description`, `image_link`, `price`, `color`, `size`, `model` FROM `item_list` WHERE 1;";

		$item_match = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		$total_id = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$item_cata = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$item_cata_id = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$item_name = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$item_description = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$item_price = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	
		$max_type = array("item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item", "item");


		$result = $conn->query($sql);
		if($result->num_rows>0){
			$i=0;
			while($row=$result->fetch_assoc()){
				
				$total_string = $row[name]." ".$row[description]." ".$row[price]." ".$row[color]." ".$row[size]." ".$row[model];
				$total_string = strtolower($total_string);
				$total_string=str_replace(str_split('`~!@#$%^&*()_+-={}[]:"|;\\<>?,./'), " ", $total_string);

				$single_string = explode(" ", $total_string);


				$suggestion_string = strtolower($_GET[suggestion]);
				$single_suggestioin = explode(" ", $suggestion_string);
				$i=0;
				$max=0;
				while (isset($single_string[$i])) {
					
					$j=0;	
					while (isset($single_suggestioin[$j])) {
						if($single_suggestioin[$j] == $single_string[$i]){
							$max++;
							echo $single_suggestioin[$j]."<br>";
						}
						$j++;
					}


					$i++;
				}
				// echo "==========<br>";
				// echo $total_string."<br>";
				// echo $_GET[suggestion]."<br>";
				// echo $max."<br>";
				// echo "==========<br>";



				for ($j=0; $j <15 ; $j++) { 

					if($item_match[$j] < $max){
						for ($k=15; $k > $j; $k--) { 
							$item_match[$k] =$item_match[$k-1];
							$total_id[$k] =$total_id[$k-1];
						}
						$item_match[$j]=$max;
						$total_id[$j] =$row[id];

						$item_cata[$row[id]] = $row[cata];
						$item_cata_id[$row[id]] = $row[cata_id];
						$item_name[$row[id]] = $row[name];
						$item_description[$row[id]] = $row[description];
						$item_price[$row[id]] = $row[price];


						break;
					}
				}

			}	
		}

		


		$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `image_link`, `description`, `column_1`, `column_2`, `column_3`, `column_4`, `column_5`, 
		`column_6`, `column_7`, `column_8`, `column_9`, `column_10`, `column_11`,  `branches`,  `main_location`, `location`
		 FROM `general_info` WHERE 1;";

		$match = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

		$id = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$cata = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$sub_cata = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$name = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$description = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		$main_location = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);


		$result = $conn->query($sql);
		if($result->num_rows>0){
			$i=0;
			while($row=$result->fetch_assoc()){
				$total_string = $row[name]." ".$row[description]." ".$row[column_1]." ".$row[column_2]." ".$row[column_3]." ".$row[column_4]." ".$row[column_5]." ".$row[column_6]." ".$row[column_7]." ".$row[column_8]." ".$row[column_9]." ".$row[column_10]." ".$row[column_11]." ".$row[column_12]." ".$row[branches]." ".$row[main_location]." ".$row[location];
				
				$total_string=str_replace(str_split('`~!@#$%^&*()_+-={}[]:"|;\\<>?,./'), " ", $total_string);
				$total_string = strtolower($total_string);
				$single_string = explode(" ", $total_string);
				
				
				
				$suggestion_string = strtolower($_GET[suggestion]);
				$single_suggestioin = explode(" ", $suggestion_string);
				$i=0;
				$max=0;
				while (isset($single_string[$i])) {
					
					$j=0;	
					while (isset($single_suggestioin[$j])) {
						if($single_suggestioin[$j] == $single_string[$i]){
							$max++;
							// echo $single_suggestioin[$j]."<br>";
						}
						$j++;
					}


					$i++;
				}
				// echo "==========<br>";
				// echo $total_string."<br>";
				// echo $_GET[suggestion]."<br>";
				// echo $max."<br>";
				// echo "==========<br>";



				for ($j=0; $j <15 ; $j++) { 
					if($item_match[$j] < $max){

						for ($k=15; $k > $j; $k--) { 

							$item_match[$k] =$item_match[$k-1];
							$total_id[$k] =$total_id[$k-1];
						}
						$total_id[$j] = $row[id];
						$item_match[$j] = $max;

						$max_type[$j] = "pro";

						$cata[$row[id]]  = $row[cata];
						$sub_cata[$row[id]] = $row[sub_cata];
						$image_link[$row[id]] = $row[image_link];
						$name[$row[id]] = $row[name];
						$description[$row[id]]   = $row[description];
						$main_location[$row[id]] = $row[main_location];

							
						break;
					}
				}

			}
		}

		for($i=0; $i<16; $i++){
			if($item_match[$i] != 0){

				$id = $total_id[$i];
				if($max_type[$i]=="item"){
					
					for ($k=$i+1; $k <16 ; $k++) { 
						if($total_id[$i] == $total_id[$k]){
							// echo ">>>>".$total_id[$i]."<<<<";
							$item_match[$k]=0;
						}
					}
							$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata, general_info.name, general_info.image_link, general_info.description, item_list.name AS item_name
							FROM `item_list` LEFT JOIN `general_info` ON item_list.cata_id = general_info.id WHERE item_list.id = '$id';";
							

							$result = $conn->query($sql);
							if($result->num_rows>0){
								while ($row = $result->fetch_assoc()) {
									$des = $row[description];
									 $des = substr($des, 0, 350);
									 if($_GET[cata] == "food" || $_GET[cata] == "electronics"){
									 }
									echo '<div class="search_result_box">
									    	<div class="image_side">
									    		<img src="image/title_image/'.$row[image_link].'" style="max-width: 120px;">		
									    	</div>
									    	<div class="data_side">
									    		<div class="bus_pro_name">
									    			<a href="'.$row[cata].'.php?sub_cata='.$row[sub_cata].'&cata='.$row[cata].'&id='.$row[id].'"><span>Item: '.$row[name].'</span></a>
									    		</div>
									    		<div  class="bus_pro_cata_sub">
									    			 <span><a href="specific.php?cata='.$row[cata].'">'.$row[cata].'</a> > <a href="specific.php?cata='.$row[cata].'&sub_cata='.$row[sub_cata].'">'.$row[sub_cata].'</a></span>
									    		</div>
									    		<div class="bus_pro_des">
									    			<span>'.$des.'</span>
									    		</div>
									    		<div class="search_item">
									    				<a class="search_item_name show-popup" href="#show" data-showpopup="54441" onclick="show_overlay(\''.$_GET[cata].'\', '.$id.')">'.$row[item_name].'</a>
									    		</div>
									    	</div>
									    </div>
									';
								}
							}
				}

				else if($max_type[$i] == "pro"){
					

				  echo '<div class="search_result_box">
					    	<div class="image_side">
					    		<img src="image/title_image/'.$image_link[$id].'" style="max-width: 120px;">		
					    	</div>
					    	<div class="data_side">
					    		<div class="bus_pro_name">
					    			<a href="'.$cata[$id].'.php?sub_cata='.$sub_cata[$id].'&cata='.$cata[$id].'&id='.$id.'"><span >'.$name[$id].'</span></a>
					    		</div>
					    		<div  class="bus_pro_cata_sub">
					    			 <span><a href="specific.php?cata='.$cata[$id].'">'.$cata[$id].'</a> > <a href="specific.php?cata='.$cata[$id].'&sub_cata='.$sub_cata[$id].'">'.$sub_cata[$id].'</a></span>
					    		</div>
					    		<div class="bus_pro_des">
					    			<span>'.$description[$id].'</span>
					    		</div>
					    		<div class="search_item">
					    			<span>'.$main_location[$id].'</span>
						    	</div>
					    	</div>
					    </div>
					    ';
				}
			}
		}
	
	} //next_suggestion()






?>
