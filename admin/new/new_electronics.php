<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>


<!DOCTYPE html>

<head>
 
 <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  
  <script src="../../js/jquery.min.js"></script>
  
  <link href="../../css/style.css" rel="stylesheet" type="text/css" media="all" />
  
  <link href="css/css_for_upload.css" rel="stylesheet" type="text/css" media="all" />
   
   <script src="../../js/self.js"> </script>

   
   <!-- // 
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js">
   </script> -->
  
  <!-- WINDOW OVERLAY -->
    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    
    <script type="text/javascript" src="../../js/win_overlay.js"></script>
    
    <link type="text/css" rel="stylesheet" href="../../css/win_overlay.css" />
  
  <!-- WINDOW OVERLAY -->

<title>
</title>







<script>
	
	function mapInput(){
		var num_branch= document.getElementById("num_branch").options;		
		var n= document.getElementById("num_branch").selectedIndex;		
		
		//alert(num_branch[n].text);
		for (var i = 1; i <= num_branch[n].text; i++) {
			// alert(i);
				var check = 0;
			 var map_pos       = $(".info");
			$(map_pos).append('<div ID="map_box" class="input_box last">'+
					'<div class="input_box">'+
						'<input class="input_field" type="text" name="main_location[]" placeholder="Main Location">'+
					'</div>'+
					'<div class="input_box"><input class="input_field" type="text" name="contact[]" placeholder="Contact"></div>'+
					'<div class="input_box">'+
						'<input class="input_field" type="text" name="full_address[]" placeholder="Full Address">'+
					'</div>'+
					'<textarea class="input_field" type="text" name="map[]" placeholder="Map"></textarea>'+
				'</div>');
		};
	}
	var x;
	function setXvalue(){
			x=-1;
		}
	
	$(document).ready(function() {
		
		// $("#sub_cata").val($("#sub_cata option:first").val()); //auto select the first option in select tag in comment section
	    var title_img       = $(".title_img");
	    var title_img_but = $(".title_img_but");

	    var y = 0; //initlal text box count
	    $(title_img_but).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(y < 1){ //max input box allowed
	            y++; //text box increment
	            $(title_img).append('<div class="title_image">'+
			            '<iframe src="../upload_crop/upload_crop.php?type=title_image">'+
			            '</iframe>'+
	            	'</div>'); 
	        }
	    });



		
		var main_image = $(".main_image");
		var main_img_but = $(".main_img_but");
		var z=-1;
		$(main_img_but).click(function(e){ //on add input button click
	        
	        e.preventDefault();
	        if(z < 10){ //max input box allowed
	            z++; //text box increment
	            $(main_image).append('<div class="col-sm-6">'+
			          '  <div class="title_image">'+
				            '<iframe src="../upload_crop/upload_crop.php?type=main_image&main_image_num='+z+'"></iframe>'+
			            '</div>'+
		           ' </div>');
	        }
	    });



		var imageShape="square";
	    $('select').on('change', function() {
		  imageShape=this.value ;
		});



	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_mobile_button"); //Add button ID
	    
	    // //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Mobile No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="mobile">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+

					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="sim['+x+']" placeholder="SIM: single/multi. nano or not">'+
			        	'<input class="" type="text" name="ant['+x+']" placeholder="ANT+: yes/not">'+
			        	'<input class="" type="text" name="usb['+x+']" placeholder="USB VERSION: ?">'+
			        	'<input class="" type="text" name="earjack['+x+']" placeholder="EarJack: head phone pin">'+
			        	'<input class="" type="text" name="mhl['+x+']" placeholder="MHL: mobile to monitor port(yes or not)">'+
			        	'<input class="" type="text" name="wifi['+x+']" placeholder="Wi-Fi">'+
			        	'<input class="" type="text" name="bluetooth['+x+']" placeholder="Bluetooth Version">'+
			        	'<input class="" type="text" name="os['+x+']" placeholder="Operating System">'+
			        	'<input class="" type="text" name="tech_size['+x+']" placeholder="Display Techonology and Size">'+
			        	'<input class="" type="text" name="res_color['+x+']" placeholder="Display Resolution and color Depth">'+
			        	'<input class="" type="text" name="pen['+x+']" placeholder="S Pen Support or not">'+
			        	'<input class="" type="text" name="cpu['+x+']" placeholder="processor type and speed">'+
			        	'<input class="" type="text" name="ram['+x+']" placeholder="Ram size and details">'+
			        	'<input class="" type="text" name="rear_cam['+x+']" placeholder="Rear camera">'+
			        	'<input class="" type="text" name="front_cam['+x+']" placeholder="Front Camera">'+
			        	'<input class="" type="text" name="flash['+x+']" placeholder="Camera Flash">'+
			        	'<input class="" type="text" name="sensor['+x+']" placeholder="Sensor">'+
			        	'<input class="" type="text" name="dimension_weight['+x+']" placeholder="Dimention Size">'+
			        	'<input class="" type="text" name="bat_capacity['+x+']" placeholder="Battery capacity">'+
			        	'<input class="" type="text" name="internet_usage_time['+x+']" placeholder="Internet Usage Time (Battery)">'+
			        	'<input class="" type="text" name="talk_time['+x+']" placeholder="Talk Time (Battery)">'+
			        	'<input class="" type="text" name="removable['+x+']" placeholder="Battery Removable or not">'+
			        	'<input class="" type="text" name="video_format_resolution['+x+']" placeholder="Video playing Format and">'+
			        	'<input class="" type="text" name="audio_format['+x+']" placeholder="Audio playing Formate">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });







		var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_laptop_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Laptop No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="laptop">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	
			        	'<input class="" type="text" name="processor['+x+']" placeholder="Processor model">'+
			        	'<input class="" type="text" name="clock_speed['+x+']" placeholder="Processor clock speed">'+
			        	'<input class="" type="text" name="cache['+x+']" placeholder="Cache memory">'+
			        	'<input class="" type="text" name="display['+x+']" placeholder="Display type and size">'+
			        	'<input class="" type="text" name="ram['+x+']" placeholder="Ram size and type">'+
			        	'<input class="" type="text" name="storage['+x+']" placeholder="Storage">'+
			        	'<input class="" type="text" name="graphics['+x+']" placeholder="Graphics memory and model">'+
			        	'<input class="" type="text" name="optical_device['+x+']" placeholder="DVD/CD drive">'+
			        	'<input class="" type="text" name="display_port['+x+']" placeholder="Display Port">'+
						'<input class="" type="text" name="audio_port['+x+']" placeholder="Audio Port">'+
						'<input class="" type="text" name="usb['+x+']" placeholder="USB Version ">'+
						'<input class="" type="text" name="battery['+x+']" placeholder="Battery Type">'+
						'<input class="" type="text" name="backup['+x+']" placeholder="Battery back up time">'+
						'<input class="" type="text" name="os['+x+']" placeholder="Operating System">'+
						'<input class="" type="text" name="weight['+x+']" placeholder="Weight">'+
						'<input class="" type="text" name="network['+x+']" placeholder="Network(wifi/bluetooth/lan)">'+
						'<input class="" type="text" name="web_cam['+x+']" placeholder="Web Cam">'+
						'<input class="" type="text" name="finger_print['+x+']" placeholder="Finger Print">'+
						'<input class="" type="text" name="color['+x+']" placeholder="Color">'+
						'<input class="" type="text" name="other['+x+']" placeholder="Other">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });





		var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_computer_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Computer No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="computer">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
						'<input class="" type="text" name="processor['+x+']" placeholder="Processor model">'+
						'<input class="" type="text" name="clock_speed['+x+']" placeholder="Processor clock speed">'+
						'<input class="" type="text" name="cache['+x+']" placeholder="Cache memory">'+
						'<input class="" type="text" name="mother_board['+x+']" placeholder="Mother Board">'+
						'<input class="" type="text" name="display['+x+']" placeholder="Display type and size">'+
						'<input class="" type="text" name="ram['+x+']" placeholder="Ram size and type">'+
						'<input class="" type="text" name="storage['+x+']" placeholder="Storage(hard disk)">'+
						'<input class="" type="text" name="graphics['+x+']" placeholder="Graphics memory and model">'+
						'<input class="" type="text" name="optical_device['+x+']" placeholder="DVD/CD drive">'+
						'<input class="" type="text" name="display_port['+x+']" placeholder="Display Port">'+
						'<input class="" type="text" name="Speaker['+x+']" placeholder="Speaker">'+
						'<input class="" type="text" name="audio_port['+x+']" placeholder="Audio Port">'+
						'<input class="" type="text" name="keyboard['+x+']" placeholder="Keyboard">'+
						'<input class="" type="text" name="mouse['+x+']" placeholder="Mouse">'+
						'<input class="" type="text" name="usb['+x+']" placeholder="USB Version ">'+
						'<input class="" type="text" name="os['+x+']" placeholder="Operating System">'+
						'<input class="" type="text" name="network['+x+']" placeholder="Network(wifi/bluetooth/lan)">'+
						'<input class="" type="text" name="turbo_boost['+x+']" placeholder="Turbo Boost technology">'+
						'<input class="" type="text" name="other['+x+']" placeholder="Other">'+
					'</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });

		var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_processor_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Processor No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="processor">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="generation['+x+']" placeholder="generetion">'+
			        	'<input class="" type="text" name="cores['+x+']" placeholder="Cores">'+
			        	'<input class="" type="text" name="threads['+x+']" placeholder="Threads">'+
			        	'<input class="" type="text" name="clock_speed['+x+']" placeholder="Clock Speed">'+
			        	'<input class="" type="text" name="max_turbo_frequency['+x+']" placeholder="max turbo frequency">'+
			        	'<input class="" type="text" name="cache['+x+']" placeholder="cache">'+
			        	'<input class="" type="text" name="instruction_set['+x+']" placeholder="instruction set 64/32 bit">'+
			        	'<input class="" type="text" name="lithography['+x+']" placeholder="lithography">'+
			        	'<input class="" type="text" name="max_memory_size['+x+']" placeholder="max memory size">'+
						'<input class="" type="text" name="turbo_boost_techonology['+x+']" placeholder="turbo boost techonology">'+
						'<input class="" type="text" name="hyper_threading_techonology['+x+']" placeholder="hyper threading techonology">'+
						'<input class="" type="text" name="trusted_execution_techonology['+x+']" placeholder="trusted execution techonology">'+
						'<input class="" type="text" name="integreted_graphics['+x+']" placeholder="integreted graphics">'+
						'<input class="" type="text" name="socket_support['+x+']" placeholder="Socket Suppotrs">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });


		var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_mother_board_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Mother Board No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="mother_board">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input class="" type="text" name="form_factor['+x+']" placeholder="Form Factor">'+
			        	'<input class="" type="text" name="socket['+x+']" placeholder="Socket">'+
			        	'<input class="" type="text" name="chipset['+x+']" placeholder="Chipset">'+
			        	'<input class="" type="text" name="supported_cpu['+x+']" placeholder="Supported Cpu">'+
			        	'<input class="" type="text" name="ram_type_size['+x+']" placeholder="Ram Type and Size">'+
			        	'<input class="" type="text" name="ram_bus['+x+']" placeholder="Ram BUS">'+
			        	'<input class="" type="text" name="ram_slot['+x+']" placeholder="Ram Slot">'+
						'<input class="" type="text" name="pci_slot['+x+']" placeholder="pci_slot">'+
						'<input class="" type="text" name="sata_port['+x+']" placeholder="Sata Port">'+
						'<input class="" type="text" name="lan_speed['+x+']" placeholder="Lan Speed">'+
						'<input class="" type="text" name="usb_port['+x+']" placeholder="USB Port">'+
						'<input class="" type="text" name="bios['+x+']" placeholder="Bios">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });

			

		var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_graphics_card_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Grphics Card No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="graphics_card">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="chipset['+x+']" placeholder="Chipset">'+
			        	'<input class="" type="text" name="interfaces['+x+']" placeholder="Interface">'+
			        	'<input class="" type="text" name="gpu_clock['+x+']" placeholder="GPU Clock Speed">'+
			        	'<input class="" type="text" name="memory_and_type['+x+']" placeholder="Memory Size and Type">'+
			        	'<input class="" type="text" name="memory_bus['+x+']" placeholder="Memory bus">'+
			        	'<input class="" type="text" name="memory_clock['+x+']" placeholder="Memory Clock">'+
			        	'<input class="" type="text" name="resolution['+x+']" placeholder="Resolution">'+
			        	'<input class="" type="text" name="directx['+x+']" placeholder="Supported Direct X">'+
			        	'<input class="" type="text" name="port['+x+']" placeholder="Port(HDMI/DVI-I or DVI-D/VGA)">'+
						'<input class="" type="text" name="power['+x+']" placeholder="Power Supply required ">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });


	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_ram_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>RAM No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="ram">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input class="" type="text" name="for_lap_desk['+x+']" placeholder="For(Desktop/Laptop)">'+
			        	'<input class="" type="text" name="memory_type['+x+']" placeholder="Memory Type">'+
			        	'<input class="" type="text" name="bus_speed['+x+']" placeholder="Bus Speed">'+
			        	'<input class="" type="text" name="pin_number['+x+']" placeholder="Pin number">'+
			        	'<input class="" type="text" name="heat_sink['+x+']" placeholder="Heat Sink">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });



	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_storage_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Storage No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="storage">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<fieldset>'+
			        	'<input type="radio" name="position_type['+x+']" value="internal"> Internal'+
					    '<input type="radio" name="position_type['+x+']" value="external"> External<br>'+
					    '</fieldset>'+
					    '<fieldset>'+
					    '<input type="radio" name="storage_type['+x+']" value="hdd"> HDD'+
					    '<input type="radio" name="storage_type['+x+']" value="ssd"> SSD<br>'+
					    '</fieldset>'+
			        	'<input class="" type="text" name="for_lap_desk['+x+']" placeholder="For(desktop,laptop)">'+
			        	'<input class="" type="text" name="storage['+x+']" placeholder="Storage">'+
			        	'<input class="" type="text" name="rpm['+x+']" placeholder="RPM">'+
			        	'<input class="" type="text" name="seek_time['+x+']" placeholder="Seek Time">'+
			        	'<input class="" type="text" name="buffer['+x+']" placeholder="Buffer">'+
			        	'<input class="" type="text" name="from_factor['+x+']" placeholder="From Factor">'+
			        	'<input class="" type="text" name="transfer_rate['+x+']" placeholder="Transfer Rate">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });



	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_ram_button"); //Add button ID
	    
	     //initlal text box count
	     /*
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>RAM No: '+x+'</h1>'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="">'+
			        	'<input type="radio" name="type['+x+']" value="hdd"> LCD'+
					    '<input type="radio" name="type['+x+']" value="ssd"> LED<br>'+
					    '<input class="" type="text" name="size['+x+']" placeholder="">'+
					    '<input class="" type="text" name="screen_type['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="resolution['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="contrast_ratio['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="viewing_angel['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="brightness['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="port['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="power_adapter['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="voltage['+x+']" placeholder="">'+
			        	'<input class="" type="text" name="response_time['+x+']" placeholder="">'+
						'<input class="" type="text" name="refresh_rate['+x+']" placeholder="">'+
						'<input class="" type="text" name="dimension['+x+']" placeholder="">'+
						'<input class="" type="text" name="weight['+x+']" placeholder="">'+
			        '</div>'+
				'</div>'); 
	        }
	    });

		*/


	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_monitor_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>MONITOR No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="monitor">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input type="radio" name="type['+x+']" value="hdd"> LCD'+
					    '<input type="radio" name="type['+x+']" value="ssd"> LED<br>'+
					    '<input class="" type="text" name="size['+x+']" placeholder="Size">'+
					    '<input class="" type="text" name="screen_type['+x+']" placeholder="Screen (wide/ultra-wides/squre)">'+
			        	'<input class="" type="text" name="resolution['+x+']" placeholder="Resolution">'+
			        	'<input class="" type="text" name="contrast_ratio['+x+']" placeholder="Contrast Ratio">'+
			        	'<input class="" type="text" name="viewing_angel['+x+']" placeholder="Viewing">'+
			        	'<input class="" type="text" name="brightness['+x+']" placeholder="Brightness">'+
			        	'<input class="" type="text" name="port['+x+']" placeholder="Port">'+
			        	'<input class="" type="text" name="power_adapter['+x+']" placeholder="Power Adapter">'+
			        	'<input class="" type="text" name="voltage['+x+']" placeholder="Voltage">'+
			        	'<input class="" type="text" name="response_time['+x+']" placeholder="Response Time">'+
						'<input class="" type="text" name="refresh_rate['+x+']" placeholder="Refresh Rate">'+
						'<input class="" type="text" name="dimension['+x+']" placeholder="Dimension">'+
						'<input class="" type="text" name="weight['+x+']" placeholder="Weight">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });





	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_camera_button"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>CAMERA No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="camera">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input type="radio" name="type['+x+']" value="dslr"> DSLR'+
			        	'<input type="radio" name="type['+x+']" value="hdd"> SLR'+
			        	'<input type="radio" name="type['+x+']" value="hdd"> COMPACT'+
					    '<input type="radio" name="type['+x+']" value="ssd"> VIDEO<br>'+
			        	'<input class="" type="text" name="image_sensor['+x+']" placeholder="Image Sensor">'+
			        	'<input class="" type="text" name="processor['+x+']" placeholder="Processor">'+
			        	'<input class="" type="text" name="auto_focus['+x+']" placeholder="Auto Focus">'+
			        	'<input class="" type="text" name="iso['+x+']" placeholder="ISO">'+
			        	'<input class="" type="text" name="shutter['+x+']" placeholder="Shutter">'+
			        	'<input class="" type="text" name="flash['+x+']" placeholder="Flash">'+
			        	'<input class="" type="text" name="resolution['+x+']" placeholder="Resolution">'+
			        	'<input class="" type="text" name="lens['+x+']" placeholder="Lens">'+
						'<input class="" type="text" name="weight['+x+']" placeholder="Weight">'+
						'<input class="" type="text" name="sensor_type['+x+']" placeholder="Sensor">'+
						'<input class="" type="text" name="display['+x+']" placeholder="Display">'+
						'<input class="" type="text" name="video['+x+']" placeholder="Video">'+
						'<input class="" type="text" name="interfaces['+x+']" placeholder="Interface">'+
						'<input class="" type="text" name="memory['+x+']" placeholder="Memory">'+
						'<input class="" type="text" name="battery['+x+']" placeholder="Battery">'+
						'<input class="" type="text" name="dimension['+x+']" placeholder="Dimension">'+
						'<input class="" type="text" name="light_sensor['+x+']" placeholder="Light Sensor">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });




	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_opticle_drive"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>OPTICLE DRIVE No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="optical_drive">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input class="" type="text" name="type['+x+']" placeholder="Type(cd/dvd)">'+
			        	'<input class="" type="text" name="intreface['+x+']" placeholder="Interface">'+
			        	'<input class="" type="text" name="buffer['+x+']" placeholder="Buffer rate">'+
			        	'<input class="" type="text" name="read_speed['+x+']" placeholder="Read Speed">'+
			        	'<input class="" type="text" name="form_factor['+x+']" placeholder="Form Factor">'+
			        	'<input class="" type="text" name="access_time['+x+']" placeholder="Access time">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });




	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_power_supply"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>Power Supply No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="power_supply">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input class="" type="text" name="maximum_power['+x+']" placeholder="Maximum Power">'+
			        	'<input class="" type="text" name="input_voltage['+x+']" placeholder="Input Voltage">'+
			        	'<input class="" type="text" name="input_current['+x+']" placeholder="Input Current">'+
			        	'<input class="" type="text" name="oprating_temperature['+x+']" placeholder="Operating Temperature">'+
			        	'<input class="" type="text" name="short_description['+x+']" placeholder="Short Description">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });



	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_mouse"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>MOUSE No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="mouse">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input class="" type="text" name="interfaces['+x+']" placeholder="Interface(usb/ps2/wireless)">'+
			        	'<input class="" type="text" name="num_of_buttons['+x+']" placeholder="Number of Buttons">'+
			        	'<input class="" type="text" name="wheel['+x+']" placeholder="Wheel">'+
			        	'<input class="" type="text" name="dpi['+x+']" placeholder="DPI">'+
			        	'<input class="" type="text" name="dimension['+x+']" placeholder="Dimension">'+
			        	'<input class="" type="text" name="weight['+x+']" placeholder="Weight">'+
			        	'<input class="" type="text" name="short_description['+x+']" placeholder="Short Description">'+
			        '</div>'+
				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });



	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_keyboard"); //Add button ID
	    
	     //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-12 one_item_list ">'+
	            	'<h1>KEYBOARD No: '+x+'</h1>'+
	            	'<input type="hidden" name="what_item['+x+']" value="keyboard">'+
					'<div class="col-sm-5">'+
				            '<div class="item_list_info item_list_elec">'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">'+
					            '</div>'+
					            '<div class="input_box input_box_elec">'+
					            	'<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>'+
					            '</div>'+
				            '</div>'+
				            '<input type="radio" name="item_time_status['+x+']" value="running"> Running'+
							'<input type="radio" name="item_time_status['+x+']" value="new"> New'+
							'<input type="radio" name="item_time_status['+x+']" value="coming"> Coming'+
				            '<div class="item_image">'+
				            	'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
				            '</div>'+
			        '</div>'+
			        '<div class="col-sm-7 specification">'+
			        	'<input class="" type="text" name="brand['+x+']" placeholder="Brand">'+
			        	'<input class="" type="text" name="type['+x+']" placeholder="Type(Multimedia or not)">'+
			        	'<input class="" type="text" name="interfaces['+x+']" placeholder="Interface(usb/ps2/wireless)">'+
			        	'<input class="" type="text" name="multimedia_key['+x+']" placeholder="Multimedia Keys">'+
			        	'<input class="" type="text" name="weight['+x+']" placeholder="Weight">'+
			        	'<input class="" type="text" name="dimension['+x+']" placeholder="Dimension">'+
			        	'<input class="" type="text" name="color['+x+']" placeholder="Color">'+
			        	'<input class="" type="text" name="graphical_ui['+x+']" placeholder="Graphical UI">'+
			        	'<input class="" type="text" name="on_board_memory['+x+']" placeholder="On Board Memory">'+
			        	'<input class="" type="text" name="back_light['+x+']" placeholder="Back Light">'+
			        	'<input class="" type="text" name="cable_length['+x+']" placeholder="Cable Length">'+
						'<input class="" type="text" name="short_description['+x+']" placeholder="Short Description">'+
			        '</div>'+

				'</div>'+
				'<a href="#0" class="more_image">Add more Item image</a>'); //add input box
	        }
	    });

	     var wrapper2         = $(".item_list"); //Fields wrapper
	    var d = -1; 

	    $(document).on("click", '.more_image', function(event) { 
		   		d++; //text box increment
	           	$(wrapper2).append('<div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_multi_image&imageShape='+imageShape+'&item_num='+x+'&item_multi_num='+d+'"></iframe></div>');
	        
		});


	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })
	});


</script>
</head>




<body onload="setXvalue()">

<div class = "container food">


	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>
	<div class="col-sm-12 field">
	<?php
			$update_option=$_GET[update_option];
			if(!isset($update_option)){
			
				
				echo "actual Link".$actual_link;
				echo "<form method='get' action='".$actual_link."'>
						<input type='hidden' name='cata' value='".$_GET[cata]."'>
						<input type='hidden' name='sub_cata' value='".$_GET[sub_cata]."'>
						<input type='hidden' name='update_option' value='new'>
						<input type='submit' value='NEW'>
					</form>";
				echo "<form method='get' action='".$actual_link."'>
						<input type='hidden' name='cata' value='".$_GET[cata]."'>
						<input type='hidden' name='sub_cata' value='".$_GET[sub_cata]."'>
						<input type='text' name='content_id' placeholder='Content ID you want to UPDATE'>
						<input type='hidden' name='update_option' value='update'>
						<input type='submit' value='UPDATE'>
					</form>";
			}

		if($update_option == "new" && isset($update_option)){

	?>
	<form method="POST" action="../upload_data.php?cata=<?php echo $_GET[cata] ?>&sub_cata=<?php echo $_GET[sub_cata] ?>" >
			<div class="elec_sub"><input class="input_field" type="text" name="elec_sub_cata" placeholder="SUB CATAGORY NAMES"></div>
			<div class="col-sm-12 info_title_img">
				<h2>Main Information and Title Image</h2>
				<div class="col-sm-4 info">
					<div class="input_box"><input class="input_field" type="text" name="name" placeholder="Name"></div>
					<div class="input_box"><input class="input_field" type="text" name="product_type" placeholder="Product Type"></div>
					<div class="input_box"><textarea class="input_field" type="text" name="description" placeholder="Description"></textarea></div>

					<div class="input_box"><input class="input_field" type="text" name="services" placeholder="Services"></div>
					<div class="input_box"><input class="input_field" type="text" name="warranty" placeholder="Warranty Policy"></div>
					<div class="input_box"><input class="input_field" type="text" name="servicing_center" placeholder="Servicing Center"></div>
					<div class="input_box"><input class="input_field" type="text" name="help_line" placeholder="Help Line"></div>

					<div class="input_box"><input class="input_field" type="text" name="web_site" placeholder="Web Site"></div>
					<div class="input_box"><input class="input_field" type="text" name="social_links" placeholder="Social Links"></div>
					<div class="input_box"><input class="input_field" type="text" name="main_contact" placeholder="Contact"></div>
						
						<select id="num_branch" name="num_branch" onchange="mapInput()">
							<option >Select Number</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="10">20</option>
						</select>
						<span>Select Number of Branches</span>
				</div>

				<div class="col-sm-8 title_img ">
					<button class="title_img_but">UP TITLE IMAGE</button>
				</div>
			</div>

			<div class="col-sm-12 main_image">
				<h2>Main Image (Slide Show)</h2>
				<button class="main_img_but">UP MAIN IMAGE</button>

			</div>
		
			
		
		<div class="col-sm-12 item_list electronic">
				<!-- <div class="col-sm-12 one_item_list ">
					<div class="col-sm-5">
				            <div class="item_list_info item_list_elec">
					            <div class="input_box input_box_elec">
					            	<input class="input_field item_name" type="text" name="item_name['+x+']" placeholder="ITEM NAME/MODEL">
					            </div>
					            <div class="input_box input_box_elec">
					            	<input class="input_field item_price" type="text" name="item_price['+x+']" placeholder="ITEM PRICE">
					            </div>
					            <div class="input_box input_box_elec">
					            	<textarea class="input_field item_des" type="text" name="item_description['+x+']" placeholder="ITEM DESCRIPTION"></textarea>
					            </div>
				            </div>
				            <div class="item_image">
				            	<iframe src="../upload_crop/upload_crop.php?type=item_image&item_num="></iframe>
				            </div>
			        </div>
			        <div class="col-sm-7 specification">
			        	<input class="" type="text" name="sim['+x+']" placeholder="SIM: single/multi. nano or not">
			        	<input class="" type="text" name="ant['+x+']" placeholder="ANT+: yes/not">
			        	<input class="" type="text" name="usb['+x+']" placeholder="USB VERSION: ?">
			        	<input class="" type="text" name="earjack['+x+']" placeholder="EarJack: head phone pin">
			        	<input class="" type="text" name="mhl['+x+']" placeholder="MHL: mobile to monitor port(yes or not)">
			        	<input class="" type="text" name="wifi['+x+']" placeholder="Wi-Fi">
			        	<input class="" type="text" name="bluetooth['+x+']" placeholder="Bluetooth Version">
			        	<input class="" type="text" name="os['+x+']" placeholder="Operating System">
			        	<input class="" type="text" name="tech_size['+x+']" placeholder="Display Techonology and Size">
			        	<input class="" type="text" name="res_color['+x+']" placeholder="Display Resolution and color Depth">
			        	<input class="" type="text" name="pen['+x+']" placeholder="S Pen Support or not">
			        	<input class="" type="text" name="cpu['+x+']" placeholder="processor type and speed">
			        	<input class="" type="text" name="ram['+x+']" placeholder="Ram size and details">
			        	<input class="" type="text" name="rear_cam['+x+']" placeholder="Rear camera">
			        	<input class="" type="text" name="front_cam['+x+']" placeholder="Front Camera">
			        	<input class="" type="text" name="flash['+x+']" placeholder="Camera Flash">
			        	<input class="" type="text" name="sensor['+x+']" placeholder="Sensor">
			        	<input class="" type="text" name="dimension_weight['+x+']" placeholder="Dimention Size">
			        	<input class="" type="text" name="bat_capacity['+x+']" placeholder="Battery capacity">
			        	<input class="" type="text" name="internet_usage_time['+x+']" placeholder="Internet Usage Time (Battery)">
			        	<input class="" type="text" name="talk_time['+x+']" placeholder="Talk Time (Battery)">
			        	<input class="" type="text" name="removable['+x+']" placeholder="Battery Removable or not">
			        	<input class="" type="text" name="video_format_resolution['+x+']" placeholder="Video playing Format and">
			        	<input class="" type="text" name="audio_format['+x+']" placeholder="Audio playing Formate">
			        </div>
				</div> -->

		</div>
		<select id="image_s" onchange="getImageSize()">
				<option value="square">square</option>
				<option value="landscape">landscape</option>
				<option value="portrait">portrait</option>
		</select>

		<button class="add_mobile_button">Add Mobile</button>
		<button class="add_laptop_button">Add Laptop</button>
		<button class="add_computer_button">Add Computer</button>
		<button class="add_processor_button">Add Processor</button>
		<button class="add_mother_board_button">Add Mother board</button>
		<button class="add_graphics_card_button">Add Graphics Card board</button>
		<button class="add_ram_button">Add RAM</button>
		<button class="add_storage_button">Add Storage</button>
		<button class="add_monitor_button">Add Monitor</button>
		<button class="add_camera_button">Add Camera</button>
		<button class="add_opticle_drive">Add Opticle Drive</button>
		<button class="add_power_supply">Add Power Supply</button>
		<button class="add_mouse">Add Mouse</button>
		<button class="add_keyboard">Add KeyBoard</button>
		<br>
		<button class="add_more_item_image">Add more Item image</button>
		<div class="submit_button">
			<input type="submit" value="SUBMIT">
		</div>
	</form>



	<?php
			}
			$content_id = $_GET[content_id];
			if($update_option == "update" && isset($update_option) && !empty($content_id)){
			echo "<h3>UPDATE CONTENT: ".$_GET[content_id]."</h3>";
	?>
			
			<form method="POST" action="../upload_data.php?cata=<?php echo $_GET[cata] ?>&sub_cata=<?php echo $_GET[sub_cata] ?>&content_id=<?php echo $content_id ?>" >
				<div class="col-sm-12 info_title_img">
					

					<div class="col-sm-8 title_img ">
						<button class="title_img_but">UP TITLE IMAGE</button>
					</div>
				</div>

			<div class="info">
			</div>

			<select id="num_branch" name="num_branch" onchange="mapInput()">
						<option >Select Number</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="10">20</option>
			</select>
			<span>Select Number of Branches</span>

				<div class="col-sm-12 main_image">
					<h2>Main Image (Slide Show)</h2>
					<button class="main_img_but">UP MAIN IMAGE</button>

				</div>
			
				<div class="col-sm-12 item_list">
				</div>
				
				<select id="image_s" onchange="getImageSize()">
					<option value="square">square</option>
					<option value="landscape">landscape</option>
					<option value="portrait">portrait</option>
				</select>

				<button class="add_mobile_button">Add Mobile</button>
				<button class="add_laptop_button">Add Laptop</button>
				<button class="add_computer_button">Add Computer</button>
				<button class="add_processor_button">Add Processor</button>
				<button class="add_mother_board_button">Add Mother board</button>
				<button class="add_graphics_card_button">Add Graphics Card board</button>
				<button class="add_ram_button">Add RAM</button>
				<button class="add_storage_button">Add Storage</button>
				<button class="add_monitor_button">Add Monitor</button>
				<button class="add_camera_button">Add Camera</button>
				<button class="add_opticle_drive">Add Opticle Drive</button>
				<button class="add_power_supply">Add Power Supply</button>
				<button class="add_mouse">Add Mouse</button>
				<button class="add_keyboard">Add KeyBoard</button>
				<br>
				<button class="add_more_item_image">Add more Item image</button>
				<div class="submit_button">
					<input type="submit" value="SUBMIT">
				</div>
		</form>
	<?php
		}

	?>



	</div>





</div>
<!-- END OF container -->
<div class="admin_footer">
	
</div>
</body>
</html>