<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>
	<!DOCTYPE html>
<head>

<title></title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script language="javascript" type="text/javascript">
				  function resizeIframe(obj) {
				  	var offsetHeight = document.getElementById('info_height').offsetHeight;
				  	//document.getElementById("frame_height").innerHTML = offsetHeight;
				  	//window.alert(offsetHeight);
				  	offsetHeight = offsetHeight -25;
				    obj.style.height = offsetHeight + 'px';
				   //obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
				  }

		$(document).ready(function(){
			


			 // For life Style Image Update
				// for image shape
			var imageShape="square";
			    $('select').on('change', function() {
				  imageShape=this.value ;
				});
		    var title_img_but = $(".tourism_map_but");
		    var where = $(".tourism_map");

		    var y = 0; //initlal text box count
		    $(title_img_but).click(function(e){ //on add input button click

		        e.preventDefault();
		        if(y < 1){ //max input box allowed
		            y++; //text box increment
		            $(where).append('<div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num=0"></iframe></div>'); //add input box
		        }
		    });

		    var title_img_but2 = $(".tourism_map_but2");
		    var where2 = $(".tourism_map2");

		    var y = 0; //initlal text box count
		    $(title_img_but2).click(function(e){ //on add input button click

		        e.preventDefault();
		        if(y < 1){ //max input box allowed
		            y++; //text box increment
		            $(where2).append('<div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num=0"></iframe></div>'); //add input box
		        }
		    });
		});
				 
</script>
</head>

<body>

<div class = "container">

	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>


	<div class="col-sm-12 field wide_pattern">
		<div class="col-sm-10">  <!-- Whole information without add -->
			
			<form method='get' action="new_<?php echo $_GET[cata]; ?>.php">
				<input type='hidden' name='cata' value="<?php echo $_GET[cata]; ?>">
				<input type='hidden' name='sub_cata' value="<?php echo $_GET[sub_cata]; ?>">
				<input type='hidden' name="content_id" value="<?php echo $_GET[id]; ?>">
				<input type='hidden' name='update_option' value='update'>
				<input type='submit' value='Advance UPDATE'>
			</form>

			<div class="col-sm-12 first">    <!-- FOR NAME -->
				<div class="info_box">
					<!-- <div class="title">
						<span>Name</span>
					</div> -->
					<div class="title_info">
						<span>
							<?php
								$general_obj = new general();
								$general_obj->general_name($_GET[cata], $_GET[sub_cata], $_GET[id]);
							?>
						</span>
					</div>
				</div>
			</div><!-- FOR NAME -->

		 	<!-- PHOTO GALLARY -->
<!-- 			<div class="col-sm-12 second" >   
   				<div >
	   				<?php 
	   					//include 'slide-show.php';
					 ?>
				</div>
	   		</div>	 -->		
	   		<!-- PHOTO GALLARY -->
	   					<!-- PHOTO GALLARY -->
			<div class="col-sm-12 second" >   
   				<div >
	   				<?php 
	   					 // include 'edu-slide-show.php';
	   					$general_obj = new general();
	   					 include 'slide-show.php';
					 ?>
				</div>
	   		</div>			
	   		<!-- PHOTO GALLARY -->
	   		
	   		<div class="col-sm-8 first" > 
				<div class="col-sm-12 first" ID="info_height">  <!-- NORMAL INFORMATION  -->
					<?php
						$general_obj = new general();
						$general_obj->general_info($_GET[cata], $_GET[sub_cata], $_GET[id]);

						$general_obj->general_map_location($_GET[cata], $_GET[sub_cata], $_GET[id]);					
						$general_obj->general_map($_GET[cata], $_GET[sub_cata], $_GET[id]);					
					?>	

				</div>   <!-- NORMAL INFORMATION  -->
	   		</div>

	   		<!-- HIGHLIGHTED MAP / HOTEL NEARBY / RESTAURANT / TRANSPORT / DISTANCE CHART -->
	   		<!-- <div class="col-sm-4 first">  -->


	   			<?php
						$general_obj->tourismMapAndVideo($_GET[cata], $_GET[sub_cata], $_GET[id]);
				?>	
	   			<!-- <div class="col-sm-4 spot-in-map">
	   				<img src="../../content_image/tourism/bd map/bdmap.png" class="img-responsive" alt="MAP OF BANGLADESH">
	   			</div>

	   			<div class="col-sm-4 spot-in-map">
	   				<img src="../../content_image/tourism/bd map/bdmap.png" class="img-responsive" alt="MAP OF BANGLADESH">
	   			</div> -->

	   			<div class="clearfix"> </div>


	   			<div class="attraction_title">
	   					<span>TOURIST ATTRACTION</span>
	   			</div>

	   			<div class="attraction_iframe">
	   				<iframe src="tourist_attraction.php?cata=<?php echo $_GET[cata]; ?>&sub_cata=<?php echo $_GET[sub_cata]; ?>&id=<?php echo $_GET[id]; ?>"></iframe> 
	   				<?php
	   					// include 'tourist_attraction.php';
	   					
	   				?>
	   			</div>


	   			<div class="col-sm-12 nearby_panel">
		   			<div class="col-sm-4 nearby_left">
		   				<span class="nearby_title">Hotel Nearby</span>


		   				<?php
		   					$nearby = new general();
		   					$nearby->getNearbyInfo($_GET[id], 'hotel');
		   				?>
		   				
		   			</div>

		   			<div class="col-sm-4 nearby_right">
		   				<span class="nearby_title">Restaurant Nearby</span>

		   				<?php
		   					$nearby = new general();
		   					$nearby->getNearbyInfo($_GET[id], 'restaurant');
		   				?>
		   				<!-- <div class="nearby_box">
		   					<div class="nearby_img">
		   						<img class="img-responsive" src="image/title_image/40_food_cafe.jpg">
		   					</div>
		   					<div class="nearby_data">
		   						<a href=""><span>Name</span></a><br>
		   						<span>Type</span><br>
		   						<span>Full Address</span>
		   					</div>
		   				</div>	 -->
		   			</div>
		   			<div class="col-sm-4 nearby_right">
		   				<span class="nearby_title">google Add</span>
		   			</div>
	   			</div>
	   		<!-- </div> -->
	   		<!-- HIGHLIGHTED MAP / HOTEL NEARBY / RESTAURANT / TRANSPORT / DISTANCE CHART -->
			
				<!-- <div class="clearfix"></div> -->

				<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
										
				</div>   <!-- COMMENT AND QUESTIOIN -->


		</div> <!-- Whole information without add -->


		<!-- add -->
		<div class="col-sm-4 third">  
			<div class="add">
				<?php
					$general_obj->seoData($_GET[cata], $_GET[sub_cata], $_GET[id]);
				?>
			</div>
		</div>
	</div>

	


</div>
<!-- END OF container -->

<div class="footer">
	
</div>
</body>
</html>