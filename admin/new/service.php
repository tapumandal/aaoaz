<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>

<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="../../js/jquery.min.js"></script>
  <link href="../../css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/css_for_upload.css" rel="stylesheet" type="text/css" media="all" />
   <script src="../../js/self.js"></script>

   <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> -->
  <!-- WINDOW OVERLAY -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/win_overlay.js"></script>
    <link type="text/css" rel="stylesheet" href="../../css/win_overlay.css" />
  <!-- WINDOW OVERLAY -->
<title></title>
</head>


<body>
<script>
	 $(document).ready(function() {
		// $("#sub_cata").val($("#sub_cata option:first").val()); //auto select the first option in select tag in comment section
	    var title_img       = $(".title_img");
	    var title_img_but = $(".title_img_but");

	    var y = 0; //initlal text box count
	    $(title_img_but).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(y < 1){ //max input box allowed
	            y++; //text box increment
	            $(title_img).append('<div class="title_image"><iframe src="../upload_crop/upload_crop.php?type=title_image"></iframe></div>'); //add input box
	        }
	    });



		
		var main_image = $(".main_image");
		var main_img_but = $(".main_img_but");
		var z=-1;
		$(main_img_but).click(function(e){ //on add input button click
	        
	        e.preventDefault();
	        if(z < 10){ //max input box allowed
	            z++; //text box increment
	            $(main_image).append('<div class="col-sm-6"><div class="title_image"><iframe src="../upload_crop/upload_crop.php?type=main_image&main_image_num='+z+'"></iframe></div></div>'); //add input box
	        }
	    });







	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_field_button"); //Add button ID
	    
	    var x = -1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div class="col-sm-6 one_item_list"><div class="item_list_info"><div class="input_box"><input class="input_field item_name" type="text" name="item_name[]" placeholder="ITEM NAME"></div><div class="input_box"><input class="input_field item_price" type="text" name="item_price[]" placeholder="ITEM PRICE"></div><div class="input_box"><textarea class="input_field item_des" type="text" name="item_description[]" placeholder="ITEM DESCRIPTION"></textarea></div></div><div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_image&item_num='+x+'"></iframe></div></div>'); //add input box
	        }
	    });
	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })
	});

</script>
<div class = "container food">


	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>
	<div class="col-sm-12 field">
	<form method="POST" action="../upload_data.php?cata=<?php echo $_GET[cata] ?>&sub_cata=<?php echo $_GET[sub_cata] ?>">
		
			<div class="col-sm-12 info_title_img">
				<h2>Main Information and Title Image</h2>
				<div class="col-sm-4 info">
					<div class="input_box"><input class="input_field" type="text" name="name" placeholder="Name"></div>
					<div class="input_box"><textarea class="input_field" type="text" name="description" placeholder="Description"></textarea></div>
					<div class="input_box"><input class="input_field" type="text" name="main_location" placeholder="Main Location"></div>
					<div class="input_box"><input class="input_field" type="text" name="location" placeholder="Full Address"></div>
					<div class="input_box"><input class="input_field" type="text" name="web_site" placeholder="Web Site"></div>
					<div class="input_box"><textarea class="input_field" type="text" name="map" placeholder="Map"></textarea></div>
				</div>

				<div class="col-sm-8 title_img ">
					<button class="title_img_but">UP TITLE IMAGE</button>
				</div>
			</div>

			<div class="col-sm-12 main_image">
				<h2>Main Image (Slide Show)</h2>
				<button class="main_img_but">UP MAIN IMAGE</button>

			</div>

		
		<div class="col-sm-12 item_list">
			
		</div>

		<button class="add_field_button">Add Item</button>
		<input type="submit" value="SUBMIT">
	</form>
	</div>

</div>
<!-- END OF container -->
</body>
</html>