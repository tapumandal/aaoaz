<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>
<!DOCTYPE html>
<head>
 <!-- Image Upload -->
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

<title>FOOD PAGE</title>
</head>
<script>


		// For Image Update
	$(document).ready(function(){
		

			 var imageShape="square";
			    $('select').on('change', function() {
				  imageShape=this.value ;
				});
				
			var title_img_but = $(".item_img_but");
		    var y = 0; //initlal text box count
		    $(title_img_but).click(function(e){ //on add input button click

		    	var classes = $(this).attr('class').split(' ');
		    	var target_class = classes[1];

		        e.preventDefault();
		        if(y < 1){ //max input box allowed
		            y++; //text box increment
		            $('.parent'+target_class).append('<div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num=0"></iframe></div>'); //add input box
		        }
		    });

			var add_button22      = $(".add_more_item_image"); //Add button ID
		    
		    var d = -1; //initlal text box count
		    $(add_button22).click(function(e){ //on add input button click
		    	var child_classes = $(this).attr('class').split(' ');
		    	var child_target_class = child_classes[1];
		        e.preventDefault();
		        	var j=0;
		            d++; //text box increment
		           	$('.child'+child_target_class).append('<div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_multi_image&imageShape='+imageShape+'&item_num='+y+'&item_multi_num='+d+'"></iframe></div>'); //add input box	
		        
		    });


			var slide_height = $('#slide_height').height();        //Get the height of the browser window
		    var info_height = $('#info_height').height();
		    if (slide_height>info_height) {
		    	slide_height =slide_height;
		    	$('#info_height').height(slide_height);  //Resize the pageContent div, with a size of 60 - page height.
		    	$("#info_height").css("cssText", "height: "+slide_height+"px !important;");
		    }

		    var height = $('#info_height').height();        //Get the height of the browser window
		    height = height-445;
		    if (height>400) {
		    	$('#food_list_height').height(height);  //Resize the pageContent div, with a size of 60 - page height.
		    }
		    else{
		    	$('#food_list_height').height(400);  //Resize the pageContent div, with a size of 60 - page height.
		    }

		    $("#comment_option").val($("#comment_option option:first").val()); //auto select the first option in select tag in comment section
	});

</script>


<body>



<div class = "container food">
		<!-- OVERLAY -->
		<!-- FOR BLUR -->
		<!-- CLICK DISPAPPEAR -->
		<!-- <a href="" onclick='show_food_overlay(this)'>  <div class="overlay-bg-new" ID="food_overlay_bg" style="display:none"></div></a>
		<div class="overlay-content popup54441" >

			<a href="" onclick='show_food_overlay(this)'><button class="close-btn">X</button></a>
		    Everything Here
		</div> -->
		<!-- CLICK DISPAPPEAR -->

		<!-- OVERLAY -->	


		<!-- OVERLAY -->
		<!-- FOR BLUR -->
		
		<div class="overlay-bg"> </div> <!-- FOR BLUR -->
		<div class="overlay-content popup54441" >
			<!-- <button class="close-btn">X</button> -->
			<div ID="overlay_content">
				
			</div>
			
		</div>

		<!-- OVERLAY -->	



	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>

	<div class="col-sm-12 field food">
		<div class="wide_pattern " id="search_result">  <!-- Whole information without add -->


			<form method='get' action="new_<?php echo $_GET[cata]; ?>.php">
				<input type='hidden' name='cata' value="<?php echo $_GET[cata]; ?>">
				<input type='hidden' name='sub_cata' value="<?php echo $_GET[sub_cata]; ?>">
				<input type='hidden' name="content_id" value="<?php echo $_GET[id]; ?>">
				<input type='hidden' name='update_option' value='update'>
				<input type='submit' value='Advance UPDATE'>
			</form>
		



			<div class="clearfix"></div>

			

			<div class="col-sm-6 first food_left_side" ID="info_height">  <!-- NORMAL INFORMATION  -->
				<?php
				
					$general_obj = new general();
					
					$general_obj->general_info($_GET[cata], $_GET[sub_cata], $_GET[id]);
					// $general_obj->general_map_location($_GET[cata], $_GET[sub_cata], $_GET[id]);					
					// $general_obj->general_map($_GET[cata], $_GET[sub_cata], $_GET[id]);					
					
				?>

			</div>   <!-- NORMAL INFORMATION  -->
			</div>

			<div class="col-sm-6 first food_left_side" ID="info_height">  <!-- NORMAL INFORMATION  -->
				<?php
					$general_obj->seoData($_GET[cata], $_GET[sub_cata], $_GET[id]);


					$general_obj = new general();
					$general_obj->general_map_location($_GET[cata], $_GET[sub_cata], $_GET[id]);					
					$general_obj->general_map($_GET[cata], $_GET[sub_cata], $_GET[id]);					
					echo "<div style='float:left; '>";
						include 'slide-show.php';
					echo "</div>";
					//PHOTO GALLARY
				?>
			</div>   <!-- NORMAL INFORMATION  -->



			<div class="col-sm-12 second"  id="slide_height">   <!-- PHOTO GALLARY -->
   					<?php
   						// include 'slide-show.php';
   					?>
					<!-- PHOTO GALLARY -->

	   		
							        <?php
							        	$general_obj = new general();
							        	$table_pattern_top_1 = '
							        	<!-- ITEM LIST -->
							   				<div class="menu food_item_table">
								   				<!-- <h4>Item List</h4> -->

								   				<ul class="nav nav-tabs">
												    
												 <!-- <li><a data-toggle="tab" href="#" class="starter_but">STARTER</a></li>
												    <li><a data-toggle="tab" href="#" class="combo_but" >COMBO</a></li> -->
												   ';
										$table_pattern_top_2 = $general_obj->filter_list($_GET[cata], $_GET[sub_cata], $_GET[id]);
										
										$table_pattern_top_3 = '</ul>
								   							
								   						<div class="food_item_list" ID="food_list_height" style="height:300px">
														  <div class="table-responsive">          
														    <table class="table">
														      <thead>
														        <tr class="food_list_table_hader">
														          <th width="2%"></th>
														          <th></th>
														          <th></th>
														          <th></th>
														          <th></th>
														          <th></th>
														        </tr>
														      </thead>
														      <tbody>
														        	';
										$table_pattern_top = $table_pattern_top_1.$table_pattern_top_2.$table_pattern_top_3;
										$table_pattern_bottom = '</tbody>
														    </table>
														    </div>
												</div>
								   			</div>
								   		</div>  <!-- ITEM LIST -->
																	';

										
										$general_obj->general_item_list($_GET[cata], $_GET[sub_cata], $_GET[id], $table_pattern_top, $table_pattern_bottom);
									?>
							         
							      
					</div>
				<!-- MAP TAB -->
	   		<!-- <div class="col-sm-6" >   
						<div class="map-tab">
			   			  <ul class="nav nav-tabs tab-pos">
						  	 	<li ><a data-toggle="tab" href="#show">Show Map</a></li>
						  	 	<li class="active"><a data-toggle="tab" href="#blank">Hide Map</a></li>
						  </ul>

					   <div class="tab-content">
					   		<div id="blank" class="tab-pane fade">
						    </div>

						    <div id="show" class="tab-pane fade">
						     	<div class="map">
					   				<iframe width="100%" height="250" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1433590604982"  frameborder="0" style="border:0">
					   				</iframe>
						   		</div>
						    </div>
						    
						</div>
				    </div>
	   		</div>	   		    -->
	   		<!-- MAP TAB -->

				<div class="clearfix"></div>

				

		</div> <!-- Whole information without add -->
		

		<!-- add -->
		<div class="col-sm-2 third">  
		</div>
		<!-- add -->

	</div>
	
<div class="footer">
	<h1>FOOTER</h1>
</div>
</div>
<!-- END OF container -->

</body>
</html>