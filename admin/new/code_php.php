<?php
	error_reporting(0);
	session_start();
	if(!isset($_SESSION[adminId])){
	    header("Location: ../admin_login.php");
	}

	if($_GET[getFilter] == "get_food_filter"){
		
		include "data_connection.php";

		$getFilter = "SELECT filter_1 FROM item_list WHERE cata = 'food' GROUP BY filter_1 ORDER BY filter_1;";

		$result = $conn->query($getFilter);

		$returnFilter="";
		if($result->num_rows>0){
			while ($row=$result->fetch_assoc()) {
				$returnFilter = $returnFilter.'<option value="'.$row[filter_1].'">'.$row[filter_1].'</option>'.' ';
			}
		}
		echo $returnFilter;
	}
	else if($_GET[getFilter] == "get_life_style_filter1"){
		
		include "data_connection.php";

		$getFilter = "SELECT filter_1 FROM item_list WHERE cata = 'life style' AND sub_cata = '$_GET[sub_cata_4_filter]' GROUP BY filter_1 ORDER BY filter_1;";

		$result = $conn->query($getFilter);

		$returnFilter="";
		if($result->num_rows>0){
			$i=0;
			while ($row=$result->fetch_assoc()) {
				if($row[filter_1] != ""){
					$returnFilter = $returnFilter.'<option value="'.$row[filter_1].'">'.$row[filter_1].'</option>'.' ';
					$life_style_1s_filter[$i] = $row[filter_1];
					$i++;
				}
			}
		}
		$_SESSION[lifeStyle1stFilter] = $life_style_1s_filter;
		echo $returnFilter;
	}
	else if($_GET[getFilter] == "get_life_style_sub_filter"){
		
		include "data_connection.php";
		$lifeStyle1stFilter = $_SESSION[lifeStyle1stFilter];
		
		$len = count($lifeStyle1stFilter);

		for($j=0; $j<$len; $j++){
			$k=$j;
			$returnFilter = $returnFilter."";

				$getFilter = "SELECT filter_2 FROM item_list WHERE filter_1 = '$lifeStyle1stFilter[$k]' AND cata = 'life style' AND sub_cata = '$_GET[sub_cata_4_filter]' GROUP BY filter_2 ORDER BY filter_2;";

				$result = $conn->query($getFilter);

				
				if($result->num_rows>0){

					$returnFilter = $returnFilter.'<div id="'.$lifeStyle1stFilter[$k].'_filter" style="Display:block;">
					 								<select name="lifeStyle'.$lifeStyle1stFilter[$k].'Filter[]">
					 								<option class="'.$lifeStyle1stFilter[$k].'_c_" style="color:red" value="">==>> '.$lifeStyle1stFilter[$k].'</option>';
					while ($row=$result->fetch_assoc()) {
						if($row[filter_2] != ""){
							$returnFilter = $returnFilter.'<option class="male_c_" value="'.$row[filter_2].'">'.$row[filter_2].'</option>';
						}
									
					}
					$returnFilter = $returnFilter.'</select></div>';
				}
			
		}
		echo $returnFilter;
	}

	if($_GET[com_type]=="question" || $_GET[com_type]=="comment" && !empty($_GET[com_value]))
	{

		include "data_connection.php";

		$com_ins = "INSERT INTO `comment`(`cata_id`, `cata`, `sub_cata`, `user_id`, `user_type`, `text`, `type`, `time`, `date`) 
		VALUES ('$_GET[cata_id]', '$_GET[cata]', '$_GET[sub_cata]', '$_SESSION[userid]', 'user', '$_GET[com_value]', '$_GET[com_type]', now(), now())";

		if ($conn->query($com_ins) === TRUE) {

			  $last_id = $conn->insert_id;
			 $general_obj = new general();
		     $general_obj->fetch_comment($_GET[cata], $_GET[sub_cata], $_GET[cata_id], $last_id);
		    
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}


	}


	if($_GET[com_type]=="rate")
	{

		include "data_connection.php";
		

		// to get the item original id using the user given id.
			$fetch_item_original_id = "SELECT id FROM `item_list` WHERE cata_id = '$_GET[cata_id]' AND cata= '$_GET[cata]' AND sub_cata='$_GET[sub_cata]';";
	
			$i=1;
			$data= $conn->query($fetch_item_original_id);
			if($data->num_rows>0){
				while ($row=$data->fetch_assoc()) {
					if($i==$_GET[item_id]){
						$item_id = $row[id];
						$_SESSION[item_ori_id]=$item_id;
					}
					$i=$i+1;
				}
			}

			$tmp_item_id = $_GET[tmp_item_id];
			$item_id=$_SESSION[food_item_detect][$tmp_item_id];


		$rate_avg = $_GET[rate_1]+$_GET[rate_2]+$_GET[rate_3];
		$rate_avg = $rate_avg/3;

		$com_ins = "INSERT INTO `comment`(`cata_id`, `cata`, `sub_cata`, `item_id`, `user_id`,	`user_type`, `text`, `type`, 
			`rate_1`, `rate_2`, `rate_3`, `avg`, `time`, `date`) 
		VALUES ('$_GET[cata_id]', '$_GET[cata]', '$_GET[sub_cata]', '$item_id', '$_SESSION[userid]', 'user', '$_GET[rate_text]', '$_GET[com_type]',
		'$_GET[rate_1]', '$_GET[rate_2]', '$_GET[rate_3]', '$rate_avg',  now(), now())";

		if ($conn->query($com_ins) === TRUE) {
		    echo "Comment insert successfully";
		} else {
		    echo " SQL Error: " . $sql . "<br>" . $conn->error;
		}

		$total_rate = $_GET[rate_1]+$_GET[rate_2]+$_GET[rate_3];
		$item_rate = "UPDATE `item_list` SET `total_rate`=total_rate+'$total_rate', `rate_1`=rate_1+'$_GET[rate_1]', `rate_2`=rate_2+'$_GET[rate_2]',
		`rate_3`= rate_3+'$_GET[rate_3]',`rate_people`=rate_people+1 WHERE id = '$item_id';";

		if ($conn->query($item_rate) === TRUE) {
		    $last_id = $conn->insert_id;
			 $general_obj = new general();
		     $general_obj->fetch_comment($_GET[cata], $_GET[sub_cata], $_GET[cata_id], $last_id);
		    
		} else {
		    echo " SQL Error: " . $sql . "<br>" . $conn->error;
		}

	}

	function getLifeStyleFilter($cata_id){
		include 'data_connection.php';

		$sql = "SELECT `filter_1` FROM `item_list` WHERE `cata_id` = '$cata_id' GROUP BY `filter_1`";

		$res = $conn->query($sql);

		$subFilter="";

		$FilterTop = '<div class="fashion_cata_list"><ul>';
		$menu_list =1;

		if($res->num_rows>0){

			while($row=$res->fetch_assoc()){

				$subFilter = $subFilter.getLifeStyleSubFilter($cata_id, $row[filter_1], $menu_list);
				$menu_list++;

			}
		}

		$FilterBottom="</ul></div>";

		$LSFilter = $FilterTop.$subFilter.$FilterBottom;

		return $LSFilter;
	}
	function getLifeStyleSubFilter($cata_id, $filter1, $menu_list){
		include 'data_connection.php';
		$sql2 = "SELECT `filter_2` FROM `item_list` WHERE `cata_id` = '$cata_id' AND `filter_1` = '$filter1' GROUP BY `filter_2`";

		$res2 = $conn->query($sql2);

		$filter1Class =  str_replace(" ", "_", $filter1);
			// $filter1Class = strtolower($filter1Class);

		$SubFilterTop='<li class="fashion_cata"><a class="lifetStyleFilter '.$filter1Class.'_but" id="menu_list'.$menu_list.'" href="#">'.$filter1.'</a>
		<div id="sub_menu_list'.$menu_list.'">
			<ul class="fashion_sub_cata">';

			// $sub_menu_list=1;
				if($res2->num_rows>0){
					while($row2=$res2->fetch_assoc()){
					// $SubFilter = $SubFilter." ".$row2[filter_2];
						$subFilterClass =  str_replace(" ", "_", $row2[filter_2]);
						$SubFilter = $SubFilter.'<a class="lifetStyleFilter '.$subFilterClass.'_but" href="#"><li>'.$row2[filter_2].'</li></a>';
					}
				}

				$SubFilterBottom = '</ul>
			</div>
		</li>';

		$SubFilter = $SubFilterTop.$SubFilter.$SubFilterBottom;

		return $SubFilter;
	}
	class general{


		function getNearbyInfo($cata_id, $res_hotel){
			include 'data_connection.php';
			if($res_hotel == "hotel"){

				$getNearby = "SELECT tourism_nearby.cata_id, tourism_nearby.hotel_id, general_info.name, general_info.image_link, general_info.type
							FROM `tourism_nearby` LEFT JOIN `general_info` ON tourism_nearby.hotel_id=general_info.id WHERE tourism_nearby.cata_id = '$cata_id' LIMIT 5";
			}
			else if($res_hotel == 'restaurant'){
				$getNearby = "SELECT tourism_nearby.cata_id, tourism_nearby.restaurant_id, general_info.name, general_info.image_link, general_info.type
							FROM `tourism_nearby` LEFT JOIN `general_info` ON tourism_nearby.restaurant_id=general_info.id WHERE tourism_nearby.cata_id = '$cata_id' LIMIT 5";	
			}

			$result = $conn->query($getNearby);

			if($result->num_rows>0){
				while($row = $result->fetch_assoc()){
					if($res_hotel == "restaurant")
					echo '
						<div class="nearby_box">
		   					<div class="nearby_img">
		   						<img class="img-responsive" src="../../image/title_image/'.$row[image_link].'">
		   					</div>
		   					<div class="nearby_data">
		   						<a href=""><span>'.$row[name].'</span></a><br>
		   						<span>'.$row[type].'</span><br>
		   						<span>Full Address</span>
		   					</div>

		   					<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=tourismNearby&column_name=restaurant_id&nearby_id='.$row[restaurant_id].'&cata_id='.$cata_id.'">
							    <textarea  name="text" placeholder="Enter Nearby Restaurant"></textarea>
							    <input type="submit" value="UPDATE">
							</form></div>

		   				</div>	
					';

					if($res_hotel == "hotel")
					echo '
						<div class="nearby_box">
		   					<div class="nearby_img">
		   						<img class="img-responsive" src="../../image/title_image/'.$row[image_link].'">
		   					</div>
		   					<div class="nearby_data">
		   						<a href=""><span>'.$row[name].'</span></a><br>
		   						<span>'.$row[type].'</span><br>
		   						<span>Full Address</span>
		   					</div>

		   					<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=tourismNearby&column_name=hotel_id&nearby_id='.$row[hotel_id].'&cata_id='.$cata_id.'">
							    <textarea  name="text" placeholder="Enter Nearby Hotel"></textarea>
							    <input type="submit" value="UPDATE">
							</form></div>
		   				</div>	
					';
				}

			}
		}
		
		function getUserImage($userid){
			include 'data_connection.php';
			$getId = "SELECT image_link FROM user where id='$userid';";
			$getdata = $conn->query($getId);
			if ($getdata->num_rows>0) {
				while ($row=$getdata->fetch_assoc()) {
					
					// echo '<a href="#0" class="pro_link"  ><img src="../../image/user_image/'.$row[image_link].'"></a>';
				$image_link = "image/user_image/".$row[image_link];
				$imgExist = file_exists($image_link);
				if($imgExist == false) $image_link = "image/user_image/profile.jpg";
				
					echo '<a href="#show"  "><img src="'.$image_link.'"></a>';
					
				}
			}
			
		}

		function general_name($cata, $sub_cata, $id){
			include 'data_connection.php';
			$fetch_name = "SELECT name, status FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

			$data= $conn->query($fetch_name);
			
			if($data->num_rows>0){
				while ($row=$data->fetch_assoc()) {

					if($row[status] == "not approved"){
						$status = '<div class="app_status1"><button onclick="contentStatus(\'approved\', \''.$id.'\')">Not Approved</button></div>';
					}
					else if($row[status] == "approved"){
						$status = '<div class="app_status2"><button onclick="contentStatus(\'not approved\', \''.$id.'\')">Approved</button></div>';
					}
					echo $status.'<br><span>'.$row[name].'</span>';
					echo '
						<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=name&id='.$row[id].'">
						    <input name="text" placeholder="Updated Name">
						    <input type="submit" value="UPDATE">
						 </form></div>
					';
				}
			}
		}

		function general_image($cata, $sub_cata, $id){
			include "data_connection.php";

			$fetch_image_data = "SELECT * FROM `general_image` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

			$data= $conn->query($fetch_image_data);
			$ac=0;
			if($data->num_rows>0){
				while ($row=$data->fetch_assoc()) {
					$tmpImg = $row[image_link];	
					if($tmpImg != ""){
						if($ac==0)
						{
							echo '
								<div class="item active">
							        <img src="../../image/main_image/'.$row[image_link].'" alt="Chania">
							        <div class="carousel-caption">
							          <a href="#" onclick="deleteMainImage(\''.$row[id].'\')">Delete</a>
							          <h3>'.$row[image_des].'</h3>
							        <h3>'.$row[id].'</h3>
							        </div>
							     </div>
								';	
								$ac=$ac+1;
						}
						else {
								echo '
								 <div class="item">
							        <img src="../../image/main_image/'.$row[image_link].'" alt="Chania">
							        <div class="carousel-caption">
							        	<a href="#" onclick="deleteMainImage(\''.$row[id].'\')">Delete</a>
							          <h3>'.$row[image_des].'</h3>
							          <h3>'.$row[id].'</h3>
							        </div>
							     </div>
							';
						}
					}else{
						echo "else";
					}
				}//image while
			}//image if
		}//general_image_function

		function doctor_faculty_list($cata, $sub_cata, $id, $top, $bottom, $menu){
			include "data_connection.php";
			$fetch_list_data = "SELECT * FROM `doctor_faculty` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

			$data= $conn->query($fetch_list_data);
			
			if($data->num_rows>0){
				echo '<div class="col-sm-12 edu_item_name_list_X" >';
				echo $menu;
				echo $top;
				while ($row=$data->fetch_assoc()) {
					echo '  <div class="edu-info-box '.$row[depertment].'">';

						echo '<div class="fac_doc_img img-responsive">
							<img src="../../image/item_image/'.$row[image_link].'">
							<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
			          			<select id="image_s">
										<option value="square">square</option>
										<option value="landscape">landscape</option>
										<option value="portrait">portrait</option>
								</select>
			          			<div class="col-sm-12 ">
									<div class="item_img parent'.$row[id].'"></div>
									<div class="col-sm-12 item_list child'.$row[id].'"></div>	

									<button class="item_img_but '.$row[id].'">UP IMAGE</button>
									<button class="add_more_item_image '.$row[id].'">More</button>

								</div>
								<br>
								<input type="submit" value="UPDATE">
							</form></div>
							</div>
						';


						echo '<h3>'.$row[name].'</h3>
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
								    <textarea  name="text" placeholder="Enter Name"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
							';
						echo '<h4>'.$row[designation].'</h4>
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=designation&item_id='.$row[id].'&cata='.$_GET[cata].'">
								    <textarea  name="text" placeholder="Enter designation"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
							';
						echo '<span>'.$row[schedule].'<br></span>
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=schedule&item_id='.$row[id].'&cata='.$_GET[cata].'">
								    <textarea  name="text" placeholder="Enter schedule"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
							';
						echo '<span>'.$row[education].'<br></span>
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=education&item_id='.$row[id].'&cata='.$_GET[cata].'">
								    <textarea  name="text" placeholder="Enter education"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
							';
						echo '<span> Email: '.$row[email].'<br> </span>
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=email&item_id='.$row[id].'&cata='.$_GET[cata].'">
								    <textarea  name="text" placeholder="Enter email"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
							';
						echo '<span> Phone: '.$row[mobile].'<br> </span>
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=mobile&item_id='.$row[id].'&cata='.$_GET[cata].'">
								    <textarea  name="text" placeholder="Enter mobile"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
							';
					echo  '</div>';
				}
				echo $bottom;
				echo '</div>';
			}
			
		}

		


		function general_item_list($cata, $sub_cata, $id, $table_pattern_top, $table_pattern_bottom, $life_style_menu, $filter_c){
			echo $filter_c;

			include "data_connection.php";
			if($cata == "electronics"){
				$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata' ;";
			}
			else if($cata == "food"){
				$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata'  ORDER BY filter_1;";
			}
			else if($cata == "residence"){
				$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata'  AND filter_1='$filter_c';";
				
			}
			else{
				$fetch_item_data = "SELECT * FROM `item_list` WHERE cata_id = '$id' AND cata= '$cata' ;";
			}
			$data= $conn->query($fetch_item_data);
			$item_number=1;
			if($data->num_rows>0 && $cata=="food"){

				$filter_access = "true";
				while ($row=$data->fetch_assoc()) {
						
					$_SESSION[food_item_detect][$item_number] = $row[id];


					$rate = ($row[total_rate]/$row[rate_people])/3;
					$rate = $rate*20;

					if($item_number == 1){
						echo $table_pattern_top;
					}
					$id = 5;

					if($filter_access == "false" && $filter_access_tmp != $row[filter_1]){
						$filter_access = "true";
					}
					
					$f_class = strtolower($row[filter_1]);
		     		$f_class = str_replace(" ", "_", $f_class);

					if($filter_access == "true"){

						$filter_access_tmp = $row[filter_1]; 
						$filter_access = "false";
						echo '
						<tr>
							<td class="food_item_Type" colspan="6">
							<div class="item_type item_filter '.$f_class.'" ID="'.$f_class.' ">
								<span>'.strtoupper($row[filter_1]).'</span>
							</div>
							</td>
						</tr>
						';

					}


					echo '
						<tr  class="item_filter '.$f_class.'">
				          <td class="item-num"><span>'.$item_number.'</span></td>
				          <td class="table_img"> <img src="../../image/item_image/'.$row[image_link].'"></td>
				          <td class="item-name">'.$row[name].'</td>
				          <td class="item-name">'.$row[filter_1].'</td>
				          <td class="item-price">'.$row[price].'tk</td>
				          <td class="item-rate">
				          		<ul class="star-rating">
								  <li class="current-rating" id="current-rating" style="width:'.$rate.'%"><!-- will show current rating --></li>
								  <span id="ratelinks">
								  <li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
								  <li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
								  <li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
								  <li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
								  <li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
								  </span>
								</ul>
				           </td>
				          <td class="item-see">
				          	<a class="delete_item" href="#" onclick="delete_item(\''.$row[id].'\', \''.$cata.'\')">Delete</a>
				          </td>

				        </tr>
				        

				        <tr class="item_filter '.$f_class.'">

				          <td class="item-num"></td>
				          <td class="table_img">
				          		<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'&item_name='.$row[name].'">
				          			<select id="image_s">
											<option value="square">square</option>
											<option value="landscape">landscape</option>
											<option value="portrait">portrait</option>
									</select>
					          		<div class="col-sm-12 ">
										<div class="item_img parent'.$row[id].'"></div>
										<div class="col-sm-12 item_list child'.$row[id].'"></div>	
										
										<button class="item_img_but '.$row[id].'">UP IMAGE</button>
										<button class="add_more_item_image '.$row[id].'">More</button>

									</div>
									<br>
									
									<input type="submit" value="UPDATE">
								</form></div>
				          </td>
				          <td class="item-name">
				          	<div class="admin_input_box_item"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
							    <textarea  name="text" placeholder="Enter updated Name"></textarea>
							    <input type="submit" value="UPDATE">
							</form></div>
				          </td>
				          <td class="item-name">
				          	<div class="admin_input_box_item"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=filter_1&item_id='.$row[id].'&cata='.$_GET[cata].'">
							    <textarea  name="text" placeholder="Enter updated Filter 1"></textarea>
							    <input type="submit" value="UPDATE">
							</form></div>
				          </td>
				          <td class="item-price">
				          	<div class="admin_input_box_item"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=price&item_id='.$row[id].'&cata='.$_GET[cata].'">
							    <textarea  name="text" placeholder="Enter updated Price"></textarea>
							    <input type="submit" value="UPDATE">
							</form></div>
				          </td>
				          <td class="item-rate">
				          	<div class="admin_input_box_item"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=description&item_id='.$row[id].'&cata='.$_GET[cata].'">
							    <textarea  name="text" placeholder="Enter updated Description"></textarea>
							    <input type="submit" value="UPDATE">
							</form></div>
				          </td>









				        </tr>
					';
					$item_number=$item_number+1;
				}//item_list while
				echo $table_pattern_bottom;
			}//item_list if
			else if($data->num_rows>0 && $cata=="life style")
			{
				echo '<div class="col-sm-12 life-list" ><!-- Item List LIST -->';
				echo $table_pattern_top;
				echo $life_style_menu;
				echo "<div class='all_items'>";
				while ($row=$data->fetch_assoc()) {
					$filter1=str_replace(" ", "_", $row[filter_2]);
					$filter1=strtolower($filter1);
					  echo '<div class="info-box  item_filter '.$row[filter_1].' '.$filter1.' '.$row[filter_3].'" >
						          <div class="info_box_img">
						          	<img  src="../../image/item_image/'.$row[image_link].'">
							          	<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
						          			<select id="image_s">
													<option value="square">square</option>
													<option value="landscape">landscape</option>
													<option value="portrait">portrait</option>
											</select>
						          			<div class="col-sm-12 ">
												<div class="item_img parent'.$row[id].'"></div>
												<div class="col-sm-12 item_list child'.$row[id].'"></div>	
												
												<button class="item_img_but '.$row[id].'">UP IMAGE</button>
												<button class="add_more_item_image '.$row[id].'">More</button>

											</div>
											<br>
											<input type="submit" value="UPDATE">
										</form></div>
						          </div>
						          <div class="product_info">
						          	<div class="table-responsive">          
									  <table class="table">
									   
									    <tbody>
									      <tr>
									        <td>'.$row[name].'
									        		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
													    <textarea  name="text" placeholder="Enter updated"></textarea>
													    <input type="submit" value="UPDATE">
													</form></div>
									        </td>
									      </tr>
									      <tr>
									        <td>Price:'.$row[price].'
									        		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
													    <textarea  name="text" placeholder="Enter updated"></textarea>
													    <input type="submit" value="UPDATE">
													</form></div>
									        </td>
									      </tr>

									      <tr class="item-see">
				            	          	<a class="show-popup" href="item_details.php?cata='.$cata.'&item_id='.$row[id].'" data-showpopup="54441" onclick="show_overlay(\''.$cata.'\','.$row[id].')">See Details</a>
								          </tr>
									      
									    </tbody>
									  </table>
									  </div>
						          </div>
					        </div>
					       ';
				}
				echo "</div>";
				echo $table_pattern_bottom;
				echo '	</div><!-- Item LIST -->';
			}
			else if($data->num_rows>0 && $cata=="electronics"){
				while ($row=$data->fetch_assoc()) {
					echo '<div class="col-sm-2 product_area" >
	                        <a href="#">
	                          <div class="product-box">
	                            <div class="product-box-hover">
	                             
	                                <div class="remember">
	                                  <a href="http://www.youtube.com">REMEMBER</a>
	                                </div>
	                               <div class="compare">
	                                    <a  href="www.facebook.com">COMPARE</a>
	                                </div>
	                                <div class="details">
	                                  <a class="show-popup" href="item_details.php?cata='.$cata.'&item_id='.$row[id].'" data-showpopup="54441" onclick="show_overlay(\''.$cata.'\','.$row[id].')">Details</a>
	                                </div>
	                            </div>

	                            <!-- <div class="image-space"></div>  -->
	                            <img src="../../image/item_image/'.$row[image_link].'" class="img-responsive img-circle"  alt="IMAGE"></img>
	                            <div class="table-responsive">          
	                            <table class="pro_table">
	                             
	                              <tbody>
	                                <tr>
	                                  <td>'.$row[name].'</td>
	                                </tr>
	                                <tr>
	                                  <td>'.$row[description].'</td>
	                                </tr>
	                                <tr>
	                                  <td>Price : '.$row[price].'</td>
	                                </tr>
	                                <tr>
	                                  <td>Review : </td>
	                                </tr>
	                              </tbody>
	                            </table>
	                            </div>
	                          </div>
	                        </a>

	                        			<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
						          			<select id="image_s">
													<option value="square">square</option>
													<option value="landscape">landscape</option>
													<option value="portrait">portrait</option>
											</select>
						          			<div class="col-sm-12 ">
												<div class="item_img parent'.$row[id].'"></div>
												<div class="col-sm-12 item_list child'.$row[id].'"></div>	
												
												<button class="item_img_but '.$row[id].'">UP IMAGE</button>
												<button class="add_more_item_image '.$row[id].'">More</button>

											</div>
											<br>
											<input type="submit" value="UPDATE">
										</form></div>


	                      </div>
	                ';
	            }
			}
			else if($data->num_rows>0 && $cata=="residence" && $filter_c == "room")
			{
				while ($row=$data->fetch_assoc()) {
						echo '<div class="attraction_box col-sm-2" style="margin-right:200px;">
		   						<img class="img-responsive"  src="../../image/item_image/'.$row[image_link].'">
			   						<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
					          			<select id="image_s">
												<option value="square">square</option>
												<option value="landscape">landscape</option>
												<option value="portrait">portrait</option>
										</select>
					          			<div class="col-sm-12 ">
											<div class="item_img parent'.$row[id].'"></div>
											<div class="col-sm-12 item_list child'.$row[id].'"></div>	
											
											<button class="item_img_but '.$row[id].'">UP IMAGE</button>
											<a href="#0" class="more_image '.$row[id].'">Add more image</a>
										</div>
										<br>
										<input type="submit" value="UPDATE">
									</form></div>
			   					<div class="attraction_info">
			   						<span>'.$row[name].'</span>
											<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
											    <textarea  name="text" placeholder="Enter Name"></textarea>
											    <input type="submit" value="UPDATE">
											</form></div>
			   						<p>'.$row[description].'</p>
			   								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=description&item_id='.$row[id].'&cata='.$_GET[cata].'">
											    <textarea  name="text" placeholder="Enter Description"></textarea>
											    <input type="submit" value="UPDATE">
											</form></div>

									<p>'.$row[column_1].'</p>
			   								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=column_1&item_id='.$row[id].'&cata='.$_GET[cata].'">
											    <textarea  name="text" placeholder="Enter Bed type"></textarea>
											    <input type="submit" value="UPDATE">
											</form></div>
									<p>'.$row[column_2].'</p>
			   								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=column_2&item_id='.$row[id].'&cata='.$_GET[cata].'">
											    <textarea  name="text" placeholder="Enter View"></textarea>
											    <input type="submit" value="UPDATE">
											</form></div>
									<p>'.$row[column_3].'</p>
			   								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=column_3&item_id='.$row[id].'&cata='.$_GET[cata].'">
											    <textarea  name="text" placeholder="Enter Facilities"></textarea>
											    <input type="submit" value="UPDATE">
											</form></div>
			   					</div>
			   				</div>';
			   		
			   		
				}
			}
			else if($data->num_rows>0 && $cata=="residence" && $filter_c == "service")
			{
				while ($row=$data->fetch_assoc()) {
					
										
			   			echo '<div class="attraction_box col-sm-2" style="margin-right:200px;">
		   						<img class="img-responsive"  src="../../image/item_image/'.$row[image_link].'">
			   						<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
					          			<select id="image_s">
												<option value="square">square</option>
												<option value="landscape">landscape</option>
												<option value="portrait">portrait</option>
										</select>
					          			<div class="col-sm-12 ">
											<div class="item_img parent'.$row[id].'"></div>
											<div class="col-sm-12 item_list child'.$row[id].'"></div>	
											
											<button class="item_img_but '.$row[id].'">UP IMAGE</button>
											<a href="#0" class="more_image '.$row[id].'">Add more image</a>
										</div>
										<br>
										<input type="submit" value="UPDATE">
									</form></div>
			   					<div class="attraction_info">
			   						<span>'.$row[name].'</span>
											<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
											    <textarea  name="text" placeholder="Enter Name"></textarea>
											    <input type="submit" value="UPDATE">
											</form></div>
			   						<p>'.$row[description].'</p>
			   								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=description&item_id='.$row[id].'&cata='.$_GET[cata].'">
											    <textarea  name="text" placeholder="Enter Description"></textarea>
											    <input type="submit" value="UPDATE">
											</form></div>
			   					</div>
			   				</div>';
			   		
				}
			}
			else if($data->num_rows>0 && $cata=="tourism")
			{
				while ($row=$data->fetch_assoc()) {
					echo '<div class="attraction_box col-sm-2">
	   					<img class="img-responsive"  src="../../image/item_image/'.$row[image_link].'">
	   							<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
				          			<select id="image_s">
											<option value="square">square</option>
											<option value="landscape">landscape</option>
											<option value="portrait">portrait</option>
									</select>
				          			<div class="col-sm-12 ">
										<div class="item_img parent'.$row[id].'"></div>
										<div class="col-sm-12 item_list child'.$row[id].'"></div>	
										
										<button class="item_img_but '.$row[id].'">UP IMAGE</button>
										<a href="#0" class="more_image '.$row[id].'">Add more image</a>
									</div>
									<br>
									<input type="submit" value="UPDATE">
								</form></div>
	   					<div class="attraction_info">
	   						<span><u>'.$row[name].'</u></span>
	   									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
										    <textarea  name="text" placeholder="Enter updated"></textarea>
										    <input type="submit" value="UPDATE">
										</form></div>
	   						<p>'.$row[description].'</p>
	   									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=description&item_id='.$row[id].'&cata='.$_GET[cata].'">
										    <textarea  name="text" placeholder="Enter updated"></textarea>
										    <input type="submit" value="UPDATE">
										</form></div>
	   					</div>
	   				</div>';
				}
			}
			else if($data->num_rows>0 && $cata=="ecommerce")
			{
				echo '<div class="col-sm-12 life-list" ><!-- Item List LIST -->';
				echo $table_pattern_top;
				echo $life_style_menu;
				echo "<div class='all_items'>";
				while ($row=$data->fetch_assoc()) {
					$filter1=str_replace(" ", "_", $row[filter_2]);
					$filter1=strtolower($filter1);
					  echo '<div class="info-box  item_filter '.$row[filter_1].' '.$filter1.' '.$row[filter_3].'" >
						          <div class="info_box_img">
						          	<img  src="../../image/item_image/'.$row[image_link].'">
							          	<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
						          			<select id="image_s">
													<option value="square">square</option>
													<option value="landscape">landscape</option>
													<option value="portrait">portrait</option>
											</select>
						          			<div class="col-sm-12 ">
												<div class="item_img parent'.$row[id].'"></div>
												<div class="col-sm-12 item_list child'.$row[id].'"></div>	
												
												<button class="item_img_but '.$row[id].'">UP IMAGE</button>
												<button class="add_more_item_image '.$row[id].'">More</button>

											</div>
											<br>
											<input type="submit" value="UPDATE">
										</form></div>
						          </div>
						          <div class="product_info">
						          	<div class="table-responsive">          
									  <table class="table">
									   
									    <tbody>
									      <tr>
									        <td>'.$row[name].'
									        		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
													    <textarea  name="text" placeholder="Enter updated"></textarea>
													    <input type="submit" value="UPDATE">
													</form></div>
									        </td>
									      </tr>
									      <tr>
									        <td>Price:'.$row[price].'
									        		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=itemInfo&column_name=name&item_id='.$row[id].'&cata='.$_GET[cata].'">
													    <textarea  name="text" placeholder="Enter updated"></textarea>
													    <input type="submit" value="UPDATE">
													</form></div>
									        </td>
									      </tr>

									      <tr class="item-see">
				            	          	<a class="show-popup" href="item_details.php?cata='.$cata.'&item_id='.$row[id].'" data-showpopup="54441" onclick="show_overlay(\''.$cata.'\','.$row[id].')">See Details</a>
								          </tr>
									      
									    </tbody>
									  </table>
									  </div>
						          </div>
					        </div>
					       ';
				}
				echo "</div>";
				echo $table_pattern_bottom;
				echo '	</div><!-- Item LIST -->';
			}
		}//item_list_function




		function fetch_comment($cata, $sub_cata, $id, $comment_id, $fixed_item_value)
		{
			// echo "LAST ITEM CHECK 55555555555555".$item_id;
			include "data_connection.php";
			if(!empty($fixed_item_value)){
				$fetch_comment_data = "SELECT comment.user_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, user.name,user.id, user.name, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id 
				 WHERE comment.item_id='$fixed_item_value' ORDER BY comment.date DESC, comment.time DESC ;";
			}
			else if(empty($comment_id))
			{
				$fetch_comment_data = "SELECT comment.user_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, user.name,user.id, user.name, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id 
				 WHERE comment.cata_id = '$id' AND comment.cata= '$cata' AND comment.sub_cata='$sub_cata' ORDER BY comment.date DESC, comment.time DESC ;";

			}
			else if(!empty($comment_id)){
				$fetch_comment_data = "SELECT comment.user_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, user.name,user.id, user.name, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id 
				 WHERE comment.id='$comment_id' ORDER BY comment.date DESC, comment.time DESC ;";

				 

			}
			 $image_link = "image/user_image/logo.jpg";
			$data= $conn->query($fetch_comment_data);
			$item_number=1;
			if($data->num_rows>0){
				while ($row=$data->fetch_assoc()) {

					if(!empty($row[image_link]))
					{
						$image_link=$row[image_link];
						$image_link="image/user_image/".$image_link;
					}
					if($row[type]=="comment" || $row[type]=="question"){
						$com_type_img = "image/default_image/comment.png";
						if($row[type]=="question")
						{
							$com_type_img = "image/default_image/question.png";
						}
						else $com_type_img = "image/default_image/comment.png";
						echo '
							<div class="only_com">
		   						<div class="only_com_top">
		   							<div class="only_com_top_img">
										<img class="img-responsive img-circle " src="../../'.$image_link.'"></img>
									</div>
									<div class="only_com_head">
										<span class="name">'.$row[name].'</span>
										<div class="comment_period">
											<span class="comment_date">05 Oct 2015--9:53am</span>
											<!-- <span class="comment_time"></span> -->
											<img class="comment_type" style="height:40px" src="'.$com_type_img.'">
										</div>
									</div>
									<!-- <div class="com-rev">
				   					</div> -->
				   					<!-- <div class="col-sm-10"> -->
					   					<div class=" opinion">
					   						<div class="opinion_img">
						   						<!-- <img class="img-responsive" src="../../image/test.jpg"> -->
						   					</div>
					   						<span class="op">'.$row[text].'</span> 

					   					</div>
					   				<!-- </div> -->
				   				</div>
				   				<!-- only_com_top -->
			   				
			   				<!--
				   				<div class="reply_input">
				   					<div class="reply">
										<img class="img-responsive img-circle" src="../../image/gta.png"></img>
										<div class="admin_input_box"><form>
					   						<input class="reply_input_txt" type="text" name="content" placeholder="Enter your Reply">
					   						<input class="reply_input_but" type="submit" value="REPLY">
				   						</form></div>
									
			   						</div>
				   				</div>
				   			-->						   					
			   				</div>
						';
					}
					else if($row[type]=="rate"){
						
						$rate_1 = $row[rate_1]*20;
						$rate_2 = $row[rate_2]*20;
						$rate_3 = $row[rate_3]*20;
		 
						echo '
							<div class="only_com">

								<img class="img-responsive img-circle" src="../../'.$image_link.'"></img>
								<span class="name">'.$row[name].'</span>
								<div class="comment_period">
									<span class="comment_date">05 Oct 2015--9:53am</span>
									<img class="comment_type" style="height:40px" src="../../image/default_image/review.png">
									<!-- <span class="comment_time"></span> -->
								</div>
								<div class="comment_pro_name">
									<span>Product Name</span>
			   					</div>
			   					<div class="comment_user_review">
											<div class="comment_review_quality">
												<div class="comment_review_title">
													<span>QUALITY</span>
												</div>
												<div class="comment_review_rate">
													<ul class="star-rating">
													  <li class="current-rating" id="current-rating" style="width:'.$rate_1.'%"><!-- will show current rating --></li>
													  <span id="ratelinks">
													  <li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
													  <li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
													  <li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
													  <li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
													  <li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
													  </span>
													</ul>


												</div>
											</div>

											<div class="comment_review_quality">
												<div class="comment_review_title">
													<span>PRICE</span>
												</div>
												<div class="comment_review_rate">
													
													<ul class="star-rating">
													  <li class="current-rating" id="current-rating" style="width:'.$rate_2.'%"><!-- will show current rating --></li>
													  <span id="ratelinks">
													  <li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
													  <li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
													  <li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
													  <li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
													  <li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
													  </span>
													</ul>

												</div>
											</div>

											<!-- style="margin-left:110px" -->
											<div class="comment_review_quality" >
												<div class="comment_review_title">
													<span>SERVICE</span>
												</div>
												<div class="comment_review_rate">
													
													<ul class="star-rating">
													  <li class="current-rating" id="current-rating" style="width:'.$rate_3.'%"><!-- will show current rating --></li>
													  <span id="ratelinks">
													  <li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
													  <li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
													  <li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
													  <li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
													  <li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
													  </span>
													</ul>

												</div>
											</div>
								</div>
								<div class="opinion">
			   						<span class="op"> '.$row[text].'</span> 
			   					</div>
			   					
			   				</div>
						';

					} // else if of rate
				}
			}
		}


		function seoData($cata, $sub_cata, $id){
			include "data_connection.php";
			//$np means narrow pattern
			$nptop = '<div class="info-area">
				   				<div class="info-area-title">
				   					<span>';
			$npmiddle  = '</span>
		   				</div>
		   				<div class="info-ans">
		   					<span>';
		   	$npbottom = '</span>
				   				</div>
				   			</div>';

			
			$wptop = '<div class="info_box2">
						<div class="title2">
							<span>
			';
			$wpmiddle = 	'</span>
						</div>
						<div class="title_info2">
						<span>
			';
			$wpbottom = '
						</span> 
						</div> 
				</div>
				
			';


			$fetch_g_data = "SELECT * FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

			$data= $conn->query($fetch_g_data);

			if($data->num_rows>0){
				while ($row=$data->fetch_assoc()) {

						
					echo $nptop."Meta Title".$npmiddle."".$row[meta_title]."".$npbottom;
					echo '
						<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=meta_title&id='.$row[id].'">
						    <textarea  name="text" placeholder="Meta Title"></textarea>
						    <input type="submit" value="UPDATE">
						  </form></div>
					';

					echo $nptop."Meta Description".$npmiddle."".$row[meta_description]."".$npbottom;
					echo '
						<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=meta_description&id='.$row[id].'">
						    <textarea  name="text" placeholder="Meta description"></textarea>
						    <input type="submit" value="UPDATE">
						  </form></div>
					';

					echo $nptop."Meta Keyword".$npmiddle."".$row[meta_keyword]."".$npbottom;
					echo '
						<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=meta_keyword&id='.$row[id].'">
						    <textarea  name="text" placeholder="Meta keyword"></textarea>
						    <input type="submit" value="UPDATE">
						  </form></div>
					';
					
				}
			}
		}




		function general_info($cata, $sub_cata, $id){
			include "data_connection.php";
			//$np means narrow pattern
			$nptop = '<div class="admin_single_input_box"><div class="info-area">
				   				<div class="info-area-title">
				   					<span>';
			$npmiddle  = '</span>
		   				</div>
		   				<div class="info-ans">
		   					<span>';
		   	$npbottom = '</span>
				   				</div>
				   			</div>';

			
			$wptop = '<div class="admin_single_input_box"><div class="info_box2">
						<div class="title2">
							<span>
			';
			$wpmiddle = 	'</span>
						</div>
						<div class="title_info2">
						<span>
			';
			$wpbottom = '
						</span> 
						</div> 
				</div>
				
			';


			$fetch_g_data = "SELECT * FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

			$data= $conn->query($fetch_g_data);

			if($data->num_rows>0){
				while ($row=$data->fetch_assoc()) {

					

					if($row[status] == "not approved"){
						$status = '<div class="app_status1"><button onclick="contentStatus(\'approved\', \''.$id.'\')">Not Approved</button></div>';
					}
					else if($row[status] == "approved"){
						$status = '<div class="app_status2"><button onclick="contentStatus(\'not approved\', \''.$id.'\')">Approved</button></div>';
					}

				if($cata == "food" )
					{		


							echo '<div class="narrow_pattern">
									<div class="shop_title"><span>'.$row[name].'</span>'.$status.'</div>
								  </div>
							     ';
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=name&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div>
							';

					
							echo $nptop."Type".$npmiddle."".$row[type]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=type&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';

					
							echo $nptop."Description".$npmiddle."".$row[description]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';


					
							echo $nptop."Speciality".$npmiddle."".$row[column_2]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_2&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';


							echo $nptop."Facilities".$npmiddle."".$row[column_5]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_5&id='.$row[id].'">
								    
								    				<div class="input_box facilities_input">
														
														<div class="facilities_set">
															<span>AC : &nbsp;&nbsp;</span>
															<fieldset id="facilities_ac">
														        <input type="radio" value="YES" name="facilities_ac"><span> &nbsp;Yes&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_ac"><span> &nbsp;No&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_ac"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>

														<div class="facilities_set">
														    <span>WiFi :&nbsp;&nbsp; </span>
															<fieldset id="facilities_wifi">
														        <input type="radio" value="YES" name="facilities_wifi"><span> &nbsp;Yes&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_wifi"><span> &nbsp;No&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_wifi"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>

														<div class="facilities_set">
														    <span>Card :&nbsp;&nbsp; </span>
															<fieldset id="facilities_card">
														        <input type="radio" value="YES" name="facilities_card"><span> &nbsp;ACCEPT&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_card"><span> &nbsp;NOT ACCEPT&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_card"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>

														<div class="facilities_set">
														    <span>Reservation :&nbsp;&nbsp; </span>
															<fieldset id="facilities_reservation">
														        <input type="radio" value="YES" name="facilities_reservation"><span> &nbsp;YES&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_reservation"><span> &nbsp;NO&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_reservation"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>

														<div class="facilities_set">
														    <span>PARKING LOT:&nbsp;&nbsp; </span>
															<fieldset id="facilities_parking">
														        <input type="radio" value="YES" name="facilities_parking"><span> &nbsp;YES&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_parking"><span> &nbsp;NO&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_parking"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>

														<div class="facilities_set">
														    <span>SMOKING ZONE:&nbsp;&nbsp; </span>
															<fieldset id="facilities_smoking_zone">
														        <input type="radio" value="YES" name="facilities_smoking_zone"><span> &nbsp;YES&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_smoking_zone"><span> &nbsp;NO&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_smoking_zone"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>


														<div class="facilities_set">
														    <span>KIDS ZONE :&nbsp;&nbsp; </span>
															<fieldset id="facilities_kids_zone">
														        <input type="radio" value="YES" name="facilities_kids_zone"><span> &nbsp;YES&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_kids_zone"><span> &nbsp;NO&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_kids_zone"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>


														<div class="facilities_set">
														    <span>Self Service :&nbsp;&nbsp; </span>
															<fieldset id="facilities_service">
														        <input type="radio" value="YES" name="facilities_service"><span> &nbsp;YES&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="NO" name="facilities_service"><span> &nbsp;NO&nbsp;&nbsp;&nbsp;</span>
														        <input type="radio" value="" name="facilities_service"><span> &nbsp;Don\'t know&nbsp;&nbsp;&nbsp;</span>
														    </fieldset>
														</div>

														<div class="facilities_set">
													    	<input type="text" name="facilities_capacity" placeholder="Capacity">
													    </div>

													    <div class="facilities_set">
													    <br>
														    <span>Other</span>
														    <textarea type="text" name="facilities_other" placeholder="Other Facilities use , to seperate"></textarea>
														</div>

													</div>
										<input type="submit" value="UPDATE">
								  </form></div></div>
							';	

					
							echo $nptop."Offer".$npmiddle."".$row[offer]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=offer&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';

					
							echo $nptop."Opening Hour".$npmiddle."".$row[column_4]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_4&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';				

					
							echo $nptop."Web Site".$npmiddle."".$row[web_site]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';

						
							echo $nptop."Location".$npmiddle."".$row[location]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=location&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';
	
						
							echo $nptop."Social Links".$npmiddle."".$row[social_links]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';

							echo "<div class='text_height_37'>".$nptop."Contact".$npmiddle."".$row[contact].$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
						/*
						if(!empty($row[map]))
						{
							echo '<div class="info-area">
					   				'.$row[map].'
					   			</div>
							     ';
						}
						*/
					}
				else if($cata == "life style" ){
						
							echo $nptop."Type".$npmiddle."".$row[type]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=type&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';		

							echo $nptop."Description".$npmiddle."".$row[description]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	


						
							echo $nptop."Speciality".$npmiddle."".$row[column_2]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_2&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	

							echo $nptop."Offer".$npmiddle."".$row[offer]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=offer&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';

							echo $nptop."Programm Schedual".$npmiddle."".$row[column_4]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_4&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	

						
							echo $nptop."Sponsor".$npmiddle."".$row[column_5]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_5&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	

						
							echo $nptop."Opening Hour".$npmiddle."".$row[column_6]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_6&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';							

							echo $nptop."Web Site".$npmiddle."".$row[web_site]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	
						
							echo $nptop."Social Links".$npmiddle."".$row[social_links]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	

							echo "<div class='text_height_37'>".$nptop."Contact".$npmiddle."".$row[contact].$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
					}


					else if($cata == "education")
					{
						
						
							echo "<div class='text_height_1'>".$wptop."Description".$wpmiddle."".$row[description].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';


						
							echo "<div class='text_height_2'>".$wptop."Department List".$wpmiddle."".$row[column_1].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_1&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';


						
							echo "<div class='text_height_3'>".$wptop."Information".$wpmiddle."".$row[column_2].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_2&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';


						
							echo "<div class='text_height_4'>".$wptop."Highlight".$wpmiddle."".$row[column_3].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_3&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';


					
							echo "<div class='text_height_5'>".$wptop."Admission".$wpmiddle."".$row[column_4].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_4&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_6'>".$wptop."Admission Requirment".$wpmiddle."".$row[column_5].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_5&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';


					
							echo "<div class='text_height_7'>".$wptop."Notice".$wpmiddle."".$row[column_6].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_6&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_8'>".$wptop."Course Details".$wpmiddle."".$row[column_8].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_8&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_9'>".$wptop."CGPA System".$wpmiddle."".$row[column_10].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_10&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_10'>".$wptop."Class Plan".$wpmiddle."".$row[column_11].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_11&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_11'>".$wptop."Exam Schedual".$wpmiddle."".$row[column_13].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_13&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_12'>".$wptop."Exam Hall".$wpmiddle."".$row[column_15].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_15&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_13'>".$wptop."Expanse".$wpmiddle."".$row[column_16].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_16&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_14'>".$wptop."Web Site".$wpmiddle."".$row[web_site].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_15'>".$wptop."Social Links".$wpmiddle."".$row[social_links].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_37'>".$wptop."Contact".$wpmiddle."".$row[contact].$wpbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							// echo "<div class='text_height_16'>".$wptop."Address".$wpmiddle."".$row[location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
							// 	echo '
							// 		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=location&id='.$row[id].'">
							// 		    <textarea  name="text" placeholder="Enter updated data"></textarea>
							// 		    <input type="submit" value="UPDATE">
							// 		</form></div>
							// ';

							// echo "<div class='text_height_17'>".$wptop."Google Map".$wpmiddle."".$row[map].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
							// 	echo '
							// 		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=map&id='.$row[id].'">
							// 		    <textarea  name="text" placeholder="Enter updated data"></textarea>
							// 		    <input type="submit" value="UPDATE">
							// 		</form></div>
							// ';

					}
					else if ($cata == "health") {
						
						
							echo "<div class='text_height_18'>".$wptop."Description".$wpmiddle."".$row[description].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_19'>".$wptop."Department List".$wpmiddle."".$row[column_1].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_1&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_20'>".$wptop."Facilities".$wpmiddle."".$row[column_2].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_2&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_21'>".$wptop."Ambulance".$wpmiddle."".$row[column_3].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_3&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_22'>".$wptop."Opening Hour".$wpmiddle."".$row[column_4].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_4&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_23'>".$wptop."Web Site".$wpmiddle."".$row[web_site].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_24'>".$wptop."Social Links".$wpmiddle."".$row[social_links].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							echo "<div class='text_height_25'>".$wptop."Address".$wpmiddle."".$row[location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=location&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							// echo "<div class='text_height_26'>".$wptop."Google Map".$wpmiddle."".$row[map].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
							// 	echo '
							// 		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=map&id='.$row[id].'">
							// 		    <textarea  name="text" placeholder="Enter updated data"></textarea>
							// 		    <input type="submit" value="UPDATE">
							// 		</form></div>
							// 	';
							echo "<div class='text_height_37'>".$wptop."Contact".$wpmiddle."".$row[contact].$wpbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

					}
					else if($cata == "residence"){
						
						
							echo '<div class="narrow_pattern">
									<div class="shop_title"><span>'.$row[name].'</span>'.$status.'</div>
								  </div>
							     ';
							 echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=name&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';
						

						

						
							echo $nptop."Type".$npmiddle."".$row[type]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=type&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo $nptop."Location".$npmiddle."".$row[main_location]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=main_location&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
						
							
							echo $nptop."Offer".$npmiddle."".$row[offer]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=offer&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';


							echo $nptop."Size".$npmiddle."".$row[column_2]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_2&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo $nptop."Rent".$npmiddle."".$row[column_3]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_3&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo $nptop."Description".$npmiddle."".$row[description]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							
							echo $nptop."Web Site".$npmiddle."".$row[web_site]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo $nptop."Social Links".$npmiddle."".$row[social_links]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_37'>".$nptop."Contact".$npmiddle."".$row[contact]."".$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							// echo $nptop."Address".$npmiddle."".$row[location]."".$npbottom;
							// 	echo '
							// 		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=location&id='.$row[id].'">
							// 		    <textarea  name="text" placeholder="Enter updated data"></textarea>
							// 		    <input type="submit" value="UPDATE">
							// 		</form></div>
							// 	';
						
					
							// echo '<div ID="branch_map">';
							// echo '<div class="info-area">
							// 		<span>'.$row[main_location].' </span>
					  //  				'.$row[map].'
					  //  			</div>
							//      ';
							// echo '</div>';
						
						
					}
					else if($cata == "tourism"){
					
						
							// echo "<div class='text_height_27'>".$wptop."Description".$wpmiddle."".$row[description]."".$wpbottom." <div class='see_more'><a href='#0' onclick=\"seeMore('text_height_28')\">See More</a></div>".$wpbottom."</div>";
							echo "<div class='text_height_27'>".$wptop."Description".$wpmiddle."".$row[description].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';	
							echo "<div class='text_height_28'>".$wptop."Area".$wpmiddle."".$row[column_1].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_1&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_29'>".$wptop."Opening".$wpmiddle."".$row[column_2].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_2&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_30'>".$wptop."Close Day".$wpmiddle."".$row[column_3].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_3&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_31'>".$wptop."Entry Fee".$wpmiddle."".$row[column_4].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_4&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_32'>".$wptop."Pakage".$wpmiddle."".$row[column_5].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_5&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_33'>".$wptop."Offer".$wpmiddle."".$row[offer].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=offer&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_34'>".$wptop."Travel Alert".$wpmiddle."".$row[column_7].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_7&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_34'>".$wptop."How to go".$wpmiddle."".$row[column_8].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_8&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_35'>".$wptop."Distance Chart".$wpmiddle."".$row[column_9].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_9&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_36'>".$wptop."History".$wpmiddle."".$row[column_10].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_10&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							
							echo "<div class='text_height_38'>".$wptop."Web Site".$wpmiddle."".$row[web_site].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_39'>".$wptop."Social Links".$wpmiddle."".$row[social_links].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
							echo "<div class='text_height_37'>".$wptop."Contact".$wpmiddle."".$row[contact].$wpbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';

							// echo "<div class='text_height_40'>".$wptop."Main Location".$wpmiddle."".$row[main_location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
							// 	echo '
							// 		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=main_location&id='.$row[id].'">
							// 		    <textarea  name="text" placeholder="Enter updated data"></textarea>
							// 		    <input type="submit" value="UPDATE">
							// 		</form></div>
							// 	';
							// echo "<div class='text_height_41'>".$wptop."Address".$wpmiddle."".$row[location].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
							// 	echo '
							// 		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=location&id='.$row[id].'">
							// 		    <textarea  name="text" placeholder="Enter updated data"></textarea>
							// 		    <input type="submit" value="UPDATE">
							// 		</form></div>
							// 	';

							// echo "<div class='text_height_42'>".$wptop."Google Map".$wpmiddle."".$row[map].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
							// 	echo '
							// 		<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=map&id='.$row[id].'">
							// 		    <textarea  name="text" placeholder="Enter updated data"></textarea>
							// 		    <input type="submit" value="UPDATE">
							// 		</form></div>
							// 	';

					}
					else if($cata == "electronics"){
						
							echo "<div class='text_height_43'>".$wptop."Description".$wpmiddle."".$row[description].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';	
							echo "<div class='text_height_44'>".$wptop."Type".$wpmiddle."".$row[type].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=type&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
							echo "<div class='text_height_45'>".$wptop."Service".$wpmiddle."".$row[column_2].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_2&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
							echo "<div class='text_height_46'>".$wptop."Warrenty".$wpmiddle."".$row[column_3].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_3&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
							echo "<div class='text_height_47'>".$wptop."Servicing Center".$wpmiddle."".$row[column_4].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_4&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
							echo "<div class='text_height_48'>".$wptop."Help Line".$wpmiddle."".$row[column_5].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_5&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
							echo "<div class='text_height_49'>".$wptop."Web Site".$wpmiddle."".$row[web_site].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
							echo "<div class='text_height_50'>".$wptop."Social Links".$wpmiddle."".$row[social_links].$wpbottom." <div class='see_more'><a href='#0' >See More</a></div>  </div>";
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
							echo "<div class='text_height_37'>".$wptop."Contact".$wpmiddle."".$row[contact].$wpbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
								';
					}
					else if($cata == "ecommerce" ){

							echo $nptop."Social Links".$npmiddle."".$row[social_links]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=social_links&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	

							echo $nptop."Type".$npmiddle."".$row[type]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=type&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	

							echo $nptop."Offer".$npmiddle."".$row[offer]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=offer&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	

							echo $nptop."Description".$npmiddle."".$row[description]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=description&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	
						

							echo $nptop."Web Site".$npmiddle."".$row[web_site]."".$npbottom;
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=web_site&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								  </form></div></div>
							';	
						
							
							echo "<div class='text_height_37'>".$nptop."Contact".$npmiddle."".$row[contact].$npbottom;
								echo '
									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=contact&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter updated data"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div></div>
							';
					}
				}
				//general_info while
			}//general_info if
		} // general_info function




		function general_time_item_list($cata, $sub_cata, $id, $time_status){
			include "data_connection.php";
			
			$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' AND item_time_status='$time_status';";
				
			$data= $conn->query($fetch_item_data);

			$form="";
			if($time_status=="coming"){

				if($data->num_rows>0){

					while ($row=$data->fetch_assoc()) {
							echo '
								<tr>
						          <td> <img style="height:50px;" src="../../image/item_image/'.$row[image_link].'">
						          				<div class="admin_input_box"><form method="POST" target="_blank" action="../single_info_update.php?updateType=item_image&imageLink='.$row[image_link].'&item_id='.$row[id].'&cata='.$_GET[cata].'">
								          			<select id="image_s">
															<option value="square">square</option>
															<option value="landscape">landscape</option>
															<option value="portrait">portrait</option>
													</select>
								          			<div class="col-sm-12 ">
														<div class="item_img parent'.$row[id].'"></div>
														<div class="col-sm-12 item_list child'.$row[id].'"></div>	
														
														<button class="item_img_but '.$row[id].'">UP IMAGE</button>
														<button class="add_more_item_image '.$row[id].'">More</button>

													</div>
													<br>
													<input type="submit" value="UPDATE">
												</form></div>
						          </td>
						          <td class="item-name">'.$row[name].'</td>
						          <td class="item-name">'.$row[price].' tk</td>
						          
						          <td class="item-name">
						            <a class="show-popup food_list_see" href="item_details.php?cata='.$cata.'&item_id='.$row[id].'" data-showpopup="54441" onclick="show_overlay(\''.$cata.'\','.$row[id].')">See Details</a>
						          </td>

						        </tr>
							';

					}
				}
					
				
			}
			else{

				if($data->num_rows>0){

					while ($row=$data->fetch_assoc()) {
							echo '
								<tr>
						          <td> <img style="height:50px;" src="../../image/item_image/'.$row[image_link].'">
						          '.$form.'
						          </td>
						          <td class="item-name">'.$row[name].'</td>
						          <td class="item-name">'.$row[price].' tk</td>
						          
						          <td class="item-name">
						            <a class="show-popup food_list_see" href="item_details.php?cata='.$cata.'&item_id='.$row[id].'" data-showpopup="54441" onclick="show_overlay(\''.$cata.'\','.$row[id].')">See Details</a>
						          </td>

						        </tr>
							';

					}
				}
			}
			
		}



		function general_map_location($cata, $sub_cata, $id)
		{
			include "data_connection.php";
			$fetch_map = "SELECT id, location FROM `map` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";
				
			$data= $conn->query($fetch_map);
			if($data->num_rows>0){

				while ($row=$data->fetch_assoc()) {
					echo '
						<div class="map_list">
							<div class="map_location"> <a href="#0" onclick="getMap(\''.$row[id].'\' )">'.$row[location].' </a> </div>
									
							<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=map_info&column_name=location&id='.$row[id].'">
							    <textarea  name="text" placeholder="Enter Location"></textarea>
							    <input type="submit" value="UPDATE">
							</form></div>
						</div>
					';
				}
			}
		}


		function general_map($cata, $sub_cata, $id)
		{
			include "data_connection.php";
			$fetch_map = "SELECT * FROM `map`  WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' LIMIT 1;";
				
			$data= $conn->query($fetch_map);

			if($data->num_rows>0){

				while ($row=$data->fetch_assoc()) {
					
					if(!empty($row[map]))
						{
							echo '<div ID="branch_map">';
							echo '<br>'.$row[address];
							echo '
								<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=map_info&column_name=address&id='.$row[id].'">
								    <textarea  name="text" placeholder="Enter Address"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
							';
							echo '<div class="info-area">
									<span>'.$row[location].' </span>
					   				'.$row[map].'

									<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=map_info&column_name=map&id='.$row[id].'">
									    <textarea  name="text" placeholder="Enter Embed Map"></textarea>
									    <input type="submit" value="UPDATE">
									</form></div>
					   			</div>
							     ';
							echo '</div>';
						}

				}
			}
		}


		function filter_list($cata, $sub_cata, $id){
			include 'data_connection.php';
			// echo $cata."<>".$sub_cata."<>".$id;
			// $tmp='<li><a data-toggle="tab" href="#" class="combo_but" >'.$cata.'</a></li>';
		 //     $tmp = $tmp.'<li><a data-toggle="tab" href="#" class="combo_but" >'.$sub_cata.'</a></li>';
		     
		 //     return $tmp;

		     $sql = "SELECT filter_1 from `item_list` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' GROUP BY filter_1;";
		     $result = $conn->query($sql);
		     $tmp="";
		     if($result->num_rows>0){
		     	while ($row=$result->fetch_assoc()) {
		     		$f_class = strtolower($row[filter_1]);
		     		$f_class = str_replace(" ", "_", $f_class);
		     		$tmp = $tmp.'<li class=""><a data-toggle="tab" href="#" class="food_filter_list_but '.$f_class.'_but" >'.strtoupper($row[filter_1]).'</a></li>';		
		     	}
		     }

		     return $tmp;
		}

		function tourismMapAndVideo($cata, $sub_cata, $id){
			include 'data_connection.php';
			$fetch_g_data = "SELECT `column_12`, `column_13`, `column_14` FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";

			$data= $conn->query($fetch_g_data);

			if($data->num_rows>0){
				while ($row=$data->fetch_assoc()) {
					echo '
						<div class="col-sm-4 spot-in-map">
			   				<img src="../../image/item_image/'.$row[column_12].'" class="img-responsive" alt="MAP OF BANGLADESH">
				   				
				   				<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=tourism_map1&cata='.$cata.'&cata_id='.$id.'">
				          			<select id="image_s">
											<option value="square">square</option>
											<option value="landscape">landscape</option>
											<option value="portrait">portrait</option>
									</select>
				          			<div class="col-sm-12 ">
										<div class="tourism_map"></div>
										<button class="tourism_map_but ">UP IMAGE</button>
									</div>
									<br>
									<input type="submit" value="UPDATE">
								</form></div>
			   			</div>';

			   		echo '
						<div class="col-sm-4 spot-in-map">
			   				<img src="../../image/item_image/'.$row[column_13].'" class="img-responsive" alt="MAP OF BANGLADESH">
				   				
				   				<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=tourism_map_2&cata='.$cata.'&cata_id='.$id.'">
				          			<select id="image_s">
											<option value="square">square</option>
											<option value="landscape">landscape</option>
											<option value="portrait">portrait</option>
									</select>
				          			<div class="col-sm-12 ">
										<div class="tourism_map2"></div>
										<button class="tourism_map_but2">UP IMAGE</button>
									</div>
									<br>
									<input type="submit" value="UPDATE">
								</form></div>
			   			</div>';





			   		echo  '
			   			<div class="col-sm-4 spot-in-map">
			   				'.$row[column_14].'
					   			<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_14&id='.$id.'">
								    <textarea  name="text" placeholder="Enter updated data"></textarea>
								    <input type="submit" value="UPDATE">
								</form></div>
			   			</div>
					';
				}
			}
			else{
					echo '<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_12&id='.$id.'">
						    <textarea  name="text" placeholder="Enter updated data"></textarea>
						    <input type="submit" value="UPDATE">
						</form></div>';
					echo '<div class="admin_input_box"><form method="POST" action="../single_info_update.php?updateType=general_info&column_name=column_14&id='.$id.'">
						    <textarea  name="text" placeholder="Enter updated data"></textarea>
						    <input type="submit" value="UPDATE">
						</form></div>';
			}
		}
		


	} //class general

?>