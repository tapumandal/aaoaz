<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>
<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="js/jquery.min.js"></script>
  <script src="js/self.js"></script>
  <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />

  	<!-- FOR POP UP DIV -->
	  	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
		<!-- // <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> -->
  	<!-- FOR POP UP DIV -->
<title></title>

</head>

<script>
		$(document).ready(function(){

			var imageShape="square";
			    $('select').on('change', function() {
				  imageShape=this.value ;
				});
		    var title_img_but = $(".item_img_but");

		    var y = 0; //initlal text box count
		    $(title_img_but).click(function(e){ //on add input button click

		    	var classes = $(this).attr('class').split(' ');
		    	var target_class = classes[1];

		        e.preventDefault();
		        if(y < 1){ //max input box allowed
		            y++; //text box increment
		            $('.parent'+target_class).append('<div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num=0"></iframe></div>'); //add input box
		        }
		    });


		    

			$(".aurthopedix_but").click(function(){
					$('.edu-info-box').not('.aurthopedix').hide(800);
					$('.aurthopedix').show(800);    
			});

			$(".nurologist_but").click(function(){
					$('.edu-info-box').not('.nurologist').hide(800);
					$('.nurologist').show(800);    
			});

			$(".general_surgery_but").click(function(){
					$('.edu-info-box').not('.general_surgery').hide(800);
					$('.general_surgery').show(800);    
			});

			var height = $('#info_height').height();        //Get the height of the browser window
		    height = height;
		    if (height>400) {
		    	$('.edu_item_name_list').height(height-15);  //Resize the pageContent div, with a size of 60 - page height.
		    }
		    else{
		    	$('.edu_item_name_list').height(400-15);  //Resize the pageContent div, with a size of 60 - page height.
		    }
		    
		    $("#comment_option").val($("#comment_option option:first").val()); //auto select the first option in select tag in comment section
		});

</script>
<body>

<div class = "container">

	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>

	<div class="col-sm-12 field wide_pattern">
		<div class="col-sm-10">  <!-- Whole information without add -->
			
			<form method='get' action="new_<?php echo $_GET[cata]; ?>.php">
				<input type='hidden' name='cata' value="<?php echo $_GET[cata]; ?>">
				<input type='hidden' name='sub_cata' value="<?php echo $_GET[sub_cata]; ?>">
				<input type='hidden' name="content_id" value="<?php echo $_GET[id]; ?>">
				<input type='hidden' name='update_option' value='update'>
				<input type='submit' value='Advance UPDATE'>
			</form>

			<div class="col-sm-12 first">    <!-- FOR NAME -->
				<div class="info_box">
					<!-- <div class="title">
						<span>Name</span>
					</div> -->
					<div class="title_info">
						<span>
							<?php
								$general_obj = new general();
								$general_obj->general_name($_GET[cata], $_GET[sub_cata], $_GET[id]);
							?>
						</span>
					</div>
				</div>
			</div><!-- FOR NAME -->

			<?php
				$general_obj->seoData($_GET[cata], $_GET[sub_cata], $_GET[id]);

			?>

			
			<!-- Item List LIST -->
			<!-- <div class="col-sm-12 doc-list" >
					<iframe  src="item-list/yellow_custoom_list.php"></iframe>
			</div> -->
			<!-- Item LIST -->

			<!-- PHOTO GALLARY -->
			<div class="col-sm-12 second" >   
   				<div >
	   				<?php 
	   					// include 'edu-slide-show.php';
	   					include 'slide-show.php';
					 ?>
				</div>
	   		</div>			
	   		<!-- PHOTO GALLARY -->
	   		

			<div class="col-sm-8 first" ID="info_height">  <!-- NORMAL INFORMATION  -->
				
				<?php
					$general_obj = new general();
					$general_obj->general_info($_GET[cata], $_GET[sub_cata], $_GET[id]);

					$general_obj->general_map_location($_GET[cata], $_GET[sub_cata], $_GET[id]);					
					$general_obj->general_map($_GET[cata], $_GET[sub_cata], $_GET[id]);					
				?>

			</div>   <!-- NORMAL INFORMATION  -->

			<!-- DOCTORS LIST -->
							<?php
								if($_GET[sub_cata] == "hospital" && $_GET[cata] == "health")
						 		{
							 		$faculty_menu = '<ul>
												            <li>
												              <div class="title_doc_tea">
												                    <span>DOCTORS</span>
												              </div>

												              <div class="edu_list_button">
												                   <nav class="navbar navbar-inverse">
												                      <ul class="nav navbar-nav edu_button_option_list">
												                          <li><a href="#0" class="aurthopedix_but">Aurthopedix</a></li>
												                          <li><a href="#0" class="nurologist_but">Nurologist</a></li>
												                          <li><a href="#0" class="general_surgery_but">General Surgery</a></li>
												                      </ul>
												                   </nav>
												              </div>
												            </li>
												     	</ul>';
								}

								$list_top = '<div class="edu_item_name_list " >
							 					<div class="list">';
							 	$list_bottom = ' </div>
												</div>';

								
								$general_obj = new general();
								$general_obj->doctor_faculty_list($_GET[cata], $_GET[sub_cata], $_GET[id], $list_top, $list_bottom, $faculty_menu);
							?>

			


	   		<!-- <div class="col-sm-6 second" > <!-- ITEM LIST 
   				<div class="menu">
	   				<h4>Item List</h4>
	   				
	   				<iframe src="item-list/item-list.php" width="100%" height="344px"></iframe>
	   			</div>
	   		</div> --> <!-- ITEM LIST -->

	   								 <!-- MAP TAB -->
							   		<!-- <div class="col-sm-6" >  
						   					<div class="map-tab">
									   			  <ul class="nav nav-tabs tab-pos">
												  	 	<li ><a data-toggle="tab" href="#show">Show Map</a></li>
												  	 	<li class="active"><a data-toggle="tab" href="#blank">Hide Map</a></li>
												  </ul>

											   <div class="tab-content">
											   		<div id="blank" class="tab-pane fade">
												    </div>

												    <div id="show" class="tab-pane fade">
												     	<div class="map">
											   				<iframe width="100%" height="250" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1433590604982"  frameborder="0" style="border:0">
											   				</iframe>
												   		</div>
												    </div>
												    
												</div>
										    </div>
							   		</div>	   		 -->   
							   		<!-- MAP TAB -->

				<!-- <div class="clearfix"></div> -->

				<div class="clearfix"></div>

					<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
						<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->
			   				<h3>Public COMMENT || QUESTION || REVIEW</h3>

			   				<?php
			   					include 'comment_3.php';
			   				?>
			   				

			   				<div class="com">
								<?php
			   						$general_obj->fetch_comment($_GET[cata], $_GET[sub_cata], $_GET[id]);
			   					?>
			   				</div>

			   			</div>   <!-- COMMENT  -->
					</div>   <!-- COMMENT AND QUESTIOIN -->

					<div style="height:200px">
						
					</div>


		</div> <!-- Whole information without add -->

		<div class="col-sm-2 third">  <!-- add -->
		</div>
	</div>


</div>
<!-- END OF container -->
</body>
</html>