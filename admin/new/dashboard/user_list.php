<?php
	
	error_reporting(0);
	session_start();
	if(!isset($_SESSION[adminId])){
	    header("Location: ../admin_login.php");
	}


	include '../../data_connection.php';

	$getUser = "SELECT user.*, (SELECT COUNT(*) FROM package WHERE package.user_id = user.id) AS numOfPackage, (SELECT COUNT(*) FROM comment WHERE comment.user_id = user.id AND comment.type='comment') AS numOfComment, (SELECT COUNT(*) FROM comment WHERE comment.user_id = user.id AND comment.type='rate') AS numOfReview FROM user WHERE 1=1";


	$getRes = $conn->query($getUser);	

	if($getRes->num_rows>0){
		while ($row = $getRes->fetch_assoc()) {
			
			
			$pass = decryptPassword($row[pass]);
			
			echo '
			<div class="user_info_panel col-sm-4">
					<div class="user_info_img"><img class="img-responsive" src="../../image/user_image/'.$row[image_link].'"> </div>	
					<div class="user_info_data">
						<h4>'.$row[name].'</h4>
						<span> '.$row[email].'</span>
						<span> '.$row[phone].'</span>
						<span> '.$row[gender].'</span>
						<span> '.$pass.'</span>
						<span>ST:'.$row[signup_type].'</span>
						<span> '.$row[date].'</span>
						<span> '.$row[time].'</span>
					</div>

					<div class="user_usages_info_data">
						<span>TC: '.$row[numOfComment].'</span>
						<span>TR: '.$row[numOfReview].'</span>
						<span>TP: '.$row[total_point].'</span>
						<span>UP: '.$row[used_point].'</span>
						<span>PT: '.$row[numOfPackage].'</span>
					</div>	
			</div>
			';

		}
	}



	function decryptPassword($string){
		$enTmp1 ="";
		$enTmp2 ="";
		$enTmp3 ="";
		$enTmp4 ="";
		$enTmp5 ="";

		$i=0;
		for($i=0; $i<strlen($string);  $i++){
			if($i<4){
				$enTmp1 = $enTmp1.$string[$i];
			}
			else if($i<8){
				$enTmp2 = $enTmp2.$string[$i];
			}
			else if($i<12){
				$enTmp3 = $enTmp3.$string[$i];
			}
			else if($i<16){
				$enTmp4 = $enTmp4.$string[$i];
			}
			else{
				$enTmp5 = $enTmp5.$string[$i];
			}
		}
		$decrypted = $enTmp5."<br>".$enTmp1.$enTmp3.$enTmp4.$enTmp2;
		
		return $decrypted;
	}
?>