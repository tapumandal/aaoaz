<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../../admin_login.php");
  }
?>


<style type="text/css">
	.content_box{
		float: left;
		width: 100%;
		padding: 10px;
		background: white;
	}
	.col-sm-4{
		margin-bottom: 10px;
		padding-left: 0px;
	}
	.content_image{
		float: left;
		max-width: 45%;
		padding-right: 10px;
	}
	.content_box_img img{
		float: left;
		width: 100%;
		border: 1px solid gray;
	}
	.content_data{
		float: left;
		width: 55%;
	}
	.content_data span{
		float: left;
		width: 100%;
		/*border-bottom: 1px dotted rgb(200,200,200);*/
	}
	.content_data h4{
		float: left;
		width: 100%;
		padding-bottom: 3px;
		border-bottom: 1px dotted rgb(190,190,190);
	}
	.content_data h4 span{
		float: none;
		/*
		border-radius: 50px;
		background: rgb(220,220,220);*/
		font-size: 12px;
		padding-left: 6px;
		color: red;
	}

	.content_data a{
		float: left;
		width: 100%;
	}
	.content_data a.c_link{
		float: left;
		width: 100%;
		padding-top: 5px;
	}

	.app_status2 button, .app_status1 button{
		font-size: 15px !important;
		padding: 0px 5px 0px 5px !important;
		border: none !important;
		border-radius: 2px;
		color: white;
		margin-top: 5px;
	}
	.app_status2 button{
		background: green !important; 
	}
	.app_status1 button{
		background: red !important; 
	}


	.app_status2 button:hover {
    	background: red !important;
	}
	.app_status1 button:hover {
    	background: green !important;
	}


</style>


<div class = "content">
	<div class="container-fluid">
		  <div class="row">
		    <div class="col-sm-9 area-demo" id="search_result" >

								<?php 

						    		include '../../data_connection.php';

						    		
						    			$sql = "SELECT * from general_info where `status` = 'not approved';";
						    		
									$data = $conn->query($sql);

									if($data->num_rows>0)
									{
										$i=1;
										while($row = $data-> fetch_assoc())
										{
											$id = $row['id'];
											$name = $row['name'];
											$rating = $row['rating'];
											$location =  $row['location'];
											$catagory =  $row['cata'];
											$image = strpos($row['image_link'], ".jpg");

											if(strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false)
											{
													$image   = "1_1no_title_image.jpg";
											}
											else if(empty($row['image_link']))
											{
													$image   = "1_1no_title_image.jpg";
											}
											
											else {
												$image   = $row['image_link'];
											}


											$subcata = strtolower($row['sub_cata']);
											
											$link = strtolower($catagory);


											if(!empty($row[social_links]))
											{
												$social_links = $row[social_links];
												$social_links = str_replace("http", " http", $social_links);
												$social_links = str_replace("  ", " ", $social_links);
												$social_links = str_replace("   ", " ", $social_links);
												$social_links = str_replace("    ", " ", $social_links);
												if(strpos($social_links, ", ")!==false){
													$linkArray = explode(", ", $social_links);
												}
												else if(strpos($social_links, ",")){
													$linkArray = explode(",", $social_links);
												} 	
												else if(strpos($social_links, " ")){
													$linkArray = explode(" ", $social_links);
												}
												else{
													$linkArray  = array($social_links);
												}
												$k=0;
												while($linkArray[$k]){
													if(strpos($linkArray[$k], "facebook") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>facebook</a>";
													}
													else if(strpos($linkArray[$k], "twitter") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>twitter</a>";
													}
													else if(strpos($linkArray[$k], "google") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>google</a>";
													}
													else if(strpos($linkArray[$k], "instagram") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>instagram</a>";
													}

													if($k==30)break;
													$k++;

												}
												$social_link = $linkArray[0]."  ".$linkArray[1]."  ".$linkArray[2]." ".$linkArray[3]." ".$linkArray[4]." ".$linkArray[5]." ".$linkArray[6]." ".$linkArray[7]." ".$linkArray[8]." ".$linkArray[9];
											}


											
													if($row[status] == "not approved"){
														$status = '<div class="app_status1"><button onclick="contentStatus(\'approved\', \''.$id.'\')">Not Approved</button></div>';
													}
													else if($row[status] == "approved"){
														$status = '<div class="app_status2"><button onclick="contentStatus(\'not approved\', \''.$id.'\')">Approved</button></div>';
													}
											    	echo '
											    		<div class="col-sm-4">
												    		<div class="content_box">
																<div class="content_image">
																	<a class="c_link" href="'.$link.'.php?sub_cata='.$subcata.'&cata='.$row[cata].'&id='.$id.'"><img src="../../image/title_image/'.$image.'" class="img-responsive "	 alt="IMAGE"></img></a>
																</div>

																<div class="content_data">
																	<h4>'.$row[id].'. '.$row[name].'<span>'.$row[total_view].'</span> </h4>
																	'.$status.'
																	<span>'.$row[sub_cata].'</span>
																	<span>'.$social_link.'</span>
																	<span>'.$row[date].'</span>
																	
																</div>
															</div>
														</div>
											    	';
											    	if($i%3 == 0) echo '<div class="clearfix"> </div>';
											    	$i = $i+1;
											   
										}
										
									}

						    			
						    	?>

			</div>
		  </div>
	</div>
</div>