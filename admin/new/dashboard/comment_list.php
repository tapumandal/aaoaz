<?php
	error_reporting(0);
	session_start();
	if(!isset($_SESSION[adminId])){
	    header("Location: ../admin_login.php");
	}
	include 'data_connection.php';

	
	if($_GET[point] < 11 && isset($_GET[commentid]) && isset($_GET[userid])){
		
		insertPoint($_GET[point], $_GET[commentid], $_GET[userid]);
		// echo "<script>window.location = '".$_SERVER['HTTP_REFERER']."';</script>";
	}



	getComment();

	function getComment(){

		include 'data_connection.php';

		if(!isset($_GET[user_id])){
			if($_GET[s]<1){
								$getComment = "SELECT 
								comment.id AS commentId, comment.text, comment.type, comment.rate_1 AS commentRate1, comment.rate_2  AS commentRate2, comment.rate_3  AS commentRate3, comment.avg, comment.points,  comment.status, comment.time AS commentTime, comment.date AS commentDate, comment.status,
								user.id AS userId, user.name AS userName, user.email, user.phone, user.image_link AS userImageLink,  user.total_point, user.used_point, user.date userDate,
								item_list.id AS itemId, item_list.name AS itemName, item_list.image_link AS itemImageLink, item_list.price, item_list.rate_1 AS itemRate1, item_list.rate_2 AS itemRate2, item_list.rate_3 AS itemRate3, item_list.rate_people AS itemRatePeople,
								general_info.id AS generalId, general_info.name AS generalInfoName, general_info.cata, general_info.sub_cata, general_info.rate_1 AS generalInfoRate1, general_info.rate_2 AS generalInfoRate2, general_info.rate_3 AS generalInfoRate3, general_info.review_people AS generalInfoReviewPeople, general_info.total_view, general_info.monthly_view
								FROM `comment` 
								LEFT JOIN `user` ON comment.user_id = user.id 
								LEFT JOIN `item_list` ON comment.item_id = item_list.id
								LEFT JOIN `general_info` ON comment.cata_id = general_info.id 
								ORDER BY comment.date DESC, comment.time DESC  LIMIT 10 ;";
			}else{
								$getComment = "SELECT 
								comment.id AS commentId, comment.text, comment.type, comment.rate_1 AS commentRate1, comment.rate_2  AS commentRate2, comment.rate_3  AS commentRate3, comment.avg, comment.points,  comment.status, comment.time AS commentTime, comment.date AS commentDate, comment.status,
								user.id AS userId, user.name AS userName, user.email, user.phone, user.image_link AS userImageLink,  user.total_point, user.used_point, user.date userDate,
								item_list.id AS itemId, item_list.name AS itemName, item_list.image_link AS itemImageLink, item_list.price, item_list.rate_1 AS itemRate1, item_list.rate_2 AS itemRate2, item_list.rate_3 AS itemRate3, item_list.rate_people AS itemRatePeople,
								general_info.id AS generalId, general_info.name AS generalInfoName, general_info.cata, general_info.sub_cata, general_info.rate_1 AS generalInfoRate1, general_info.rate_2 AS generalInfoRate2, general_info.rate_3 AS generalInfoRate3, general_info.review_people AS generalInfoReviewPeople, general_info.total_view, general_info.monthly_view
								FROM `comment` 
								LEFT JOIN `user` ON comment.user_id = user.id 
								LEFT JOIN `item_list` ON comment.item_id = item_list.id
								LEFT JOIN `general_info` ON comment.cata_id = general_info.id 
								WHERE comment.id < '$_GET[s]'
								ORDER BY comment.date DESC, comment.time DESC  LIMIT 10 ;";
			}
		}else{
			if($_GET[s]<1){
								$getComment = "SELECT 
								comment.id AS commentId, comment.text, comment.type, comment.rate_1 AS commentRate1, comment.rate_2  AS commentRate2, comment.rate_3  AS commentRate3, comment.avg, comment.points,  comment.status, comment.time AS commentTime, comment.date AS commentDate, comment.status,
								user.id AS userId, user.name AS userName, user.email, user.phone, user.image_link AS userImageLink,  user.total_point, user.used_point, user.date userDate,
								item_list.id AS itemId, item_list.name AS itemName, item_list.image_link AS itemImageLink, item_list.price, item_list.rate_1 AS itemRate1, item_list.rate_2 AS itemRate2, item_list.rate_3 AS itemRate3, item_list.rate_people AS itemRatePeople,
								general_info.id AS generalId, general_info.name AS generalInfoName, general_info.cata, general_info.sub_cata, general_info.rate_1 AS generalInfoRate1, general_info.rate_2 AS generalInfoRate2, general_info.rate_3 AS generalInfoRate3, general_info.review_people AS generalInfoReviewPeople, general_info.total_view, general_info.monthly_view
								FROM `comment` 
								LEFT JOIN `user` ON comment.user_id = user.id 
								LEFT JOIN `item_list` ON comment.item_id = item_list.id
								LEFT JOIN `general_info` ON comment.cata_id = general_info.id 
								WHERE user.id = '$_GET[user_id]'
								ORDER BY comment.date DESC, comment.time DESC  LIMIT 10 ;";

								// echo $getComment;
			}else{
								$getComment = "SELECT 
								comment.id AS commentId, comment.text, comment.type, comment.rate_1 AS commentRate1, comment.rate_2  AS commentRate2, comment.rate_3  AS commentRate3, comment.avg, comment.points,  comment.status, comment.time AS commentTime, comment.date AS commentDate, comment.status,
								user.id AS userId, user.name AS userName, user.email, user.phone, user.image_link AS userImageLink,  user.total_point, user.used_point, user.date userDate,
								item_list.id AS itemId, item_list.name AS itemName, item_list.image_link AS itemImageLink, item_list.price, item_list.rate_1 AS itemRate1, item_list.rate_2 AS itemRate2, item_list.rate_3 AS itemRate3, item_list.rate_people AS itemRatePeople,
								general_info.id AS generalId, general_info.name AS generalInfoName, general_info.cata, general_info.sub_cata, general_info.rate_1 AS generalInfoRate1, general_info.rate_2 AS generalInfoRate2, general_info.rate_3 AS generalInfoRate3, general_info.review_people AS generalInfoReviewPeople, general_info.total_view, general_info.monthly_view
								FROM `comment` 
								LEFT JOIN `user` ON comment.user_id = user.id 
								LEFT JOIN `item_list` ON comment.item_id = item_list.id
								LEFT JOIN `general_info` ON comment.cata_id = general_info.id 
								WHERE user.id = '$_GET[user_id]'
								AND comment.id < '$_GET[s]'
								ORDER BY comment.date DESC, comment.time DESC  LIMIT 10 ;";
			}
		}

		$data = $conn->query($getComment);

		if($data->num_rows >0){
			while($row = $data->fetch_assoc()){
				echo '
							<div class="col-sm-12 user_admin_comment_panel">
								<div class="admin_comment_inside">
									<div class="col-sm-3">
										<div class="admin_comment_user">
											<div class="admin_comment_user_img">
												<img class="-img-responsive" src="../../image/user_image/'.$row[userImageLink].'">
											</div>

											<div class="admin_comment_user_info">
												<a href="./?admin_action=comment_list&user_id='.$row[userId].'&s='.$_GET[s].'"><h3>'.$row[userName].'</h3></a>

												<span><b>Phone:</b> '.$row[phone].'</span>
												<span><b>E-Mail:</b> '.$row[email].'</span>
												<span><b>Open:</b> '.$row[userDate].'</span>
												<span><b>Total Points:</b> '.$row[total_point].'</span>
												<span><b>Used Points:</b> '.$row[used_point].'</span>
											</div>
										</div>
									</div>

									<div class="col-sm-2">
										<div class="admin_comment_content">
											<h3>'.$row[generalInfoName].'</h3>
											<span><b>'.$row[cata].' >> </b>'.$row[sub_cata].'</span>
											<span><b>RP:</b> '.$row[generalInfoReviewPeople].'
											<b>AR:</b> '.($row[generalInfoRate1]+$row[generalInfoRate2]+$row[generalInfoRate3])/3/$row[generalInfoReviewPeople].'</span>
											'.getRating($row[generalInfoRate1]/$row[generalInfoReviewPeople], $row[generalInfoRate2]/$row[generalInfoReviewPeople],  $row[generalInfoRate3]/$row[generalInfoReviewPeople], $row[cata], $row[sub_cata]).'
										</div>

									</div>

									<div class="col-sm-2">
								';
								if($row[itemName] != ""){
									echo '
										<div class="admin_comment_item">
											<div class="admin_comment_item_img">
												<img class="img-responsive" src="../../image/item_image/'.$row[itemImageLink].'">
											</div>
											<div class="admin_comment_item_info">
												<h4>'.$row[itemName].'</h4>
												<span><b>Rate People: </b> '.$row[itemRatePeople].'</span>
												<span><b>Rate Average: </b> '.($row[itemRate1]+$row[itemRate2]+$row[itemRate3])/3/$row[itemRatePeople].'</span>					
											</div>
											<div class="admin_comment_item_rating">
											'.getRating($row[itemRate1]/$row[itemRatePeople], $row[itemRate2]/$row[itemRatePeople],  $row[itemRate3]/$row[itemRatePeople], $row[cata], $row[sub_cata]).'
											</div>
										</div>
										';
								}
							echo '
									</div>

									<div class="col-sm-4">
										<div class="admin_comment_comment">
											<div class="admin_comment_data">
												<span><b>'.$row[commentId].' No '.$row[type].': </b><i>'.$row[text].'</i></span>
												<span><u><b>Points: </b>'.$row[points].'</u></span>
												<span><b>Date : </b>'.$row[commentDate].' <b><br>Time : </b>'.$row[commentTime].'<span>
												<b>Status : </b>'.$row[status].'<span>
												'; 
												if($row[type] == "rate"){
													$rating = getRating($row[commentRate1], $row[commentRate2],  $row[commentRate3], $row[cata], $row[sub_cata]);
													echo $rating;
												}
										echo '
											</div>
											<div class="admin_comment_image">
												'.getCommentImg($row[commentId]).'
											</div>
										</div>
									</div>

									<div class="col-sm-1">
										<div class="admin_comment_action">
											<form action="'.$_SERVER['REQUEST_URI'].'">
												<input type="number" name="point">
												<input type="hidden" name="userid" value="'.$row[userId].'">
												<input type="hidden" name="commentid" value="'.$row[commentId].'">
												<input type="hidden" name="admin_action" value="comment_list">
												<input type="hidden" name="s" value="'.$_GET[s].'">
												<select name="comment_status">
													<option value="">Select Status</option>
													<option value="show">Show</option>
													<option value="hide">Hide</option>
												</select>
												<input type="submit" value="SUBMIT">
											</form>

											
										</div>
									</div>

								</div>

							</div>
				';
				$last_comment_id = $row[commentId];
				$user_id = $row[userId];
			}

			

			if(isset($_GET[user_id])){
				echo '<div class="comment_np_btn">
					<a href="./?admin_action=comment_list&user_id='.$user_id.'&s='.($last_comment_id+20).'">PREVIOUS</a>
					<a href="./?admin_action=comment_list&user_id='.$user_id.'&s='.$last_comment_id.'">NEXT</a>  
				</div>';
			}else{
				echo '<div class="comment_np_btn">
					<a href="./?admin_action=comment_list&s='.($last_comment_id+20).'">PREVIOUS</a>
					<a href="./?admin_action=comment_list&s='.$last_comment_id.'">NEXT</a>  
				</div>';	
			}
		}
	}


	function getCommentImg($commentId){
		include 'data_connection.php';

		$imgStore = "";
		$getImg = "SELECT * FROM `comment_image` WHERE `comment_id` = '$commentId' LIMIT 3";

		$data = $conn->query($getImg);

		if($data->num_rows>0){
			while ($row = $data->fetch_assoc()) {
				 $imgStore = $imgStore.'<div class="comment_img_box"> <img class="img-responsive" src="../../image/comment_img/'.$row[image_link].'"></div>';
			}
		}
		return $imgStore;
	}


	function getRating($rate1, $rate2, $rate3, $cata, $sub_cata){
		$rate1 = $rate1*20;
		$rate2 = $rate2*20;
		$rate3 = $rate3*20;

		if($cata == "food"){
			$rateTitle1 = "TASTE";
			$rateTitle2 = "PRICE";
			$rateTitle3 = "SERVICE";
		}
		else if($cata == "life style"){
			if($sub_cata == "fashion" || $sub_cata == "accessorice"){
				$rateTitle1 = "QUALITY";
				$rateTitle2 = "SERVICE";
				$rateTitle3 = "PRICE";		
			}
			else if($sub_cata == "parlor_salon" || $sub_cata == "gym_fitness"){
				$rateTitle1 = "ENVIRONMENT";
				$rateTitle2 = "SERVICE";
				$rateTitle3 = "EXPENSE";		
			}
		}
		else if($cata == "education"){
			$rateTitle1 = "FACULTY";
			$rateTitle2 = "FACILITIE";
			$rateTitle3 = "EXPENSE";
		}

		else if($cata == "health"){
			if($sub_cata=="hospital_clinic"){
				$rateTitle1 = "DOCTOR";
				$rateTitle2 = "FACILITIE";
				$rateTitle3 = "EXPENSE";
			}
		}
		else if($cata == "education"){
			if($sub_cata=="university" || $sub_cata=="college" || $sub_cata=="coaching"){
				$rateTitle1 = "FACULTY";
				$rateTitle2 = "FACILITIES";
				$rateTitle3 = "EXPENSE";
			}
		}
		else if($cata == "electronics"){
				$rateTitle1 = "DESIGN";
				$rateTitle2 = "PERFORMANCE";
				$rateTitle3 = "BATTERY LIFE";
		}
		else if($cata == "residence"){
			if($sub_cata != "to-let"){
				$rateTitle1 = "SERVICE";
				$rateTitle2 = "FACILITIE";
				$rateTitle3 = "EXPENSE";
			}
		}
		else if($cata == "ecommerce"){
			if($sub_cata != "buy_sell" || $sub_cata != "others" || $sub_cata != "job"){
				
				$rateTitle1 = "PRICE";
				$rateTitle2 = "SERVICE";
				$rateTitle3 = "RESPONSE";
			}
			else{
				$rateTitle1 = "SERVICE";
				$rateTitle2 = "PRICE";
				$rateTitle3 = "RESPONSE";
			}
		}


		return '

				<div class="comment_user_review">
					<div class="comment_review_quality">
						<div class="comment_review_title">
							<span>'.$rateTitle1.'</span>
						</div>
						<div class="comment_review_rate">
							<ul class="star-rating star_not_move">
								<li class="current-rating" id="current-rating" style="width:'.$rate1.'%"><!-- will show current rating --></li>
								<span id="ratelinks">
									<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
									<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
									<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
									<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
									<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
								</span>
							</ul>


						</div>
					</div>

					<div class="comment_review_quality">
						<div class="comment_review_title">
							<span>'.$rateTitle2.'</span>
						</div>
						<div class="comment_review_rate">

							<ul class="star-rating">
								<li class="current-rating" id="current-rating" style="width:'.$rate2.'%"><!-- will show current rating --></li>
								<span id="ratelinks">
									<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
									<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
									<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
									<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
									<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
								</span>
							</ul>

						</div>
					</div>

					<!-- style="margin-left:110px" -->
					<div class="comment_review_quality" >
						<div class="comment_review_title">
							<span>'.$rateTitle3.'</span>
						</div>
						<div class="comment_review_rate">

							<ul class="star-rating">
								<li class="current-rating" id="current-rating" style="width:'.$rate3.'%"><!-- will show current rating --></li>
								<span id="ratelinks">
									<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
									<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
									<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
									<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
									<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
								</span>
							</ul>

						</div>
					</div>
				</div>
		';
	}

	function insertPoint($point, $commentId, $userId){
		include 'data_connection.php';

		$previousPoint = 0;
		$getPoints = "SELECT `points` FROM `comment` WHERE `id` = '$commentId'";
		$data = $conn->query($getPoints);
		if($data->num_rows>0){
			while ($row = $data->fetch_assoc()) {
				 $previousPoint = $row[points];
			}
		}


		if(!empty($point)){

			
			$insertPoint = "UPDATE `comment` SET `points`='$point',`admin_id`= '$_SESSION[adminId]' WHERE `id`= '$commentId'";

			
			if($conn->query($insertPoint) == TRUE){

				$updateUserPoint = "UPDATE `user` SET `total_point`= total_point-'$previousPoint'+'$point' WHERE `id` = '$userId'";
				if($conn->query($updateUserPoint) === TRUE){
				}else{
				}

			}	

		}

		if(strlen($_GET[comment_status])>2){
			$insertPoint = "UPDATE `comment` SET `admin_id`= '$_SESSION[adminId]', `status`='$_GET[comment_status]' WHERE `id`= '$commentId'";

			
			if($conn->query($insertPoint) == TRUE){

			}
		}


		

		

		// echo '<script>history.back();</script>';
		
	}

?>

