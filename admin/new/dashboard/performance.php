<?php 
	// error_reporting(0);
	session_start();
	if(!isset($_SESSION[adminId])){
	    header("Location: ../admin_login.php");
	}

	if($_SESSION[adminType] == 'super admin'){ 

?>
	

		
<?php 	
		echo '<div class="all_cata_num">';
	
			include '../../data_connection.php';

			$getSql = "SELECT sub_cata, COUNT(*) AS numSubCata FROM `general_info` WHERE cata ='food' GROUP BY sub_cata;";
			$getRes = $conn->query($getSql);

			if($getRes->num_rows>0){
				echo '<div class="cata_num">
						<div class="cata_num_title">FOOD</div>';
				$total_sub_cata = 0;
				while ($row = $getRes->fetch_assoc()) {
					$total_sub_cata += $row[numSubCata];
					echo '
							<div class="sub_cata_num_list">
								<div class="sub_cata_num"><span>'.$row[sub_cata].' : </span><span>'.$row[numSubCata].'</span></div>
							</div>
					';
				}
				echo '
							<div class="sub_cata_num_list_total">
								<div class="sub_cata_num"><span>Total : </span><span>'.$total_sub_cata.'</span></div>
							</div>
					';

				echo '</div>';
			}



			$getSql = "SELECT sub_cata, COUNT(*) AS numSubCata FROM `general_info` WHERE cata ='life style' GROUP BY sub_cata;";
			$getRes = $conn->query($getSql);

			if($getRes->num_rows>0){
				echo '<div class="cata_num">
						<div class="cata_num_title">Life Style</div>';
				$total_sub_cata = 0;
				while ($row = $getRes->fetch_assoc()) {
					$total_sub_cata += $row[numSubCata];
					echo '
							<div class="sub_cata_num_list">
								<div class="sub_cata_num"><span>'.$row[sub_cata].' : </span><span>'.$row[numSubCata].'</span></div>
							</div>
					';
				}
				echo '
							<div class="sub_cata_num_list_total">
								<div class="sub_cata_num"><span>Total : </span><span>'.$total_sub_cata.'</span></div>
							</div>
					';				

				echo '</div>';
			}



			$getSql = "SELECT sub_cata, COUNT(*) AS numSubCata FROM `general_info` WHERE cata ='education' GROUP BY sub_cata;";
			$getRes = $conn->query($getSql);

			if($getRes->num_rows>0){
				echo '<div class="cata_num">
						<div class="cata_num_title">Education</div>';
				$total_sub_cata = 0;
				while ($row = $getRes->fetch_assoc()) {
					$total_sub_cata += $row[numSubCata];
					echo '
							<div class="sub_cata_num_list">
								<div class="sub_cata_num"><span>'.$row[sub_cata].' : </span><span>'.$row[numSubCata].'</span></div>
							</div>
					';
				}
				echo '
							<div class="sub_cata_num_list_total">
								<div class="sub_cata_num"><span>Total : </span><span>'.$total_sub_cata.'</span></div>
							</div>
					';

				echo '</div>';
			}


			$getSql = "SELECT sub_cata, COUNT(*) AS numSubCata FROM `general_info` WHERE cata ='health' GROUP BY sub_cata;";
			$getRes = $conn->query($getSql);

			if($getRes->num_rows>0){
				echo '<div class="cata_num">
						<div class="cata_num_title">Health</div>';
				$total_sub_cata = 0;
				while ($row = $getRes->fetch_assoc()) {
					$total_sub_cata += $row[numSubCata];
					echo '
							<div class="sub_cata_num_list">
								<div class="sub_cata_num"><span>'.$row[sub_cata].' : </span><span>'.$row[numSubCata].'</span></div>
							</div>
					';
				}
				echo '
							<div class="sub_cata_num_list_total">
								<div class="sub_cata_num"><span>Total : </span><span>'.$total_sub_cata.'</span></div>
							</div>
					';

				echo '</div>';
			}


			$getSql = "SELECT sub_cata, COUNT(*) AS numSubCata FROM `general_info` WHERE cata ='residence' GROUP BY sub_cata;";
			$getRes = $conn->query($getSql);

			if($getRes->num_rows>0){
				echo '<div class="cata_num">
						<div class="cata_num_title">Residence</div>';
				$total_sub_cata = 0;
				while ($row = $getRes->fetch_assoc()) {
					$total_sub_cata += $row[numSubCata];
					echo '
							<div class="sub_cata_num_list">
								<div class="sub_cata_num"><span>'.$row[sub_cata].' : </span><span>'.$row[numSubCata].'</span></div>
							</div>
					';
				}
				echo '
							<div class="sub_cata_num_list_total">
								<div class="sub_cata_num"><span>Total : </span><span>'.$total_sub_cata.'</span></div>
							</div>
					';

				echo '</div>';
			}



			$getSql = "SELECT sub_cata, COUNT(*) AS numSubCata FROM `general_info` WHERE cata ='tourism' GROUP BY sub_cata;";
			$getRes = $conn->query($getSql);

			if($getRes->num_rows>0){
				echo '<div class="cata_num">
						<div class="cata_num_title">Tourism</div>';
				$total_sub_cata = 0;
				while ($row = $getRes->fetch_assoc()) {
					$total_sub_cata += $row[numSubCata];
					echo '
							<div class="sub_cata_num_list">
								<div class="sub_cata_num"><span>'.$row[sub_cata].' : </span><span>'.$row[numSubCata].'</span></div>
							</div>
					';
				}
				echo '
							<div class="sub_cata_num_list_total">
								<div class="sub_cata_num"><span>Total : </span><span>'.$total_sub_cata.'</span></div>
							</div>
					';

				echo '</div>';
			}



			$getSql = "SELECT sub_cata, COUNT(*) AS numSubCata FROM `general_info` WHERE cata ='ecommerce' GROUP BY sub_cata;";
			$getRes = $conn->query($getSql);

			if($getRes->num_rows>0){
				echo '<div class="cata_num">
						<div class="cata_num_title">E-Commerce</div>';
				$total_sub_cata = 0;
				while ($row = $getRes->fetch_assoc()) {
					$total_sub_cata += $row[numSubCata];
					echo '
							<div class="sub_cata_num_list">
								<div class="sub_cata_num"><span>'.$row[sub_cata].' : </span><span>'.$row[numSubCata].'</span></div>
							</div>
					';
				}
				echo '
							<div class="sub_cata_num_list_total">
								<div class="sub_cata_num"><span>Total : </span><span>'.$total_sub_cata.'</span></div>
							</div>
					';

				echo '</div>';
			}

		echo '</div>';
	}
?>

