<?php
	error_reporting(0);
	session_start();
	if(!isset($_SESSION[adminId])){
	    header("Location: ../../admin_login.php");
	}
?>

<?php  if($_SESSION[adminType] == 'super admin'){ ?>

<script type="text/javascript">
	function admin_status(action){
		alert(action);
	}

	function seeLoginInfo(id){
		
		var value = $(".admin_num_"+id).height();

		var len = $(".admin_num_"+id+' .login_info_box').length;
		if(len>0){
			var value2 = len*40+90;	
		}
		

		if(value < 100){
			$(".admin_num_"+id).css("cssText", "height:"+value2+"px; opacity:1; ");
			// $(".admin_num_"+id).css("cssText", "height:auto; opacity:1; ");
		}
		else{
			$(".admin_num_"+id).css("cssText", "height:60px; opacity:1;");
		}
		
		

	}
</script>

<style type="text/css">
	.active_admin_list, .inactive_admin_list{
		float: left;
		width: 100%;
		padding: 10px;
		margin-bottom: 100px;
		background: white;
		border:1px solid rgb(190,190,190);
	}

	.admin_list_title{
		float: left;
		width: 100%;
		text-align: center;
		background: rgb(240,240,240);
		padding: 10px;
	}

	.admin_row{
		float: left;
		width: 100%;
	}
	.admin_image{
		height: 50px;
		width: 50px;
		float: left;
		background: rgb(180,180,180);
		margin-right: 20px;
	}

	.admin_info{
		float: left;
		margin-right: 20px;
		text-align: center;
	}
	.admin_action{
		float: left;
		margin-right: 20px;
		text-align: center;
		z-index: 100;
	}
	.check_login{
		float: left;
		margin-right: 20px;
		text-align: center;
	}
	.admin_info span{
		padding: 13px;
	}

	.login_info{
		width: 100%;
		float: left;
		display: block;
		margin-top: 10px;
	}
	.login_info_inside{
		float: left;
		width: 100%;
		background: rgb(240,240,230);
		padding: 20px;
	}

	.login_list, .logout_list{
		float: left;
		width: 50%;
		border-bottom: 1px solid gray;
		padding: 4px;
		margin-bottom: 6px;
		text-align: center;
	}
	.login_list span, .logout_list span{
		float: left;
		width: 25%;
	}
	.login_list span:first-child{
		width: 20% !important; 	
	}

	.total_admin_info{
		height: 60px;
		float: left;
		width: 100%;
		overflow: hidden;
		padding-top: 10px;

		transition: height .300s ease-in-out;  
	}


</style>



<div class="active_admin_list">
	<div class="admin_list_title">
		<h2>Active User</h2>
	</div>

<?php


	include '../../data_connection.php';

	$getAdminInfo = "SELECT * FROM `admin` WHERE `status` = 'enable'";

	$result = $conn->query($getAdminInfo);

	if($result->num_rows >0){
		while ($row = $result-> fetch_assoc()) {
		echo '<div class="total_admin_info admin_num_'.$row[id].'">';
			echo '
				<div class="admin_row">
					<div class="admin_image col-sm-1">
						<img src="'.$row[image_link].'">
					</div>
					
					<div class="admin_info col-sm-9">
						<span class="col-sm-1">'.$row[id].' :</span>
						<span class="col-sm-3">'.$row[name].'</span>
						<span class="col-sm-3">'.$row[email].'</span>
						<span class="col-sm-3">'.$row[phone].'</span>
						<span class="col-sm-2">'.$row[pass].'</span>
					</div>

					
					<div class="check_login col-sm-1">
						<a href="#" onclick="seeLoginInfo('.$row[id].')">Access Info</a>
					</div>

					<div class="admin_action col-sm-1">
						     <a  href="'.$_SERVER['REQUEST_URI'].'&task=disable&admin_id='.$row[id].'" >Disable</a>
					</div>
				</div>
			';

			$getAdminAccessInfo = "SELECT * FROM `admin_access_time` WHERE `admin_id`=".$row[id]."";
			$result2 = $conn->query($getAdminAccessInfo);

			if($result2->num_rows >0){

				echo '<div class="login_info">
						<div class="login_info_inside">
						';

				while ($row2 = $result2-> fetch_assoc()) {

					echo '
							<div class="login_info_box">
								<div class="login_list">
									<span>Login : </span>  <span>'.$row2['login date'].'</span>  <span>'.$row2['login time'].'</span>
								</div>

								<div class="logout_list">
									<span>Logout : </span>  <span>'.$row2['logout date'].'</span>  <span>'.$row2['logout time'].'</span>
								</div>
							</div>
					';
				}
				echo "</div>
				</div>";
			}

		echo "</div>";
		}
	}

?>

</div>


<div class="inactive_admin_list">
	<div class="admin_list_title">
		<h2>Inactive User</h2>
	</div>

<?php
$getAdminInfo = "SELECT * FROM `admin` WHERE `status` = 'disable'";

	$result = $conn->query($getAdminInfo);

	if($result->num_rows >0){
		while ($row = $result-> fetch_assoc()) {
			
			echo '<div class="total_admin_info admin_num_'.$row[id].'">';
			echo '
				<div class="admin_row">
					<div class="admin_image col-sm-1">
						<img src="'.$row[image_link].'">
					</div>
					
					<div class="admin_info col-sm-9">
						<span class="col-sm-1">'.$row[id].' :</span>
						<span class="col-sm-3">'.$row[name].'</span>
						<span class="col-sm-4">'.$row[email].'</span>
						<span class="col-sm-4">'.$row[phone].'</span>
						<span class="col-sm-4">'.$row[pass].'</span>
					</div>

					
					<div class="check_login col-sm-1">
						<a href="#" onclick="seeLoginInfo('.$row[id].')">Access Info</a>
					</div>

					<div class="admin_action col-sm-1">
						<a  href="'.$_SERVER['REQUEST_URI'].'&task=enable&admin_id='.$row[id].'" >Enable</a>
					</div>
				</div>
			';

			$getAdminAccessInfo = "SELECT * FROM `admin_access_time` WHERE `admin_id`=".$row[id]."";
			$result2 = $conn->query($getAdminAccessInfo);

			if($result2->num_rows >0){

				echo '<div class="login_info">
						<div class="login_info_inside">
						';

				while ($row2 = $result2-> fetch_assoc()) {

					echo '
							<div class="login_info_box">
								<div class="login_list">
									<span>Login : </span>  <span>'.$row2['login date'].'</span>  <span>'.$row2['login time'].'</span>
								</div>

								<div class="logout_list">
									<span>Logout : </span>  <span>'.$row2['logout date'].'</span>  <span>'.$row2['logout time'].'</span>
								</div>
							</div>
					';
				}
				echo "</div>
				</div>";
			}

		echo "</div>";
		}
	}
?>




<?php
	
	if(isset($_GET[task])){
		$adminStatus = "UPDATE `admin` SET `status`='$_GET[task]' WHERE `id`='$_GET[admin_id]' AND `status` != '$_GET[task]'";
		if($conn->query($adminStatus)){
			echo "<script>window.location='".$_SERVER['HTTP_REFERER']."'</script>";
		}else{
			echo "Not success";
		}
	}
?>
</div>


<?php  } ?>
