<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>

<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="../../js/jquery.min.js"></script>
  <link href="../../css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/css_for_upload.css" rel="stylesheet" type="text/css" media="all" />
   <script src="../../js/self.js"></script>

   <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> -->
  <!-- WINDOW OVERLAY -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/win_overlay.js"></script>
    <link type="text/css" rel="stylesheet" href="../../css/win_overlay.css" />
  <!-- WINDOW OVERLAY -->
<title></title>
</head>


<body>
<script>
	function mapInput(){
		var num_branch= document.getElementById("num_branch").options;		
		var n= document.getElementById("num_branch").selectedIndex;		
		//alert(num_branch[n].text);
		for (var i = 1; i <= num_branch[n].text; i++) {
			// alert(i);
			 var map_pos       = $(".info");
			$(map_pos).append('<div class="input_box last"><div class="input_box"><input class="input_field" type="text" name="main_location[]" placeholder="Main Location"></div><div class="input_box"><input class="input_field" type="text" name="contact[]" placeholder="Contact"></div><div class="input_box"><input class="input_field" type="text" name="full_address[]" placeholder="Full Address"></div><textarea class="input_field" type="text" name="map[]" placeholder="Map"></textarea></div>'); //add input box
		};
	}

	function filterChange(){
		var value = document.getElementById("life_style_filter").value;
		
		if(value=="male"){
			document.getElementById('male_filter').style.display = "block";

			document.getElementById('female_filter').style.display = "none";
			document.getElementById('kids_filter').style.display = "none";
		}
		else if(value=="female"){
			document.getElementById('female_filter').style.display = "block";

			document.getElementById('male_filter').style.display = "none";
			document.getElementById('kids_filter').style.display = "none";
		}
		else if(value=="kids"){
			document.getElementById('kids_filter').style.display = "block";

			document.getElementById('male_filter').style.display = "none";
			document.getElementById('female_filter').style.display = "none";
		}
	}
	// var imageShape;
	// function getImageSize()
	// {
	//     	var imageShape = document.getElementById("image_s").value;
	//     	alert(imageShape);
	// }

	
	 $(document).ready(function() {
	 	var cata_name= document.getElementById("cata_name").firstChild.data;
	    var sub_cata_name= document.getElementById("sub_cata_name").firstChild.data;
	    


	    var getLifeStyleFilter1 = "";
	    var Fnum = 0;
	    $.ajax({
				type: "GET",
				url: "code_php.php",
				data: "getFilter=get_life_style_filter1&sub_cata_4_filter="+sub_cata_name,
				cache: false,
				async: false,
				success: function(result) {
					// document.getElementById("single_com").innerHTML = result;
					getLifeStyleFilter = result;
					// alert(getLifeStyleFilter);
				},
				error: function(result) {
					alert("some error occured, please try again later");
				}
		});

	    var lifeStyleFilter = '<div class="life_style_1_filter"><select id="life_style_filter"  name="lifeStyleFilter[]">'+
        '<option  value="">Select Filter 1</option>'+
        getLifeStyleFilter+
        '</select>'+
        '<input type="text" name="life_style_filter1_b[]">'+
        '</div>';


			
		var getLifeStyleSubFilter = "";
	    $.ajax({
				type: "GET",
				url: "code_php.php",
				data: "getFilter=get_life_style_sub_filter&sub_cata_4_filter="+sub_cata_name,
				cache: false,
				async: false,
				success: function(result) {
					// document.getElementById("single_com").innerHTML = result;
					getLifeStyleSubFilter = result;
					// alert(getLifeStyleSubFilter);
				},
				error: function(result) {
					alert("some error occured, please try again later");
				}
		});        
	    var lifeStyleSubFilter = getLifeStyleSubFilter;




		// $("#sub_cata").val($("#sub_cata option:first").val()); //auto select the first option in select tag in comment section
	    var title_img       = $(".title_img");
	    var title_img_but = $(".title_img_but");

	    var y = 0; //initlal text box count
	    $(title_img_but).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(y < 1){ //max input box allowed
	            y++; //text box increment
	            $(title_img).append('<div class="title_image"><iframe src="../upload_crop/upload_crop.php?type=title_image"></iframe></div>'); //add input box
	        }
	    });



		
		var main_image = $(".main_image");
		var main_img_but = $(".main_img_but");
		var z=-1;
		$(main_img_but).click(function(e){ //on add input button click
	        
	        e.preventDefault();
	        if(z < 10){ //max input box allowed
	            z++; //text box increment

	            $(main_image).append('<div class="col-sm-6"><div class="title_image"><iframe src="../upload_crop/upload_crop.php?type=main_image&main_image_num='+z+'"></iframe></div></div>'); //add input box
	        }
	    });






		var imageShape="square";
	    $('select').on('change', function() {
		  imageShape=this.value ;
		});

	    var max_fields      = 100; //maximum input boxes allowed
	    var wrapper         = $(".item_list"); //Fields wrapper
	    var add_button      = $(".add_item_button"); //Add button ID
	    
	    var x = -1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	    	
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	           
	            if(sub_cata_name == "fashion"){
	            	$(wrapper).append('<div class="col-sm-6 one_item_list"><div class="item_list_info"><div class="input_box"><input class="input_field item_name" type="text" name="item_name[]" placeholder="ITEM NAME"></div><div class="input_box"><input class="input_field item_price" type="text" name="item_price[]" placeholder="ITEM PRICE"></div><div class="input_box"><input class="input_field item_price" type="text" name="item_color[]" placeholder="ITEM COLOR"></div><div class="input_box"><input class="input_field item_price" type="text" name="item_size[]" placeholder="ITEM SIZE"></div><div class="input_box"><textarea class="input_field item_des" type="text" name="item_description[]" placeholder="ITEM DESCRIPTION"></textarea></div>'+lifeStyleFilter+lifeStyleSubFilter+'<input type="text" name="life_style_sub_filter_b[]"></div><div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe><a href="#0" class="more_image">Add more Item image</a></div></div>'); //add input box	
	        	}
	        	else if(sub_cata_name == "accessorice"){
	        		$(wrapper).append('<div class="col-sm-6 one_item_list">'+
							'<div class="item_list_info">'+
								'<div class="input_box">'+
								'	<input class="input_field item_name" type="text" name="item_name[]" placeholder="ITEM NAME">'+
								'</div>'+
								'<div class="input_box">'+
									'<input class="input_field item_price" type="text" name="item_price[]" placeholder="ITEM PRICE">'+
								'</div>'+
								'<div class="input_box">'+
									'<textarea class="input_field item_des" type="text" name="item_description[]" placeholder="ITEM DESCRIPTION"></textarea>'+
								'</div>'+lifeStyleFilter+lifeStyleSubFilter+
								'<input type="text" name="life_style_sub_filter_b[]">'+
							'</div>'+
						'<div class="item_image">'+
						'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
						'<a href="#0" class="more_image">Add more Item image</a>'+
						'</div>'+
						'</div>'); //add input box
	        	}
	        	else if(sub_cata_name == "parlor_salon"){
	        		$(wrapper).append('<div class="col-sm-6 one_item_list">'+
							'<div class="item_list_info">'+
								'<div class="input_box">'+
								'	<input class="input_field item_name" type="text" name="item_name[]" placeholder="ITEM NAME">'+
								'</div>'+
								'<div class="input_box">'+
									'<input class="input_field item_price" type="text" name="item_price[]" placeholder="ITEM PRICE">'+
								'</div>'+
								'<div class="input_box">'+
									'<textarea class="input_field item_des" type="text" name="item_description[]" placeholder="ITEM DESCRIPTION"></textarea>'+
								'</div>'+lifeStyleFilter+lifeStyleSubFilter+
								'<input type="text" name="life_style_sub_filter_b[]">'+
							'</div>'+
						'<div class="item_image">'+
						'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
						'<a href="#0" class="more_image">Add more Item image</a>'+
						'</div>'+
						'</div>'); //add input box
	        	}
	        	else if(sub_cata_name == "gym_fitness"){
	        		$(wrapper).append('<div class="col-sm-6 one_item_list">'+
						'<div class="item_list_info">'+
						'<div class="input_box">'+
						'<input class="input_field item_name" type="text" name="item_name[]" placeholder="ITEM NAME">'+
						'</div>'+
						
						'<div class="input_box">'+
						'<textarea class="input_field item_des" type="text" name="item_description[]" placeholder="ITEM DESCRIPTION"></textarea>'+
						'</div>'+
						'</div>'+
						'<div class="item_image">'+
						'<iframe src="../upload_crop/upload_crop.php?type=item_image&imageShape='+imageShape+'&item_num='+x+'"></iframe>'+
						'<a href="#0" class="more_image">Add more Item image</a>'+
						'</div>'+
						'</div>'); //add input box
	        	}
	        	d=-1;
	        }
	    });


	     
	    var wrapper2         = $(".item_list"); //Fields wrapper
	    var d = -1; 

	    $(document).on("click", '.more_image', function(event) { 
		   		d++; //text box increment
	           	$(wrapper2).append('<div class="item_image"><iframe src="../upload_crop/upload_crop.php?type=item_multi_image&imageShape='+imageShape+'&item_num='+x+'&item_multi_num='+d+'"></iframe></div>'); //add input box	
	        
		});



	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })


	    
	     function xlFileUpload(input) {

	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	        	    
	            reader.onload = function (e) {
	                  var url = e.target.result;
	                  var oReq = new XMLHttpRequest();
	                  oReq.open("GET", url, true);
	                  oReq.responseType = "arraybuffer";

	                  oReq.onload = function(e) {
	                    var arraybuffer = oReq.response;

	                    var data = new Uint8Array(arraybuffer);
	                    var arr = new Array();
	                    for(var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
	                    var bstr = arr.join("");

	                    var workbook = XLSX.read(bstr, {type:"binary"});
	                    
	                    var first_sheet_name = workbook.SheetNames[0];

	                    var worksheet = workbook.Sheets[first_sheet_name];
	                    

	                    var objArray = XLSX.utils.sheet_to_json(worksheet,{raw:true});

	                    var limit=0;

	                    var wrapper2         = $(".item_list");
	                    
	                    var itemName = "";
						var itemPrice = "";
						var itemDescription = "";
						var itemType = "";

	                    while(objArray[limit]){
	                      itemName = objArray[limit].NAME;
	                      itemPrice = objArray[limit].PRICE;
	                      itemColor = objArray[limit].COLOR;
	                      itemSize = objArray[limit].SIZE;
	                      itemDescription = objArray[limit].Description;
	                      itemFilter1 = objArray[limit].Filter1;
	                      itemFilter2 = objArray[limit].Filter2;


	                      if(itemName == 'undefined' || itemName == null || itemName == ''){
	                      	itemName = "";
	                      }
	                      if(itemPrice == 'undefined' || itemPrice == null || itemPrice == ''){
	                      	itemPrice = "---";
	                      }
	                      
	                      if(itemColor == 'undefined' || itemColor == null || itemColor == ''){
	                      	itemColor = "---";
	                      }
	                      if(itemSize == 'undefined' || itemSize == null || itemSize == ''){
	                      	itemSize = "---";
	                      }

	                      if(itemDescription == 'undefined' || itemDescription == null || itemDescription == ''){
	                      	itemDescription = "---";
	                      }
	                      if(itemFilter1 == 'undefined' || itemFilter1 == null || itemFilter1 == ''){
	                      	itemFilter1 = "---";
	                      }
	                      if(itemFilter2 == 'undefined' || itemFilter2 == null || itemFilter2 == ''){
	                      	itemFilter2 = "---";
	                      }

	                      if(itemName != "" || itemName != null || itemName != ''){
	                      		$(wrapper2).append('<div class="xmlFilePanel">'+
	                      					'<span>Name: </span><input class="input_field item_name" type="text" name="item_name[]" value="'+itemName+'" placeholder="ITEM NAME">'+
											'<span>Price: </span><input class="input_field item_price" type="text" name="item_price[]" value="'+itemPrice+'" placeholder="ITEM PRICE">'+

											'<span>Price: </span><input class="input_field item_price" type="text" name="item_color[]" value="'+itemColor+'" placeholder="ITEM Color">'+
											'<span>Price: </span><input class="input_field item_price" type="text" name="item_size[]" value="'+itemSize+'" placeholder="ITEM Size">'+

											'<span>Description: </span><input class="input_field item_des" type="text" name="item_description[]" value="'+itemDescription+'" placeholder="ITEM DESCRIPTION">'+

											'<span>Price: </span><input class="input_field item_price" type="text" name="life_style_filter1_b[]" value="'+itemFilter1+'" placeholder="ITEM Filter 1">'+
											'<span>Price: </span><input class="input_field item_price" type="text" name="life_style_sub_filter_b[]" value="'+itemFilter2+'" placeholder="ITEM Filter 2">');
	                      }

	                    itemName = "";
						itemPrice = "";
						itemDescription = "";
						itemType = "";

	                      limit++;
	                    }

	                  }

	                  oReq.send();



	            }
	            
	            reader.readAsDataURL(input.files[0]);
	        }
	    }
	    
	    $("#xlFile").change(function(){

	        xlFileUpload(this);
	    });
	    
	});

</script>






















<!-- <div class="col-sm-6 one_item_list">
<div class="item_list_info">
<div class="input_box">
<input class="input_field item_name" type="text" name="item_name[]" placeholder="ITEM NAME">
</div>
<div class="input_box">
<input class="input_field item_price" type="text" name="item_price[]" placeholder="ITEM PRICE">
</div>
<div class="input_box">
<textarea class="input_field item_des" type="text" name="item_description[]" placeholder="ITEM DESCRIPTION"></textarea>
</div>'+styleItemFilter_1+styleItemFilter_2+styleItemFilter_3+'
</div>
<div class="item_image">
<iframe src="../upload_crop/upload_crop.php?type=item_image&item_num='+x+'"></iframe>
</div>
</div> -->



<div class = "container food">


	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>
	<div class="col-sm-12 field">
	<div class="cata_sub_cata">
		<h1><div class="cata_t">Catagory: </div> <div ID="cata_name"><?php echo $_GET[cata] ?></div></h1>
		<h1><div class="sub_cata_t">Sub Catagory: </div><div ID="sub_cata_name"><?php echo $_GET[sub_cata] ?></div></h1>

		
	</div>
	<?php
			$update_option=$_GET[update_option];
			if(!isset($update_option)){
			
				
				echo "actual Link".$actual_link;
				echo "<form method='get'  action='".$actual_link."'>
						<input type='hidden' name='cata' value='".$_GET[cata]."'>
						<input type='hidden' name='sub_cata' value='".$_GET[sub_cata]."'>
						<input type='hidden' name='update_option' value='new'>
						<input type='submit' value='NEW'>
					</form>";
				echo "<form method='get' action='".$actual_link."'>
						<input type='hidden' name='cata' value='".$_GET[cata]."'>
						<input type='hidden' name='sub_cata' value='".$_GET[sub_cata]."'>
						<input type='text' name='content_id' placeholder='Content ID you want to UPDATE'>
						<input type='hidden' name='update_option' value='update'>
						<input type='submit' value='UPDATE'>
					</form>";
			}

		if($update_option == "new" && isset($update_option)){

	?>
	<form method="POST" target="_blank" action="../upload_data.php?cata=<?php echo $_GET[cata] ?>&sub_cata=<?php echo $_GET[sub_cata] ?>" >
		
			<div class="col-sm-12 info_title_img">
				<h2>Main Information and Title Image</h2>
				
				<div class="col-sm-12 info">
					<div class="input_box"><input class="input_field" type="text" name="name" placeholder="Name"></div>
					<div class="input_box"><input class="input_field" type="text" name="type" placeholder="Type"></div>
					<div class="input_box"><textarea class="input_field" type="text" name="description" placeholder="Description"></textarea></div>
					<div class="input_box"><input class="input_field" type="text" name="speciality" placeholder="Speciality"></div>
					<div class="input_box"><input class="input_field" type="text" name="offer" placeholder="Offer"></div>
					
					<div class="input_box"><input class="input_field" type="text" name="program_schedual" placeholder="Program Schedual (entertainment)"></div>
					<div class="input_box"><input class="input_field" type="text" name="sponsor" placeholder="Sponsor (entertainment)"></div>
					<!-- <div class="input_box"><input class="input_field" type="text" name="branches" placeholder="Branches"></div> -->
					<div class="input_box"><input class="input_field" type="text" name="opening_hour" placeholder="Opening Hour"></div>
					

					<!-- <div class="input_box"><input class="input_field" type="text" name="main_location" placeholder="Main Location"></div>
					<div class="input_box"><input class="input_field" type="text" name="location" placeholder="Full Address"></div> -->
					<div class="input_box"><input class="input_field" type="text" name="web_site" placeholder="Web Site"></div>
					<div class="input_box"><input class="input_field" type="text" name="social_links" placeholder="Social Links"></div>
					<div class="input_box"><input class="input_field" type="text" name="main_contact" placeholder="Contact"></div>
					
					<select id="num_branch" name="num_branch" onchange="mapInput()">
						<option >Select Number</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="10">20</option>
					</select>
					<span>Select Number of Branches</span>
				</div>

				<div class="col-sm-8 title_img ">
					<button class="title_img_but">UP TITLE IMAGE</button>
				</div>
			</div>

			<div class="col-sm-12 main_image">
				<h2>Main Image (Slide Show)</h2>
				<button class="main_img_but">UP MAIN IMAGE</button>

			</div>
		<select id="image_s">
			<option value="square">square</option>
			<option value="landscape">landscape</option>
			<option value="portrait">portrait</option>
		</select>
		<div class="all_item_data">
			<div class="col-sm-12 item_list"></div>
			<button class="add_item_button">Add Item</button>

			<br> <br><div class="xmlfilebtn">Select Excel File. Formation: NAME, PRICE, COLOR, SIZE, Description, Filter1, Filter2<br><input type='file' id="xlFile"/> </div> <br> 



		</div>

		<div class="submit_button">
			<input type="submit" value="SUBMIT">
		</div>
	</form>



	<?php
			}
			$content_id = $_GET[content_id];
			if($update_option == "update" && isset($update_option) && !empty($content_id)){
			echo "<h3>UPDATE CONTENT: ".$_GET[content_id]."</h3>";
	?>
			
			<form method="POST" target="_blank" action="../upload_data.php?cata=<?php echo $_GET[cata] ?>&sub_cata=<?php echo $_GET[sub_cata] ?>&content_id=<?php echo $content_id ?>" >
				<div class="col-sm-12 info_title_img">
					

					<div class="col-sm-8 title_img ">
						<button class="title_img_but">UP TITLE IMAGE</button>
					</div>
				</div>

			<div class="info">
			</div>

			<select id="num_branch" name="num_branch" onchange="mapInput()">
						<option >Select Number</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="10">20</option>
			</select>
			<span>Select Number of Branches</span>

				<div class="col-sm-12 main_image">
					<h2>Main Image (Slide Show)</h2>
					<button class="main_img_but">UP MAIN IMAGE</button>

				</div>
			<select id="image_s">
				<option value="square">square</option>
				<option value="landscape">landscape</option>
				<option value="portrait">portrait</option>
			</select>
			<div class="all_item_data">
				<div class="col-sm-12 item_list"></div>
				<button class="add_item_button">Add Item</button>

				<br> <br><div class="xmlfilebtn">Select Excel File. Formation: NAME, PRICE, COLOR, SIZE, Description, Filter1, Filter2<br><input type='file' id="xlFile"/> </div> <br> 



			</div>

			
			<div class="submit_button">
				<input type="submit" value="SUBMIT">
			</div>
		</form>
	<?php
		}

	?>
		<div class="admin_footer">
			
		</div>
	</div>

</div>
<!-- END OF container -->
</body>
</html>