<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>

<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="../../js/jquery.min.js"></script>
  <link href="../../css/style.css" rel="stylesheet" type="text/css" media="all" />	
<title></title>
</head>

<style type="text/css">
	.content_box{
		float: left;
		width: 100%;
		padding: 10px;
		background: white;
	}
	.col-sm-3{
		margin-bottom: 10px;
		padding-left: 0px;
		border-right: 10px solid #E6E6E6;
	}
	.content_image{
		float: left;
		max-width: 45%;
		padding-right: 10px;
	}
	.content_box_img img{
		float: left;
		width: 100%;
		border: 1px solid gray;
	}
	.content_data{
		float: left;
		width: 55%;
	}
	.content_data span{
		float: left;
		width: 100%;
		/*border-bottom: 1px dotted rgb(200,200,200);*/
	}
	.content_data h4{
		float: left;
		width: 100%;
		padding-bottom: 3px;
		border-bottom: 1px dotted rgb(190,190,190);
	}
	span.show_view{
		float: none;
		/*
		border-radius: 50px;
		background: rgb(220,220,220);*/
		font-size: 12px;
		padding-left: 6px;
		color: red;
	}

	.content_data a{
		float: left;
		width: 100%;
	}
	.content_data a.c_link{
		float: left;
		width: 100%;
		padding-top: 5px;
	}

	.app_status2 button, .app_status1 button{
		font-size: 15px !important;
		padding: 0px 5px 0px 5px !important;
		border: none !important;
		border-radius: 2px;
		color: white;
		margin-top: 5px;
	}
	.app_status2 button{
		background: green !important; 
	}
	.app_status1 button{
		background: red !important; 
	}

	.app_status2 button:hover {
    	background: red !important;
	}
	.app_status1 button:hover {
    	background: green !important;
	}

</style>

<body>

<div class = "container">

	<div class = "main-top">
		<div class="main">
				
					
				<?php 	include 'header.php'; ?>
				
	<?php 
		// if(isset($_GET[cata]) && isset($_GET[sub_cata])){
		// 	echo '<a href="new_'.$_GET[cata].'.php?cata='.$_GET[cata].'&sub_cata='.$_GET[sub_cata].'">ADD New Profile</a>';
		// }
		// else if(isset($_GET[cata]) && $_GET[cata]=="electronics"){
		// 	echo '<a href="new_'.$_GET[cata].'.php?cata='.$_GET[cata].'&sub_cata='.$_GET[sub_cata].'">ADD New Profile</a>';
		// }
		if(isset($_GET[cata]) && $_GET[cata] == "food" || isset($_GET[sub_cata])){
			echo '<a href="new_'.$_GET[cata].'.php?cata='.$_GET[cata].'&sub_cata='.$_GET[sub_cata].'">ADD New Profile</a>';
		}
		else if(isset($_GET[cata]) && $_GET[cata] == "residence" || isset($_GET[sub_cata])){
			echo '<a href="new_'.$_GET[cata].'.php?cata='.$_GET[cata].'&sub_cata='.$_GET[sub_cata].'">ADD New Profile</a>';
		}
	 ?>
	<div class="content_status_list"><a href="./data_status.php?cata=<?php echo $_GET[cata]; ?>&orderBy=id">Data Status</a></div>
	
		<div class="content_status_list"><a href="<?php echo $_SERVER['REQUEST_URI']; ?>&sortBy=total_view">Order By Total View</a></div>
		<div class="content_status_list"><a href="<?php echo $_SERVER['REQUEST_URI']; ?>&sortBy=monthly_view">Order By Monthly View</a></div>
				<div class="content_status_list"><a href="<?php echo $_SERVER['REQUEST_URI']; ?>&sortBy=weekly_view">Order By Weekly View</a></div>
		<div class="content_status_list"><a href="<?php echo $_SERVER['REQUEST_URI']; ?>&sortBy=yesterday_view">Order By Yesterday View</a></div>
		<div class="content_status_list"><a href="<?php echo $_SERVER['REQUEST_URI']; ?>&sortBy=daily_view">Order By Daily View</a></div>
<div class = "content">
	<div class="container-fluid">
		  <div class="row">
		    <div class="col-sm-12 area-demo" id="search_result" >

								<?php 

						    		include '../../data_connection.php';



								if(isset($_GET[sortBy])){
									$sortBy = $_GET[sortBy]." DESC";
								}else{
									$sortBy = 'id ASC';
								}

						    		
						    		if(empty($_GET['sub_cata']))
						    		{
						    		
						    			$sql = "SELECT * from general_info where cata= '$_GET[cata]' ORDER BY $sortBy;";
						    		}
						    		else if($_GET['cata'])
						    		{
						    			$sub_cata_tmp = '%'.$_GET[sub_cata].'%';
						    			$sql = "SELECT * from general_info where cata= '$_GET[cata]' AND sub_cata LIKE '$sub_cata_tmp' ORDER BY $sortBy;";
						    		}
						    		else{
						    			$sql = "SELECT * from general_info where cata= '$_GET[cata]' AND sub_cata LIKE '$_GET[sub_cata]' ORDER BY $sortBy;";		
						    		}
						    		
									$data = $conn->query($sql);

									if($data->num_rows>0)
									{
										$i=1;
										while($row = $data-> fetch_assoc())
										{
											$id = $row['id'];
											$name = $row['name'];
											$rating = $row['rating'];
											$location =  $row['location'];
											$catagory =  $row['cata'];
											
											$image = strpos($row['image_link'], ".jpg");

											if(strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false)
											{
													$image   = "1_1no_title_image.jpg";
											}
											else if(empty($row['image_link']))
											{
													$image   = "1_1no_title_image.jpg";
											}
											
											else {
												$image   = $row['image_link'];
											}


											$subcata = strtolower($row['sub_cata']);
											
											$link = strtolower($catagory);

											$social_link = "";
											if(!empty($row[social_links]))
											{
												$social_links = $row[social_links];
												$social_links = str_replace("http", " http", $social_links);
												$social_links = str_replace("  ", " ", $social_links);
												$social_links = str_replace("   ", " ", $social_links);
												$social_links = str_replace("    ", " ", $social_links);
												if(strpos($social_links, ", ")!==false){
													$linkArray = explode(", ", $social_links);
												}
												else if(strpos($social_links, ",")){
													$linkArray = explode(",", $social_links);
												} 	
												else if(strpos($social_links, " ")){
													$linkArray = explode(" ", $social_links);
												}
												else{
													$linkArray  = array($social_links);
												}
												$k=0;
												while($linkArray[$k]){
													if(strpos($linkArray[$k], "facebook") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>facebook</a>";
													}
													else if(strpos($linkArray[$k], "twitter") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>twitter</a>";
													}
													else if(strpos($linkArray[$k], "google") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>google</a>";
													}
													else if(strpos($linkArray[$k], "instagram") !== false){
														$linkArray[$k] = "<a class='social_a_tag' href='".$linkArray[$k]."' target='_blank'>instagram</a>";
													}

													if($k==30)break;
													$k++;

												}
												$social_link = $linkArray[0]."  ".$linkArray[1]."  ".$linkArray[2]." ".$linkArray[3]." ".$linkArray[4]." ".$linkArray[5]." ".$linkArray[6]." ".$linkArray[7]." ".$linkArray[8]." ".$linkArray[9];
											}


											
													
										    		if($row[status] == "not approved"){
														$status = '<div class="app_status1"><button onclick="contentStatus(\'approved\', \''.$id.'\')">Not Approved</button></div>';
													}
													else if($row[status] == "approved"){
														$status = '<div class="app_status2"><button onclick="contentStatus(\'not approved\', \''.$id.'\')">Approved</button></div>';
													}
											    	echo '
											    		<div class="col-sm-3">
												    		<div class="content_box">
																<div class="content_image">
																	<a class="c_link" href="'.$link.'.php?sub_cata='.$subcata.'&cata='.$_GET[cata].'&id='.$id.'"><img src="../../image/title_image/'.$image.'" class="img-responsive "	 alt="IMAGE"></img></a>
																</div>

																<div class="content_data">
																	<h4>'.$row[id].'. '.$row[name].'</h4>
																	<span class="show_view">  '.$row[total_view].'_____ '.$row[monthly_view].'_____'.$row[weekly_view].'_____'.$row[daily_view].'</span> 
																	<span class="" style="font-size:10px; padding-left:5px;">  '.$row[total_view].'____ '.$row[last_month_view].'_____'.$row[last_week_view].'_____'.$row[yesterday_view].'</span> 
																	'.$status.'_
																	<span>'.$row[sub_cata].'</span>
																	<span>'.$social_link.'</span>
																	<span>'.$row[date].'</span>
																	
																</div>
															</div>
														</div>
											    	';
											    	
											    	if($i%4 == 0) echo '<div class="clearfix"> </div>';
											    	$i = $i+1;
											   
										}
										
									}

						    			
						    	?>

			</div>
		  </div>
	</div>
</div>
				
</div>
</div>



</div>
<!-- END OF container -->
</body>
</html>











