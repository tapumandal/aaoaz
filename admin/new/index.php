<?php
error_reporting(0);
 session_start();
  if(!isset($_SESSION[adminId])){
    header("Location: ../admin_login.php");
  }
?>

<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="../../js/jquery.min.js"></script>
  <link href="../../css/style.css" rel="stylesheet" type="text/css" media="all" />	
<title></title>
</head>

<body>

<style type="text/css">
	
</style>

<div class = "container">
	<div class = "main-top">
		<div class="main">
				<?php
				
				include 'header.php';
			if($_SESSION[adminType] == "super admin"){
				
				?>

				<div class="admin_dash col-sm-12">
					<div class="col-sm-3">
						<a href="./?admin_action=admin">Admin</a>
					</div>

					<div class="col-sm-3">
						<a href="./?admin_action=performance">Performance</a>
					</div>

					<div class="col-sm-3">
						<a href="./?admin_action=unpublished_content">Unpublished Content</a>
					</div>

					<div class="col-sm-3">
						<a href="./?admin_action=point_table">Point Table</a>
					</div>

					<div class="col-sm-3">
						<a href="./?admin_action=user_list">User List</a>
					</div>

					<div class="col-sm-3">
						<a href="./?admin_action=comment_list&s=0">User Comment</a>
					</div>

					<div class="col-sm-3">
						<a href="./?admin_action=package_request">Package Request</a>
					</div>


				</div>


				<?php
				
			}
			else if($_SESSION[adminType] == "admin"){
				echo '
					<div class="admin_dash col-sm-12">
						
						<div class="col-sm-4">
							<a href="./?admin_action=comment_list&s=0">User Comment</a>
						</div>	

						<div class="col-sm-3">
							<a href="./?admin_action=performance">Performance</a>
						</div>

						<div class="col-sm-4">
							<a href="./?admin_action=unpublished_content">Unpublished Content</a>
						</div>

						<div class="col-sm-3">
							<a href="./?admin_action=package_request">Package Request</a>
						</div>	
						<div class="col-sm-3">
							<a href="./?admin_action=point_table">Point Table</a>
						</div>

					</div>
				';
			}

			if($_GET['admin_action'] == "admin" && $_SESSION[adminType] == "super admin"){
				include 'dashboard/admin_details.php';
			}
			else if($_GET['admin_action'] == "user_list" && $_SESSION[adminType] == "super admin"){
				include 'dashboard/user_list.php';
			}
			else if($_GET['admin_action'] == "performance"){
				include 'dashboard/performance.php';
			}
			else if($_GET['admin_action'] == "unpublished_content"){
				include 'dashboard/unpublished_content.php';
			}
			else if($_GET['admin_action'] == "comment_list"){
				include 'dashboard/comment_list.php';
			}
			else if($_GET['admin_action'] == "point_table"){
				include 'dashboard/point_table.php';
			}
			else if($_GET['admin_action'] == "package_request"){
				include 'dashboard/package_request.php';
			}
			else{
				// include 'dashboard/unpublished_content.php';
			}

			?>


		</div>
	</div>




</div>
<!-- END OF container -->
</body>
</html>