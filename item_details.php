<?php

session_start();
// include 'operation/url_operation.php';
?>

<!DOCTYPE html>
<head>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106976768-1');
</script>


<?php
// include 'header_tags.php';
include 'top_link_list.php';
?>
</head>


<body>


<div class="page_loader"></div>

<div class = "container single_item_details">





		<div class = "main-top">
			<div class="main">
				<?php

include 'header.php';

?>
			</div>
		</div>

	<?php echo '<div class="back_main_page"><a href="' . $getCata . '.php?content=' . $_GET[content] . '">Go to the Content page</a></div>'; ?>




<?php
function itemDetailsStringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '/', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '.', $string);
	$string = str_replace('(', '', $string);
	$string = str_replace(')', '.', $string);
	return $string;
}

$getCata = itemDetailsStringFilter($_GET[cata]);

if ($getCata == "food") {

	itemFoodOverlay();
} else if ($getCata == "electronics") {
	itemElectronicsOverlay();
} else if ($getCata == "life style") {
	itemLifeStyleOverlay();
} else if ($getCata == "tourism") {
	itemTourismOverlay();
} else if ($getCata == "residence") {
	itemResidenceOverlay();
}

function itemFoodOverlay() {
	include "data_connection.php";
	// include "code_php.php";

	$getCata = itemDetailsStringFilter($_GET[cata]);
	$getItem_id = itemDetailsStringFilter($_GET[item_id]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";
	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$total_rate = ($row[total_rate] / $row[rate_people]) / 3;
			$total_rate = $total_rate * 20;
			$rate_1 = ($row[rate_1] / $row[rate_people]) * 20;
			$rate_2 = ($row[rate_2] / $row[rate_people]) * 20;
			$rate_3 = ($row[rate_3] / $row[rate_people]) * 20;

			$multiImage = fetchMultiImage($getItem_id, $row[image_link]);

			echo '
							<div class="col-sm-12 popup">
								<div class="col-sm-12 popup_head">
									<div class="col-sm-3 img_">
										<div class="single_image" id="single_image">
											<img class="one_img" src="image/item_image/' . $row[image_link] . '">
										</div>

										<div class="multi_image">
										' . $multiImage . '
										</div>

									</div>
									<div class="col-sm-2 name_rate">
										<div class="popup_name">
											<span>' . $row[name] . '</span>
										</div>
										<div class="popup_rate">
												<div class="comment_user_review">

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>Avg Rating</span>
																</div>
																' . rating($total_rate) . '
															</div>


															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>TASTE</span>
																</div>
																' . rating($rate_1) . '
															</div>

															<div class="comment_review_quality">
																<div class="comment_review_title">
																	<span>PRICE</span>
																</div>
																' . rating($rate_2) . '
															</div>

															<!-- style="margin-left:110px" -->
															<div class="comment_review_quality" >
																<div class="comment_review_title">
																	<span>SERVICE</span>
																</div>
																' . rating($rate_3) . '
															</div>
												</div>
										</div>
									</div>
									<!-- popup -->
									<div class="col-sm-7 popup_des ">
										<span>' . $row[description] . '</span>

									</div>
								</div>';
			//giveRate();
			echo '</div>
							<!-- popup -->
							';
		} //overlay item data while
	} //overlay item data if

	$fetch_item_des = "SELECT * FROM `item_des` WHERE item_id = '$getItem_id';";

	$data = $conn->query($fetch_item_des);
	$left_right_pos = 1;

	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			if ($left_right_pos % 2 == 1) {
				echo '
						<div class="col-sm-12">
							<div class="product_details">
								<div class="product_details_1">
									<div class="product_details_left product_img">
										<img class="img-responsive " src="image/item_des/' . $row[image_link] . '">
										<div class="product_img_name_left"><span>' . $row[image_name] . '</span></div>
									</div>
									<div class="product_details_left_des">
										<span>' . $row[image_des] . '</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						';
				$left_right_pos++;
			} else if ($left_right_pos % 2 == 0) {
				echo '
						<div class="col-sm-12">
							<div class="product_details">
								<div class="product_details_1">
									<div class="product_details_right product_img">
										<img class="img-responsive" src="image/item_des/' . $row[image_link] . '">
										<div class="product_img_name_right"><span>' . $row[image_name] . '</span></div>
									</div>
									<div class="product_details_right_des">
										<span>' . $row[image_des] . '</span>
									</div>

								</div>
							</div>
						</div>

						';
				$left_right_pos++;
			}
		}
	}
	echo '<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
					<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->

		   				<div class="com">';

	$getSub_cata = itemDetailsStringFilter($_GET[sub_cata]);
	$getId = itemDetailsStringFilter($_GET[id]);

	$general_obj = new general();
	$comment_id = null;
	$general_obj->fetch_comment($getCata, $getSub_cata, $getId, $comment_id, $getItem_id);

	echo '		</div>
		   			</div>   <!-- COMMENT  -->
				</div>   <!-- COMMENT AND QUESTIOIN -->
				';

} //food_overlay

function itemElectronicsOverlay() {

	include "data_connection.php";
	// include "code_php.php";

	$getItem_id = itemDetailsStringFilter($_GET[item_id]);

	$fetch_item_data = "SELECT * FROM `elec_item_details` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$total_rate = ($row[total_rate] / $row[rate_people]) / 3;
			$total_rate = $total_rate * 20;
			$rate_1 = ($row[rate_1] / $row[rate_people]) * 20;
			$rate_2 = ($row[rate_2] / $row[rate_people]) * 20;
			$rate_3 = ($row[rate_3] / $row[rate_people]) * 20;

			$k = 0;
			$fetch_item_des = "SELECT * FROM `item_multi_image` WHERE item_id = '$getItem_id' AND cata='$getCata';";
			$data2 = $conn->query($fetch_item_des);
			if ($data2->num_rows > 0) {
				while ($row2 = $data2->fetch_assoc()) {
					echo "===";
					if ($k % 2 == 0) {
						$imgDesData = $imgDesData . '
										<div class="product_details_1">
											<div class="product_details_left">
												<img class="img-responsive" src="image/item_image/item_multi_image/' . $row2[image_link] . '">
											</div>
											<div class="product_details_right">
												<span>' . $row2[short_note] . '</span>
											</div>
										</div>
										<div class="clearfix"></div>'
						;
					} else {
						$imgDesData = $imgDesData . '
										<div class="product_details_2">
											<div class="product_details_left">
												<span>' . $row2[short_note] . '</span>
											</div>
											<div class="product_details_right">
												<img class="img-responsive" src="image/item_image/item_multi_image/' . $row2[image_link] . '">
											</div>
										</div>
										<div class="clearfix"></div>
										';
					}
					$k++;

				}
			}
			echo '
						<div class=" popup comment">
							<div class="popup_head">
								<div class="img_">
									<img src="image/item_image/' . $row[image_link] . '">
								</div>
								<div class="popup_name">
									<span>' . $row[name] . '</span>
								</div>
								<div class="popup_rate">
										<div class="comment_user_review">

																			<div class="comment_review_quality">
																				<div class="comment_review_title">
																					<span>Avg Rating</span>
																				</div>
																				' . rating($total_rate) . '
																			</div>


																			<div class="comment_review_quality">
																				<div class="comment_review_title">
																					<span>QUALITY</span>
																				</div>
																				' . rating($rate_1) . '
																			</div>

																			<div class="comment_review_quality">
																				<div class="comment_review_title">
																					<span>PRICE</span>
																				</div>
																				' . rating($rate_2) . '
																			</div>

																			<!-- style="margin-left:110px" -->
																			<div class="comment_review_quality" >
																				<div class="comment_review_title">
																					<span>SERVICE</span>
																				</div>
																				' . rating($rate_3) . '
																			</div>
										</div>
								</div>
								<!-- popup_rate -->

								<div class="popup_des">

									<span><b>Price:</b> ' . $row[price] . '</span><br>
									<span><b>Short Description:</b> ' . $row[description] . '</span><br>
								</div>
							</div>
							<div class="clearfix"></div>

							<!-- details info table -->
							<div class="col-sm-12 elec_details">
								<div class="col-sm-6 pro_table">
										<table>
										      <thead>
										        <tr>
										          <th class="spe_1">Specification</th>
										          <th class="spe_2">Details</th>
										        </tr>
										      </thead>
										      <tbody>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Sim</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_1] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">ANT</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_2] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">USB</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_3] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Ear Jack</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_4] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">MHL</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_5] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Wi-Fi</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_6] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Bluetooth</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_7] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Operating System</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_8] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Display</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_9] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Resolution and Color</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_10] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">S Pen</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_11] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Processor</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_12] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">RAM</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_13] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Rear Camera</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_14] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Front Camera</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_15] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Camera Flash</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_16] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Sensor</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_17] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Dimention</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_18] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Battery</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_19] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Internet Usage Time</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_20] . '</td>
										        </tr>

										        <tr>
										          <td class="spe_1 spe_line1 item-name">Talk Time</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_21] . '</td>
										        </tr>

										        <tr>
										          <td class="spe_1 spe_line2 item-name">Batery Removale</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_22] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line1 item-name">Video Playing Format</td>
										          <td class="spe_2 spe_line1 item-name">' . $row[column_23] . '</td>
										        </tr>
										        <tr>
										          <td class="spe_1 spe_line2 item-name">Audio Playing Formate</td>
										          <td class="spe_2 spe_line2 item-name">' . $row[column_24] . '</td>
										        </tr>



										      </tbody>
										</table>
								</div>

									<div class="col-sm-6">
										<div class="product_details">



										' . $imgDesData . '
										</div>
									</div>

							</div>
							<!-- details info table -->

						</div>
						<!-- popup -->
					';
		}
	}
}
// electronics_overlay function

function itemLifeStyleOverlay() {
	include "data_connection.php";
	// include "code_php.php";

	$getItem_id = itemDetailsStringFilter($_GET[item_id]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			echo '
							<div class=" popup comment">
								<div class="popup_head">
									<div class="img_">
										<img src="image/item_image/' . $row[image_link] . '">
									</div>
									<div class="popup_name">
										<span>' . $row[name] . '</span>
									</div>

									<div class="">
										<span>Price: ' . $row[price] . '</span>
									</div>

									<div class="">
										<span>Color: ' . $row[color] . '</span>
									</div>

									<div class="">
										<span>Size: ' . $row[size] . '</span>
									</div>

									<!-- popup -->
									<div class="popup_des">
										<span>' . $row[description] . '</span>

									</div>
								</div>
							</div>
							<!-- popup -->
							';
		} //overlay item data while
	} //overlay item data if
}

function itemTourismOverlay() {
	include "data_connection.php";
	// include "code_php.php";
	$getItem_id = itemDetailsStringFilter($_GET[item_id]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			echo '
							<div class=" popup comment">
							<div class="popup_head">
								<div class="img_">
									<img src="image/item_image/' . $row[image_link] . '">
								</div>
								<div class="popup_name">
									<span>' . $row[name] . '</span>
								</div>

								<div class="">
									<span>Price: ' . $row[price] . '</span>
								</div>

								<div class="">
									<span>Color: ' . $row[color] . '</span>
								</div>

								<div class="">
									<span>Size: ' . $row[size] . '</span>
								</div>

								<!-- popup -->
								<div class="popup_des">
									<span>' . $row[description] . '</span>

								</div>
							</div>
							</div>
							<!-- popup -->
							';
		} //overlay item data while
	} //overlay item data if
}

function itemResidenceOverlay() {
	// echo "string";
	include "data_connection.php";
	// include "code_php.php";
	$getItem_id = itemDetailsStringFilter($_GET[item_id]);

	$fetch_item_data = "SELECT * FROM `item_list` WHERE id = '$getItem_id';";

	$data = $conn->query($fetch_item_data);
	//$item_number=1;
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			echo '
							<div class=" popup comment">
							<div class="popup_head">
								<div class="img_">
									<img src="image/item_image/' . $row[image_link] . '">
								</div>
								<div class="popup_name">
									<span>' . $row[name] . '</span>
								</div>

								<div class="">
									<span>Price: ' . $row[price] . '</span>
								</div>

								<div class="">
									<span>Color: ' . $row[color] . '</span>
								</div>

								<div class="">
									<span>Size: ' . $row[size] . '</span>
								</div>

								<!-- popup -->
								<div class="popup_des">
									<span>' . $row[description] . '</span>

								</div>
							</div>
							</div>
							<!-- popup -->
							';
		} //overlay item data while
	} //overlay item data if
}

?>
</div>
</div>
<?php
include 'footer.php';
?>
</body>
</html>