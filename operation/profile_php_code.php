<?php
//error_reporting(0);
session_start();
include '../data_connection.php';

function profilePhpStringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '/', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '.', $string);
	$string = strip_tags($string);
	return $string;
}

function profilePhpStringFilter2($string) {

	$string = strip_tags($string);
	$string = htmlspecialchars($string);
	$string = str_replace("'", "\'", $string);
	$string = str_replace("\\\\\'", "\'", $string);
	$string = str_replace("\\\\\\\'", "\'", $string);
	$string = str_replace("\\\\\\\\\'", "\'", $string);
	$string = str_replace("\\\\\\\\\\\'", "\'", $string);
	return $string;
}

function stringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('SELECT', '', $string);
	$string = str_replace('UPDATE', '', $string);
	$string = str_replace('DROP', '', $string);
	$string = str_replace('INSERT', '', $string);
	$string = str_replace('DELETE', '', $string);
	$string = str_replace('FROM', '', $string);
	$string = str_replace('WHERE', '', $string);

	$string = str_replace('select', '', $string);
	$string = str_replace('update', '', $string);
	$string = str_replace('drop', '', $string);
	$string = str_replace('insert', '', $string);
	$string = str_replace('delete', '', $string);
	$string = str_replace('from', '', $string);
	$string = str_replace('where', '', $string);

	return $string;
}

function encryptPassword($string) {
	$enPass = md5($string);
	$enTmp1 = "";
	$enTmp2 = "";
	$enTmp3 = "";
	$enTmp4 = "";
	$enTmp5 = "";

	$i = 0;
	for ($i = 0; $i < strlen($enPass); $i++) {
		if ($i < 4) {
			$enTmp1 = $enTmp1 . $enPass[$i];
		} else if ($i < 8) {
			$enTmp2 = $enTmp2 . $enPass[$i];
		} else if ($i < 12) {
			$enTmp3 = $enTmp3 . $enPass[$i];
		} else if ($i < 16) {
			$enTmp4 = $enTmp4 . $enPass[$i];
		} else {
			$enTmp5 = $enTmp5 . $enPass[$i];
		}
	}

	$encrypted = $enTmp1 . $enTmp4 . $enTmp2 . $enTmp3 . $enTmp5;

	return $encrypted;
}

$postAction = profilePhpStringFilter($_POST[action]);
$postCommentId = profilePhpStringFilter($_POST[commentId]);
$getDataType = profilePhpStringFilter($_GET[dataType]);
$getRange = profilePhpStringFilter($_GET[range]);
$getUserId = profilePhpStringFilter($_GET[userId]);

$postUser_id = profilePhpStringFilter($_POST[user_id]);
$postPoint_id = profilePhpStringFilter($_POST[point_id]);
$postPoints = profilePhpStringFilter($_POST[points]);
$postMessage = profilePhpStringFilter2($_POST[message]);

$postChangeType = profilePhpStringFilter($_POST[changeType]);
$postChangeValue = profilePhpStringFilter($_POST[changeValue]);

if ($postAction == "changePersonalInfo" && isset($_SESSION[userid])) {

	if ($postChangeType == "password") {

		$oldPass = stringFilter($_POST[oldP]);
		$newPass = stringFilter($_POST[newP]);
		$conPass = stringFilter($_POST[conP]);

		$oldEncryptPass = encryptPassword($oldPass);

		if ($newPass == $conPass) {

			$newEncryptPass = encryptPassword($newPass);

			$getPass = "SELECT `pass` FROM `user` WHERE id = '$_SESSION[userid]'";

			$res = $conn->query($getPass);
			if ($res->num_rows > 0) {
				while ($row = $res->fetch_assoc()) {
					if ($row[pass] == $oldEncryptPass) {

					} else {
						echo "Old Password didn't Match";
					}
				}
			}

			$updateInfo = "UPDATE `user` SET pass = '$newEncryptPass' WHERE `id` = '$_SESSION[userid]' AND pass = '$oldEncryptPass'";

			if ($conn->query($updateInfo)) {
				echo "Password Change >" . $conn->insert_id . "   ";
			} else {
				echo "Old Password is not correct";
			}
		} else {
			echo "Confirm Password didn't match" . $newPass . " > " . $conP;
		}
	} else {
		$updateInfo = "UPDATE `user` SET $postChangeType = '$postChangeValue' WHERE `id` = '$_SESSION[userid]' ";

		if ($conn->query($updateInfo)) {
			// echo "";
		}
	}
}

if ($postUser_id != "" && $postPoint_id != "" && $_SESSION[userid] == $postUser_id) {

	$getPointById = "SELECT `points` FROM `point_table` WHERE id = '$postPoint_id' AND `points` = '$postPoints'";
	$pointRes = $conn->query($getPointById);
	if ($pointRes->num_rows > 0) {
		while ($row2 = $pointRes->fetch_assoc()) {

			$getUserPoint = "SELECT `total_point`, `used_point` FROM `user` WHERE id = '$postUser_id'";
			$res = $conn->query($getUserPoint);
			if ($res->num_rows > 0) {
				while ($row = $res->fetch_assoc()) {
					$pointLeft = $row[total_point] - $row[used_point] - $postPoints;

					if ($pointLeft >= 0) {

						$insertPackage = "INSERT INTO `package`(`user_id`, `point_table_id`, `message`, `date`, `status`) VALUES ('$postUser_id', '$postPoint_id', '$postMessage', now(), 'waiting')";
						if ($conn->query($insertPackage)) {

							$userPoint = "UPDATE `user` SET `used_point` = used_point+'$row2[points]' WHERE id = '$postUser_id'";
							if ($conn->query($userPoint)) {
								// echo $postPoints." points added as used.     ";
							}
							echo "<div class='point_submit_warrning'> <span><b>CONGRATULATION!</b>You have successfully place your reward request. <br> Aaoaz USER Support will contact you soon. <br> Thank you for being a part of aaoaz. </span> </div>";
						} else {
							echo "<div class='point_submit_warrning'> <span><br>Your package request is not place successfully.<br> Please write a email to us. To write a email <a href='http://aaoaz.com/contact.php'>click here</a>. </span> </div>";
						}
					} else {
						echo "<div class='point_submit_warrning'> <span>You do not have sufficient point to get this package.<br>If you think there is any problem <br> Please write a email to us using contact option bottom of <a href='http://aaoaz.com'>aaoaz.com</a> </span> </div>";
					}

				}
			} else {
				echo "<div class='point_submit_warrning'> <span>You do not have sufficient point to get this package.<br>If you think there is any problem <br> Please write a email to us using contact option bottom of <a href='http://aaoaz.com'>aaoaz.com</a> </span> </div>";
			}

		}
	} else {
		echo "<div class='point_submit_warrning'> <span>There is a problem at your request. If you think everything is fine. <br> Please write a email to us using contact option bottom of <a href='http://aaoaz.com'>aaoaz.com</a> </span> </div>";
	}

	// echo '
	// 	<script type="text/javascript">
	// 		setTimeout(function(){
	// 			history.back();
	// 		}, 5000);

	// 	</script>
	// ';
	exit();
}

if (isset($_SESSION[pro_image_type]) && strlen($_SESSION[pro_image_type]) > 2 && $_SESSION[pro_image_name]) {

	$file = '../admin/upload_crop/upload_pic/' . $_SESSION[pro_image_name] . $_SESSION[pro_image_type];

	$data = file_get_contents($file);

	// New file
	//$new = 'C:\wamp\www\reelstubs\app\webroot\img\movies\newimage.jpg';
	$name = $_SESSION[userid] . "_" . $_SESSION[userName] . $_SESSION[pro_image_type];
	$new = '../image/user_image/' . $name;

	// Write the contents back to a new file
	// file_put_contents($new, $data);

	if (copy($file, $new)) {
		$sql = "UPDATE `user` SET `image_link`='$name' WHERE  id='$_SESSION[userid]'";

		if ($conn->query($sql) === TRUE) {
			$last_id = $conn->last_id;

		} else {
			// echo "<h3>Error: " . $sql . "<br>" . $conn->error."</h3>";
		}
	} else {
		// echo "Image Not Copied";
	}
	unset($_SESSION[pro_image_name]);
	unset($_SESSION[pro_image_type]);

	echo "<script type='text/javascript'>window.parent.location.reload()</script>";
	// echo '<script>window.location.replace("./");</script>';
	// echo "<div class='image_upload_status'><h2>Image successfully Uploaded</h2></div>";
}

if ($postAction == "commdentImgShow") {
	getCommentImg($postCommentId);
}

function getCommentImg($commentId) {
	include 'data_connection.php';

	$imgStore = "";
	$getImg = "SELECT * FROM `comment_image` WHERE `comment_id` = '$commentId' LIMIT 3";

	$data = $conn->query($getImg);

	if ($data->num_rows > 0) {
		$imgStore = '';
		while ($row = $data->fetch_assoc()) {
			$imgStore = $imgStore . '<div> <img class="img-responsive" src="image/comment_img/' . $row[image_link] . '"></div>';
		}
	}
	echo $imgStore;
}

if ($getDataType == "bookmark") {
	if ($getRange < 1) {
		$fetch_book_data = "SELECT  bookmark.user_id, bookmark.business_pro_id, bookmark.item_id, bookmark.date, general_info.id, general_info.name, general_info.cata, general_info.sub_cata, general_info.type
						FROM bookmark LEFT JOIN general_info ON bookmark.business_pro_id=general_info.id WHERE bookmark.user_id='$getUserId' ORDER BY bookmark.date DESC LIMIT 10;";
	} else {
		$fetch_book_data = "SELECT  bookmark.user_id, bookmark.business_pro_id, bookmark.item_id, bookmark.date, general_info.id, general_info.name, general_info.cata, general_info.sub_cata, general_info.type
						FROM bookmark LEFT JOIN general_info ON bookmark.business_pro_id=general_info.id WHERE bookmark.user_id='$getUserId' AND bookmark.id < '$getRange' ORDER BY bookmark.date DESC LIMIT 10;";
	}
	$data = $conn->query($fetch_book_data);
	if ($data->num_rows > 0) {
		echo '<div class="bookmark_left">';
		while ($row = $data->fetch_assoc()) {
			if ($row[item_id] == null) {

				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

				$link_name = $row[name];
				$link_name = str_replace(" ", "-", $link_name);

				echo '<div class="user_comment">
								    		<div class="comment_head" >
								    			<a href="' . $subcata_url[0] . '/' . strtolower($link_name) . '"><span class="bus_pro">' . $row[name] . '</span><br></a>
								    			<span class="bus_cata"><a href="specific.php?cata=' . $row[cata] . '">' . $row[cata] . '</a> > <a href="specific.php?cata=' . $row[cata] . '&sub_cata=' . $row[sub_cata] . '">' . $row[sub_cata] . '</a></span>
								    		</div>
								    		<div class="bus_type"><span>Type: ' . $row[type] . ' </span></div>
								    		<span class="bus_date">' . $row[date] . '</span>
								    	</div>
									';

				$lastRowId = $row[id];
			}
		}
		echo ' </div>';
		echo "	<div class='user_pro_list_btn'>
									<button onclick=\"userData('bookmark', '" . $_SESSION[userid] . "', '" . ($lastRowId + 20) . "');\">PREVIOUS</button>
									<button onclick=\"userData('bookmark', '" . $_SESSION[userid] . "', '" . $lastRowId . "');\">NEXT</button>
									</div>
								";
	}

	if ($getRange < 1) {
		$fetch_book_data = "SELECT  bookmark.user_id, bookmark.business_pro_id, bookmark.item_id, bookmark.date, general_info.name, general_info.cata, general_info.sub_cata
						FROM bookmark LEFT JOIN general_info ON bookmark.business_pro_id=general_info.id WHERE bookmark.user_id='$getUserId' ORDER BY bookmark.date DESC LIMIT 10 ;";
	} else {
		$fetch_book_data = "SELECT  bookmark.user_id, bookmark.business_pro_id, bookmark.item_id, bookmark.date, general_info.name, general_info.cata, general_info.sub_cata
						FROM bookmark LEFT JOIN general_info ON bookmark.business_pro_id=general_info.id WHERE bookmark.user_id='$getUserId' AND bookmark.id < '$getRange' ORDER BY bookmark.date DESC LIMIT 10 ;";
	}
	$data = $conn->query($fetch_book_data);
	if ($data->num_rows > 0) {
		echo '<div class="bookmark_right">';
		while ($row = $data->fetch_assoc()) {
			if ($row[item_id] != null) {

				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

				$link_name = $row[name];
				$link_name = str_replace(" ", "-", $link_name);

				echo '<div class="user_comment">
								    		<div class="comment_head" >
								    			<a href="' . $subcata_url[0] . '/' . strtolower($link_name) . '"><span class="bus_pro">' . $row[name] . '</span><br></a>
								    			<span class="bus_cata"><a href="specific.php?cata=' . $row[cata] . '">' . $row[cata] . '</a> > <a href="specific.php?cata=' . $row[cata] . '&sub_cata=' . $row[sub_cata] . '">' . $row[sub_cata] . '</a></span>
								    			<span class="bus_date">Date</span>
								    		</div>
								    		<div class="bus_type"><span>Business Profile</span></div>
								    	</div>';
			}

			$lastRowId = $row[id];
		}
		echo ' </div>';

	}

} else if ($getDataType == "comment") {

	if ($getRange < 1) {
		$fetch_comment_data = "SELECT comment.id, comment.user_id, comment.cata, comment.sub_cata, comment.user_type, comment.text,
					comment.type,  comment.time, comment.date, comment.points, general_info.name FROM comment
					LEFT JOIN general_info ON general_info.id=comment.cata_id
					 WHERE comment.user_id='$getUserId' AND comment.type ='comment' ORDER BY comment.date DESC, comment.time DESC LIMIT 10 ;";
	} else {
		$fetch_comment_data = "SELECT comment.id, comment.user_id, comment.cata, comment.sub_cata, comment.user_type, comment.text,
					comment.type,  comment.time, comment.date, comment.points, general_info.name FROM comment
					LEFT JOIN general_info ON general_info.id=comment.cata_id
					 WHERE comment.user_id='$getUserId' AND comment.type ='comment' AND comment.id < '$getRange' ORDER BY comment.date DESC, comment.time DESC LIMIT 10 ;";
	}

	$data = $conn->query($fetch_comment_data);
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {

			$subcata_url = explode(" ", $row[sub_cata]);
			$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

			$link_name = $row[name];
			$link_name = str_replace(" ", "-", $link_name);

			echo '<div class="user_comment col-sm-12">
		    		<div class="comment_head col-sm-4">
		    			<a href="' . $subcata_url[0] . '/' . strtolower($link_name) . '"><span class="bus_pro">' . $row[name] . '</span><br></a>
		    			<span class="bus_cata"><a href="specific.php?cata=' . $row[cata] . '">' . $row[cata] . '</a> > <a href="specific.php?cata=' . $row[cata] . '&sub_cata=' . $row[sub_cata] . '">' . $row[sub_cata] . '</a></span>
		    			<br>
		    			<span class="bus_date">' . $row[date] . '</span>
		    		</div>
		    		<div class="comment_body col-sm-6">
		    			<span>' . $row[text] . '</span>
		    		</div>

		    		<div class="user_point col-sm-2">
						<span><b>Points: </b>' . $row[points] . '</span>
					</div>

					<div class="comment_img col-sm-12">
						' . getCommentImgbtn($row[id]) . '
					</div>
		    	</div>
		    	';

			$lastRowId = $row[id];
		}

		echo "	<div class='user_pro_list_btn'>
				<button onclick=\"userData('comment', '" . $_SESSION[userid] . "', '" . ($lastRowId + 20) . "');\">PREVIOUS</button>
				<button onclick=\"userData('comment', '" . $_SESSION[userid] . "', '" . $lastRowId . "');\">NEXT</button>
				</div>
			";
	}

} else if ($getDataType == "question") {

	if ($getRange < 1) {
		$fetch_comment_data = "SELECT comment.user_id, comment.cata, comment.sub_cata, comment.user_type, comment.text,
					comment.type,  comment.time, comment.date, general_info.name FROM comment
					LEFT JOIN general_info ON general_info.id=comment.cata_id
					 WHERE comment.user_id='$getUserId' AND comment.type ='question' ORDER BY comment.date DESC, comment.time DESC LIMIT 10 ;";
	} else {
		$fetch_comment_data = "SELECT comment.user_id, comment.cata, comment.sub_cata, comment.user_type, comment.text,
					comment.type,  comment.time, comment.date, general_info.name FROM comment
					LEFT JOIN general_info ON general_info.id=comment.cata_id
					 WHERE comment.user_id='$getUserId' AND comment.type ='question' AND comment.id < '$getRange' ORDER BY comment.date DESC, comment.time DESC LIMIT 10 ;";
	}
	$data = $conn->query($fetch_comment_data);
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			echo '<div class="user_comment">
		    		<div class="comment_head" >
		    			<span class="bus_pro">' . $row[name] . '</span>
		    			<span class="bus_cata">' . $row[cata] . ' > ' . $row[sub_cata] . '</span>
		    			<span class="bus_date">' . $row[date] . '</span>
		    		</div>
		    		<div class="comment_body">
		    			<span>' . $row[text] . '</span>
		    		</div>
		    	</div>
		    	';

			$lastRowId = $row[id];
		}
		echo "	<div class='user_pro_list_btn'>
				<button onclick=\"userData('question', '" . $_SESSION[userid] . "', '" . ($lastRowId + 20) . "');\">PREVIOUS</button>
				<button onclick=\"userData('question', '" . $_SESSION[userid] . "', '" . $lastRowId . "');\">NEXT</button>
				</div>
			";
	}
} else if ($getDataType == "review") {

	if ($getRange < 1) {
		$fetch_comment_data = "SELECT comment.id, comment.user_id, comment.item_id, comment.cata, comment.sub_cata, comment.user_type, comment.text,
					comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, general_info.name FROM comment
					LEFT JOIN general_info ON general_info.id=comment.cata_id
					 WHERE comment.user_id='$getUserId' AND comment.type ='rate' ORDER BY comment.date DESC, comment.time DESC LIMIT 10 ;";
	} else {
		$fetch_comment_data = "SELECT comment.id, comment.user_id, comment.item_id, comment.cata, comment.sub_cata, comment.user_type, comment.text,
					comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, general_info.name FROM comment
					LEFT JOIN general_info ON general_info.id=comment.cata_id
					 WHERE comment.user_id='$getUserId' AND comment.type ='rate' AND comment.id < '$getRange' ORDER BY comment.date DESC, comment.time DESC LIMIT 10 ;";
	}
	$data = $conn->query($fetch_comment_data);
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$rate_1 = $row[rate_1] * 20;
			$rate_2 = $row[rate_2] * 20;
			$rate_3 = $row[rate_3] * 20;
			$rev_txt = "";
			if ($row[text] != 'undefined') {$rev_txt = $row[text];}

			$getCommentItem = "SELECT * FROM `item_list` WHERE `id` = '$row[item_id]'";
			$resItemId = $conn->query($getCommentItem);

			if ($resItemId->num_rows > 0) {
				while ($res2 = $resItemId->fetch_assoc()) {
					$tmp = $res2[id];
					$itemIndex = array_search($tmp, $_SESSION[food_item_detect]);

					$comment_item = '
											<div class="comment_item">
												<div class="comment_item_img">
													<img class="img-responsive" src="image/item_image/s_' . $res2[image_link] . '">
												</div>

												<div class="comment_item_name">
													<a class="show-popup food_list_see" href="item_details.php?cata=' . $res2[cata] . '&sub_cata=' . $res2[sub_cata] . '&id=' . $res2[id] . '&item_id=' . $res2[id] . '&content=' . $row[name] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $res2[id] . ')">' . $res2[name] . '</a>
												</div>
											</div>
										';
				}
			}

			if ($row[type] == "rate") {

				if ($row[cata] == "food") {
					$rateTitle1 = "TASTE";
					$rateTitle2 = "PRICE";
					$rateTitle3 = "SERVICE";
				} else if ($row[cata] == "life style") {
					if ($row[sub_cata] == "fashion" || $row[sub_cata] == "accessorice") {
						$rateTitle1 = "QUALITY";
						$rateTitle2 = "SERVICE";
						$rateTitle3 = "PRICE";
					} else if ($row[sub_cata] == "parlor_salon" || $row[sub_cata] == "gym_fitness") {
						$rateTitle1 = "ENVIRONMENT";
						$rateTitle2 = "SERVICE";
						$rateTitle3 = "EXPENSE";
					}
				} else if ($row[cata] == "education") {
					$rateTitle1 = "FACULTY";
					$rateTitle2 = "FACILITIE";
					$rateTitle3 = "EXPENSE";
				} else if ($row[cata] == "health") {
					if ($row[sub_cata] == "hospital_clinic") {
						$rateTitle1 = "DOCTOR";
						$rateTitle2 = "FACILITIE";
						$rateTitle3 = "EXPENSE";
					}
				} else if ($row[cata] == "education") {
					if ($row[sub_cata] == "university" || $row[sub_cata] == "college" || $row[sub_cata] == "coaching") {
						$rateTitle1 = "FACULTY";
						$rateTitle2 = "FACILITIES";
						$rateTitle3 = "EXPENSE";
					}
				} else if ($row[cata] == "electronics") {
					$rateTitle1 = "DESIGN";
					$rateTitle2 = "PERFORMANCE";
					$rateTitle3 = "BATTERY LIFE";
				} else if ($row[cata] == "residence") {
					if ($row[sub_cata] != "to-let") {
						$rateTitle1 = "SERVICE";
						$rateTitle2 = "FACILITIE";
						$rateTitle3 = "EXPENSE";
					}
				} else if ($row[cata] == "ecommerce") {
					if ($row[sub_cata] != "buy_sell" || $row[sub_cata] != "others" || $row[sub_cata] != "job") {

						$rateTitle1 = "PRICE";
						$rateTitle2 = "SERVICE";
						$rateTitle3 = "RESPONSE";
					} else {
						$rateTitle1 = "SERVICE";
						$rateTitle2 = "PRICE";
						$rateTitle3 = "RESPONSE";
					}
				}

				$rate_1 = $row[rate_1] * 20;
				$rate_2 = $row[rate_2] * 20;
				$rate_3 = $row[rate_3] * 20;

				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

				$link_name = $row[name];
				$link_name = str_replace(" ", "-", $link_name);

				echo '
								<div class="only_com col-sm-12 profile_rating">

									<div class="col-sm-6">

											<div class="pro_user_review_txt col-sm-6">
									    		<div class="comment_head" >
									    			<a href="' . $subcata_url[0] . '/' . strtolower($link_name) . '"><span class="bus_pro">' . $row[name] . '</span><br></a>
									    			<span class="bus_cata"><a href="specific.php?cata=' . $row[cata] . '">' . $row[cata] . '</a> > <a href="specific.php?cata=' . $row[cata] . '&sub_cata=' . $row[sub_cata] . '">' . $row[sub_cata] . '</a></span>
									    			<br><span class="bus_date">' . $row[date] . '</span>
									    		</div>
									    	</div>


											<div class="col-sm-6">
												' . $comment_item . '
												<div class="opinion rate_opinion">
													<span class="op"><b>Review: </b> <i>' . $row[text] . '</i></span>
												</div>
											</div>
									</div>










									<div class="comment_user_review col-sm-4">
										<div class="comment_review_quality">

											<div class="comment_review_title">
												<span>' . $rateTitle1 . '</span>
											</div>
											<div class="comment_review_rate">
												<ul class="star-rating star_not_move">
													<li class="current-rating" id="current-rating" style="width:' . $rate_1 . '%"><!-- will show current rating --></li>
													<span id="ratelinks">
														<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
														<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
														<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
														<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
														<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
													</span>
												</ul>


											</div>
										</div>

										<div class="comment_review_quality">
											<div class="comment_review_title">
												<span>' . $rateTitle2 . '</span>
											</div>
											<div class="comment_review_rate">

												<ul class="star-rating">
													<li class="current-rating" id="current-rating" style="width:' . $rate_2 . '%"><!-- will show current rating --></li>
													<span id="ratelinks">
														<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
														<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
														<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
														<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
														<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
													</span>
												</ul>

											</div>
										</div>

										<!-- style="margin-left:110px" -->
										<div class="comment_review_quality" >
											<div class="comment_review_title">
												<span>' . $rateTitle3 . '</span>
											</div>
											<div class="comment_review_rate">

												<ul class="star-rating">
													<li class="current-rating" id="current-rating" style="width:' . $rate_3 . '%"><!-- will show current rating --></li>
													<span id="ratelinks">
														<li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
														<li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
														<li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
														<li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
														<li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
													</span>
												</ul>

											</div>
										</div>
									</div>






									<div class="user_point col-sm-2">
										<span><b>Points: </b>' . $row[points] . '</span>
									</div>

									<div class="comment_img col-sm-12">
										' . getCommentImgbtn($row[id]) . '
									</div>

								</div>
								';
				$lastRowId = $row[id];
			} // else if of rate
		}
		echo "	<div class='user_pro_list_btn'>
				<button onclick=\"userData('review', '" . $_SESSION[userid] . "', '" . ($lastRowId + 20) . "');\">PREVIOUS</button>
				<button onclick=\"userData('review', '" . $_SESSION[userid] . "', '" . $lastRowId . "');\">NEXT</button>
				</div>
			";
	}
} else if ($getDataType == "bookmarkInsert") {

	$getProductID = profilePhpStringFilter($_GET[productID]);
	$getId = profilePhpStringFilter($_GET[id]);

	if ($getProductID == "") {
		$fetchData = "SELECT * FROM bookmark WHERE user_id = '$getUserId';";
		$fetchData = $conn->query($fetchData);
		$access = 1;
		if ($fetchData->num_rows > 0) {
			while ($row = $fetchData->fetch_assoc()) {
				if ($row[business_pro_id] == $getId) {
					$access = 0;
					echo "You Already Bookmarked it.";
				}
			}
		}
		if ($access == 1) {

			$keepData = "INSERT INTO `bookmark`(`user_id`, `business_pro_id`, `date`)
							VALUES('$getUserId', '$getId', now());";
			if ($conn->query($keepData) === TRUE) {
				echo " New Business Profile Bookmarked";
			} else {
				echo "Something is WRONG Business Profile is not Bookmarked";
			}
		}
	} else {
		echo "product Id set";
	}
} else if ($getDataType == "points") {

	$getPointData = "SELECT * FROM `point_table` WHERE `status` = 'show' AND `action` = 'active' ORDER BY points ASC";

	$data = $conn->query($getPointData);

	if ($data->num_rows > 0) {

		$j = 1;
		echo '
				<div class="reward_panel ">
					<div class="col-sm-12">
			';
		while ($row = $data->fetch_assoc()) {

			echo '
					<div class="reward_list col-sm-12">
					 	<div class="col-sm-5 reward_list_img">
					 		<img class="img-responsive" src="../image/reward_image/' . $row[img] . '">
					 	</div>

					 	<div class="col-sm-7 reward_list_data">
							<h3 class=" package_list_name">' . $j . ' . ' . $row[pakage_name] . '</h3>
							<span class="package_list_points"> Required Points: ' . $row[points] . '</span>

							<span class="package_list_des">' . $row[description] . '</span>
							<span class="action_status">' . $action . '</span>
							<textarea class="col-sm-3 package_list_send_massage" id="pointMessage" type="text"  name="message" placeholder="Type your message"></textarea>
					 		<button class="col-sm-1 package_list_btn"    onclick="postPointRequest(' . $row[id] . ', ' . $_SESSION[userid] . ', ' . $row[points] . ')">ACCEPT</button>
					 	</div>
					</div>
					<div class="clearfix"> </div>
				';

			$j++;
		}
		echo '
					</div>
				</div>
			';
	}

} else if ($getDataType == "requested_package") {

	$insertPointdata = "SELECT point_table.pakage_name, point_table.img, point_table.description, point_table.points,
									package.message, package.admin_message, package.status, package.date
		FROM `package` LEFT JOIN `point_table` ON package.point_table_id = point_table.id WHERE `user_id` = '$getUserId' ORDER BY package.id ASC";

	$data = $conn->query($insertPointdata);

	if ($data->num_rows > 0) {

		// echo '

		//     <div class="user_point_table_list">
		// 		<span class="col-sm-1">'.$j.'</span>
		// 		<span class="col-sm-1">'.$row[pakage_name].'</span>
		// 		<span class="col-sm-3">'.$row[description].'</span>
		// 		<span class="col-sm-1">'.$row[points].'</span>
		// 		<span class="col-sm-2">'.$row[message].'</span>
		// 		<span class="col-sm-2">'.$row[admin_message].'</span>
		// 		<span class="col-sm-1">'.$row[status].'</span>
		// 		<span class="col-sm-1">'.$row[date].'</span>
		// 	</div>

		// ';

		$j = 1;
		echo '
				<div class="reward_panel ">
					<div class="col-sm-12">
			';
		while ($row = $data->fetch_assoc()) {

			echo '
					<div class="reward_list col-sm-12">
					 	<div class="col-sm-5 reward_list_img">
					 		<img class="img-responsive" src="../image/reward_image/' . $row[img] . '">
					 	</div>

					 	<div class="col-sm-7 reward_list_data">
							<h3 class=" package_list_name">' . $j . ' . ' . $row[pakage_name] . '</h3>
							<span class="package_list_points"> Required Points: ' . $row[points] . '</span>

							<span class="package_list_des">' . $row[description] . '</span>
							<span class="action_status">' . $action . '</span>
							<span class="package_list_des""><b>Your Message: </b>' . $row[message] . '</span>
							<span class="package_list_des""><b>User Support Message: </b>' . $row[admin_message] . '</span>
					 		<span class="action_status"><b>Status: </b>' . $row[status] . '</span>
							<span class="action_status">' . $row[date] . '</span>
					 	</div>
					</div>
					<div class="clearfix"> </div>
				';

			$j++;
		}
		echo '
					</div>
				</div>
			';
	}

}

function getCommentImgbtn($commentId) {
	include 'data_connection.php';
	$imgBtn = "";
	$matchImg = "SELECT * FROM `comment_image` WHERE `comment_id` = '$commentId' LIMIT 1";

	$data = $conn->query($matchImg);

	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {
			$imgBtn = $imgBtn . '<div id="comment_img_box_' . $commentId . '"><button onclick="showCommentImg(' . $commentId . ')">Show Image</button></div>';
		}
	}
	return $imgBtn;
}

class profileData {
	function getUserInfo($userId) {
		include 'data_connection.php';
		$sql = "SELECT user.*, (SELECT count(type) FROM `comment` WHERE type = 'rate' AND status = 'show' AND user_id = '$userId') AS numOfRate,
		(SELECT count(type) FROM `comment` WHERE type = 'comment' AND status = 'show' AND user_id = '$userId') AS numOfComment,
		(SELECT count(id) FROM `package` WHERE status = 'complete' AND user_id = '$userId') AS numOfPackage
		FROM user WHERE user.id='$userId' LIMIT 1 ;";
		$sql = $conn->query($sql);
		if ($sql->num_rows > 0) {
			while ($row = $sql->fetch_assoc()) {

				if (strpos($row[image_link], ".jpg") != false) {
					$image_link = "image/user_image/" . $row[image_link] . "";
					$imgExist = true;
				} else if (strpos($row[image_link], ".png") != false) {
					$image_link = "image/user_image/" . $row[image_link] . "";
					$imgExist = true;
				} else {
					$imgExist = false;
				}

				$imgExist = file_exists($image_link);

				if ($imgExist == false) {

					if ($row[gender] == 'female') {
						$image_link = "image/user_image/profile2.jpg";
					} else {
						$image_link = "image/user_image/profile.jpg";
					}
				}

				// if($imgExist == false){
				// 	echo '<button class="pro_up_img_but">Upload Image</button>';
				// }
				// else{
				// 	echo '<button class="pro_up_img_but">Change Image</button>';
				// }
				if ($row[signup_type] == "aaoaz") {
					$changePassBtn = '<span class="changePassBtn" onclick="showChangeInput(4)">Change Password</span>';
				} else {
					$changePassBtn = "";
				}
				echo '

				<div class="col-sm-3">
					<div class="profile_image">
						<div class="pro_up_img">
							<a onclick="showImgChange()"><img class="pro_up_img_but" src="./image/default_image/change_image.png"></a>
						</div>
						<div class="pro_up_img_back"></div>
						<img src="' . $image_link . '" class="img-thumbnail">
					  </div>
				</div>
				';

				echo '
				 <div class="col-sm-4">
				 <div class="personal_data">

				 		<div class="personal_data_cng"> <span class="name">' . $row[name] . '</span> <img src="./image/default_image/p_change3.png" onclick="showChangeInput(1)" alt="info change icon"></div>
				 		<div class="p_info_update p_info_update1">
   				 			<form id="p_info_change" >
							      <input id="changeName" placeholder="Your Name" type="name" name="value" tabindex="5" required>
							      <img src="./image/default_image/YES.png" onclick="return changePersonalInfo(\'name\')" >
							</form>
				 		</div>


						<div class="personal_data_cng"> <span class="user_email"><b>E-Mail: </b>' . $row[email] . '</span> <img src="./image/default_image/p_change3.png" onclick="showChangeInput(2)" alt="info change icon"></div>
						<div class="p_info_update p_info_update2">
   				 			<form id="p_info_change" >
							      <input id="changeEmail" placeholder="Email Address" type="email" name="value" tabindex="5" required>
							      <img src="./image/default_image/YES.png" onclick="return changePersonalInfo(\'email\')" >
							</form>
				 		</div>

					 	<div class="personal_data_cng"> <span class="user_phone"><b>Phone: </b>' . $row[phone] . '</span> <img src="./image/default_image/p_change3.png" onclick="showChangeInput(3)" alt="info change icon"></div>
					 	<div class="p_info_update p_info_update3">
   				 			<form id="p_info_change" >
							      <input id="changePhone" placeholder="Phone Number" type="phone" name="value" tabindex="5" required>
							      <img src="./image/default_image/YES.png" onclick="return changePersonalInfo(\'phone\')" >
							</form>
				 		</div>

						<div class="personal_data_cng">
							<span class="user_date"><b>Join: </b>' . $row[date] . '</span>
						</div>


						<div class="personal_data_cng">
							<span class="points_left"><b>Points Left:</b> ' . ($row[total_point] - $row[used_point]) . '</span>
						</div>

						' . $changePassBtn . '

						<div class="p_info_update p_info_update4">
   				 			<form id="p_info_change" >
   				 				<input id="changePasswordOld" placeholder="Old Password" type="password" name="oldpass" tabindex="5" required><br>
   				 				<input id="changePasswordNew" placeholder="New Password" type="password" name="newpss" tabindex="5" required><br>
							    <input id="changePasswordCon" placeholder="Confirm Password" type="password" name="confirmpass" tabindex="5" required><br>
							    <img src="./image/default_image/YES.png" onclick="return changePersonalInfo(\'password\')" >
							</form>
				 		</div>
						';

				echo '
				</div>
				</div>';

				echo '
				 <div class="col-sm-4">
				 <div class="activity_data">
				 			<span class="points_earn"><b>Points Earn:</b> ' . $row[total_point] . '</span>
					 	    <span class="points_spends"><b>Points Spends:</b> ' . $row[used_point] . '</span>

					 		<span class="overall_comment"><b>Overall Comment:</b> ' . $row[numOfComment] . '</span>
					 		<span class="overall_review"><b>Overall Review: </b>  ' . $row[numOfRate] . '</span>
					 		<span class="overall_review"><b>Offer Package Enjoyed: </b>  ' . $row[numOfPackage] . '</span>
				</div>
				</div>';

			}
		}
	}
}
?>