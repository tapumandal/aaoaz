<?php



	function getMap($cata, $sub_cata, $id){
		include "data_connection.php";
		
		$fetch_map = "SELECT * FROM `map`  WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata';";
			
		$data= $conn->query($fetch_map);
		$location= "";
		if($data->num_rows>0){
			
			while ($row=$data->fetch_assoc()) {
				
				if(!empty($row[location]))
					{	
						$location = $location.$row[location]." | ";
					}

			}
		}
		$location = $location." ";
		$location = str_replace(" |  ", "", $location);
		return $location;
	}




	function getFirstPanel($cata){
		include "data_connection.php";

		if($cata=="food" || $cata == "life style"){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY weekly_view DESC LIMIT 8;";
			
		}
		else{
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY total_view DESC LIMIT 8;";	
		}

		$data = $conn->query($sql);

		if($data->num_rows > 0){
			while($row=$data->fetch_assoc()){

				$location = getMap($row[cata], $row[sub_cata], $row[id]);

				$link = $cata;
				$link_name = strtolower($row[name]);
				// $link_name = str_replace("&", "%26", $link_name);
				$link_name = str_replace(" ", "-", $link_name);
				

	
				$nameLen = strlen ( $row[name] );
				$setpadding="";
				if($nameLen>27){
					$setpadding = 'style="padding-top:6px;"';
				}
				
				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);
				echo 
				'
					<div class="home_box">
							<div class="home_box_image">
								<img class="img-responsive" src="image/title_image/'.$row[image_link].'" alt="image">
							</div>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_name">
									<div class="home_box_data">
										<span>'.$row[type].'</span>
										<br>
										<span>'.$location.'</span>
									</div>
								</div>
							</a>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_content_name"><h4 '.$setpadding.'> '.$row[name].'</h4></div>
							</a>
					</div>
				';
			}	
		}
		else{
			
			$subcata_url = explode(" ", $row[sub_cata]);
			$subcata_url[0] = str_replace(",", "", $subcata_url[0]);
			echo "";
		}

	}


	function getSecondPanel($cata){
		include "data_connection.php";

		
		$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `date` DESC LIMIT 6;";
		$data = $conn->query($sql);

		if($data->num_rows>0){
			while($row=$data->fetch_assoc()){

				$link = $cata;
				$link_name = strtolower($row[name]);
				// $link_name = str_replace("&", "%26", $link_name);
				$link_name = str_replace(" ", "-", $link_name);
				

					
								$nameLen = strlen ( $row[name] );
				$setpadding="";
				if($nameLen>27){
					$setpadding = 'style="padding-top:6px;"';
				}
				
				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);
				echo '<div class="home_box2">
							<div class="home_box_image2">
								<img class="img-responsive" src="image/title_image/'.$row[image_link].'" alt="image">
							</div>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_name">
									<div class="home_box_data">
										<span>'.$row[type].'</span>
										<br>
										<span>lOCATIOIN'.$location.'</span>
									</div>
								</div>
							</a>

<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_content_name"><h4 '.$setpadding.'> '.$row[name].'</h4></div>
							</a>
					</div>
				';
			}	
		}
	}

	function getThirdPanel($cata){
		include "data_connection.php";

		// $sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `date` DESC LIMIT 3;";
		

		$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `total_avg_review` DESC LIMIT 6;";

		
		$data = $conn->query($sql);

		if($data->num_rows>0){
			while($row=$data->fetch_assoc()){

				$link = $cata;
				$link_name = strtolower($row[name]);
				// $link_name = str_replace("&", "%26", $link_name);
				$link_name = str_replace(" ", "-", $link_name);
				
	
				$nameLen = strlen ( $row[name] );
				$setpadding="";
				if($nameLen>27){
					$setpadding = 'style="padding-top:6px;"';
				}
				
				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);
				echo 
				'
					<div class="home_box4">
							<div class="home_box_image4">
								<img class="img-responsive" src="image/title_image/'.$row[image_link].'" alt="image">
							</div>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_name">
									<div class="home_box_data">
										<span>'.$row[type].'</span>
										<br>
										<span>'.$location.'</span>
									</div>
								</div>
							</a>


<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_content_name"><h4 '.$setpadding.'> '.$row[name].'</h4></div>
							</a>
					</div>
				';
			}	
		}
	}

	function getFourthPanel($cata){
		include "data_connection.php";
		// $sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `date` DESC LIMIT 3;";
		if($cata == "food"){
			$offerCName = 'column_3';
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata' AND '$offerCName' IS NOT NULL AND TRIM($offerCName) <> ''  LIMIT 5;";
		}
		else if($cata == "life style"){
			$offerCName = 'column_2';	
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata' AND '$offerCName' IS NOT NULL AND TRIM($offerCName) <> ''  LIMIT 5;";
		}
		else if($cata == 'education'){
			$offerCName = 'column_4';	
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata' AND '$offerCName' IS NOT NULL AND TRIM($offerCName) <> ''  LIMIT 5;";	
		}
		else if($cata == 'health'){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `total_view` DESC LIMIT 5;";
		}
		else if($cata == 'electronics'){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `total_view` DESC LIMIT 5;";
		}
		else if($cata == 'residence'){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `total_view` DESC LIMIT 5;";
		}
		else if($cata == 'tourism'){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata'  ORDER BY `total_view` DESC LIMIT 5;";
		}




		$data = $conn->query($sql);

		if($data->num_rows>0){
			while($row=$data->fetch_assoc()){

				$link = $cata;
				$link_name = strtolower($row[name]);
				// $link_name = str_replace("&", "%26", $link_name);
				$link_name = str_replace(" ", "-", $link_name);
				
	
				$nameLen = strlen ( $row[name] );
				$setpadding="";
				if($nameLen>27){
					$setpadding = 'style="padding-top:6px;"';
				}
				
				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);
				echo 
				'
					<div class="home_box3">
							<div class="home_box_image3">
								<img class="img-responsive" src="image/title_image/'.$row[image_link].'" alt="image">
							</div>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_name">
									<div class="home_box_data">
										<span>'.$row[type].'</span>
										<br>
										<span>'.$location.'</span>
									</div>
								</div>
							</a>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_content_name"><h4 '.$setpadding.'> '.$row[name].'</h4></div>
							</a>
					</div>
					
				';
			}	
		}
	}

	function getFifthPanel($cata){
		include "data_connection.php";

	
		if($cata == 'food'){
			$sql = "SELECT * FROM item_list LEFT JOIN general_info ON general_info.id = item_list.cata_id WHERE general_info.status = 'approved' AND general_info.cata = '$cata'  ORDER BY item_list.date DESC LIMIT 5;";
		}
		else if($cata == 'life style'){
			$sql = "SELECT * FROM item_list LEFT JOIN general_info ON general_info.id = item_list.cata_id WHERE general_info.status = 'approved' AND general_info.cata = '$cata'  ORDER BY item_list.date DESC LIMIT 5;";
		}
		else if($cata == 'education'){
			$exam = 'column_13';
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata' AND '$exam' IS NOT NULL AND TRIM($exam) <> ''  LIMIT 5;";
		}
		else if($cata == 'health'){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata' ORDER BY rand()  LIMIT 5;";
		}
		else if($cata == 'electronics'){
			$sql = "SELECT * FROM elec_item_details LEFT JOIN general_info ON general_info.id = elec_item_details.cata_id WHERE general_info.status = 'approved' AND general_info.cata = '$cata'  ORDER BY elec_item_details.date  DESC LIMIT 5;";
		}
		else if($cata == 'residence'){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata' ORDER BY rand()  LIMIT 5;";
		}
		else if($cata == 'tourism'){
			$sql = "SELECT * FROM general_info WHERE status = 'approved' AND cata = '$cata' ORDER BY rand()  LIMIT 5;";
		}

		$data = $conn->query($sql);

		if($data->num_rows>0){
			while($row=$data->fetch_assoc()){

				$link = $cata;
				$link_name = strtolower($row[name]);
				// $link_name = str_replace("&", "%26", $link_name);
				$link_name = str_replace(" ", "-", $link_name);
				

					
								$nameLen = strlen ( $row[name] );
				$setpadding="";
				if($nameLen>27){
					$setpadding = 'style="padding-top:6px;"';
				}
				
				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);
				echo 
				'	<div class="home_box5">
							<div class="home_box_image5">
								<img class="img-responsive" src="image/title_image/'.$row[image_link].'" alt="image">
							</div>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_name">
									<div class="home_box_data">
										<span>'.$row[type].'</span>
										<br>
										<span>'.$location.'</span>
									</div>
								</div>
							</a>
							<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
								<div class="home_box_content_name"><h4 '.$setpadding.'> '.$row[name].'</h4></div>
							</a>
					</div>
				';
			}	
		}
	}


?>