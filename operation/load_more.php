<?php
	session_start();
	error_reporting(0);
	include '../data_connection.php';

	include '../code_php.php';


	function loadStringFilter($string){
		$string = trim($string);
		$string = str_replace('\\','/', $string);
		$string = str_replace('\'',"", $string);
		$string = str_replace('"','', $string);
		$string = str_replace('`',"", $string);
		$string = str_replace(';','.', $string);
		return $string;
	}

	$getLoadMoreType = loadStringFilter($_GET[loadMoreType]);
	$getSub_cata = loadStringFilter($_GET[sub_cata]);
	$getCata = loadStringFilter($_GET[cata]);

	if($getLoadMoreType=="specific" || $getLoadMoreType == "home"){
					

					// $loadedContentId = $_SESSION[loadedContentId];

					// echo ">".$loadedContentId[0].">".$loadedContentId[1].">".$loadedContentId[2];
					if($getLoadMoreType=="home"){
							if($_SESSION[locationFilter] == "ANYWHERE" || $_SESSION[locationFilter]==""){
											    			
								$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `type`, `image_link`, `main_location` from general_info where `status` = 'approved' ORDER BY RAND();";	
							}
				    		else {
				    			
						    	$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata, general_info.name, general_info.type, general_info.image_link,   map.cata_id, map.location from general_info LEFT JOIN map ON general_info.id = map.cata_id where  general_info.status = 'approved' AND  map.location = '$_SESSION[locationFilter]' GROUP BY general_info.id ORDER BY RAND() ;";
						    		
				    		}
					}
					else if($getLoadMoreType=="specific"){
							if($_SESSION[locationFilter] == "ANYWHERE" || $_SESSION[locationFilter]==""){
				    			// echo "<h1> ANYWHERE </h1>";
				    				if($getSub_cata == "")
						    		{
						    			//echo $getCata;
						    			$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `type`, `image_link`, `main_location` from general_info where cata= '$getCata' AND `status` = 'approved' ORDER BY RAND();";
						    		}
						    		else if($getCata)
						    		{
						    			$sub_cata_tmp = '%'.$getSub_cata.'%';
						    			$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `type`, `image_link`, `main_location` from general_info where cata= '$getCata' AND sub_cata LIKE '$sub_cata_tmp' AND `status` = 'approved' ORDER BY RAND();";	
						    		}
						    		else{
						    			$sql = "SELECT `id`, `sub_cata`, `cata`, `name`, `type`, `image_link`, `main_location` from general_info where cata= '$getCata' AND sub_cata LIKE '$getSub_cata' AND `status` = 'approved' ORDER BY RAND();";		
						    		}
				    		}

				    		else {
				    			
				    				if($getSub_cata == "")
						    		{
						    			//echo $getCata;
						    			$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata, general_info.name, general_info.type, general_info.image_link,   map.cata_id, map.location from general_info LEFT JOIN map ON general_info.id = map.cata_id where general_info.cata= '$getCata' AND general_info.status = 'approved' AND  map.location = '$_SESSION[locationFilter]' GROUP BY general_info.id ORDER BY RAND() ;";
						    		}
						    		else if($getCata)
						    		{
						    			$sub_cata_tmp = '%'.$getSub_cata.'%';
						    			$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata, general_info.name, general_info.type, general_info.image_link, general_info.main_location,  map.cata_id, map.location from general_info LEFT JOIN map ON general_info.id = map.cata_id where general_info.cata= '$getCata' AND general_info.status = 'approved' AND  map.location = '$_SESSION[locationFilter]' AND general_info.sub_cata LIKE '$sub_cata_tmp' GROUP BY general_info.id ORDER BY RAND() ;";	
						    		}
						    		else{
						    			$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata, general_info.name, general_info.type, general_info.image_link, general_info.main_location,  map.cata_id, map.location from general_info LEFT JOIN map ON general_info.id = map.cata_id where general_info.cata= '$getCata' AND general_info.status = 'approved'  AND map.location = '$_SESSION[locationFilter]' AND general_info.sub_cata LIKE '$getSub_cata' GROUP BY general_info.id ORDER BY RAND() ;";		
						    		}
				    		}
				    }

						$data = $conn->query($sql);
						
						$window_width = $_SESSION[scrWidth];
						if($data->num_rows>0)
						{
							$i=1;

							

							while($row = $data-> fetch_assoc())
							{
								if (in_array($row[id], $_SESSION[loadedContentId])) {
								   
								}
								else{
									
													$id = $row['id'];
													$name = $row['name'];
													$link_name = $row[name];
													// $link_name = str_replace("&", "%26", $link_name);
													$link_name = str_replace(" ", "-", $link_name);
													// $link_name = str_replace("&", "%26", $link_name);
													$rating = $row['rating'];
													$location =  $row['location'];
													$catagory =  $row['cata'];
													$image = strpos($row['image_link'], ".jpg");

													// Blank image remove
													$image = strpos($row['image_link'], ".jpg");
													if(strpos($row['image_link'], ".jpg") === false && strpos($row['image_link'], ".png") === false)
													{
															$image   = "1_1no_title_image.jpg";
													}
													else if(empty($row['image_link']))
													{
															$image   = "1_1no_title_image.jpg";
													}
													else {
														$image   = $row['image_link'];
													}
													// Blank image remove
													
													//$name = substr($name, 0, 22);
													
													$flagType=0;
													
													if(strlen($row[type])>25)$type = $type."...";
													

													$subcata = strtolower($row['sub_cata']);
													
													
													// $loadedContentId[$i] = $row[id];
													array_push($_SESSION[loadedContentId], $row[id]);

													
														$link = strtolower($catagory);

														$subcata_url = explode(" ", $subcata);
														$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

														

														if($_SESSION[scrWidth]>500){
															if (!file_exists("../image/title_image/".$image."")) {   
																$image = "1_1no_title_image.jpg";
															}

															

															$nameLen = strlen ( $row[name] );
															$setpadding="";
															if($nameLen>27){
																$setpadding = 'style="padding-top:6px;"';
															}
															$general_rate_obj = new general();

															// GET LOCATION
																$fetch_map = "SELECT * FROM `map`  WHERE cata_id = '$id' AND cata= '$catagory';";
																$data2= $conn->query($fetch_map);
																$location= "";
																if($data2->num_rows>0){
																	
																	while ($row3=$data2->fetch_assoc()) {
																		
																		if(!empty($row3[location]))
																			{	
																				$location = $location.$row3[location]." | ";
																			}

																	}
																}
																$location = $location." ";
																$location = str_replace(" |  ", "", $location);
															// GET LOCATION



															$type = substr($row[type], 0, 25);
															echo '
																<div class="col-sm-3 area" >
																	<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
																		<div class="dx_box_img">
													                  		<img class="img-responsive" src="image/title_image/'.$image.'" alt="image">
													                  	</div>
													                </a>
												                  	<div class="dx_box_info">
													                	<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
													                		<h3 '.$setpadding.'> '.$row[name].'</h3>
													                	</a>
													                  	'.$general_rate_obj->ratingSingle($total_rate).'
													                	<span>'.$row[type].'</span>
													                	<br>
													                	<span>'.$location.'</span>
													              	</div>
													             
												                </div>
												                ';
													    }else{

													    	$type = substr($row[type], 0, 60);

													    	$fetch_map2 = "SELECT * FROM `map`  WHERE `cata_id` = '$row[id]';";
															$data2= $conn->query($fetch_map2);
															$location2= "";
															if($data2->num_rows>0){
																
																while ($rowMap=$data2->fetch_assoc()) {
																	
																	if(!empty($rowMap[location]))
																		{	
																			$location2 = $location2.$rowMap[location]." | ";
																		}
													
																}
															}
															$location2 = $location2." ";
															$location2 = str_replace(" |  ", "", $location2);

															$location = substr($location2, 0, 40);

															if(strlen($location2) > 40){
																$location = $location."...";
																$location = str_replace(" |.", ".", $location);
																$location = str_replace(" | .", "	.", $location);
															}

															if(strlen($name)>22){
																$name = substr($name, 0, 20);
																$name = $name."..";
															}


															if (!file_exists("../image/title_image/s_".$image."")) {   
																$image = "1_1no_title_image.jpg";
															}





													    	echo '
													    		<a href="'.$subcata_url[0].'/'.strtolower($link_name).'">
													    		<div class="content_list">
													    			<div class="content_list_image">
													    				<img class="img-responsive" src="image/title_image/s_'.$image.'" class="img-responsive img-circle"	 alt="IMAGE"></img>
													    			</div>

													    			<div class="content_list_info">
													    				<div class="content_list_name"> <h2>'.$name.'</h2> </div>
													    				<div class="content_list_type"> <span>'.$type.'</span> </div>
													    				<div class="content_list_location"> <span>'.$location.'</span> </div>
													    			</div>
													    		</div>
													    		</a>
													    	';

													    	if($i>15){
															   	break;
															   }
													    }
													    		
													    	if($window_width>1549){
											 				if($i%5 == 0) echo '<div class="clearfix"> </div>';
											 				
											 			}
											 			else if($window_width<1549 && $window_width>1049){
											 				if($i%4 == 0) echo '<div class="clearfix"> </div>';
											 				
											 			}
											 			else if($window_width<1050 && $window_width>759){
											 				if($i%3 == 0) echo '<div class="clearfix"> </div>';
											 			}
											 			//else if($window_width<760 && $window_width>349){
											 			else if($window_width<760){
											 				if($i%2 == 0) echo '<div class="clearfix"> </div>';
											 			}
											 			else if($window_width<350){
											 				//if($i%1 == 0) echo '<div class="clearfix"> </div>';
											 			}
													   $i = $i+1;

													   if($i>20){
													   	break;
													   }
								}
								   
							}
							
						}

					// $_SESSION[loadedContentId] = $loadedContentId;
	}
?>