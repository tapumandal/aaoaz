<?php
//error_reporting(0);
//session_start();

$_SESSION[viewCount] = "deny";
$RequestSignature = $_SERVER['REQUEST_URI'];
if ($_SESSION['LastRequest'] == $RequestSignature) {
	$_SESSION[viewCount] = "deny";
	//echo 'This is a refresh.';
} else {
	$_SESSION[viewCount] = "accept";
	//echo 'This is a new request.';
	$_SESSION['LastRequest'] = $RequestSignature;
}

$getCata = stringFilter($_GET[cata]);
$getsubCata = stringFilter($_GET[sub_cata]);

$getCata = strip_tags($getCata);
$getsubCata = strip_tags($getsubCata);

if (!isset($_GET[content])) {

	if ($getCata != "food" && $getCata != "life style" && $getCata != "health" && $getCata != "education" && $getCata != "residence" && $getCata != "tourism" && $getCata != "ecommerce") {

		// comment these code when you are in local server
		if ($_SERVER["REQUEST_URI"] == "/") {
			unset($_SESSION[content_name]);
			unset($_SESSION[content_type]);
			unset($_SESSION[content_id]);
			unset($_SESSION[sub_cata]);
			unset($_SESSION[cata]);
		} else if ($_SERVER["REQUEST_URI"] == "/index.php") {
			unset($_SESSION[content_name]);
			unset($_SESSION[content_type]);
			unset($_SESSION[content_id]);
			unset($_SESSION[sub_cata]);
			unset($_SESSION[cata]);
		} else if ($_SERVER["REQUEST_URI"] == "/email.php") {
			unset($_SESSION[content_name]);
			unset($_SESSION[content_type]);
			unset($_SESSION[content_id]);
			unset($_SESSION[sub_cata]);
			unset($_SESSION[cata]);
		} else if ($_SERVER["REQUEST_URI"] == "/about.php") {
			unset($_SESSION[content_name]);
			unset($_SESSION[content_type]);
			unset($_SESSION[content_id]);
			unset($_SESSION[sub_cata]);
			unset($_SESSION[cata]);
		} else if ($_SERVER["REQUEST_URI"] == "/contact.php") {
			unset($_SESSION[content_name]);
			unset($_SESSION[content_type]);
			unset($_SESSION[content_id]);
			unset($_SESSION[sub_cata]);
			unset($_SESSION[cata]);
		} else {
			header('Location: addressnotfound.php');
		}

		unset($_SESSION[cata]);
		unset($_SESSION[sub_cata]);

	} else if ($getsubCata != "") {

		if ($getsubCata != "restaurant" && $getsubCata != "cafe" && $getsubCata != "cart" && $getsubCata != "sweets" && $getsubCata != "cake_pastry" &&
			$getsubCata != "fashion" && $getsubCata != "accessorice" && $getsubCata != "parlor_salon" && $getsubCata != "gym_fitness" &&
			$getsubCata != "university" && $getsubCata != "college" && $getsubCata != "coaching" && $getsubCata != "exams_result" && $getsubCata != "admission" &&
			$getsubCata != "hospital_clinic" && $getsubCata != "doctor" && $getsubCata != "ambulance" &&
			$getsubCata != "hotel" && $getsubCata != "resort" && $getsubCata != "to-let" &&
			$getsubCata != "natural_site" && $getsubCata != "heritage" && $getsubCata != "picnic_spot" && $getsubCata != "lake_park" &&
			$getsubCata != "health_beauty" && $getsubCata != "fashion" && $getsubCata != "food_grocery" && $getsubCata != "electronics_gadget" && $getsubCata != "gift_craft" && $getsubCata != "buy_sell" &&
			$getsubCata != "job" && $getsubCata != "others") {

			if ($_SERVER["REQUEST_URI"] == "/") {
			} else if ($_SERVER["REQUEST_URI"] == "/index.php") {
			} else if ($_SERVER["REQUEST_URI"] == "/email.php") {
			} else if ($_SERVER["REQUEST_URI"] == "/about.php") {
			} else {
				header('Location: addressnotfound.php');
			}
		}

		$_SESSION[cata] = $getCata;
		$_SESSION[sub_cata] = $getsubCata;
	} else {
		$_SESSION[cata] = $getCata;
		unset($_SESSION[sub_cata]);
	}
} else {
	if ($_GET[content] == "") {
		$newUrl = rtrim($_SERVER["REQUEST_URI"], "/ ");
		echo "<script>window.location='" . $newUrl . "'; </script>";
	}

}

if (isset($_GET[content])) {

	include 'data_connection.php';

	unset($_SESSION[content_name]);
	unset($_SESSION[content_type]);
	unset($_SESSION[content_id]);
	unset($_SESSION[sub_cata]);
	unset($_SESSION[cata]);

	$preUrl = explode("/", $_SERVER["REQUEST_URI"]);

	if (strpos($preUrl[2], '&') !== false) {
		$content = $preUrl[2];
		$content = strip_tags($content);
	} else {

		$content = $_GET[content];
		$content = strip_tags($content);
	}
	// if($preUrl[2])

	$content = str_replace("-", " ", $content);

	$max = 0;
	$sql2 = "SELECT `id`, `sub_cata`, `cata`, `name`, `type` FROM `general_info` WHERE `status` = 'approved';";
	$sql2 = $conn->query($sql2);
	if ($sql2->num_rows > 0) {
		while ($row2 = $sql2->fetch_assoc()) {

			similar_text(strtolower($row2[name]), strtolower($content), $match);

			if ($match > $max) {
				$max = $match;

				$content_name = $row2[name];
				$content_type = $row2[type];
				$content_id_s = $row2[id];
				$sub_cata_s = $row2[sub_cata];
				$cata_s = $row2[cata];

			}

		}

		// $_SESSION[content_name] = str_replace("&","%26", $content_name);
		$_SESSION[content_name] = $content_name;
		$_SESSION[content_type] = $content_type;
		$_SESSION[content_id] = $content_id_s;
		$_SESSION[sub_cata] = $sub_cata_s;
		$_SESSION[cata] = $cata_s;

		$fetch_map = "SELECT * FROM `map`  WHERE `cata_id` = '$_SESSION[content_id]' AND `cata`= '$_SESSION[cata]' AND `sub_cata`='$_SESSION[sub_cata]';";
		$data = $conn->query($fetch_map);
		$location = "";
		if ($data->num_rows > 0) {

			while ($row = $data->fetch_assoc()) {

				if (!empty($row[location])) {
					$location = $location . $row[location] . " | ";
				}

			}
		}
		$location = $location . " ";
		$location = str_replace(" |  ", "", $location);
		$_SESSION[all_location] = $location;

		// echo "<script> window.location = './".$_SESSION[cata].".php?content=".$_SESSION[content_name]."'; </script>";

	}

	if ($max != 100) {
		$link_name = str_replace(" ", "-", $_SESSION[content_name]);
		$link_name = strtolower($link_name);
		$link_name;
		$preUrl = explode("/", $_SERVER["REQUEST_URI"]);
		$preUrl[2] = $link_name;
		$newRealUrl = implode("/", $preUrl);
		echo "<script>window.location='" . $newRealUrl . "'; </script>";
	}

}

function stringFilter($string) {
	$string = trim($string);
	//$string = str_replace('%27','', $string);
	$string = str_replace('\\', '', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('SELECT', '', $string);
	$string = str_replace('UPDATE', '', $string);
	$string = str_replace('DROP', '', $string);
	$string = str_replace('INSERT', '', $string);
	$string = str_replace('DELETE', '', $string);
	$string = str_replace('FROM', '', $string);
	$string = str_replace('WHERE', '', $string);

	$string = str_replace('select', '', $string);
	$string = str_replace('update', '', $string);
	$string = str_replace('drop', '', $string);
	$string = str_replace('insert', '', $string);
	$string = str_replace('delete', '', $string);
	$string = str_replace('from', '', $string);
	$string = str_replace('where', '', $string);
	return $string;
}

function getMap2($cata, $sub_cata, $id) {
	/*	include "data_connection.php";

		$fetch_map = "SELECT * FROM `map`  WHERE `cata_id` = '$id' AND `cata`= '$cata' AND `sub_cata`='$sub_cata';";

		$data= $conn->query($fetch_map);
		$location= "";
		if($data->num_rows>0){

			while ($row=$data->fetch_assoc()) {

				if(!empty($row[location]))
					{
						$location = $location.$row[location]." | ";
					}

			}
		}
		$location = $location." ";
		$location = str_replace(" |  ", "", $location);
		$_SESSION[all_location] = $location;*/

}
function getFbSingleImg($cata, $sub_cata, $id) {
	include "data_connection.php";

	$fetch_image_data = "SELECT image_link FROM `general_image` WHERE cata_id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' LIMIT 1";

	$data = $conn->query($fetch_image_data);
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {

			$image_link = $row[image_link];
			echo '<meta property="og:image" content="http://aaoaz.com/image/main_image/' . $image_link . '"/>';

		}
	}
}
function getFbSingleDescription($cata, $sub_cata, $id) {
	include "data_connection.php";

	$fetch_des_data = "SELECT description FROM `general_info` WHERE id = '$id' AND cata= '$cata' AND sub_cata='$sub_cata' LIMIT 1";

	$data = $conn->query($fetch_des_data);
	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {

			$des = $row[description];
			$des = substr($des, 0, 150);
			$des = str_replace('"', '', $des);
			echo $des;

		}
	}
}
?>