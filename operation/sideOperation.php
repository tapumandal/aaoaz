<?php

function getFirstRightSide($getCata) {
	if ($getCata == "food") {
		foodCusineList();
	} else if ($getCata == "") {
		lifeStyleFirstRight();

	} else if ($getCata == "health") {
		healthDeptList();
	} else if ($getCata == "education") {
		educationDeptList();

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	}
}

function getSecondRightSide($getCata) {
	if ($getCata == "food") {
		getPopularFood();
	} else if ($getCata == "") {

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	} else if ($getCata == "") {

	}
}

function educationDeptList() {
	include "data_connection.php";

	$sql = "SELECT depertment, COUNT(*) AS numOfDept FROM `doctor_faculty` WHERE cata = 'education' AND depertment != '' AND depertment != '---' GROUP BY depertment ORDER BY numOfDept DESC LIMIT 8";

	$res = $conn->query($sql);
	if ($res->num_rows > 0) {
		echo '
				<div class="dx_spec_right_title">
                      <h5>Popular Department</h5>
                </div>
                <div class="dx_spec_right_list">
                      <ul>
				';

		while ($row = $res->fetch_assoc()) {
			$department = strtolower(str_replace(" ", "-", $row[depertment]));

			echo '<li> <a href="./education/department/' . $department . '/1"> <img class="img-responsive" src="image/default_image/right_side/doc_department2.svg"> <span>' . $department . '</span><span></span></a></li>';
		}

		echo '
					</ul>
                </div>
			';
	}
}

function healthDeptList() {
	include "data_connection.php";
	$sql = "SELECT depertment, COUNT(*) AS numOfDept FROM `doctor_faculty` WHERE cata = 'health' AND depertment != '' AND depertment != '---' GROUP BY depertment ORDER BY numOfDept DESC LIMIT 8";

	$res = $conn->query($sql);
	if ($res->num_rows > 0) {
		echo '
				<div class="dx_spec_right_title">
                      <h5>Popular Department</h5>
                </div>
                <div class="dx_spec_right_list">
                      <ul>
				';

		while ($row = $res->fetch_assoc()) {
			$department = strtolower(str_replace(" ", "-", $row[depertment]));

			echo '<li> <a href="./health/department/' . $department . '/1"> <img class="img-responsive" src="image/default_image/right_side/doc_department2.svg"> <span>' . $department . '</span><span></span></a></li>';
		}

		echo '
					</ul>
                </div>
			';
	}

}

function foodCusineList() {
	echo '
				<div class="dx_spec_right_title">
	                  <h5>Popular Cusine</h5>
	            </div>
	            <div class="dx_spec_right_list">
	                  <ul>
	                    <li> <a href="./cusine/bengali/1"> <img class="img-responsive" src="image/default_image/cusine_icon/bengali.png"> <span> Bengali</span> <span></span> </a></li>
	                    <li> <a href="./cusine/indian/1"> <img class="img-responsive" src="image/default_image/cusine_icon/indian.png"> <span> Indian</span> <span></span> </a></li>
	                    <li> <a href="./cusine/thai/1"> <img class="img-responsive" src="image/default_image/cusine_icon/thai.png"> <span> Thai</span> <span></span> </a></li>
	                    <li> <a href="./cusine/chainese/1"> <img class="img-responsive" src="image/default_image/cusine_icon/chinese.png"> <span> Chinese</span> <span></span> </a></li>
	                    <li> <a href="./cusine/italian/1"> <img class="img-responsive" src="image/default_image/cusine_icon/italian.png"> <span> Italian</span> <span></span> </a></li>
	                    <li> <a href="./cusine/mexico/1"> <img class="img-responsive" src="image/default_image/cusine_icon/mexican.png"> <span> Mexican</span> <span></span> </a></li>
	                    <li> <a href="./cusine/fast_food/1"> <img class="img-responsive" src="image/default_image/cusine_icon/fast_food.png"> <span> Fast Food</span> <span></span> </a></li>
	                    <li> <a href="./cusine/sea_food/1"> <img class="img-responsive" src="image/default_image/cusine_icon/sea_food.png"> <span> Sea Food</span> <span></span> </a></li>
	                    <li> <a href="./cusine/buffet/1"> <img class="img-responsive" src="image/default_image/cusine_icon/buffet.png"> <span> Buffet</span> <span></span> </a></li>
	                  </ul>
	            </div>
			';
}

function getPopularFood() {
	include "data_connection.php";
	// $getTopItem = "SELECT name, COUNT(*) AS numOfFood FROM `item_list` GROUP BY name HAVING numOfFood > 2 ORDER BY name ASC LIMIT 4";
	$getTopItem = "SELECT name, COUNT(*) AS numOfFood FROM `item_list` WHERE name != '' GROUP BY name ORDER BY numOfFood DESC LIMIT 7";
	$data = $conn->query($getTopItem);

	if ($data->num_rows > 0) {

		echo '
				<div class="dx_spec_right_title">
                      <h5>Popular Food</h5>
                </div>
                <div class="dx_spec_right_list">
                      <ul>
				';

		while ($row = $data->fetch_assoc()) {
			$name = strtolower(str_replace(" ", "-", $row[name]));

			echo '<li> <a href="./popular/food/' . $name . '/1"> <img class="img-responsive" src="image/default_image/recomended_food_icon2.png"> <span>' . $row[name] . '</span> <span>(' . $row[numOfFood] . ')</span> </a></li>';
		}

		echo '
					</ul>
                </div>
			';
	}
}
?>