<?php
	session_start();
	
	
	
	//echo $RequestSignature."<br>".$_SESSION['LastRequest']."<br>";
	//$viewCount is set in url_operation.php
	
	if($_POST[takeAction] == "action" && $_SESSION[viewCount] == "accept" && !isset($_SESSION[adminId])){
		//echo "IF ".$_SESSION[viewCount];
		$vObj2 = new view();
		$vObj2->everyView($_SESSION[content_id]);
		
		$_SESSION[viewCount] = "deny";
	}
	else{
		//echo "<br>"."ELSE ".$_SESSION[viewCount];
	}

	

	class view{
		function everyView($content_id){
			include 'data_connection.php';

			$update = "UPDATE `general_info` SET `total_view` = `total_view`+1, `daily_view` = `daily_view`+1, `weekly_view` = `weekly_view`+1, `monthly_view` = `monthly_view`+1, `yearly_view` = `yearly_view`+1 WHERE `id` = '$content_id';";
			// echo $update;
			if($conn->query($update)===TRUE){
			}

		}

		function startView(){
			include 'data_connection.php';

			date_default_timezone_set('Asia/Dhaka'); // CDT

			$info = getdate();
			$date = $info['mday'];
			$day  =  $info['weekday'];
			//$weekNum = $info->format("W");
			$weekNum = date('W');
			$month = $info['mon'];
			$year = $info['year'];
			$hour = $info['hours'];
			$min = $info['minutes'];
			$sec = $info['seconds'];


			$sql = "SELECT * FROM `set_view`;";

			$result = $conn->query($sql);

			if($result->num_rows>0){
				while($row=$result->fetch_assoc()){
					$dailyChangeStatus = $row[setDailyView];
					$dailyFlag = $row[dailyFlag];
					
					$weeklyChangeStatus = $row[setWeeklyView];
					$weeklyFlag = $row[weeklyFlag];
					
					$monthlyChangeStatus = $row[setMonthlyView];
					$monthlyFlag = $row[monthlyFlag];
					
					$yearlyChangeStatus = $row[setYearlyView];
					$yearlyFlag = $row[yearlyFlag];
				}
			}


			//echo "<h1>Hour:".$hour."=Day:".$day."=Date:".$date."=Month:".$month."=Year:".$year."</h1>".$dailyChangeStatus;

			$ob = new view();

			if($dailyFlag != $date){
				$ob->setDailyView($date);
				//echo "First function call";
			}
			


			if($weeklyFlag != $weekNum){
				$ob->setWeeklyView($weekNum);
			}
			


			if($monthlyFlag != $month){
				$ob->setMonthlyView($month);
			}


			if($yearlyFlag != $year){
				$ob->setYearlyView($year);
			}



		}

		function setDailyView($date){
			include 'data_connection.php';

				$updateLV = "UPDATE `general_info` SET  `yesterday_view` = `daily_view` WHERE 1;";
			
				if($conn->query($updateLV)===TRUE){
					//echo "Set Daily View to 0";
				}
				
				
				

				$update = "UPDATE `general_info` SET  `daily_view` = 0 WHERE 1;";
			
				if($conn->query($update)===TRUE){
					//echo "Set Daily View to 0";
				}
				else{
					//echo "Daily View Not Set";
				}


				$viewSql = "UPDATE `set_view` SET  `dailyFlag` = '$date';";
				if($conn->query($viewSql)===TRUE){
					
				}
			
		}

		function setWeeklyView($weekNum){
			include 'data_connection.php';
			
			
				$updateLV = "UPDATE `general_info` SET  `last_week_view` = `weekly_view` WHERE 1;";
			
				if($conn->query($updateLV)===TRUE){
					//echo "Set Daily View to 0";
				}

				$update = "UPDATE `general_info` SET  `weekly_view` = 0 WHERE 1;";
			
				if($conn->query($update)===TRUE){
					//echo "Set Daily View to 0";
				}
				else{
					//echo "Daily View Not Set";
				}

				$viewSql = "UPDATE `set_view` SET  `weeklyFlag` = '$weekNum';";
				if($conn->query($viewSql)===TRUE){
					
				}
		}

		function setMonthlyView($month){
			include 'data_connection.php';
				
				$updateLV = "UPDATE `general_info` SET  `last_month_view` = `monthly_view` WHERE 1;";
				if($conn->query($updateLV)===TRUE){
					//echo "Set Daily View to 0";
				}
				
				
				
				$update = "UPDATE `general_info` SET  `monthly_view` = 0 WHERE 1;";
			
				if($conn->query($update)===TRUE){
					//echo "Set Daily View to 0";
				}
				else{
					//echo "Daily View Not Set";
				}


				$viewSql = "UPDATE `set_view` SET  `monthlyFlag` = '$month';";
				if($conn->query($viewSql)===TRUE){
					
				}
		}

		function setYearlyView($year){
			include 'data_connection.php';
				
				$updateLV = "UPDATE `general_info` SET  `last_year_view` = `yearly_view` WHERE 1;";
				if($conn->query($updateLV)===TRUE){
					//echo "Set Daily View to 0";
				}
				
				
				$update = "UPDATE `general_info` SET  `yearly_view` = 0 WHERE 1;";
			
				if($conn->query($update)===TRUE){
					// echo "Set Daily View to 0";
				}
				else{
					// echo "Daily View Not Set";
				}


				$viewSql = "UPDATE `set_view` SET  `yearlyFlag` = '$year';";
				if($conn->query($viewSql)===TRUE){
					
				}
		}
	}
?>