
<!DOCTYPE html>
<?php
session_start();
include 'operation/url_operation.php';
?>

<head>

	<title><?php echo $_SESSION[content_name].' | '.$_SESSION[all_location].' | '.$_SESSION[content_type]; ?></title>
	<meta charset="utf-8">
	<meta name="description" content="Looking for <?php echo $_SESSION[content_name].' in '.$_SESSION[all_location]; ?>. Here is all information and public review/rating. aaoaz provide basic information, images, item or product, contact, user comment of the services or company you are asking for">
	<meta name="keywords" content="<?php echo "aaoaz.com contact, product, item, address, location  information, ". $_SESSION[content_name].', '.$_SESSION[all_location].', '.$_SESSION[content_type].', '.$_SESSION[cata].', '.$_SESSION[sub_cata]; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


</head>
<script>




	

	$(document).ready(function(){

		var g_info_h = $('.container').height();
		alert(g_info_h);
		$('.info-area-title').css("cssText", "height: "+g_info_h+"px !important;");
	});

</script>


<body>



	<div class = "container food">
		<!-- OVERLAY -->
		<!-- FOR BLUR -->
		<!-- CLICK DISPAPPEAR -->
		<!-- <a href="" onclick='show_food_overlay(this)'>  <div class="overlay-bg-new" ID="food_overlay_bg" style="display:none"></div></a>
		<div class="overlay-content popup54441" >

			<a href="" onclick='show_food_overlay(this)'><button class="close-btn">X</button></a>
		    Everything Here
		</div> -->
		<!-- CLICK DISPAPPEAR -->

		<!-- OVERLAY -->	


		<!-- OVERLAY -->
		<!-- FOR BLUR -->
		
		<div class="overlay-bg"> </div> <!-- FOR BLUR -->
		<div class="overlay-content popup54441" >
			<!-- <button class="close-btn">X</button> -->
			<div ID="overlay_content">
				
			</div>
			
		</div>

		<!-- OVERLAY -->	



		<div class = "main-top">
			<div class="main">
				<?php
				
				include 'header.php';
				
				?>
			</div>
		</div>

		<div class="col-sm-12 field food">
			<div class="wide_pattern col-sm-10" id="search_result">  <!-- Whole information without add -->

				<div class="col-sm-6 first food_left_side" ID="info_height">  <!-- NORMAL INFORMATION  -->
					<?php
					$general_obj = new general();
					$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
					$general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
					$general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
					
					?>
				</div>   <!-- NORMAL INFORMATION  -->

				<div class="col-sm-6 second"  id="slide_height">   <!-- PHOTO GALLARY -->
					
					<?php 
					include 'slide-show.php';
					?>

					<!-- PHOTO GALLARY -->

					
					<?php
					$general_obj = new general();
					$table_pattern_top_1 = '
					<!-- ITEM LIST -->
					<div class="menu food_item_table">

						<ul class="nav nav-tabs">
							';
							$table_pattern_top_2 = $general_obj->filter_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
							
							$table_pattern_top_3 = '</ul>
							
							<div class="food_item_list" ID="food_list_height" style="height:300px">
								<div class="table-responsive">          
									<table class="table">
										<thead>
											<tr class="food_list_table_hader">
												<th width="2%"></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											';
											$table_pattern_top = $table_pattern_top_1.$table_pattern_top_2.$table_pattern_top_3;
											$table_pattern_bottom = '</tbody>
										</table>
									</div>
								</div>
							</div>  <!-- ITEM LIST -->
							';

							
							$general_obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], $table_pattern_top, $table_pattern_bottom);
							?>
						</div>	
						<!-- <div class="clearfix"></div> -->

						<!-- <div class="col-sm-12">   COMMENT AND QUESTIOIN -->
						<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->
							

							<?php
							include 'comment_3.php';
							?>
							
							<div class="com">
								<?php
								$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
								?>
							</div>

						</div>   <!-- COMMENT  -->	   				
						<!-- </div>   COMMENT AND QUESTIOIN -->



					</div> <!-- Whole information without add -->

					<!-- add -->
					<div class="col-sm-2 third">  
						<div class="inside_top_right_panel">
							<div class="inside_top_head">
								<h3>Recomended</h3>
							</div>
							<div class="insider_box_panel">

								<?php
								include 'operation/suggestion.php';
								$obj = new suggestion();
								$obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
								?>
								
							</div>
						</div>
					</div>
					<!-- add -->

				</div>
				

			</div>
			<!-- END OF container -->
			<div class="footer">
				<?php
				include 'operation/page_view.php';

				$vObj = new view();

				$vObj->everyView($_SESSION[content_name]);

				$vObj->startView();
				?>	
			</div>

		</body>
		</html>