<?php
  include 'operation/home_data_operation.php';
  include 'operation/sideOperation.php';

  if($getCata == "food"){
    $firstTitle = "Most weekly viewed";
    $secondTitle = "New in aaoaz";
    $thirdTitle = "Most rated.";
    $fourthTitle = "Offer going on.";
    $fifthTitle = "New Item";
  }
  else if($getCata == "life style"){
    $firstTitle = "Most weekly viewed.";
    $secondTitle = "New in aaoaz";
    $thirdTitle = "Most rated.";
    $fourthTitle = "Offer going on.";
    $fifthTitle = "New Product";
  }
  else if($getCata == "education"){
    $firstTitle = "Most monthly viewed.";
    $secondTitle = "New in aaoaz";
    $thirdTitle = "Most rated.";
    $fourthTitle = "Admission going on.";
    $fifthTitle = "Upcoming exam";
  }
  else if($getCata == "health"){
    $firstTitle = "Most monthly viewed.";
    $secondTitle = "New in aaoaz";
    $thirdTitle = "Most rated.";
    $fourthTitle = "Most viewed.";
    $fifthTitle = "24 Hours Service";
  }
  else if($getCata == "electronics"){
    $firstTitle = "Most monthly viewed.";
    $secondTitle = "New in aaoaz";
    $thirdTitle = "Most rated.";
    $fourthTitle = "Most viewed.";
    $fifthTitle = "New Device arrived.";
  }
  else if($getCata == "residence"){
    $firstTitle = "Most monthly viewed.";
    $secondTitle = "New in aaoaz";
    $thirdTitle = "Most rated.";
    $fourthTitle = "Most viewed.";
    $fifthTitle = "Offer";
  }
  else if($getCata == "tourism"){
    $firstTitle = "Most monthly viewed.";
    $secondTitle = "New in aaoaz.";
    $thirdTitle = "Most rated.";
    $fourthTitle = "Most viewed.";
    $fifthTitle = "Random";
  }

?>
<div class="home" id="search_result">
    
 
<!--   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
  <style>

  .jumbotron {
      background-color: #f4511e;
      color: #fff;
      padding: 100px 25px;
      font-family: Montserrat, sans-serif;
  }

  .bg-grey {
      background-color: #f6f6f6;
  }
  .logo-small {
      color: #f4511e;
      font-size: 50px;
  }

  .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
  }
  .thumbnail img {
      width: 100%;
      height: 100%;
      margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
      background-image: none;
      color: #f4511e;
  }
  .carousel-indicators li {
      border-color: #f4511e;
  }
  .carousel-indicators li.active {
      background-color: #f4511e;
  }
  /*.item h4 {
      font-size: 19px;
      line-height: 1.375em;
      font-weight: 400;
      font-style: italic;
      margin: 70px 0;
  }*/
  .item span {
      font-style: normal;
  }
  .panel {
      border: 1px solid #f4511e; 
      border-radius:0 !important;
      transition: box-shadow 0.5s;
  }
  /*.panel:hover {
      box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }*/
  .panel-footer .btn:hover {
      border: 1px solid #f4511e;
      background-color: #fff !important;
      color: #f4511e;
  }
  .panel-heading {
      color: #fff !important;
      background-color: #f4511e !important;
      padding: 25px;
      border-bottom: 1px solid transparent;
      border-top-left-radius: 0px;
      border-top-right-radius: 0px;
      border-bottom-left-radius: 0px;
      border-bottom-right-radius: 0px;
  }
  .panel-footer {
      background-color: white !important;
  }
  .panel-footer h3 {
      font-size: 32px;
  }
  .panel-footer h4 {
      color: #aaa;
      font-size: 14px;
  }
  .panel-footer .btn {
      margin: 15px 0;
      background-color: #f4511e;
      color: #fff;
  }
  .navbar {
      margin-bottom: 0;
      background-color: #f4511e;
      z-index: 9999;
      border: 0;
      font-size: 12px !important;
      line-height: 1.42857143 !important;
      letter-spacing: 4px;
      border-radius: 0;
      font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
      color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
      color: #f4511e !important;
      background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
  }
  footer .glyphicon {
      font-size: 20px;
      margin-bottom: 20px;
      color: #f4511e;
  }
  /*.slideanim {visibility:hidden;}*/
  .slide {
      animation-name: slide;
      -webkit-animation-name: slide;
      animation-duration: 1s;
      -webkit-animation-duration: 1s;
      visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-3 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
        width: 100%;
        margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
        font-size: 150px;
    }
  }
  </style>


<!-- Container (Services Section) -->
<div id="services" class="text-center">
   

    <div class="dx_slider_slide">

      <div class="dx_slider_left">
          <div id="myCarousel-2" class="dx_slider_panel  carousel slide text-center" data-ride="carousel">
              <h5 class="dx_slider_title"><?php echo $secondTitle; ?></h5>
              <div class="carousel-inner dx_slider_row" role="listbox">
                <div class="item active">
                        <div class="row slideanim">
                          <?php
                              getFirstPanel($getCata, $getSub_cata, "second");
                          ?>
                      </div>
                </div>      
              </div>

              <a class="left carousel-control" href="#myCarousel-2" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel-2" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
          </div>


          <div id="myCarousel-4" class="dx_slider_panel  carousel slide text-center" data-ride="carousel">
            <h5 class="dx_slider_title"><?php echo $fourthTitle; ?></h5>
            <div class="carousel-inner dx_slider_row" role="listbox">
              <div class="item active">
                      <div class="row slideanim">
                        <?php
                            getFirstPanel($getCata, $getSub_cata, "fourth");
                        ?>
                    </div>
              </div>      
            </div>

            <a class="left carousel-control" href="#myCarousel-4" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel-4" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
      </div>

       <div class="dx_spec_right">
            <div class="dx_spec_right_first">

                <?php
                  getFirstRightSide($getCata);
                ?>

                

            </div>
        </div>


        <div class="dx_spec_right">
            <div class="dx_spec_right_second">

                <?php
                  getSecondRightSide($getCata);
                ?>
                

            </div>
        </div>


    </div>


    <div class="peoples_review_panel">
        <div id="myCarousel-5" class="dx_slider_panel  dx_people_co_panel carousel slide text-center" data-ride="carousel">
          <div class="peoples_review_slide_inside">
              <h5 class="dx_slider_title">People's Opinion</h5>
              <div class="carousel-inner dx_slider_row" role="listbox">
                        <?php
                            getPeoplesOpinion($getCata);
                        ?>
              </div>

              <a class="left carousel-control" href="#myCarousel-5" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel-5" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
          </div>
        </div>
    </div>







    <div class="dx_slider_slide">
        <div class="dx_slider_left">
            <div id="myCarousel-1" class="dx_slider_panel  carousel slide text-center" data-ride="carousel">
              <h5 class="dx_slider_title"><?php echo $firstTitle; ?></h5>
              <div class="carousel-inner dx_slider_row" role="listbox">
                <div class="item active">
                        <div class="row slideanim">
                          <?php
                              getFirstPanel($getCata, $getSub_cata, "first");
                          ?>
                      </div>
                </div>      
              </div>

              <a class="left carousel-control" href="#myCarousel-1" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel-1" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>







            <div id="myCarousel-3" class="dx_slider_panel  carousel slide text-center" data-ride="carousel">
              <h5 class="dx_slider_title"><?php echo $thirdTitle; ?></h5>
              <div class="carousel-inner dx_slider_row" role="listbox">
                <div class="item active">
                        <div class="row slideanim">
                          <?php
                              getFirstPanel($getCata, $getSub_cata, "third");
                          ?>
                      </div>
                </div>      
              </div>

              <a class="left carousel-control" href="#myCarousel-3" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel-3" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
        </div>
    </div>






</div>




<script>
$(document).ready(function(){

  // #myCarousel1




  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>

<div class="clearfix"></div>

  
</div>