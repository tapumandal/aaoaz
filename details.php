<!DOCTYPE html>
<head>
 <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <script src="js/jquery.min.js"></script>
  <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> -->

  	<!-- FOR POP UP DIV -->
	  	<!-- <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script> -->
		<!-- // <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> -->
  	<!-- FOR POP UP DIV -->
<title></title>
</head>

<body>

<div class = "container">

	<div class = "main-top">
		<div class="main">
				<?php
					
					include 'header.php';
					
				?>
		</div>
	</div>

	<div class = "content">
		<div class="container-fluid">
		   <div class="row">
		   		<div class="col-sm-10 field" >
		   		 <div class="col-sm-12" >
			   		<div class="col-sm-6 first" >
			   			<div class="button-list">
			   				
							   		<nav class="navbar navbar-inverse">
					                
					                 
					                    <ul class="nav navbar-nav">
					                      <li><a href="#">Compare</a></li>
					                      <li><a href="#">Remember</a></li>
					                      <li><a href="#jump">Comment</a></li>
					                      <li><a href="#jump">Ask Question</a></li>
					                      <li><a href="#">Update Info</a></li>

					                    </ul>
					             </nav>
			   			</div>
			   			<div class="info-field">
			   			
				   				<div class="info-area">
				   					<span>Name &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </span>
				   					<div class="info-ans">
				   						<span>Star Kabab and Resaurent </span>
				   					</div>
				   				</div>


					   			<div class="review info-area">
					   				<span>Review :</span> 
					   				 <div class="rev-pos">
						   				<i class="review-img" >
						   					<i class="review-img2" style="width:50%"></i>
						   				</i>
						   			</div>

					   			
							   		<div class="graph">
							   			<img class="img-responsive gp" src="image/graph.png">
							   			<img class="img-responsive level" src="image/down.png">
							   		</div>
							   	</div>


				   			<div class="food-type info-area">
				   				<span>Food Type : </span>
				   				<div class="info-ans">
				   					 <span>Kabab | Bengali Food | Bekari</span>
				   				</div>
				   			</div> 
				   			<div class="discription info-area">
				   				<span>Discription : </span>
				   				<div class = "info-ans">
					   				 <span>Average cost Restaurent with all types of bengali food. Alongside Bekari is also renowned. It has over 70 items of food. Morning, noon and Evening food are different.
					   				 </span>
				   				</div>

				   			</div>
				   			<div class="brand info-area">
				   				<span>Brand &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </span>
				   				<div class="info-ans">
				   					<span>International </span>
				   				</div>
				   				
				   			</div>
				   			<div class="spetiality info-area">
				   				<span>Speciality&nbsp&nbsp: </span>
				   				<div class="info-ans">
				   					<span>Kitchen access open Kitchen access open Kitchen access open Kitchen access open
				   					</span>	
				   				</div>
				   			</div>
				   			<div class="info-area">
				   				<span>Offer&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:</span>
				   				<div class="info-ans">
				   					<span>10% Less at Eid</span>
				   				</div>
				   			</div>
				   			
				   			<div class="info-area">
				   				<span>Opening H: </span>
				   				<div class="info-ans">
				   					<span>7am-11pm. Closing day Monday</span>
				   				</div>
				   			</div>
				   			<div class="info-area">
				   				<span>Web Site&nbsp&nbsp&nbsp: </span>
				   				<div class="info-ans">
				   					<span><a href="#">starkabak.com</a></span>
				   				</div>
				   			</div>
				   			<div class="info-area">
				   				<span>Social link&nbsp: </span>
				   				<div class="info-ans">
				   					<span>facebook.com/star.kabab</span>
				   					<br>
				   					<span>gmail.com/star.kabab</span>
				   				</div>
				   			</div>

				   			<div class="info-area">
				   				<span>Location&nbsp&nbsp&nbsp: </span>
				   				<div class="info-ans">
				   					<span>Dhanmondi, Science-Lab</span>
				   				</div>
				   			</div>

				   			<div class="map-tab">
				   			  <ul class="nav nav-tabs tab-pos">
							  	 	<li ><a data-toggle="tab" href="#show">Show Map</a></li>
							  	 	<li class="active"><a data-toggle="tab" href="#blank">Hide Map</a></li>
							  </ul>

							   <div class="tab-content">
							   		<div id="blank" class="tab-pane fade">
								     
								    </div>

								    <div id="show" class="tab-pane fade">
								     	<div class="map">
							   				<iframe width="100%" height="250" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1433590604982"  frameborder="0" style="border:0">
							   				</iframe>
								   		</div>
								    </div>
								    
								</div>
							</div>
				   			

				   		</div>
			   		</div>

			   		<div class="col-sm-6 second" >
			   			<div class="col-sm-12" >
			   				<div >
				   				<?php 
				   					include 'slide-show.php';

				   				 ?>

				   			</div>
				   		</div>

				   		<div class="col-sm-12" >
			   				<div class="menu">
				   				<h4>Item List</h4>
				   				
				   				<iframe src="item-list.php" width="100%" height="344px"></iframe>
				   			</div>
				   		</div>
				   		
			   		</div>
			   	</div>

			   		<div class="col-sm-6 comment" id="jump" >
			   			<div >
			   				<h3>Public Comment</h3>
			   				<div class="input-field">
			   						<form action="#url">
										<input class="txt" type="text" name="content" placeholder="Enter your Openion">
										<input class="but" type="submit" value="POST">
									</form>
			   				</div>
			   				<div class="com">

			   					<img class="img-responsive img-circle" src="image/gta.png"></img>

			   					<span class="name">Name Unknown</span>

			   					<div class="com-rev">
			   						<span>Review : </span>
			   					</div>

			   					<div class="opinion">
			   						<span class="op">Food are really good. Price is perfect. But behaviour of waiter is dicreasing </span> 
			   					</div>


			   				</div>

			   				<div  class="com">

			   					<img class="img-responsive img-circle" src="image/girl.png"></img>

			   					<span class="name">Name Unknown</span>

			   					<div class="com-rev">
			   						<span>Review : </span>
			   					</div>

			   					<div class="opinion">
			   						<span class="op">Food are really good. Price is perfect. But behaviour of waiter is dicreasing Food are really good. Price is perfect. But behaviour of waiter is dicreasing Food are really good. Price is perfect. But behaviour of waiter is dicreasing </span> 
			   					</div>


			   				</div>
			   			</div>
		   			</div>

		   			<div class="col-sm-6 question" >
			   			

			   			<div >
			   				<h3>Question & Answar</h3>
			   				<div class="input-field">
			   						<form action="#url">
										<input class="txt" type="text" name="content" placeholder="Enter your Openion">
										<input class="but" type="submit" value="POST">
									</form>
			   				</div>
			   				<div class="com">

			   					<img class="img-responsive img-circle" src="image/girl.png"></img>

			   					<span class="name">Name Unknown</span>

			   					<div class="com-rev">
			   						<span>Review : </span>
			   					</div>

			   					<div class="opinion">
			   						<span class="op">Food are really good. Price is perfect. But behaviour of waiter is dicreasing </span> 
			   					</div>


			   				</div>

			   				<div  class="com">

			   					<img class="img-responsive img-circle" src="image/gta.png"></img>

			   					<span class="name">Name Unknown</span>

			   					<div class="com-rev">
			   						<span>Review : </span>
			   					</div>

			   					<div class="opinion">
			   						<span class="op">Food are really good. Price is perfect. But behaviour of waiter is dicreasing Food are really good. Price is perfect. But behaviour of waiter is dicreasing Food are really good. Price is perfect. But behaviour of waiter is dicreasing </span> 
			   					</div>


			   				</div>
			   			</div>
		   			</div>


		   		</div>


		   		<div class="col-sm-2 third" >
		   			<div >
		   				<h1>Third Area</h1>
		   			</div>
		   		</div>

		   </div>
		</div>
	</div>

</div>
<!-- END OF container -->
</body>
</html>