

<?php
$previous_page = $_SERVER['HTTP_REFERER'];
$current_page = $_SERVER['REQUEST_URI'];

?>
<div class="login_main">
	<div class="login_main_inside">

		<div class="log_in">

			<div class="login_close">
				<a href="javascript:void(0)" class="closebtn sideNavClose" onclick="closeLogin()">&times;</a>
			</div>

			<h2>Get Connected</h2>
			<div class="log_input">


				<form method="POST" action="log_return.php">

					<input class="box" type="email" name="email" placeholder="Enter Email" required>
					<input class="box" type="password" name="pass" placeholder="Password" required>
					<input type="hidden" name="page" value="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']) ?>">
					<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
					<input class="but" type="submit" value="SIGN IN">
				</form>

				<a class="forget_pass" href="./recover">Forget password?</a>
				<div class="facebook_login">
					<div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"></div>
					<a href="./fblogin.php">LOGIN</a>
				</div>

				<!-- <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"> -->
				</div>
			</div>

			<div class="sign_up">
				<h2>Be a part of aaoaz</h2>
				<div class="sign_input">
					<form method="POST" action="signup">
								<input class="box" type="text" name="name" placeholder="Full Name" required>
								<input class="box" type="email" name="email" placeholder="Email" required>
								<input class="box" type="password" name="pass" placeholder="Password" required>
								<input type="radio" name="sex" value="male" required>Male
								<input type="radio" name="sex" value="female">Female
								<br>
								<input type="hidden" name="page" value="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']); ?>">
								<input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>">
								<input class="but" type="submit" value="SIGN UP">
							</form>
				</div>
			</div>
		</div>

	</div>
