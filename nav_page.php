 <script type="text/javascript">
 	 $(document).ready(function(){
 	 	var winHeight = $( window ).height();
 		var tmpHeight = winHeight- $(".header").height();

 			
 		var nav_page_off = $(".nav_page").offset().top;
 		tmpHeight = winHeight - nav_page_off;
 		$(".nav_page").css("cssText", "height:"+tmpHeight+"px;");

 		// var navTitleH = $(".nav_title").height();
 		var nav_pageW = $(".nav_page").width();
		var nav_pageH = $(".nav_page").height();

		// alert(nav_pageW+" "+nav_pageH);
		nav_pageH = nav_pageW/2
		nav_pageH += 20;
		$(".nav_page").css("cssText", "height:"+nav_pageH+"px;");


 		// var navHeight1 = $(".nav_one").height() - navTitleH;
 		var navHeight1 = $(".nav_one").height();
 		$(".food_nav img").css("cssText", "height:"+navHeight1+"px !important;");

 		// var navHeight2 = $(".nav_two").height() - navTitleH;
 		var navHeight2 = $(".nav_two").height();
 		$(".life_style_nav img").css("cssText", "height:"+navHeight2+"px !important;");

 		// var navHeight3 = $(".nav_three").height() - navTitleH;
 		var navHeight3 = $(".nav_three").height();
 		$(".education_nav img").css("cssText", "height:"+navHeight3+"px !important;");

 		// var navHeight4 = $(".nav_four").height() - navTitleH;
 		var navHeight4 = $(".nav_four").height();
 		$(".health_nav img").css("cssText", "height:"+navHeight4+"px !important;");

 		// var navHeight5 = $(".nav_five").height() - navTitleH;
 		var navHeight5 = $(".nav_five").height();
 		$(".summary_nav img").css("cssText", "height:"+navHeight5+"px !important;");

 		// var navHeight6 = $(".nav_six").height() - navTitleH;
 		var navHeight6 = $(".nav_six").height();
 		$(".residence_nav img").css("cssText", "height:"+navHeight6+"px !important;");

 		// var navHeight7 = $(".nav_seven").height() - navTitleH;
 		var navHeight7 = $(".nav_seven").height();
 		$(".tourism_nav img").css("cssText", "height:"+navHeight7+"px !important;");

 		// var navHeight8 = $(".nav_eight").height() - navTitleH;
 		var navHeight8 = $(".nav_eight").height();
 		$(".electronics_nav img").css("cssText", "height:"+navHeight8+"px !important;");

 		
 		

 	 });
 </script>
<div class="nav_total_page">
<div class="nav_page ">
	<div class="home_nav_left">
		<div class="nav_one ">
			<div class="food_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Hungry?<br>Check out the best</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/food.jpg" tab="aaoaz food restaurant"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><a href="food"><h2>Food</h2></a></div>

					 		<div class="sub_nav_list col-sm-12">

					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="restaurant"><img class="img-responsive" src="image/default_image/nav/sub_nav/restaurant.png" tab="Restaurant"><h4>Restaurant</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="cafe"><img class="img-responsive" src="image/default_image/nav/sub_nav/cafe.png" tab="Cafe"><h4>Cafe</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="cart"><img class="img-responsive" src="image/default_image/nav/sub_nav/cart.png" tab="Cart"><h4>Cart</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="sweets"><img class="img-responsive" src="image/default_image/nav/sub_nav/sweets.png" tab="Sweets"><h4>Sweets</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="cake_pastry"><img class="img-responsive" src="image/default_image/nav/sub_nav/cake_pastry.png" tab="Cake & Pastry"><h4>Cake & Pastry</h4></a>
					 				</div>
					 			</div>
					 			
					 		</div>
				 	</div>
				 </div>
			</div>
		</div>

		<div class="nav_two ">
			
			<div class="life_style_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Fashion freak?<br>Find out what suits you</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/life style.jpg" tab="aaoaz Life Style"></div>
				 	</div>

						 		
				 			<div class="nav_list_inside">
						 		<div class="nav_title"><a href="lifestyle"><h2>Life Style</h2></a></div>

						 		<div class="sub_nav_list col-sm-12">

						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="fashion"><img class="img-responsive" src="image/default_image/nav/sub_nav/fashion.png" tab="fashion"><h4>fashion</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="accessories"><img class="img-responsive" src="image/default_image/nav/sub_nav/accessories.png" tab="accessories"><h4>accessories</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="parlor_salon"><img class="img-responsive" src="image/default_image/nav/sub_nav/parlor_salon.png" tab="Parlor & Salon"><h4>Parlor & Salon</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="gym_fitness"><img class="img-responsive" src="image/default_image/nav/sub_nav/gym_fitness.png" tab="Gym & Fitness"><h4>Gym & Fitness</h4></a>
						 				</div>
						 			</div>
						 			
						 		</div>
						 	</div>

				 </div>
			</div>
		</div>

		<div class="nav_three ">
			<div class="education_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Worried about Institution?<br>Inquire for the Best</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/education.jpg" tab="aaoaz education"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><a href="education"><h2>Education</h2></a></div>

						 		<div class="sub_nav_list col-sm-12">

						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="university"><img class="img-responsive" src="image/default_image/nav/sub_nav/university.png" tab="University"><h4>University</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="college"><img class="img-responsive" src="image/default_image/nav/sub_nav/college.png" tab="College"><h4>College</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="coaching"><img class="img-responsive" src="image/default_image/nav/sub_nav/coaching.png" tab="Coaching"><h4>Coaching</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="exam_result"><img class="img-responsive" src="image/default_image/nav/sub_nav/exam_result.png" tab="Exam & Result"><h4>Exam & Result</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="admission"><img class="img-responsive" src="image/default_image/nav/sub_nav/admission.png" tab="Admission"><h4>Admission</h4></a>
						 				</div>
						 			</div>
						 			
						 		</div>
				 	</div>
				 </div>
			</div>
		</div>

		

		<div class="nav_five ">
			<div class="summary_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Information Verification Reliability</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/summary.jpg"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><h2>What to do in aaoaz?</h2></div>

				 		<div class="sub_nav_list">
				 			
				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
		<div class="clearfix"> </div>
		<div class="nav_six ">
			<div class="residence_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Looking for a place to stay?<br>Compare to reach your demand</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/residence.jpg" tab="aaoaz residence hotel"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><a href="residence"><h2>Residence</h2></a></div>

							 	<div class="sub_nav_list col-sm-12">

						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="hotel"><img class="img-responsive" src="image/default_image/nav/sub_nav/hotel.png" tab="Hotel"><h4>Hotel</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="resort"><img class="img-responsive" src="image/default_image/nav/sub_nav/resort.png" tab="Resort"><h4>Resort</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="tolet"><img class="img-responsive" src="image/default_image/nav/sub_nav/to-let.png" tab="To Let"><h4>To Let</h4></a>
						 				</div>
						 			</div>
						 			
						 			
						 		</div>
				 	</div>
				 </div>
			</div>
		</div>

		<div class="nav_seven ">
			<div class="tourism_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Stressed ? Need a tour?<br>Prepare you plan</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/tourism.jpg" tab="aaoaz tourism"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><a href="tourism"><h2>Tourism</h2></a></div>

							 	<div class="sub_nav_list col-sm-12">

						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="natural_site"><img class="img-responsive" src="image/default_image/nav/sub_nav/natural_site.png" tab="Natural Site"><h4>Natural Site</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="heritage"><img class="img-responsive" src="image/default_image/nav/sub_nav/heritage.png" tab="Heritage"><h4>Heritage</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="picnic_spot"><img class="img-responsive" src="image/default_image/nav/sub_nav/picnic_spot.png" tab="Picnic Spot"><h4>Picnic Spot</h4></a>
						 				</div>
						 			</div>
						 			
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="lake_park"><img class="img-responsive" src="image/default_image/nav/sub_nav/lake_park.png" tab="Lake & Park"><h4>Lake & Park</h4></a>
						 				</div>
						 			</div>
						 			
						 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</div>


	<div class="home_nav_right">
		<div class="nav_four">
			<div class="health_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Feeling sick,<br> Where to go?</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/health.jpg" tab="aaoaz health"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><a href="health"><h2>Health</h2></a></div>

							 	<div class="sub_nav_list col-sm-12">

						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="hospital_clinic"><img class="img-responsive" src="image/default_image/nav/sub_nav/hospital_clinic.png" tab="Hospital Clinic"><h4>Hospital Clinic</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="doctor"><img class="img-responsive" src="image/default_image/nav/sub_nav/doctor.png" tab="Doctor"><h4>Doctor</h4></a>
						 				</div>
						 			</div>
						 			<div class="sub_nav_list_inside ">
						 				<div class="per_sub_nav">
						 					<a href="ambulance"><img class="img-responsive" src="image/default_image/nav/sub_nav/ambulance.png" tab="Ambulance"><h4>Ambulance</h4></a>
						 				</div>
						 			</div>
						 			
						 			
						 		</div>
				 	</div>
				 </div>
			</div>
		</div>

		<!-- <div class="nav_eight ">
			<div class="electronics_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Need a device?<br>Compare before you take</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/electronics.jpg"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><a href="h2>Electronics</h2></a></div>

						 	<div class="sub_nav_list col-sm-12">

					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="computer"><img class="img-responsive" src="image/default_image/nav/sub_nav/computer.png" tab=""><h4>Computer</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="computer components"><img class="img-responsive" src="image/default_image/nav/sub_nav/computer components.png" tab=""><h4>Computer Components</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="mobile"><img class="img-responsive" src="image/default_image/nav/sub_nav/mobile.png" tab=""><h4>Mobile</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="gadget"><img class="img-responsive" src="image/default_image/nav/sub_nav/gadget.png" tab=""><h4>Gadget</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="sound system"><img class="img-responsive" src="image/default_image/nav/sub_nav/sound_system.png" tab=""><h4>sound system</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="camera"><img class="img-responsive" src="image/default_image/nav/sub_nav/camera.png" tab=""><h4>Camera</h4></a>
					 				</div>
					 			</div>
					 			
					 		</div>
				 	</div>
				 </div>
			</div>
		</div> -->
		<div class="nav_eight ">
			<div class="ecommerce_nav">
				 
				 <div class="nav_list">
				 	<div class="nav_list_line"></div>
				 	<div class="nav_list_cover">
				 		<div class="nav_text"><h3>Wanna Shopping at home?<br>Find the reliable one</h3></div>
				 		<div class="nav_image"><img class="img-responsive" src="image/default_image/nav/ecommerce.jpg" tab="aaoaz ecommerce"></div>
				 	</div>

				 	<div class="nav_list_inside">
				 		<div class="nav_title"><a href="ecommerce"><h2>E-Commerce</h2></a></div>

						 	<div class="sub_nav_list col-sm-12">

					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="health_beauty"><img class="img-responsive" src="image/default_image/nav/sub_nav/computer.png" tab="Health & beauty"><h4>Health & beauty</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="efashion"><img class="img-responsive" src="image/default_image/nav/sub_nav/computer components.png" tab="Fashion"><h4>Fashion</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="food_grocery"><img class="img-responsive" src="image/default_image/nav/sub_nav/mobile.png" tab="Food & Grocery"><h4>Food & Grocery</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="electronics_gadget"><img class="img-responsive" src="image/default_image/nav/sub_nav/gadget.png" tab="Electronics & Gadget"><h4>Electronics & Gadget</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="gift_craft"><img class="img-responsive" src="image/default_image/nav/sub_nav/sound_system.png" tab="Gift & Craft"><h4>Gift & Craft</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="buy_sell"><img class="img-responsive" src="image/default_image/nav/sub_nav/camera.png" tab="Buy & Sell"><h4>Buy & Sell</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="job"><img class="img-responsive" src="image/default_image/nav/sub_nav/camera.png" tab="Job"><h4>Job</h4></a>
					 				</div>
					 			</div>
					 			<div class="sub_nav_list_inside ">
					 				<div class="per_sub_nav">
					 					<a href="others"><img class="img-responsive" src="image/default_image/nav/sub_nav/camera.png" tab="Others"><h4>Others</h4></a>
					 				</div>
					 			</div>
					 			
								
								
								
								
								
								
					 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</div>
</div>
</div>