<?php
session_start();
include 'operation/url_operation.php';
?>
<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">

<head>
	<meta charset="utf-8">
	<title><?php echo $_SESSION[content_name].' | '.$_SESSION[content_type].' | '.$_SESSION[all_location]; ?></title>
	<meta charset="utf-8">
	<meta name="description" content="<?php getFbSingleDescription($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);  ?> Looking for <?php echo $_SESSION[content_name].' in '.$_SESSION[all_location]; ?>. Here is all information and public review/rating. aaoaz provide basic information, images, item or product, contact, user comment of the services or company you are asking for">
	<meta name="keywords" content="<?php echo "aaoaz.com contact, product, item, address, location  information, ". $_SESSION[content_name].', '.$_SESSION[all_location].', '.$_SESSION[content_type].', '.$_SESSION[cata].', '.$_SESSION[sub_cata]; ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php
	
		//getFbSingleImg($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]); 
		$fetch_image_data = "SELECT image_link FROM `general_image` WHERE cata_id = '$_SESSION[content_id]' AND cata= '$_SESSION[cata]' LIMIT 1";
		$data= $conn->query($fetch_image_data);
		if($data->num_rows>0){
			while ($row=$data->fetch_assoc()) {

				$image_link = $row[image_link];
				echo '<meta property="og:image" content="http://aaoaz.com/image/main_image/'.$image_link.'"/>';
				

			}
		}
		
	?>
	
<script language="javascript" type="text/javascript">

		$(document).ready(function(){
		    $("#comment_option").val($("#comment_option option:first").val()); //auto select the first option in select tag in comment section
		});



		function resizeIframe(obj) {
			var offsetHeight = document.getElementById('info_height').offsetHeight;
	  	//document.getElementById("frame_height").innerHTML = offsetHeight;
	  	//window.alert(offsetHeight);
	  	offsetHeight = offsetHeight -25;
	  	obj.style.height = offsetHeight + 'px';
	   //obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
	}
</script>
</head>

<body>

	<div class = "container electronic">
		<div class="overlay-bg"> </div> <!-- FOR BLUR -->

		<div class="overlay-content popup54441" >
			<!-- <button class="close-btn">X</button> -->
			<div ID="overlay_content">
				
			</div>
		</div>

		<!-- OVERLAY -->	



		<div class = "main-top">
			<div class="main">
				<?php
				
				include 'header.php';
				
				?>
			</div>
		</div>




		<div class="col-sm-12 field wide_pattern">
			<div class="col-sm-10">  <!-- Whole information without add -->

				<div class="col-sm-12 first">    <!-- FOR NAME -->
					<div class="info_box">
					<!-- <div class="title">
						<span>Name</span>
					</div> -->
					<div class="title_info">
						<span>
							<?php
							$general_obj = new general();
							$general_obj->general_name($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
							?>
						</span>
					</div>
				</div>
			</div><!-- FOR NAME -->


			
			<!-- Item List LIST -->
			<div class="col-sm-12  elec_pro_list" >
				<!-- <iframe src="item-list/elec_pro_list.php"></iframe> -->
				<!-- <div  style="background:transparent;" > -->
				<!-- </div> -->
				<div class="elec_pro_filter col-sm-1">
					<div class="elec_filter_list">
						<div class="price_range">
							<select onchange="elecPriceProFilter('<?php echo $_SESSION[cata]; ?>', '<?php echo $_SESSION[sub_cata]; ?>', '<?php echo $_SESSION[content_id]; ?>', this)">
								
								<option value="">Price Range</option>
								<option value="0-2000">0-2000</option>
								<option value="2000-5000">2000-5000</option>
								<a>	<option value="5000-10000">5000-100000</option></a>
							</select>	
						</div>

						<div class="product_type">
			    			<!-- <a href="#0" onclick="elecProFilter('mobile', '<?php echo $_SESSION[cata]; ?>', '<?php echo $_SESSION[sub_cata]; ?>', '<?php echo $_SESSION[content_id]; ?>')">mobile</a>
			    			<a href="#0" onclick="elecProFilter('computer', '<?php echo $_SESSION[cata]; ?>', '<?php echo $_SESSION[sub_cata]; ?>', '<?php echo $_SESSION[content_id]; ?>')">computer</a>
			    			<a href="#0" onclick="elecProFilter('laptop', '<?php echo $_SESSION[cata]; ?>', '<?php echo $_SESSION[sub_cata]; ?>', '<?php echo $_SESSION[content_id]; ?>')">laptop</a> -->
			    			<?php
			    			$general_obj = new general();
			    			$general_obj->elecFilterList($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
			    			?>
			    		</div>
			    	</div>	
			    </div>

			    <div class="list col-sm-11">
			    	<div id="allElecItem">
			    		<?php
			    		$general_obj = new general();
			    		$general_obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], $table_pattern_top, $table_pattern_bottom);
			    		?>
			    	</div>
			    </div>

			    
			</div>
			<!-- Item LIST -->
			
			<div class="col-sm-12"><!-- NORMAL INFORMATION With arrival soon  -->
				<div class="col-sm-8 first" ID="info_height">  <!-- NORMAL INFORMATION  -->
					
					<?php
					$general_obj = new general();
					$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
					$general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
					$general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);				
					?>
				   	<!-- <div class="info_box2">
						<div class="title2">
							<span>HELP LINE</span>
						</div>
						<div class="title_info2">
							<span ><u>Office :</u> 01712345678
								<br>
								<u>Servicing Center :</u> 015464534
							</span>

						</div>
					</div>

					<div class="info_box2">
						<div class="title2">
							<span>WARRANTY POLICY</span>
						</div>
						<div class="title_info2">
							<span >Details about the warranty system
							</span>

						</div>
					</div>

					<div class="info_box2">
						<div class="title2">
							<span>Web Site & Social Links</span>
						</div>
						<div class="title_info2" >
							<span ><a href="www.institution.com">www.institution.com</a>
								<br>
								<u><b>Facebook</b></u><br>
								<span>
									Facebook Page Link
									<br>
									Facebook group Link
									<br>
								</span>
							</span>
						</div>
					</div>

					<div class="info_box2">
						<div class="title2">
							<span>LOCATION</span>
						</div>
						<div class="title_info2">
							<span >IDB vabon Agargao taltola
							</span>

						</div>
					</div> -->
					
					
				</div>   <!-- NORMAL INFORMATION  -->



				<div class="col-sm-4 ">
					<div class="new_arrival">
						<div class="new_title">
							<span>New Arrival</span>
						</div>
						<div class="arrival">
							<table>
								<thead>
									<tr>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php
									$general_obj = new general();
									$general_obj->general_time_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], "new");
									?>
									

								</table>
							</div>
						</div>

						<div class="new_arrival">
							<div class="new_title">
								<span> Coming SooN</span>
							</div>
							<div class="arrival">
								<table>
									<thead>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php
										$general_obj = new general();
										$general_obj->general_time_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], "coming");
										?>
										
									</table>
								</div>
							</div>
							
						</div><!-- NORMAL INFORMATION  -->

			   <!-- <div class="onoffswitch">
				    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
				    <label class="onoffswitch-label" for="myonoffswitch">
				        <span class="onoffswitch-inner"></span>
				        <span class="onoffswitch-switch"></span>
				    </label>
				</div> -->

				<!-- <div ID="myonoffswitch">
					<span>Tapu Mandal</span>
				</div> -->


			</div><!-- NORMAL INFORMATION  Arrival soon -->
			<!-- MAP TAB -->
							   		<!-- <div class="col-sm-6" >  
						   					<div class="map-tab">
									   			  <ul class="nav nav-tabs tab-pos">
												  	 	<li ><a data-toggle="tab" href="#show">Show Map</a></li>
												  	 	<li class="active"><a data-toggle="tab" href="#blank">Hide Map</a></li>
												  </ul>

											   <div class="tab-content">
											   		<div id="blank" class="tab-pane fade">
												    </div>

												    <div id="show" class="tab-pane fade">
												     	<div class="map">
											   				<iframe width="100%" height="250" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1433590604982"  frameborder="0" style="border:0">
											   				</iframe>
												   		</div>
												    </div>
												    
												</div>
										    </div>
										</div>	   		 -->   
										<!-- MAP TAB -->

										<div class="clearfix"></div>

										<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
											<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->
												<h3>Public COMMENT || QUESTION || REVIEW</h3>

												<?php
												include 'comment_3.php';
												?>
												

												<div class="com">
													<?php
													$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
													?>
												</div>

											</div>   <!-- COMMENT  -->
										</div>   <!-- COMMENT AND QUESTIOIN -->


									</div> <!-- Whole information without add -->


									<!-- add -->
									<div class="col-sm-2 third">  
										<div class="inside_top_right_panel">
											<div class="inside_top_head">
												<h3>Recomended</h3>
											</div>
											<div class="insider_box_panel">

												<?php
												include 'operation/suggestion.php';
												$obj = new suggestion();
												$obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
												?>
												
											</div>
										</div>
									</div>
								</div>



							</div>
							<!-- END OF container -->
							<div class="footer">
								<?php
								include 'operation/page_view.php';

								$vObj = new view();

								$vObj->everyView($_SESSION[content_name]);

								$vObj->startView();
								?>	
							</div>

<?php
	include 'footer.php';
?>


</body>
</html>