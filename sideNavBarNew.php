<link href="https://fonts.googleapis.com/css?family=Arapey|Bellefair|Cinzel+Decorative|Cormorant|Kameron|Old+Standard+TT|Oranienbaum|Playfair+Display+SC|Pompiere|Prata|Radley" rel="stylesheet">

<style type="text/css">
.sidenav {
    height: 100%;
    width: 240px;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: white;
    overflow-x: scroll;
    transition: 0.5s;
    text-align:right;
    box-shadow: 5px 5px 10px rgb(100,100,100);
    box-shadow: 2px 1px 10px grey;
    z-index: 100;

    left: -170px;
    overflow: hidden;
}

#MainMenu{
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: -17px; /* Increase/Decrease this value for cross-browser compatibility */
  overflow-y: scroll;
}

.sidenav a {
    text-decoration: none;
    font-size: 22px;
    display: block;
    transition: 0.3s;
    background: transparent;
    border-right: none;
    color: rgb(50,50,50)!important;


    font-family: 'Arapey', serif;
    text-align: center;


}

a.sideNavClose{
  padding: 0px;
  padding-right: 5px;
  color: #dedddd !important;
  font-size: 40px !important;
  text-decoration: none !important;
}

.list-group-item{
  border:0px !important;
  padding-right: 10px !important;
}
a.sub_cata_a_tag{
  padding: 5px;
  font-size: 18px;
  background: rgb(200,200,200);
  text-align: right;
  padding-right: 83px;
}

.side_nav_panel{
  border: none !important;
}




@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


/*.side_nav_link{
  float: right;
  width: 100%;
}*/
.side_nav_link a{
  float: left;
  width: 170px;
  text-align: right;
  right: 00px;
}

@media screen and (max-width: 1400px){
    .side_nav_link a{
      width: 165px;
    }

    .styled-select select{
      padding-right: 70px;
    }
}

.side_nav_link a img{
  height: 40px;
  width: : 40px;
  float: right;
  margin-top: -3px;
}


.list-group-item{
  padding:10px 20px;
  border-radius: 0px !important;
}


.side_nav_images{
  position: relative;
  float: right;
  width: 40px;
  margin-right: 20px;
  margin-left: 10px;

}
img.icon_image{
  margin-top: 6px;
  opacity: 1;
  right: 0;
  position: absolute;
  -webkit-transition: opacity .5s; /* Safari */
    transition: opacity .5s;
}
img.down_image{
  margin-top: 6px;
  position: absolute;
  right: 0;
  height: 40px;
  width: 40px;
  opacity: 0;
  -webkit-transition: opacity .5s; /* Safari */
    transition: opacity .5s;
    cursor: pointer;
}

.side_nav_link:hover img.down_image{
  opacity: 1;
}
.side_nav_link:hover img.icon_image{
  opacity: 0;

}

.list-group{
  margin-bottom: 0px !important;
}

.side_nav_panel{
  margin-bottom: 0px !important;
}

.side_nav_inner:hover, .side_nav_inner:focus{
  background: #d0e9c6;
}

a.list-group-item-success:hover, a.list-group-item-success:focus{
  background: transparent !important;
}

.side_nav_inner{
  float: left;
  width: 100%;
  margin-top: 1px;
}

.my_collapse{
  float: left;
  width: 100%;
  margin-top: 1px;
}

#demo1, #demo2, #demo3, #demo4, #demo5, #demo6, #demo7{
  max-height: 0px;
  float: left;
  width: 100%;
  overflow: hidden;
  -webkit-transition: max-height 1s;
  transition: max-height 1s;
}

.side_location{
  float: left;
  width: 150px !important;
  text-align: right;
  right: -500px;
  position: absolute;
  right: 16px;
}

/*.styled-select {
   height: 29px;
   overflow: hidden;
   width: 240px;
}
*/

.styled-select select {
   background: transparent;
   border: none;
   font-size: 22px;
   height: 29px;
   padding: 5px; /* If you add too much padding here, the options won't show in IE */
   width: 250px !important;
   text-align-last:right;
   padding-right: 65px;
   padding-top: 10px;
   height: 52px;
   font-family: 'Arapey', serif;

   border-radius: 0px;
   -moz-appearance: none;
     -webkit-appearance:none;
     cursor: pointer;
     text-transform: capitalize;
}

.side_nav_close a{
  color: black !important;
  padding-right: 100px;
}


.select_area{

<?php
if (isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] != "ANYWHERE") {
	echo 'background: url(./image/default_image/nav/side_nav/location.png) no-repeat;';
} else {
	echo 'background: url(./image/default_image/nav/side_nav/location_black.png) no-repeat;';
}
?>
  background-position: right 16px bottom 5px;
  height: 52px;
  width: 230px;
  width: 100%;
}

.styled-select select::-ms-expand {
    display: none;
}
</style>


<div id="mySidenav" class="sidenav">


      <div id="MainMenu">

          <div class="side_nav_close mobile_view">
            <a href="javascript:void(0)" class="closebtn sideNavClose" onclick="closeSideNav()">&times;</a>
          </div>

        <div class="list-group side_nav_panel">


            <div class="side_nav_link">
                <div class="side_nav_inner">

                    <div class="select_top_border"></div>

                    <div class="styled-select select_area">

                        <select class="form-control side_location" name="location_filter" id="location_filter" onchange="loactionFilter()">
                            <?php
if (isset($_SESSION[locationFilter]) && $_SESSION[locationFilter] == "ANYWHERE") {
	echo '<option value="ANYWHERE">Bangladesh</option>';
} else if (isset($_SESSION[locationFilter])) {
	echo '<option value="' . $_SESSION[locationFilter] . '">' . $_SESSION[locationFilter] . '</option>';
}

echo '<optgroup label="Select a Place">';
echo '<option value="ANYWHERE">Bangladesh</option>';
echo '<optgroup>';
?>
                          </option>


                          <?php
// include 'code_php.php';
getLocationList();
?>
                        </select>
                    </div>

                    <!-- <div class="side_nav_images">
                            <img class="img-responsive" src="image/default_image/nav/side_nav/location.png">
                    </div> -->
                </div>
            </div>


            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="./" class="list-group-item list-group-item-success" data-parent="#MainMenu">Home</a>
                    <div class="side_nav_images">
                            <!-- <a href="./"> -->
                                <img href="./" style="margin-top: 6px;" class="img-responsive" src="image/default_image/nav/side_nav/home.png">
                            <!-- </a> -->
                        </div>
                </div>
            </div>



            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="/food" class="list-group-item list-group-item-success"  data-parent="#MainMenu">Food </a>
                    <div class="side_nav_images">
                            <img class="icon_image img-responsive" src="image/default_image/nav/side_nav/food.png">
                            <img type="button"  data-target="#demo1" class="down_image img-responsive" src="image/default_image/down2.png">
                        </div>
                </div>
            </div>

            <div class="my_collapse" id="demo1">
              <a class="sub_cata_a_tag" href="food">ALL</a>
              <a class="sub_cata_a_tag" href="restaurant">Restaurant</a>
              <a class="sub_cata_a_tag" href="cafe">Cafe</a>
              <a class="sub_cata_a_tag" href="cart">Cart</a>
              <a class="sub_cata_a_tag" href="sweets">Sweets</a>
              <a class="sub_cata_a_tag" href="cake_pastry">Cake & Pastry</a>
            </div>

            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="lifestyle" class="list-group-item list-group-item-success"  data-parent="#MainMenu">Life Style </a>
                    <div class="side_nav_images">
                            <img class="icon_image img-responsive" src="image/default_image/nav/side_nav/lifestyle.png">
                            <img type="button"  data-target="#demo2" class="down_image img-responsive" src="image/default_image/down2.png">
                        </div>
                </div>
            </div>
            <div class="my_collapse" id="demo2">
              <a class="sub_cata_a_tag" href="lifestyle">ALL</a>
              <a class="sub_cata_a_tag" href="fashion">Fashion</a>
              <a class="sub_cata_a_tag" href="accessorice">Accessorice</a>
              <a class="sub_cata_a_tag" href="parlor_salon">Parlor & Salon</a>
              <a class="sub_cata_a_tag" href="gym_fitness">Gym & Fitness</a>
            </div>

            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="education" class="list-group-item list-group-item-success"  data-parent="#MainMenu">Education  </a>
                    <div class="side_nav_images">
                            <img class="icon_image img-responsive" src="image/default_image/nav/side_nav/education.png">
                            <img type="button"  data-target="#demo3" class="down_image img-responsive" src="image/default_image/down2.png">
                        </div>
                </div>
            </div>
            <div class="my_collapse" id="demo3">
              <a class="sub_cata_a_tag" href="education">ALL</a>
              <a class="sub_cata_a_tag" href="university">University</a>
              <a class="sub_cata_a_tag" href="college">College</a>
              <a class="sub_cata_a_tag" href="coaching">Coaching</a>
              <a class="sub_cata_a_tag" href="exams_result">Exams & Results</a>
              <a class="sub_cata_a_tag" href="admission">Admission</a>
            </div>

            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="health" class="list-group-item list-group-item-success"  data-parent="#MainMenu">Health </a>
                    <div class="side_nav_images">
                            <img class="icon_image img-responsive" src="image/default_image/nav/side_nav/health.png">
                            <img type="button"  data-target="#demo4" class="down_image img-responsive" src="image/default_image/down2.png">
                        </div>
                </div>
            </div>
            <div class="my_collapse" id="demo4">
              <a class="sub_cata_a_tag" href="health">ALL</a>
              <a class="sub_cata_a_tag" href="hospital_clinic">Hospital & Clinic</a>
              <a class="sub_cata_a_tag" href="doctor">Doctor</a>
              <a class="sub_cata_a_tag" href="ambulance">Ambulance</a>
            </div>

            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="residence" class="list-group-item list-group-item-success"  data-parent="#MainMenu">Residence  </a>
                     <div class="side_nav_images">
                            <img class="icon_image img-responsive" src="image/default_image/nav/side_nav/residence.png">
                            <img type="button"  data-target="#demo5" class="down_image img-responsive" src="image/default_image/down2.png">
                        </div>
                </div>
            </div>
            <div class="my_collapse" id="demo5">
              <a class="sub_cata_a_tag" href="residence">ALL</a>
              <a class="sub_cata_a_tag" href="hotel">Hotel</a>
              <a class="sub_cata_a_tag" href="resort">Resort</a>
              <a class="sub_cata_a_tag" href="tolet">To-Let</a>
            </div>

            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="tourism" class="list-group-item list-group-item-success"  data-parent="#MainMenu">Tourism  </a>
                     <div class="side_nav_images">
                            <img class="icon_image img-responsive" src="image/default_image/nav/side_nav/tourism.png">
                            <img type="button"  data-target="#demo6" class="down_image img-responsive" src="image/default_image/down2.png">
                        </div>
                </div>
            </div>
            <div class="my_collapse" id="demo6">
              <a class="sub_cata_a_tag" href="tourism">ALL</a>
              <a class="sub_cata_a_tag" href="natural_site">Natural Site</a>
              <a class="sub_cata_a_tag" href="heritage">Heritage</a>
              <a class="sub_cata_a_tag" href="picnic_spot">Picnic Spot</a>
              <a class="sub_cata_a_tag" href="lake_park">Lake & Park</a>
            </div>

            <div class="side_nav_link">
                <div class="side_nav_inner">
                    <a href="ecommerce" class="list-group-item list-group-item-success"  data-parent="#MainMenu">E-Commerce </a>
                    <div class="side_nav_images">
                            <img class="icon_image img-responsive" src="image/default_image/nav/side_nav/ecommerce.png">
                                <img type="button"  data-target="#demo7" class="down_image img-responsive" src="image/default_image/down2.png">
                        </div>
                </div>
            </div>
            <div class="my_collapse" id="demo7">
              <a class="sub_cata_a_tag" href="ecommerce">ALL</a>
              <a class="sub_cata_a_tag" href="health_beauty">Health & beauty</a>
              <a class="sub_cata_a_tag" href="efashion">Fashion</a>
              <a class="sub_cata_a_tag" href="food_grocery">Food & Grocery </a>
              <a class="sub_cata_a_tag" href="electronics_gadget">Electronics & Gadget</a>
              <a class="sub_cata_a_tag" href="gift_craft">Gift & Craft</a>
              <a class="sub_cata_a_tag" href="buy_sell">Buy & Sell</a>
              <a class="sub_cata_a_tag" href="job">Job</a>
              <a class="sub_cata_a_tag" href="others">Others</a></div>
            </div>

        </div>
      </div>
</div>



<script type='text/javascript'>



  var winHeight = $(window).height();
  if(winHeight<100){
    winHeight = $(document).height();
  }

  var sideMenuH = 9*60;
  var topMargin =  (winHeight-sideMenuH)/2;
  $(".side_nav_panel").css("cssText", "margin-top:"+topMargin+"px;");




  var downStatus = 0;
  var pre = '';
  $(".down_image").click(function(){

        if(pre != $(this).data("target")){
            $(".my_collapse").css("cssText", "max-height:0px;");
            downStatus =0;
        }

        if(downStatus == 0){
            $(".my_collapse").css("cssText", "max-height:0px;");
            $($(this).data("target")).css("cssText", "max-height:320px;");
            downStatus++;
        }else{
            $(".my_collapse").css("cssText", "max-height:0px;");
            downStatus = 0  ;
        }


        pre = $(this).data("target");
  });

  var winWidth = $(window).width();

  $( "#mySidenav" )
  .mouseout(function() {
      if(winWidth>560){
        document.getElementById("mySidenav").style.left = "-170px";
      }
  })
  .mouseover(function() {
      if(winWidth>560){
        document.getElementById("mySidenav").style.left = "0px";
      }
  });


  function showSideMenuM(){
      document.getElementById("mySidenav").style.left = "0%";
  }

  function closeSideNav(){
      document.getElementById("mySidenav").style.left = "-105%";
  }


</script>