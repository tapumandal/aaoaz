<div class="mobile_home_panel m_full_width_top">
	<div class="m_full_width m_food">
		<a href="/food">
			<div class="m_view">
				<img class="img-responsive" src="image/default_image/nav/mobile_home/mfood.jpg">
				<h1>FOOD</h1>
			</div>
		</a>
	</div>

	<div class="m_full_width">
		<div class="m_half_width m_education">
			<a href="/education">
				<div class="m_view">
					<img class="img-responsive" src="image/default_image/nav/mobile_home/meducation.jpg">
					<h1>EDUCATION</h1>
				</div>
			</a>
		</div>
		<div class="m_half_width m_health">
			<a href="/health">
				<div class="m_view">
					<img class="img-responsive" src="image/default_image/nav/mobile_home/mhealth.jpg">
					<h1>HEALTH</h1>
				</div>
			</a>
		</div>
	</div>

	<div class="m_full_width m_lifestyle">
		<a href="/lifestyle">
			<div class="m_view">
				<img class="img-responsive" src="image/default_image/nav/mobile_home/mlifestyle.jpg">
				<h1>LIFE STYLE</h1>
			</div>
		</a>
	</div>

	<div class="m_full_width">
		<div class="m_half_width m_residence">
			<a href="/residence">
				<div class="m_view">
					<img class="img-responsive" src="image/default_image/nav/mobile_home/mresidence.jpg">
					<h1>RESIDENCE</h1>
				</div>
			</a>
		</div>
		<div class="m_half_width m_ecommerce">
			<a href="/ecommerce">
				<div class="m_view">
					<img class="img-responsive" src="image/default_image/nav/mobile_home/mecommerce.jpg">
					<h1>ECOMMERCE</h1>
				</div>
			</a>
		</div>
	</div>

	<div class="m_full_width m_tourism">
		<a href="/tourism">
			<div class="m_view">
				<img class="img-responsive" src="image/default_image/nav/mobile_home/mtourism.jpg">
				<h1>TOURISM</h1>
			</div>
		</a>
	</div>
</div>