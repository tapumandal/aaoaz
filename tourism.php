<?php
session_start();
include 'operation/url_operation.php';
?>
<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:fb="http://ogp.me/ns/fb#">
<head>	
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106976768-1');
	</script>

	<?php
	include 'header_tags.php';
	include 'top_link_list.php'; 
	?>
	<script language="javascript" type="text/javascript">
		function resizeIframe(obj) {
			var offsetHeight = document.getElementById('info_height').offsetHeight;
				  	//document.getElementById("frame_height").innerHTML = offsetHeight;
				  	//window.alert(offsetHeight);
				  	offsetHeight = offsetHeight -25;
				  	obj.style.height = offsetHeight + 'px';
				   //obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
				}

			</script>
</head>

<body>
	<?php
		// include 'sideNabBar.php';
	?>
	<div class="page_loader"></div>

<div class = "container details_page">


				<div class="overlay-bg"> </div> <!-- FOR BLUR -->
				<div class="overlay-content popup54441" >
					<!-- <button class="close-btn">X</button> -->
					<div ID="overlay_content">

					</div>

				</div>


				<div class = "main-top">
					<div class="main">
						<?php

						include 'header.php';

						?>
					</div>
				</div>

	<div class="main_content_center">
				<div class="col-sm-12 field wide_pattern">
					<div class="">  <!-- Whole information without add -->

						<div class="col-sm-12 first">    <!-- FOR NAME -->
							<div class="info_box">
					<!-- <div class="title">
						<span>Name</span>
					</div> -->
								<div class="title_info">
									<span>
										<?php
										$general_obj = new general();
										$general_obj->general_name($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);

										?>
									</span>
								</div>
							</div>
						</div><!-- FOR NAME -->

			

			<div class="col-sm-8 first" > 
				<div class="col-sm-12 second" >   
					<div >
						<?php 
						$general_obj = new general();
						include 'slide-show.php';
						?>
					</div>
				</div>			
				<!-- PHOTO GALLARY -->
			
				<div class="col-sm-12 first" ID="info_height">  <!-- NORMAL INFORMATION  -->
					<?php
					$general_obj = new general();
					$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
					$general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
					$general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
					
					?>	

				</div>   <!-- NORMAL INFORMATION  -->
			</div>

			<div class="col-sm-4">
				<?php
				$general_obj->tourismMapAndVideo($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
				?>	
			</div>
			<!-- HIGHL	IGHTED MAP / HOTEL NEARBY / RESTAURANT / TRANSPORT / DISTANCE CHART -->
			<!-- <div class="col-sm-4 first">  -->
	   			<!-- <div class="col-sm-4 spot-in-map">
	   				<img src="content_image/tourism/bd map/bdmap.png" class="img-responsive" alt="MAP OF BANGLADESH">
	   			</div> -->
	   			<div class="clearfix"> </div>

	   			<div class="attrction_panel">
	   				<div class="attraction_title">
	   					<span>TOURIST ATTRACTION</span>
	   				</div>

	   				<div class="attraction_iframe">
	   					<!-- <iframe src="tourist_attraction.php?cata=<?php echo $_SESSION[cata]; ?>&sub_cata=<?php echo $_SESSION[sub_cata]; ?>&id=<?php echo $_SESSION[content_id]; ?>"></iframe>  -->

	   					<button class="slide_right_bt" onclick="tourismSlide('left')"><img src="image/default_image/left.png"></button>
	   					<button class="slide_left_bt" onclick="tourismSlide('right')"><img src="image/default_image/right.png"></button>
	   					<?php
		   					// include 'code_php.php';
	   					echo '<div class="attraction col-sm-12">';
	   					$obj = new general();
	   					$obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], $table_pattern_top, $table_pattern_bottom);
	   					echo '</div>';
	   					?>
	   				</div>
	   			</div>










	   			<!-- <div class="clearfix"></div> -->

	   			<div class="col-sm-12 nearby_panel">
	   				<div class="col-sm-4 nearby_left">
	   					<span class="nearby_title">Hotel Nearby</span>


	   					<?php
	   					$nearby = new general();
	   					$nearby->getNearbyInfo($_SESSION[content_id], 'hotel');
	   					?>

	   				</div>

	   				<div class="col-sm-4 nearby_right">
	   					<span class="nearby_title">Restaurant Nearby</span>

	   					<?php
	   					$nearby = new general();
	   					$nearby->getNearbyInfo($_SESSION[content_id], 'restaurant');
	   					?>

	   			</div>
	   			<div class="col-sm-4 nearby_right">
	   				<span class="nearby_title">google Add</span>
	   			</div>
	   		</div>
	 

	   		<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->


	   			<?php
	   			include 'comment_content.php';
	   			?>


	   			<div class="com">
	   				<?php
	   				$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
	   				?>
	   			</div>

	   		</div>   <!-- COMMENT  -->	   

	   	</div> <!-- Whole information without add -->


	   	<!-- add -->
	   	<!-- <div class="col-sm-2 third">  
	   		<div class="inside_top_right_panel">
	   			<div class="inside_top_head">
	   				<h3>Recomended</h3>
	   			</div>
	   			<div class="insider_box_panel">

	   				<?php
	   				// include 'operation/suggestion.php';
	   				// $obj = new suggestion();
	   				// $obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
	   				?>

	   			</div>
	   		</div>
	   	</div> -->
	   </div>




		</div>
	</div>
	<!-- END OF container -->

</div>
<?php
include 'footer.php';
include 'bottom_link_list.php';
?>

</body>
</html>