<?php
session_start();
include 'operation/url_operation.php';
?>
<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:fb="http://ogp.me/ns/fb#">
<head>

	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106976768-1');
	</script>

	
	<?php
	include 'header_tags.php';
	include 'top_link_list.php'; 
	?>
</head>

<script>
	
	$(document).ready(function(){


		    $("#comment_option").val($("#comment_option option:first").val()); //auto select the first option in select tag in comment section


		});

	</script>


<body>
	<?php
		// include 'sideNabBar.php';
	?>
		<div class="page_loader"></div>
	<div class = "container details_page">
			<div class="overlay-bg"> </div> <!-- FOR BLUR -->
			<div class="overlay-content popup54441" >
				<!-- <button class="close-btn">X</button> -->
				<div ID="overlay_content">

				</div>

			</div>


			<div class = "main-top">
				<div class="main">
					<?php
					
					include 'header.php';
					
					?>
				</div>
			</div>
		<div class="main_content_center">

					<div class="col-sm-12 field">
						  <!-- Whole information without add -->

						<div class="col-sm-12 wide_pattern first">    <!-- FOR NAME -->
							<div class="info_box">
								<div class="title_info" >
									<span>
										<?php
										$general_obj = new general();
										$general_obj->general_name($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);


										?>
									</span>
								</div>
							</div>
						</div><!-- FOR NAME -->


					<?php 
					if($_SESSION[sub_cata] == "gym_fitness" || $_SESSION[sub_cata] == "parlor_salon"){
						echo '
						<div class="clearfix"> </div>
						<div class="attrction_panel">
							<div class="attraction_title">
								<span>Service & Facilities</span>
							</div>

							<div class="attraction_iframe">
								<a class="slide_right_bt" onclick="tourismSlide(\'right\')"><i class="far fa-arrow-alt-circle-left"></i></a>
								<a class="slide_left_bt" onclick="tourismSlide(\'left\')"><i class="far fa-arrow-alt-circle-right"></i></a>
								<div class="attraction col-sm-12">';
								$obj = new general();
								$obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
								echo    '</div>
							</div>
						</div>
						';
					}
					else{

						$LSFilter = getLifeStyleFilter($_SESSION[content_id]);

						if($_SESSION[sub_cata] == "fashion")
						{
							$life_style_menu = $LSFilter;
						}
						else if($_SESSION[sub_cata] == "accessorice"){	
							$life_style_menu = $LSFilter;
						}

						else if($_SESSION[sub_cata] == "parlor_salon")
						{
							$life_style_menu = $LSFilter;
						}
						$table_pattern_top = '<div class="fashion_product_list">
						<div class="list">';
						$table_pattern_bottom = ' </div>
						</div>';

																		//echo $table_pattern_top."".$life_style_menu."".$table_pattern_bottom;
						$general_obj = new general();
						$general_obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], $table_pattern_top, $table_pattern_bottom, $life_style_menu);
					}
				?>


								<div class="clearfix"></div>

								<div class="col-sm-6 first"  id="info_height">  <!-- NORMAL INFORMATION  -->

									<?php
										$general_obj = new general();
										$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
									?>

								</div>   <!-- NORMAL INFORMATION  -->


								<div class="col-sm-6 second" id="slide_height">   <!-- PHOTO GALLARY -->
									<div >
										<?php 
										include 'slide-show.php';
										$general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
										$general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);	
										?>
	   		
									</div>
								</div>			<!-- PHOTO GALLARY -->




					<div class="clearfix"></div>

					<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
						<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->

							<?php
							include 'comment_content.php';
							?>


							<div class="com">
								<?php
								$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
								?>
							</div>

						</div>   <!-- COMMENT  -->




					</div>   <!-- COMMENT AND QUESTIOIN -->
				



				<!-- <div class="col-sm-2 third">
					<div class="inside_top_right_panel">
						<div class="inside_top_head">
							<h3>Recomended</h3>
						</div>
						<div class="insider_box_panel">

							<?php
							include 'operation/suggestion.php';
							$obj = new suggestion();
							$obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
							?>

						</div>
					</div>
				</div> -->


			</div>
			<!-- Whole information without add -->
		</div>


</div>
<!-- END OF container -->

<?php
include 'footer.php';
include 'bottom_link_list.php';
?>
</body>
</html>