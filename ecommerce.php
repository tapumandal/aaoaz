<?php
session_start();
include 'operation/url_operation.php';
?>
<!DOCTYPE html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:fb="http://ogp.me/ns/fb#">
<head>	
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106976768-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments)};
		gtag('js', new Date());

		gtag('config', 'UA-106976768-1');
	</script>

	<?php
	include 'header_tags.php';
	include 'top_link_list.php'; 
	?>
</head>

<script>
	
	$(document).ready(function(){


		    $("#comment_option").val($("#comment_option option:first").val()); //auto select the first option in select tag in comment section


		});

	</script>


<body>
	<?php
		// include 'sideNabBar.php';
	?>
	<div class="page_loader"></div>

<div class = "container details_page">
			<div class="overlay-bg"> </div> <!-- FOR BLUR -->
			<div class="overlay-content popup54441" >
				<!-- <button class="close-btn">X</button> -->
				<div ID="overlay_content">

				</div>

			</div>


			<div class = "main-top">
				<div class="main">
					<?php
					
					include 'header.php';
					
					?>
				</div>
			</div>
	<div class="main_content_center">
			<div class="col-sm-12 field">
				<div class="">  <!-- Whole information without add -->

					


					<div class="col-sm-12 wide_pattern first">    <!-- FOR NAME -->
						<div class="info_box">
							<!-- <div class="title">
								<span>Name</span>
							</div> -->
							<div class="title_info" >
								<span>
									<?php
									$general_obj = new general();
									$general_obj->general_name($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);


									?>
								</span>
							</div>

							


						</div>
						
					</div><!-- FOR NAME -->













					

					


					<?php 
					$LSFilter = getLifeStyleFilter($_SESSION[content_id]);

					$life_style_menu = $LSFilter;											

					$table_pattern_top = '<div class="fashion_product_list">
					<div class="list">';
					$table_pattern_bottom = ' </div>
					</div>';

																		//echo $table_pattern_top."".$life_style_menu."".$table_pattern_bottom;
					$general_obj = new general();
					$general_obj->general_item_list($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id], $table_pattern_top, $table_pattern_bottom, $life_style_menu);
					?>


					<div class="clearfix"></div>

					<div class="col-sm-6 first"  id="info_height">  <!-- NORMAL INFORMATION  -->

						<?php
							//include "code_php.php";
						$general_obj = new general();
						$general_obj->general_info($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
							// $general_obj->general_map_location($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);					
							// $general_obj->general_map($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);	
						?>
						
					</div>   <!-- NORMAL INFORMATION  -->


					<div class="col-sm-6 second" id="slide_height">   <!-- PHOTO GALLARY -->
						<div >
							<?php 
							include 'slide-show.php';
							$general_obj->general_description_ecommerce($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);	
							?>
			   				<!--  <div class="" style="margin-top:10px; border:2px solid #A3A4A6;">
			   					<iframe width="100%" height="250"  src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d116834.13673771221!2d90.41932575!3d23.780636450000003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1433590604982"  frameborder="0" style="border:0">
								</iframe>
							</div> -->
						</div>
					</div>			<!-- PHOTO GALLARY -->




					<div class="clearfix"></div>

					<div class="col-sm-12">   <!-- COMMENT AND QUESTIOIN -->
						<div class="col-sm-12 comment" id="jump" >   <!-- COMMENT -->

							<?php
							include 'comment_content.php';
							?>


							<div class="com">
								<?php
								$general_obj->fetch_comment($_SESSION[cata], $_SESSION[sub_cata], $_SESSION[content_id]);
								?>
							</div>

						</div>   <!-- COMMENT  -->




					</div>   <!-- COMMENT AND QUESTIOIN -->


				</div> <!-- Whole information without add -->

				<!-- <div class="col-sm-2 third">
					<div class="inside_top_right_panel">
						<div class="inside_top_head">
							<h3>Recomended</h3>
						</div>
						<div class="insider_box_panel">

							<?php
							// include 'operation/suggestion.php';
							// $obj = new suggestion();
							// $obj->recomended($_SESSION[cata], $_SESSION[sub_cata]);
							?>

						</div>
					</div>
				</div> -->
			</div>

	</div>
</div>
<!-- END OF container -->

<?php
include 'footer.php';
include 'bottom_link_list.php';
?>
</body>
</html>