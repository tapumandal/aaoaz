<link href="https://fonts.googleapis.com/css?family=Arapey|Bellefair|Cinzel+Decorative|Cormorant|Kameron|Old+Standard+TT|Oranienbaum|Playfair+Display+SC|Pompiere|Prata|Radley" rel="stylesheet">

<style type="text/css">
.sidenav {
    height: 100%;
    width: 0px;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: white;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
    text-align:right;
    box-shadow: 5px 5px 10px gray;
    z-index: 100;
}

.sidenav a {
    padding: 8px 30px 8px 32px;
    text-decoration: none;
    font-size: 22px;
    display: block;
    transition: 0.3s;
    background: white;
    border-right: none;
    color: rgb(50,50,50)!important;
    /*color: black !important;*/


    /*font-family: 'Old Standard TT', serif;*/
    /*font-family: 'Playfair Display SC', serif;*/
    font-family: 'Arapey', serif;
    /*font-family: 'Oranienbaum', serif;*/
    text-align: center;
    

}

a.sideNavClose{
  padding: 0px;
  padding-right: 5px;
  color: red !important;
  font-size: 50px !important;
}

a.list-group-item-success:hover, a.list-group-item-success:focus{
  /*background: rgb(80,80,80) !important;*/
  background: rgb(50,50,50) !important;
  color: white !important;


}
.list-group-item{
  border:0px !important;
}
a.sub_cata_a_tag{
  padding: 5px;
  /*padding-right: 60px;*/
  font-size: 18px;
  background: rgb(200,200,200);
}

a.sub_cata_a_tag:hover {
    /* background: #262626 !important; */
    /* color: white !important; */
    /* color: #b3b3b3; */
    /* width: 100%; */
     /*font-size: 20px;*/
     background: rgb(150,150,150);
     color: white;
     /*padding: 4px;*/
     /*padding-right: 60px;*/
    /* font-weight: normal; */
    /* padding: 15px 0px 15px 0px !important; */
    /* opacity: 1 !important; */
    text-decoration: none;
    /*transform: scale(1.1);*/
    font-size: 22px;
    /*margin-right: 20px;*/
    color: white !important;
    padding-top: 2px;
    padding-bottom: 2px;
    /*padding-right:60px;*/
}
/*.collapse{
  height: 0px;
  overflow: hidden;
  display: block !important;
  -webkit-transition: height 2s ease;
    -moz-transition: height 2s ease;
      -o-transition: height 2s ease;
     -ms-transition: height 2s ease;
         transition: height 2s ease;

}
.list-group-item-success:hover + .collapse{
  height: 300px;
}*/


.panel{
  border: none !important;
}

.sidenav a:hover{
    color: #f1f1f1;
}

.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
    text-decoration: none;
    margin-top: -7px;
    background: transparent;
}

.menu_btn span{
  width: 10%;
  float: left;

  text-align: right;

  margin-top: 72px;
  padding-right: 10px;
  height: 37px;
  border: 1px solid #ff751a;
  border-radius: 0px 4px 4px 0px;
  color: #ff751a;
  border-left: none;
  font-size:25px;
  cursor:pointer;
   z-index: 1;
   position: absolute;
}

.menu_btn span:hover{
  background: #ff751a;
  color: white;
}
.menu_btn span{
    float: left;
  }
  .menu_btn p{
    float: right;
    min-width: 60px;
    padding-left: 5px;
    padding-right: 25px;
  }


@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}


</style>
  

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn sideNavClose" onclick="closeNav()">&times;</a>
  <!-- <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a> -->



      <div id="MainMenu">
        <div class="list-group panel">
          <a href="./" class="list-group-item list-group-item-success" data-parent="#MainMenu">Home</a>
      


            <a href="#demo1" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">Food  <!-- <i class="fa fa-caret-down"></i>  --> </a>
            <div class="collapse" id="demo1">
              <a class="sub_cata_a_tag" href="food">ALL</a>
              <a class="sub_cata_a_tag" href="restaurant">Restaurant</a>
              <a class="sub_cata_a_tag" href="cafe">Cafe</a>
              <a class="sub_cata_a_tag" href="cart">Cart</a>
              <a class="sub_cata_a_tag" href="sweets">Sweets</a>
              <a class="sub_cata_a_tag" href="cake_pastry">Cake & Pastry</a>
            </div>

            <a href="#demo2" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">Life Style  <!-- <i class="fa fa-caret-down"></i>  --> </a>
            <div class="collapse" id="demo2">
              <a class="sub_cata_a_tag" href="lifestyle">ALL</a>
              <a class="sub_cata_a_tag" href="fashion">Fashion</a>
              <a class="sub_cata_a_tag" href="accessorice">Accessorice</a>
              <a class="sub_cata_a_tag" href="parlor_salon">Parlor & Salon</a>
              <a class="sub_cata_a_tag" href="gym_fitness">Gym & Fitness</a>              
            </div>

            <a href="#demo3" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">Education  <!-- <i class="fa fa-caret-down"></i> --> </a>
            <div class="collapse" id="demo3">
              <a class="sub_cata_a_tag" href="education">ALL</a>
              <a class="sub_cata_a_tag" href="university">University</a>
              <a class="sub_cata_a_tag" href="college">College</a>
              <a class="sub_cata_a_tag" href="coaching">Coaching</a>
              <a class="sub_cata_a_tag" href="exams_result">Exams & Results</a>
              <a class="sub_cata_a_tag" href="admission">Admission</a>        
            </div>

            <a href="#demo4" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">Health  <!-- <i class="fa fa-caret-down"></i>  --> </a>
            <div class="collapse" id="demo4">
              <a class="sub_cata_a_tag" href="health">ALL</a>
              <a class="sub_cata_a_tag" href="hospital_clinic">Hospital & Clinic</a>
              <a class="sub_cata_a_tag" href="doctor">Doctor</a>
              <a class="sub_cata_a_tag" href="ambulance">Ambulance</a>
            </div>

            <a href="#demo5" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">Residence  <!-- <i class="fa fa-caret-down"></i> --> </a>
            <div class="collapse" id="demo5">
              <a class="sub_cata_a_tag" href="residence">ALL</a>
              <a class="sub_cata_a_tag" href="hotel">Hotel</a>
              <a class="sub_cata_a_tag" href="resort">Resort</a>
              <a class="sub_cata_a_tag" href="tolet">To-Let</a>
            </div>

            <a href="#demo6" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">Tourism  <!-- <i class="fa fa-caret-down"></i> --> </a>
            <div class="collapse" id="demo6">
              <a class="sub_cata_a_tag" href="tourism">ALL</a>
              <a class="sub_cata_a_tag" href="natural_site">Natural Site</a>
              <a class="sub_cata_a_tag" href="heritage">Heritage</a>
              <a class="sub_cata_a_tag" href="picnic_spot">Picnic Spot</a>
              <a class="sub_cata_a_tag" href="lake_park">Lake & Park</a>
            </div>

            <a href="#demo7" class="list-group-item list-group-item-success" data-toggle="collapse" data-parent="#MainMenu">E-Commerce  <!-- <i class="fa fa-caret-down"></i>  --> </a>
            <div class="collapse" id="demo7">
              <a class="sub_cata_a_tag" href="ecommerce">ALL</a>
              <a class="sub_cata_a_tag" href="health_beauty">Health & beauty</a>
              <a class="sub_cata_a_tag" href="efashion">Fashion</a>
              <a class="sub_cata_a_tag" href="food_grocery">Food & Grocery </a>
              <a class="sub_cata_a_tag" href="electronics_gadget">Electronics & Gadget</a>
              <a class="sub_cata_a_tag" href="gift_craft">Gift & Craft</a>
              <a class="sub_cata_a_tag" href="buy_sell">Buy & Sell</a>
              <a class="sub_cata_a_tag" href="job">Job</a>
              <a class="sub_cata_a_tag" href="others">Others</a></div>                 
            </div>

        </div>
      </div>
</div>

<div class="menu_btn"><span style="" onclick="openNav()">&#9776; <p>MENU</p></span></div>

<script>
function openNav() {
    var scrWidth = $(window).width();
    if(scrWidth>800){
      document.getElementById("mySidenav").style.width = "15%";
    }
    else if(scrWidth<801 && scrWidth>550 ){
      document.getElementById("mySidenav").style.width = "50%";
    }
    else{
      document.getElementById("mySidenav").style.width = "100%"; 
    }


    
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>