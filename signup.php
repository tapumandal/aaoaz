<?php
error_reporting(0);
session_start();

$token = "";
if (isset($_POST['token'])) {
	$token = $_POST['token'];
} else if (isset($_GET['token'])) {
	$token = $_POST['token'];
}

if ($token == $_SESSION['token'] && $tokenTime < 500) {
} else {
	$_SESSION[access_failed_2] = '
					<div class="access_failed">
						<span>There is something wrong in your request or time out.
						 Please try again.</span>
					</div>
				';
	echo '<script> window.location.replace("./login"); </script>';
	exit();
}

include 'data_connection.php';
include 'operation/cookie.php';

$name = stringFilter($_POST[name]);
$email = stringFilter($_POST[email]);
$pass = stringFilter($_POST[pass]);
$gender = stringFilter($_POST[sex]);

if (strlen($name) > 4 && strlen($email) > 9 && strlen($pass) > 5 && strlen($gender) > 3) {
	if (!empty($name) && !empty($email) && !empty($pass) && !empty($gender)) {

		$encryptPass = encryptPassword($pass);

		//echo $name."   ".$email."   ".$pass;

		// $sign_up = "INSERT INTO `user`(`name`, `email`, `pass`, `gender`, `type`, `signup_type`, `status`, `date`, `time`) VALUES ('$name','$email','$encryptPass','$gender','user', 'aaoaz', 'enable',now(),now());";
		$sign_up = $conn->prepare("INSERT INTO `user`(`name`, `email`, `pass`, `gender`, `type`, `signup_type`, `status`, `date`, `time`)
							VALUES (?,?,?,?,'user', 'aaoaz', 'enable',now(),now())");
		$sign_up->bind_param("ssss", $name, $email, $encryptPass, $gender);

		if (!$sign_up->execute()) {
			echo "Your  account is not opened correctly.<br> Please contact aaoaz.com via email or phone.<br> Thank you__aaoaz.com";
		} else {

			$last_user_id = $sign_up->insert_id;

			$_SESSION[userid] = $last_user_id;
			setUserCookie($_SESSION[userid]);

			$user_acc = "INSERT INTO `user_access_time`(`user_id`, `login date`, `login time`) VALUES ('$_SESSION[userid]', now(),now())";
			if ($conn->query($user_acc)) {
				echo "Logging in";
				$_SESSION[loginStatTime] = time();
			} else {
				echo "user access time problem";
			}
			$sign_up->close();
			echo '<script>history.back();</script>';
		}
		$stmt->close();

		// if ($conn->query($sign_up) === TRUE) {
		// 	$last_user_id = $conn->insert_id;

		// 	$_SESSION[userid] = $last_user_id;
		// 	setUserCookie($_SESSION[userid]);

		// 	$user_acc = "INSERT INTO `user_access_time`(`user_id`, `login date`, `login time`) VALUES ('$_SESSION[userid]', now(),now())";
		// 	if ($conn->query($user_acc)) {
		// 		echo "Logging in";
		// 		$_SESSION[loginStatTime] = time();
		// 	} else {
		// 		echo "user access time problem";
		// 	}

		// 	echo '<script>history.back();</script>';
		// } else {
		// 	echo "Your  account is not opened correctly.<br> Please contact aaoaz.com via email or phone.<br> Thank you__aaoaz.com";
		// }
	} else {
		echo '<script>history.back();</script>';
	}
} else {
	$_SESSION[access_failed_2] = '
					<div class="access_failed">
						<span>Input charecter length is not correct. Please input valid name and email.
						Password must be 6 charecter minimum.  </span>
					</div>
				';
	echo '<script> window.location.replace("./login"); </script>';
	exit();
}

function encryptPassword($string) {
	$enPass = md5($string);
	$enTmp1 = "";
	$enTmp2 = "";
	$enTmp3 = "";
	$enTmp4 = "";
	$enTmp5 = "";

	$i = 0;
	for ($i = 0; $i < strlen($enPass); $i++) {
		if ($i < 4) {
			$enTmp1 = $enTmp1 . $enPass[$i];
		} else if ($i < 8) {
			$enTmp2 = $enTmp2 . $enPass[$i];
		} else if ($i < 12) {
			$enTmp3 = $enTmp3 . $enPass[$i];
		} else if ($i < 16) {
			$enTmp4 = $enTmp4 . $enPass[$i];
		} else {
			$enTmp5 = $enTmp5 . $enPass[$i];
		}
	}

	$encrypted = $enTmp1 . $enTmp4 . $enTmp2 . $enTmp3 . $enTmp5;

	return $encrypted;
}

function stringFilter($string) {
	$string = trim($string);
	$string = str_replace('\\', '', $string);
	$string = str_replace('\'', "", $string);
	$string = str_replace('"', '', $string);
	$string = str_replace('`', "", $string);
	$string = str_replace(';', '', $string);
	$string = str_replace('SELECT', '', $string);
	$string = str_replace('UPDATE', '', $string);
	$string = str_replace('DROP', '', $string);
	$string = str_replace('INSERT', '', $string);
	$string = str_replace('DELETE', '', $string);
	$string = str_replace('FROM', '', $string);
	$string = str_replace('WHERE', '', $string);

	$string = str_replace('select', '', $string);
	$string = str_replace('update', '', $string);
	$string = str_replace('drop', '', $string);
	$string = str_replace('insert', '', $string);
	$string = str_replace('delete', '', $string);
	$string = str_replace('from', '', $string);
	$string = str_replace('where', '', $string);

	return $string;
}

?>