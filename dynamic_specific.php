<?php
	include 'operation/home_data_operation.php';

	if($getCata == "food"){
		$firstTitle = "Most weekly viewed.";
		$secondTitle = "New in aaoaz";
		$thirdTitle = "Most rated.";
		$fourthTitle = "Offer going on.";
		$fifthTitle = "New Item";
	}
	else if($getCata == "life style"){
		$firstTitle = "Most weekly viewed.";
		$secondTitle = "New in aaoaz";
		$thirdTitle = "Most rated.";
		$fourthTitle = "Offer going on.";
		$fifthTitle = "New Product";
	}
	else if($getCata == "education"){
		$firstTitle = "Most monthly viewed.";
		$secondTitle = "New in aaoaz";
		$thirdTitle = "Most rated.";
		$fourthTitle = "Admission going on.";
		$fifthTitle = "Upcoming exam";
	}
	else if($getCata == "health"){
		$firstTitle = "Most monthly viewed.";
		$secondTitle = "New in aaoaz";
		$thirdTitle = "Most rated.";
		$fourthTitle = "Most viewed.";
		$fifthTitle = "24 Hours Service";
	}
	else if($getCata == "electronics"){
		$firstTitle = "Most monthly viewed.";
		$secondTitle = "New in aaoaz";
		$thirdTitle = "Most rated.";
		$fourthTitle = "Most viewed.";
		$fifthTitle = "New Device arrived.";
	}
	else if($getCata == "residence"){
		$firstTitle = "Most monthly viewed.";
		$secondTitle = "New in aaoaz";
		$thirdTitle = "Most rated.";
		$fourthTitle = "Most viewed.";
		$fifthTitle = "Offer";
	}
	else if($getCata == "tourism"){
		$firstTitle = "Most monthly viewed.";
		$secondTitle = "New in aaoaz.";
		$thirdTitle = "Most rated.";
		$fourthTitle = "Most viewed.";
		$fifthTitle = "Random";
	}

?>
<div class="home" id="search_result">
		
		
			<div class="first_panel" id="first_panel">
				<div class="panel_title"><h4><?php echo $firstTitle; ?></h4></div>
				
				<div class="next_pre_panel">
					<div class="next_pre_bt left_but"><img src="image/default_image/left.png"></div>
					<div class="next_pre_bt right_but"><img src="image/default_image/right.png"></div>
				</div>

				<div class="first_panel_inside">


					<?php
							getFirstPanel($getCata);
					?>
				</div>

			</div>




			<div class="second_panel" id="second_panel">
				<div class="panel_title"><h4><?php echo $secondTitle; ?></h4></div>
				
				<div class="next_pre_panel2">
					<div class="next_pre_bt2 left_but2"><img src="image/default_image/left.png"></div>
					<div class="next_pre_bt2 right_but2"><img src="image/default_image/right.png"></div>
				</div>

				<div class="second_panel_inside">

					<?php
						getSecondPanel($getCata);
					?>
					<!-- <div class="home_box4">
						<div class="home_box_inside4">
							<div class="home_box_image4">
								<img class="img-responsive" src="image/title_image/17_life style_parlor_salon.jpg" alt="image">
							</div>
							<div class="home_box_name4">
								<h4>1Tapu Mandal</h4>
							</div>
						</div>
					</div> -->
					
					

				</div>

			</div>
			<div class="clearfix"></div>
		

			<div class="fourth_panel" id="fourth_panel">
				<div class="panel_title"><h4><?php echo $thirdTitle; ?></h4></div>
				
				<div class="next_pre_panel4">
					<div class="next_pre_bt4 left_but4"><img src="image/default_image/left.png"></div>
					<div class="next_pre_bt4 right_but4"><img src="image/default_image/right.png"></div>
				</div>

				<div class="fourth_panel_inside">

						<?php
							getThirdPanel($getCata);
						?>

				</div>

			</div>
		
			<div class="third_panel" id="third_panel">
				<div class="panel_title"><h4><?php echo $fourthTitle; ?></h4></div>
				
				<div class="next_pre_panel3">
					<div class="next_pre_bt3 left_but3"><img src="image/default_image/left.png"></div>
					<div class="next_pre_bt3 right_but3"><img src="image/default_image/right.png"></div>
				</div>

				<div class="third_panel_inside">

					<?php
						getFourthPanel($getCata);
					?>
					<!-- <div class="home_box4">
						<div class="home_box_inside4">
							<div class="home_box_image4">
								<img class="img-responsive" src="image/title_image/17_life style_parlor_salon.jpg" alt="image">
							</div>
							<div class="home_box_name4">
								<h4>1Tapu Mandal</h4>
							</div>
						</div>
					</div> -->
					
					

				</div>

			</div>



			<div class="five_panel" id="five_panel">
				<div class="panel_title"><h4><?php echo $fifthTitle; ?></h4></div>
				
				<div class="next_pre_panel5">
					<div class="next_pre_bt5 left_but5"><img src="image/default_image/left.png"></div>
					<div class="next_pre_bt5 right_but5"><img src="image/default_image/right.png"></div>
				</div>

				<div class="five_panel_inside">

						<?php
							getFifthPanel($getCata);
						?>


				</div>

			</div>


<div class="clearfix"></div>

	
</div>