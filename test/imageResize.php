<?php
	
			include '../data_connection.php';
	
			$sql= "SELECT * FROM general_info WHERE 1;";
			$sqlRes = $conn->query($sql);
			if($sqlRes->num_rows>0){
				while ($row=$sqlRes->fetch_assoc()) {
					
					$name = $row[image_link];
					$originalImage= '../image/title_image/'.$name;
					$smallerImage= '../image/title_image/s_'.$name;

					$imageCopyStatus = resizeImage($originalImage, $smallerImage, 4, 92);

						if($imageCopyStatus) echo $imageCopyStatus." > ".$name."<br>";
				}
			}


	function resizeImage($sourceImage, $targetImage, $ratio, $quality){	
					$status = false;
				    // Get dimensions of source image.
				    list($origWidth, $origHeight) = getimagesize($sourceImage);

				    if ($maxWidth == 0)
				    {
				        $maxWidth = $origWidth/10;
				        $maxWidth = $maxWidth*$ratio;
				    }

				    if ($maxHeight == 0)
				    {
				        $maxHeight = $origHeight/10;
				        $maxHeight = $maxHeight*$ratio;
				    }

				    // Calculate ratio of desired maximum sizes and original sizes.
				    $widthRatio = $maxWidth / $origWidth;
				    $heightRatio = $maxHeight / $origHeight;

				    // Ratio used for calculating new image dimensions.
				    $ratio = min($widthRatio, $heightRatio);

				    // Calculate new image dimensions.
				    $newWidth  = (int)$origWidth  * $ratio;
				    $newHeight = (int)$origHeight * $ratio;

				    // Obtain image from given source file.
				    if(strpos($sourceImage, '.jpg') !== false){
					    if (!$image = @imagecreatefromjpeg($sourceImage))
					    {
					        return false;
					    }
					    // Create final image with new dimensions.
					    $newImage = imagecreatetruecolor($newWidth, $newHeight);
					    imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
					    imagejpeg($newImage, $targetImage, $quality);
					    $status = true;
					}
					else if(strpos($sourceImage, '.png') !== false){
						$newImage = imagecreatetruecolor($newWidth, $newHeight);
						$source = imagecreatefrompng($sourceImage);
						imagecopyresized($newImage, $source, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
						imagepng($newImage,$targetImage);	
						$status = true;
					}else if(strpos($sourceImage, '.jpeg') !== false){
					    if (!$image = @imagecreatefromjpeg($sourceImage))
					    {
					        return false;
					    }
					    // Create final image with new dimensions.
					    $newImage = imagecreatetruecolor($newWidth, $newHeight);
					    imagecopyresampled($newImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
					    imagejpeg($newImage, $targetImage, $quality);
					    $status = true;
					}
				
				    // Free up the memory.
				    imagedestroy($image);
				    imagedestroy($newImage);




				    
					imagedestroy($source);

				    return $status;
			}
?>