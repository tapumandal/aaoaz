<?php
error_reporting(0);
session_start();
// include 'operation/url_operation.php';
?>
<!DOCTYPE html>
<head>

<title>aaoaz reward</title>
<base href="/">
<meta charset="utf-8">
<link rel="icon" href="image/default_image/short_logo.png">


<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


				<script language="javascript" type="text/javascript">
				  function resizeIframe(obj) {
				  	var offsetHeight = document.getElementById('info_height').offsetHeight;
				  	offsetHeight = offsetHeight -25;
				    obj.style.height = offsetHeight + 'px';
				  }

				</script>
	<?php include '../top_link_list.php';?>
</head>

<body>
	<?php
// include '../sideNabBar.php';
?>
<div class = "container ">
		<div class="overlay-bg"> </div> <!-- FOR BLUR -->
		<div class="overlay-content popup54441" >
			<!-- <button class="close-btn">X</button> -->
			<div ID="overlay_content"></div>
		</div>


	<div class = "main-top">
		<div class="main">
				<?php

include '../header.php';
?>
		</div>
	</div>


	<div class="col-md-12 reward">

		<?php
include '../explore/explore.php';
?>
		<div class="reward_panel">

				<?php

include '../operation/home_data_operation.php';

peoplesReview($_GET[cata], $_GET[length]);

$next = $_GET[length] + 1;
$pre = $_GET[length] - 1;
if ($pre < 1) {
	$pre = 1;
}
?>

				<div class="review_pager">
					<div class="review_pager_inside">
						<ul class="pager">
						    <li><a href="./review/<?php echo $_GET[cata] . "/" . $pre; ?>">Previous</a></li>
						    <li><a href="./review/<?php echo $_GET[cata] . "/" . $next; ?>">Next</a></li>
						</ul>
					</div>
				</div>
		</div>
	</div>


</div>

<?php
include '../footer.php';
include '../bottom_link_list.php';
?>



<div class="footer">
	<?php
include '../operation/page_view.php';

$vObj = new view();

$vObj->everyView($_SESSION[content_name]);

$vObj->startView();
?>
</div>


</body>
</html>


<?php

function peoplesReview($cata, $l) {

	$comment_id = $last_comment_id;
	$last_comment_id = preg_replace('/[^0-9.]+/', '', $last_comment_id);

	// Select Rating title

	include "../data_connection.php";

	$activeClass = "active";

	if ($cata == "all") {
		$fetch_comment_data = "SELECT comment.id as commentid, comment.cata_id, comment.cata, comment.sub_cata, comment.user_id, comment.item_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, comment.points, user.name,user.id, user.name, user.gender, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id
				WHERE comment.status = 'show' ORDER BY comment.points DESC;";
	} else {
		$fetch_comment_data = "SELECT comment.id as commentid, comment.cata_id, comment.cata, comment.sub_cata, comment.user_id, comment.item_id, comment.user_type, comment.text,
				comment.type, comment.rate_1, comment.rate_2, comment.rate_3, comment.time, comment.date, comment.points, comment.points, user.name,user.id, user.name, user.gender, user.image_link FROM comment
				LEFT JOIN user ON user.id=comment.user_id
				WHERE comment.cata='$cata' AND comment.status = 'show' ORDER BY comment.points DESC;";
	}

	$image_link = "";
	$data = $conn->query($fetch_comment_data);
	$item_number = 1;

	$lend = $l * 10;
	$lstart = $lend - 9;

	$inc = 1;

	$reviewCount = 0;

	if ($data->num_rows > 0) {
		while ($row = $data->fetch_assoc()) {

			if ($lstart <= $inc && $lend >= $inc) {

				$reviewCount++;

				if (!empty($row[image_link])) {
					$image_link = "image/user_image/" . $row[image_link];
				} else {
					if ($row[gender] == 'female') {
						$image_link = "image/user_image/profile2.jpg";
					} else {
						$image_link = "image/user_image/profile.jpg";
					}
				}

				$general_obj_com = new general();
				$getCommentImg = $general_obj_com->getCommentImg($row[commentid]);

				$date = date_create_from_format("Y-m-d", $row[date]);
				$date = date_format($date, "j M Y");

				$time = date_create_from_format("H:i:s", $row[time]);
				$time = date_format($time, "g:i:a");

				if ($row[cata] == "food" && $row[type] == "rate") {

					$getCommentItem = "SELECT * FROM `item_list` WHERE `id` = '$row[item_id]'";
					$resItemId = $conn->query($getCommentItem);

					if ($resItemId->num_rows > 0) {
						while ($res2 = $resItemId->fetch_assoc()) {
							$tmp = $res2[id];
							$id = $res2[cata_id];
							$itemIndex = array_search($tmp, $_SESSION[food_item_detect]);
							$sub_cata = $res2[sub_cata];
							$comment_item = '
										<div class="comment_item">
											<div class="comment_item_num">
												<span>' . $itemIndex . '</span>
											</div>

											<div class="comment_item_img">
												<img class="img-responsive" src="http://www.aaoaz.com/image/item_image/s_' . $res2[image_link] . '">
											</div>

											<div class="comment_item_name">
												<a class="show-popup food_list_see" href="item_details.php?cata=' . $cata . '&sub_cata=' . $sub_cata . '&id=' . $id . '&item_id=' . $res2[id] . '" data-showpopup="54441" onclick="show_overlay(\'' . $cata . '\',' . $res2[id] . ')">' . $res2[name] . '</a>
											</div>
										</div>
									';
						}
					} else {
						$comment_item = "";
					}

				}

				$contentName = "";

				$getCommentContentName = "SELECT name FROM `general_info` WHERE `id` = '$row[cata_id]'";
				$ContentName = $conn->query($getCommentContentName);
				if ($ContentName->num_rows > 0) {
					while ($res3 = $ContentName->fetch_assoc()) {
						$contentName = $res3[name];
					}
				}

				$subcata_url = explode(" ", $row[sub_cata]);
				$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

				$link_name = strtolower($contentName);
				$link_name = str_replace(" ", "-", $link_name);

				$general_rate_obj = new general();

				if ($row[type] == "rate") {

					$rate_1 = $row[rate_1] * 20;
					$rate_2 = $row[rate_2] * 20;
					$rate_3 = $row[rate_3] * 20;

					if ($row[cata] == "food") {
						$rateTitle1 = "TASTE";
						$rateTitle2 = "PRICE";
						$rateTitle3 = "SERVICE";
					} else if ($row[cata] == "life style") {
						if ($row[sub_cata] == "fashion" || $row[sub_cata] == "accessorice") {
							$rateTitle1 = "QUALITY";
							$rateTitle2 = "PRICE";
							$rateTitle3 = "SERVICE";

						} else if ($row[sub_cata] == "parlor_salon" || $row[sub_cata] == "gym_fitness") {
							$rateTitle1 = "ENVIRONMENT";
							$rateTitle2 = "EXPENSE";
							$rateTitle3 = "SERVICE";
						}
					} else if ($row[cata] == "education") {
						$rateTitle1 = "FACULTY";
						$rateTitle2 = "EXPENSE";
						$rateTitle3 = "FACILITIE";
					} else if ($row[cata] == "health") {
						if ($row[sub_cata] == "hospital_clinic") {
							$rateTitle1 = "DOCTOR";
							$rateTitle2 = "EXPENSE";
							$rateTitle3 = "FACILITIE";
						}
					} else if ($row[cata] == "education") {
						if ($row[sub_cata] == "university" || $row[sub_cata] == "college" || $row[sub_cata] == "coaching") {
							$rateTitle1 = "FACULTY";
							$rateTitle2 = "EXPENSE";
							$rateTitle3 = "FACILITIES";
						}
					} else if ($row[cata] == "electronics") {
						$rateTitle1 = "DESIGN";
						$rateTitle2 = "PERFORMANCE";
						$rateTitle3 = "BATTERY LIFE";
					} else if ($row[cata] == "residence") {
						if ($row[sub_cata] != "to-let") {
							$rateTitle1 = "FACILITIE";
							$rateTitle2 = "EXPENSE";
							$rateTitle3 = "SERVICE";
						}
					} else if ($row[cata] == "ecommerce") {
						if ($row[sub_cata] != "buy_sell" || $row[sub_cata] != "others" || $row[sub_cata] != "job") {

							$rateTitle1 = "RESPONSE";
							$rateTitle2 = "PRICE";
							$rateTitle3 = "SERVICE";

						} else {
							$rateTitle1 = "RESPONSE";
							$rateTitle2 = "PRICE";
							$rateTitle3 = "SERVICE";
						}
					}

					echo '
									<div class="item ' . $activeClass . '">
									<div class="only_com">
										<div class="only_com_user_info">
											<div class="com_user_data">
												<div class="com_user_data_img">
													<img class="img-responsive user_img" src="' . $image_link . '"></img>
												</div>
												<div class="com_user_all">
													<div class="com_user_name">
														<span class="name">' . $row[name] . '</span><br>
														<span class="comment_date">' . $time . '<br>' . $date . '</span><br>
														<span class="comment_date"><b>Points: ' . $row[points] . '</b></span>
													</div>

													<div class="com_user_content_info">
														<a href="' . $subcata_url[0] . '/' . strtolower($link_name) . '">' . $contentName . '</a>
													</div>
												</div>

											</div>
										</div>

										<div class="review_comment">
											<div class="com_user_review">
												<div class="comment_user_review">
													<div class="comment_review_quality">
														<div class="comment_review_title">
															<span>' . $rateTitle1 . '</span>
														</div>
														' . $general_rate_obj->ratingSingle($rate_1) . '
													</div>

													<div class="comment_review_quality">
														<div class="comment_review_title">
															<span>' . $rateTitle2 . '</span>
														</div>
														' . $general_rate_obj->ratingSingle($rate_2) . '
													</div>

													<!-- style="margin-left:110px" -->
													<div class="comment_review_quality" >
														<div class="comment_review_title">
															<span>' . $rateTitle3 . '</span>
														</div>
														' . $general_rate_obj->ratingSingle($rate_3) . '
													</div>
												</div>
												' . $comment_item . '
											</div>
											<div class="review_text">

												<div class="opinion rate_opinion">
													<span class="op"> ' . $row[text] . '</span>
												</div>
											</div>

											<div class="comment_img">
												' . $getCommentImg . '
											</div>
										</div>


									</div>
									</div>
									';

				} // else if of rate

				$activeClass = "";
			} else {

			}

			$inc++;
		}
	}

	if ($l <= 0) {
		echo "<h3 style='width:100%; text-align:center'>No Content Found</h3>";
	} else if ($reviewCount == 0) {
		$l = $l - 1;
		$url = "./review/" . $cata . "/" . $l;
		echo '<script>window.location.replace("' . $url . '");</script>';

		// die();
	}
}

?>