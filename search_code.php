<?php
error_reporting(0);
	
	function searchStringFilter($string){
		$string = trim($string);
		$string = str_replace('\\','/', $string);
		$string = str_replace('\'',"", $string);
		$string = str_replace('"','', $string);
		$string = str_replace('`',"", $string);
		$string = str_replace(';','.', $string);
		$string = strip_tags($string);
		return $string;
	}


	$getFind = searchStringFilter($_GET[find]);
	$getS = searchStringFilter($_GET['s']);

	include 'data_connection.php';
	if($getFind != ''){
		$idList = array();

		$sString = $getFind;

		$find = explode(" ", $sString);

		$i=0;
		
		while (isset($find[$i])) {
			$idList = findResult($find[$i], $idList);	
			$i++;
		}

		$mulValue = array_count_values($idList);		
		$mulValueStore = $mulValue;

		$j=max($mulValue);
		for ($i=1; $i >0 ; $i++) { 
			
			$key = array_search($j, $mulValue);			
			if($key == ""){
				$j--;
			}else{		
				getSearchContent($key);
			}

			if($j==0){
				break;
			}
			unset($mulValue[$key]);
		}

		
	}
	else if($getS != ''){
		$s = $getS;
		$sql = "SELECT * FROM `search_suggestion` WHERE text_s LIKE '$s%'";
		$result = $conn->query($sql);
		if($result->num_rows>0){
			while($row = $result->fetch_assoc()){
				
				$suggestion = $row['text_s'];
				$s_text_type = $row['s_text_type'];
				$cata = $row[cata];
				$cata_item_id = $row[cata_item_id];

				// echo "<div style='' id='searchtitle'>" . 
				// "<a style='font-family: verdana; text-decoration: none;' 
				// onclick='search_result(\"".$suggestion."\",\"".$s_text_type."\",\"".$cata."\",\"".$cata_item_id."\")'> " . $suggestion . "</a>" . "
				// </div>";

				// echo "<div style='' id='searchtitle'>" . 
				// "<a style='font-family: verdana; text-decoration: none;'  href='./search_result.php?suggestion=".$suggestion."&sTextType=".$s_text_type."&cata=".$cata."&cataItemId=".$cata_item_id."&searchNum=first'
				// > " . $suggestion . "</a>" . "
				// </div>";

				echo "<div style='' id='searchtitle'>" . 
				"<a style='font-family: verdana; text-decoration: none;'  href='./search_result.php?find=".$suggestion."'> " . $suggestion . "</a>
				</div>";
			}
		}
	
	}

	$getSuggestion = searchStringFilter($_GET[suggestion]);
	$getSearchNum = searchStringFilter($_GET[searchNum]);

	if(isset($getSuggestion) && $getSearchNum=="first"){
		first_suggestion();
	}
	else if(isset($getSuggestion) && $getSearchNum=="next"){
		next_suggestion();
	}



	function findResult($find, $idList){
		include 'data_connection.php';
			$sql = "SELECT general_info.id, general_info.sub_cata, general_info.cata, general_info.name AS genName, general_info.image_link, general_info.description AS genDes, general_info.type, general_info.column_2, general_info.column_3,
							item_list.name AS itemName, item_list.description AS itemDes, item_list.price, item_list.filter_1, item_list.filter_2,
							map.location, map.address
			FROM `general_info` LEFT JOIN `item_list` ON general_info.id = item_list.cata_id
			LEFT JOIN `map` ON general_info.id = map.cata_id
			WHERE general_info.status = 'approved' AND 
			
			(general_info.name LIKE '%$find%' OR  general_info.description LIKE '%$find%' OR  general_info.type LIKE '%$find%' OR general_info.column_2 LIKE '%$find%' OR general_info.column_3 LIKE '%$find%' OR
			item_list.name LIKE '%$find%' OR  item_list.description LIKE '%$find%' OR  item_list.price LIKE '%$find%' OR  item_list.filter_1 LIKE '%$find%' OR  item_list.filter_2 LIKE '%$find%' OR
			map.location LIKE '%$find%' OR  map.address LIKE '%$find%')			
			ORDER BY CASE 
			WHEN general_info.name LIKE '%$find%' THEN 0
			WHEN general_info.type LIKE '%$find%' THEN 1
			WHEN item_list.name LIKE '%$find%' THEN 2
			WHEN general_info.column_3 LIKE '%$find%' THEN 3
			WHEN item_list.filter_1 LIKE '%$find%' THEN 4
			WHEN item_list.filter_2 LIKE '%$find%' THEN 5
			WHEN map.location LIKE '%$find%' THEN 6
			WHEN map.address LIKE '%$find%' THEN 7
			WHEN general_info.column_2 LIKE '%$find%' THEN 8
			WHEN general_info.description LIKE '%$find%' THEN 9
			WHEN item_list.description LIKE '%$find%' THEN 10
			WHEN item_list.price LIKE '%$find%' THEN 11
			ELSE 12 END
			
			;";

			// echo $sql;
			
			$tpm_idList = array();

			$result = $conn->query($sql);
			if($result->num_rows>0){
				while ($row = $result->fetch_assoc()) {
					

					if(in_array($row[id], $tpm_idList)){
					}
					else{
						array_push($tpm_idList, $row[id]);
					}
				}
			}
			$idList = array_merge($idList, $tpm_idList);
			return $idList;
	}


	function getSearchContent($id){
			include 'data_connection.php';
			$sql = "SELECT id, cata, sub_cata, name, image_link, description, type FROM `general_info` WHERE id = '$id'";
			$result = $conn->query($sql);
			if($result->num_rows>0){
				while ($row = $result->fetch_assoc()) {

						$link_name = $row[name];
						$link_name = str_replace(" ", "-", $link_name);

						$sub_cata = explode(" ", $row[sub_cata]);

						$subcata_url = $sub_cata;
						$subcata_url[0] = str_replace(",", "", $subcata_url[0]);

						$des = $row[description];
						$des = substr($des, 0, 310);
						echo '<div class="search_result_box">
						    	<div class="image_side">
						    		<img class="img-responsive" src="image/title_image/'.$row[image_link].'">		
						    	</div>
						    	<div class="data_side">
						    		<div class="bus_pro_name">
						    			<a href="'.$subcata_url[0].'/'.strtolower($link_name).'"><span >'.$row[name].'</span></a>
						    		</div>
						    		<div  class="bus_pro_cata_sub">
						    			 <span>'.$row[type].'</span>
						    		</div>
						    		<div  class="bus_pro_cata_sub">
						    			 <span>
						    			 	<a href="/'.$row[cata].'">'.$row[cata].'</a> > 
						    			 	<a href="/'.$sub_cata[0].'">'.$sub_cata[0].'</a>
						    			 	<a href="/'.$sub_cata[1].'">'.$sub_cata[1].'</a>
						    			 	<a href="/'.$sub_cata[2].'">'.$sub_cata[2].'</a>
						    			 </span>
						    		</div>
						    		<div class="bus_pro_des">
						    			<span>'.$des.'</span>
						    		</div>
						    		<!--<div class="search_item">
						    			<a class="search_item_name show-popup" href="#show" data-showpopup="54441" onclick="show_overlay(\'food\',1)">Item Name</a>
						    		</div> -->
						    	</div>
						    </div>

						';
				}
			}
			$idList = array_merge($idList, $tpm_idList);
			return $idList;
	}








	






?>
